package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.SubjectModel;

import java.util.ArrayList;

public class SubjectListAdapter  extends BaseAdapter {
	
	private Context mContext;
	private LayoutInflater layoutInfalater;
	private ArrayList<SubjectModel> standardModels = new ArrayList<>();
	private TextView txtStandard;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };
	
	public SubjectListAdapter(Context context, ArrayList<SubjectModel> standardModels) {
		this.mContext = context;
		this.standardModels = standardModels;
	}

	@Override
	public int getCount() {
		return standardModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.textview, parent, false);
		txtStandard = (TextView) convertView.findViewById(R.id.txtStanderd);
		convertView.setBackgroundColor(colors[position % colors.length]);
		txtStandard.setText(standardModels.get(position).getSubjectname());
		return convertView;
	}

}
