package com.edusunsoft.erp.orataro.Interface;

import com.edusunsoft.erp.orataro.model.RouteListResModel;

public interface OnRouteItemClickListener {
    void onItemClick(RouteListResModel.Data data);
}
