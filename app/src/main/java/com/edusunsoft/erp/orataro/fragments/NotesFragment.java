package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddNotesActivity;
import com.edusunsoft.erp.orataro.activities.ViewNoticeDetailsActivity;
import com.edusunsoft.erp.orataro.adapter.NotesListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.NoticeListDataDao;
import com.edusunsoft.erp.orataro.database.NoticeListModel;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.NotesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NotesFragment extends Fragment implements OnClickListener, OnItemClickListener, ResponseWebServices, RefreshListner {

    private PullAndLoadListView noticeListView;
    public static NotesListAdapter noticeListAdapter;
    private LinearLayout ll_add_new;
    private Dialog mAddNewDialog;
    private LinearLayout ll_comming_soon;
    private FrameLayout fl_main;
    private Context mContext;
    private TextView txt_nodatafound;
    private ArrayList<NotesModel> noticeModels;
    private EditText edt_subject_name, edt_notes_title, edt_note_details;
    private Spinner edt_dress_code;
    private TextView tv_start_date, tv_end_date;
    private int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1, R.drawable.teacher_2};
    private int pos;

    // linearlayout for search
    LinearLayout searchlayout;
    private EditText edtTeacherName;

    // variable declaration for homeworklist from ofline database
    NoticeListDataDao noticeListDataDao;
    private List<NoticeListModel> NoticeList = new ArrayList<>();
    NoticeListModel noticeModel = new NoticeListModel();
    public String ISDataExist = "";
    /*END*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.commonlist_fragment, container, false);
        mContext = getActivity();
        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Notes) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        noticeListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).noticeListDataDao();

        searchlayout = (LinearLayout) view.findViewById(R.id.searchlayout);
        edtTeacherName = (EditText) view.findViewById(R.id.edtsearchStudent);

        fl_main = (FrameLayout) view.findViewById(R.id.fl_main);
        ll_comming_soon = (LinearLayout) view.findViewById(R.id.ll_comming_soon);
        fl_main.setVisibility(View.VISIBLE);
        ll_comming_soon.setVisibility(View.GONE);
        noticeListView = (PullAndLoadListView) view.findViewById(R.id.homeworklist);
        txt_nodatafound = (TextView) view.findViewById(R.id.txt_nodatafound);
        ll_add_new = (LinearLayout) view.findViewById(R.id.ll_add_new);
        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.NOTE).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.GONE);
        }

        ll_add_new.setOnClickListener(this);

        Utility.ISLOADNOTE = false;

        // Display HomeworkList from offline as well as online
        try {
            DisplayNoticeList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*END*/

        noticeListView.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {
                // Display NoteList from offline as well as online
                try {
                    DisplayNoticeList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/
            }
        });


        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                noticeListAdapter.filter(text);
            }

        });

        noticeListView.setOnItemClickListener(this);

        return view;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        pos = position;
        if (Utility.isNetworkAvailable(mContext)) {
            if (!Global.notesModels.get(pos).isIsRead()) {
                noteisread();
            } else {
                Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
                intent.putExtra("NotesModel", Global.notesModels.get(pos));
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            }
        } else {
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
    }

    @Override
    public void onResume() {


        if (Utility.ISLOADNOTE) {
            // Display NoteList from offline as well as online
            try {
                DisplayNoticeList();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        /*END*/
        } else {
        }

        super.onResume();

//        // Display HomeworkList from offline as well as online
//        try {
//            DisplayNoticeList();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        /*END*/
    }



    private void DisplayNoticeList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            NoticeList = noticeListDataDao.getNoticeList();
            if (NoticeList.isEmpty() || NoticeList.size() == 0 || NoticeList == null) {
                ISDataExist = "true";
                notelist(true);
            } else {
                //set Adapter
                setNoticelistAdapter(NoticeList);
                /*END*/
                notelist(false);
            }
        } else {
            NoticeList = noticeListDataDao.getNoticeList();
            if (NoticeList != null) {
                setNoticelistAdapter(NoticeList);
            }
        }
    }

    private void setNoticelistAdapter(List<NoticeListModel> noticeList) {
        if (noticeList != null && noticeList.size() > 0) {

            searchlayout.setVisibility(View.VISIBLE);
            Collections.sort(noticeList, new SortedDate());
            Collections.reverse(noticeList);
            for (int i = 0; i < noticeList.size(); i++) {
                if (i != 0) {
                    String temp = noticeList.get(i - 1).getMonth();
                    if (temp.equalsIgnoreCase(noticeList.get(i)
                            .getMonth())) {

                        if (noticeList.get(i).getYear().equalsIgnoreCase(noticeList.get(i - 1).getYear())) {
                            noticeList.get(i).setVisible(false);
                        } else {
                            noticeList.get(i).setVisible(true);
                        }
                    } else {
                        noticeList.get(i).setVisible(true);
                    }
                } else {
                    noticeList.get(i).setVisible(true);
                }

            }

            noticeListAdapter = new NotesListAdapter(mContext, noticeList, this);
            noticeListView.setAdapter(noticeListAdapter);
            txt_nodatafound.setVisibility(View.GONE);

        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NotesListNotAvailable));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_add_new:
                Intent intent;
                if (Utility.isTeacher(mContext)) {
                    intent = new Intent(mContext, AddNotesActivity.class);
                    intent.putExtra("isFrom", ServiceResource.NOTE_FLAG_TEACHER);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                }
                break;

            default:

                break;

        }
    }

    public void notelist(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLENAME, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        Log.d("noterequest", arrayList.toString());
        new AsynsTaskClass(getActivity(), arrayList, isViewPopup, this).execute(ServiceResource.NOTES_METHODNAME, ServiceResource.NOTES_URL);

    }

    public void noteisread() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, Global.notesModels.get(pos).getNotesID()));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Note"));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.NOTES_METHODNAME.equalsIgnoreCase(methodName)) {
            parseNote(result, ServiceResource.UPDATE);
        }

        if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
            intent.putExtra("NotesModel", Global.notesModels.get(pos));
            startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        }

    }

    public class SortedDate implements Comparator<NoticeListModel> {
        @Override
        public int compare(NoticeListModel o1, NoticeListModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }

    @Override
    public void refresh(String methodName) {

        if (Utility.isNetworkAvailable(mContext)) {

            notelist(false);

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    public void parseNote(String result, String update) {

        if (result != null
                && !result.toString().equals(
                "[{\"message\":\"No Data Found\",\"success\":0}]")) {
            Log.d("noteresult", result);
            JSONArray jsonObj;
            noticeListDataDao.deleteNoticeListItem();
            try {
                Global.notesModels = new ArrayList<NotesModel>();
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);

                    // parse homework list data
                    ParseNoticeList(innerObj);
                    /*END*/
                }

                if (noticeListDataDao.getNoticeList().size() > 0) {
                    //set Adapter
                    setNoticelistAdapter(noticeListDataDao.getNoticeList());
                    /*END*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NotesListNotAvailable));
        }


    }

    private void ParseNoticeList(JSONObject innerObj) {
        try {
            noticeModel.setNoteDate(innerObj.getString("DayOfMonth") + " " + innerObj.getString("DATENAME"));
            noticeModel.setNotesID(innerObj.getString(ServiceResource.NOTES_NOTESID));
            noticeModel.setGradeID(innerObj.getString(ServiceResource.NOTES_GRADEID));
            noticeModel.setDivisionID(innerObj.getString(ServiceResource.NOTES_DIVISIONID));
            noticeModel.setSubjectID(innerObj.getString(ServiceResource.NOTES_SUBJECTID));
            noticeModel.setSubjectName(innerObj.getString(ServiceResource.NOTES_SUBJECTNAME));
            noticeModel.setStrActionStartDate(innerObj.getString(ServiceResource.NOTES_STRACTIONSTARTDATE));
            noticeModel.setReferenceLink(innerObj.getString(ServiceResource.NOTES_REFERENCELINK));
            noticeModel.setImgUrl(ServiceResource.BASE_IMG_URL + ServiceResource.DATAFILE + innerObj.getString(ServiceResource.PHOTOPARSE));

            if (noticeModel.getSubjectName() != null && !noticeModel.getSubjectName().equals("") && !noticeModel.getSubjectName().equals("null")) {
            } else {
                noticeModel.setSubjectName("");
            }

            Log.d("getStartDate", innerObj.getString(ServiceResource.NOTES_ACTIONSTARTDATE));

            try {
                if (!innerObj.getString(ServiceResource.NOTES_ACTIONSTARTDATE).equalsIgnoreCase("null")) {

                    String tempdate = innerObj
                            .getString(ServiceResource.NOTES_ACTIONSTARTDATE)
                            .replace("/Date(", "").replace(")/", "");
                    SimpleDateFormat formatter = new SimpleDateFormat("EEEE/dd/MMM/yyyy");
                    String dateString = formatter.format(new Date(Long.parseLong(tempdate)));
                    Log.d("convertedDate", dateString);

                    String[] dateArray = dateString.split("/");
                    if (dateArray != null && dateArray.length > 0) {

                        noticeModel.setDay(dateArray[1] + " " + dateArray[0].substring(0, 3));
                        Log.d("daydate", dateArray[1] + " " + dateArray[0].substring(0, 3));
                        noticeModel.setMonth(dateArray[2]);
                        noticeModel.setYear(dateArray[3]);

                    }

                    noticeModel.setMilliSecond("" +
                            Utility.ConvertTimeSTamptoMilliseconds(innerObj
                                    .getString(ServiceResource.NOTES_ACTIONSTARTDATE)
                                    .replace("/Date(", "").replace(")/", ""), "MM/dd/yyyy"));

                    noticeModel.setDateOfNotes(innerObj.getString(ServiceResource.NOTES_ACTIONSTARTDATE));
                    noticeModel.setNoteTitle(innerObj.getString(ServiceResource.NOTES_NOTETITLE));
                    noticeModel.setNoteDetails(innerObj.getString(ServiceResource.NOTES_NOTEDETAILS));
                    noticeModel.setActionStartDate(Utility.dateFormatter(
                            innerObj
                                    .getString(
                                            ServiceResource.NOTES_ACTIONSTARTDATE)
                                    .replace("/Date(", "")
                                    .replace(")/", ""),
                            "dd/MM/yyyy", "MM/dd/yyyy"));

                    try {

                        noticeModel.setActionEndDate(Utility.dateFormatter(
                                innerObj
                                        .getString(
                                                ServiceResource.NOTES_ACTIONENDDATE)
                                        .replace("/Date(", "")
                                        .replace(")/", ""),
                                "dd/MM/yyyy", "MM/dd/yyyy"));

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                    noticeModel.setDressCode(innerObj.getString(ServiceResource.NOTES_DRESSCODE));
                    noticeModel.setRead(Boolean.valueOf(innerObj.getString(ServiceResource.NOTES_ISREAD)));
                    noticeModel.setIamReady(Boolean.valueOf(innerObj.getString(ServiceResource.NOTES_IAMREADY)));
                    noticeModel.setParentNote(innerObj.getString(ServiceResource.NOTES_PARENTNOTE));
                    noticeModel.setRating(innerObj.getString(ServiceResource.NOTES_RATING));
                    noticeModel.setUserName(innerObj.getString(ServiceResource.NOTES_USERNAME));
                    noticeModel.setProfilePicture(innerObj.optString(ServiceResource.NOTES_PROFILEPICTURE));
                    String isCheck = innerObj.optString(ServiceResource.HOMEWORK_ISCHECKED);

                    if (isCheck.equalsIgnoreCase("true")) {
                        noticeModel.setCheck(true);
                    } else {
                        noticeModel.setCheck(false);
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() == ServiceResource.USER_STUDENT_INT) {
                        if (noticeModel.getProfilePicture() != null && !noticeModel.getProfilePicture().equals("null")) {
                            if (noticeModel.getProfilePicture().contains("/img/"))
                                noticeModel.setProfilePicture(ServiceResource.BASE_IMG_URL1 + noticeModel.getProfilePicture());
                            else
                                noticeModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + noticeModel
                                                .getProfilePicture());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        noticeListDataDao.insertNoticeList(noticeModel);

    }

}
