
package com.edusunsoft.erp.orataro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("StudentCode")
    @Expose
    private Object studentCode;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("InstituteCode")
    @Expose
    private String instituteCode;
    @SerializedName("DisplayName")
    @Expose
    private String displayName;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("ClientID")
    @Expose
    private String clientID;
    @SerializedName("InstituteID")
    @Expose
    private String instituteID;
    @SerializedName("RoleID")
    @Expose
    private String roleID;
    @SerializedName("OrderNo")
    @Expose
    private Object orderNo;
    @SerializedName("RoleName")
    @Expose
    private String roleName;
    @SerializedName("WallID")
    @Expose
    private String wallID;
    @SerializedName("GradeName")
    @Expose
    private String gradeName;
    @SerializedName("GradeID")
    @Expose
    private Object gradeID;
    @SerializedName("DivisionName")
    @Expose
    private String divisionName;
    @SerializedName("MemberType")
    @Expose
    private String memberType;
    @SerializedName("DivisionID")
    @Expose
    private Object divisionID;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("WallImage")
    @Expose
    private String wallImage;
    @SerializedName("PrimaryName")
    @Expose
    private String primaryName;
    @SerializedName("PostByType")
    @Expose
    private String postByType;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("InstitutionWallID")
    @Expose
    private String institutionWallID;
    @SerializedName("InstitutionWallImage")
    @Expose
    private String institutionWallImage;
    @SerializedName("SubDomain")
    @Expose
    private String subDomain;
    @SerializedName("InstituteName")
    @Expose
    private String instituteName;
    @SerializedName("IsAllowUserToPostStatus")
    @Expose
    private Boolean isAllowUserToPostStatus;
    @SerializedName("IsAllowUserToPostComment")
    @Expose
    private Boolean isAllowUserToPostComment;
    @SerializedName("IsAllowUserToPostVideo")
    @Expose
    private Boolean isAllowUserToPostVideo;
    @SerializedName("IsAllowUserToPostFiles")
    @Expose
    private Boolean isAllowUserToPostFiles;
    @SerializedName("IsAllowUserToSharePost")
    @Expose
    private Boolean isAllowUserToSharePost;
    @SerializedName("IsAllowUserToLikeDislikes")
    @Expose
    private Boolean isAllowUserToLikeDislikes;
    @SerializedName("IsAllowUserToPostPhoto")
    @Expose
    private Boolean isAllowUserToPostPhoto;
    @SerializedName("Prayer")
    @Expose
    private String prayer;
    @SerializedName("Information")
    @Expose
    private Object information;
    @SerializedName("ExamTimeTable")
    @Expose
    private String examTimeTable;
    @SerializedName("SchoolTiming")
    @Expose
    private String schoolTiming;
    @SerializedName("DeviceIdentity")
    @Expose
    private String deviceIdentity;
    @SerializedName("BatchID")
    @Expose
    private String batchID;
    @SerializedName("BatchStart")
    @Expose
    private String batchStart;
    @SerializedName("BatchEnd")
    @Expose
    private String batchEnd;
    @SerializedName("ERPUserID")
    @Expose
    private String eRPUserID;
    @SerializedName("ERPMemberID")
    @Expose
    private String eRPMemberID;


    @SerializedName("UserType")
    @Expose
    private int userType;

    @SerializedName("save")
    @Expose
    private boolean save;

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public Object getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(Object studentCode) {
        this.studentCode = studentCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getInstituteCode() {
        return instituteCode;
    }

    public void setInstituteCode(String instituteCode) {
        this.instituteCode = instituteCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getInstituteID() {
        return instituteID;
    }

    public void setInstituteID(String instituteID) {
        this.instituteID = instituteID;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public Object getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Object orderNo) {
        this.orderNo = orderNo;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getWallID() {
        return wallID;
    }

    public void setWallID(String wallID) {
        this.wallID = wallID;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public Object getGradeID() {
        return gradeID;
    }

    public void setGradeID(Object gradeID) {
        this.gradeID = gradeID;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public Object getDivisionID() {
        return divisionID;
    }

    public void setDivisionID(Object divisionID) {
        this.divisionID = divisionID;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getWallImage() {
        return wallImage;
    }

    public void setWallImage(String wallImage) {
        this.wallImage = wallImage;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getPostByType() {
        return postByType;
    }

    public void setPostByType(String postByType) {
        this.postByType = postByType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getInstitutionWallID() {
        return institutionWallID;
    }

    public void setInstitutionWallID(String institutionWallID) {
        this.institutionWallID = institutionWallID;
    }

    public String getInstitutionWallImage() {
        return institutionWallImage;
    }

    public void setInstitutionWallImage(String institutionWallImage) {
        this.institutionWallImage = institutionWallImage;
    }

    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public Boolean getIsAllowUserToPostStatus() {
        return isAllowUserToPostStatus;
    }

    public void setIsAllowUserToPostStatus(Boolean isAllowUserToPostStatus) {
        this.isAllowUserToPostStatus = isAllowUserToPostStatus;
    }

    public Boolean getIsAllowUserToPostComment() {
        return isAllowUserToPostComment;
    }

    public void setIsAllowUserToPostComment(Boolean isAllowUserToPostComment) {
        this.isAllowUserToPostComment = isAllowUserToPostComment;
    }

    public Boolean getIsAllowUserToPostVideo() {
        return isAllowUserToPostVideo;
    }

    public void setIsAllowUserToPostVideo(Boolean isAllowUserToPostVideo) {
        this.isAllowUserToPostVideo = isAllowUserToPostVideo;
    }

    public Boolean getIsAllowUserToPostFiles() {
        return isAllowUserToPostFiles;
    }

    public void setIsAllowUserToPostFiles(Boolean isAllowUserToPostFiles) {
        this.isAllowUserToPostFiles = isAllowUserToPostFiles;
    }

    public Boolean getIsAllowUserToSharePost() {
        return isAllowUserToSharePost;
    }

    public void setIsAllowUserToSharePost(Boolean isAllowUserToSharePost) {
        this.isAllowUserToSharePost = isAllowUserToSharePost;
    }

    public Boolean getIsAllowUserToLikeDislikes() {
        return isAllowUserToLikeDislikes;
    }

    public void setIsAllowUserToLikeDislikes(Boolean isAllowUserToLikeDislikes) {
        this.isAllowUserToLikeDislikes = isAllowUserToLikeDislikes;
    }

    public Boolean getIsAllowUserToPostPhoto() {
        return isAllowUserToPostPhoto;
    }

    public void setIsAllowUserToPostPhoto(Boolean isAllowUserToPostPhoto) {
        this.isAllowUserToPostPhoto = isAllowUserToPostPhoto;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

    public Object getInformation() {
        return information;
    }

    public void setInformation(Object information) {
        this.information = information;
    }

    public String getExamTimeTable() {
        return examTimeTable;
    }

    public void setExamTimeTable(String examTimeTable) {
        this.examTimeTable = examTimeTable;
    }

    public String getSchoolTiming() {
        return schoolTiming;
    }

    public void setSchoolTiming(String schoolTiming) {
        this.schoolTiming = schoolTiming;
    }

    public String getDeviceIdentity() {
        return deviceIdentity;
    }

    public void setDeviceIdentity(String deviceIdentity) {
        this.deviceIdentity = deviceIdentity;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public String getBatchStart() {
        return batchStart;
    }

    public void setBatchStart(String batchStart) {
        this.batchStart = batchStart;
    }

    public String getBatchEnd() {
        return batchEnd;
    }

    public void setBatchEnd(String batchEnd) {
        this.batchEnd = batchEnd;
    }

    public String getERPUserID() {
        return eRPUserID;
    }

    public void setERPUserID(String eRPUserID) {
        this.eRPUserID = eRPUserID;
    }

    public String getERPMemberID() {
        return eRPMemberID;
    }

    public void setERPMemberID(String eRPMemberID) {
        this.eRPMemberID = eRPMemberID;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {

        return
                "studentCode=" + studentCode +
                        ", mobileNumber='" + mobileNumber + '\'' +
                        ", userID='" + userID + '\'' +
                        ", instituteCode='" + instituteCode + '\'' +
                        ", displayName='" + displayName + '\'' +
                        ", memberID='" + memberID + '\'' +
                        ", clientID='" + clientID + '\'' +
                        ", instituteID='" + instituteID + '\'' +
                        ", roleID='" + roleID + '\'' +
                        ", orderNo=" + orderNo +
                        ", roleName='" + roleName + '\'' +
                        ", wallID='" + wallID + '\'' +
                        ", gradeName=" + gradeName +
                        ", gradeID=" + gradeID +
                        ", divisionName=" + divisionName +
                        ", memberType='" + memberType + '\'' +
                        ", divisionID=" + divisionID +
                        ", profilePicture='" + profilePicture + '\'' +
                        ", wallImage='" + wallImage + '\'' +
                        ", primaryName='" + primaryName + '\'' +
                        ", postByType='" + postByType + '\'' +
                        ", fullName='" + fullName + '\'' +
                        ", institutionWallID='" + institutionWallID + '\'' +
                        ", institutionWallImage='" + institutionWallImage + '\'' +
                        ", subDomain='" + subDomain + '\'' +
                        ", instituteName='" + instituteName + '\'' +
                        ", isAllowUserToPostStatus=" + isAllowUserToPostStatus +
                        ", isAllowUserToPostComment=" + isAllowUserToPostComment +
                        ", isAllowUserToPostVideo=" + isAllowUserToPostVideo +
                        ", isAllowUserToPostFiles=" + isAllowUserToPostFiles +
                        ", isAllowUserToSharePost=" + isAllowUserToSharePost +
                        ", isAllowUserToLikeDislikes=" + isAllowUserToLikeDislikes +
                        ", isAllowUserToPostPhoto=" + isAllowUserToPostPhoto +
                        ", prayer='" + prayer + '\'' +
                        ", information=" + information +
                        ", examTimeTable='" + examTimeTable + '\'' +
                        ", schoolTiming='" + schoolTiming + '\'' +
                        ", deviceIdentity='" + deviceIdentity + '\'' +
                        ", batchID='" + batchID + '\'' +
                        ", batchStart='" + batchStart + '\'' +
                        ", batchEnd='" + batchEnd + '\'' +
                        ", eRPUserID='" + eRPUserID + '\'' +
                        ", eRPMemberID='" + eRPMemberID + '\'' +
                        '}';
    }
}
