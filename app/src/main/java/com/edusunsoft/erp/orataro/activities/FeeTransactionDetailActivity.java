package com.edusunsoft.erp.orataro.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FeesReceiptTransactionDetailListAdapter;
import com.edusunsoft.erp.orataro.fragments.FeeReciptFragment;
import com.edusunsoft.erp.orataro.util.Utility;

public class FeeTransactionDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView student_list;
    LinearLayoutManager linearLayoutManager;
    LinearLayout lyl_feestransdetailview;

    private ImageView imgLeftheader;
    public TextView header_text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checked_student_list_activity);

        Initialization();

    }

    private void Initialization() {


        imgLeftheader = (ImageView) findViewById(R.id.img_back);
        imgLeftheader.setOnClickListener(this);

        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText(getResources().getString(R.string.fee_transaction_detail));

        lyl_feestransdetailview = (LinearLayout) findViewById(R.id.lyl_feestransdetailview);
        lyl_feestransdetailview.setVisibility(View.VISIBLE);

        student_list = (RecyclerView) findViewById(R.id.student_list);
        linearLayoutManager = new LinearLayoutManager(this);
        // use a linear layout manager
        student_list.setLayoutManager(linearLayoutManager);


        if (FeeReciptFragment.FeesTransactionDetailList.size() > 0) {

            FeesReceiptTransactionDetailListAdapter transactiondetailAdapter = new FeesReceiptTransactionDetailListAdapter(this, FeeReciptFragment.FeesTransactionDetailList);
            student_list.setAdapter(transactiondetailAdapter);

        } else {

            student_list.setAdapter(null);
            Utility.showToast("No Data Found", this);
//            lyl_transaction_detail.setVisibility(View.GONE);
//            lvfees_transaction_detail.setAdapter(null);
//            txt_nodatafound_transactiion.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                this.finish();
                break;
        }
    }
}
