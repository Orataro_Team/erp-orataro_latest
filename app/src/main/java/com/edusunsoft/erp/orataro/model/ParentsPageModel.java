package com.edusunsoft.erp.orataro.model;

public class ParentsPageModel {

	private String pr_date;
	private String pr_note;
	private String pr_to_teacher;
	private boolean isVisibleMonth = false;
	private String pr_month;

	public String getPr_date() {
		return pr_date;
	}

	public void setPr_date(String pr_date) {
		this.pr_date = pr_date;
	}

	public String getPr_note() {
		return pr_note;
	}

	public void setPr_note(String pr_note) {
		this.pr_note = pr_note;
	}

	public boolean isVisibleMonth() {
		return isVisibleMonth;
	}

	public void setVisibleMonth(boolean isVisibleMonth) {
		this.isVisibleMonth = isVisibleMonth;
	}

	public String getPr_to_teacher() {
		return pr_to_teacher;
	}

	public void setPr_to_teacher(String pr_to_teacher) {
		this.pr_to_teacher = pr_to_teacher;
	}

	public String getPr_month() {
		return pr_month;
	}

	public void setPr_month(String pr_month) {
		this.pr_month = pr_month;
	}

}
