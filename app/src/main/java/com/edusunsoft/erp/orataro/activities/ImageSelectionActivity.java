package com.edusunsoft.erp.orataro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edusunsoft.erp.orataro.FragmentActivity.BucketHomeFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ImageCount;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.gallery.MediaChooser;
import com.edusunsoft.erp.orataro.model.ImageModel;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.util.Application;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;


public class ImageSelectionActivity extends Activity implements ImageCount {

    GridView grdImage;
    boolean isActivityIopen = true;
    private ArrayList<ImageModel> imageModels;
    private ImageAdapter imageAdapter;
    private Button btnSelect;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getIntent() != null) {
            count = getIntent().getIntExtra("count", 0);
        }


        if (isActivityIopen) {
//			MediaChooser.setSelectionLimit(count);
            Application p = (Application) getApplication();
            p.setSelectionLimit(count);
            isActivityIopen = false;
            IntentFilter videoIntentFilter = new IntentFilter(MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
            registerReceiver(videoBroadcastReceiver, videoIntentFilter);

            IntentFilter imageIntentFilter = new IntentFilter(MediaChooser.IMAGE_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
            registerReceiver(imageBroadcastReceiver, imageIntentFilter);


            Intent intent = new Intent(ImageSelectionActivity.this, BucketHomeFragmentActivity.class);
            startActivityForResult(intent, 1);
        }

        //		grdImage = (GridView) findViewById(R.id.grdImage);
        //
        //		btnSelect = (Button) findViewById(R.id.btnSelect);
        //
        //
        //		imageAdapter = new ImageAdapter(getApplicationContext(),ImageSelectionActivity.this);
        //		grdImage.setAdapter(imageAdapter);
        //
        //		loadImages();
        //		btnSelect.setVisibility(View.GONE);
        //
        //		btnSelect.setOnClickListener(new OnClickListener() {
        //
        //			@Override
        //			public void onClick(View paramView) {
        //				Intent intent = new Intent();
        //				intent.putParcelableArrayListExtra("list",imageAdapter.getSelectedImage());
        //				//				intent.putExtra("GetFileName",o.getName());
        //				setResult(RESULT_OK, intent);
        //				finish();
        //				//				Toast.makeText(MainActivity.this, ""+imageAdapter.getSelectedImage().size(), 1).show();;
        //			}
        //		});
    }

    BroadcastReceiver videoBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //			Toast.makeText(ImageSelectionActivity.this, "yippiee Video ", Toast.LENGTH_SHORT).show();
            //			Toast.makeText(ImageSelectionActivity.this, "Video SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();
            //			setAdapter(intent.getStringArrayListExtra("list"));


        }
    };


    BroadcastReceiver imageBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //			Toast.makeText(ImageSelectionActivity.this, "yippiee Image ", Toast.LENGTH_SHORT).show();
            //			Toast.makeText(ImageSelectionActivity.this, "Image SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();
            //				ArrayList<LoadedImage> seletedPhoto = new ArrayList<LoadedImage>();
            //
            //				for(int i =0;i<intent.getStringArrayListExtra("list").size();i++){
            //					LoadedImage loadImage = new LoadedImage();
            //					loadImage.setUri(Uri.parse(intent.getStringArrayListExtra("list").get(i)));
            //
            //					seletedPhoto.add(loadImage);
            //				}

            ArrayList<LoadedImage> seletedPhoto = new ArrayList<LoadedImage>();

            for (int i = 0; i < intent.getStringArrayListExtra("list").size(); i++) {
                LoadedImage loadImage = new LoadedImage();
                loadImage.setUri(Uri.parse(intent.getStringArrayListExtra("list").get(i)));
                seletedPhoto.add(loadImage);
            }

            Intent intent1 = new Intent();
            intent1.putParcelableArrayListExtra("list", seletedPhoto);
            //				intent.putExtra("GetFileName",o.getName());
            ImageSelectionActivity.this.setResult(RESULT_OK, intent1);
            finish();
            //				Intent intent1 = new Intent();
            //				intent1.putStringArrayListExtra("list",intent.getStringArrayListExtra("list"));
            //				//				intent.putExtra("GetFileName",o.getName());
            //				ImageSelectionActivity.this.setResult(RESULT_OK, intent1);
            //				finish();

            //			setAdapter(intent.getStringArrayListExtra("list"));
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            ArrayList<LoadedImage> seletedPhoto = new ArrayList<LoadedImage>();
            if (data != null) {
                for (int i = 0; i < data.getStringArrayListExtra("list").size(); i++) {
                    LoadedImage loadImage = new LoadedImage();
                    loadImage.setUri(Uri.parse(data.getStringArrayListExtra("list").get(i)));

                    seletedPhoto.add(loadImage);
                }
            }
            Intent intent1 = new Intent();
            intent1.putParcelableArrayListExtra("list", seletedPhoto);
            //				intent.putExtra("GetFileName",o.getName());
            ImageSelectionActivity.this.setResult(RESULT_OK, intent1);
            finish();
        }
    }


    protected void onDestroy() {
        super.onDestroy();
        //		isActivityIopen = false;
        unregisterReceiver(imageBroadcastReceiver);
        unregisterReceiver(videoBroadcastReceiver);

    }

    private void loadImages() {
        @SuppressWarnings("deprecation")
        Object lastNonConfigurationInstance = getLastNonConfigurationInstance();
        final Object data = lastNonConfigurationInstance;
        if (data == null) {
            if (isActivityIopen) {
                new LoadImagesFromSDCard().execute();
            }
        } else {
            final LoadedImage[] photos = (LoadedImage[]) data;
            if (photos.length == 0) {
                if (isActivityIopen) {
                    new LoadImagesFromSDCard().execute();
                }
            }
            for (LoadedImage photo : photos) {
                addImage(photo);
            }
        }
    }


    private void addImage(LoadedImage... value) {
        for (LoadedImage image : value) {
            imageAdapter.addPhoto(image);
            imageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        final GridView grid = grdImage;
        final int count = grid.getChildCount();
        final LoadedImage[] list = new LoadedImage[count];

        for (int i = 0; i < count; i++) {
            final ImageView v = (ImageView) grid.getChildAt(i);
            list[i] = new LoadedImage(((BitmapDrawable) v.getDrawable()).getBitmap(), null, false);
        }

        return list;
    }


    class LoadImagesFromSDCard extends AsyncTask<Object, LoadedImage, Object> {


        /**
         * Load images from SD Card in the background, and display each image on the screen.
         *
//         * @see AsyncTask#doInBackground(Params[])
         */

        @Override
        protected Object doInBackground(Object... params) {

            imageModels = new ArrayList<ImageModel>();

            //setProgressBarIndeterminateVisibility(true);

            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            Uri uri = null;
            final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION, MediaStore.Images.ImageColumns.DATE_TAKEN};
            final String orderBy = MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC";

            //		        Cursor imageCursor =
            // Set up an array of the Thumbnail Image ID column we want
            //			String[] projection = {MediaStore.Images.Thumbnails._ID};
            String[] projection = new String[]{
                    MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DATA,
                    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.DATE_TAKEN,
                    MediaStore.Images.ImageColumns.MIME_TYPE
            };
            // Create the cursor pointing to the SDCard
            Cursor cursor =
                    //					managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);

                    managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            projection, // Which columns to return
                            null,       // Return all rows
                            null,
                            orderBy);
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns._ID);
            int size = cursor.getCount();
            // If size is 0, there are no images on the SD Card.
            if (size == 0) {
                //No Images available, post some message to the aboutorataro
            }
            int imageID = 0;
            for (int i = 0; i < size; i++) {
                if (cursor != null && !cursor.isClosed()) {
                    //					try{
                    cursor.moveToPosition(i);
                    imageID = cursor.getInt(columnIndex);
                    uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + imageID);
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                        if (bitmap != null) {
                            newBitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
                            bitmap.recycle();
                            if (newBitmap != null) {
                                publishProgress(new LoadedImage(newBitmap, uri, false));

                                ImageModel imageModel = new ImageModel();
                                imageModel.setUri(uri.toString());
                                imageModels.add(imageModel);
                            }
                        }

                    } catch (Exception e) {

                        //Error fetching image, try to recover

                    }

                }

            }
            cursor.close();
            return null;
        }

        /**
         * Add a new LoadedImage in the images grid.
         *
         * @param value The image.
         */
        @Override
        public void onProgressUpdate(LoadedImage... value) {
            addImage(value);
        }

        /**
         * Set the visibility of the progress bar to false.
         *
         * @see AsyncTask#onPostExecute(Object)
         */
        @Override
        protected void onPostExecute(Object result) {
            setProgressBarIndeterminateVisibility(false);
        }
    }


    class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private ArrayList<LoadedImage> photos = new ArrayList<LoadedImage>();
        ImageCount imgcount;

        public ImageAdapter(Context context, ImageCount imgcount) {
            mContext = context;
            this.imgcount = imgcount;
        }

        public void addPhoto(LoadedImage photo) {
            photos.add(photo);
        }

        public int getCount() {
            return photos.size();
        }

        public Object getItem(int position) {
            return photos.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ResourceAsColor")
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInfalater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //			if(convertView==null)
            convertView = layoutInfalater.inflate(R.layout.imageraw,
                    parent, false);
            final LinearLayout ll_selector = (LinearLayout) convertView.findViewById(R.id.ll_selector);
            final ImageView imageView = (ImageView) convertView.findViewById(R.id.imgShell);
            final CheckBox chkImage = (CheckBox) convertView.findViewById(R.id.chkImage);


            if (photos.get(position).isSelected()) {

                chkImage.setChecked(true);
                ll_selector.setBackgroundColor(R.color.blue);

            } else {

                chkImage.setChecked(false);
                ll_selector.setBackgroundColor(R.color.grey_list);

            }

            chkImage.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton paramCompoundButton,
                                             boolean paramBoolean) {
                    if (paramBoolean) {


                        if (getSelectedImage().size() >= count) {

                            Utility.toast(mContext, "You already added " + count + " Image");

                        } else {

                            photos.get(position).setSelected(paramBoolean);
                            imgcount.imageCount(getSelectedImage().size());
                        }

                    } else {

                        photos.get(position).setSelected(paramBoolean);
                        imgcount.imageCount(getSelectedImage().size());

                    }


                }
            });
            //            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            //            imageView.setPadding(8, 8, 8, 8);
            ll_selector.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    //					posArray[position]=position;
                    if (getSelectedImage().size() >= count) {
                        if (!photos.get(position).isSelected()) {
                            Utility.toast(mContext, "You already added " + count + " Image");
//							Toast.makeText(mContext, "You already added "+count+" Image", 1).show();
                        } else {

                            chkImage.setSelected(false);
                            photos.get(position).setSelected(false);
                            ll_selector.setBackgroundColor(R.color.grey_list);
                            imgcount.imageCount(getSelectedImage().size());
                        }

                    } else {

                        if (!photos.get(position).isSelected()) {

                            chkImage.setSelected(true);
                            photos.get(position).setSelected(true);
                            ll_selector.setBackgroundColor(R.color.blue);
                            imgcount.imageCount(getSelectedImage().size());
                        } else {
                            chkImage.setSelected(false);
                            photos.get(position).setSelected(false);
                            ll_selector.setBackgroundColor(R.color.grey_list);
                            imgcount.imageCount(getSelectedImage().size());
                        }
                    }
                }
            });


            imageView.setImageBitmap(photos.get(position).getBitmap());
            return convertView;
        }

        public ArrayList<LoadedImage> getSelectedImage() {
            ArrayList<LoadedImage> seletedPhoto = new ArrayList<LoadedImage>();

            for (int i = 0; i < photos.size(); i++) {
                if (photos.get(i).isSelected()) {
                    seletedPhoto.add(photos.get(i));
                }
            }

            return seletedPhoto;
        }
    }


    @Override
    public void imageCount(int size) {
        if (size > 0) {
            btnSelect.setVisibility(View.VISIBLE);
            btnSelect.setText("ADD(" + (size) + ")IMAGE");
        } else {
            btnSelect.setVisibility(View.GONE);
        }
    }

}
