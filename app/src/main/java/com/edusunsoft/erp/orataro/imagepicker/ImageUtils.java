package com.edusunsoft.erp.orataro.imagepicker;


import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;

public class ImageUtils {

    public static void saveBitmap(Bitmap bmp, String filePath, Bitmap.CompressFormat format, int quality) {
        FileOutputStream fo;
        try {
            File f = new File(filePath);
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }

            if (f.exists()) {
                f.delete();
            }
            f.createNewFile();
            fo = new FileOutputStream(f, true);
            bmp.compress(format, quality, fo);
            fo.flush();
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
