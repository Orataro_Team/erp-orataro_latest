package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class CreateEventActivity extends AppCompatActivity implements View.OnClickListener, ResponseWebServices {

    EditText edt_event_title, edt_event_details;
    Spinner spn_event_type;
    TextView tv_start_date, tv_end_date, txtGradeDivision, tv_reminder_date, header_text;
    ImageView img_back, iv_save;
    ArrayAdapter<String> eventTypeadapter;
    Context context;
    private String gradeDivisionIdStr = "";

    private int date;
    private static final int DATE_DIALOG_ID = 111;
    private int myear, month, day;
    private Calendar c;
    private int mHour, mMinute;
    private String todayDate;
    String spnTypeStr;
    public boolean DateCompare, ReminderDateComparison, StartDateComparison;
    ImageView img_speack, img_speack_description;
    protected static final int RESULT_SPEECH = 121, RESULT_SPEECH_DESCRIPTION = 122;
    LinearLayout ll_save;

    // added to add link for online video url or link - 05-06-2020
    EditText edt_referanceLink;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_event_activity);

        Initialization();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == RESULT_SPEECH) {

                ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edt_event_title.setText(edt_event_title.getText().toString() + "" + text.get(0));

            }

            if (requestCode == RESULT_SPEECH_DESCRIPTION) {

                ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edt_event_details.setText(edt_event_details.getText().toString() + "" + text.get(0));
            }

        }

    }

    private void Initialization() {

        context = CreateEventActivity.this;

        edt_referanceLink = (EditText) findViewById(R.id.edt_referanceLink);

        tv_start_date = (TextView) findViewById(R.id.tv_start_date);
        tv_end_date = (TextView) findViewById(R.id.tv_end_date);
        tv_reminder_date = (TextView) findViewById(R.id.tv_reminder_date);
        txtGradeDivision = (TextView) findViewById(R.id.txtGradeDivision);

        edt_event_title = (EditText) findViewById(R.id.edt_event_title);
        edt_event_details = (EditText) findViewById(R.id.edt_event_details);

        spn_event_type = (Spinner) findViewById(R.id.spn_event_type);

        img_back = (ImageView) findViewById(R.id.img_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);

        img_speack = (ImageView) findViewById(R.id.img_speack);
        img_speack_description = (ImageView) findViewById(R.id.img_speack_description);

        header_text = (TextView) findViewById(R.id.header_text);

        if (!Utility.isTeacher(context)) {

            txtGradeDivision.setVisibility(View.GONE);

        } else {

            txtGradeDivision.setVisibility(View.VISIBLE);
        }

        /*Commented By Krishna : 24-05-2019 - Bind Event Type */

        eventTypeadapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.eventType));
        eventTypeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_event_type.setAdapter(eventTypeadapter);
        spn_event_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    spnTypeStr = "";
                } else {
                    spnTypeStr = parent.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*END*/

        txtGradeDivision.setOnClickListener(this);
        tv_end_date.setOnClickListener(this);
        tv_start_date.setOnClickListener(this);
        tv_reminder_date.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_speack.setOnClickListener(this);
        img_speack_description.setOnClickListener(this);
        ll_save.setOnClickListener(this);


        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        todayDate = (day < 10 ? "0" + day : day)
                + "-"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "-" + myear;


        if (Utility.isNetworkAvailable(context)) {
            getStandarDivisionList();
        } else {
            Utility.showAlertDialog(context, context.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

    }

    public void getStandarDivisionList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(context).getLoginModel().getMemberType()));

        new AsynsTaskClass(context, arrayList, false, CreateEventActivity.this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txtGradeDivision:
                showStanderdPopup();
                break;
            case R.id.tv_end_date:
                date = 1;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.tv_start_date:
                date = 2;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.tv_reminder_date:
                date = 3;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.iv_save:
                Validation();
                break;
            case R.id.ll_save:
                Validation();
                break;
            case R.id.img_back:
                Utility.ISLOADEVENT = false;
                finish();
                break;
            case R.id.img_speack:
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {
                    Utility.showToast("Opps! Your device doesn't support Speech to Text", context);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                    startActivity(browserIntent);
                    overridePendingTransition(0, 0);
                }
                break;
            case R.id.img_speack_description:
                Intent descintent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                descintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(descintent, RESULT_SPEECH_DESCRIPTION);
                } catch (ActivityNotFoundException a) {
                    Utility.showToast("Opps! Your device doesn't support Speech to Text", context);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                    startActivity(browserIntent);
                    overridePendingTransition(0, 0);
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADEVENT = false;
        finish();
    }

    private void Validation() {

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            DateCompare = Utility.CompareDates(df, df.parse(tv_start_date.getText().toString()), df.parse(tv_end_date.getText().toString()));
            ReminderDateComparison = Utility.CompareDates(df, df.parse(tv_end_date.getText().toString()), df.parse(tv_reminder_date.getText().toString()));
            StartDateComparison = Utility.CompareDates(df, df.parse(tv_reminder_date.getText().toString()), df.parse(tv_start_date.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!Utility.isNull(edt_event_title.getText().toString())) {
            edt_event_title.setError(context.getResources().getString(R.string.enternotetitle));
        } else if (spnTypeStr.equalsIgnoreCase("")) {
            Utility.toast(context, context.getResources().getString(R.string.eventTypeValidation));
        } else if (txtGradeDivision.getText().toString().equalsIgnoreCase("")) {
            Utility.toast(context, context.getResources().getString(R.string.eventstddivValidation));
        } else if (!Utility.isNull(tv_start_date.getText().toString())) {
            Utility.toast(context, context.getResources().getString(R.string.enterstartdate));
        } else if (!Utility.isNull(tv_end_date.getText().toString())) {
            Utility.toast(context, context.getResources().getString(R.string.enterenddate));
        } else if (DateCompare) {
            Utility.toast(context, context.getResources().getString(R.string.datevalidation));
        } else if (!Utility.isNull(tv_reminder_date.getText().toString())) {
            Utility.toast(context, context.getResources().getString(R.string.enterreminderdate));
        } else if ((!tv_start_date.getText().toString().trim().isEmpty() && !tv_end_date.getText().toString().isEmpty()) && ReminderDateComparison && !StartDateComparison) {
            Utility.toast(context, context.getResources().getString(R.string.reminderdatevalidation));
        } else {
            addEvent();
        }

    }

    private void addEvent() {

        Utility.getUserModelData(context);
        ContentValues values = new ContentValues();

        values.put(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID());
        values.put(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID());
        values.put(ServiceResource.WALLID, new UserSharedPrefrence(context).getLoginModel().getWallID());
        values.put(ServiceResource.CALENDERDATA_REFERENCELINK, edt_referanceLink.getText().toString());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID());
        values.put(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID());
        values.put(ServiceResource.BATCHID, "");

        if (Utility.isTeacher(context)) {
            values.put(ServiceResource.GRADEIDDIVISIONID, gradeDivisionIdStr);
        } else {
            values.put(ServiceResource.GRADEIDDIVISIONID, "");
        }


        String EventType = "";

        try {
            EventType = spn_event_type.getSelectedItem().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        values.put(ServiceResource.EVENTTITLE, edt_event_title.getText().toString());
        values.put(ServiceResource.EVENTTYPE, EventType);
        values.put(ServiceResource.EVENTDETAILS, edt_event_details.getText().toString().replace("'", ""));
        values.put(ServiceResource.STARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {
            values.put(ServiceResource.ENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy"));
        } else {
            values.put(ServiceResource.ENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        }


        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(context).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_REFERENCELINK, edt_referanceLink.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(context).getLoginModel().getBatchID()));

        if (Utility.isTeacher(context)) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEIDDIVISIONID, gradeDivisionIdStr));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEIDDIVISIONID, ""));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(context).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(context).getLoginModel().getDivisionID()));
        }

        try {
            EventType = spn_event_type.getSelectedItem().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        arrayList.add(new PropertyVo(ServiceResource.EVENTTITLE, edt_event_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.EVENTTYPE, EventType));
        arrayList.add(new PropertyVo(ServiceResource.EVENTDETAILS, edt_event_details.getText().toString().replace("'", "")));
        arrayList.add(new PropertyVo(ServiceResource.STARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));

        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.ENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.ENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        }

        if (tv_reminder_date.getText().toString().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.REMINDERDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.REMINDERDATE, Utility.dateFormate(tv_reminder_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        }
        Log.d("CreateEventRequest", arrayList.toString());

        if (Utility.isNetworkAvailable(context)) {
            new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.ADD_EVENT, ServiceResource.EVENT_URL);
        }

    }


    // show standard  popup which have multiple selection and make string of division id and standard id  with standardId, divisionId # format
    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(context);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);
        final StanderdAdapter adapter = new StanderdAdapter(context, Global.StandardModels, true, true);
        TextView dialogTitle = (TextView) dialogStandard.findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);
        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard.findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard.findViewById(R.id.custome_Dialog_cancle_btn);
        edtSearch.setHint(context.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(context.getResources().getString(R.string.selectgroupmember));
        edtSearch.setVisibility(View.GONE);


        dialogTitle.setText(context.getResources().getString(R.string.selectstandard));
        if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
            lv.setAdapter(adapter);
        }

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
                adapter.getSelectedDivision();
                String txtTempStr = "";
                gradeDivisionIdStr = "";
                for (int i = 0; i < adapter.getSelectedDivision().size(); i++) {
                    if (i == adapter.getSelectedDivision().size() - 1) {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ".";
                    } else {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ",";
                    }
                    String tempStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + "#";

                    gradeDivisionIdStr = gradeDivisionIdStr + tempStr;

                }

                txtGradeDivision.setText(txtTempStr);

            }

        });

        btnCancle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }

        });

        dialogStandard.show();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog datePickerDialog;

        switch (id) {

            case DATE_DIALOG_ID:

                datePickerDialog = new DatePickerDialog(this, datePickerListener,
                        myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;

        }

        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (date == 1) {
                tv_end_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            } else if (date == 2) {
                tv_start_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            } else if (date == 3) {
                tv_reminder_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            }

            date = 0;

        }

    };

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, methodName, context);
            parsestddivisionlist(result);

        }

        if (ServiceResource.ADD_EVENT.equalsIgnoreCase(methodName)) {

            Utility.ISLOADEVENT = true;
            Utility.sendPushNotification(context);
            Utility.showToast(getResources().getString(R.string.eventsuccessmessage), context);
            finish();

        }

    }

    private void parsestddivisionlist(String result) {

        JSONArray hJsonArray;

        try {

            Global.holidaysModel = new HolidaysModel();

            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);
                Global.StandardModels = new ArrayList<StandardModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    boolean isAlreadyInsert = false;
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StandardModel model = new StandardModel();
                    model.setStandardName(hJsonObject
                            .getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject
                            .getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject
                            .getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject
                            .getString(ServiceResource.DIVISION));
                    model.setSubjectId(hJsonObject
                            .getString(ServiceResource.STANDARD_SUBJECTID));
                    model.setSubjectName(hJsonObject
                            .getString(ServiceResource.SUBJECT));
                    if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
                        for (int j = 0; j < Global.StandardModels.size(); j++) {
                            if (Global.StandardModels.get(j).getDivisionId().equalsIgnoreCase(model.getDivisionId())
                                    && Global.StandardModels.get(j).getStandrdId().equalsIgnoreCase(model.getStandrdId())) {
                                isAlreadyInsert = true;
                            }
                        }
                    }

                    if (!isAlreadyInsert) {
                        Global.StandardModels.add(model);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
