package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@Entity(tableName = "ProjectList")
public class ProjectListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "projectName")
    @SerializedName("projectName")
    public String projectName;

    @ColumnInfo(name = "projectdetail")
    @SerializedName("projectdetail")
    public String projectdetail;

    @ColumnInfo(name = "projectTime")
    @SerializedName("projectTime")
    public String projectTime;


    @ColumnInfo(name = "WallID")
    @SerializedName("WallID")
    public String WallID;


    @ColumnInfo(name = "GroupID")
    @SerializedName("GroupID")
    public String GroupID;


    @ColumnInfo(name = "ProjectScope")
    @SerializedName("ProjectScope")
    public String ProjectScope;


    @ColumnInfo(name = "ProjectDefination")
    @SerializedName("ProjectDefination")
    public String ProjectDefination;


    @ColumnInfo(name = "ProfilePicture")
    @SerializedName("ProfilePicture")
    public String ProfilePicture;


    @ColumnInfo(name = "ProjectTitle")
    @SerializedName("ProjectTitle")
    public String ProjectTitle;


    @ColumnInfo(name = "UpdateOn")
    @SerializedName("UpdateOn")
    public String UpdateOn;


    @ColumnInfo(name = "UserName")
    @SerializedName("UserName")
    public String UserName;


    @ColumnInfo(name = "CreateBy")
    @SerializedName("CreateBy")
    public String CreateBy;


    @ColumnInfo(name = "CreateOn")
    @SerializedName("CreateOn")
    public String CreateOn;


    @ColumnInfo(name = "EndDate")
    @SerializedName("EndDate")
    public String EndDate;


    @ColumnInfo(name = "StartDate")
    @SerializedName("StartDate")
    public String StartDate;

    @ColumnInfo(name = "ProjectID")
    @SerializedName("ProjectID")
    public String ProjectID;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getProjectDefination() {
        return ProjectDefination;
    }

    public void setProjectDefination(String projectDefination) {
        ProjectDefination = projectDefination;
    }

    public String getProjectScope() {
        return ProjectScope;
    }

    public void setProjectScope(String projectScope) {
        ProjectScope = projectScope;
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public String getProjectID() {
        return ProjectID;
    }

    public void setProjectID(String projectID) {
        ProjectID = projectID;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getCreateOn() {
        return CreateOn;
    }

    public void setCreateOn(String createOn) {
        CreateOn = createOn;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUpdateOn() {
        return UpdateOn;
    }

    public void setUpdateOn(String updateOn) {
        UpdateOn = updateOn;
    }

    public String getProjectTitle() {
        return ProjectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        ProjectTitle = projectTitle;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }


    boolean isShow = false;

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean isShow) {
        this.isShow = isShow;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectdetail() {
        return projectdetail;
    }

    public void setProjectdetail(String projectdetail) {
        this.projectdetail = projectdetail;
    }

    public String getProjectTime() {
        return projectTime;
    }

    public void setProjectTime(String projectTime) {
        this.projectTime = projectTime;
    }

    @Override
    public String toString() {
        return "ProjectListModel{" +
                "id=" + id +
                ", projectName='" + projectName + '\'' +
                ", projectdetail='" + projectdetail + '\'' +
                ", projectTime='" + projectTime + '\'' +
                ", WallID='" + WallID + '\'' +
                ", GroupID='" + GroupID + '\'' +
                ", ProjectScope='" + ProjectScope + '\'' +
                ", ProjectDefination='" + ProjectDefination + '\'' +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", ProjectTitle='" + ProjectTitle + '\'' +
                ", UpdateOn='" + UpdateOn + '\'' +
                ", UserName='" + UserName + '\'' +
                ", CreateBy='" + CreateBy + '\'' +
                ", CreateOn='" + CreateOn + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", StartDate='" + StartDate + '\'' +
                ", ProjectID='" + ProjectID + '\'' +
                ", isShow=" + isShow +
                '}';
    }
}
