package com.edusunsoft.erp.orataro.customeview;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

public class CTextView  extends androidx.appcompat.widget.AppCompatTextView {

	public CTextView(Context context) {
		super(context);
	}

	public CTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		text = Html.fromHtml(String.valueOf(text));
		super.setText(text, type);
	}
}
