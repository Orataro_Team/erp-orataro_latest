package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.SeenByAdapter;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class SeenByActivity extends Activity implements OnClickListener, ResponseWebServices {

    ImageView leftimg, rightimg;
    TextView txtHeader;
    //	EditText edtHeader;
    ListView lvSeenBy;
    SeenByAdapter seenbyadapter;
    ImageView img_back;
    ArrayList<PersonModel> personList;
    int count = 1;
    EditText edtTeacherName;
    boolean isFriend;
    private Context mContext = SeenByActivity.this;
    LinearLayout searchlayout;
    TextView txt_nodatafound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seen_by);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
        leftimg = (ImageView) findViewById(R.id.img_home);
        rightimg = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        //		edtHeader = (EditText) findViewById(R.id.edtHeader);
        lvSeenBy = (ListView) findViewById(R.id.lvSeenBy);
        searchlayout = (LinearLayout) findViewById(R.id.searchlayout);
        img_back = (ImageView) findViewById(R.id.img_back);
        if (getIntent() != null) {
            String countStr = getIntent().getStringExtra("count");
            count = Integer.valueOf(countStr);
            isFriend = getIntent().getBooleanExtra("friend", false);
        }
        try {
            if (isFriend) {
                txtHeader.setText(getResources().getString(R.string.friends) + " (" + Utility.GetFirstName(mContext) + ")");
                searchlayout.setVisibility(View.VISIBLE);
            } else {
                txtHeader.setText(getResources().getString(R.string.Peoplewhosawthis) + " (" + Utility.GetFirstName(mContext) + ")");
                //HomeWorkFragmentActivity.txt_header.setText("People who saw this");
            }

        } catch (Exception e) {

        }
        if (Utility.isNetworkAvailable(mContext)) {
            friendList();
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
        //addPersonList();


        img_back.setOnClickListener(this);
        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                String text = edtTeacherName.getText().toString()
                        .toLowerCase(Locale.getDefault());
                seenbyadapter.filter(text);
            }
        });
    }

    private void addPersonList() {
        personList = new ArrayList<PersonModel>();

        PersonModel model;
        model = new PersonModel();
        model.setPersonName("Rahul Patel");
        model.setProfileImg("dp1.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Ram Jain");
        model.setProfileImg("dp2.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Ram Jain");
        model.setProfileImg("dp3.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Janak Shah");
        model.setProfileImg("dp4.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Prakash Patel");
        model.setProfileImg("dp5.png");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Akshay Gandhi");
        model.setProfileImg("dp6.png");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Rahul Patil");
        model.setProfileImg("dp7.png");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Paresh Bhatt");
        model.setProfileImg("dp3.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Sunil Jungi");
        model.setProfileImg("dp6.jpg");
        personList.add(model);

        model = new PersonModel();
        model.setPersonName("Meera Rai");
        model.setProfileImg("dp4.jpg");
        personList.add(model);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.img_back:

                finish();

                break;

            default:
                break;
        }
    }


    public void friendList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.FRIENDS_METHODNAME, ServiceResource.FRIENDS_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.FRIENDS_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.FriendsList = new ArrayList<PersonModel>();


                jsonObj = new JSONArray(result);
                //JSONArray detailArrray= jsonObj.getJSONArray("");


                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    PersonModel model = new PersonModel();
                    model.setPersonName(innerObj.getString(ServiceResource.FRIENDS_FULLNAME));
                    model.setProfileImg(innerObj.getString(ServiceResource.FRIENDS_PROFILEPIC));
                    model.setFriendId(innerObj.getString(ServiceResource.FRIENDS_FRIENDID));
                    model.setFriendListID(innerObj.getString(ServiceResource.FRIENDS_FRIENDLIST));
                    model.setWallID(innerObj.getString(ServiceResource.FRIENDS_WALLID));
                    Global.FriendsList.add(model);

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (Global.FriendsList != null && Global.FriendsList.size() > 0) {
                seenbyadapter = new SeenByAdapter(SeenByActivity.this, Global.FriendsList, count);
                lvSeenBy.setAdapter(seenbyadapter);
                txt_nodatafound.setVisibility(View.GONE);
            } else {
                searchlayout.setVisibility(View.GONE);
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoFriendsAvailable));
            }

        }
    }


}
