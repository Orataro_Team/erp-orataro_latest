package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddNotesActivity;
import com.edusunsoft.erp.orataro.activities.CheckedStudentListActivity;
import com.edusunsoft.erp.orataro.activities.ViewNoticeDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GenerallWallDataDao;
import com.edusunsoft.erp.orataro.database.NoticeListDataDao;
import com.edusunsoft.erp.orataro.database.NoticeListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NotesListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private TextView homeworkListSubject;
    private ImageView list_image;
    private TextView txt_month;
    private TextView txt_date, txt_nt_date, txt_nt_subject, txt_nt_note, txt_nt_title, txt_teacher_name;
    private LinearLayout ll_date;
    private LinearLayout ll_view, ll_date_img;
    private Context mContext;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private static final int ID_CHECKED = 7;
    private RefreshListner listner;
    private List<NoticeListModel> noticeModels = new ArrayList<>();
    private ArrayList<NoticeListModel> copyList = new ArrayList<>();
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    NoticeListDataDao noticeListDataDao;
    GenerallWallDataDao generallWallDataDao;
    public String DELETE_ID = "";
    public static int pos;
    public static boolean isFinish;

    public NotesListAdapter(Context context, List<NoticeListModel> noticeModels, RefreshListner listner) {
        mContext = context;
        this.noticeModels = noticeModels;
        this.listner = listner;
        copyList = new ArrayList<NoticeListModel>();
        copyList.addAll(noticeModels);
    }

    @Override
    public int getCount() {
        return noticeModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.notice_listraw, parent, false);
        LinearLayout ll_finish = (LinearLayout) convertView.findViewById(R.id.ll_finish);
        CheckBox chk_finish = (CheckBox) convertView.findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) convertView.findViewById(R.id.txtisfinish);
        txtisfinish.setText(mContext.getResources().getString(R.string.Checked));
        if (Utility.isTeacher(mContext)) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }
        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);
        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.NOTE).getIsDelete() || Utility.ReadWriteSetting(ServiceResource.NOTE).getIsEdit()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.INVISIBLE);
            }
        } else {
            ll_editdelete.setVisibility(View.GONE);
        }

        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        txt_nt_subject = (TextView) convertView.findViewById(R.id.txt_nt_subject);
        txt_nt_date = (TextView) convertView.findViewById(R.id.txt_nt_date);
        txt_nt_note = (TextView) convertView.findViewById(R.id.txt_nt_note);
        txt_nt_title = (TextView) convertView.findViewById(R.id.txt_nt_title);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        txt_teacher_name = (TextView) convertView.findViewById(R.id.txt_teacher_name);
        if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            txt_teacher_name.setVisibility(View.GONE);
        }

        int colorPos = position % colors.length;
        convertView.setBackgroundColor(colors[colorPos]);
        int colorPos1 = position % colors_list.length;
        ll_view.setBackgroundColor(colors_list[colorPos1]);
        txt_date.setText(noticeModels.get(position).getYear());
        txt_nt_subject.setText(noticeModels.get(position).getSubjectName());
        txt_nt_title.setText(noticeModels.get(position).getNoteTitle());
        txt_teacher_name.setText(noticeModels.get(position).getUserName());

        if (noticeModels.get(position).isVisible()) {

            ll_date.setVisibility(View.VISIBLE);

        }

        noticeListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).noticeListDataDao();

        final QuickAction quickActionForEditOrDelete;
        ActionItem actionEdit, actionDelete, actionChecked;
        actionEdit = new ActionItem(ID_EDIT, "Edit", mContext.getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, "Delete", mContext.getResources().getDrawable(R.drawable.delete));
        actionChecked = new ActionItem(ID_CHECKED, "Checked", mContext.getResources().getDrawable(R.drawable.check_icon));
        quickActionForEditOrDelete = new QuickAction(mContext, QuickAction.VERTICAL);

        quickActionForEditOrDelete.addActionItem(actionEdit);
        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.addActionItem(actionChecked);

        quickActionForEditOrDelete
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int _pos, int actionId) {
                        ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                        if (actionId == ID_EDIT) {

                            int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                            Intent i = new Intent(mContext, AddNotesActivity.class);
                            i.putExtra("isFrom", ServiceResource.NOTE_FLAG_TEACHER);
                            i.putExtra("isEdit", true);
                            i.putExtra("model", noticeModels.get(editPos));
                            mContext.startActivity(i);

                        } else if (actionId == ID_DELETE) {
                            final int deletepos = Integer.valueOf(((View) quickActionForEditOrDelete.getAnchor()).getTag().toString());

                            Utility.deleteDialog(mContext, mContext.getResources().getString(R.string.Note),
                                    noticeModels.get(deletepos).getNoteTitle(), new Popup() {

                                        @Override
                                        public void deleteYes() {
                                            DELETE_ID = noticeModels.get(deletepos).getNotesID();
                                            deleteNote(noticeModels.get(deletepos).getNotesID());
                                        }

                                        @Override
                                        public void deleteNo() {
                                        }

                                    });

                        } else if (actionId == ID_CHECKED) {

                            /*commented By Krishna : 17-05-2019 Redirect to CheckHomework List of Student*/

                            Intent studentlistintent = new Intent(mContext, CheckedStudentListActivity.class);
                            studentlistintent.putExtra("FROMSSTR", "Note");
                            studentlistintent.putExtra("AssociationID", noticeModels.get(position).getNotesID());

                            if (noticeModels.get(position).getGradeID().equalsIgnoreCase(null) || noticeModels.get(position).getGradeID().equalsIgnoreCase("null")) {
                                studentlistintent.putExtra("GradeID", "");
                            } else {
                                studentlistintent.putExtra("GradeID", noticeModels.get(position).getGradeID());
                            }
                            if (noticeModels.get(position).getDivisionID().equalsIgnoreCase(null) || noticeModels.get(position).getDivisionID().equalsIgnoreCase("null")) {
                                studentlistintent.putExtra("DivisionID", "");
                            } else {
                                studentlistintent.putExtra("DivisionID", noticeModels.get(position).getDivisionID());
                            }
                            studentlistintent.putExtra("AssociationType", "Note");
                            mContext.startActivity(studentlistintent);

                            /*END*/

                        }


                    }


                });


        quickActionForEditOrDelete
                .setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {

                    }

                });

        ll_editdelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                quickActionForEditOrDelete.show(v);

            }

        });

        if (noticeModels.get(position).isCheck()) {
            chk_finish.setChecked(true);
        } else {
            chk_finish.setChecked(false);
        }

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pos = position;
                isFinish = isChecked;
                noteListIsApprove(noticeModels.get(position).getNotesID(), isChecked);

            }

        });


        txt_nt_date.setText(noticeModels.get(position).getNoteDate());
        if (Utility.isNull(noticeModels.get(position).getNoteDetails())) {

            if (noticeModels.get(position).getNoteDetails().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = noticeModels.get(position).getNoteDetails()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
                        intent.putExtra("NotesModel", noticeModels.get(position));
                        mContext.startActivity(intent);
                    }
                };
                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                txt_nt_note.setMovementMethod(LinkMovementMethod.getInstance());
                txt_nt_note.setText(text, BufferType.SPANNABLE);
            } else {
                txt_nt_note
                        .setText(noticeModels.get(position).getNoteDetails());
            }

        }

        if (noticeModels.get(position).isRead()) {
            list_image.setImageResource(R.drawable.double_tick_sky_blue);
        }

        txt_nt_note.setText(noticeModels.get(position).getNoteDetails());
        if (noticeModels.get(position).getMonth() != null){
            Log.d("getMonth", noticeModels.get(position).getMonth());
            txt_month.setText(noticeModels.get(position).getMonth().toUpperCase());
        }

        ll_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            }

        });

        txt_nt_note.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
                intent.putExtra("NotesModel", noticeModels.get(position));
                mContext.startActivity(intent);
            }
        });

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!noticeModels.get(position).isRead()) {
                    if (Utility.isNetworkAvailable(mContext)) {
                        pos = position;
                        NotesListIsRead(noticeModels.get(position).getNotesID());
                    } else {
                        Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                    }
                } else {
                    Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
                    intent.putExtra("NotesModel", noticeModels.get(position));
                    mContext.startActivity(intent);
                }

            }

        });

        return convertView;

    }

    public void NotifyNotesSingleItem(String noteid, boolean checked) {

        for (int i = 0; i < noticeModels.size(); i++) {

            if (noticeModels.get(i).getNotesID().equalsIgnoreCase(noteid)) {
                noticeModels.get(pos).setCheck(checked);
                noticeModels.set(pos, noticeModels.get(pos));
                notifyDataSetChanged();
            }
        }
    }

    private void NotesListIsRead(String notesID) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, notesID));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Note"));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));

        Log.d("getapprveRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);
    }

    public void noteListIsApprove(String assosiationId, boolean isApprove) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Note"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void deleteNote(String id) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.PRIMARYID, id));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Note"));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ASSOCIATIONDELETE_METHODNAME, ServiceResource.CHECK_URL);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        noticeModels.clear();
        if (charText.length() == 0) {
            noticeModels.addAll(copyList);
        } else {
            for (NoticeListModel vo : copyList) {
                if (vo.getNoteTitle().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getNoteDetails().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getStrActionStartDate().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getUserName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    noticeModels.add(vo);
                }
            }
        }

        this.notifyDataSetChanged();

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.APPROVE_METHODNAME.equalsIgnoreCase(methodName)) {
            // Commented By Krishna : 18-09-2020 -12:22 - Added to Not refresh after back from detail activity.
            noticeModels.get(pos).setCheck(isFinish);
            noticeModels.set(pos, noticeModels.get(pos));
            notifyDataSetChanged();
            /*END*/
//            listner.refresh(methodName);
        } else if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            noticeModels.get(pos).setRead(true);
            noticeModels.set(pos, noticeModels.get(pos));
            notifyDataSetChanged();
            noticeListDataDao.updateIsReadyByNoticeID(noticeModels.get(pos).getNotesID(), true);
            Intent intent = new Intent(mContext, ViewNoticeDetailsActivity.class);
            intent.putExtra("NotesModel", noticeModels.get(pos));
            mContext.startActivity(intent);
        } else if (ServiceResource.ASSOCIATIONDELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            noticeListDataDao.deleteNotesItemByNoticeID(DELETE_ID);
            generallWallDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).generawallDataDao();
            generallWallDataDao.deleteWallItemByAssID(DELETE_ID);
            listner.refresh(methodName);
            Utility.showToast(mContext.getResources().getString(R.string.notedelete), mContext);
        }

    }

}
