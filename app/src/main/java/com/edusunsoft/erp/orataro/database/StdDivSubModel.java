package com.edusunsoft.erp.orataro.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "StdDivSub")
public class StdDivSubModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "standrdId")
    @SerializedName("standrdId")
    public String standrdId;

    @ColumnInfo(name = "standardName")
    @SerializedName("standardName")
    public String standardName;

    @ColumnInfo(name = "divisionId")
    @SerializedName("divisionId")
    public String divisionId;

    @ColumnInfo(name = "divisionName")
    @SerializedName("divisionName")
    public String divisionName;

    @ColumnInfo(name = "subjectId")
    @SerializedName("subjectId")
    public String subjectId;

    @ColumnInfo(name = "subjectName")
    @SerializedName("subjectName")
    public String subjectName;

    @ColumnInfo(name = "countleave")
    @SerializedName("countleave")
    public String countleave;

    @ColumnInfo(name = "isChecked")
    @SerializedName("isChecked")
    public boolean isChecked;

    @ColumnInfo(name = "isLeave")
    @SerializedName("isLeave")
    public String isLeave;


    public StdDivSubModel() {
    }

    public String getIsLeave() {
        return isLeave;
    }

    public void setIsLeave(String isLeave) {
        this.isLeave = isLeave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStandrdId() {
        return standrdId;
    }

    public void setStandrdId(String standrdId) {
        this.standrdId = standrdId;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCountleave() {
        return countleave;
    }

    public void setCountleave(String countleave) {
        this.countleave = countleave;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString() {
        return "StdDivSubModel{" +
                "id=" + id +
                ", standrdId='" + standrdId + '\'' +
                ", standardName='" + standardName + '\'' +
                ", divisionId='" + divisionId + '\'' +
                ", divisionName='" + divisionName + '\'' +
                ", subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", countleave='" + countleave + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
