package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.DaynamicWallAdapter;
import com.edusunsoft.erp.orataro.model.DynamicWallModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListWallActivity extends Activity implements ResponseWebServices {
    ImageView imgLeftHeader, imgRightHeader;
    TextView txt_header;
    String from;
    ListView lvList;
    Context mContext = ListWallActivity.this;
    String wallType;
    TextView txt_nodatafound;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_wall);
        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        txt_header = (TextView) findViewById(R.id.header_text);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.blue);
        lvList = (ListView) findViewById(R.id.lvList);

        if (getIntent() != null) {
            from = getIntent().getStringExtra("isFrom");
        }

        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);
        imgLeftHeader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utility.RedirectToDashboard(ListWallActivity.this);


            }

        });

        try {

            if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                txt_header.setText(getResources().getString(R.string.Group) + " (" + Utility.GetFirstName(mContext) + ")");
                wallType = "Group";
                //			groupWall();

            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                txt_header.setText(getResources().getString(R.string.Project) + " (" + Utility.GetFirstName(mContext) + ")");
                wallType = "Project";
            } else {

                try {
                    if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                        txt_header.setText(getResources().getString(R.string.Subjects) + " (" + Utility.GetFirstName(mContext) + ")");
                        wallType = "Subject";
                    } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                        txt_header.setText(getResources().getString(R.string.Standard) + " (" + Utility.GetFirstName(mContext) + ")");
                        wallType = "Grade";
                    } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                        txt_header.setText(getResources().getString(R.string.Division) + " (" + Utility.GetFirstName(mContext) + ")");
                        wallType = "Division";
                    }

                } catch (Exception e) {
                }
            }
        } catch (Exception e) {

        }
        if (Utility.isNetworkAvailable(mContext)) {
            DynamicMenuList();
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    DynamicMenuList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        lvList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                itemClick(position, false);

            }

        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.RedirectToDashboard(ListWallActivity.this);
    }

    @Override
    protected void onResume() {

        // TODO Auto-generated method stub
        super.onResume();

    }

    public void DynamicMenuList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID,
                    null));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID,
                    null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID,
                    new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        }

        arrayList.add(new PropertyVo(
                ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        Log.d("getgroupRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).
                execute(ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME,
                        ServiceResource.LOGIN_URL);
    }


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME, mContext);
            parsedaynamic(result);

        }

    }


    public void itemClick(int position, boolean isFinish) {

        Intent i;

        if (Global.DynamicWallModels != null && Global.DynamicWallModels.size() > 0) {

            new UserSharedPrefrence(mContext).setCURRENTWALLID(Global.DynamicWallModels
                    .get(position).getWallID());

            if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {

                i = new Intent(ListWallActivity.this, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                i.putExtra("title", Global.DynamicWallModels
                        .get(position).getWallName());
                ServiceResource.WALLTITLE = Global.DynamicWallModels.get(position).getWallName();
                startActivity(i);

            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {

                i = new Intent(ListWallActivity.this, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.STANDARDWALL);
                i.putExtra("title", Global.DynamicWallModels
                        .get(position).getWallName());
                ServiceResource.WALLTITLE = Global.DynamicWallModels.get(position).getWallName();
                startActivity(i);

            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {

                i = new Intent(ListWallActivity.this, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                i.putExtra("title", Global.DynamicWallModels
                        .get(position).getWallName());
                ServiceResource.WALLTITLE = Global.DynamicWallModels.get(position).getWallName();
                startActivity(i);

            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {

                ServiceResource.FROM_SELECTED_WALL = "FromGroupList";
                ServiceResource.SELECTED_WALL_TITLE = Global.DynamicWallModels
                        .get(position).getWallName();
                i = new Intent(ListWallActivity.this, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.GROUPWALL);
                i.putExtra("title", Global.DynamicWallModels
                        .get(position).getWallName());
                Log.d("getListWallTitle", Global.DynamicWallModels
                        .get(position).getWallName());
                ServiceResource.WALLTITLE = Global.DynamicWallModels.get(position).getWallName();
                startActivity(i);

            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {

                ServiceResource.FROM_SELECTED_WALL = "FromProjectList";
                ServiceResource.SELECTED_WALL_TITLE = Global.DynamicWallModels
                        .get(position).getWallName();
                i = new Intent(ListWallActivity.this, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.PROJECTWALL);
                i.putExtra("title", Global.DynamicWallModels
                        .get(position).getWallName());
                ServiceResource.WALLTITLE = Global.DynamicWallModels.get(position).getWallName();
                startActivity(i);

            }

        }

    }


    public void parsedaynamic(String result) {

        Global.DynamicWallModels = new ArrayList<DynamicWallModel>();
        ArrayList<DynamicWallModel> templist = new ArrayList<DynamicWallModel>();

        try {

            Log.e("UpdateProfile", result);
            JSONObject jsonobj = new JSONObject(result);
            JSONArray table = jsonobj
                    .getJSONArray(ServiceResource.GETUSERDYNAMICMENUDATA_TABLE);

            for (int i = 0; i < table.length(); i++) {

                JSONObject obj = table.getJSONObject(i);
                DynamicWallModel model = new DynamicWallModel();
                model.setWallID(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLID));
                model.setWallName(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLNAME));
                model.setWallImage(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLIMAGE));
                model.setAssociationType(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_ASSOCIATIONTYPE));
                templist.add(model);

            }

        } catch (Exception e) {

            // error = "Error Occures";

        }

        for (int i = 0; i < templist.size(); i++) {

            if (templist.get(i).getAssociationType()
                    .equalsIgnoreCase(wallType)) {

                Global.DynamicWallModels.add(templist.get(i));

            }

        }

        if (Global.DynamicWallModels != null
                && Global.DynamicWallModels.size() > 0) {


            DaynamicWallAdapter adapter = new DaynamicWallAdapter(
                    mContext, Global.DynamicWallModels);
            lvList.setAdapter(adapter);

            /*Commented By Krishna : 11-07-2019 - Remove condition of Redirection to direct project wall if Project list is Only 1*/

//            if (Global.DynamicWallModels.size() == 1) {
//
//                itemClick(0, true);
//
//            } else {
//
//                DaynamicWallAdapter adapter = new DaynamicWallAdapter(
//                        mContext, Global.DynamicWallModels);
//                lvList.setAdapter(adapter);
//
//            }

            /*END*/

            txt_nodatafound.setVisibility(View.INVISIBLE);

        } else {

            txt_nodatafound.setVisibility(View.VISIBLE);
            if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoSubjectAvailable));
            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoStandardAvailable));
            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoDivisionDataAvailable));
            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoGroupAvailable));
            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoProjectAvailable));
            }
        }
    }
}
