package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.ListSelectionActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewHomWorkDetailsActivity;
import com.edusunsoft.erp.orataro.adapter.DateListAdapter;
import com.edusunsoft.erp.orataro.adapter.HomeWorkListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.HomeWorkListModel;
import com.edusunsoft.erp.orataro.database.HomeworkListDataDao;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class HomeWorkListFragment extends Fragment implements OnItemClickListener, OnClickListener, ResponseWebServices, RefreshListner {

    private ListView lst_date;
    private PullAndLoadListView homeworkList;
    public static HomeWorkListAdapter adapter;
    private Context mContext;
    private FrameLayout fl_main;
    private DateListAdapter date_list_Adapter;
    private ArrayList<HomeWorkModel> homeWorkModels;
    private int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1, R.drawable.teacher_2};
    private EditText edtTeacherName;
    private TextView txt_nodatafound;
    private LinearLayout ll_add_new;
    private boolean isFirst;
    private View header;

    // linearlayout for search
    LinearLayout searchlayout;

    // variable declaration for homeworklist from ofline database
    HomeworkListDataDao homeworkListDataDao;
    private List<HomeWorkListModel> HomeworkList = new ArrayList<>();
    HomeWorkListModel homeworkmodel = new HomeWorkListModel();
    public String ISDataExist = "";
    /*END*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View convertView = inflater.inflate(R.layout.homeworklist_fragment, container, false);
        mContext = getActivity();
        edtTeacherName = (EditText) convertView.findViewById(R.id.edtsearchStudent);
        homeworkList = (PullAndLoadListView) convertView.findViewById(R.id.homeworklist);
        ll_add_new = (LinearLayout) convertView.findViewById(R.id.ll_add_new);
        fl_main = (FrameLayout) convertView.findViewById(R.id.fl_main);
        txt_nodatafound = (TextView) convertView.findViewById(R.id.txt_nodatafound);

        searchlayout = (LinearLayout) convertView.findViewById(R.id.searchlayout);

        homeworkListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).homeworkListDataDao();

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Homework) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        isFirst = true;
        Utility.ISLOADHOMEWORK = false;

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.HOMEWORKSETTING).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.GONE);
        }

        // Display HomeworkList from offline as well as online
        try {
            DisplayHomeworkList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*END*/

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, ListSelectionActivity.class);
                i.putExtra("isFrom", ServiceResource.HOMEWORK_FLAG);
                startActivity(i);

            }

        });

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());

                if (adapter != null) {

                    adapter.filter(text);

                }

            }

        });

        homeworkList.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {

                // Display HomeworkList from offline as well as online
                try {
                    DisplayHomeworkList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/
            }

        });

        return convertView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {

        if (Utility.ISLOADHOMEWORK) {
            // Display HomeworkList from offline as well as online
            try {
                DisplayHomeworkList();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        /*END*/
        } else {
        }
        super.onResume();
    }

    private void DisplayHomeworkList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            HomeworkList = homeworkListDataDao.getHomeworkListList();
            if (HomeworkList.isEmpty() || HomeworkList.size() == 0 || HomeworkList == null) {
                ISDataExist = "true";
                HomeWorkList(true);
            } else {
                //set Adapter
                setHomeworklistAdapter(HomeworkList);
                /*END*/
                HomeWorkList(false);
            }
        } else {
            HomeworkList = homeworkListDataDao.getHomeworkListList();
            if (HomeworkList != null) {
                setHomeworklistAdapter(HomeworkList);
            }
        }
    }

    private void setHomeworklistAdapter(List<HomeWorkListModel> HomeworkList) {

        if (HomeworkList != null && HomeworkList.size() > 0) {

            searchlayout.setVisibility(View.VISIBLE);

            for (int i = 0; i < HomeworkList.size(); i++) {

                if (i != 0) {

                    String temp = HomeworkList.get(i - 1)
                            .getMonth();

                    Log.v("listmonth", temp);
                    Log.v("listmonth12", HomeworkList.get(i)
                            .getMonth());

                    if (temp.equalsIgnoreCase(HomeworkList.get(i)
                            .getMonth())) {
                        if (HomeworkList.get(i).getYear().equalsIgnoreCase(HomeworkList.get(i - 1).getYear())) {
                            HomeworkList.get(i).setVisibleMonth(false);
                        } else {
                            HomeworkList.get(i).setVisibleMonth(true);
                        }

                    } else {

                        HomeworkList.get(i).setVisibleMonth(true);

                    }

                } else {

                    HomeworkList.get(i).setVisibleMonth(true);

                }

            }

            adapter = new HomeWorkListAdapter(mContext, HomeworkList, this);
            homeworkList.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            homeworkList.setVisibility(View.VISIBLE);

        } else {
            homeworkList.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.HomeWorkListNotAvailable));

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(mContext, ViewHomWorkDetailsActivity.class);
        intent.putExtra("Subname", Global.homeworkModels.get(position).getTitle());
        intent.putExtra("Sub_details", Global.homeworkModels.get(position).getHomeWorksDetails());
        intent.putExtra("Teacher_name", homeWorkModels.get(position).getTecherName());
        intent.putExtra("Teacher_img", homeWorkModels.get(position).getTeacherProfilePicture());
        startActivity(intent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home:
                break;

            default:
                break;
        }
    }

    public class SortedDate implements Comparator<HomeWorkModel> {
        @Override
        public int compare(HomeWorkModel o1, HomeWorkModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }


    public void HomeWorkList(boolean isViewPopup) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        }
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        Log.d("getGHomewrokparams", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.HOMEWORK_METHODNAME, ServiceResource.HOMEWORK_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.HOMEWORK_METHODNAME.equalsIgnoreCase(methodName)) {
            parseHomework(result, ServiceResource.UPDATE);
        }

    }

    @Override
    public void refresh(String methodName) {

        if (Utility.isNetworkAvailable(mContext)) {

            HomeWorkList(false);

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    public void parseHomework(String result, String update) {

        Log.d("homeworkresultoffline", result);

        JSONArray jsonObj;
        homeworkListDataDao.deleteHomeworkListItem();
        if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
            homeworkList.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.HomeWorkListNotAvailable));
        } else {
            try {
                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    // parse homework list data
                    ParseHomeworkList(innerObj);
                    /*END*/
                }

                if (homeworkListDataDao.getHomeworkListList().size() > 0) {
                    //set Adapter
                    setHomeworklistAdapter(homeworkListDataDao.getHomeworkListList());
                    /*END*/
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void ParseHomeworkList(JSONObject innerObj) {

        String date = null;

        try {
            date = Utility.getDate(Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.HOMEWORK_DATEOFHOMEWORLK), "MM/dd/yyyy"), "EEEE/dd/MMM/yyyy/");
            Log.v("date And Time", Utility.getDate(
                    Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.HOMEWORK_DATEOFHOMEWORLK), "MM/dd/yyyy"), "EEEE/dd/MMM/yyyy/ hh:mm:ss"));
            String[] dateArray = date.split("/");

            if (dateArray != null && dateArray.length > 0) {

                homeworkmodel.setDay(dateArray[1] + " " + dateArray[0].substring(0, 3));
                homeworkmodel.setMonth(dateArray[2]);
                homeworkmodel.setYear(dateArray[3]);
                homeworkmodel.setDateOfFinish(dateArray[1] + " " + dateArray[2] + " " + dateArray[3]);

            }

            homeworkmodel.setMilliSecond(Utility.dateToMilliSeconds(
                    innerObj.getString(ServiceResource.HOMEWORK_DATEOFHOMEWORLK).replace("/Date(", "").replace(")/", ""),
                    "MM/dd/yyyy") + "");

            homeworkmodel.setGradeID(innerObj.getString(ServiceResource.HOMEWORK_GRADEID));
            homeworkmodel.setDivisionID(innerObj.getString(ServiceResource.HOMEWORK_DIVISIONID));
            homeworkmodel.setSubjectID(innerObj.getString(ServiceResource.HOMEWORK_SUBJECTID));
            homeworkmodel.setDateOfFinish(innerObj.getString(ServiceResource.HOMEWORK_DATEOFFINISH));
            homeworkmodel.setAssignmentID(innerObj.getString(ServiceResource.HOMEWORK_ASSINMENTID));
            homeworkmodel.setSubjectName(innerObj.getString(ServiceResource.HOMEWORK_SUBJECTNAME));
            homeworkmodel.setAssignmentID(innerObj.getString(ServiceResource.HOMEWORK_ASSINMENTID));
            homeworkmodel.setDateOfHomeWork(innerObj.getString(ServiceResource.HOMEWORK_DATEOFHOMEWORLK));
            homeworkmodel.setReferenceLink(innerObj.getString(ServiceResource.HOMEWORK_REFERENCELINK));
            homeworkmodel.setTitle(innerObj.getString(ServiceResource.HOMEWORK_TITLE));
            homeworkmodel.setTecherName(innerObj.getString(ServiceResource.HOMEWORK_TEACHERNAME));
            homeworkmodel.setTeacherProfilePicture(innerObj.getString(ServiceResource.HOMEWORK_TEACHERPROFILEPIC));
            homeworkmodel.setHomeWorksDetails(innerObj.getString(ServiceResource.HOMEWORK_HOMEWORKDETAILS));
            homeworkmodel.setIsWorkFinished(Boolean.valueOf(innerObj.getString(ServiceResource.HOMEWORK_ISWORKFINISH)));
            Log.d("getimageurl", ServiceResource.BASE_IMG_URL + ServiceResource.DATAFILE + innerObj.getString(ServiceResource.PHOTOPARSE));
            homeworkmodel.setImgUrl(ServiceResource.BASE_IMG_URL + ServiceResource.DATAFILE + innerObj.getString(ServiceResource.PHOTOPARSE));
            homeworkmodel.setGradeName(innerObj.getString(ServiceResource.GRADENAME));
            homeworkmodel.setDivisionName(innerObj.getString(ServiceResource.DIVISIONNAME));
            homeworkmodel.setStrDateOfHomeWork(innerObj.getString(ServiceResource.STRDATEOFFHOMEWORK));
            String isRead = innerObj.getString(ServiceResource.HOMEWORK_ISREAD);
            String isFinish = innerObj.getString(ServiceResource.HOMEWORK_ISWORKFINISHED);

            if (isFinish.equalsIgnoreCase("true")) {
                homeworkmodel.setFinish(true);
            } else {
                homeworkmodel.setFinish(false);
            }
            if (isRead.equalsIgnoreCase("true")) {
                homeworkmodel.setIsRead(true);
            } else {
                homeworkmodel.setIsRead(false);
            }

            homeworkListDataDao.insertHomeworkList(homeworkmodel);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
