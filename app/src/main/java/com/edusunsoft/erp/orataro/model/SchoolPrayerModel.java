package com.edusunsoft.erp.orataro.model;

public class SchoolPrayerModel {

    private String title;
    private String prayer;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrayer() {
        return prayer;
    }

    public void setPrayer(String prayer) {
        this.prayer = prayer;
    }

}
