package com.edusunsoft.erp.orataro.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ImageSelectionActivity;
import com.edusunsoft.erp.orataro.activities.PreviewImageActivity;
import com.edusunsoft.erp.orataro.adapter.PhotoListAdapter;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingDataDao;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

/**
 * album View
 *
 * @author admin
 */

public class AlbumFragment extends Fragment implements ResponseWebServices {

    private ImageView imgLeftMenu, imgRightMenu;
    private TextView txtHeader;
    private Context mContext;
    private GridView gv_album;
    private ArrayList<PhotoModel> photoModels;
    private ArrayList<PhotoModel> photoModels_add;
    private String uploaded_images_name = "";
    private PhotoModel photoModel;
    private PhotoListAdapter photo_ListAdapter;
    private Dialog dialog_image_Upload_select_image;
    private String fielPath;
    private byte[] byteArray;
    private GridView gv_photo;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private EditText et_album_name;
    private String from;
    private Uri fileUri;
    public static String ISFROM = "isFrom";
    TextView txt_nodatafound;

    // variable declaration for dynamicwallsetting
    DynamicWallSettingDataDao dynamicWallSettingDataDao;
    String BASE64_STRING = "", Path = "";
    ArrayList<String> GetImagename = new ArrayList<>();


    public static final AlbumFragment newInstance(String isFrom) {

        AlbumFragment f = new AlbumFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(ISFROM, isFrom);
        f.setArguments(bdl);
        return f;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.albumfragment, container, false);

        mContext = getActivity();
        from = getArguments().getString(ISFROM);
        imgLeftMenu = (ImageView) v.findViewById(R.id.img_home);
        imgRightMenu = (ImageView) v.findViewById(R.id.img_menu);
        txtHeader = (TextView) v.findViewById(R.id.header_text);
        gv_album = (GridView) v.findViewById(R.id.gv_photo);
        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);
        LinearLayout ll_add_new = (LinearLayout) v.findViewById(R.id.ll_add_new);
        imgLeftMenu.setImageResource(R.drawable.back);
        imgRightMenu.setVisibility(View.INVISIBLE);
        txtHeader.setText(getResources().getString(R.string.Album));

        dynamicWallSettingDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).dynamicWallSettingDataDao();
        if (dynamicWallSettingDataDao != null) {
            Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();
        }


        if (new UserSharedPrefrence(mContext).getLoginModel().isAllowUserToPostAlbum().equalsIgnoreCase("false")) {
            ll_add_new.setVisibility(View.GONE);
        } else {
            ll_add_new.setVisibility(View.VISIBLE);
        }


        if (Utility.isNetworkAvailable(getActivity())) {
            albumList(true);
        } else {
            Utility.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

        photoModels = new ArrayList<PhotoModel>();
        photoModels_add = new ArrayList<PhotoModel>();

        if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {
            if (new UserSharedPrefrence(mContext).getLoginModel().isAllowUserToPostAlbum().equalsIgnoreCase("false")) {
                ll_add_new.setVisibility(View.INVISIBLE);
            }

        } else {

            if (new UserSharedPrefrence(mContext).getLoginModel().isAllowUserToPostAlbum().equalsIgnoreCase("true")) {
                if (Global.DynamicWallSetting != null) {
                    if (!Global.DynamicWallSetting.getIsAllowPeopleToUploadAlbum()) {
                        ll_add_new.setVisibility(View.INVISIBLE);
                    }
                } else {
                    ll_add_new.setVisibility(View.INVISIBLE);
                }
            }
        }

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AddPhotoDialog();
            }
        });

        return v;
    }

    /**
     * addPhoto In ALbum Dialog
     */

    public void AddPhotoDialog() {

        dialog_image_Upload_select_image = CustomDialog.ShowDialog(getActivity(), R.layout.dialog_add_album, false);

        ImageView img_save = (ImageView) dialog_image_Upload_select_image
                .findViewById(R.id.img_save);

        ImageView img_back = (ImageView) dialog_image_Upload_select_image
                .findViewById(R.id.img_back);

        et_album_name = (EditText) dialog_image_Upload_select_image
                .findViewById(R.id.et_album_name);

        gv_photo = (GridView) dialog_image_Upload_select_image
                .findViewById(R.id.gv_photo_album);

        img_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog_image_Upload_select_image.dismiss();

            }

        });

        img_save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // dialog_image_Upload_select_image.dismiss();
                if (!et_album_name.getText().toString().isEmpty()) {

                    if (photoModels_add != null && photoModels_add.size() > 0) {

                        addAlbum();
                        photoModels_add.clear();

                    } else {

                        Utility.toast(getActivity(),
                                getResources().getString(R.string.PleaseselectatleastoneImage));
                    }

                } else {

                    Utility.toast(getActivity(),
                            getResources().getString(R.string.PleaseSpecifyAlbumName));

                }

            }

        });

        LinearLayout ll_add_new = (LinearLayout) dialog_image_Upload_select_image
                .findViewById(R.id.ll_add_new);

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    /**
     * select Image from Camera and Gallary
     */
    private void selectImage() {
        final CharSequence[] items = {getActivity().getResources().getString(R.string.takephoto),
                getActivity().getResources().getString(R.string.ChoosefromLibrary),
                getActivity().getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getActivity().getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getActivity().getResources().getString(R.string.takephoto))) {
                    Intent intent = new Intent(getActivity(), PreviewImageActivity.class);
                    intent.putExtra("FromIntent", "true");
                    intent.putExtra("RequestCode", 100);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals(getActivity().getResources().getString(R.string.ChoosefromLibrary))) {
                    Intent intent = new Intent(getActivity(), ImageSelectionActivity.class);
                    intent.putExtra("count", 5);
                    startActivityForResult(intent, SELECT_FILE);
                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {

//            photoModels_add.clear();

            if (requestCode == REQUEST_CAMERA) {

                Utility.getUserModelData(getActivity());

                try {

                    fielPath = data.getExtras().getString("imageData_uri");
                    Path = Utility.ImageCompress(fielPath, mContext);

                    File file = new File(fielPath);
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(getActivity()).compressToFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

//                    Utility.GetBASE64STRING(Path);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                photoModel = new PhotoModel();
                photoModel.setPhoto(fielPath);
                photoModels_add.add(photoModel);
                Log.d("photoadded", photoModels_add.toString());
                photo_ListAdapter = new PhotoListAdapter(getActivity(), photoModels_add, 1);
                gv_photo.setAdapter(photo_ListAdapter);


                if (Utility.BASE64_STRING != null) {
                    uploadPhoto();
                }

            } else if (requestCode == SELECT_FILE) {

//                photoModel = new PhotoModel();

                if (data != null) {

                    ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");

                    for (int k = 0; k < imgList.size(); k++) {

                        fielPath = imgList.get(k).getUri().toString();

                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                        Path = Utility.ImageCompress(fielPath, mContext);

                        File file = new File(fielPath);
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(getActivity()).compressToFile(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

//                        Utility.GetBASE64STRING(Path);
                        /*END*/

                        photoModel = new PhotoModel();
                        photoModel.setPhoto(fielPath);
                        photoModels_add.add(photoModel);

                        Log.d("photomodels", photoModels_add.toString());
                        photo_ListAdapter = new PhotoListAdapter(getActivity(), photoModels_add, 1);
                        gv_photo.setAdapter(photo_ListAdapter);

                        if (Utility.BASE64_STRING != null) {

                            uploadPhoto();

                        }
                    }
                }
            }
        }
    }


    public byte[] convertImageToByteArra(String filePath) {

        byte[] b = null;
        Bitmap thePic;
        thePic = Utility.getBitmap(filePath, mContext);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (filePath.contains(".png") || filePath.contains(".PNG")) {
            thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else {
            thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }

        b = null;
        b = stream.toByteArray();
        return b;

    }

    /**
     * webservice call for get album list
     */
    public void albumList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        Log.d("getRequest", arrayList.toString());

        new AsynsTaskClass(getActivity(), arrayList, isViewPopup, this).execute(ServiceResource.ALBUM_METHODNAME,
                ServiceResource.ALBUM_URL);

    }

    /**
     * WebService call For add Photo in  Album
     */

    public void addAlbum() {

        String photoNames = "";

        if (GetImagename != null && GetImagename.size() > 0) {
            for (int i = 0; i < GetImagename.size(); i++) {
                photoNames += GetImagename.get(i) + "#";
            }
        }

        if (photoNames != "") {
            photoNames = photoNames.substring(0, photoNames.length() - 1);
        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_WALLID,
                new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_USERID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_BEACHID, null));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_ALBUMSHARETYPE,
                "PUBLIC"));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_IMAGESPATH,
                photoNames));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_ALBUMDETAILS,
                ""));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_ALBUMTITLE,
                et_album_name.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_PLACES, ""));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_POSTBYTYPE,
                new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_APPROVED,
                true));

        if (photoNames != "") {
            new AsynsTaskClass(getActivity(), arrayList, true, this).execute(ServiceResource.UPLOAD_ALBUM_METHODNAME,
                    ServiceResource.ADDPHOTO_URL);
        }

    }

    /**
     * webservice call For Upload Photo Which return Image Server PATH
     */
    public void uploadPhoto() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
                new File(Utility.NewFileName).getName()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));

        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel()
                .getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE,
                "IMAGE"));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        if (Utility.BASE64_STRING != null)
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE,
                    Utility.BASE64_STRING));
        new AsynsTaskClass(getActivity(), arrayList, true, this).execute(ServiceResource.UPLOAD_PHOTO_METHODNAME,
                ServiceResource.ADDPHOTO_URL);


    }

    /**
     * we Get Result of All Webservice in THis Method
     */
    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.UPLOAD_PHOTO_METHODNAME.equalsIgnoreCase(methodName)) {

            if (photoModels_add != null && photoModels_add.size() > 0) {

                JSONArray arr = null;
                try {
                    arr = new JSONArray(result);
                    JSONObject jObj = arr.getJSONObject(0);
                    String message = jObj.getString("message");

                    String[] photoname = message.split("\\s");

                    if (!message.equalsIgnoreCase("Server is not responding, Please Try after some time.") || !message.equalsIgnoreCase("No Data Found")) {
                        GetImagename.add(photoname[1]);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        } else if (ServiceResource.ALBUM_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, methodName, getActivity());
            parseAlbulmList(result);

        } else if (ServiceResource.UPLOAD_ALBUM_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result != "") {
                dialog_image_Upload_select_image.dismiss();
            }

            if (Utility.isNetworkAvailable(getActivity())) {
                albumList(false);
            } else {
                Utility.showAlertDialog(getActivity(),
                        getActivity().getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

        }

    }

    public void parseAlbulmList(String result) {

        Log.d("getResult", result);
        JSONArray jsonObj;

        try {
            Global.AlbumModels = new ArrayList<PhotoModel>();

            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                PhotoModel model = new PhotoModel();
                model.setAlbumID(innerObj
                        .getString(ServiceResource.ALBUM_ID));
                model.setAlbumTitle(innerObj
                        .getString(ServiceResource.ALBUM_TITLE));
                model.setAlbumDetails(innerObj
                        .getString(ServiceResource.ALBUM_DETAIL));
                model.setPhoto(innerObj
                        .getString(ServiceResource.ALBUM_PHOTO));

                if (model.getPhoto() != null
                        && !model.getPhoto().equals("")) {

                    model.setPhoto(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + model.getPhoto());

                }

                model.setTotal(innerObj
                        .getString(ServiceResource.ALBUM_TOTAL));

                Global.AlbumModels.add(model);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        if (Global.AlbumModels != null && Global.AlbumModels.size() > 0) {

            PhotoListAdapter adapter = new PhotoListAdapter(
                    getActivity(), Global.AlbumModels, 0);
            gv_album.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            gv_album.setVisibility(View.VISIBLE);

        } else {

            gv_album.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoPhotoAlbumAvailable));

        }


    }


}
