package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ProjectListAdapter;
import com.edusunsoft.erp.orataro.adapter.SchoolGroupListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GroupListDataDao;
import com.edusunsoft.erp.orataro.database.GroupListModel;
import com.edusunsoft.erp.orataro.database.ProjectListDataDao;
import com.edusunsoft.erp.orataro.database.ProjectListModel;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.edusunsoft.erp.orataro.util.Utility.RedirectToDashboard;

public class SchoolGroupActivity extends Activity implements OnClickListener, ResponseWebServices, RefreshListner {

    private PullAndLoadListView listview;
    Context mContext;
    SchoolGroupListAdapter adapter;
    private LinearLayout ll_create_grp;
    ArrayList<GroupListModel> groupList;
    private ImageView img_back;
    EditText edtTeacherName;
    LinearLayout searchlayout;
    TextView txt_nodatafound;

    LinearLayout ll_comming_soon;
    FrameLayout fl_main;
    String isFrom = "";
    TextView header_text;

    // variable declaration for grouplist from ofline database
    GroupListDataDao groupListDataDao;
    private List<GroupListModel> GroupList = new ArrayList<>();
    GroupListModel groupListModel = new GroupListModel();
    /*END*/

    // variable declaration for projectlist from ofline database
    ProjectListDataDao projectListDataDao;
    private List<ProjectListModel> ProjectList = new ArrayList<>();
    ProjectListModel projectListModel = new ProjectListModel();
    /*END*/


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_group);

        mContext = this;

        if (getIntent() != null) {
            isFrom = getIntent().getStringExtra("isFrom");
            Log.d("isFrom", isFrom);
        }

        fl_main = (FrameLayout) findViewById(R.id.fl_main);
        ll_comming_soon = (LinearLayout) findViewById(R.id.ll_comming_soon);
        fl_main.setVisibility(View.VISIBLE);
        ll_comming_soon.setVisibility(View.GONE);
        listview = (PullAndLoadListView) findViewById(R.id.group_list);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        ll_create_grp = (LinearLayout) findViewById(R.id.ll_create_grp);
        edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
        searchlayout = (LinearLayout) findViewById(R.id.searchlayout);
        header_text = (TextView) findViewById(R.id.header_text);
        ll_create_grp.setOnClickListener(this);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        groupListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).groupListDataDao();
        projectListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).projectListDataDao();

        if (ServiceResource.SCHOOLGROUP_FLAG.equalsIgnoreCase(isFrom)) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsCreate()) {
                    ll_create_grp.setVisibility(View.VISIBLE);
                } else {
                    ll_create_grp.setVisibility(View.GONE);
                }
            } else {
                ll_create_grp.setVisibility(View.GONE);
            }
        }

        if (ServiceResource.PROJECT_FLAG.equalsIgnoreCase(isFrom)) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsCreate()) {

                    ll_create_grp.setVisibility(View.VISIBLE);

                } else {

                    ll_create_grp.setVisibility(View.GONE);

                }

            } else {

                ll_create_grp.setVisibility(View.GONE);

            }

        }

        Utility.ISLOADGROUP = false;
        Utility.ISLOADPROJECT = false;

        try {

            if (ServiceResource.SCHOOLGROUP_FLAG.equalsIgnoreCase(isFrom)) {

                header_text.setText(getResources().getString(R.string.SchoolGroup) + " (" + Utility.GetFirstName(mContext) + ")");
                header_text.setPadding(20, 0, 5, 0);

                // Display groupList from offline as well as online

                try {
                    DisplaySchoolGroupList();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                /*END*/

            } else if (ServiceResource.PROJECT_FLAG.equalsIgnoreCase(isFrom)) {
                header_text.setText(getResources().getString(R.string.Project) + " (" + Utility.GetFirstName(mContext) + ")");
                // Display projectList from offline as well as online
                try {
                    DisplayProjectList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        listview.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {
                try {
                    if (ServiceResource.SCHOOLGROUP_FLAG.equalsIgnoreCase(isFrom)) {

                        // Display HomeworkList from offline as well as online
                        try {
                            DisplaySchoolGroupList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*END*/
                        header_text.setText(getResources().getString(R.string.SchoolGroup) + " (" + Utility.GetFirstName(mContext) + ")");

                    } else if (ServiceResource.PROJECT_FLAG.equalsIgnoreCase(isFrom)) {
                        header_text.setText(getResources().getString(R.string.Project) + " (" + Utility.GetFirstName(mContext) + ")");
                        // Display projectList from offline as well as online
                        try {
                            DisplayProjectList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*END*/

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

        });

    }

    @Override
    protected void onResume() {

        try {
            if (ServiceResource.SCHOOLGROUP_FLAG.equalsIgnoreCase(isFrom)) {

                header_text.setText(getResources().getString(R.string.SchoolGroup) + " (" + Utility.GetFirstName(mContext) + ")");
                header_text.setPadding(20, 0, 5, 0);

                // Display groupList from offline as well as online

                if (Utility.ISLOADGROUP) {

                    try {
                        DisplaySchoolGroupList();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                /*END*/

            } else if (ServiceResource.PROJECT_FLAG.equalsIgnoreCase(isFrom)) {
                header_text.setText(getResources().getString(R.string.Project) + " (" + Utility.GetFirstName(mContext) + ")");

                if (Utility.ISLOADPROJECT) {
                    // Display projectList from offline as well as online
                    try {
                        DisplayProjectList();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /*END*/
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onResume();

    }

    private void DisplayProjectList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            ProjectList = projectListDataDao.getProjectList();
            if (ProjectList.isEmpty() || ProjectList.size() == 0 || ProjectList == null) {
                projectList(true);
            } else {
                //set Adapter
                setProjectlistAdapter(ProjectList);
                /*END*/
                projectList(false);
            }
        } else {
            ProjectList = projectListDataDao.getProjectList();
            if (ProjectList != null) {
                setProjectlistAdapter(ProjectList);
            }
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    private void setProjectlistAdapter(List<ProjectListModel> projectList) {
        if (projectList != null && projectList.size() > 0) {
            ProjectListAdapter adapter = new ProjectListAdapter(mContext, projectList, this);
            listview.setAdapter(adapter);
            listview.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);
        } else {
            listview.setVisibility(View.INVISIBLE);
            searchlayout.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoProjectAvailable));

        }
    }

    private void DisplaySchoolGroupList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            GroupList = groupListDataDao.getGroupList();
            if (GroupList.isEmpty() || GroupList.size() == 0 || GroupList == null) {
                GroupList(true);
            } else {
                //set Adapter
                setGrouplistAdapter(GroupList);
                /*END*/
                GroupList(false);
            }
        } else {
            GroupList = groupListDataDao.getGroupList();
            if (GroupList != null) {
                setGrouplistAdapter(GroupList);
            }
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }
    }

    private void setGrouplistAdapter(List<GroupListModel> groupList) {

        if (groupList != null && groupList.size() > 0) {

            adapter = new SchoolGroupListAdapter(mContext, groupList,
                    SchoolGroupActivity.this);
            listview.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);

        } else {

            listview.setVisibility(View.INVISIBLE);
            searchlayout.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoGroupAvailable));

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_create_grp:
                if (ServiceResource.SCHOOLGROUP_FLAG.equalsIgnoreCase(isFrom)) {
                    Intent intent = new Intent(mContext, Create_Group_Activity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                } else if (ServiceResource.PROJECT_FLAG.equalsIgnoreCase(isFrom)) {
                    Intent intent = new Intent(mContext, CreateProjectActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }

                break;

            case R.id.img_back:

                RedirectToDashboard(this);

                break;

            default:

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        RedirectToDashboard(this);

    }

    public void GroupList(boolean isViewPopup) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        Log.d("grouplistrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this, false).execute(ServiceResource.GROUP_METHODNAME, ServiceResource.GROUP_URL);
    }

    public void projectList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        Log.d("getprojectRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this, false).execute(ServiceResource.PROJECT_METHODNAME, ServiceResource.PROJECT_URL);
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GROUP_METHODNAME.equalsIgnoreCase(methodName)) {
            parsegrouplist(result);
        } else if (ServiceResource.PROJECT_METHODNAME.equalsIgnoreCase(methodName)) {
            parseProject(result);
        }

    }

    @Override
    public void refresh(String methodName) {
        if (ServiceResource.DELETEPROJECTS_METHODNAME.equalsIgnoreCase(methodName)) {
            projectList(false);
        } else {
            GroupList(false);
        }
    }

    public void parsegrouplist(String result) {

        Log.d("groupresult", result);

        JSONArray jsonObj;
        groupListDataDao.deleteGroupList();

        try {

            Global.groupModels = new ArrayList<GroupListModel>();
            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {

                JSONObject innerObj = jsonObj.getJSONObject(i);

                // parse homework list data
                ParseGroupList(innerObj);
                /*END*/

            }
            if (groupListDataDao.getGroupList().size() > 0) {
                //set Adapter
                setGrouplistAdapter(groupListDataDao.getGroupList());
                /*END*/
            }

        } catch (JSONException e) {

            e.printStackTrace();

        }


    }

    private void ParseGroupList(JSONObject innerObj) {

        try {
            groupListModel.setGroupId(innerObj.getString(ServiceResource.GROUP_ID));
            groupListModel.setGroupName(innerObj.getString(ServiceResource.GROUP_TITLE));
            groupListModel.setGroupSubject(innerObj.getString(ServiceResource.GROUP_SUBJECT));
            groupListModel.setGroupstatus(innerObj.getString(ServiceResource.GROUP_ABOUTGROUP));

            try {
                groupListModel.setGroupCreate(innerObj.getString(ServiceResource.GROUP_CREATEON));
            } catch (Exception e) {
                e.printStackTrace();
            }

            groupListModel.setGroupImage(innerObj.getString(ServiceResource.GROUP_IMAGE));
            groupListModel.setTotalMemeber(innerObj.getString(ServiceResource.GROUP_TOTALMEMEBER));
            groupListModel.setUserName(innerObj.getString(ServiceResource.GROUP_USERNAME));
            groupListModel.setWallID(innerObj.getString(ServiceResource.WALLID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        groupListDataDao.insertGroupList(groupListModel);

    }

    public void parseProject(String result) {

        Log.d("getprojectreult", result);
        if (result != null
                && !result.toString().equals(
                "[{\"message\":\"No Data Found\",\"success\":0}]")) {
            JSONArray jsonObj;
            projectListDataDao.deleteProjectList();
            try {
                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);

                    // parse homework list data
                    ParseProjectList(innerObj);
                    /*END*/
                }

                if (projectListDataDao.getProjectList().size() > 0) {
                    //set Adapter
                    setProjectlistAdapter(projectListDataDao.getProjectList());
                    /*END*/
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            listview.setVisibility(View.INVISIBLE);
            searchlayout.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoProjectAvailable));
        }


    }

    private void ParseProjectList(JSONObject innerObj) {
        try {
            projectListModel.setProjectID(innerObj.getString(ServiceResource.PROJECT_PROJECTID));
            try {
                projectListModel.setStartDate(Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PROJECT_STARTDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "MM/dd/yyyy"));
                projectListModel.setEndDate(Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PROJECT_ENDDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "MM/dd/yyyy"));
                projectListModel.setCreateBy(innerObj.getString(ServiceResource.PROJECT_CREATEBY));
                projectListModel.setCreateOn(Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PROJECT_CREATEON)
                                .replace("/Date(", "").replace(")/", "")),
                        "MM/dd/yyyy"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            projectListModel.setUserName(innerObj.getString(ServiceResource.PROJECT_USERNAME));
            projectListModel.setUpdateOn(innerObj.getString(ServiceResource.PROJECT_UPDATEON));
            projectListModel.setProjectTitle(innerObj.getString(ServiceResource.PROJECT_PROJECTTITLE));
            projectListModel.setProfilePicture(innerObj.getString(ServiceResource.PROJECT_PROFILEPIC));
            projectListModel.setWallID(innerObj.getString(ServiceResource.WALLID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        projectListDataDao.insertProjectList(projectListModel);

    }

}
