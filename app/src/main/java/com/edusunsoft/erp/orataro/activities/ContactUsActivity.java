package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class ContactUsActivity extends Activity {
	
	ImageView imgLeftheader,imgRightheader;
	TextView txtHeader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactus_activity);
		
		imgLeftheader = (ImageView) findViewById(R.id.img_home);
		imgRightheader = (ImageView) findViewById(R.id.img_menu);
		txtHeader  = (TextView) findViewById(R.id.header_text);
		
		imgLeftheader.setImageResource(R.drawable.back);
		imgRightheader.setVisibility(View.INVISIBLE);
		txtHeader.setText(getResources().getString(R.string.ContactUs));
		
		
		imgLeftheader.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		
	}

}
