package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FeesStructureAdapter;
import com.edusunsoft.erp.orataro.model.FeesAtomResponse;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.model.FeesPaymentType;
import com.edusunsoft.erp.orataro.model.FeesStructureModel;
import com.edusunsoft.erp.orataro.model.FeesSuccessResponseSend;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FeesDetailActivity extends Activity {

    Context mContext;
    ImageView img_home, img_menu;
    TextView header_text;
    ListView lv_feesstructure;
    FeesModel model;
    Button btnPay;
    public static final String KEY_ATOM2REQUEST = "Atom2Request";
    ArrayList<FeesStructureModel> listFeesstructure;
    ArrayList<FeesPaymentType> listFeespaymenttype;
    String DoubleAmountStr = "";
    TextView txtamounttext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees_detail);
        mContext = FeesDetailActivity.this;

        model = getIntent().getParcelableExtra("model");

        img_home = (ImageView) findViewById(R.id.img_home);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        header_text = (TextView) findViewById(R.id.header_text);
        btnPay = (Button) findViewById(R.id.btnPay);

        TextView intxtfees = (TextView) findViewById(R.id.txtfees);
        TextView intxtammount = (TextView) findViewById(R.id.txtammount);
        TextView intxtdue = (TextView) findViewById(R.id.txtdue);
        TextView intxtaction = (TextView) findViewById(R.id.txtaction);
        txtamounttext = (TextView) findViewById(R.id.txtamounttext);

        lv_feesstructure = (ListView) findViewById(R.id.lv_feesstructure);
        ImageView iv_std_icon = (ImageView) findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) findViewById(R.id.tv_std_name);
        TextView tv_institutename = (TextView) findViewById(R.id.tv_schoolname);
        TextView tv_std_standard = (TextView) findViewById(R.id.tv_std_standard);
        TextView tv_std_division = (TextView) findViewById(R.id.tv_std_division);

        if (header_text != null) {
            header_text.setText(getResources().getString(R.string.feesdetail) + " (" + Utility.GetFirstName(mContext) + ")");
        }


        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(),new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(iv_std_icon);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        tv_std_name.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME()));
        tv_institutename.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getINSTITUTENAME()));
        tv_std_standard.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_GRADENAME()));
        tv_std_division.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_DIVISIONNAME()));


        intxtfees.setText("Fees");
        intxtammount.setText("Amount");
        intxtdue.setText("Payable Amount");
        intxtaction.setText("Pay");


        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                DoubleAmountStr = txtamounttext.getText().toString();
//                Intent intent = new Intent(mContext, PaymentSelectionActivity.class);
//                intent.putExtra("price", DoubleAmountStr);
//                intent.putExtra("list", listFeesstructure);
//                intent.putExtra("feestype", listFeespaymenttype);
//                Log.d("passdeestype", listFeespaymenttype.toString());
//                intent.putExtra("model", model);
//                startActivityForResult(intent, 3);
//                finish();

            }

        });

        img_menu.setVisibility(View.INVISIBLE);
        img_home.setImageResource(R.drawable.back);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.FEESTRUCTUREID, model.getFeesStructuteID()));

        Log.d("feesdetailrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, new ResponseWebServices() {
            @Override
            public void response(String result, String methodName) {

                try {

                    JSONObject jobj = new JSONObject(result + "}");
                    JSONArray mainJsonArray = jobj.getJSONArray(ServiceResource.TABLE);
                    JSONArray mainJsonArray1 = jobj.getJSONArray(ServiceResource.TABLE1);
                    Type type = new TypeToken<ArrayList<FeesStructureModel>>() {
                    }.getType();
                    Type type1 = new TypeToken<ArrayList<FeesPaymentType>>() {
                    }.getType();

                    listFeesstructure = new Gson().fromJson(mainJsonArray.toString(), type);
                    listFeespaymenttype = new Gson().fromJson(mainJsonArray1.toString(), type1);

                    if (model.getStudentFeesCollectionID() == null || model.getStudentFeesCollectionID().equalsIgnoreCase("")) {

                        for (int i = 0; i < listFeesstructure.size(); i++) {

                            if (listFeesstructure.get(i).getIsFeeExemptional().equalsIgnoreCase("true")) {

                                double feesDiscount = Double.valueOf(listFeesstructure.get(i).getOriginalStructureHeadAmount())
                                        * Double.valueOf(listFeesstructure.get(i).getExemptionInPercentage()) / 100;
                                double netfees = Double.valueOf(listFeesstructure.get(i).getOriginalStructureHeadAmount()) - feesDiscount;

                                listFeesstructure.get(i).setTotalDue(String.valueOf(netfees));

                            } else {

                                if (listFeesstructure.get(i).getTotalDue() == null
                                        || listFeesstructure.get(i).getTotalDue().equalsIgnoreCase("") ||
                                        listFeesstructure.get(i).getTotalDue().equalsIgnoreCase("null")) {
                                    listFeesstructure.get(i).setTotalDue(listFeesstructure.get(i).getOriginalStructureHeadAmount());
                                }

                            }

                        }

                    } else {

                        for (int i = 0; i < listFeesstructure.size(); i++) {

                            if (listFeesstructure.get(i).getTotalDue() == null
                                    || listFeesstructure.get(i).getTotalDue().equalsIgnoreCase("") ||
                                    listFeesstructure.get(i).getTotalDue().equalsIgnoreCase("null")) {

                                listFeesstructure.get(i).setTotalDue(listFeesstructure.get(i).getOriginalStructureHeadAmount());

                            }

                        }

                    }

                    FeesStructureAdapter listAdapter = new FeesStructureAdapter(mContext, listFeesstructure);
                    lv_feesstructure.setAdapter(listAdapter);
                    double totalAmount = 0;
                    for (int i = 0; i < listFeesstructure.size(); i++) {
                        totalAmount = totalAmount + StringToDouble(listFeesstructure.get(i).getTotalDue());
                    }

                    txtamounttext.setText(totalAmount + "");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).execute(ServiceResource.GETSTUDENTFEESTRUCTUREINFO_METHODNAME,
                ServiceResource.FEES_URL);
    }

    public double StringToDouble(String str) {
        double totalAmount = 0;
        try {
            totalAmount = Double.valueOf(str);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalAmount;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 3) {

            if (data != null) {
                String response = data.getStringExtra("Result");


                Type collectionType = new TypeToken<FeesAtomResponse>() {
                }.getType();
                FeesAtomResponse responseobject = new Gson().fromJson(response, collectionType);

                FeesSuccessResponseSend feesSucessResponse = new FeesSuccessResponseSend();
                FeesSuccessResponseSend.Table table = new FeesSuccessResponseSend.Table();
                FeesSuccessResponseSend.Table1 table1 = new FeesSuccessResponseSend.Table1();

                table.setStudentFeesCollectionID("");
                table.setBatchID(new UserSharedPrefrence(mContext).getBATCHID());
                table.setStudentID(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
                table.setFeesStructureID(model.getFeesStructuteID());
                table.setStandardID(new UserSharedPrefrence(mContext).getLOGIN_GRADEID());
                table.setDateOfFees(responseobject.getDate());
                table.setAmountToBePaid(DoubleAmountStr);
                table.setTotalDue("0");
                table.setIsPaid("true");
                table.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());
                table.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());
                table.setIsActive("true");
                table.setPaidAmounts(DoubleAmountStr);
                table.setUserID(new UserSharedPrefrence(mContext).getLOGIN_USERID());
                table.setBookID("");
                table.setRefStudentFeesCollectionID(model.getStudentFeesCollectionID());
                table.setMOPTypeTerm(responseobject.getDiscriminator());
                table.setBankName(responseobject.getBankName());
                table.setReferenceNo(responseobject.getMmpTxn());


                for (int j = 0; j < listFeespaymenttype.size(); j++) {

                    table1.setStudentFeesCollectionDetailID("");
                    table1.setStudentFeesCollectionID("");
                    table1.setFeesStructureID(listFeesstructure.get(j).getFeesStructuteID());
                    table1.setFeesHeadID(listFeesstructure.get(j).getFeesHeadID());
                    table1.setOriginalAmount(listFeesstructure.get(j).getTotalDue());
                    table1.setReceivedAmount(listFeesstructure.get(j).getTotalDue());
                    table1.setBookID("");
                    table1.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());
                    table1.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());
                    table1.setIsActive("true");
                    table1.setIsPaid("true");
                    table1.setTotalDue("0");
                    table1.setIsOptionalFeeHead("false");

                }

                ArrayList<FeesSuccessResponseSend.Table> tableList = new ArrayList<FeesSuccessResponseSend.Table>();
                ArrayList<FeesSuccessResponseSend.Table1> table1List = new ArrayList<FeesSuccessResponseSend.Table1>();
                tableList.add(table);
                table1List.add(table1);
                feesSucessResponse.setTable(tableList);
                feesSucessResponse.setTable1(table1List);
                String sendJson = new Gson().toJson(feesSucessResponse);

            }

        }
        finish();

    }

}



