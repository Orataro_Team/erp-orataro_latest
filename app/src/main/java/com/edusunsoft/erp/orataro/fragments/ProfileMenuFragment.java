package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.FragmentViewActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.Information_prayerActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.ListSelectionActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPostOnWallActivity;
import com.edusunsoft.erp.orataro.activities.HealthRecordActivity;
import com.edusunsoft.erp.orataro.activities.ListLeaveActivity;
import com.edusunsoft.erp.orataro.activities.ListWallActivity;
import com.edusunsoft.erp.orataro.activities.MyPhoneChainActivity;
import com.edusunsoft.erp.orataro.activities.ParentProfileActivity;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;
import com.edusunsoft.erp.orataro.activities.SchoolGroupActivity;
import com.edusunsoft.erp.orataro.adapter.MenuAdapter;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.gaurav.badgelibproject.provider.Badges;
import com.gaurav.badgelibproject.provider.BadgesNotSupportedException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class ProfileMenuFragment extends Fragment implements OnClickListener, OnItemClickListener, ResponseWebServices {

    Context mContext;
    String[] actionlist;
    boolean[] showArrow = {true, true, false, false, true, false, false, false, false,
            false, false, false, false, false, false, false, false};

    int[] iconlist = new int[]{R.drawable.institutepages, R.drawable.blogs,
            //R.drawable.friends,
            R.drawable.photo,
            R.drawable.schoolgroups, R.drawable.myhappygram,
            /*	R.drawable.dash_information,*/R.drawable.dash_school_prayer,
            R.drawable.dash_institute, R.drawable.standard,
            R.drawable.dash_division, R.drawable.dash_subject,
            R.drawable.project, R.drawable.leave
            , R.drawable.power_off};

    private ListView menuList;

    private MenuAdapter adpter;

    LinearLayout ll_profile, ll_lefticon, ll_madel;
    TextView tv_name;
    ImageView imgPhoneChain, imgHealthRecord, imgParentProfile;
    CircleImageView img_profilepic;
    private Dialog mPoweroffDialog;
    private String regId;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private byte[] byteArray;

    private String filePath;

    TextView txt_version;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.profile_menu, container, false);

        mContext = getActivity();

        if (Utility.isTeacher(getActivity())) {

            iconlist = new int[]{
//                    R.drawable.institutepages,
//                    R.drawable.blogs,
                    //R.drawable.friends,
                    R.drawable.photo_blue,
                    R.drawable.video,
                    R.drawable.album_blue,
                    R.drawable.attched_file,
//                    R.drawable.schoolgroups,
//                    R.drawable.myhappygram,
                    /*	R.drawable.dash_information,*/
                    R.drawable.dash_school_prayer,
//                    R.drawable.dash_institute,
//                    R.drawable.standard,
//                    R.drawable.dash_division,
//                    R.drawable.dash_subject,
//                    R.drawable.project,
//                    R.drawable.leave
                    //R.drawable.stdentprofiles
//                    R.drawable.power_off
            };

            actionlist = new String[]{
//                    mContext.getResources().getString(R.string.InstitutePages),
//                    mContext.getResources().getString(R.string.blogs),
                    //	mContext.getResources().getString(R.string.Friends),
                    mContext.getResources().getString(R.string.allphoto),
                    mContext.getResources().getString(R.string.Video),
                    mContext.getResources().getString(R.string.Album),
                    mContext.getResources().getString(R.string.File),
//                    mContext.getResources().getString(R.string.SchoolGroups),
//                    mContext.getResources().getString(R.string.MyHappygram),
                    //				mContext.getResources().getString(R.string.Information),
                    mContext.getResources().getString(R.string.SchoolPrayer),
//                    mContext.getResources().getString(R.string.Institute),
//                    mContext.getResources().getString(R.string.Standard),
//                    mContext.getResources().getString(R.string.Division),
//                    mContext.getResources().getString(R.string.Subjects),
//                    mContext.getResources().getString(R.string.Projects),
//                    mContext.getResources().getString(R.string.leave)
                    // mContext.getResources().getString(R.string.studentinformation),
//                    mContext.getResources().getString(R.string.LogOut
            };

        } else {

            iconlist = new int[]{
//                    R.drawable.institutepages,
//                    R.drawable.blogs,
                    //R.drawable.friends,
                    R.drawable.photo_blue, R.drawable.video, R.drawable.album_blue, R.drawable.attched_file,
//                    R.drawable.schoolgroups, R.drawable.myhappygram,
                    /*	R.drawable.dash_information,*/R.drawable.dash_school_prayer,
//                    R.drawable.dash_institute, R.drawable.standard,
//                    R.drawable.dash_division, R.drawable.dash_subject,
//                    R.drawable.project, R.drawable.leave
            };

            actionlist = new String[]{
//                    mContext.getResources().getString(R.string.InstitutePages),
//                    mContext.getResources().getString(R.string.blogs),
                    //	mContext.getResources().getString(R.string.Friends),
                    mContext.getResources().getString(R.string.allphoto),
                    mContext.getResources().getString(R.string.Video),
                    mContext.getResources().getString(R.string.Album),
                    mContext.getResources().getString(R.string.File),
//                    mContext.getResources().getString(R.string.SchoolGroups),
//                    mContext.getResources().getString(R.string.MyHappygram),
                    //			mContext.getResources().getString(R.string.Information),
                    mContext.getResources().getString(R.string.SchoolPrayer),
//                    mContext.getResources().getString(R.string.Institute),
//                    mContext.getResources().getString(R.string.Standard),
//                    mContext.getResources().getString(R.string.Division),
//                    mContext.getResources().getString(R.string.Subjects),
//                    mContext.getResources().getString(R.string.Projects),
//                    mContext.getResources().getString(R.string.leave)

//                    mContext.getResources().getString(R.string.LogOut

            };

        }

        initializeUI(view);
        menuList.setOnItemClickListener(this);
        ll_profile = (LinearLayout) view.findViewById(R.id.ll_profile);
        ll_profile.setOnClickListener(this);
        new UserSharedPrefrence(mContext).setCURRENTWALLID(new UserSharedPrefrence(mContext).getLoginModel().getWallID());

        return view;

    }

    private void initializeUI(View v) {

        // version textview
        txt_version = (TextView) v.findViewById(R.id.txt_version);

        HomeWorkFragmentActivity.btn_menu.setVisibility(View.GONE);
        HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.INVISIBLE);
        // menu UI
        menuList = (ListView) v.findViewById(R.id.menu_list);
        imgPhoneChain = (ImageView) v.findViewById(R.id.imgPhoneChain);
        imgHealthRecord = (ImageView) v.findViewById(R.id.imghealthrecord);
        imgParentProfile = (ImageView) v.findViewById(R.id.imgParentProfile);
        ll_lefticon = (LinearLayout) v.findViewById(R.id.ll_lefticon);
        tv_name = (TextView) v.findViewById(R.id.tv_name);
        img_profilepic = (CircleImageView) v.findViewById(R.id.img_profilepic);
        ll_madel = (LinearLayout) v.findViewById(R.id.ll_madel);
        adpter = new MenuAdapter(getActivity().getApplicationContext(),
                iconlist, actionlist, true, showArrow, false);
        adpter.setcolor(true);
        menuList.setAdapter(adpter);

        // get Current Version of Application - 05-09-2019

        PackageInfo pinfo = null;
        try {
            pinfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            String versionName = pinfo.versionName;
            txt_version.setText("Version " + versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.MyProfile) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 40, 0);
                HomeWorkFragmentActivity.btn_menu.setVisibility(View.GONE);
                HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {

            if (Utility.isNull(new UserSharedPrefrence(mContext).getLoginModel().getDisplayName())) {
                tv_name.setText(new UserSharedPrefrence(mContext).getLoginModel().getDisplayName());
            }

            if (Utility.isTeacher(mContext)) {
                ll_lefticon.setVisibility(View.GONE);
                ll_madel.setVisibility(View.INVISIBLE);
            }

        }

        imgPhoneChain.setOnClickListener(this);
        imgHealthRecord.setOnClickListener(this);
        imgParentProfile.setOnClickListener(this);
        img_profilepic.setOnClickListener(this);

        // Code Comment by hardik_kanak 25-01-2021
//        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
//
//            String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
//            if (ProfilePicture != null) {
//
//                try {
//
//                    RequestOptions options = new RequestOptions()
//                            .centerCrop()
//                            .placeholder(R.drawable.photo)
//                            .diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .priority(Priority.HIGH)
//                            .dontAnimate()
//                            .dontTransform();
//
//                    Glide.with(mContext)
//                            .load(ServiceResource.BASE_IMG_URL1
//                                    + ProfilePicture
//                                    .replace("//", "/")
//                                    .replace("//", "/"))
//                            .apply(options)
//                            .into(img_profilepic);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//        }


        if (new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture() != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture())
                        .apply(options)
                        .into(img_profilepic);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.ll_profile:
                intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                ServiceResource.WALLTITLE = "My Wall";
                startActivity(intent);
                break;

            case R.id.imgPhoneChain:
                intent = new Intent(mContext, MyPhoneChainActivity.class);
                startActivity(intent);
                break;

            case R.id.imghealthrecord:
                intent = new Intent(mContext, HealthRecordActivity.class);
                startActivity(intent);
                break;

            case R.id.imgParentProfile:

                intent = new Intent(mContext, ParentProfileActivity.class);
                startActivity(intent);

                break;

            case R.id.img_profilepic:

                //selectImage();

                break;

            default:
                break;

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (Utility.isTeacher(getActivity())) {

            onClickTeacher(position);

        } else {

            onclick(position);

        }

    }

    public void onClickTeacher(int position) {

        Intent intent;
        FragmentTransaction ft;

        switch (position) {

            case 0:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.PHOTO);
                startActivity(intent);

                break;

            case 1:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.VIDEO);
                startActivity(intent);

                break;

            case 2:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                startActivity(intent);

                break;

            case 3:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.FILE);
                startActivity(intent);

//                intent = new Intent(mContext, FragmentViewActivity.class);
//                intent.putExtra("isFrom", ServiceResource.VIDEO);
//                startActivity(intent);

                break;

            case 4:

                intent = new Intent(mContext, Information_prayerActivity.class);
                intent.putExtra("isFrom", ServiceResource.SCHOOLPRAYER_FLAG);
                startActivity(intent);

//                intent = new Intent(mContext, FragmentViewActivity.class);
//                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
//                startActivity(intent);

                break;

            case 5:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.FILE);
                startActivity(intent);

                break;

            case 6:

                if (Utility.isTeacher(mContext)) {

                    if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {

                        intent = new Intent(mContext, SchoolGroupActivity.class);
                        intent.putExtra("isFrom", ServiceResource.SCHOOLGROUP_FLAG);
                        startActivity(intent);

                    } else {

                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                    }

                } else {

                    intent = new Intent(mContext, ListWallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                    startActivity(intent);

                }

                break;

            case 7:

                if (Utility.isTeacher(mContext)) {

                    if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsView()) {

                        intent = new Intent(mContext, ListSelectionActivity.class);
                        intent.putExtra("isFrom", ServiceResource.HAPPYGRAM_FLAG);
                        startActivity(intent);

                    } else {

                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                    }

                } else {

                    intent = new Intent(mContext, HappygramFragment.class);
                    startActivity(intent);

                }

                break;

            case 8:

                intent = new Intent(mContext, Information_prayerActivity.class);
                intent.putExtra("isFrom", ServiceResource.SCHOOLPRAYER_FLAG);
                startActivity(intent);

                break;

            case 9:

                ServiceResource.FROMACTIVITY = "FromProfile";
                intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                ServiceResource.WALLTITLE = ServiceResource.INSTITUTEWALL;
                startActivity(intent);

                break;

            case 10:

                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
                startActivity(intent);

                break;

            case 11:

                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                startActivity(intent);

                break;

            case 12:

                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                startActivity(intent);

                break;

            case 13:

                if (Utility.isTeacher(mContext)) {

                    if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsView()) {

                        intent = new Intent(mContext, SchoolGroupActivity.class);
                        intent.putExtra("isFrom", ServiceResource.PROJECT_FLAG);
                        startActivity(intent);

                    } else {

                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                    }

                } else {

                    intent = new Intent(mContext, ListWallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                    startActivity(intent);

                }

                break;

            case 14:

                if (Utility.isTeacher(mContext)) {

                    intent = new Intent(mContext, ListSelectionActivity.class);
                    intent.putExtra("isFrom", ServiceResource.LEAVE_FLAG);
                    intent.putExtra("iswithoutsubject", true);
                    startActivity(intent);

                } else {

                    intent = new Intent(mContext, ListLeaveActivity.class);
                    startActivity(intent);

                }

                break;

            case 16:

                Intent i = new Intent(mContext, ListSelectionActivity.class);
                i.putExtra("isFrom", ServiceResource.STUDENT_INFORMATION);
                i.putExtra("iswithoutsubject", true);
                startActivity(i);

                break;

//            case 15:
//                userLogout();
//                break;

            default:

                break;

        }

    }

    public void onclick(int position) {

        Intent intent;
        FragmentTransaction ft;

        switch (position) {

            case 0:

                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.PHOTO);
                startActivity(intent);

//                if (Utility.isTeacher(mContext)) {
//
//                    if (Utility.ReadWriteSetting(ServiceResource.PAGE).getIsView()) {
//
//                        intent = new Intent(mContext, PageListActivity.class);
//                        startActivity(intent);
//
//                    } else {
//
//                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
//
//                    }
//
//                } else {
//
//                    intent = new Intent(mContext, PageListActivity.class);
//                    startActivity(intent);
//
//                }

                break;

            case 1:


                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.VIDEO);
                startActivity(intent);

//                if (Utility.isTeacher(mContext)) {
//
//                    if (Utility.ReadWriteSetting(ServiceResource.BLOG).getIsView()) {
//
//                        intent = new Intent(mContext, PageListActivity.class);
//                        intent.putExtra("isFrom", ServiceResource.BLOGLIST);
//                        startActivity(intent);
//
//                    } else {
//
//                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
//
//                    }
//
//                } else {
//
//                    intent = new Intent(mContext, PageListActivity.class);
//                    intent.putExtra("isFrom", ServiceResource.BLOGLIST);
//                    startActivity(intent);
//
//                }

                break;

            case 2:
                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                startActivity(intent);
//                intent = new Intent(mContext, FragmentViewActivity.class);
//                intent.putExtra("isFrom", ServiceResource.PHOTO);
//                startActivity(intent);
                break;
            case 3:
                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.FILE);
                startActivity(intent);
//                intent = new Intent(mContext, FragmentViewActivity.class);
//                intent.putExtra("isFrom", ServiceResource.VIDEO);
//                startActivity(intent);
                break;
            case 4:
                intent = new Intent(mContext, Information_prayerActivity.class);
                intent.putExtra("isFrom", ServiceResource.SCHOOLPRAYER_FLAG);
                startActivity(intent);
//                intent = new Intent(mContext, FragmentViewActivity.class);
//                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
//                startActivity(intent);
                break;
            case 5:
                intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("isFrom", ServiceResource.FILE);
                startActivity(intent);
                break;
            case 6:
                if (Utility.isTeacher(mContext)) {
                    if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {
                        intent = new Intent(mContext, SchoolGroupActivity.class);
                        intent.putExtra("isFrom", ServiceResource.SCHOOLGROUP_FLAG);
                        startActivity(intent);
                    } else {
                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                    }
                } else {
                    intent = new Intent(mContext, ListWallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                    startActivity(intent);
                }
                break;
            case 7:
                if (Utility.isTeacher(mContext)) {
                    if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsView()) {
                        intent = new Intent(mContext, ListSelectionActivity.class);
                        intent.putExtra("isFrom", ServiceResource.HAPPYGRAM_FLAG);
                        startActivity(intent);
                    } else {
                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                    }
                } else {
                    intent = new Intent(mContext, HappygramFragment.class);
                    startActivity(intent);
                }
                break;
            case 8:
                intent = new Intent(mContext, Information_prayerActivity.class);
                intent.putExtra("isFrom", ServiceResource.SCHOOLPRAYER_FLAG);
                startActivity(intent);
                break;
            case 9:
                intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                ServiceResource.WALLTITLE = "Institute wall";
                startActivity(intent);
                break;
            case 10:
                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
                startActivity(intent);
                break;
            case 11:
                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                startActivity(intent);
                break;
            case 12:
                intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                startActivity(intent);
                break;
            case 13:
                if (Utility.isTeacher(mContext)) {
                    if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsView()) {
                        intent = new Intent(mContext, SchoolGroupActivity.class);
                        intent.putExtra("isFrom", ServiceResource.PROJECT_FLAG);
                        startActivity(intent);
                    } else {
                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                    }
                } else {
                    intent = new Intent(mContext, ListWallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                    startActivity(intent);
                }
                break;
            case 14:
                if (Utility.isTeacher(mContext)) {
                    intent = new Intent(mContext, ListSelectionActivity.class);
                    intent.putExtra("isFrom", ServiceResource.LEAVE_FLAG);
                    intent.putExtra("iswithoutsubject", true);
                    startActivity(intent);
                } else {
                    intent = new Intent(mContext, ListLeaveActivity.class);
                    startActivity(intent);
                }
                break;
            case 15:
                userLogout();
                break;
            default:
                break;
        }

    }

    private void userLogout() {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, true);

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog
                .findViewById(R.id.ll_submit);

        ll_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
                new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
                Intent intent = new Intent(mContext, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                new UserSharedPrefrence(mContext).clearPrefrence();
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(0, 0);

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog.dismiss();

            }

        });
    }

    @Override
    public void onResume() {
        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.MyProfile) + " (" + Utility.GetFirstName(mContext) + ")");
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
        super.onResume();

    }


    public void logout() {
        try {
            Badges.removeBadge(mContext);
            Badges.setBadge(mContext, Integer.valueOf("0"));
        } catch (BadgesNotSupportedException badgesNotSupportedException) {
            Log.d("Badge provider", badgesNotSupportedException.getMessage());
        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, regId));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.LOGOUT_METHODNAME, ServiceResource.LOGIN_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.LOGOUT_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result.contains("removed")) {
                if (mPoweroffDialog != null) {
                    mPoweroffDialog.dismiss();
                }
                new UserSharedPrefrence(mContext).clearPrefrence();
                Intent intent = new Intent(mContext, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        } else if (ServiceResource.CHNAGEPROFILEPHOTOS_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                JSONArray jArray = new JSONArray(result);
                JSONObject jobj = jArray.getJSONObject(0);
                String urlArray = jobj.getString(ServiceResource.MESSAGE);
                String[] url = urlArray.split(",");
                if (url != null && url.length > 0) {
                    new UserSharedPrefrence(mContext).setLOGIN_PROFILEPIC(url[0]);
                    new UserSharedPrefrence(mContext).getLoginModel().setProfilePicture(url[0]);
                }

            } catch (JSONException e) {

                e.printStackTrace();

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AddPostOnWallActivity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {
                try {
                    filePath = data.getExtras().getString("imageData_uri");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                byteArray = data.getExtras().getByteArray("imageData_byte");
                img_profilepic.setImageBitmap(new BitmapFactory().decodeByteArray(byteArray, 0, byteArray.length));
                if (byteArray != null) {
                    UploadProfilePic();
                }
            } else if (requestCode == SELECT_FILE) {
                if (data != null) {
                    ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                    String[] filePathArray = new String[imgList.size()];
                    for (int k = 0; k < imgList.size(); k++) {
                        filePath = imgList.get(k).getUri().toString();
                        filePathArray[k] = filePath;
                        Bitmap thePic;
                        thePic = Utility.getBitmap(imgList.get(k).getUri().toString(), mContext);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        if (filePath.contains(".png") || filePath.contains(".PNG")) {
                            thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        } else {
                            thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        }
                        byteArray = null;
                        byteArray = stream.toByteArray();//convertImageToByteArra(filePath);
                        img_profilepic.setImageBitmap(thePic);
                        if (byteArray != null) {
                            UploadProfilePic();
                        }
                    }
                }
            }
        }
    }

    private void UploadProfilePic() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.NAMEPHOTO, filePath.trim()));
        if (byteArray != null) arrayList.add(new PropertyVo(ServiceResource.PHOTOFILE, byteArray));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHNAGEPROFILEPHOTOS_METHODNAME, ServiceResource.PARENTPROFILE_URL);
    }


}
