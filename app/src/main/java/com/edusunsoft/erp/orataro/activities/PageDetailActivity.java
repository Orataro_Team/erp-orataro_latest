package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PageAdapter;
import com.edusunsoft.erp.orataro.database.BlogListModel;
import com.edusunsoft.erp.orataro.database.CmsPageListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class PageDetailActivity extends Activity implements OnClickListener, ResponseWebServices {

    private WebView Wv_page;
    private Context mContext;
    private int[] iconlist = {R.drawable.pages, R.drawable.homework,
            R.drawable.schoolgroups, R.drawable.myhappygram,
            R.drawable.myhappygram, R.drawable.myhappygram,
            R.drawable.myhappygram, R.drawable.myhappygram};
    private ArrayList<CmsPageListModel> pagesModels = new ArrayList<CmsPageListModel>();
    private PageAdapter adpter;
    private ImageView img_back;
    private LinearLayout ll_search;
    private EditText edtTeacherName;
    private TextView header_text, txt_nodatafound;
    private CmsPageListModel cmsPageListModel;
    private BlogListModel blogListModel;
    private String pageTypeStr;
    private boolean isBlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_detail_);

        mContext = this;
        Wv_page = (WebView) findViewById(R.id.Wv_page);
        img_back = (ImageView) findViewById(R.id.img_back);

        pageTypeStr = getIntent().getStringExtra("pageTypeStr");
        isBlog = getIntent().getBooleanExtra("isBlog", false);
        header_text = (TextView) findViewById(R.id.header_text);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

        if (isBlog) {
            blogListModel = (BlogListModel) getIntent().getSerializableExtra("Page");
        } else {
            cmsPageListModel = (CmsPageListModel) getIntent().getSerializableExtra("Page");
        }


        try {
            if (isBlog) {
                header_text.setText(blogListModel.getPageName() + " (" + Utility.GetFirstName(mContext) + ")");
            } else {
                header_text.setText(cmsPageListModel.getPageName() + " (" + Utility.GetFirstName(mContext) + ")");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isBlog) {
            blogDetail();
        } else {
            pageDetail();
        }

        img_back.setOnClickListener(this);

    }


    public void showTextView() {

        txt_nodatafound.setVisibility(View.VISIBLE);

        if (pageTypeStr != null) {
            if (pageTypeStr.equalsIgnoreCase(ServiceResource.INFORMATION_STRING)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoInformationAvailble));
            } else if (pageTypeStr.equalsIgnoreCase(ServiceResource.PRAYER_STRING)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoPrayerListAvailble));
            } else if (pageTypeStr.equalsIgnoreCase(ServiceResource.TIMETABLE_STRING)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoTimeTableListAvailble));
            } else if (pageTypeStr.equalsIgnoreCase(ServiceResource.SCHOOLTIMEING_STRING)) {
                txt_nodatafound.setText(getResources().getString(R.string.NoSchoolTimingAvailble));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            default:
                break;
        }
    }

    public void pageDetail() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CMSPAGE_ID, cmsPageListModel.getCMSPagesID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETCMSPAGEDETAIL_METHODNAME, ServiceResource.CMSPAGE_URL);
    }

    public void blogDetail() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.BLOG_BLOGID, blogListModel.getCMSPagesID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETBLOGDETAIL_METHODNAME, ServiceResource.BLOG_URL);
    }

    public void parseDetail(String result) {
        String pageDetail = null;
        JSONArray hJsonArray;

        Log.d("getResult", result);

        try {

            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);
                if (isBlog) {
                    pageDetail = hJsonArray.getJSONObject(0).getString(ServiceResource.BLOGDETAILS);
                } else {
                    pageDetail = hJsonArray.getJSONObject(0).getString(ServiceResource.CMSPAGES_PAGEDETAILS);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (pageDetail != null && !pageDetail.equals("") && !pageDetail.equals("null")) {

            Wv_page.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);
            Wv_page.getSettings().setJavaScriptEnabled(true);
            Wv_page.getSettings().setLoadWithOverviewMode(true);
            Wv_page.getSettings().setUseWideViewPort(true);
            StringBuilder sb = new StringBuilder();
            sb.append("<HTML><HEAD><LINK href=" + ServiceResource.CSSFILE + " type=\"text/css\" rel=\"stylesheet\"/></HEAD><body>");
            sb.append(pageDetail.toString().replaceAll("/DataFiles", ServiceResource.BASE_IMG_URL + "DataFiles"));
            sb.append("</body></HTML>");
            Log.v("html", sb.toString());
            Wv_page.loadDataWithBaseURL("", sb.toString(), "text/html",
                    "UTF-8", "");
            Wv_page.getSettings().setBuiltInZoomControls(true);

            txt_nodatafound.setVisibility(View.GONE);
        } else {
            Wv_page.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            showTextView();
        }
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.GETCMSPAGEDETAIL_METHODNAME.equalsIgnoreCase(methodName) ||
                ServiceResource.GETBLOGDETAIL_METHODNAME.equalsIgnoreCase(methodName)
        ) {
            parseDetail(result);
        }
    }
}
