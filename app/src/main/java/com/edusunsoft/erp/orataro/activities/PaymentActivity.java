package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

import java.util.ArrayList;

public class PaymentActivity extends AppCompatActivity implements ResponseWebServices, View.OnClickListener {

    FeesModel model;
    Context mContext;
    String PaymentMasterRegID = "", UnpaidAmount = "";
    private WebView Wv_page;
    private TextView header_text, txt_nodatafound;
    private ImageView img_back;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_detail_);

        Initialization();

    }

    private void Initialization() {

        mContext = PaymentActivity.this;
        Wv_page = (WebView) findViewById(R.id.Wv_page);
        img_back = (ImageView) findViewById(R.id.img_back);
        header_text = (TextView) findViewById(R.id.header_text);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

        try {
            header_text.setText(getResources().getString(R.string.payment));
        } catch (Exception e) {
            e.printStackTrace();
        }
        img_back.setOnClickListener(this);

        try {



            GetUrlForPaymentGateway(new UserSharedPrefrence(mContext).getLoginModel().getUserID(),
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID(),
                    getIntent().getExtras().getString("BatchID"),
                    getIntent().getExtras().getString("ClientID"),
                    getIntent().getExtras().getString("InstituteID"),
                    getIntent().getExtras().getString("FeeStructureID"),
                    getIntent().getExtras().getString("FeeStructureScheduleID"),
                    getIntent().getExtras().getString("UnpaidAmount"),
                    getIntent().getExtras().getString("PaymentGateWayMasterID"),
                    "Android", new UserSharedPrefrence(mContext).getLoginModel().getInstituteName(),
                    new UserSharedPrefrence(mContext).getLoginModel().getDisplayName(),
                    getIntent().getExtras().getDouble("PayAmt",0.0)); // add PayAmt by hardik_kanak 26-11-2020
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetUrlForPaymentGateway(String userID, String memberID, String batchID, String clientID, String instituteID, String feesStructuteID, String feesStructureScheduleID, String unpaid_amount, String paymentGateWayMasterID, String android, String instituteName, String displayName, Double PayAmt) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.U, userID));
        arrayList.add(new PropertyVo(ServiceResource.S, memberID));
        arrayList.add(new PropertyVo(ServiceResource.B, batchID));
        arrayList.add(new PropertyVo(ServiceResource.C, clientID));
        arrayList.add(new PropertyVo(ServiceResource.I, instituteID));
        arrayList.add(new PropertyVo(ServiceResource.FS, feesStructuteID));
        arrayList.add(new PropertyVo(ServiceResource.FSS, feesStructureScheduleID));
        arrayList.add(new PropertyVo(ServiceResource.a, String.valueOf(unpaid_amount)));
        arrayList.add(new PropertyVo(ServiceResource.PGM, paymentGateWayMasterID));
        arrayList.add(new PropertyVo(ServiceResource.REQUESTFROM, android));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTENAME, instituteName));
        arrayList.add(new PropertyVo(ServiceResource.USERDISPLAYNAME, displayName));
        arrayList.add(new PropertyVo(ServiceResource.PAY_AMT, ""+PayAmt)); // add PayAmt by hardik_kanak 26-11-2020
        Log.d("feesrequest", arrayList.toString());

        //new service call here, API name = "MAKEPAYMENT_NEW" by hardik_kanak 26-11-2020
        new AsynsTaskClass(mContext, arrayList, true, PaymentActivity.this).execute(ServiceResource.MAKEPAYMENT_NEW, ServiceResource.LOGIN_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (result != null && !result.equals("") && !result.equals("null")) {

            Wv_page.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);

            Wv_page.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.d("getURL", url + "--" + view.getUrl() + "--" + view.getOriginalUrl());
//                    view.loadUrl(url);
//                    view.getUrl().contains("http://erporataro.orataro.com//login") || view.getUrl().contains("http://erporataro.orataro.com/login/login")
//                            ||
                    if (view.getUrl().equalsIgnoreCase("http://erporataro.orataro.com//login")) {
                        Intent i = new Intent(mContext,
                                HomeWorkFragmentActivity.class);
                        i.putExtra("position", "Fee Receipt");
                        mContext.startActivity(i);
                        finish();
                    } else if (view.getUrl().equalsIgnoreCase("http://erporataro.orataro.com/login/login")) {
                        Intent i = new Intent(mContext,
                                HomeWorkFragmentActivity.class);
                        i.putExtra("position", "Fee Receipt");
                        mContext.startActivity(i);
                        finish();
                    } else if (url.contains("http://erporataro.orataro.com")) {
                        Intent i = new Intent(mContext,
                                HomeWorkFragmentActivity.class);
                        i.putExtra("position", "Fee Receipt");
                        mContext.startActivity(i);
                        finish();
                    }

                    return false;

                }

            });

            Wv_page.getSettings().setJavaScriptEnabled(true);
            Wv_page.loadUrl(result);
            Wv_page.getSettings().setBuiltInZoomControls(true);
            txt_nodatafound.setVisibility(View.GONE);
        } else {
            Wv_page.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            default:
                break;
        }

    }

}
