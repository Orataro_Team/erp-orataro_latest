package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewEventZoomDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.CalendarEventModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class EventsListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context context;
    private TextView homeworkListSubject;
    private ImageView list_image, iv_cl_detail_img;
    private TextView txt_month, txt_cl_date, txt_subject, txt_sub_detail;
    private TextView txt_date;
    private LinearLayout ll_view;
    private LinearLayout ll_date, ll_date_img;
    private ImageView img;
    private ArrayList<CalendarEventModel> calendarEventModels, copyEventList;
    private boolean isAddFirstTime = true;
    private static final int ID_DELETE = 6;
    private RefreshListner listner;
    public int posRemove;

    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    public EventsListAdapter(Context context, ArrayList<CalendarEventModel> calendarEventModels, RefreshListner refreshListner) {
        this.context = context;
        this.calendarEventModels = calendarEventModels;
        this.listner = refreshListner;
        copyEventList = new ArrayList<CalendarEventModel>();
        copyEventList.addAll(calendarEventModels);
    }

    @Override
    public int getCount() {
        return calendarEventModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.events_listraw, parent, false);
        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        iv_cl_detail_img = (ImageView) convertView.findViewById(R.id.iv_cl_detail_img);
        ImageView imgHoliday = (ImageView) convertView.findViewById(R.id.img_done);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        img = (ImageView) convertView.findViewById(R.id.iv_cl_detail_img);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        txt_cl_date = (TextView) convertView.findViewById(R.id.txt_cl_date);
        txt_subject = (TextView) convertView.findViewById(R.id.txt_subject);
        txt_sub_detail = (TextView) convertView.findViewById(R.id.txt_sub_detail);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        convertView.setBackgroundColor(colors[position % colors.length]);
        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);
        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);
        if (calendarEventModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        img.setVisibility(View.GONE);

        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);


        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            if (Utility.ReadWriteSetting(ServiceResource.EVENT).getIsDelete()) {
                ll_editdelete.setVisibility(View.VISIBLE);

                if (calendarEventModels.get(position).getType().equalsIgnoreCase("Holiday")) {

                    Log.d("getType", calendarEventModels.get(position).getType());
                    ll_editdelete.setVisibility(View.GONE);

                } else if (calendarEventModels.get(position).getType().equalsIgnoreCase("Event")) {

                    Log.d("getEvent", calendarEventModels.get(position).getType());
                    ll_editdelete.setVisibility(View.VISIBLE);

                }
            } else {
                ll_editdelete.setVisibility(View.GONE);
            }
        } else {

            if (calendarEventModels.get(position).getCreateBy().equalsIgnoreCase(new UserSharedPrefrence(context).getLoginModel().getUserID())) {

                ll_editdelete.setVisibility(View.VISIBLE);

            } else {

                ll_editdelete.setVisibility(View.GONE);

            }

        }


        final QuickAction quickActionForEditOrDelete;
        ActionItem actionDelete;
        actionDelete = new ActionItem(ID_DELETE, "Delete", context.getResources().getDrawable(R.drawable.delete));
        quickActionForEditOrDelete = new QuickAction(context, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos,
                                    int actionId) {

                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_DELETE) {

                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    Utility.deleteDialog(context, context.getResources().getString(R.string.Events), calendarEventModels.get(deletepos).getCl_subject(), new Popup() {

                        @Override
                        public void deleteYes() {

                            posRemove = position;
                            deleteEvent(calendarEventModels.get(position).getActivityID());

                        }

                        @Override
                        public void deleteNo() {

                        }

                    });

                }

            }

        });

        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickActionForEditOrDelete.show(v);
            }
            
        });


        txt_cl_date.setText(calendarEventModels.get(position).getCl_date());
        txt_subject.setText(calendarEventModels.get(position).getCl_subject());
        txt_sub_detail.setText(calendarEventModels.get(position).getCl_detail_txt());
        if (calendarEventModels.get(position).getCl_detail_txt() != null) {

            if (calendarEventModels.get(position).getCl_detail_txt().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = calendarEventModels.get(position)
                        .getCl_detail_txt()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(context, ViewEventZoomDetailsActivity.class);
                        intent.putExtra("Evname", calendarEventModels.get(position).getCl_subject());
                        intent.putExtra("Evdetails", calendarEventModels.get(position).getCl_detail_img());
                        intent.putExtra("Evdetails_txt", calendarEventModels.get(position).getCl_detail_txt());
                        intent.putExtra("referencelink", calendarEventModels.get(position).getReferenceLink());
                        context.startActivity(intent);
                    }
                };

                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                txt_sub_detail.setMovementMethod(LinkMovementMethod.getInstance());
                txt_sub_detail.setText(text, BufferType.SPANNABLE);
            } else {
                txt_sub_detail.setText(calendarEventModels.get(position).getCl_detail_txt());
            }
        }

        txt_month.setText(calendarEventModels.get(position).getMonth());
        txt_date.setText(calendarEventModels.get(position).getYear());
        iv_cl_detail_img.setImageResource(calendarEventModels.get(position).getCl_detail_img());

        txt_sub_detail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewEventZoomDetailsActivity.class);
                intent.putExtra("Evname", calendarEventModels.get(position).getCl_subject());
                intent.putExtra("Evdetails", calendarEventModels.get(position).getCl_detail_img());
                intent.putExtra("Evdetails_txt", calendarEventModels.get(position).getCl_detail_txt());
                intent.putExtra("referencelink", calendarEventModels.get(position).getReferenceLink());
                context.startActivity(intent);

            }

        });

        if (calendarEventModels.get(position).getType().equalsIgnoreCase("Holiday")) {
            imgHoliday.setImageResource(R.drawable.notify_red);
        } else if (calendarEventModels.get(position).getType().equalsIgnoreCase("Exam")) {
            imgHoliday.setImageResource(R.drawable.exam);
        } else {
            imgHoliday.setImageResource(R.drawable.notify);
        }

        ll_date_img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ViewEventZoomDetailsActivity.class);
                intent.putExtra("Evname", calendarEventModels.get(position).getCl_subject());
                intent.putExtra("Evdetails_txt", calendarEventModels.get(position).getCl_detail_txt());
                intent.putExtra("referencelink", calendarEventModels.get(position).getReferenceLink());
                context.startActivity(intent);

            }

        });

        return convertView;
    }

    public void fileter(String type, boolean isAdd, boolean isAllFalse) {
        if (isAdd) {
            if (isAddFirstTime) {
                calendarEventModels.clear();
                isAddFirstTime = false;
            }
            for (int i = 0; i < copyEventList.size(); i++) {
                if (copyEventList.get(i).getType().equalsIgnoreCase(type)) {
                    calendarEventModels.add(copyEventList.get(i));
                }
            }
        } else {
            for (int i = 0; i < calendarEventModels.size(); i++) {
                if (calendarEventModels.get(i).getType().equalsIgnoreCase(type)) {
                    calendarEventModels.remove(i);
                    i = i - 1;
                }
            }
        }

        if (calendarEventModels.size() == 0) {
            calendarEventModels.clear();
            calendarEventModels = copyEventList;
        }
        if (calendarEventModels != null && calendarEventModels.size() > 0) {
            Collections.sort(calendarEventModels, new SortedDate());
            Collections.reverse(calendarEventModels);

            for (int j = 0; j < calendarEventModels.size(); j++) {
                if (j != 0) {
                    String temp = calendarEventModels.get(j - 1).getMonth();
                    if (temp.equalsIgnoreCase(calendarEventModels.get(j).getMonth())) {
                        calendarEventModels.get(j).setVisibleMonth(false);
                    } else {
                        calendarEventModels.get(j).setVisibleMonth(true);
                    }
                } else {
                    calendarEventModels.get(j).setVisibleMonth(true);
                }

            }
        }

        this.notifyDataSetChanged();
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.DELETE_EVENT.equalsIgnoreCase(methodName)) {

            listner.refresh(methodName);
            Utility.showToast(context.getResources().getString(R.string.eventDeletesmessage), context);

        }

    }

    public class SortedDate implements Comparator<CalendarEventModel> {
        @Override
        public int compare(CalendarEventModel o1, CalendarEventModel o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    }

    public void deleteEvent(String eventId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.EVENTID, eventId));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.DELETE_EVENT, ServiceResource.EVENT_URL);

    }

}
