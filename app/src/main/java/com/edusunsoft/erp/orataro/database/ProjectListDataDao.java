package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ProjectListDataDao {

    @Insert
    void insertProjectList(ProjectListModel projectListModel);

    @Query("SELECT * from ProjectList")
    List<ProjectListModel> getProjectList();

    @Query("DELETE  FROM ProjectList")
    int deleteProjectList();

    @Query("delete from ProjectList where ProjectID=:projectid")
    void deleteProjectByProjectID(String projectid);

}
