
package com.edusunsoft.erp.orataro.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesStructureModel implements Parcelable {

    @SerializedName("FeesHeadID")
    @Expose
    private String feesHeadID;
    @SerializedName("StructuteName")
    @Expose
    private String structuteName;
    @SerializedName("FeesStructuteID")
    @Expose
    private String feesStructuteID;
    @SerializedName("FeesStructureScheduleID")
    @Expose
    private String feesStructureScheduleID;
    @SerializedName("AmountToBePaid")
    @Expose
    private String amountToBePaid;
    @SerializedName("OriginalStructureHeadAmount")
    @Expose
    private String originalStructureHeadAmount;
    @SerializedName("FeeHeadName")
    @Expose
    private String feeHeadName;
    @SerializedName("TotalDueAmount")
    @Expose
    private String totalDueAmount;
    @SerializedName("StudentFeesCollectionID")
    @Expose
    private String studentFeesCollectionID;
    @SerializedName("ExemptionInPercentage")
    @Expose
    private String exemptionInPercentage;
    @SerializedName("IsFeeExemptional")
    @Expose
    private String isFeeExemptional;
    @SerializedName("IsCompulsoryFees")
    @Expose
    private String isCompulsoryFees;
    @SerializedName("StudentFeesCollectionDetailID")
    @Expose
    private String studentFeesCollectionDetailID;
    @SerializedName("TotalDue")
    @Expose
    private String totalDue;

    public final static Parcelable.Creator<FeesStructureModel> CREATOR = new Creator<FeesStructureModel>() {
        public FeesStructureModel createFromParcel(Parcel in) {
            FeesStructureModel instance = new FeesStructureModel();
            instance.feesHeadID = ((String) in.readValue((String.class.getClassLoader())));
            instance.structuteName = ((String) in.readValue((String.class.getClassLoader())));
            instance.feesStructuteID = ((String) in.readValue((String.class.getClassLoader())));
            instance.feesStructureScheduleID = ((String) in.readValue((String.class.getClassLoader())));
            instance.amountToBePaid = ((String) in.readValue((String.class.getClassLoader())));
            instance.originalStructureHeadAmount = ((String) in.readValue((String.class.getClassLoader())));
            instance.feeHeadName = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalDueAmount = ((String) in.readValue((String.class.getClassLoader())));
            instance.studentFeesCollectionID = ((String) in.readValue((String.class.getClassLoader())));
            instance.exemptionInPercentage = ((String) in.readValue((String.class.getClassLoader())));
            instance.isFeeExemptional = ((String) in.readValue((String.class.getClassLoader())));
            instance.isCompulsoryFees = ((String) in.readValue((String.class.getClassLoader())));
            instance.studentFeesCollectionDetailID = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalDue = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public FeesStructureModel[] newArray(int size) {
            return (new FeesStructureModel[size]);
        }

    };

    public String getFeesHeadID() {
        return feesHeadID;
    }

    public void setFeesHeadID(String feesHeadID) {
        this.feesHeadID = feesHeadID;
    }

    public String getStructuteName() {
        return structuteName;
    }

    public void setStructuteName(String structuteName) {
        this.structuteName = structuteName;
    }

    public String getFeesStructuteID() {
        return feesStructuteID;
    }

    public void setFeesStructuteID(String feesStructuteID) {
        this.feesStructuteID = feesStructuteID;
    }

    public String getFeesStructureScheduleID() {
        return feesStructureScheduleID;
    }

    public void setFeesStructureScheduleID(String feesStructureScheduleID) {
        this.feesStructureScheduleID = feesStructureScheduleID;
    }

    public String getAmountToBePaid() {
        return amountToBePaid;
    }

    public void setAmountToBePaid(String amountToBePaid) {
        this.amountToBePaid = amountToBePaid;
    }

    public String getOriginalStructureHeadAmount() {
        return originalStructureHeadAmount;
    }

    public void setOriginalStructureHeadAmount(String originalStructureHeadAmount) {
        this.originalStructureHeadAmount = originalStructureHeadAmount;
    }

    public String getFeeHeadName() {
        return feeHeadName;
    }

    public void setFeeHeadName(String feeHeadName) {
        this.feeHeadName = feeHeadName;
    }

    public String getTotalDueAmount() {
        return totalDueAmount;
    }

    public void setTotalDueAmount(String totalDueAmount) {
        this.totalDueAmount = totalDueAmount;
    }

    public String getStudentFeesCollectionID() {
        return studentFeesCollectionID;
    }

    public void setStudentFeesCollectionID(String studentFeesCollectionID) {
        this.studentFeesCollectionID = studentFeesCollectionID;
    }

    public String getExemptionInPercentage() {
        return exemptionInPercentage;
    }

    public void setExemptionInPercentage(String exemptionInPercentage) {
        this.exemptionInPercentage = exemptionInPercentage;
    }

    public String getIsFeeExemptional() {
        return isFeeExemptional;
    }

    public void setIsFeeExemptional(String isFeeExemptional) {
        this.isFeeExemptional = isFeeExemptional;
    }

    public String getIsCompulsoryFees() {
        return isCompulsoryFees;
    }

    public void setIsCompulsoryFees(String isCompulsoryFees) {
        this.isCompulsoryFees = isCompulsoryFees;
    }

    public String getStudentFeesCollectionDetailID() {
        return studentFeesCollectionDetailID;
    }

    public void setStudentFeesCollectionDetailID(String studentFeesCollectionDetailID) {
        this.studentFeesCollectionDetailID = studentFeesCollectionDetailID;
    }

    public String getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(String totalDue) {
        this.totalDue = totalDue;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(feesHeadID);
        dest.writeValue(structuteName);
        dest.writeValue(feesStructuteID);
        dest.writeValue(feesStructureScheduleID);
        dest.writeValue(amountToBePaid);
        dest.writeValue(originalStructureHeadAmount);
        dest.writeValue(feeHeadName);
        dest.writeValue(totalDueAmount);
        dest.writeValue(studentFeesCollectionID);
        dest.writeValue(exemptionInPercentage);
        dest.writeValue(isFeeExemptional);
        dest.writeValue(isCompulsoryFees);
        dest.writeValue(studentFeesCollectionDetailID);
        dest.writeValue(totalDue);
    }

    public int describeContents() {
        return 0;
    }

}