package com.edusunsoft.erp.orataro.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ImageSelectionActivity;
import com.edusunsoft.erp.orataro.activities.PreviewImageActivity;
import com.edusunsoft.erp.orataro.adapter.PhotoAdapter;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PhotoListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class PhotoFragment extends Fragment implements ResponseWebServices {

    ImageView imgLeftMenu, imgRightMenu;
    TextView txtHeader;

    private String str_new_photo;
    GridView gv_photo;
    // private ArrayList<PhotoModel> photoModels;
    // private PhotoModel photoModel;
    private PhotoAdapter photo_ListAdapter;
    private LinearLayout ll_add_new;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private String fielPath = "";
    private byte[] byteArray = null;
    TextView txt_nodatafound;
    // public ArrayList<PhotoModel> albumModels;

    public ArrayList<PhotoListModel> photoListModels;
    PhotoListModel photoListModel;
    PhotoListModel photoListModel_new;
    private Uri fileUri;
    String from;
    public static String FROM = "isFrom";
    //	@Override
    //	public void onSaveInstanceState(Bundle outState) {
    //		// TODO Auto-generated method stub
    //		super.onSaveInstanceState(outState);
    //		outState.putString("file_uri", fileUri.toString());
    //		outState.putString("wallId", new UserSharedPrefrence(getActivity()).getLoginModel().getCurrentWallId());
    //	}

    public static final PhotoFragment newInstance(String from) {
        PhotoFragment f = new PhotoFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(FROM, from);
        f.setArguments(bdl);
        return f;
    }
    //public PhotoFragment(String from) {
//		this.from = from;
//	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.photofragment, container, false);
        from = getArguments().getString(FROM);

        imgLeftMenu = (ImageView) v.findViewById(R.id.img_home);
        imgRightMenu = (ImageView) v.findViewById(R.id.img_menu);
        txtHeader = (TextView) v.findViewById(R.id.header_text);
        gv_photo = (GridView) v.findViewById(R.id.gv_photo);
        ll_add_new = (LinearLayout) v.findViewById(R.id.ll_add_new);


        //		if(from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)){

        if (!new UserSharedPrefrence(getActivity()).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
            ll_add_new.setVisibility(View.INVISIBLE);
        }


        imgLeftMenu.setImageResource(R.drawable.back);
        imgRightMenu.setVisibility(View.INVISIBLE);
        try {
            txtHeader.setText(getResources().getString(R.string.Photos) + " (" + Utility.GetFirstName(getActivity()) + ")");
        } catch (Exception e) {

        }
        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                selectImage();
            }
        });

        // AddFriendList();

        if (Utility.isNetworkAvailable(getActivity())) {
            String result = Utility.readFromFile(ServiceResource.GET_PHOTO_METHODNAME, getActivity());
            if (result.equalsIgnoreCase("")) {
                albumList(true);
            } else {
                albumList(false);
            }
            parsePhotoList(result);
        } else {
            String result = Utility.readFromFile(ServiceResource.GET_PHOTO_METHODNAME, getActivity());
            parsePhotoList(result);

        }
        return v;
    }

    private void selectImage() {

        final CharSequence[] items = {getActivity().getResources().getString(R.string.takephoto),
                getActivity().getResources().getString(R.string.ChoosefromLibrary),
                getActivity().getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(getActivity().getResources().getString(R.string.takephoto))) {
                    //					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //
                    //					fileUri = Utility.getOutputMediaFileUri(ServiceResource.MEDIA_TYPE_IMAGE,getActivity());
                    //
                    //					intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    //
                    //					// start the image capture Intent
                    //					startActivityForResult(intent, REQUEST_CAMERA);
                    Intent intent = new Intent(getActivity(), PreviewImageActivity.class);
                    intent.putExtra("FromIntent", "true");
                    intent.putExtra("RequestCode", 100);
                    startActivityForResult(intent, REQUEST_CAMERA);

                    //					Intent intent = new Intent(getActivity(),CameraActivity.class);
                    //					startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(getActivity().getResources().getString(R.string.ChoosefromLibrary))) {
                    Intent intent = new Intent(getActivity(), ImageSelectionActivity.class);
                    intent.putExtra("count", 1);
                    startActivityForResult(intent, SELECT_FILE);
                    //					Intent intent = new Intent(
                    //							Intent.ACTION_PICK,
                    //							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //					intent.setType("image/*");
                    //					startActivityForResult(
                    //							Intent.createChooser(intent, "Select File"),
                    //							SELECT_FILE);
                } else if (items[item].equals(getActivity().getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Utility.getUserModelData(getActivity());


                try {

                    fielPath = data.getExtras().getString("imageData_uri");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                photoListModel_new = new PhotoListModel();
                photoListModel_new.setPhoto(fielPath);
                photoListModels.add(photoListModel_new);
                Log.d("photoadded", photoListModels.toString());
                photo_ListAdapter = new PhotoAdapter(getActivity(), photoListModels, 1);
                gv_photo.setAdapter(photo_ListAdapter);

                /*commented By Krishna -- 06-02-2019 : Get Bytearray From Captured ImagePAth from Camera*/

                byteArray = convertImageToByteArra(fielPath);
//                GetByteArrayFromFilePathofImage(filePath);

                /*END*/


                if (byteArray != null) {

                    uploadPhoto();

                }


            } else if (requestCode == SELECT_FILE) {

                if (data != null) {

                    ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");

                    //				Toast.makeText(getActivity(), ""+imgList.size(), 1).show();
                    if (imgList != null && imgList.size() > 0) {

                        fielPath = imgList.get(0).getUri().toString();
                        byteArray = convertImageToByteArra(fielPath);

                        photoListModel_new = new PhotoListModel();
                        photoListModel_new.setPhoto(fielPath);
                        photoListModels.add(photoListModel_new);

                        Log.d("photomodels", photoListModels.toString());
                        photo_ListAdapter = new PhotoAdapter(getActivity(), photoListModels, 1);
                        gv_photo.setAdapter(photo_ListAdapter);

                        if (byteArray != null) {

                            uploadPhoto();

                        }

                    }


                }

            }

        }

    }

    public byte[] convertImageToByteArra(String filePath) {

        byte[] b = null;
        Bitmap thePic;
        thePic = Utility.getBitmap(filePath, getActivity());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (filePath.contains(".png") || filePath.contains(".PNG")) {
            thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else {
            thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }

        b = null;
        b = stream.toByteArray();

        return b;

    }


    public void albumList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_POST_WALLID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        new AsynsTaskClass(getActivity(), arrayList, isViewPopup, this).execute(ServiceResource.GET_PHOTO_METHODNAME,
                ServiceResource.PHOTO_URL);

    }

    public void postPhoto() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_INSTITUTEID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_CLIENTID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_WALLID, new UserSharedPrefrence(getActivity()).getLoginModel()
                .getCurrentWallId()));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_MEMBERID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_USERID, new UserSharedPrefrence(getActivity()).getLoginModel()
                .getUserID()));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_BEACHID, null));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_POSTSHARETYPE, "PUBLIC"));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_IMAGEPATH, str_new_photo));
        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_POST_FILETYPE, "Image"));

        new AsynsTaskClass(getActivity(), arrayList, true, this).execute(ServiceResource.ADDPHOTOS_METHODNAME,
                ServiceResource.PHOTO_URL);
    }

    public void uploadPhoto() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
                new File(fielPath).getName()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getClientID()));

        arrayList.add(new PropertyVo(
                ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(getActivity()).getLoginModel()
                .getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID,
                new UserSharedPrefrence(getActivity()).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE,
                "IMAGE"));
        if (byteArray != null) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE,
                    byteArray));
            new AsynsTaskClass(getActivity(), arrayList, true, this)
                    .execute(ServiceResource.UPLOAD_PHOTO_METHODNAME, ServiceResource.ADDPHOTO_URL);
        }

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.GET_PHOTO_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, getActivity());
            parsePhotoList(result);
        }
        if (ServiceResource.ADDPHOTOS_METHODNAME.equalsIgnoreCase(methodName)) {
            albumList(false);
        }
        if (ServiceResource.UPLOAD_PHOTO_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("uploadPhotoresult", result.toString());
            str_new_photo = result.substring(13, result.length())
                    .split(" ")[1].split("\",\"")[0];
            if (result != null && !result.isEmpty()) {
                postPhoto();
            }
        }
    }


    public void parsePhotoList(String result) {

        Log.d("photoalbumResult", result);
        photoListModels = new ArrayList<PhotoListModel>();
        JSONArray jsonObj;

        try {

            jsonObj = new JSONArray(result);
            // JSONArray detailArrray= jsonObj.getJSONArray("");

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                PhotoListModel model = new PhotoListModel();
                //					model.setPostCommentID(innerObj.getString("PostCommentID"));
                model.setAlbumPhotosID("AlbumPhotosID");
                model.setPhoto(innerObj
                        .getString(ServiceResource.ALBUM_PHOTO));

                if (model.getPhoto() != null
                        && !model.getPhoto().equals("")) {
                    model.setPhoto(ServiceResource.BASE_IMG_URL
                            + "DataFiles/" + model.getPhoto());
                }

                photoListModels.add(model);
                Log.d("photolistmodels", photoListModels.toString());

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        if (photoListModels != null && photoListModels.size() > 0) {

            PhotoAdapter adapter = new PhotoAdapter(getActivity(),
                    photoListModels, 1);
            gv_photo.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            gv_photo.setVisibility(View.VISIBLE);

        } else {

            gv_photo.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoPhotoAlbumAvailable));

        }


    }


}
