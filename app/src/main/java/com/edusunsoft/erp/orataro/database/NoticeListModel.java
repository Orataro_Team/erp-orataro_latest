package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "NoticeList")
public class NoticeListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "NotesID")
    @SerializedName("NotesID")
    public String NotesID;

    @ColumnInfo(name = "GradeID")
    @SerializedName("GradeID")
    public String GradeID;

    @ColumnInfo(name = "DivisionID")
    @SerializedName("DivisionID")
    public String DivisionID;

    @ColumnInfo(name = "SubjectID")
    @SerializedName("SubjectID")
    public String SubjectID;

    @ColumnInfo(name = "SubjectName")
    @SerializedName("SubjectName")
    public String SubjectName;

    @ColumnInfo(name = "DateOfNotes")
    @SerializedName("DateOfNotes")
    public String DateOfNotes;

    @ColumnInfo(name = "NoteTitle")
    @SerializedName("NoteTitle")
    public String NoteTitle;

    @ColumnInfo(name = "NoteDetails")
    @SerializedName("NoteDetails")
    public String NoteDetails;

    @ColumnInfo(name = "ActionStartDate")
    @SerializedName("ActionStartDate")
    public String ActionStartDate;

    @ColumnInfo(name = "ActionEndDate")
    @SerializedName("ActionEndDate")
    public String ActionEndDate;

    @ColumnInfo(name = "DressCode")
    @SerializedName("DressCode")
    public String DressCode;

    @ColumnInfo(name = "IsRead")
    @SerializedName("IsRead")
    public boolean IsRead;

    @ColumnInfo(name = "IamReady")
    @SerializedName("IamReady")
    public boolean IamReady;

    @ColumnInfo(name = "ParentNote")
    @SerializedName("ParentNote")
    public String ParentNote;

    @ColumnInfo(name = "Rating")
    @SerializedName("Rating")
    public String Rating;

    @ColumnInfo(name = "UserName")
    @SerializedName("UserName")
    public String UserName;

    @ColumnInfo(name = "ProfilePicture")
    @SerializedName("ProfilePicture")
    public String ProfilePicture;

    @ColumnInfo(name = "isVisible")
    @SerializedName("isVisible")
    public boolean isVisible;

    @ColumnInfo(name = "isCheck")
    @SerializedName("isCheck")
    public boolean isCheck;

    @ColumnInfo(name = "imgUrl")
    @SerializedName("imgUrl")
    public String  imgUrl;

    @ColumnInfo(name = "NoteDate")
    @SerializedName("NoteDate")
    public String  NoteDate;

    @ColumnInfo(name = "strActionStartDate")
    @SerializedName("strActionStartDate")
    public String  strActionStartDate;

    @ColumnInfo(name = "ReferenceLink")
    @SerializedName("ReferenceLink")
    public String  ReferenceLink;

    @ColumnInfo(name = "month")
    @SerializedName("month")
    public String  month;

    @ColumnInfo(name = "year")
    @SerializedName("year")
    public String  year;

    @ColumnInfo(name = "day")
    @SerializedName("day")
    public String  day;

    @ColumnInfo(name = "MilliSecond")
    @SerializedName("MilliSecond")
    public String  MilliSecond;


    public NoticeListModel() {
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMilliSecond() {
        return MilliSecond;
    }

    public void setMilliSecond(String milliSecond) {
        MilliSecond = milliSecond;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotesID() {
        return NotesID;
    }

    public void setNotesID(String notesID) {
        NotesID = notesID;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getDateOfNotes() {
        return DateOfNotes;
    }

    public void setDateOfNotes(String dateOfNotes) {
        DateOfNotes = dateOfNotes;
    }

    public String getNoteTitle() {
        return NoteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        NoteTitle = noteTitle;
    }

    public String getNoteDetails() {
        return NoteDetails;
    }

    public void setNoteDetails(String noteDetails) {
        NoteDetails = noteDetails;
    }

    public String getActionStartDate() {
        return ActionStartDate;
    }

    public void setActionStartDate(String actionStartDate) {
        ActionStartDate = actionStartDate;
    }

    public String getActionEndDate() {
        return ActionEndDate;
    }

    public void setActionEndDate(String actionEndDate) {
        ActionEndDate = actionEndDate;
    }

    public String getDressCode() {
        return DressCode;
    }

    public void setDressCode(String dressCode) {
        DressCode = dressCode;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    public boolean isIamReady() {
        return IamReady;
    }

    public void setIamReady(boolean iamReady) {
        IamReady = iamReady;
    }

    public String getParentNote() {
        return ParentNote;
    }

    public void setParentNote(String parentNote) {
        ParentNote = parentNote;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getNoteDate() {
        return NoteDate;
    }

    public void setNoteDate(String noteDate) {
        NoteDate = noteDate;
    }

    public String getStrActionStartDate() {
        return strActionStartDate;
    }

    public void setStrActionStartDate(String strActionStartDate) {
        this.strActionStartDate = strActionStartDate;
    }

    public String getReferenceLink() {
        return ReferenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        ReferenceLink = referenceLink;
    }

    @Override
    public String toString() {
        return "NoticeListModel{" +
                "id=" + id +
                ", NotesID='" + NotesID + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", SubjectID=" + SubjectID +
                ", SubjectName='" + SubjectName + '\'' +
                ", DateOfNotes='" + DateOfNotes + '\'' +
                ", NoteTitle='" + NoteTitle + '\'' +
                ", NoteDetails='" + NoteDetails + '\'' +
                ", ActionStartDate='" + ActionStartDate + '\'' +
                ", ActionEndDate='" + ActionEndDate + '\'' +
                ", DressCode='" + DressCode + '\'' +
                ", IsRead=" + IsRead +
                ", IamReady=" + IamReady +
                ", ParentNote='" + ParentNote + '\'' +
                ", Rating='" + Rating + '\'' +
                ", UserName=" + UserName +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", isVisible=" + isVisible +
                ", isCheck=" + isCheck +
                ", imgUrl='" + imgUrl + '\'' +
                ", NoteDate='" + NoteDate + '\'' +
                ", strActionStartDate='" + strActionStartDate + '\'' +
                ", ReferenceLink='" + ReferenceLink + '\'' +
                '}';
    }
}
