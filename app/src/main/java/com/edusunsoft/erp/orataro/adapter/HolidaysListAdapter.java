package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.HolidaysModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HolidaysListAdapter extends BaseAdapter {
    private LayoutInflater layoutInfalater;
    Context context;
    TextView tv_date, tv_day, tv_festival_name, tv_end_date;

    List<HolidaysModel> holidaysModels, copyList;

    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    public HolidaysListAdapter(Context context,
                               List<HolidaysModel> holidaysModels) {

        this.context = context;
        this.holidaysModels = holidaysModels;
        copyList = new ArrayList<HolidaysModel>();
        copyList.addAll(holidaysModels);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return holidaysModels.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.holiday_listraw, parent, false);
        convertView.setBackgroundColor(colors[position % colors.length]);
        tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_end_date = (TextView) convertView.findViewById(R.id.tv_end_date);
        tv_day = (TextView) convertView.findViewById(R.id.tv_day);
        tv_festival_name = (TextView) convertView.findViewById(R.id.tv_festival_name);

        tv_day.setText(holidaysModels.get(position).getNoOfDay() + "");
        tv_festival_name.setText(holidaysModels.get(position).getHolidayTitle());
        tv_date.setText(holidaysModels.get(position).getDateOfHoliday());
        tv_end_date.setText(holidaysModels.get(position).getEndDateOfHoliday());

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        holidaysModels.clear();
        if (charText.length() == 0) {
            holidaysModels.addAll(copyList);

        } else {

            for (HolidaysModel vo : copyList) {

                if (vo.getHolidayTitle().toLowerCase(Locale.getDefault()).contains(charText)) {

                    holidaysModels.add(vo);

                }
            }
        }

        this.notifyDataSetChanged();

    }
}
