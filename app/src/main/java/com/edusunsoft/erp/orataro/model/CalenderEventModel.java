package com.edusunsoft.erp.orataro.model;

public class CalenderEventModel {
	
	String activityId,title,activityDetails,start,end,className,type,color;

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getActivityDetails() {
		return activityDetails;
	}

	public void setActivityDetails(String activityDetails) {
		this.activityDetails = activityDetails;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "CalenderEventModel{" +
				"activityId='" + activityId + '\'' +
				", title='" + title + '\'' +
				", activityDetails='" + activityDetails + '\'' +
				", start='" + start + '\'' +
				", end='" + end + '\'' +
				", className='" + className + '\'' +
				", type='" + type + '\'' +
				", color='" + color + '\'' +
				'}';
	}
}
