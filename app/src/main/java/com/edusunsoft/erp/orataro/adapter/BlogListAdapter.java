package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.CommentActivity;
import com.edusunsoft.erp.orataro.activities.SinglePostActivity;
import com.edusunsoft.erp.orataro.activities.ZoomImageAcitivity;
import com.edusunsoft.erp.orataro.model.BlogModel;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class BlogListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater layoutInfalater;
	private ImageView iv_fb;
	CircleImageView profilePicUser;
	private TextView userName, timeBeforepost, postContent;
	private LinearLayout ll_comment;
	private LinearLayout layoutShowHide;
	private ArrayList<BlogModel> blogModels;
	private boolean hideCommentLayout = true;

	public BlogListAdapter(Context context, ArrayList<BlogModel> blogModels) {
		this.mContext = context;
		this.blogModels = blogModels;
	}

	@Override
	public int getCount() {
		return blogModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.whats_on_mind_listraw, parent, false);

		profilePicUser = (CircleImageView) convertView.findViewById(R.id.profilePicUser);
		iv_fb = (ImageView) convertView.findViewById(R.id.iv_fb);
		ll_comment = (LinearLayout) convertView.findViewById(R.id.ll_comment);
		layoutShowHide = (LinearLayout) convertView.findViewById(R.id.commentLayouthideshow);
		userName = (TextView) convertView.findViewById(R.id.nameUser);
		timeBeforepost = (TextView) convertView.findViewById(R.id.timebeforepost);
		postContent = (TextView) convertView.findViewById(R.id.txt_sub_detail);

		if (blogModels.get(position).isAvilablepostImg()) {
			iv_fb.setVisibility(View.VISIBLE);
		} else {
			iv_fb.setVisibility(View.GONE);
		}

		if (blogModels.get(position).isAvailPostContent()) {
			postContent.setVisibility(View.VISIBLE);
		} else {
			postContent.setVisibility(View.GONE);
		}

		if (blogModels.get(position).getPersonName() != null) {
			userName.setText(blogModels.get(position).getPersonName());
		}

		if (blogModels.get(position).getTimeBeforePost() != null) {
			timeBeforepost.setText(blogModels.get(position).getTimeBeforePost());
		}

		if (blogModels.get(position).getPostContent() != null) {
			if (blogModels.get(position).getPostContent().length() > Constants.CONTINUEREADINGSIZE) {
				String continueReadingStr = Constants.CONTINUEREADINGSTR;
				String tempText = blogModels.get(position).getPostContent()
						.substring(0, Constants.CONTINUEREADINGSIZE)
						+ continueReadingStr;

				SpannableString text = new SpannableString(tempText);

				text.setSpan(
						new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR),
						Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);

				MyClickableSpan clickfor = new MyClickableSpan(
						tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

					@Override
					public void onClick(View widget) {

						Intent i = new Intent(mContext, SinglePostActivity.class);
						i.putExtra("profilepic", blogModels.get(position).getProfilePicImg());
						i.putExtra("username", blogModels.get(position).getPersonName());
						i.putExtra("imgurl", blogModels.get(position).getPostImg());
						i.putExtra("postcontent", blogModels.get(position).getPostContent());
						mContext.startActivity(i);
					}
				};
				text.setSpan(clickfor, Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);

				postContent.setMovementMethod(LinkMovementMethod.getInstance());
				postContent.setText(text, BufferType.SPANNABLE);
			} else {
				postContent.setText(blogModels.get(position).getPostContent());
			}

		}

		if (blogModels.get(position).getPostImg() != null) {
			iv_fb.setImageBitmap(Utility.getBitmapFromAsset(mContext, blogModels.get(position).getPostImg()));
		}
		if (blogModels.get(position).getProfilePicImg() != null) {
			profilePicUser.setImageBitmap(Utility.getBitmapFromAsset(mContext, blogModels.get(position).getProfilePicImg()));
		}

		ll_comment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(mContext, CommentActivity.class);
				i.putExtra("from", "");
				mContext.startActivity(i);

			}
		});

		iv_fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent in = new Intent(mContext, ZoomImageAcitivity.class);
				in.putExtra("img", blogModels.get(position).getPostImg());
				in.putExtra("name", blogModels.get(position).getPersonName());
				mContext.startActivity(in);
			}
		});

		postContent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(mContext, SinglePostActivity.class);
				i.putExtra("profilepic", blogModels.get(position).getProfilePicImg());
				i.putExtra("username", blogModels.get(position).getPersonName());
				i.putExtra("imgurl", blogModels.get(position).getPostImg());
				i.putExtra("postcontent", blogModels.get(position).getPostContent());
				mContext.startActivity(i);
			}
		});
		return convertView;

	}

	public void toast(String msg) {

		Utility.toast(mContext,"" + msg);

	}

}
