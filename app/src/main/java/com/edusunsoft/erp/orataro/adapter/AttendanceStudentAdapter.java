package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.HashMapModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

public class AttendanceStudentAdapter extends BaseAdapter {

	private Context c ;
	private ArrayList<AtteandanceModel> atteandanceModel;
	private LayoutInflater layoutInfalater;
	private String date;

	public AttendanceStudentAdapter(Context c, ArrayList<AtteandanceModel> atteandanceModel, String date) {
		this.c = c; 
		this.atteandanceModel = atteandanceModel;
		this.date= date;
	}

	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInfalater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.textview, parent, false);

		TextView txtStanderd = (TextView) convertView.findViewById(R.id.txtStanderd);
		LinearLayout ll_attendance = (LinearLayout) convertView.findViewById(R.id.ll_attendance);
		txtStanderd.setText(new UserSharedPrefrence(c).getLoginModel().getFullName());

		String[] dateArray =date.split("-");

		for(int i = 0; i<getNumberOfDays(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1])-1); i++){
			View v = layoutInfalater.inflate(R.layout.attendancelistraw, null);
			TextView txtName = (TextView) v.findViewById(R.id.txtName);
			final RadioButton rbpresent=(RadioButton) v.findViewById(R.id.rbpresent);
			final RadioButton rbabsant=(RadioButton) v.findViewById(R.id.rbabsant);
			final RadioButton rbsikleave=(RadioButton) v.findViewById(R.id.rbsikleave);
			final RadioButton rbLeave=(RadioButton) v.findViewById(R.id.rbLeave);

			txtName.setText((i+1)+"-"+dateArray[1]+"-"+dateArray[0]);
			if(isPresentFromdate((i+1)+"-"+dateArray[1]+"-"+dateArray[0]).equalsIgnoreCase(ServiceResource.PRESENT)){
				rbpresent.setChecked(true);
			}else if(isPresentFromdate((i+1)+"-"+dateArray[1]+"-"+dateArray[0]).equalsIgnoreCase(ServiceResource.ABSENT)){
				rbabsant.setChecked(true);
			}else if(isPresentFromdate((i+1)+"-"+dateArray[1]+"-"+dateArray[0]).equalsIgnoreCase(ServiceResource.LEAVE)){
				rbsikleave.setChecked(true);
			}else if(isPresentFromdate((i+1)+"-"+dateArray[1]+"-"+dateArray[0]).equalsIgnoreCase(ServiceResource.SICKLEAVE)){
				rbLeave.setChecked(true);
			}
			rbpresent.setEnabled(false);
			rbabsant.setEnabled(false);
			rbsikleave.setEnabled(false);
			rbLeave.setEnabled(false);
			
			ll_attendance.addView(v);
		}

		return convertView;
	}
	public class SortedDate implements Comparator<HashMapModel> {
		@Override
		public int compare(HashMapModel o1, HashMapModel o2) {
			return o1.getMillisceond().compareTo(o2.getMillisceond());
		}
	}

	public int getNumberOfDays(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		int numDays = calendar.getActualMaximum(Calendar.DATE);
		return numDays;
	}

	public int getMonth(String month) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(new SimpleDateFormat("MMM").parse(month));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int monthInt = cal.get(Calendar.MONTH) + 1;

		return monthInt;

	}

	public String isPresentFromdate(String d){
		String temp = "";
		for(int i = 0 ;i<atteandanceModel.size();i++){
			if(Utility.dateFormate(d, "yyyy/MM/dd", "dd-MM-yyyy").equalsIgnoreCase(atteandanceModel.get(i).getDateOfAttendence())){
				temp = atteandanceModel.get(i).getAttencenceType_Term();
			}
		}
		return temp;
	}
}
