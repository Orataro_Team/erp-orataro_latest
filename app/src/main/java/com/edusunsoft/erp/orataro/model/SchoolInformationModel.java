package com.edusunsoft.erp.orataro.model;

public class SchoolInformationModel {

	private String title;
	private String information;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

}
