package com.edusunsoft.erp.orataro.model;

public class PollOptionModel {

	private String PollOptionID,Option,PollID,totalVotes;

	public String getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(String totalVotes) {
		this.totalVotes = totalVotes;
	}

	public String getPollID() {
		return PollID;
	}

	public void setPollID(String pollID) {
		PollID = pollID;
	}

	public String getPollOptionID() {
		return PollOptionID;
	}

	public void setPollOptionID(String pollOptionID) {
		PollOptionID = pollOptionID;
	}

	public String getOption() {
		return Option;
	}

	public void setOption(String option) {
		Option = option;
	}
	

}
