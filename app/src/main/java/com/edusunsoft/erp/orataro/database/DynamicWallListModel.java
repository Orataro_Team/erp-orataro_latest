package com.edusunsoft.erp.orataro.database;



import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "DynamicWall")
public class DynamicWallListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "WallID")
    @SerializedName("WallID")
    public String WallID;

    @ColumnInfo(name = "WallName")
    @SerializedName("WallName")
    public String WallName;

    @ColumnInfo(name = "WallImage")
    @SerializedName("WallImage")
    public String WallImage;

    @ColumnInfo(name = "AssociationType")
    @SerializedName("AssociationType")
    public String AssociationType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getWallName() {
        return WallName;
    }

    public void setWallName(String wallName) {
        WallName = wallName;
    }

    public String getWallImage() {
        return WallImage;
    }

    public void setWallImage(String wallImage) {
        WallImage = wallImage;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    @Override
    public String toString() {
        return "DynamicWallListModel{" +
                "id=" + id +
                ", WallID='" + WallID + '\'' +
                ", WallName='" + WallName + '\'' +
                ", WallImage='" + WallImage + '\'' +
                ", AssociationType='" + AssociationType + '\'' +
                '}';
    }
}
