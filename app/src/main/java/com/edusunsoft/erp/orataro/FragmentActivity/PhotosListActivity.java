package com.edusunsoft.erp.orataro.FragmentActivity;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PhotoListAdapter;
import com.edusunsoft.erp.orataro.customeview.SlidingTabLayout;
import com.edusunsoft.erp.orataro.fragments.AlbumFragment;
import com.edusunsoft.erp.orataro.fragments.PhotoFragment;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class PhotosListActivity extends FragmentActivity implements OnClickListener {

    private GridView grv_photos;
    private ImageView img_back, iv_add_photos;
    private PhotoModel photoModel;
    private ArrayList<PhotoModel> photoModels;
    private PhotoListAdapter photo_ListAdapter;
    private TextView header_text;
    private Context mContext;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    private int selectedIcons[] = {R.drawable.photo_blue, R.drawable.album_blue,
            R.drawable.teachers_grp_show, R.drawable.share_show,
            R.drawable.views_show};
    private int icons[] = {R.drawable.photo_grey, R.drawable.album_grey,
            R.drawable.teachers_grp, R.drawable.share, R.drawable.views};
    private String from = ServiceResource.PROFILEWALL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_list_on_wall);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = PhotosListActivity.this;
        if (getIntent() != null) {
            from = getIntent().getStringExtra("isFrom");
        }

        header_text = (TextView) findViewById(R.id.header_text);
        img_back = (ImageView) findViewById(R.id.img_back);
        iv_add_photos = (ImageView) findViewById(R.id.img_menu);

        try {
            header_text.setText(getResources().getString(R.string.PhotosAlbum) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        iv_add_photos.setImageResource(R.drawable.add_photo);
        setupTabs();
        img_back.setOnClickListener(this);
        iv_add_photos.setOnClickListener(this);
    }

    private void setupTabs() {
        mPager = (ViewPager) findViewById(R.id.pager);
        BaseTabAdapter adapter = new BaseTabAdapter();
        mPager.setAdapter(adapter);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setCustomTabView(R.layout.tab_img_layout, R.id.tab_name_img, R.id.notificationCount, 0);
        mTabs.setDistributeEvenly(true);
        mTabs.setIconResourse(icons);
        mTabs.setSelectedIconResourse(selectedIcons);
        mTabs.setBackgroundColor(getResources().getColor(R.color.grey_list));
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.grp_txt_grey);
            }
        });

        mTabs.setViewPager(mPager);
        mTabs.setSelected(true);
        mPager.setCurrentItem(0);
    }

    public class BaseTabAdapter extends FragmentPagerAdapter implements
            SlidingTabLayout.TabIconProvider, SlidingTabLayout.TabCountProvider {
        private final int iconRes[] = {R.drawable.photo_blue,
                R.drawable.album_grey, R.drawable.teachers_grp,
                R.drawable.share, R.drawable.views};
        private int count[] = {1, 2, 30, 40, 50};

        private final String iconTxt[] = {"Camera", "Video"};

        public BaseTabAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

            super.registerDataSetObserver(observer);

        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {

                return PhotoFragment.newInstance(from);

            } else

                return AlbumFragment.newInstance(from);

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                header_text.setText(getResources().getString(R.string.photo));
            } else if (position == 1) {
                header_text.setText(getResources().getString(R.string.Album));
            }
            return iconTxt[position];
        }

        @Override
        public int getPageIconResId(int position) {
            return iconRes[position];
        }

        @Override
        public int getPageCount(int position) {
            return count[position];
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.iv_add_friend:
                finish();
                break;
            default:
                break;
        }
    }

}
