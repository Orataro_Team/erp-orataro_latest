package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "PTCommunicationList")
public class PtCommunicationModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "CommunicationID")
    @SerializedName("CommunicationID")
    public String CommunicationID;

    @ColumnInfo(name = "CommunicationDetail")
    @SerializedName("CommunicationDetail")
    public String CommunicationDetail;

    @ColumnInfo(name = "UserName")
    @SerializedName("UserName")
    public String UserName;

    @ColumnInfo(name = "UnReadCount")
    @SerializedName("UnReadCount")
    public String UnReadCount;

    @ColumnInfo(name = "SeqNo")
    @SerializedName("SeqNo")
    public String SeqNo;

    @ColumnInfo(name = "MemberID")
    @SerializedName("MemberID")
    public String MemberID;

    @ColumnInfo(name = "TeacherID")
    @SerializedName("TeacherID")
    public String TeacherID;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getTeacherID() {
        return TeacherID;
    }

    public void setTeacherID(String teacherID) {
        TeacherID = teacherID;
    }

    public String getCommunicationID() {
        return CommunicationID;
    }

    public void setCommunicationID(String communicationID) {
        CommunicationID = communicationID;
    }

    public String getCommunicationDetail() {
        return CommunicationDetail;
    }

    public void setCommunicationDetail(String communicationDetail) {
        CommunicationDetail = communicationDetail;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUnReadCount() {
        return UnReadCount;
    }

    public void setUnReadCount(String unReadCount) {
        UnReadCount = unReadCount;
    }

    public String getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(String seqNo) {
        SeqNo = seqNo;
    }

    @Override
    public String toString() {
        return "PtCommunicationModel{" +
                "id=" + id +
                ", CommunicationID='" + CommunicationID + '\'' +
                ", CommunicationDetail='" + CommunicationDetail + '\'' +
                ", UserName='" + UserName + '\'' +
                ", UnReadCount='" + UnReadCount + '\'' +
                ", SeqNo='" + SeqNo + '\'' +
                '}';
    }
}
