package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.PtCommunicationDetailActivity;
import com.edusunsoft.erp.orataro.database.PtCommunicationModel;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PtCommunicationListAdapter extends BaseAdapter {

    private Context mContext;
    private List<PtCommunicationModel> list = new ArrayList<>();
    private ArrayList<PtCommunicationModel> copyList = new ArrayList<>();
    private StdDivSubModel standardModel;
    private int[] colors2 = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    public PtCommunicationListAdapter(Context Context, List<PtCommunicationModel> list, StdDivSubModel standardModel) {
        this.mContext = Context;
        this.list = list;
        this.standardModel = standardModel;
        copyList = new ArrayList<PtCommunicationModel>();
        copyList.addAll(list);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.ptcommunicationlistraw, null);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtCount = (TextView) convertView.findViewById(R.id.txtCount);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        txtTitle.setText(list.get(position).getCommunicationDetail());
        txtName.setText(list.get(position).getUserName());

//        if(list.get(position).getUnReadCount() != null){
//            txtCount.setText(list.get(position).getUnReadCount());
//        }else{
//            txtCount.setVisibility(View.GONE);
//        }
        if (Integer.valueOf(list.get(position).getUnReadCount()) > 0) {
            txtCount.setText(list.get(position).getUnReadCount());
        } else {
            txtCount.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, PtCommunicationDetailActivity.class);
                i.putExtra("model", list.get(position));
                i.putExtra("standardmodel", standardModel);
                mContext.startActivity(i);

            }

        });

        int colorPos = position % colors2.length;
        convertView.setBackgroundColor(colors2[colorPos]);

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(copyList);
        } else {

            for (PtCommunicationModel vo : copyList) {
                if (vo.getCommunicationDetail().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

}
