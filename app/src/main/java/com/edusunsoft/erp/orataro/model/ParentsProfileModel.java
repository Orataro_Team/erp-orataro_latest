package com.edusunsoft.erp.orataro.model;

public class ParentsProfileModel {

	private String ParentProfileID,FullName,Avtar,RelationTypeTerm,ContactNo,MobileNo,OccupationTerm,DesignationOccupationTerm,OfficeAddress;
	private boolean IsPickup;
	private String FirstName,lastName,txt_birthday,txt_anywarsarydate,study,email,oocupationDetail,homeAddress,idNo,idType;
	private String dateofissue,dateofexpiry,typeGender,idproofpath;
	public String getIdproofpath() {
		return idproofpath;
	}

	public void setIdproofpath(String idproofpath) {
		this.idproofpath = idproofpath;
	}

	public String getTypeGender() {
		return typeGender;
	}

	public void setTypeGender(String typeGender) {
		this.typeGender = typeGender;
	}

	public String getParentProfileID() {
		return ParentProfileID;
	}

	public void setParentProfileID(String parentProfileID) {
		ParentProfileID = parentProfileID;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getAvtar() {
		return Avtar;
	}

	public void setAvtar(String avtar) {
		Avtar = avtar;
	}

	public String getRelationTypeTerm() {
		return RelationTypeTerm;
	}

	public void setRelationTypeTerm(String relationTypeTerm) {
		RelationTypeTerm = relationTypeTerm;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTxt_birthday() {
		return txt_birthday;
	}

	public void setTxt_birthday(String txt_birthday) {
		this.txt_birthday = txt_birthday;
	}

	public String getTxt_anywarsarydate() {
		return txt_anywarsarydate;
	}

	public void setTxt_anywarsarydate(String txt_anywarsarydate) {
		this.txt_anywarsarydate = txt_anywarsarydate;
	}

	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOocupationDetail() {
		return oocupationDetail;
	}

	public void setOocupationDetail(String oocupationDetail) {
		this.oocupationDetail = oocupationDetail;
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getDateofissue() {
		return dateofissue;
	}

	public void setDateofissue(String dateofissue) {
		this.dateofissue = dateofissue;
	}

	public String getDateofexpiry() {
		return dateofexpiry;
	}

	public void setDateofexpiry(String dateofexpiry) {
		this.dateofexpiry = dateofexpiry;
	}

	public void setIsPickup(boolean isPickup) {
		IsPickup = isPickup;
	}

	public String getContactNo() {
		return ContactNo;
	}

	public void setContactNo(String contactNo) {
		ContactNo = contactNo;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getOccupationTerm() {
		return OccupationTerm;
	}

	public void setOccupationTerm(String occupationTerm) {
		OccupationTerm = occupationTerm;
	}

	public String getDesignationOccupationTerm() {
		return DesignationOccupationTerm;
	}

	public void setDesignationOccupationTerm(String designationOccupationTerm) {
		DesignationOccupationTerm = designationOccupationTerm;
	}

	public String getOfficeAddress() {
		return OfficeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		OfficeAddress = officeAddress;
	}

	public boolean isIsPickup() {
		return IsPickup;
	}

	public void setIsPickup(String isPickup) {
		if(isPickup != null && !isPickup.equalsIgnoreCase("") && !isPickup.equalsIgnoreCase("null")){
			if(isPickup.equals("true") || isPickup.equalsIgnoreCase("false")){
				if(Boolean.valueOf(isPickup)){
					IsPickup = true;
				}
				else{
					IsPickup = false;
				}
			}
		}
	}


}
