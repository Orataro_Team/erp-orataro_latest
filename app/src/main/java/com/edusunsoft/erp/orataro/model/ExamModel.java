package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class ExamModel implements Serializable {

    String Str_DateOfExam, SubjectName, ExamTypeName,
            PaperTypeName, ResultTypeTerm, ExamName;

    Double Percentage, TotalMarks, ObtainedMarks, ClassHighest, ClassLowest, DivisionHighest, DivisionLowest;


    public ExamModel() {
        
    }


    public String getExamName() {
        return ExamName;
    }

    public void setExamName(String examName) {
        ExamName = examName;
    }

    public String getStr_DateOfExam() {
        return Str_DateOfExam;
    }

    public void setStr_DateOfExam(String str_DateOfExam) {
        Str_DateOfExam = str_DateOfExam;
    }


    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getExamTypeName() {
        return ExamTypeName;
    }

    public void setExamTypeName(String examTypeName) {
        ExamTypeName = examTypeName;
    }


    public Double getPercentage() {
        return Percentage;
    }

    public void setPercentage(Double percentage) {
        Percentage = percentage;
    }

    public Double getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(Double totalMarks) {
        TotalMarks = totalMarks;
    }

    public Double getObtainedMarks() {
        return ObtainedMarks;
    }

    public void setObtainedMarks(Double obtainedMarks) {
        ObtainedMarks = obtainedMarks;
    }

    public Double getClassHighest() {
        return ClassHighest;
    }

    public void setClassHighest(Double classHighest) {
        ClassHighest = classHighest;
    }

    public Double getClassLowest() {
        return ClassLowest;
    }

    public void setClassLowest(Double classLowest) {
        ClassLowest = classLowest;
    }

    public Double getDivisionHighest() {
        return DivisionHighest;
    }

    public void setDivisionHighest(Double divisionHighest) {
        DivisionHighest = divisionHighest;
    }

    public Double getDivisionLowest() {
        return DivisionLowest;
    }

    public void setDivisionLowest(Double divisionLowest) {
        DivisionLowest = divisionLowest;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }

    public String getResultTypeTerm() {
        return ResultTypeTerm;
    }

    public void setResultTypeTerm(String resultTypeTerm) {
        ResultTypeTerm = resultTypeTerm;
    }

    @Override
    public String toString() {
        return "ExamModel{" +
                "Str_DateOfExam='" + Str_DateOfExam + '\'' +
                ", SubjectName='" + SubjectName + '\'' +
                ", ExamTypeName='" + ExamTypeName + '\'' +
                ", TotalMarks='" + TotalMarks + '\'' +
                ", ObtainedMarks='" + ObtainedMarks + '\'' +
                ", Percentage='" + Percentage + '\'' +
                ", ClassHighest='" + ClassHighest + '\'' +
                ", ClassLowest='" + ClassLowest + '\'' +
                ", DivisionHighest='" + DivisionHighest + '\'' +
                ", DivisionLowest='" + DivisionLowest + '\'' +
                ", PaperTypeName='" + PaperTypeName + '\'' +
                ", ResultTypeTerm='" + ResultTypeTerm + '\'' +
                ", ExamName='" + ExamName + '\'' +
                '}';
    }
}
