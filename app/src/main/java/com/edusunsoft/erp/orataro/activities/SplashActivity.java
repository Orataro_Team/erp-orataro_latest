package com.edusunsoft.erp.orataro.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.adapter.DashBoardGridAdapter;
import com.edusunsoft.erp.orataro.customeview.DeviceHardWareId;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.MenuPermissionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.edusunsoft.erp.orataro.util.Global.menuPermissionList;


public class SplashActivity extends Activity implements ResponseWebServices {

    public static final int PERMISSION_RECEIVESMS = 100;
    public static final int PERMISSION_CALLPHONE = 200;
    public static final int PERMISSION_GROUP_CALENDAR = 300;
    public static final int PERMISSION_READSMS = 400;
    public static final int PERMISSION_GROUPSTORAGE = 500;
    public static final int PERMISSION_ACESSNETWORKSTATE = 600;
    public static final int PERMISSION_GETACCOUNT = 700;
    public static final int PERMISSION_READPHONESTATE = 800;
    public static final int PERMISSION_CAMERA = 900;
    public static final int PERMISSION_WAKELOCK = 1000;
    private static int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;
    private Context mContext = SplashActivity.this;
    private String TAG = SplashActivity.class.getSimpleName();
    private String currentVersion = "A", latestVersion = "", packageName;
    private String regId;
    private boolean isMoreChild;
    private boolean isMobile;
    private boolean isLogin = false;
    private boolean isValidDevice = false;
    private String authorizedEntity = ServiceResource.SENDERID; // Project id from Google Developer Console

    PackageManager pm;
    PackageInfo pInfo = null;

    MenuPermissionModel menuPermissionModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        pm = this.getPackageManager();

        Utility.setLanguage(mContext, new UUIDSharedPrefrance(mContext).getLang());
        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.get("notification") != null) {

            //here can get notification message
            String datas = bundle.get("notification").toString();

        }

        try {

            pInfo = pm.getPackageInfo(this.getPackageName(), 0);
            packageName = getApplicationContext().getPackageName();

        } catch (PackageManager.NameNotFoundException e1) {

            e1.printStackTrace();

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            Permissions();

        } else {

            getCurrentVersion();

        }


    }

    @Override
    protected void onResume() {

        super.onResume();

    }

    public void AfterCheckVersion() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (new UUIDSharedPrefrance(mContext).getIsFirstTime()) {

                    Log.d("Firsttimeeee", "false");

                    if (PreferenceData.getIsLogin()) {

//                        Utility.RedirectToDashboard(SplashActivity.this);
                        Intent intentHome = new Intent(SplashActivity.this, HomeWorkFragmentActivity.class);

                        if (PreferenceData.getIsSwitched() == false) {
                            intentHome.putExtra("position", getResources().getString(R.string.Wall));
                        } else {
                            intentHome.putExtra("position", getResources().getString(R.string.SwitchAccount));
                        }

                        startActivity(intentHome);
                        finish();
                        overridePendingTransition(0, 0);

                    } else {

                        Intent intentRegister = new Intent(SplashActivity.this, RegisterActivity.class);
                        startActivity(intentRegister);
                        finish();
                        overridePendingTransition(0, 0);

                    }

                } else {

                    Log.d("Firsttimeeee", "true");
                    new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                    Intent i = new Intent(SplashActivity.this, ChangeLangActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);

                }

            }

        }, 3000);


    }

    public void notificationCount() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.NOTIFICATIONCOUNT_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {
            notificationparse(result);
        } else if (methodName.equalsIgnoreCase(ServiceResource.LOGINWITHGCM_METHODNAME)) {
            loginParse(result);
        } else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, mContext);
            RollWriteParsing(result);
            notificationCount();

        } else if (ServiceResource.PUSHNOTIFICATIONFORAPPVERSION_BY_NAME_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("currentversionname", currentVersion);
            Log.d("notificationresult", result);

            try {

                latestVersion = new JSONArray(result).getJSONObject(0).getString("AppVersionName");
                Log.d("latestversion", latestVersion);
                Log.d("latestversion123", currentVersion);

            } catch (Exception e) {

                e.printStackTrace();

            }

            try {

                Log.d("globaluserid", new UserSharedPrefrence(mContext).getLOGIN_USERID());

                changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

            } catch (Exception e) {

                e.printStackTrace();

            }


            if (latestVersion != null && !latestVersion.equalsIgnoreCase("")) {

                try {

                    if (Double.valueOf(currentVersion) < Double.valueOf(latestVersion)) {

                        UpdateDialog();

                    } else {

                        AfterCheckVersion();

                    }


                } catch (Exception e) {

                    AfterCheckVersion();

                }

            } else {

                AfterCheckVersion();

            }

        } else if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.i(TAG, "response: GCM TOKEN UPDATED SUCCESSFULLY");

        } else if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN.equalsIgnoreCase(methodName)) {

            Global.userdataModel = new LoginModel();
            Global.userdataList = new ArrayList<>();

            JSONArray jsonObj;

            try {

                new UserSharedPrefrence(mContext).setDATE_FOR_API_CALL_ONCE(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));

                jsonObj = new JSONArray(result);

                Global.userdataModel.setLoginJson(result);
                PreferenceData.saveLoginUserData(result);

                Log.d("getData", PreferenceData.getLoginUserData().toString());
                Log.d("result123", result);

                if (jsonObj.length() == 1) {

                    PreferenceData.setLogin(true);

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        String Message = innerObj.getString(ServiceResource.MESSAGE);
                        if (Message.equalsIgnoreCase("No Data Found")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, Message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (Message.equalsIgnoreCase("Invalid Mobile Number And OTP Number...!!!")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, mContext.getResources().getString(R.string.loginintootherdevice));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {

                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            Global.userdataModel.setLoginJson(result);
                            new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);

                            PreferenceData.saveLoginUserData(result);
                            Log.d("getData", PreferenceData.getLoginUserData().toString());
                            Log.d("result123", result);

                        }


                    }


                } else {

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);
                        String Message = innerObj.getString(ServiceResource.MESSAGE);
                        if (Message.equalsIgnoreCase("No Data Found")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, Message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Message.equalsIgnoreCase("Invalid Mobile Number And OTP Number...!!!")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, mContext.getResources().getString(R.string.loginintootherdevice));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            PreferenceData.saveLoginUserData(result);
                            Global.userdataList.add(Global.userdataModel);
                            for (int k = 0; k < Global.userdataList.size(); k++) {

                                if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(Global.userdataList.get(k).getUserID())) {

                                    new UserSharedPrefrence(mContext).setLoginModel(Global.userdataList.get(k));
                                    Global.userdataModel = Global.userdataList.get(k);

                                }

                            }

                        }

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void RemoveSavedLoginData() {

        new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
        new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
        new UserSharedPrefrence(mContext).clearPrefrence();
        SharedPreferences preferences = mContext.getSharedPreferences("LOGIN_USER_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(mContext, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        }, 3000);

    }


    public void notificationparse(String result) {

        JSONObject obj;
        JSONArray jsonObj;
        JSONArray jsonObjfriend;

        try {

            Global.homeworkModels = new ArrayList<HomeWorkModel>();
            obj = new JSONObject(result);
            jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
            jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);
            JSONArray jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));
            }

            for (int i = 0; i < jsonObjfriend.length(); i++) {
                JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));
            }

            for (int i = 0; i < jsonObjleave.length(); i++) {
                JSONObject innerObj = jsonObjleave.getJSONObject(i);
                new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getCurrentVersion() {

        currentVersion = pInfo.versionName;
        packageName = pInfo.packageName;

        if (Utility.isNetworkAvailable(mContext)) {
            if (PreferenceData.getIsLogin()) {

                if (new UserSharedPrefrence(mContext).getDATE_FOR_API_CALL_ONCE().equalsIgnoreCase(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"))) {
                } else {
                    GetLoginResponse();
                }
                if (PreferenceData.getCTOKEN().isEmpty() || PreferenceData.getCTOKEN().equalsIgnoreCase("")
                        || PreferenceData.getCTOKEN().equalsIgnoreCase("null")
                        || PreferenceData.getCTOKEN().equalsIgnoreCase(null)) {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            Intent intentRegister = new Intent(SplashActivity.this, RegisterActivity.class);
                            startActivity(intentRegister);
                            finish();
                            overridePendingTransition(0, 0);
                        }
                    }, 3000);
                } else {
                    ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
                    arrayList.add(new PropertyVo(ServiceResource.APP_PACKAGE_NAME, packageName));
                    Log.d("packagename", arrayList.toString());

                    new AsynsTaskClass(mContext, arrayList, false, SplashActivity.this).execute(ServiceResource.PUSHNOTIFICATIONFORAPPVERSION_BY_NAME_METHODNAME, ServiceResource.NOTIFICATION_URL);
                }

            } else {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                        Intent i = new Intent(SplashActivity.this, ChangeLangActivity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }, 3000);
            }

        } else {

            Utility.RedirectToDashboard(SplashActivity.this);
//            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    private void GetLoginResponse() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, new UserSharedPrefrence(mContext).get_MOBILENUMBER()));
        arrayList.add(new PropertyVo(ServiceResource.OTPNumber, new UserSharedPrefrence(mContext).getOTP()));
        Log.d("otprequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, false, this, false).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN, ServiceResource.REGISTATION_URL);

    }


    public void UpdateDialog() {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);
        TextView tv_alert_title = (TextView) dialog.findViewById(R.id.tv_alert_title);
        TextView txtdetail = (TextView) dialog.findViewById(R.id.txtdetail);
        TextView txtno = (TextView) dialog.findViewById(R.id.txtno);
        tv_alert_title.setText(getResources().getString(R.string.UpdateAvailable));
        txtdetail.setText(getResources().getString(R.string.Newversion));
        TextView txtyes = (TextView) dialog.findViewById(R.id.txtyes);
        txtyes.setText(getResources().getString(R.string.update));
        txtno.setText(getResources().getString(R.string.Later));
        txtno.setVisibility(View.GONE);
        txtyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                dialog.dismiss();


            }

        });

        txtno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                AfterCheckVersion();
                dialog.dismiss();

            }

        });

        dialog.show();

    }

    public void changeGCMID(String UserId) {

        Log.d("getToken", PreferenceData.getToken());

        if (PreferenceData.getToken().equalsIgnoreCase(null) || PreferenceData.getToken().equalsIgnoreCase("null")

                || PreferenceData.getToken().equalsIgnoreCase("") || PreferenceData.getToken().isEmpty()) {

            try {

                Utility.RefreshFirebaseToken(mContext);

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        Log.d("requestlogin", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void loginParse(String result) {

        JSONArray jsonObj;
        try {
            if (result.contains("\"success\":0")) {
                isLogin = false;
            } else {
                jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).getLoginModel().setLoginJson(result);
                new UserSharedPrefrence(mContext).setLOGIN_JSON(result);
                if (jsonObj.length() == 1) {
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject innerObj = jsonObj.getJSONObject(i);
                        new UserSharedPrefrence(mContext).setLOGIN_JSON(result);
                        Global.userdataModel = new LoginModel();
                        Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);

                        isLogin = true;
                        isMoreChild = false;

                        if (!(ServiceResource.GURUKULINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                            if (ServiceResource.SINGLE_DEVICE_FLAG) {
                                if (!(ServiceResource.STATICINSTITUTEID.indexOf(new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()) >= 0)) {
                                    if (innerObj.getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))) {
                                        isValidDevice = true;

                                    } else {

                                        isValidDevice = false;

                                    }

                                } else {

                                    isValidDevice = true;

                                }

                            } else {

                                isValidDevice = true;

                            }

                        } else {

                            isLogin = false;

                        }

                    }
                    new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);
                } else {
                    int count = 0;
                    int posJson = 0;
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject innerObj = jsonObj.getJSONObject(i);
                        if (!(ServiceResource.GURUKULINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                            if (ServiceResource.SINGLE_DEVICE_FLAG) {
                                if (!(ServiceResource.STATICINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                                    if (innerObj.getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))) {
                                        isValidDevice = true;
                                        count++;
                                        posJson = i;
                                    }
                                } else {
                                    count++;
                                    isValidDevice = true;
                                    posJson = i;
                                }

                            } else {

                                count++;
                                isValidDevice = true;
                                posJson = i;

                            }
                        }
                    }

                    if (count == 1) {

                        JSONObject innerObj = jsonObj.getJSONObject(posJson);
                        Global.userdataModel = new LoginModel();
                        Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                        new UserSharedPrefrence(mContext).setLOGIN_JSON(result);

                        isLogin = true;
                        isMoreChild = false;
                        if (!(ServiceResource.GURUKULINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                            if (ServiceResource.SINGLE_DEVICE_FLAG) {
                                if (!(ServiceResource.STATICINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                                    if (innerObj.getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))) {
                                        isValidDevice = true;
                                    } else {
                                        isValidDevice = false;
                                    }
                                } else {
                                    isValidDevice = true;
                                }
                            } else {
                                isValidDevice = true;
                            }

                        } else {

                            isLogin = false;

                        }

                        new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);

                    } else if (count > 1) {
                        new UserSharedPrefrence(mContext).setLOGIN_JSON(result);
                        new UserSharedPrefrence(mContext).setISMULTIPLE(true);
                        isMoreChild = true;
                        isLogin = true;
                    } else {
                        isValidDevice = false;
                    }

                }

            }

        } catch (JSONException e) {
            isLogin = false;
            e.printStackTrace();
        }

        if (isLogin) {

            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();
            Intent i = new Intent(SplashActivity.this, HomeWorkFragmentActivity.class);
            i.putExtra("position", getResources().getString(R.string.Wall));
            startActivity(i);
            overridePendingTransition(0, 0);

        } else {

            Intent i = new Intent(SplashActivity.this, RegisterActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);

        }

        finish();
        overridePendingTransition(0, 0);

    }

    public void RollWriteParsing(String result) {
        try {
            JSONArray jsonObj = new JSONArray(result);
            Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
            ReadWriteSettingModel model;
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                model = new ReadWriteSettingModel();
                model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));
                if (!new UserSharedPrefrence(mContext).getLoginModel().getRoleName().equalsIgnoreCase("teacher")) {
                    model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                    model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                    model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                    model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));
                } else {
                    model.setIsView("true");
                    model.setIsEdit("true");
                    model.setIsCreate("true");
                    model.setIsDelete("true");
                }
                Global.readWriteSettingList.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void Permissions() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
//            permissionsNeeded.add("RECEIVE SMS");
//        if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE))
//            permissionsNeeded.add("CALL PHONE");
        if (!addPermission(permissionsList, Manifest.permission.READ_CALENDAR))
            permissionsNeeded.add("READ CALENDAR");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CALENDAR))
            permissionsNeeded.add("WRITE CALENDAR");
//        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
//            permissionsNeeded.add("READ SMS");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE EXTERNAL STORAGE");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("ACCESS NETWORK STATE");
        if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
            permissionsNeeded.add("GET ACCOUNTS");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("READ PHONE STATE");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");
        if (!addPermission(permissionsList, Manifest.permission.WAKE_LOCK))
            permissionsNeeded.add("WAKE LOCK");

        if (permissionsList.size() > 0) {

            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 0; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) mContext, permissionsList.toArray(new String[permissionsList.size()]), 1);
                            }
                        });
                return;
            }

            ActivityCompat.requestPermissions((Activity) mContext, permissionsList.toArray(new String[permissionsList.size()]), 1);

            return;

        } else {

            getCurrentVersion();

        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {

        new AlertDialog.Builder(mContext)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();

    }

    private boolean addPermission(List<String> permissionsList, String permission) {

        if (ContextCompat.checkSelfPermission(mContext, permission) != PackageManager.PERMISSION_GRANTED) {

            permissionsList.add(permission);

        }

        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {

            Map<String, Integer> perms = new HashMap<String, Integer>();
//            perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
//            perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_CALENDAR, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.WRITE_CALENDAR, PackageManager.PERMISSION_GRANTED);
//            perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.ACCESS_NETWORK_STATE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.WAKE_LOCK, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++) {

                perms.put(permissions[i], grantResults[i]);

            }

            // Check for ACCESS_FINE_LOCATION
            if (
//                    perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
//                    && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    perms.get(Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED
//                    && perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WAKE_LOCK) == PackageManager.PERMISSION_GRANTED

            ) {

                getCurrentVersion();

            } else {

                Utility.showToast("Some Permission is Denied", mContext);

            }

        }


    }

    @Override
    protected void onStop() {

        super.onStop();

    }

}
