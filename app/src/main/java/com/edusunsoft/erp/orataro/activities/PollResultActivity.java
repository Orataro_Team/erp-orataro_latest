package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.PollOptionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.PercentDrawable;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PollResultActivity extends Activity implements OnClickListener, ResponseWebServices {

    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    private Context mContext;
    ActionItem actionShare;
    private static final int ID_SHARE = 1;
    QuickAction quickAction;
    LinearLayout ll_menu;
    PollModel pollmodel;
    private ArrayList<PollOptionModel> optionList;
    private PollModel model;
    TextView txtquestion, txtdetail;
    LinearLayout addoption, lllayout;

    // commented by Krishna : 28-01-2019 - Variables declaration for take screnshot of scrollview with FullView

    ScrollView scrollview;
    public static Bitmap bitScroll;

    /*END*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pollresultactivity);
        mContext = PollResultActivity.this;
        pollmodel = (PollModel) getIntent().getSerializableExtra("model");

        PollResult();

        actionShare = new ActionItem(ID_SHARE, "Share", mContext
                .getResources().getDrawable(R.drawable.share_white));

        quickAction = new QuickAction(mContext,
                QuickAction.VERTICAL);
        quickAction.addActionItem(actionShare);

        scrollview = (ScrollView) findViewById(R.id.scrollview);

        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        ll_menu = (LinearLayout) findViewById(R.id.ll_menu);
        lllayout = (LinearLayout) findViewById(R.id.lllayout);

        txtquestion = (TextView) findViewById(R.id.txtquestion);
        txtdetail = (TextView) findViewById(R.id.txtdetail);
        addoption = (LinearLayout) findViewById(R.id.addoption);

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.GONE);
        imgRightheader.setImageResource(R.drawable.back);
        imgRightheader.setRotation(270);

        try {
            txtHeader.setText(getResources().getString(R.string.Result) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
        } catch (Exception e) {

        }

        imgLeftheader.setOnClickListener(this);
        imgRightheader.setOnClickListener(this);

        txtquestion.setText("");
        txtdetail.setText("");

        // Set listener for action item clicked
        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {

                        ActionItem actionItem = quickAction.getActionItem(pos);
                        quickAction.dismiss();

                        if (actionId == ID_SHARE) {

                            bitScroll = getBitmapFromView(scrollview, scrollview.getChildAt(0).getHeight(), scrollview.getChildAt(0).getWidth());
                            saveBitmap(bitScroll);
//                            Intent intent = new Intent(PollResultActivity.this, PreviewActivity.class);
//                            startActivity(intent);
//                            ImageSave(lllayout);

                        }

                    }

                });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
                // Toast.makeText(context, "Dismissed",
                // Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_home) {
            finish();
        } else if (v.getId() == R.id.img_menu) {

            quickAction.show(v);

        }

    }


    public void PollResult() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID,
                pollmodel.getPollID()));


        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOLLRESULT_METHODNAME,
                ServiceResource.POLL_URL);


    }


    //create bitmap from the ScrollView

    private Bitmap getBitmapFromView(View view, int height, int width) {

        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        view.draw(c);

        return b;

    }

    public void saveBitmap(Bitmap bitmap) {

        FileOutputStream fos;

        try {

            fos = new FileOutputStream(Utility.getShareResult(pollmodel.getPollID()));

            if (Utility.getShareResult(pollmodel.getPollID()).contains(".png") || Utility.getShareResult(pollmodel.getPollID()).contains(".PNG")) {

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

            } else {

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            }

//            bitmap.compress(Bitmap.CompressFormat.PNG, 10, fos);

            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {

            Log.e("GREC", e.getMessage(), e);

        } catch (IOException e) {

            Log.e("GREC", e.getMessage(), e);

        }

        ShareResult();

    }

    public void ShareResult() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID,
                pollmodel.getPollID()));
        arrayList.add(new PropertyVo(ServiceResource.ISNOTIFIYTOALL,
                model.getIsNotify()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID,
                new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));

        byte[] byteArray = null;
        Bitmap bitmap = Utility.getBitmap(Utility.getShareResult(pollmodel.getPollID()), mContext);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        if (Utility.getShareResult(pollmodel.getPollID()).contains(".png") || Utility.getShareResult(pollmodel.getPollID()).contains(".PNG")) {

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        } else {

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        }

        byteArray = bytes.toByteArray();
        arrayList.add(new PropertyVo(ServiceResource.FILE,
                byteArray));
        arrayList.add(new PropertyVo(ServiceResource.FILENAME,
                Utility.getShareResult(pollmodel.getPollID())));
        arrayList.add(new PropertyVo(ServiceResource.POSTBYTYPE,
                new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SHARERESULT_METHODNAME,
                ServiceResource.POLL_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETPOLLRESULT_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("resultpoll", result.toString());
            JSONArray jsonObj;
            JSONObject jobj;
            JSONArray OptionArray;

            try {

                jobj = new JSONObject(result);
                jsonObj = jobj.getJSONArray(ServiceResource.TABLE);
                OptionArray = jobj.getJSONArray(ServiceResource.TABLE1);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new PollModel();
                    model.setTitle(innerObj
                            .getString(ServiceResource.POLL_TITLE));
                    model.setDetails(innerObj
                            .getString(ServiceResource.POLL_DETAIL));
                    model.setIsNotify(innerObj
                            .getString(ServiceResource.ISNOTIFYALL));
                    model.setIsPresentage(innerObj
                            .getString(ServiceResource.ISPERCENTEGE));
                    model.setTotalVotes(innerObj
                            .getString(ServiceResource.TOTALVOTES));
                    //					model.setIsMultichoice(innerObj
                    //							.getString(ServiceResource.ISMULTICHOICE));

                }

                optionList = new ArrayList<PollOptionModel>();

                for (int i = 0; i < OptionArray.length(); i++) {

                    JSONObject innerObj = OptionArray.getJSONObject(i);


                    PollOptionModel optionModel = new PollOptionModel();
                    optionModel.setPollOptionID(innerObj
                            .getString(ServiceResource.POLLOPTIONID));
                    optionModel.setOption(innerObj
                            .getString(ServiceResource.OPTION));
                    optionModel.setTotalVotes(innerObj
                            .getString(ServiceResource.TOTALVOTES));
                    optionList.add(optionModel);

                }


            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            filldata();


        } else if (ServiceResource.SHARERESULT_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.toast(mContext, getResources().getString(R.string.ShareSuccessfull));

        }


    }

    private void filldata() {

        txtquestion.setText(model.getTitle());
        txtdetail.setText(model.getDetails());

        for (int i = 0; i < optionList.size(); i++) {
            LayoutInflater layoutInfalater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = layoutInfalater.inflate(R.layout.pollresultraw, null);
            TextView txtoption = (TextView) v.findViewById(R.id.txtoption);
            LinearLayout matching_progress = (LinearLayout) v.findViewById(R.id.matching_progress);
            TextView txt_percentage = (TextView) v.findViewById(R.id.txt_percentage);

            txtoption.setText((i + 1) + ") " + optionList.get(i).getOption());
            float per = 50;
            if (i == 1) {
                per = 100;
            }
            float persantage = getPercentage(Integer.valueOf(optionList.get(i).getTotalVotes()), Integer.valueOf(model.getTotalVotes()));

            matching_progress.setBackground(new PercentDrawable(persantage));
            if (model.getIsPresentage()) {

                txt_percentage.setText(String.valueOf(getPercentage(Integer.valueOf(optionList.get(i).getTotalVotes()), Integer.valueOf(model.getTotalVotes()))) + "%");
            } else {
                txt_percentage.setText(optionList.get(i).getTotalVotes() + "votes");
                //				txt_percentage.setText(String.valueOf(getPercentage(Integer.valueOf(optionList.get(i).getTotalVotes()), Integer.valueOf(model.getTotalVotes()))));
            }
//            DisplayMetrics dm = new DisplayMetrics();
//            getWindowManager().getDefaultDisplay().getMetrics(dm);
////            int width = (int) (dm.widthPixels - 4 * getResources().getDimension(R.dimen.padding) - getResources().getDimension(R.dimen.small_margin));
////            int height = dm.heightPixels;
//            int width = matching_progress.getLayoutParams().width;
//            int height = matching_progress.getLayoutParams().height;
//            matching_progress
//                    .setLayoutParams(new FrameLayout.LayoutParams((int) ((persantage * width) / 100), height));

            addoption.addView(v);

        }

    }

    public static float getPercentage(int n, int total) {

        float proportion = ((float) n) / ((float) total);
        return proportion * 100;

    }

    public static Bitmap loadBitmapFromView(LinearLayout v) {

        v.setDrawingCacheEnabled(true);
        Bitmap myBitmap = v.getDrawingCache();

        return myBitmap;

    }

    public void ImageSave(LinearLayout v) {

        Toast.makeText(mContext, "Inside Imagesave", Toast.LENGTH_SHORT).show();

        FileOutputStream out = null;
        Bitmap bitmap = loadBitmapFromView(v);

        try {

            out = new FileOutputStream(Utility.getShareResult(pollmodel.getPollID()));

            if (Utility.getShareResult(pollmodel.getPollID()).contains(".png") || Utility.getShareResult(pollmodel.getPollID()).contains(".PNG")) {

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

            } else {

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            }

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        }

        ShareResult();

    }

}
