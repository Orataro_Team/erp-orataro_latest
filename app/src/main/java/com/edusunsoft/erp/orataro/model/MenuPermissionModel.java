package com.edusunsoft.erp.orataro.model;

public class MenuPermissionModel {

    String MenuName, MenuIconName, DefaultValue;
    boolean IsView, isStatic;

    public MenuPermissionModel() {
    }

    public MenuPermissionModel(String menuName, String menuIconName, boolean isView, boolean isStatic) {
        this.MenuName = menuName;
        this.MenuIconName = menuIconName;
        this.IsView = isView;
        this.isStatic = isStatic;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getMenuIconName() {
        return MenuIconName;
    }

    public void setMenuIconName(String menuIconName) {
        MenuIconName = menuIconName;
    }

    public boolean isView() {
        return IsView;
    }

    public void setView(boolean view) {
        IsView = view;
    }

    public String getDefaultValue() {
        return DefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        DefaultValue = defaultValue;
    }

    @Override
    public String toString() {
        return "MenuPermissionModel{" +
                "MenuName='" + MenuName + '\'' +
                ", MenuIconName='" + MenuIconName + '\'' +
                ", DefaultValue='" + DefaultValue + '\'' +
                ", IsView=" + IsView +
                ", isStatic=" + isStatic +
                '}';
    }
}
