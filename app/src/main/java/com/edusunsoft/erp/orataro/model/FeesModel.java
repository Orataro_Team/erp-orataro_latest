package com.edusunsoft.erp.orataro.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesModel implements Parcelable {

    @SerializedName("FeesStructuteID")
    @Expose
    private String feesStructuteID;
    @SerializedName("FeesStructureScheduleID")
    @Expose
    private String feesStructureScheduleID;
    @SerializedName("StructuteName")
    @Expose
    private String structuteName;
    @SerializedName("DisplayName")
    @Expose
    private String displayName;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("BatchName")
    @Expose
    private String batchName;
    @SerializedName("StudentID")
    @Expose
    private String studentID;
    @SerializedName("TotalDueAmount")
    @Expose
    private String totalDueAmount;
    @SerializedName("StudentFeesCollectionID")
    @Expose
    private String StudentFeesCollectionID;
    @SerializedName("IsOptionalFeeAvailable")
    @Expose
    private String isOptionalFeeAvailable;
    @SerializedName("UnpaidAmount")
    @Expose
    private String UnpaidAmount;
    public Double unpaid_amount;

    @SerializedName("TotalPaidAmount")
    @Expose
    public Double TotalPaidAmount;

    @SerializedName("BatchID")
    @Expose
    private String BatchID;

    @SerializedName("ClientID")
    @Expose
    private String ClientID;

    @SerializedName("InstituteID")
    @Expose
    private String InstituteID;

    @SerializedName("PaymentGateWayMasterID")
    @Expose
    private String paymentgatewaymasterid;

    @SerializedName("RequestFrom")
    @Expose
    private String RequestFrom;

    @SerializedName("CounterID")
    @Expose
    private String CounterID;


    public final static Parcelable.Creator<FeesModel> CREATOR = new Creator<FeesModel>() {
        public FeesModel createFromParcel(Parcel in) {
            FeesModel instance = new FeesModel();
            instance.feesStructuteID = ((String) in.readValue((String.class.getClassLoader())));
            instance.feesStructuteID = ((String) in.readValue((String.class.getClassLoader())));
            instance.feesStructureScheduleID = ((String) in.readValue((String.class.getClassLoader())));
            instance.structuteName = ((String) in.readValue((String.class.getClassLoader())));
            instance.displayName = ((String) in.readValue((String.class.getClassLoader())));
            instance.amount = ((String) in.readValue((Double.class.getClassLoader())));
            instance.batchName = ((String) in.readValue((String.class.getClassLoader())));
            instance.studentID = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalDueAmount = ((String) in.readValue((Object.class.getClassLoader())));
            instance.StudentFeesCollectionID = ((String) in.readValue((Object.class.getClassLoader())));
            instance.isOptionalFeeAvailable = ((String) in.readValue((Integer.class.getClassLoader())));
            instance.UnpaidAmount = ((String) in.readValue((Double.class.getClassLoader())));
//            instance.ClientID = ((String) in.readValue((Double.class.getClassLoader())));
//            instance.InstituteID = ((String) in.readValue((Double.class.getClassLoader())));
//            instance.BatchID = ((String) in.readValue((String.class.getClassLoader())));
            instance.paymentgatewaymasterid = ((String) in.readValue((String.class.getClassLoader())));
            instance.ClientID = ((String) in.readValue((String.class.getClassLoader())));
            instance.InstituteID = ((String) in.readValue((String.class.getClassLoader())));
            instance.BatchID = ((String) in.readValue((String.class.getClassLoader())));
            instance.CounterID = ((String) in.readValue((String.class.getClassLoader())));
            return instance;

        }

        public FeesModel[] newArray(int size) {
            return (new FeesModel[size]);
        }

    };

    public String getCounterID() {
        return CounterID;
    }

    public void setCounterID(String counterID) {
        CounterID = counterID;
    }

    public String getPaymentgatewaymasterid() {
        return paymentgatewaymasterid;
    }

    public void setPaymentgatewaymasterid(String paymentgatewaymasterid) {
        this.paymentgatewaymasterid = paymentgatewaymasterid;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }


    public String getRequestFrom() {
        return RequestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        RequestFrom = requestFrom;
    }

    public Double getUnpaid_amount() {
        return unpaid_amount;
    }

    public Double getTotalPaidAmount() {
        return TotalPaidAmount;
    }

    public void setTotalPaidAmount(Double totalPaidAmount) {
        TotalPaidAmount = totalPaidAmount;
    }

    public void setUnpaid_amount(Double unpaid_amount) {
        this.unpaid_amount = unpaid_amount;
    }

    public String getFeesStructuteID() {
        return feesStructuteID;
    }

    public void setFeesStructuteID(String feesStructuteID) {
        this.feesStructuteID = feesStructuteID;
    }

    public String getUnpaidAmount() {
        return UnpaidAmount;
    }


    public void setUnpaidAmount(String unpaidAmount) {
        UnpaidAmount = unpaidAmount;
    }

    public String getFeesStructureScheduleID() {
        return feesStructureScheduleID;
    }

    public void setFeesStructureScheduleID(String feesStructureScheduleID) {
        this.feesStructureScheduleID = feesStructureScheduleID;
    }

    public String getStructuteName() {
        return structuteName;
    }

    public void setStructuteName(String structuteName) {
        this.structuteName = structuteName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getTotalDueAmount() {
        return totalDueAmount;
    }

    public void setTotalDueAmount(String totalDueAmount) {
        this.totalDueAmount = totalDueAmount;
    }

    public String getStudentFeesCollectionID() {
        return StudentFeesCollectionID;
    }

    public void setStudentFeesCollectionID(String studentFeesCollectionID) {
        StudentFeesCollectionID = studentFeesCollectionID;
    }

    public String getIsOptionalFeeAvailable() {
        return isOptionalFeeAvailable;
    }

    public void setIsOptionalFeeAvailable(String isOptionalFeeAvailable) {
        this.isOptionalFeeAvailable = isOptionalFeeAvailable;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(feesStructuteID);
        dest.writeValue(feesStructureScheduleID);
        dest.writeValue(structuteName);
        dest.writeValue(displayName);
        dest.writeValue(amount);
        dest.writeValue(batchName);
        dest.writeValue(studentID);
        dest.writeValue(totalDueAmount);
        dest.writeValue(StudentFeesCollectionID);
        dest.writeValue(isOptionalFeeAvailable);
//        dest.writeValue(ClientID);
//        dest.writeValue(InstituteID);
//        dest.writeValue(BatchID);
        dest.writeValue(paymentgatewaymasterid);
        dest.writeValue(ClientID);
        dest.writeValue(InstituteID);
        dest.writeValue(BatchID);

    }

    public int describeContents() {
        return 0;
    }

}
