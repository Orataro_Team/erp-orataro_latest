package com.edusunsoft.erp.orataro.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ParentProfileActivity;
import com.edusunsoft.erp.orataro.customeview.SlidingTabLayout;
import com.edusunsoft.erp.orataro.services.ServiceResource;

public class ParentProfileFragment extends Fragment {

    private View view;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    public static String[] notificationCount = {"10", "20", "30", "40", "50", "60"};
    private final int iconRes[] = {R.drawable.schoolgroups, R.drawable.file,};

    int icons[] = {R.drawable.profileico, R.drawable.father, R.drawable.mother, R.drawable.gaurdian};
    int selectedIcons[] = {R.drawable.profileico_show, R.drawable.father_show, R.drawable.mother_show, R.drawable.gaurdian_show};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.groupviewfragment, container, false);
        setupTabs();
        return view;
    }

    private void setupTabs() {
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(new BaseTabAdapter());
        mTabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        mTabs.setCustomTabView(R.layout.tab_img_layout, R.id.tab_name_img, R.id.notificationCount, 0);
        mTabs.setDistributeEvenly(true);
        mTabs.setIconResourse(icons);
        mTabs.setSelectedIconResourse(selectedIcons);
        mTabs.setBackgroundColor(getResources().getColor(R.color.grey_list));
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.grp_txt_grey);
            }
        });

        mTabs.setViewPager(mPager);
        mTabs.setSelectedTab(0);
    }

    public class BaseTabAdapter extends FragmentPagerAdapter implements SlidingTabLayout.TabIconProvider, SlidingTabLayout.TabCountProvider {
        private final int iconRes[] = {R.drawable.profileico_show, R.drawable.father, R.drawable.mother, R.drawable.gaurdian};
        private int count[] = {1, 2, 30, 40, 50};
        private final String iconTxt[] = {"Camera", "Viedeo"};

        public BaseTabAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            super.registerDataSetObserver(observer);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment f = null;

            if (position == 0) {
                ParentProfileActivity.type = ServiceResource.FATHER_PROFILE_INT;
                f = new StudentProfileFragment();
            } else if (position == 1) {
                ParentProfileActivity.type = ServiceResource.FATHER_PROFILE_INT;
                f = FatherProfileFragment.newInstance(ServiceResource.FATHER_PROFILE_INT);
            } else if (position == 2) {
                ParentProfileActivity.type = ServiceResource.MOTHER_PROFILE_INT;
                f = FatherProfileFragment.newInstance(ServiceResource.MOTHER_PROFILE_INT);
            } else if (position == 3) {
                ParentProfileActivity.type = ServiceResource.GARDIAN_PROFILE_INT;
                f = FatherProfileFragment.newInstance(ServiceResource.GARDIAN_PROFILE_INT);
            }

            return f;

        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return iconTxt[position];
        }

        @Override
        public int getPageIconResId(int position) {
            return iconRes[position];
        }

        @Override
        public int getPageCount(int position) {
            return count[position];
        }

    }
}
