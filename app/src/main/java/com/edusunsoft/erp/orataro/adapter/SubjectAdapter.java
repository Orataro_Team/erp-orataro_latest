package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.SubjectModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class SubjectAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<SubjectModel> subject;

	public SubjectAdapter(Context context, ArrayList<SubjectModel> subject) {
		this.context=context;
		this.subject=subject;
	}

	@Override
	public int getCount() {
		return subject.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView=inflater.inflate(R.layout.subject_grid_item, null);
		SubjectModel model=subject.get(position);
		TextView tvsubjctname=(TextView) convertView.findViewById(R.id.txt_sub_action);
		tvsubjctname.setText(model.getSubjectname());
		ImageView imgsubjct=(ImageView) convertView.findViewById(R.id.sub_grid_icon);
		imgsubjct.setImageBitmap(Utility.getBitmapFromAsset(context,model.getSubjectimage()));
		return convertView;
	}

}
