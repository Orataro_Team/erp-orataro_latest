package com.edusunsoft.erp.orataro.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Exam implements Serializable {

    String ID,ExamType,Standard,Division,Year,PublishDate,ClientID,InstituteID,ExamMasterID, BatchID, DivisionID, StandardID, ExamName, ShiftID,OnlyExamName;

    @SerializedName("ExamID")
    @Expose
    private String examID;
    @SerializedName("ExamName")
    @Expose
    private String examName;
    @SerializedName("OrderNo")
    @Expose
    private Integer orderNo;
//    public final static Parcelable.Creator<Exam> CREATOR = new Creator<Exam>() {
//
//        public Exam createFromParcel(Parcel in) {
//
//            Exam instance = new Exam();
//            instance.examID = ((String) in.readValue((String.class.getClassLoader())));
//            instance.examName = ((String) in.readValue((String.class.getClassLoader())));
//            instance.orderNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
//            return instance;
//
//        }
//
//        public Exam[] newArray(int size) {
//
//            return (new Exam[size]);
//
//        }
//
//    };


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getExamType() {
        return ExamType;
    }

    public void setExamType(String examType) {
        ExamType = examType;
    }

    public String getStandard() {
        return Standard;
    }

    public void setStandard(String standard) {
        Standard = standard;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String division) {
        Division = division;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }

    public String getOnlyExamName() {
        return OnlyExamName;
    }

    public void setOnlyExamName(String onlyExamName) {
        OnlyExamName = onlyExamName;
    }

    public String getShiftID() {
        return ShiftID;
    }

    public void setShiftID(String shiftID) {
        ShiftID = shiftID;
    }

    public String getExamMasterID() {
        return ExamMasterID;
    }

    public void setExamMasterID(String examMasterID) {
        ExamMasterID = examMasterID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

//    public static Creator<Exam> getCREATOR() {
//        return CREATOR;
//    }

    public String getExamID() {
        return examID;
    }

    public void setExamID(String examID) {
        this.examID = examID;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getOrderNo() {

        return orderNo;

    }

    public void setOrderNo(Integer orderNo) {

        this.orderNo = orderNo;

    }

    public void writeToParcel(Parcel dest, int flags) {

        dest.writeValue(examID);
        dest.writeValue(examName);
        dest.writeValue(orderNo);

    }

    public int describeContents() {
        return 0;
    }

}