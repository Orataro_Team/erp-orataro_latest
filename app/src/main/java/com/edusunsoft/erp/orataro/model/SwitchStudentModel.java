package com.edusunsoft.erp.orataro.model;

public class SwitchStudentModel {

    private String st_name;
    private String st_img;
    private String st_division;
    private String st_standard;
    private String st_gender;

    public String getSt_name() {
        return st_name;
    }

    public void setSt_name(String st_name) {
        this.st_name = st_name;
    }

    public String getSt_img() {
        return st_img;
    }

    public void setSt_img(String st_img) {
        this.st_img = st_img;
    }

    public String getSt_division() {
        return st_division;
    }

    public void setSt_division(String st_division) {
        this.st_division = st_division;
    }

    public String getSt_standard() {
        return st_standard;
    }

    public void setSt_standard(String st_standard) {
        this.st_standard = st_standard;
    }

    public String getSt_gender() {
        return st_gender;
    }

    public void setSt_gender(String st_gender) {
        this.st_gender = st_gender;
    }

}
