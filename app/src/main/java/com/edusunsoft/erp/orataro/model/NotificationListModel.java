package com.edusunsoft.erp.orataro.model;

public class NotificationListModel {

    private boolean Isview;
    private String profile_pic;
    private String profile_name;
    private String notification;
    private String RowNo, NotificationID, AssociationID, AssociationType, DateofNotification, FullName;
    private String DateofNotificationAsText, Detail, NotificationText, NotificationType, SendToMemberID;

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public boolean isIsview() {
        return Isview;
    }

    public void setIsview(boolean isview) {
        Isview = isview;
    }

    public String getRowNo() {
        return RowNo;
    }

    public void setRowNo(String rowNo) {
        RowNo = rowNo;
    }

    public String getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(String notificationID) {
        NotificationID = notificationID;
    }

    public String getAssociationID() {
        return AssociationID;
    }

    public void setAssociationID(String associationID) {
        AssociationID = associationID;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    public String getDateofNotification() {
        return DateofNotification;
    }

    public void setDateofNotification(String dateofNotification) {
        DateofNotification = dateofNotification;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getDateofNotificationAsText() {
        return DateofNotificationAsText;
    }

    public void setDateofNotificationAsText(String dateofNotificationAsText) {
        DateofNotificationAsText = dateofNotificationAsText;
    }

    public String getNotificationText() {
        return NotificationText;
    }

    public void setNotificationText(String notificationText) {
        NotificationText = notificationText;
    }

    public String getNotificationType() {
        return NotificationType;
    }

    public void setNotificationType(String notificationType) {
        NotificationType = notificationType;
    }

    public String getSendToMemberID() {
        return SendToMemberID;
    }

    public void setSendToMemberID(String sendToMemberID) {
        SendToMemberID = sendToMemberID;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getProfile_name() {
        return profile_name;
    }

    public void setProfile_name(String profile_name) {
        this.profile_name = profile_name;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

}
