package com.edusunsoft.erp.orataro.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.PaymentActivity;
import com.edusunsoft.erp.orataro.fragments.ReciptListFragment;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

/**
 * Created by admin on 16-05-2017.
 */

public class FeesListAdapter extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private ArrayList<FeesModel> listFees;
    public String FEERECEIPTURL = "", FromString = "";

    public FeesListAdapter(Context mContext, ArrayList<FeesModel> listFees, String FromString) {
        this.mContext = mContext;
        this.listFees = listFees;
        this.FromString = FromString;
    }

    @Override
    public int getCount() {
        return listFees.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInfalater.inflate(R.layout.feeslistraw, viewGroup, false);
        TextView txtfees = (TextView) view.findViewById(R.id.txtfees);
        TextView txtammount = (TextView) view.findViewById(R.id.txtammount);
        TextView txtdue = (TextView) view.findViewById(R.id.txtdue);
        TextView txtpaidamount = (TextView) view.findViewById(R.id.txtpaidamount);
        TextView txtaction = (TextView) view.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) view.findViewById(R.id.imgremove);
        ImageView img_fee_receipt = (ImageView) view.findViewById(R.id.img_fee_receipt);
        txtfees.setText(listFees.get(i).getDisplayName());
        txtammount.setText(listFees.get(i).getAmount());
        txtdue.setText(String.valueOf(listFees.get(i).getUnpaid_amount()));
        txtpaidamount.setText(String.valueOf(listFees.get(i).getTotalPaidAmount()));

        if (FromString.equalsIgnoreCase("FromFee")) {

            img_fee_receipt.setVisibility(View.INVISIBLE);

            if ((listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase(null) || listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase("null") ||
                    listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase(""))
                    || (listFees.get(i).getStudentID().equalsIgnoreCase(null) || listFees.get(i).getStudentID().equalsIgnoreCase("null") ||
                    listFees.get(i).getStudentID().equalsIgnoreCase(""))
                    || (listFees.get(i).getInstituteID().equalsIgnoreCase(null) || listFees.get(i).getInstituteID().equalsIgnoreCase("null") ||
                    listFees.get(i).getInstituteID().equalsIgnoreCase(""))
                    || (listFees.get(i).getClientID().equalsIgnoreCase(null) || listFees.get(i).getClientID().equalsIgnoreCase("null") ||
                    listFees.get(i).getClientID().equalsIgnoreCase(""))
                    || (listFees.get(i).getFeesStructuteID().equalsIgnoreCase(null) || listFees.get(i).getFeesStructuteID().equalsIgnoreCase("null") ||
                    listFees.get(i).getFeesStructuteID().equalsIgnoreCase(""))
                    || (listFees.get(i).getFeesStructureScheduleID().equalsIgnoreCase(null) || listFees.get(i).getFeesStructureScheduleID().equalsIgnoreCase("null") ||
                    listFees.get(i).getFeesStructureScheduleID().equalsIgnoreCase(""))
                    || (listFees.get(i).getBatchID().equalsIgnoreCase(null) || listFees.get(i).getBatchID().equalsIgnoreCase("null") ||
                    listFees.get(i).getBatchID().equalsIgnoreCase(""))
                    || (listFees.get(i).getCounterID().equalsIgnoreCase(null) || listFees.get(i).getCounterID().equalsIgnoreCase("null") ||
                    listFees.get(i).getCounterID().equalsIgnoreCase(""))) {
                txtaction.setVisibility(View.INVISIBLE);
            } else {
                txtaction.setVisibility(View.VISIBLE);
            }

//            if (listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase(null) || listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase("null")
//                    || listFees.get(i).getPaymentgatewaymasterid().equalsIgnoreCase("")) {
//                txtaction.setVisibility(View.INVISIBLE);
//            } else {
//                txtaction.setVisibility(View.VISIBLE);
//            }

        } else {
            img_fee_receipt.setVisibility(View.VISIBLE);
            txtaction.setVisibility(View.INVISIBLE);
        }

        img_fee_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FEERECEIPTURL = ServiceResource.FEESRECEIPTURL
                        + "?ClientID=" + listFees.get(i).getClientID()
                        + "&InstituteID=" + listFees.get(i).getInstituteID()
                        + "&BatchID=" + listFees.get(i).getBatchID()
                        + "&StudentID=" + listFees.get(i).getStudentID()
                        + "&FeesStructureID=" + listFees.get(i).getFeesStructuteID()
                        + "&ctoken=" + PreferenceData.getCTOKEN();

                Log.d("feercptURl", FEERECEIPTURL);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse(FEERECEIPTURL));
                mContext.startActivity(i);

            }

        });

        txtaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Create dialog for get amount from user with condition (UnpaidAmount > enter amount == TRUE) by hardik_kanak 26-11-2020

                final AlertDialog dialogBuilder = new AlertDialog.Builder(mContext).create();
                View dialogView = LayoutInflater.from(mContext).inflate(R.layout.dialog_enter_amount, null);

                final EditText editText = (EditText) dialogView.findViewById(R.id.edt_amount);
                Button buttonPay = (Button) dialogView.findViewById(R.id.buttonPay);
                Button buttonCancel = (Button) dialogView.findViewById(R.id.buttonCancel);

                buttonPay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Double mUnpaid_amount = listFees.get(i).getUnpaid_amount();
                        String amount = editText.getText().toString();
                        if (!amount.trim().equals("") && Double.parseDouble(amount) != 0 &&  mUnpaid_amount >= Double.parseDouble(amount)){

                            dialogBuilder.dismiss();

                            Bundle bundle = new Bundle();
                            bundle.putString("BatchID", listFees.get(i).getBatchID());
                            bundle.putString("ClientID", listFees.get(i).getClientID());
                            bundle.putString("InstituteID", listFees.get(i).getInstituteID());
                            bundle.putString("FeeStructureID", listFees.get(i).getFeesStructuteID());
                            bundle.putString("FeeStructureScheduleID", listFees.get(i).getFeesStructureScheduleID());
                            bundle.putString("UnpaidAmount", String.valueOf(listFees.get(i).getUnpaid_amount()));
                            bundle.putString("PaymentGateWayMasterID", String.valueOf(listFees.get(i).getPaymentgatewaymasterid()));
                            bundle.putDouble("PayAmt",Double.parseDouble(amount)); // add PayAmt by hardik_kanak 26-11-2020
                            Intent intent = new Intent(mContext, PaymentActivity.class);
                            intent.putExtras(bundle);
                            mContext.startActivity(intent);
                        } else  {
                            Toast.makeText(mContext,
                                    "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogBuilder.dismiss();
                    }
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();

                //Comment code by hardik_kanak 26-11-2020

                /*Bundle bundle = new Bundle();
                bundle.putString("BatchID", listFees.get(i).getBatchID());
                bundle.putString("ClientID", listFees.get(i).getClientID());
                bundle.putString("InstituteID", listFees.get(i).getInstituteID());
                bundle.putString("FeeStructureID", listFees.get(i).getFeesStructuteID());
                bundle.putString("FeeStructureScheduleID", listFees.get(i).getFeesStructureScheduleID());
                bundle.putString("UnpaidAmount", String.valueOf(listFees.get(i).getUnpaid_amount()));
                bundle.putString("PaymentGateWayMasterID", String.valueOf(listFees.get(i).getPaymentgatewaymasterid()));
                Intent intent = new Intent(mContext, PaymentActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent); */

            }

        });

        imgremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, ReciptListFragment.class);
                mContext.startActivity(i);

            }

        });

        return view;

    }


    @Override
    public void response(String result, String methodName) {

    }

}
