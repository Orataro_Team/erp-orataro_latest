package com.edusunsoft.erp.orataro.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "CMSPage")
public class CmsPageListModel implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "CMSPagesID")
    @SerializedName("CMSPagesID")
    public String CMSPagesID;

    @ColumnInfo(name = "Date")
    @SerializedName("Date")
    public String Date;

    @ColumnInfo(name = "ImageURl")
    @SerializedName("ImageURl")
    public String ImageURl;

    @ColumnInfo(name = "PageType")
    @SerializedName("PageType")
    public String PageType;

    @ColumnInfo(name = "PageTitle")
    @SerializedName("PageTitle")
    public String PageTitle;

    @ColumnInfo(name = "PageName")
    @SerializedName("PageName")
    public String PageName;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getImageURl() {
        return ImageURl;
    }

    public void setImageURl(String imageURl) {
        ImageURl = imageURl;
    }

    public String getPageType() {
        return PageType;
    }

    public void setPageType(String pageType) {
        PageType = pageType;
    }

    public String getCMSPagesID() {
        return CMSPagesID;
    }

    public void setCMSPagesID(String cMSPagesID) {
        CMSPagesID = cMSPagesID;
    }

    public String getPageName() {
        return PageName;
    }

    public void setPageName(String pageName) {
        PageName = pageName;
    }

    public String getPageTitle() {
        return PageTitle;
    }

    public void setPageTitle(String pageTitle) {
        PageTitle = pageTitle;
    }

    @Override
    public String toString() {
        return "CmsPageListModel{" +
                "id=" + id +
                ", CMSPagesID='" + CMSPagesID + '\'' +
                ", Date='" + Date + '\'' +
                ", ImageURl='" + ImageURl + '\'' +
                ", PageType='" + PageType + '\'' +
                ", PageTitle='" + PageTitle + '\'' +
                ", PageName='" + PageName + '\'' +
                '}';
    }
}
