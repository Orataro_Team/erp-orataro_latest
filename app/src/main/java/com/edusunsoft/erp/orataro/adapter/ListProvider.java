package com.edusunsoft.erp.orataro.adapter;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ListProvider implements RemoteViewsFactory {
    private static final int THUMBNAIL_SIZE = 500;
    private ArrayList listItemList = new ArrayList();
    private Context context = null;
    private int appWidgetId;
    private String result;
    private LoginModel model;
    int count = 0;

    public ListProvider(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
        AddUserData(context);
        count = Global.userdataList.size();
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     *Similar to getView of Adapter where instead of View
     *we return RemoteViews
     *
     */
    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.switch_student_listraw);
        //ListItem listItem = listItemList.get(position);
        remoteView.setTextViewText(R.id.tv_std_name, Global.userdataList.get(position).getFullName());
        remoteView.setTextViewText(R.id.tv_std_standard, Global.userdataList.get(position).getGradeName());
        remoteView.setTextViewText(R.id.tv_std_division, Global.userdataList.get(position).getDivisionName());
        remoteView.setTextViewText(R.id.tv_usertype, Global.userdataList.get(position).getRoleName());
        remoteView.setTextViewText(R.id.tv_schoolname, Global.userdataList.get(position).getInstituteName());
        ImageView imgview = new ImageView(context);

        Bitmap image = null;
        remoteView.setImageViewUri(R.id.iv_std_icon, Uri.parse(Global.userdataList.get(position).getBtmProfilepic()));

        if (Global.userdataList.get(position).getUserType() == ServiceResource.USER_STUDENT_INT ||
                Global.userdataList.get(position).getUserType() == ServiceResource.USER_PARENTS_INT) {
        } else {
            remoteView.setViewVisibility(R.id.iv_std, View.INVISIBLE);
            remoteView.setViewVisibility(R.id.tv_std_standard, View.INVISIBLE);
            remoteView.setViewVisibility(R.id.ll_division, View.INVISIBLE);
            remoteView.setViewVisibility(R.id.ll_batches, View.INVISIBLE);
        }

        remoteView.setViewVisibility(R.id.ll_extra, View.VISIBLE);

        Intent intentcircular = new Intent();
        Intent intentHomework = new Intent();
        Intent intentclasswork = new Intent();
        intentcircular.putExtra("position", 1);
        intentHomework.putExtra("position", 3);
        intentclasswork.putExtra("position", 4);
        intentcircular.putExtra("userposition", position);
        intentHomework.putExtra("userposition", position);
        intentclasswork.putExtra("userposition", position);
        intentcircular.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentHomework.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentclasswork.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingcircular = PendingIntent.getActivity(context, 0, intentcircular, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingHomework = PendingIntent.getActivity(context, 0, intentHomework, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingclasswork = PendingIntent.getActivity(context, 0, intentclasswork, PendingIntent.FLAG_UPDATE_CURRENT);

        remoteView.setOnClickFillInIntent(R.id.img_homework, intentHomework);
        remoteView.setOnClickFillInIntent(R.id.img_classwork, intentclasswork);
        remoteView.setOnClickFillInIntent(R.id.img_circular, intentcircular);

        return remoteView;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public RemoteViews getLoadingView() {
        final RemoteViews remoteView = new RemoteViews(
                context.getPackageName(), R.layout.switch_student_listraw);
        remoteView.setViewVisibility(R.id.ll_extra, View.VISIBLE);
        return remoteView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean hasStableIds() {

        return false;
    }

    public void AddUserData(Context mContext) {

        result = new UserSharedPrefrence(mContext).getLOGIN_JSON();
        Log.d("responseuserlogin", result);

        JSONArray jsonObj;

        try {
            Global.userdataList = new ArrayList<LoginModel>();

            if (result.contains("\"success\":0")) {

            } else {

                jsonObj = new JSONArray(result);
                // JSONArray detailArrray= jsonObj.getJSONArray("");
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new LoginModel();
                    model = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                    if (ServiceResource.SINGLE_DEVICE_FLAG) {
                        if (!innerObj
                                .getString(ServiceResource.LOGIN_INSTITUTEID).equalsIgnoreCase(ServiceResource.STATICINSTITUTEID)) {
                            if (innerObj
                                    .getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID))) {
                                Global.userdataList.add(model);
                            } else {

                            }
                        } else {
                            Global.userdataList.add(model);
                        }
                    } else {
                        Global.userdataList.add(model);
                    }

                }
            }


        } catch (JSONException e) {

            e.printStackTrace();
        }


    }

    public static Bitmap getThumbnail(Uri uri, Context mContext) throws FileNotFoundException, IOException {
        InputStream input = mContext.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = mContext.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    private void setBitmap(RemoteViews views, int resId, Bitmap bitmap) {
        Bitmap proxy = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(proxy);
        c.drawBitmap(bitmap, new Matrix(), null);
        views.setImageViewBitmap(resId, proxy);
    }


}
