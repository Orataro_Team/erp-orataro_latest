package com.edusunsoft.erp.orataro.model;


public class LoginModel {

    private String UserID, DisplayName, MemberID, ClientID, ERPLoginID, InstituteID, RoleID,
            RoleName, WallID, ProfilePicture, WallImage, PrimaryName, InstituteLogo, StudentHostelRegID, StudentBusRegID;
    private String GradeID, DivisionID, PostByType, instituteWallId,
            standarWallId, divisionWallId, subjectWallId, ProfileWallId, currentWallId;
    private String InstitutionWallImage,InstitutionWallID, FullName, InstituteName, MemberType;
    private String firstName;
    private String MobileNumber, StudentCode, InstituteCode = null,ExamTimeTable,ERPMemberID,ERPUserID,DeviceIdentity,StudentBusRegMasterID = null;
    private String btmProfilepic;
    private String BatchStart, BatchEnd, BatchID;
    private String IsAllowUserToPostStatus, IsAllowUserToPostComment, IsAllowUserToPostPhoto;
    private String IsAllowUserToPostVideo, IsAllowUserToPostFiles, IsAllowUserToSharePost, IsAllowUserToPostAlbum;
    private String IsAllowUserToLikeDislikes;
    private int UserType;
    private String loginJson;
    private boolean save;
    private String GradeName, DivisionName, Prayer, Information, SchoolTiming, ctoken = "";

    public String getStudentBusRegMasterID() {
        return StudentBusRegMasterID;
    }

    public void setStudentBusRegMasterID(String studentBusRegMasterID) {
        StudentBusRegMasterID = studentBusRegMasterID;
    }

    public String getDeviceIdentity() {
        return DeviceIdentity;
    }

    public void setDeviceIdentity(String deviceIdentity) {
        DeviceIdentity = deviceIdentity;
    }

    public String getERPUserID() {
        return ERPUserID;
    }

    public void setERPUserID(String ERPUserID) {
        this.ERPUserID = ERPUserID;
    }

    public String getExamTimeTable() {
        return ExamTimeTable;
    }

    public void setExamTimeTable(String examTimeTable) {
        ExamTimeTable = examTimeTable;
    }

    public String getInstitutionWallID() {
        return InstitutionWallID;
    }

    public void setInstitutionWallID(String institutionWallID) {
        InstitutionWallID = institutionWallID;
    }

    public String getCtoken() {
        return ctoken;
    }

    public void setCtoken(String ctoken) {
        this.ctoken = ctoken;
    }

    public String getStudentBusRegID() {
        return StudentBusRegMasterID;
    }

    public void setStudentBusRegID(String studentBusRegMasterID) {
        StudentBusRegMasterID = studentBusRegMasterID;
    }

    public String getStudentHostelRegID() {
        return StudentHostelRegID;
    }

    public void setStudentHostelRegID(String studentHostelRegID) {
        StudentHostelRegID = studentHostelRegID;
    }

    public String getERPLoginID() {
        return ERPLoginID;
    }

    public void setERPLoginID(String ERPLoginID) {
        this.ERPLoginID = ERPLoginID;
    }

    public String isAllowUserToPostAlbum() {
        return IsAllowUserToPostAlbum;
    }

    public void setAllowUserToPostAlbum(String allowUserToPostAlbum) {
        IsAllowUserToPostAlbum = allowUserToPostAlbum;
    }

    public String getInstituteLogo() {
        return InstituteLogo;
    }

    public void setInstituteLogo(String instituteLogo) {
        InstituteLogo = instituteLogo;
    }

    public String getBatchStart() {
        return BatchStart;
    }

    public void setBatchStart(String BatchStart) {
        this.BatchStart = BatchStart;
    }

    public String getBatchEnd() {
        return BatchEnd;
    }

    public void setBatchEnd(String BatchEnd) {
        this.BatchEnd = BatchEnd;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String BatchID) {
        this.BatchID = BatchID;
    }

    public String getBtmProfilepic() {
        return btmProfilepic;
    }

    public void setBtmProfilepic(String btmProfilepic) {
        this.btmProfilepic = btmProfilepic;
    }

    public String getInstituteCode() {
        return InstituteCode;
    }

    public void setInstituteCode(String instituteCode) {
        InstituteCode = instituteCode;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getStudentCode() {
        return StudentCode;
    }

    public void setStudentCode(String studentCode) {
        StudentCode = studentCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMemberType() {
        return MemberType;
    }

    public void setMemberType(String memberType) {
        MemberType = memberType;
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String instituteName) {
        InstituteName = instituteName;
    }

    public String isIsAllowUserToPostStatus() {
        return IsAllowUserToPostStatus;
    }

    public void setIsAllowUserToPostStatus(String isAllowUserToPostStatus) {
        IsAllowUserToPostStatus = isAllowUserToPostStatus;
    }

    public String isIsAllowUserToPostComment() {
        return IsAllowUserToPostComment;
    }

    public void setIsAllowUserToPostComment(String isAllowUserToPostComment) {
        IsAllowUserToPostComment = isAllowUserToPostComment;
    }

    public String isIsAllowUserToPostPhoto() {
        return IsAllowUserToPostPhoto;
    }

    public void setIsAllowUserToPostPhoto(String isAllowUserToPostPhoto) {
        IsAllowUserToPostPhoto = isAllowUserToPostPhoto;
    }

    public String isIsAllowUserToPostVideo() {
        return IsAllowUserToPostVideo;
    }

    public void setIsAllowUserToPostVideo(String isAllowUserToPostVideo) {
        IsAllowUserToPostVideo = isAllowUserToPostVideo;
    }

    public String isIsAllowUserToPostFiles() {
        return IsAllowUserToPostFiles;
    }

    public void setIsAllowUserToPostFiles(String isAllowUserToPostFiles) {
        IsAllowUserToPostFiles = isAllowUserToPostFiles;
    }

    public String isIsAllowUserToSharePost() {
        return IsAllowUserToSharePost;
    }

    public void setIsAllowUserToSharePost(String isAllowUserToSharePost) {
        IsAllowUserToSharePost = isAllowUserToSharePost;
    }

    public String isIsAllowUserToLikeDislikes() {
        return IsAllowUserToLikeDislikes;
    }

    public void setIsAllowUserToLikeDislikes(String isAllowUserToLikeDislikes) {
        IsAllowUserToLikeDislikes = isAllowUserToLikeDislikes;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getPrayer() {
        return Prayer;
    }

    public void setPrayer(String prayer) {
        Prayer = prayer;
    }

    public String getInformation() {
        return Information;
    }

    public void setInformation(String information) {
        Information = information;
    }

    public String getSchoolTiming() {
        return SchoolTiming;
    }

    public void setSchoolTiming(String schoolTiming) {
        SchoolTiming = schoolTiming;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getLoginJson() {
        return loginJson;
    }

    public void setLoginJson(String loginJson) {
        this.loginJson = loginJson;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String GradeID) {
        this.GradeID = GradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String DivisionID) {
        this.DivisionID = DivisionID;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

    public String getInstitutionWallImage() {
        return InstitutionWallImage;
    }

    public void setInstitutionWallImage(String institutionWallImage) {
        InstitutionWallImage = institutionWallImage;
    }

    public String getCurrentWallId() {
        return currentWallId;
    }

    public void setCurrentWallId(String currentWallId) {
        this.currentWallId = currentWallId;
    }

    public String getInstituteWallId() {
        return instituteWallId;
    }

    public void setInstituteWallId(String instituteWallId) {
        this.instituteWallId = instituteWallId;
    }

    public String getStandarWallId() {
        return standarWallId;
    }

    public void setStandarWallId(String standarWallId) {
        this.standarWallId = standarWallId;
    }

    public String getDivisionWallId() {
        return divisionWallId;
    }

    public void setDivisionWallId(String divisionWallId) {
        this.divisionWallId = divisionWallId;
    }

    public String getSubjectWallId() {
        return subjectWallId;
    }

    public void setSubjectWallId(String subjectWallId) {
        this.subjectWallId = subjectWallId;
    }

    public String getProfileWallId() {
        return ProfileWallId;
    }

    public void setProfileWallId(String profileWallId) {
        ProfileWallId = profileWallId;
    }

    public String getPostByType() {
        return PostByType;
    }

    public void setPostByType(String postByType) {
        PostByType = postByType;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getERPMemberID() {
        return ERPLoginID;
    }

    public void setERPMemberID(String erpLoginID) {
        ERPLoginID = erpLoginID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }

    public String getRoleID() {
        return RoleID;
    }

    public void setRoleID(String roleID) {
        RoleID = roleID;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getWallImage() {
        return WallImage;
    }

    public void setWallImage(String wallImage) {
        WallImage = wallImage;
    }

    public String getPrimaryName() {
        return PrimaryName;
    }

    public void setPrimaryName(String primaryName) {
        PrimaryName = primaryName;
    }


    @Override
    public String toString() {
        return "LoginModel{" +
                "UserID='" + UserID + '\'' +
                ", DisplayName='" + DisplayName + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", ClientID='" + ClientID + '\'' +
                ", ERPLoginID='" + ERPLoginID + '\'' +
                ", InstituteID='" + InstituteID + '\'' +
                ", RoleID='" + RoleID + '\'' +
                ", RoleName='" + RoleName + '\'' +
                ", WallID='" + WallID + '\'' +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", WallImage='" + WallImage + '\'' +
                ", PrimaryName='" + PrimaryName + '\'' +
                ", InstituteLogo='" + InstituteLogo + '\'' +
                ", StudentHostelRegID='" + StudentHostelRegID + '\'' +
                ", StudentBusRegMasterID='" + StudentBusRegMasterID + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", PostByType='" + PostByType + '\'' +
                ", instituteWallId='" + instituteWallId + '\'' +
                ", standarWallId='" + standarWallId + '\'' +
                ", divisionWallId='" + divisionWallId + '\'' +
                ", subjectWallId='" + subjectWallId + '\'' +
                ", ProfileWallId='" + ProfileWallId + '\'' +
                ", currentWallId='" + currentWallId + '\'' +
                ", InstitutionWallImage='" + InstitutionWallImage + '\'' +
                ", FullName='" + FullName + '\'' +
                ", InstituteName='" + InstituteName + '\'' +
                ", MemberType='" + MemberType + '\'' +
                ", firstName='" + firstName + '\'' +
                ", MobileNumber='" + MobileNumber + '\'' +
                ", StudentCode='" + StudentCode + '\'' +
                ", InstituteCode='" + InstituteCode + '\'' +
                ", btmProfilepic='" + btmProfilepic + '\'' +
                ", BatchStart='" + BatchStart + '\'' +
                ", BatchEnd='" + BatchEnd + '\'' +
                ", BatchID='" + BatchID + '\'' +
                ", IsAllowUserToPostStatus=" + IsAllowUserToPostStatus +
                ", IsAllowUserToPostComment=" + IsAllowUserToPostComment +
                ", IsAllowUserToPostPhoto=" + IsAllowUserToPostPhoto +
                ", IsAllowUserToPostVideo=" + IsAllowUserToPostVideo +
                ", IsAllowUserToPostFiles=" + IsAllowUserToPostFiles +
                ", IsAllowUserToSharePost=" + IsAllowUserToSharePost +
                ", IsAllowUserToPostAlbum=" + IsAllowUserToPostAlbum +
                ", IsAllowUserToLikeDislikes=" + IsAllowUserToLikeDislikes +
                ", UserType=" + UserType +
                ", loginJson='" + loginJson + '\'' +
                ", save=" + save +
                ", GradeName='" + GradeName + '\'' +
                ", DivisionName='" + DivisionName + '\'' +
                ", Prayer='" + Prayer + '\'' +
                ", Information='" + Information + '\'' +
                ", SchoolTiming='" + SchoolTiming + '\'' +
                ", ctoken='" + ctoken + '\'' +
                '}';
    }
}
