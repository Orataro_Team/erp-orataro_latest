package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPollActivity;
import com.edusunsoft.erp.orataro.activities.PollResultActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.PollOptionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class PollListAdapter extends BaseAdapter implements ResponseWebServices {

    private static final int ID_CONFIGURATION = 1;
    private static final int ID_QUESTION = 2;
    private static final int ID_RESULT = 3;
    private static final int ID_SHARE = 4;
    private static final int ID_DELETE = 7;
    private static final int ID_VOTE = 8;
    private static final int ID_ERASE = 5;
    private static final int ID_OK = 6;

    private LayoutInflater layoutInfalater;
    private Context context;
    private boolean isStudent;
    private ArrayList<PollModel> pollList, copyList;
    private ArrayList<PollOptionModel> optionList, tempOptionList;
    private String polloptionId = "";
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    public Dialog voteListDialog;
    public int Pos;
    private RefreshListner listner;

    public PollListAdapter(Context context, ArrayList<PollModel> list, boolean isStudent, RefreshListner listner) {
        this.context = context;
        this.pollList = list;
        copyList = new ArrayList<PollModel>();
        copyList.addAll(list);
        this.isStudent = isStudent;
        this.listner = listner;
    }

    public PollListAdapter(Context context, ArrayList<PollModel> list, boolean isStudent, ArrayList<PollOptionModel> optionList, RefreshListner listner) {
        this.context = context;
        this.pollList = list;
        copyList = new ArrayList<PollModel>();
        copyList.addAll(list);
        this.isStudent = isStudent;
        this.optionList = optionList;
        this.listner = listner;
    }

    @Override
    public int getCount() {

        return pollList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        /*
         * ActionItem actionConfiguration = new ActionItem(ID_CONFIGURATION,
         * "Configuration", context.getResources().getDrawable(
         * R.drawable.configuration));
         */
        ActionItem actionQuestion = new ActionItem(ID_QUESTION, "Answer",
                context.getResources()
                        .getDrawable(R.drawable.dash_information1));
        ActionItem actionResult = null;
        if (!isStudent) {
            if (Integer.valueOf(pollList.get(position).getStartIn()) > 0) {
                actionResult = new ActionItem(ID_RESULT, "Edit", context
                        .getResources().getDrawable(R.drawable.edit));
            } else {
                actionResult = new ActionItem(ID_RESULT, "Result", context
                        .getResources().getDrawable(R.drawable.fb_result));
            }
        }
        ActionItem actionShare = new ActionItem(ID_SHARE, "Share", context
                .getResources().getDrawable(R.drawable.share_white));

        ActionItem actionDelete = new ActionItem(ID_DELETE, "Delete", context
                .getResources().getDrawable(R.drawable.delete));

        ActionItem actionVote = new ActionItem(ID_VOTE, "Vote", context
                .getResources().getDrawable(R.drawable.political5));

        // use setSticky(true) to disable QuickAction dialog being dismissed
        // after an item is clicked
        // .setSticky(true);
        actionQuestion.setSticky(true);

        layoutInfalater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.pollraw, parent, false);

        LinearLayout ll_menu = (LinearLayout) convertView.findViewById(R.id.ll_menu);
        ll_menu.setTag(position + "");
        ImageView list_image = (ImageView) convertView.findViewById(R.id.list_image);
        LinearLayout ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        TextView txtSub = (TextView) convertView.findViewById(R.id.txt_subject);
        TextView txtcreateby = (TextView) convertView.findViewById(R.id.txtcreateby);
        TextView txtstartend = (TextView) convertView.findViewById(R.id.txtstartend);
        TextView txtstartin = (TextView) convertView.findViewById(R.id.txtstartinstatus);
        TextView participateCount = (TextView) convertView.findViewById(R.id.participateCount);
        TextView voteCount = (TextView) convertView.findViewById(R.id.voteCount);
        TextView tv_sub_detail = (TextView) convertView.findViewById(R.id.tv_sub_detail);
        TextView txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        TextView txtYear = (TextView) convertView.findViewById(R.id.txt_date);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        TextView txt_date = (TextView) convertView.findViewById(R.id.txtDate);
        LinearLayout ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        LinearLayout ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        LinearLayout llParticipate = (LinearLayout) convertView.findViewById(R.id.llParticipate);

        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            if (Utility.ReadWriteSetting(ServiceResource.POLL).getIsDelete() || Utility.ReadWriteSetting(ServiceResource.POLL).getIsEdit()) {
                ll_menu.setVisibility(View.VISIBLE);
            } else {
                ll_menu.setVisibility(View.GONE);
            }
        }
//        else {
//            ll_menu.setVisibility(View.GONE);
//        }

        // int colorPos = position % colors.length;
        convertView.setBackgroundColor(colors[position % colors.length]);

        // int colorPos1 = position % colors_list.length;
        ll_view.setBackgroundColor(colors_list[position
                % colors_list.length]);
        if (pollList.get(position).isChangeDate()) {
            ll_date.setVisibility(View.VISIBLE);
            if (Utility.isNull(pollList.get(position).getMonth())) {
                txt_month.setText(pollList.get(position).getMonth());
            }
        } else {
            ll_date.setVisibility(View.GONE);
        }

        if (Utility.isNull(pollList.get(position).getUserName())) {
            txtcreateby.setText(pollList.get(position).getUserName());
        }

        if (Utility.isNull(pollList.get(position).getDetails())) {
            tv_sub_detail.setText(pollList.get(position).getDetails());
        }

        if (Utility.isNull(pollList.get(position).getTitle())) {
            txtSub.setText(pollList.get(position).getTitle());
        }

        if (Utility.isNull(pollList.get(position).getDate())) {
            txt_date.setText(pollList.get(position).getDate());
        }

        if (Utility.isNull(pollList.get(position).getYear())) {
            txtYear.setText(pollList.get(position).getYear());
        }

        if (isStudent) {
            llParticipate.setVisibility(View.GONE);
            txtstartend.setText(context.getResources().getString(R.string.endin));
            txtstartin.setText(Utility.getDate(Long.valueOf(pollList.get(position).getEndDate()), "dd-MM-yyyy"));
            txtstartin.setTextColor(Color.parseColor("#000000"));
        } else {
            if (Utility.isNull(pollList.get(position).getParticipateCOunt())) {
                participateCount.setText(pollList.get(position).getParticipateCOunt());
            }
            if (Utility.isNull(pollList.get(position).getVoteCount())) {
                voteCount.setText(pollList.get(position).getVoteCount());
            }

            try {
                if (Integer.valueOf(pollList.get(position).getStartIn()) > 0) {
                    txtstartend.setText(context.getResources().getString(R.string.StartIn));
                    txtstartin.setText(pollList.get(position).getStartIn());
                } else if (Integer.valueOf(pollList.get(position).getEndIn()) > 0) {
                    txtstartend.setText(context.getResources().getString(R.string.endin));
                    txtstartin.setText(pollList.get(position).getEndIn());
                } else {
                    txtstartend.setText(context.getResources().getString(R.string.endin));
                    txtstartin.setText(context.getResources().getString(R.string.AlreadyEnd));
                    txtstartin.setTextColor(context.getResources().getColor(R.color.red));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final QuickAction quickAction = new QuickAction(context,
                QuickAction.VERTICAL);

        if (!isStudent) {
            quickAction.addActionItem(actionResult);
            if (Utility.isTeacher(context)) {
                if (Utility.ReadWriteSetting(ServiceResource.POLL).getIsDelete()) {
                    quickAction.addActionItem(actionDelete);
                } else {
//                    Utility.toast(context, ServiceResource.TOASTPERMISSIONMSG);
                }
            }

        } else {

            quickAction.addActionItem(actionVote);

        }

        // Set listener for action item clicked
        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        int posforUse = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                        ActionItem actionItem = quickAction.getActionItem(pos);
                        quickAction.dismiss();
                        // here we can filter which action item was clicked with
                        // pos or actionId parameter
                        if (!isStudent) {
                            if (actionId == ID_CONFIGURATION) {
                                Intent in = new Intent(context, AddPollActivity.class);
                                context.startActivity(in);
                            } else if (actionId == ID_RESULT) {
                                Intent in = new Intent(context, PollResultActivity.class);
                                in.putExtra("model", pollList.get(posforUse));
                                context.startActivity(in);
                            } else if (actionId == ID_DELETE) {
                                deletePoll(pollList.get(posforUse).getPollID());
                                pollList.remove(posforUse);
                                notifyDataSetChanged();
                            }
                        } else {
                            if (actionId == ID_VOTE) {
                                showDialogWithAns(posforUse);
                            }
                        }
                    }
                });

        ll_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                quickAction.show(v);

            }

        });

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!isStudent) {
                    Intent i = new Intent(context, AddPollActivity.class);
                    i.putExtra("isEdit", true);
                    i.putExtra("model", pollList.get(position));
                    context.startActivity(i);
                }

            }

        });

        return convertView;
    }

    public void deletePoll(String id) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID, id));
        new AsynsTaskClass(context, arrayList, false, this).execute(ServiceResource.POLLDELETE_METHODNAME, ServiceResource.POLL_URL);

    }

    public void saveVotePoll(String id) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POLLIDOPTIONIDBYCOMMAHASE, id));
        arrayList.add(new PropertyVo(ServiceResource.VMEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.VUSERID,
                new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BACHID,
                new UserSharedPrefrence(context).getLoginModel().getBatchID()));
        new AsynsTaskClass(context, arrayList, true, this, false, false).execute(ServiceResource.SAVEPOLLOPTIONVOTE_METHODNAME,
                ServiceResource.POLL_URL);

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        pollList.clear();
        if (charText.length() == 0) {
            pollList.addAll(copyList);
        } else {
            for (PollModel vo : copyList) {
                if (vo.getSubName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    pollList.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.POLLDELETE_METHODNAME.equalsIgnoreCase(methodName)) {

        } else if (ServiceResource.SAVEPOLLOPTIONVOTE_METHODNAME.equalsIgnoreCase(methodName)) {

            listner.refresh(methodName);

        }

    }

    private void showDialogWithAns(int posforUse) {

        Pos = posforUse;

        tempOptionList = new ArrayList<PollOptionModel>();

        Log.d("getVoteListtt", pollList.toString());

        for (int i = 0; i < optionList.size(); i++) {

            if (optionList.get(i).getPollID().equalsIgnoreCase(pollList.get(posforUse).getPollID())) {
                tempOptionList.add(optionList.get(i));
            }

        }

        voteListDialog = CustomDialog.ShowDialog(context, R.layout.dialogvotelist, true);

//        final Dialog voteListDialog = new Dialog(context);
//        voteListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        voteListDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        voteListDialog.setContentView(R.layout.dialogvotelist);
//        voteListDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        voteListDialog.setCancelable(true);
//        voteListDialog.show();


        final LinearLayout llAddChoice = (LinearLayout) voteListDialog.findViewById(R.id.llAddChoice);
        TextView txttitle = (TextView) voteListDialog.findViewById(R.id.txttitle);
        final ImageView imgclose = (ImageView) voteListDialog.findViewById(R.id.imgclose);
        Button btnok = (Button) voteListDialog.findViewById(R.id.btnok);
        Button btncancle = (Button) voteListDialog.findViewById(R.id.btncancle);
        txttitle.setText(pollList.get(posforUse).getTitle());
        imgclose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                voteListDialog.dismiss();

            }

        });

        Log.d("getvotelist", tempOptionList.toString());
        for (int i = 0; i < tempOptionList.size(); i++) {
            layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = layoutInfalater.inflate(R.layout.votecheckbox, null);
            final CheckBox chkvote = (CheckBox) v.findViewById(R.id.chkvote);
            TextView txtPollans = (TextView) v.findViewById(R.id.txtPollans);
            TextView txtPolloptionId = (TextView) v.findViewById(R.id.txtPolloptionId);
            txtPolloptionId.setText(tempOptionList.get(i).getPollOptionID());
            if (!pollList.get(posforUse).getIsMultichoice()) {
                chkvote.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int count = llAddChoice.getChildCount();

                        for (int k = 0; k < count; k++) {
                            View vChild = null;
                            vChild = llAddChoice.getChildAt(k);
                            if (vChild instanceof LinearLayout) {
                                int childCount = ((LinearLayout) vChild).getChildCount();
                                for (int j = 0; j < childCount; j++) {
                                    View ChildView = ((LinearLayout) vChild).getChildAt(j);
                                    if (ChildView instanceof CheckBox) {
                                        CheckBox edtAnswer = (CheckBox) ChildView;
                                        edtAnswer.setChecked(false);
                                    }
                                }

                            }
                        }
                        chkvote.setChecked(isChecked);
                    }
                });
            }

            txtPollans.setText(tempOptionList.get(i).getOption());

            llAddChoice.addView(v);

        }

        btnok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                int count = llAddChoice.getChildCount();
                polloptionId = "";
                for (int k = 0; k < count; k++) {
                    View vChild = null;
                    boolean isChecked = false;
                    vChild = llAddChoice.getChildAt(k);
                    if (vChild instanceof LinearLayout) {
                        int childCount = ((LinearLayout) vChild).getChildCount();
                        for (int j = 0; j < childCount; j++) {
                            View ChildView = ((LinearLayout) vChild).getChildAt(j);
                            if (ChildView instanceof CheckBox) {
                                CheckBox edtAnswer = (CheckBox) ChildView;
                                isChecked = edtAnswer.isChecked();
                            }
                            if (ChildView instanceof TextView) {
                                TextView edtAnswer = (TextView) ChildView;
                                if (isChecked) {
                                    if (edtAnswer.getId() == R.id.txtPolloptionId) {
                                        polloptionId += tempOptionList.get(k).getPollID() + "," + edtAnswer.getText().toString() + ",#";
                                    }
                                }
                            }

                        }

                    }
                }

                saveVotePoll(polloptionId);
                Log.v("idPolloption ", polloptionId + "fdsfds");
                voteListDialog.dismiss();

            }

        });

        btncancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                voteListDialog.dismiss();

            }

        });

    }

}
