package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface HolidayDataDao {

    @Insert
    void insertHoliday(HolidaysModel holidaysModel);

    @Query("SELECT * from Holiday")
    List<HolidaysModel> getHolidayList();

    @Query("delete from Holiday")
    void deleteHoliday();

}
