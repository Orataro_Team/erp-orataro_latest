package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewParentsPageDetailsActivity;
import com.edusunsoft.erp.orataro.model.ParentsPageModel;
import com.edusunsoft.erp.orataro.util.Constants;

import java.util.ArrayList;
import java.util.Locale;

public class ParentPageListAdapter extends BaseAdapter {

	private LayoutInflater layoutInfalater;
	private Context mContext;
	private TextView homeworkListSubject;
	private ImageView list_image;
	private TextView txt_month;
	private TextView txt_date, txt_pr_date, txt_pr_note, txt_pr_to_teacher;
	private LinearLayout ll_date;
	private LinearLayout ll_view, ll_date_img;
	private ArrayList<ParentsPageModel> parentsPageModels, copyList;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };

	public ParentPageListAdapter(Context context, ArrayList<ParentsPageModel> parentsPageModels) {
		mContext = context;
		this.parentsPageModels = parentsPageModels;
		copyList = new ArrayList<ParentsPageModel>();
		copyList.addAll(parentsPageModels);
	}

	@Override
	public int getCount() {
		return parentsPageModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.parent_page_listraw, parent, false);

		ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
		list_image = (ImageView) convertView.findViewById(R.id.list_image);
		txt_month = (TextView) convertView.findViewById(R.id.txt_month);
		txt_date = (TextView) convertView.findViewById(R.id.txt_date);
		txt_pr_date = (TextView) convertView.findViewById(R.id.txt_pr_date);
		txt_pr_note = (TextView) convertView.findViewById(R.id.txt_pr_note);
		txt_pr_to_teacher = (TextView) convertView.findViewById(R.id.txt_pr_to_teacher);
		ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
		ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);

		int colorPos = position % colors.length;
		convertView.setBackgroundColor(colors[colorPos]);
		int colorPos1 = position % colors_list.length;
		ll_view.setBackgroundColor(colors_list[colorPos1]);

		if (parentsPageModels.get(position).isVisibleMonth() == true) {
			ll_date.setVisibility(View.VISIBLE);
		} else {
			ll_date.setVisibility(View.GONE);
		}

		txt_pr_date.setText(parentsPageModels.get(position).getPr_date());
		txt_pr_note.setText(parentsPageModels.get(position).getPr_note());
		txt_pr_to_teacher.setText(parentsPageModels.get(position).getPr_to_teacher());
		txt_month.setText(parentsPageModels.get(position).getPr_month());

		if (parentsPageModels.get(position).getPr_note() != null) {

			if (parentsPageModels.get(position).getPr_note().length() > Constants.CONTINUEREADINGSIZE) {
				String continueReadingStr = Constants.CONTINUEREADINGSTR;
				String tempText = parentsPageModels.get(position).getPr_note()
						.substring(0, Constants.CONTINUEREADINGSIZE)
						+ continueReadingStr;

				SpannableString text = new SpannableString(tempText);

				text.setSpan(
						new ForegroundColorSpan(
								Constants.CONTINUEREADINGTEXTCOLOR),
						Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE
								+ continueReadingStr.length(), 0);

				MyClickableSpan clickfor = new MyClickableSpan(
						tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

					@Override
					public void onClick(View widget) {
						Intent intent = new Intent(mContext, ViewParentsPageDetailsActivity.class);
						intent.putExtra("Pr_note", parentsPageModels.get(position).getPr_note());
						intent.putExtra("Pr_Teacher_name", parentsPageModels.get(position).getPr_to_teacher());
						mContext.startActivity(intent);
					}
				};

				text.setSpan(
						clickfor,
						Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE
								+ continueReadingStr.length(), 0);

				txt_pr_note.setMovementMethod(LinkMovementMethod.getInstance());
				txt_pr_note.setText(text, BufferType.SPANNABLE);
			} else {
				txt_pr_note.setText(parentsPageModels.get(position).getPr_note());
			}

		}

		txt_pr_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, ViewParentsPageDetailsActivity.class);
				intent.putExtra("Pr_note", parentsPageModels.get(position).getPr_note());
				intent.putExtra("Pr_Teacher_name", parentsPageModels.get(position).getPr_to_teacher());
				mContext.startActivity(intent);
			}
		});

		return convertView;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		parentsPageModels.clear();
		if (charText.length() == 0) {
			parentsPageModels.addAll(copyList);
		} else {
			for (ParentsPageModel vo : copyList) {
				if (vo.getPr_note().toLowerCase(Locale.getDefault()).contains(charText)) {
					parentsPageModels.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}

}
