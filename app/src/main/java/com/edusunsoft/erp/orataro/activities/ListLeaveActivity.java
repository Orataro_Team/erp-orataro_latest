package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.LeaveListAdapter;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.LeaveListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ListLeaveActivity extends Activity implements ResponseWebServices, RefreshListner {

    Context mContext;
    private LeaveListAdapter adapter;
    ListView mCircularListView;
    private TextView txt_nodatafound;

    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;

    StdDivSubModel stdandardModel;

    LinearLayout ll_add_new;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_leave);
        mContext = this;

        stdandardModel = (StdDivSubModel) getIntent().getSerializableExtra("model");

        mCircularListView = (ListView) findViewById(R.id.homeworklist);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);

        try {
            txtHeader.setText(getResources().getString(R.string.leave) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
        }

        imgLeftheader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utility.RedirectToDashboard(ListLeaveActivity.this);

            }

        });

        Utility.ISLOADLEAVELIST = false;

        if (Utility.isNetworkAvailable(mContext)) {
            getLeaveList(true);
        }

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(mContext)) {
                    Intent i = new Intent(mContext, AddLeaveActivity.class);
                    startActivity(i);
                } else {
                    Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                }

            }

        });

        if (Utility.isTeacher(mContext)) {
            ll_add_new.setVisibility(View.GONE);
        }

    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Utility.RedirectToDashboard(ListLeaveActivity.this);

    }

    @Override
    protected void onResume() {

        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            if (Utility.ISLOADLEAVELIST) {
                getLeaveList(true);
            }
        }

    }

    public void getLeaveList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, stdandardModel.getStandrdId()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, stdandardModel.getDivisionId()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID,
                    new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        }

        arrayList.add(new PropertyVo(ServiceResource.MEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        Log.d("leave", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.LEAVE_METHODNAME, ServiceResource.LEAVE_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.LEAVE_METHODNAME.equalsIgnoreCase(methodName)) {

            if (Utility.isTeacher(mContext)) {

                Utility.writeToFile(result, ServiceResource.LEAVE_METHODNAME + stdandardModel.getStandrdId() + stdandardModel.getDivisionId(), mContext);

            } else {

                Utility.writeToFile(result, ServiceResource.LEAVE_METHODNAME + new UserSharedPrefrence(mContext).getLoginModel().getGradeID() +
                        new UserSharedPrefrence(mContext).getLoginModel().getDivisionID(), mContext);

            }

            parseleavelist(result);

        }

        Log.e("", result.toString());

    }

    public void parseleavelist(String result) {

        Log.d("applicationBy", result);

        JSONArray jsonObj;
        try {
            Global.leaveListModels = new ArrayList<LeaveListModel>();
            jsonObj = new JSONArray(result);
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                LeaveListModel model = new LeaveListModel();
                String dateParse = innerObj.getString(ServiceResource.LEAVE_DATEOFAPPLICATION);

                if (dateParse != null && !dateParse.equalsIgnoreCase("")

                        && !dateParse.equalsIgnoreCase("null")) {
                    String date = Utility
                            .getDate(Long.valueOf(innerObj.getString(ServiceResource.LEAVE_DATEOFAPPLICATION)
                                            .replace("/Date(", "")
                                            .replace(")/", "")),
                                    "EEEE/dd/MMM/yyyy/");

                    String[] dateArray = date.split("/");

                    if (dateArray != null && dateArray.length > 0) {

                        Log.v("date", (dateArray[0] + "" + dateArray[1]
                                + "" + dateArray[2] + "" + dateArray[3]));
                        model.setCr_date(dateArray[1] + " "
                                + dateArray[0].substring(0, 3));
                        model.setCr_month(dateArray[2]);
                        model.setYear(dateArray[3]);

                    }

                    model.setMilliSecond(innerObj.getString(ServiceResource.LEAVE_DATEOFAPPLICATION)
                            .replace("/Date(", "").replace(")/", ""));
                    model.setSchoolLeaveNoteID(innerObj.getString(ServiceResource.LEAVE_SCHOOLLEAVENOTEID));
                    model.setDateOfApplication(innerObj.getString(ServiceResource.LEAVE_DATEOFAPPLICATION));
                    model.setApplicationStatus_Term(innerObj.getString(ServiceResource.LEAVE_APPLICATIONSTATUS_TERM));
                    model.setReasonForLeave(innerObj.getString(ServiceResource.LEAVE_REASONFORLEAVE));
                    model.setTeacherComment(innerObj.getString(ServiceResource.LEAVE_TEACHERCOMMENT));
                    model.setApprovedOn(innerObj.getString(ServiceResource.LEAVE_APPROVEDON));
                    model.setStartDate(innerObj.getString(ServiceResource.LEAVE_STARTDATE));
                    model.setEndDate(innerObj.getString(ServiceResource.ENDDATE));
                    model.setStudentName(innerObj.getString(ServiceResource.LEAVE_STUDENTNAME));
                    model.setApplicationBY(innerObj.getString(ServiceResource.LEAVE_APPLICATIONBY));
                    model.setTeacherName(innerObj.getString(ServiceResource.LEAVE_TEACHERNAME));
                    model.setApplicationBy_Term(innerObj.getString(ServiceResource.LEAVE_APPLICATIONBY_TERM));
                    model.setStudentID(innerObj.getString(ServiceResource.LEAVE_STUDENTID));
                    model.setDefStatus(innerObj.getString(ServiceResource.LEAVE_DEFSTATUS));
                    model.setIsPerApplication(innerObj.getString(ServiceResource.LEAVE_ISPERAPPLICATION));
                    model.setApplicationToTeacherID(innerObj.getString(ServiceResource.LEAVE_APPLICATIONTOTEACHERID));
                    Global.leaveListModels.add(model);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Global.leaveListModels != null && Global.leaveListModels.size() > 0) {
            Collections.sort(Global.leaveListModels, new SortedDate());
            Collections.reverse(Global.leaveListModels);

            for (int i = 0; i < Global.leaveListModels.size(); i++) {
                if (i != 0) {
                    String temp = Global.leaveListModels.get(i - 1).getCr_month();
                    if (temp.equalsIgnoreCase(Global.leaveListModels.get(i).getCr_month())) {
                        Global.leaveListModels.get(i).setVisibleMonth(false);
                    } else {
                        Global.leaveListModels.get(i).setVisibleMonth(true);
                    }
                } else {
                    Global.leaveListModels.get(i).setVisibleMonth(true);
                }

            }

        }

        if (Global.leaveListModels != null && Global.leaveListModels.size() > 0) {

            adapter = new LeaveListAdapter(mContext, Global.leaveListModels, ListLeaveActivity.this, stdandardModel);
            mCircularListView.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            mCircularListView.setVisibility(View.VISIBLE);

        } else {

            mCircularListView.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(
                    R.string.noleave));

        }

    }

    class SortedDate implements Comparator<LeaveListModel> {
        @Override
        public int compare(LeaveListModel o1, LeaveListModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }

    @Override
    public void refresh(String methodName) {
        getLeaveList(false);
    }

}
