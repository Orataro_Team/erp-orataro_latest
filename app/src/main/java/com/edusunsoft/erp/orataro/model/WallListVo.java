package com.edusunsoft.erp.orataro.model;

import com.edusunsoft.erp.orataro.database.DynamicWallListModel;

import java.util.ArrayList;

public class WallListVo {
    private int ico;
    private String wallName;
    private ArrayList<DynamicWallListModel> dynamicWallList = new ArrayList<>();

    public ArrayList<DynamicWallListModel> getDynamicWallList() {
        return dynamicWallList;
    }

    public void setDynamicWallList(ArrayList<DynamicWallListModel> dynamicWallList) {
        this.dynamicWallList = dynamicWallList;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getWallName() {
        return wallName;
    }

    public void setWallName(String wallName) {
        this.wallName = wallName;
    }

    @Override
    public String toString() {
        return "WallListVo{" +
                "ico=" + ico +
                ", wallName='" + wallName + '\'' +
                ", dynamicWallList=" + dynamicWallList +
                '}';
    }
}
