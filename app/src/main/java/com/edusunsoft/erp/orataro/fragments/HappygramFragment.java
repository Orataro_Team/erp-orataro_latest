package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddHappyGramActivity;
import com.edusunsoft.erp.orataro.adapter.DateListAdapter;
import com.edusunsoft.erp.orataro.adapter.MyHappyGramListAdapter;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static com.edusunsoft.erp.orataro.util.Utility.RedirectToDashboard;

public class HappygramFragment extends Activity implements OnItemClickListener, RefreshListner, OnClickListener, ResponseWebServices {

    private PullAndLoadListView mHappygramListView;
    private MyHappyGramListAdapter happy_gram_ListAdapter;
    private Context mContext;
    private LinearLayout ll_add_new;
    private DateListAdapter date_list_Adapter;
    private ListView lst_date;
    private Dialog mDialog;
    private Dialog mHappygramDialog;
    private ArrayList<MyHappyGramModel> happyGramModels;
    private MyHappyGramModel happyGramModel;
    private ImageView img_back;
    private EditText edtTeacherName;
    private LinearLayout searchlayout;
    private ImageView imgHeaderRight;
    private TextView txt_nodatafound, header_text;
    private StdDivSubModel stadardModel;
    private String gradeId, divisionId, subjectId, subjectName, isFrom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.happygram_fragment);

        mContext = this;

        if (getIntent() != null) {
            gradeId = getIntent().getStringExtra("gradeId");
            divisionId = getIntent().getStringExtra("divisionId");//subjectId,subjectName,isFrom
            subjectId = getIntent().getStringExtra("subjectId");
            subjectName = getIntent().getStringExtra("subjectName");
            isFrom = getIntent().getStringExtra("isFrom");
            stadardModel = (StdDivSubModel) getIntent().getSerializableExtra("model");
        }

        try {
            ((TextView) findViewById(R.id.header_text)).setText(getResources().getString(R.string.MyHappygram) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        mHappygramListView = (PullAndLoadListView) findViewById(R.id.homeworklist);
        edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);
        searchlayout = (LinearLayout) findViewById(R.id.searchlayout);
        header_text = (TextView) findViewById(R.id.header_text);
        ll_add_new.setVisibility(View.VISIBLE);
        img_back = (ImageView) findViewById(R.id.img_home);
        imgHeaderRight = (ImageView) findViewById(R.id.img_menu);

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.GONE);
        }

        img_back.setOnClickListener(this);
        ll_add_new.setOnClickListener(this);
        img_back.setImageResource(R.drawable.back);
        imgHeaderRight.setVisibility(View.INVISIBLE);
        searchlayout.setVisibility(View.VISIBLE);

        if (Utility.isTeacher(mContext)) {
            header_text.setText(stadardModel.getStandardName() + " " + stadardModel.getDivisionName() + " " + stadardModel.getSubjectName());
        }

        mHappygramListView.setOnItemClickListener(this);
        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                happy_gram_ListAdapter.filter(text);
            }
        });

        mHappygramListView.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {

                if (Utility.isNetworkAvailable(mContext)) {

                    Happygram(true);

                } else {

                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                }

            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            String result = Utility.readFromFile(ServiceResource.GETHAPPYGRAM_METHODNAME, mContext);
            Happygram(true);
        } else {
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//        if (Global.myHappyGramModels.size() > 0) {
//
//            Intent i = new Intent(mContext, AddHappyGramActivity.class);
//            i.putExtra("isEdit", true);
//            i.putExtra("model", Global.myHappyGramModels.get(position));
//            startActivity(i);
//            overridePendingTransition(0, 0);
//
//        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_add_new:

                Intent i = new Intent(HappygramFragment.this, AddHappyGramActivity.class);
                i.putExtra("gradeId", gradeId);
                i.putExtra("divisionId", divisionId);
                i.putExtra("subjectId", subjectId);
                i.putExtra("isFrom", isFrom);
                i.putExtra("stdmodel", stadardModel);
                startActivity(i);
                overridePendingTransition(0, 0);

                break;

            case R.id.img_home:

                RedirectToDashboard(this);


                break;

            default:

                break;

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        RedirectToDashboard(this);

    }

    @Override
    public void refresh(String methodName) {

        if (Utility.isNetworkAvailable(mContext)) {

            Happygram(true);

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    public class SortedDate implements Comparator<MyHappyGramModel> {
        @Override
        public int compare(MyHappyGramModel o1, MyHappyGramModel o2) {
            return o1.getDateInMillisecond().compareTo(
                    o2.getDateInMillisecond());
        }
    }

    public void Happygram(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, divisionId));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        }
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.ISSTUDNETS, false));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.ISSTUDNETS, true));
        }

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETHAPPYGRAM_METHODNAME, ServiceResource.HAPPYGRAM_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETHAPPYGRAM_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("getHappygranmList", result);
            Utility.writeToFile(result, methodName, mContext);
            parseHappyGram(result);

        }

    }

    public void parseHappyGram(String result) {

        JSONArray jsonObj;

        try {

            Global.myHappyGramModels = new ArrayList<MyHappyGramModel>();
            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                MyHappyGramModel happyGramModel = new MyHappyGramModel();
                String date = Utility
                        .getDate(Utility.dateToMilliSeconds(
                                innerObj.getString(ServiceResource.HAPPYGRAM_DATEOFHAPPYGRAM),
                                "dd MMM yyyy"), "EEEE/dd/MMM/yyyy/");

                String[] dateArray = date.split("/");

                if (dateArray != null && dateArray.length > 0) {

                    happyGramModel.setHg_date(dateArray[1] + " " + dateArray[0].substring(0, 3));
                    happyGramModel.setHg_month(dateArray[2].toUpperCase());
                    happyGramModel.setYear(dateArray[3]);

                }

                happyGramModel.setDateInMillisecond(""
                        + Utility.dateToMilliSeconds(
                        innerObj.getString(ServiceResource.HAPPYGRAM_DATEOFHAPPYGRAM),
                        "dd MMM yyyy"));
                happyGramModel.setHg_apprication(innerObj.getString(ServiceResource.HAPPYGRAM_APPRECIATION));
                happyGramModel.setHg_note(innerObj.getString(ServiceResource.HAPPYGRAM_NOTE));
                happyGramModel.setEmotion(innerObj.getString(ServiceResource.HAPPYGRAM_EMOTION));
                happyGramModel.setFullName(innerObj.getString(ServiceResource.HAPPYGRAM_FULLNAME));
                happyGramModel.setHappyGramID(innerObj.getString(ServiceResource.HAPPYGRAM_HAPPYGRAMID));
                happyGramModel.setMemberID(innerObj.getString(ServiceResource.HAPPYGRAM_MEMBERID));
                happyGramModel.setSubjectID(innerObj.getString(ServiceResource.HAPPYGRAM_SUBJECTID));
                happyGramModel.setSubjectName(innerObj.getString(ServiceResource.HAPPYGRAM_SUBJECTNAME));
                happyGramModel.setRegistationNo(innerObj.getString(ServiceResource.HAPPYGRAM_REGISTRATIONNO));
                Global.myHappyGramModels.add(happyGramModel);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        if (Global.myHappyGramModels != null && Global.myHappyGramModels.size() > 0) {

            Collections.sort(Global.myHappyGramModels, new SortedDate());
            Collections.reverse(Global.myHappyGramModels);

            for (int i = 0; i < Global.myHappyGramModels.size(); i++) {
                if (i != 0) {
                    String temp = Global.myHappyGramModels.get(i - 1).getHg_month();
                    if (temp.equalsIgnoreCase(Global.myHappyGramModels.get(i).getHg_month())) {
                        Global.myHappyGramModels.get(i).setVisibleMonth(false);
                    } else {
                        Global.myHappyGramModels.get(i).setVisibleMonth(true);
                    }
                } else {
                    Global.myHappyGramModels.get(i).setVisibleMonth(true);
                }
            }

            happy_gram_ListAdapter = new MyHappyGramListAdapter(mContext, Global.myHappyGramModels, stadardModel, this);
            mHappygramListView.setAdapter(happy_gram_ListAdapter);
            txt_nodatafound.setVisibility(View.GONE);

        } else {

            txt_nodatafound.setVisibility(View.VISIBLE);
            mHappygramListView.setAdapter(null);
            txt_nodatafound.setText(getResources().getString(R.string.NoHappygramAvailble));

        }

    }

}
