package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FeesListAdapter;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class FeesFragment extends Fragment implements ResponseWebServices {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Context mContext;

    TextView txt_total_amount, txt_title, txt_show_transaction_detail;
    ListView lvfees;
    LinearLayout ll_remove;
    FeesModel feesReceiptModel;
    ArrayList<FeesModel> FeesReceiptList = new ArrayList<>();
    Double TotalAmount = 0.0;

    public FeesFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeesFragment.
     */

    public static FeesFragment newInstance(String param1, String param2) {

        FeesFragment fragment = new FeesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertview = inflater.inflate(R.layout.fragment_fees, container, false);
        mContext = getActivity();
        txt_total_amount = (TextView) convertview.findViewById(R.id.txt_total_amount);
        txt_title = (TextView) convertview.findViewById(R.id.txt_title);
        txt_show_transaction_detail = (TextView) convertview.findViewById(R.id.txt_show_transaction_detail);
        CircleImageView iv_std_icon = (CircleImageView) convertview.findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) convertview.findViewById(R.id.tv_std_name);
        TextView tv_institutename = (TextView) convertview.findViewById(R.id.tv_schoolname);
        TextView tv_std_standard = (TextView) convertview.findViewById(R.id.tv_std_standard);
        TextView tv_std_division = (TextView) convertview.findViewById(R.id.tv_std_division);
        ll_remove = (LinearLayout) convertview.findViewById(R.id.ll_remove);
        lvfees = (ListView) convertview.findViewById(R.id.lvfees);

        if (HomeWorkFragmentActivity.txt_header != null) {
            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Fees) + " (" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
        }

        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(iv_std_icon);

            } catch (Exception e) {
                e.printStackTrace();
            }

            tv_std_name.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME()));
            tv_institutename.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getINSTITUTENAME()));
            tv_std_standard.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_GRADENAME()));
            tv_std_division.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_DIVISIONNAME()));

        }


        return convertview;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            feesinfo();
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
    }

    public void feesinfo() {

        Log.d("getBatchID12345", new UserSharedPrefrence(mContext).getLoginModel().getBatchID());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        Log.d("feesrequest12345", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETSTUDENTFEEINFOSTRUCTURE_METHODNAME, ServiceResource.FEES_URL1);
        //        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETSTUDENTFEEHISTORYINFO_METHODNAME, ServiceResource.FEES_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETSTUDENTFEEINFOSTRUCTURE_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.v(methodName, "" + result + " fees examresult");

            Log.d("GetExamFeeResult", result);

            try {

                TotalAmount = 0.0;
                FeesReceiptList.clear();
                JSONObject jobj = new JSONObject(result + "}");
                JSONArray mainJsonArray2 = jobj.getJSONArray(ServiceResource.TABLE);

                for (int i = 0; i < mainJsonArray2.length(); i++) {

                    JSONObject childobj = mainJsonArray2.getJSONObject(i);

                    feesReceiptModel = new FeesModel();
                    feesReceiptModel.setAmount(childobj.getString("Amount"));
                    feesReceiptModel.setUnpaid_amount(childobj.getDouble("UnpaidAmount"));
                    feesReceiptModel.setTotalPaidAmount(childobj.getDouble("TotalPaidAmount"));
                    feesReceiptModel.setFeesStructuteID(childobj.getString("FeesStructuteID"));
                    feesReceiptModel.setFeesStructureScheduleID(childobj.getString("FeesStructureScheduleID"));
                    feesReceiptModel.setStudentID(childobj.getString("StudentID"));
                    feesReceiptModel.setDisplayName(childobj.getString("DisplayName"));
                    feesReceiptModel.setBatchName(childobj.getString("BatchName"));
                    feesReceiptModel.setStudentFeesCollectionID(childobj.getString("StudentFeesCollectionID"));
                    feesReceiptModel.setBatchID(childobj.getString("BatchID"));
                    feesReceiptModel.setClientID(childobj.getString("ClientID"));
                    feesReceiptModel.setInstituteID(childobj.getString("InstituteID"));
                    feesReceiptModel.setPaymentgatewaymasterid(childobj.getString("PaymentGateWayMasterID"));
                    feesReceiptModel.setCounterID(childobj.getString("CounterID"));

                    Log.d("getPaymenrMasterID", String.valueOf(feesReceiptModel.getPaymentgatewaymasterid()));

                    if (feesReceiptModel.getUnpaid_amount().equals(0.0) || feesReceiptModel.getUnpaid_amount().equals(0.00) ||
                            feesReceiptModel.getUnpaid_amount().equals(0)) {
                    } else {
                        txt_total_amount.setVisibility(View.VISIBLE);
                        TotalAmount = TotalAmount + feesReceiptModel.getUnpaid_amount();
                        txt_total_amount.setText(String.valueOf(TotalAmount));
                        FeesReceiptList.add(feesReceiptModel);
                    }

                }

                FeesListAdapter listAdapter = new FeesListAdapter(mContext, FeesReceiptList, "FromFee");
                lvfees.setAdapter(listAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (ServiceResource.STUDENTFEESCOLLECTIONLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                JSONObject jobj = new JSONObject(result + "}");
                JSONArray mainJsonArray = jobj.getJSONArray(ServiceResource.TABLE);
                Type type = new TypeToken<ArrayList<FeesModel>>() {
                }.getType();
                ArrayList<FeesModel> listFees = new Gson().fromJson(mainJsonArray.toString(), type);
                FeesListAdapter listAdapter = new FeesListAdapter(mContext, listFees, "");
                lvfees.setAdapter(listAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

}
