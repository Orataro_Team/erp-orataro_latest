package com.edusunsoft.erp.orataro.model;

public class FriendSearchModel {

	private String MemberID;
	private String FullName;
	private String WallID;
	private String RoleName;
	private String ProfilePicture;
	private String IsFriend;
	private String IsRequested;
	private String DivisionName,GradeName,GradeID,DivisionID;

	public String getDivisionName() {
		return DivisionName;
	}

	public void setDivisionName(String divisionName) {
		DivisionName = divisionName;
	}

	public String getGradeName() {
		return GradeName;
	}

	public void setGradeName(String gradeName) {
		GradeName = gradeName;
	}

	public String getGradeID() {
		return GradeID;
	}

	public void setGradeID(String gradeID) {
		GradeID = gradeID;
	}

	public String getDivisionID() {
		return DivisionID;
	}

	public void setDivisionID(String divisionID) {
		DivisionID = divisionID;
	}

	public String getMemberID() {
		return MemberID;
	}

	public void setMemberID(String memberID) {
		MemberID = memberID;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getWallID() {
		return WallID;
	}

	public void setWallID(String wallID) {
		WallID = wallID;
	}

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public String getIsFriend() {
		return IsFriend;
	}

	public void setIsFriend(String isFriend) {
		IsFriend = isFriend;
	}

	public String getIsRequested() {
		return IsRequested;
	}

	public void setIsRequested(String isRequested) {
		IsRequested = isRequested;
	}

}
