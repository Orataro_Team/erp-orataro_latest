package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.edusunsoft.erp.orataro.R;

public class EmojiesAdapter  extends BaseAdapter {
	
	private Context mContext;
	private int[] emojies = {R.drawable.emotion_01, R.drawable.emotion_02, R.drawable.emotion_03
			, R.drawable.emotion_04, R.drawable.emotion_05, R.drawable.emotion_06
			, R.drawable.emotion_07, R.drawable.emotion_08, R.drawable.emotion_09
			, R.drawable.emotion_10, R.drawable.emotion_11, R.drawable.emotion_12
			, R.drawable.emotion_13, R.drawable.emotion_14, R.drawable.emotion_15
			, R.drawable.emotion_16, R.drawable.emotion_17, R.drawable.emotion_18
			, R.drawable.emotion_19, R.drawable.emotion_20, R.drawable.emotion_21
			, R.drawable.emotion_22, R.drawable.emotion_23, R.drawable.emotion_24
			, R.drawable.emotion_25
	};

	private LayoutInflater layoutInfalater;
	public EmojiesAdapter(Context mContext) {
     this.mContext = mContext;
	}

	@Override
	public int getCount() {
		return emojies.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.emojiesimage, parent, false);
		ImageView imgEmojies = (ImageView) convertView.findViewById(R.id.imgemojies);
		imgEmojies.setImageResource(emojies[position]);
		return convertView;
	}

}
