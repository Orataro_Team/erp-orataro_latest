package com.edusunsoft.erp.orataro.Interface;

public interface WallCustomeClick {
	
	public void clickOnPhoto();
	public void clickOnVideo();
	public void clickOnFriends();
	public void clickOnPost();
	public void onDeletePost();
	public void onEditPost();

}
