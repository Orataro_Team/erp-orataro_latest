package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.PtCOmmunicationActivity;
import com.edusunsoft.erp.orataro.database.PTCommMemberModel;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.List;

public class StudentTeacherAdapter extends BaseAdapter {

    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private Context mContext;
    private List<PTCommMemberModel> list;
    private LayoutInflater layoutInfalater;
    private StdDivSubModel model;

    public StudentTeacherAdapter(Context mContext, List<PTCommMemberModel> list, StdDivSubModel model) {
        this.mContext = mContext;
        this.list = list;
        this.model = model;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.student_list_row, parent, false);

        CheckBox chkdivision = (CheckBox) convertView.findViewById(R.id.chkdivision);
        TextView txtStandard = (TextView) convertView.findViewById(R.id.txtStanderd);
        TextView txtDivision = (TextView) convertView.findViewById(R.id.txtDivision);
        TextView txtSubject = (TextView) convertView.findViewById(R.id.txtSubject);
        TextView txtrollnumber = (TextView) convertView.findViewById(R.id.txtrollnumber);
        convertView.setBackgroundColor(colors[position % colors.length]);
        txtStandard.setText(list.get(position).getFullName());

        txtStandard.setGravity(Gravity.START);


        if (Utility.isNull(list.get(position).getRollNo())) {
            txtrollnumber.setText(list.get(position).getRollNo());
            txtrollnumber.setVisibility(View.VISIBLE);
        } else {
            txtrollnumber.setVisibility(View.INVISIBLE);
        }

        if (!Utility.isTeacher(mContext)) {
            txtSubject.setText(list.get(position).getSubjectName());
            txtrollnumber.setVisibility(View.GONE);
            txtDivision.setVisibility(View.GONE);
            txtSubject.setGravity(Gravity.START);
        } else {
            txtSubject.setVisibility(View.GONE);
        }

        txtDivision.setVisibility(View.GONE);
        chkdivision.setVisibility(View.GONE);

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, PtCOmmunicationActivity.class);
                i.putExtra("model", list.get(position));
                i.putExtra("modelStandard", model);
                mContext.startActivity(i);

            }

        });

        return convertView;

    }

}
