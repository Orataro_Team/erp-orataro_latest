package com.edusunsoft.erp.orataro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class RouteListResModel implements Serializable {

    @SerializedName("data")
    @Expose
    private ArrayList<Data> data = null;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable {
        @SerializedName("IsForPickup")
        @Expose
        private int isForPickup;
        @SerializedName("StudentID")
        @Expose
        private String studentID;
        @SerializedName("PointID")
        @Expose
        private String pointID;
        @SerializedName("RouteID")
        @Expose
        private String routeID;
        @SerializedName("RouteName")
        @Expose
        private String routeName;
        @SerializedName("PolyLine")
        @Expose
        private String polyLine;
        @SerializedName("TotalKM")
        @Expose
        private double totalKM;
        @SerializedName("TotalTimeToTravel")
        @Expose
        private int totalTimeToTravel;
        @SerializedName("PointName")
        @Expose
        private String pointName;
        @SerializedName("StartPoint")
        @Expose
        private String startPoint;
        @SerializedName("EndPoint")
        @Expose
        private String endPoint;
        @SerializedName("BusRouteID")
        @Expose
        private String busRouteID;
        @SerializedName("BusID")
        @Expose
        private String busID;
        @SerializedName("DriverID")
        @Expose
        private String driverID;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("EndTime")
        @Expose
        private String endTime;
        @SerializedName("DriverHelperID")
        @Expose
        private String driverHelperID;
        @SerializedName("BusRouteName")
        @Expose
        private String busRouteName;
        @SerializedName("Color")
        @Expose
        private String color;
        @SerializedName("VehicleRegNo")
        @Expose
        private String vehicleRegNo;
        @SerializedName("RTORegNo")
        @Expose
        private String rTORegNo;
        @SerializedName("BusName")
        @Expose
        private String busName;
        @SerializedName("BusCode")
        @Expose
        private String busCode;
        @SerializedName("DriverName")
        @Expose
        private String driverName;
        @SerializedName("DriverMobileNo")
        @Expose
        private String driverMobileNo;
        @SerializedName("DriverHelperName")
        @Expose
        private String driverHelperName;
        @SerializedName("DriverHelperMobileNo")
        @Expose
        private String driverHelperMobileNo;
        @SerializedName("BusTripLogID")
        @Expose
        private String busTripLogID;
        @SerializedName("AttendanceStatusTerm")
        @Expose
        private String attendanceStatusTerm;

        public int getIsForPickup() {
            return isForPickup;
        }

        public void setIsForPickup(int isForPickup) {
            this.isForPickup = isForPickup;
        }

        public String getStudentID() {
            return studentID;
        }

        public void setStudentID(String studentID) {
            this.studentID = studentID;
        }

        public String getPointID() {
            return pointID;
        }

        public void setPointID(String pointID) {
            this.pointID = pointID;
        }

        public String getRouteID() {
            return routeID;
        }

        public void setRouteID(String routeID) {
            this.routeID = routeID;
        }

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        public String getPolyLine() {
            return polyLine;
        }

        public void setPolyLine(String polyLine) {
            this.polyLine = polyLine;
        }

        public double getTotalKM() {
            return totalKM;
        }

        public void setTotalKM(double totalKM) {
            this.totalKM = totalKM;
        }

        public int getTotalTimeToTravel() {
            return totalTimeToTravel;
        }

        public void setTotalTimeToTravel(int totalTimeToTravel) {
            this.totalTimeToTravel = totalTimeToTravel;
        }

        public String getPointName() {
            return pointName;
        }

        public void setPointName(String pointName) {
            this.pointName = pointName;
        }

        public String getStartPoint() {
            return startPoint;
        }

        public void setStartPoint(String startPoint) {
            this.startPoint = startPoint;
        }

        public String getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getBusRouteID() {
            return busRouteID;
        }

        public void setBusRouteID(String busRouteID) {
            this.busRouteID = busRouteID;
        }

        public String getBusID() {
            return busID;
        }

        public void setBusID(String busID) {
            this.busID = busID;
        }

        public String getDriverID() {
            return driverID;
        }

        public void setDriverID(String driverID) {
            this.driverID = driverID;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getDriverHelperID() {
            return driverHelperID;
        }

        public void setDriverHelperID(String driverHelperID) {
            this.driverHelperID = driverHelperID;
        }

        public String getBusRouteName() {
            return busRouteName;
        }

        public void setBusRouteName(String busRouteName) {
            this.busRouteName = busRouteName;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getVehicleRegNo() {
            return vehicleRegNo;
        }

        public void setVehicleRegNo(String vehicleRegNo) {
            this.vehicleRegNo = vehicleRegNo;
        }

        public String getRTORegNo() {
            return rTORegNo;
        }

        public void setRTORegNo(String rTORegNo) {
            this.rTORegNo = rTORegNo;
        }

        public String getBusName() {
            return busName;
        }

        public void setBusName(String busName) {
            this.busName = busName;
        }

        public String getBusCode() {
            return busCode;
        }

        public void setBusCode(String busCode) {
            this.busCode = busCode;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverMobileNo() {
            return driverMobileNo;
        }

        public void setDriverMobileNo(String driverMobileNo) {
            this.driverMobileNo = driverMobileNo;
        }

        public String getDriverHelperName() {
            return driverHelperName;
        }

        public void setDriverHelperName(String driverHelperName) {
            this.driverHelperName = driverHelperName;
        }

        public String getDriverHelperMobileNo() {
            return driverHelperMobileNo;
        }

        public void setDriverHelperMobileNo(String driverHelperMobileNo) {
            this.driverHelperMobileNo = driverHelperMobileNo;
        }

        public String getBusTripLogID() {
            return busTripLogID;
        }

        public void setBusTripLogID(String busTripLogID) {
            this.busTripLogID = busTripLogID;
        }

        public String getAttendanceStatusTerm() {
            return attendanceStatusTerm;
        }

        public void setAttendanceStatusTerm(String attendanceStatusTerm) {
            this.attendanceStatusTerm = attendanceStatusTerm;
        }

    }

}
