package com.edusunsoft.erp.orataro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

import java.util.ArrayList;

public class BloodGroupListAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<String> bloodGrp;
	private String strbg;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	public BloodGroupListAdapter(Context context) {

	}

	public BloodGroupListAdapter(Context context, ArrayList<String> year) {
		this.mContext = context;
		this.bloodGrp = year;
	}

	@Override
	public int getCount() {
		return bloodGrp.size();
	}

	@Override
	public Object getItem(int position) {
		return 0;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.blood_grp_list_item, parent, false);

		final TextView countryName = (TextView) convertView.findViewById(R.id.txt_bg_item);
		countryName.setText(bloodGrp.get(position).toString());
		convertView.setBackgroundColor(colors[position % colors.length]);

		return convertView;
	}

}
