package com.edusunsoft.erp.orataro.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.OnUploadedHomeworkItemClickListener;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewUploadedHWImageActivityActivity;
import com.edusunsoft.erp.orataro.databinding.UploadedHwListRowForParentBinding;
import com.edusunsoft.erp.orataro.model.uploadHomeworkResModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UploadedHomeworkListAdapterForParent extends RecyclerView.Adapter<UploadedHomeworkListAdapterForParent.RouteListAdapterHolder> {
    private ArrayList<uploadHomeworkResModel.Data> dataArrayList;
    private OnUploadedHomeworkItemClickListener onRouteItemClickListener;
    Context context;
    File file;

    public UploadedHomeworkListAdapterForParent(Context mContext, ArrayList<uploadHomeworkResModel.Data> data, OnUploadedHomeworkItemClickListener onRouteItemClickListener) {
        this.context = mContext;
        this.dataArrayList = data;
        this.onRouteItemClickListener = onRouteItemClickListener;
    }


    @NonNull
    @Override
    public RouteListAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new RouteListAdapterHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.uploaded_hw_list_row_for_parent, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RouteListAdapterHolder routeListAdapterHolder, int position) {
        routeListAdapterHolder.bindView(dataArrayList.get(position));
        routeListAdapterHolder.uploadhomeworkListparentRowBinding.txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("getImageURlll", dataArrayList.get(position).getImageURL());
                Log.d("getFileType", dataArrayList.get(position).getFileType());
                List<String> items = Arrays.asList(dataArrayList.get(position).getImageURL().split("\\s*,\\s*"));
                ServiceResource.GetHwUploadedImageList = items;
                Log.d("getURLImage", items.toString());
                Intent studentlistintent = new Intent(context, ViewUploadedHWImageActivityActivity.class);
                studentlistintent.putExtra("StudentReplayID", dataArrayList.get(position).getStudentReplayID());
                studentlistintent.putExtra("FileType", dataArrayList.get(position).getFileType());
                if (dataArrayList.get(position).getFileType().equalsIgnoreCase("FILE")) {
                    studentlistintent.putExtra("FileURL", ServiceResource.BASE_IMG_URL + dataArrayList.get(position).getImageURL().replace("//DataFiles//", "/DataFiles/"));
                }
                studentlistintent.putExtra("Note", dataArrayList.get(position).getNote());
                context.startActivity(studentlistintent);

            }

        });

    }

    @Override
    public int getItemCount() {
        return dataArrayList != null ? dataArrayList.size() : 0;
    }

    class RouteListAdapterHolder extends RecyclerView.ViewHolder {
        UploadedHwListRowForParentBinding uploadhomeworkListparentRowBinding;

        RouteListAdapterHolder(@NonNull UploadedHwListRowForParentBinding uploadehwListRowBinding) {
            super(uploadehwListRowBinding.getRoot());
            this.uploadhomeworkListparentRowBinding = uploadehwListRowBinding;
        }

        void bindView(uploadHomeworkResModel.Data data) {

            uploadhomeworkListparentRowBinding.txtcreatedby.setText(data.getDisplayName());
            uploadhomeworkListparentRowBinding.txtnote.setText(data.getNote());
            uploadhomeworkListparentRowBinding.txtcreatedon.setText(data.getCreatedOn());
            if (data.getIsApprove() == false) {
                uploadhomeworkListparentRowBinding.txtapprove.setText("No");
            } else {
                uploadhomeworkListparentRowBinding.txtapprove.setText("Yes");
            }

        }

    }

}
