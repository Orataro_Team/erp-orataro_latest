package com.edusunsoft.erp.orataro.customeview;

/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;

import java.text.DecimalFormat;

/**
 * To be used with ViewPager to provide a tab indicator component which give
 * constant feedback as to the aboutorataro's scroll progress.
 * <p>
 * To use the component, simply add it to your view hierarchy. Then in your
 * {@link Activity} or {@link androidx.fragment.app.Fragment} call
 * {@link #setViewPager(androidx.viewpager.widget.ViewPager)} providing it the
 * ViewPager this layout is being used for.
 * <p>
 * The colors can be customized in two ways. The first and simplest is to
 * provide an array of colors via {@link #setSelectedIndicatorColors(int...)}.
 * The alternative is via the {@link //com.google.samples.apps.iosched.ui.widget.SlidingTabLayout.TabColorizer} interface which provides you complete control over
 which
 * color is used for any individual position.
 * <p>
 * <p>
 * 
 * The views used as tabs can be customized by calling
 * <p>
 * 
 * The views used as tabs can be customized by calling
 * <p>
 * 
 * The views used as tabs can be customized by calling<p>
 * 
 * The views used as tabs can be customized by calling
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * {@link #setCustomTabView(int, int, int, int)} (int, int)}, providing the layout ID of your custom
 * layout.
 */
public class SlidingTabLayout extends HorizontalScrollView {

	public int visibleNotificationCount;

	/**
	 * Allows complete control over the colors drawn in the tab layout. Set with
	 * {@link #//setCustomTabColorizer(com.google.samples.apps.iosched.ui.widget.
 SlidingTabLayout
	 * )}.
	 */
	public interface TabColorizer {

		/**
		 * @return return the color of the indicator used when {@code position}
		 *         is selected.
		 */
		int getIndicatorColor(int position);

	}

	int selecteddrawable[] = {R.drawable.post_show, R.drawable.students_grp_show,
			R.drawable.teachers_grp_show,
			R.drawable.share_show, R.drawable.views_show };
	int unSelecteddrawable[] = { R.drawable.post, R.drawable.students_grp,
			R.drawable.teachers_grp, R.drawable.share,
			R.drawable.views };

	int selecteddrawable_profile[] = { R.drawable.father_show,
			R.drawable.mother_show, R.drawable.gaurdian_show };
	int unselecteddrawable_profile[] = { R.drawable.father, R.drawable.mother,
			R.drawable.gaurdian };

	Context mContext;

	
	int iconSelectedTintcolor;
	int iconDefaultTintcolor;
	
	public interface TabIconProvider {
		int getPageIconResId(int position);
		
	}

	public interface TabCountProvider {
		int getPageCount(int position);
	}

	private static final int TITLE_OFFSET_DIPS = 24;
	private static final int TAB_VIEW_PADDING_DIPS = 16;
	private static final int TAB_VIEW_TEXT_SIZE_SP = 12;

	private int mTitleOffset;

	private int mTabViewLayoutId;
	private int mTabViewTextViewId;
	private int mTabViewCountId;
	private boolean mDistributeEvenly;

	private ViewPager mViewPager;
	private SparseArray<String> mContentDescriptions = new SparseArray<String>();
	private ViewPager.OnPageChangeListener mViewPagerPageChangeListener;

	private final SlidingTabStrip mTabStrip;

	public SlidingTabLayout(Context context) {
		this(context, null);
		mContext = context;
	}

	
	private int[] mIcon ;
	int[] mSelectedIcon;
	
	public SlidingTabLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		mContext = context;
	}

	public SlidingTabLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		// Disable the Scroll Bar
		setHorizontalScrollBarEnabled(false);
		// Make sure that the Tab Strips fills this View
		setFillViewport(true);

		mTitleOffset = (int) (TITLE_OFFSET_DIPS * getResources()
				.getDisplayMetrics().density);

		mTabStrip = new SlidingTabStrip(context);
		addView(mTabStrip, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

/**
	 * Set the custom {@link //com.google.samples.apps.iosched.ui.widget.SlidingTabLayout.TabColorizer} to be used.
 If
	 * you only require simple custmisation then you can use
	 * 
	 * @link #setSelectedIndicatorColors(int...)} to achieve similar effects.
	 */
	public void setCustomTabColorizer(TabColorizer tabColorizer) {
		mTabStrip.setCustomTabColorizer(tabColorizer);
	}

	
	public void setIconResourse(int[] icons)
	{
		mIcon=icons;
		
	}
	public void setSelectedIconResourse(int[] icons)
	{
		mSelectedIcon=icons;
		
	}
	
	public void setDistributeEvenly(boolean distributeEvenly) {
		mDistributeEvenly = distributeEvenly;
	}

	/**
	 * Sets the colors to be used for indicating the selected tab. These colors
	 * are treated as a circular array. Providing one color will mean that all
	 * tabs are indicated with the same color.
	 */
	public void setSelectedIndicatorColors(int... colors) {
		mTabStrip.setSelectedIndicatorColors(colors);
	}

	public void setTitleTextColor(int color) {
		for (int i = 0; i < mTabStrip.getChildCount(); i++) {
			if (mTabStrip.getChildAt(i) != null
					&& TextView.class.isInstance(mTabStrip.getChildAt(i))) {
				((TextView) mTabStrip.getChildAt(i)).setTextColor(color);
			}
		}
	}

	/**
	 * Set the {@link ViewPager.OnPageChangeListener}.
	 * When using {@link //com.google.samples.apps.iosched.ui.widget.SlidingTabLayout} you are
 required
	 * to set any {@link ViewPager.OnPageChangeListener}
	 * through this method. This is so that the layout can update it's scroll
	 * position correctly.
	 * 
	 * @see ViewPager#setOnPageChangeListener(ViewPager.OnPageChangeListener)
	 */
	public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
		mViewPagerPageChangeListener = listener;
	}

	/**
	 * Set the custom layout to be inflated for the tab views.
	 * 
	 * @param layoutResId
	 *            Layout id to be inflated
	 * @param textViewId
	 *            id of the {@link TextView} in the inflated view
	 */
	public void setCustomTabView(int layoutResId, int textViewId, int countId,
			int visibleCount) {
		mTabViewLayoutId = layoutResId;
		mTabViewTextViewId = textViewId;
		mTabViewCountId = countId;
		visibleNotificationCount = visibleCount;
		iconSelectedTintcolor = Color.parseColor("#27305b");
		iconDefaultTintcolor = Color.parseColor("#9e9e9e") ;
	}

	/**
	 * Sets the associated view pager. Note that the assumption here is that the
	 * pager content (number of tabs and tab titles) does not change after this
	 * call has been made.
	 */
	public void setViewPager(ViewPager viewPager) {
		mTabStrip.removeAllViews();

		mViewPager = viewPager;
		if (viewPager != null) {
			viewPager.setOnPageChangeListener(new InternalViewPagerListener());
			populateTabStrip();
		}
	}

	/**
	 * Create a default view to be used for tabs. This is called if a custom tab
	 * view is not set via {@link #setCustomTabView(int, int, int, int)} (int, int)}.
	 */
	@SuppressLint("NewApi") protected TextView createDefaultTabView(Context context) {
		TextView textView = new TextView(context);
		textView.setGravity(Gravity.CENTER);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, TAB_VIEW_TEXT_SIZE_SP);
		textView.setTypeface(Typeface.DEFAULT_BOLD);
		textView.setLayoutParams(new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT));

		TypedValue outValue = new TypedValue();
		getContext().getTheme().resolveAttribute(
				android.R.attr.selectableItemBackground, outValue, true);
		textView.setBackgroundResource(outValue.resourceId);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			textView.setAllCaps(true);
		}

		int padding = (int) (TAB_VIEW_PADDING_DIPS * getResources()
				.getDisplayMetrics().density);
		textView.setPadding(padding, padding, padding, padding);

		return textView;
	}

	private void populateTabStrip() {
		final PagerAdapter adapter = mViewPager.getAdapter();
		final OnClickListener tabClickListener = new TabClickListener();

		for (int i = 0; i < adapter.getCount(); i++) {
			View tabView = null;
			View tabTitleView = null;

			View tabCount = null;

			if (mTabViewLayoutId != 0) {
				// If there is a custom tab view layout id set, try and inflate
				// it
				tabView = LayoutInflater.from(getContext()).inflate(
						mTabViewLayoutId, mTabStrip, false);
				tabTitleView = tabView.findViewById(mTabViewTextViewId);
				tabCount = tabView.findViewById(mTabViewCountId);
			}

			if (tabView == null) {
				tabView = createDefaultTabView(getContext());
			}

			if (tabTitleView == null && TextView.class.isInstance(tabView)) {
				tabTitleView = tabView;
			}
			if (tabCount == null && TextView.class.isInstance(tabView)) {
				tabCount = tabView;
			}

			if (mDistributeEvenly) {
				LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tabView
						.getLayoutParams();
				lp.width = 0;
				lp.weight = 1;
			}

			if (TextView.class.isInstance(tabTitleView)) {
				((TextView) tabTitleView).setText(adapter.getPageTitle(i));
			} else if (ImageView.class.isInstance(tabTitleView)
					&& adapter instanceof TabIconProvider) {
				TabIconProvider mTabIconProvider = (TabIconProvider) adapter;
				TabCountProvider mTabCountProvider = (TabCountProvider) adapter;
				DecimalFormat twodigits = new DecimalFormat("00");
				((TextView) tabCount).setText(twodigits
						.format(mTabCountProvider.getPageCount(i)));

				if (visibleNotificationCount == 1) {
					tabCount.setVisibility(View.VISIBLE);
				} else {
					tabCount.setVisibility(View.INVISIBLE);
				}

				tabTitleView.setBackgroundResource(mTabIconProvider
						.getPageIconResId(i));
			}

			tabView.setOnClickListener(tabClickListener);
			String desc = mContentDescriptions.get(i, null);
			if (desc != null) {
				tabView.setContentDescription(desc);
			}

			mTabStrip.addView(tabView);
			if (i == mViewPager.getCurrentItem()) {
				tabView.setSelected(true);
			}
		}
	}

	public void setContentDescription(int i, String desc) {
		mContentDescriptions.put(i, desc);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (mViewPager != null) {
			scrollToTab(mViewPager.getCurrentItem(), 0);
		}
	}

	private void scrollToTab(int tabIndex, int positionOffset) {
		final int tabStripChildCount = mTabStrip.getChildCount();
		if (tabStripChildCount == 0 || tabIndex < 0
				|| tabIndex >= tabStripChildCount) {
			return;
		}

		View selectedChild = mTabStrip.getChildAt(tabIndex);
		if (selectedChild != null) {
			int targetScrollX = selectedChild.getLeft() + positionOffset;

			if (tabIndex > 0 || positionOffset > 0) {
				// If we're not at the first child and are mid-scroll, make sure
				// we obey the offset
				targetScrollX -= mTitleOffset;
			}

			scrollTo(targetScrollX, 0);
		}
	}

	private class InternalViewPagerListener implements
			ViewPager.OnPageChangeListener {
		private int mScrollState;

		@Override
		public void onPageScrolled(int position, float positionOffset,
								   int positionOffsetPixels) {
			int tabStripChildCount = mTabStrip.getChildCount();
			if ((tabStripChildCount == 0) || (position < 0)
					|| (position >= tabStripChildCount)) {
				return;
			}

			mTabStrip.onViewPagerPageChanged(position, positionOffset);

			View selectedTitle = mTabStrip.getChildAt(position);
			int extraOffset = (selectedTitle != null) ? (int) (positionOffset * selectedTitle
					.getWidth()) : 0;
			scrollToTab(position, extraOffset);

			if (mViewPagerPageChangeListener != null) {
				mViewPagerPageChangeListener.onPageScrolled(position,
						positionOffset, positionOffsetPixels);
			}
		}

		@Override
		public void onPageScrollStateChanged(int state) {
			mScrollState = state;

			if (mViewPagerPageChangeListener != null) {
				mViewPagerPageChangeListener.onPageScrollStateChanged(state);
			}
		}

		@Override
		public void onPageSelected(int position) {
			if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
				mTabStrip.onViewPagerPageChanged(position, 0f);
				scrollToTab(position, 0);
			}

			for (int i = 0; i < mTabStrip.getChildCount(); i++) {
				mTabStrip.getChildAt(i).setSelected(position == i);
			}

			if (mViewPagerPageChangeListener != null) {
				mViewPagerPageChangeListener.onPageSelected(position);
			}

			tabSelector(mTabStrip, position, mTabStrip.getChildCount());
		}

	}

	public void setSelectedTab(int position) {
		tabSelector(mTabStrip, position, mTabStrip.getChildCount());
	}

	private class TabClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			for (int i = 0; i < mTabStrip.getChildCount(); i++) {
				if (v == mTabStrip.getChildAt(i)) {
					mViewPager.setCurrentItem(i);
					return;
				}
			}
		}
	}

	public void tabSelector(final SlidingTabStrip slidingTabStrip,
			final int position, final int count) {

		((Activity) mContext).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < count; i++) {
					if (visibleNotificationCount == 1) {
						if (i == position) {
							ImageView iv=((ImageView) slidingTabStrip.getChildAt(i).findViewById(R.id.tab_name_img));
							Utility.setImageViewResource(iv, mSelectedIcon[i],iconSelectedTintcolor, mContext);
						} else {
							ImageView iv=((ImageView) slidingTabStrip.getChildAt(i).findViewById(R.id.tab_name_img));
							Utility.setImageViewResource(iv, mIcon[i],iconDefaultTintcolor, mContext);
						}
					}
					
					else  {
						if (i == position) {
							ImageView iv=((ImageView) slidingTabStrip.getChildAt(i).findViewById(R.id.tab_name_img));
							Utility.setImageViewResource(iv, mSelectedIcon[i],iconSelectedTintcolor, mContext);
						} else {
							ImageView iv=((ImageView) slidingTabStrip.getChildAt(i).findViewById(R.id.tab_name_img));
							Utility.setImageViewResource(iv, mIcon[i],iconDefaultTintcolor, mContext);
						}
					}

				}

			}
		});

	}

}
