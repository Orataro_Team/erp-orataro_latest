package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.OnUploadedHomeworkItemClickListener;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.UploadedHomeworkListAdapter;
import com.edusunsoft.erp.orataro.adapter.UploadedHomeworkListAdapterForParent;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.uploadHomeworkResModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ShowUploadedHomeworkActivity extends AppCompatActivity implements ResponseWebServices, View.OnClickListener {

    LinearLayout lyl_show_uploaded_homework, lyl_show_uploaded_homework_parent;

    RecyclerView uploade_homework_list;
    LinearLayoutManager linearLayoutManager;

    private ImageView imgLeftheader;
    public TextView header_text, txt_no_data_found;

    Context mContext;

    String ASSOCIATIONID = "";
    Toolbar toolbar;
    uploadHomeworkResModel uploadhwListResModel;
    ArrayList<uploadHomeworkResModel.Data> StudentList = new ArrayList<>();
    ArrayList<uploadHomeworkResModel.Data> FilterStudentList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_list_activity);

        Initialization();

    }

    private void Initialization() {

        mContext = ShowUploadedHomeworkActivity.this;

        ASSOCIATIONID = getIntent().getStringExtra("AssociationID");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Show Uploaded Homework");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

        txt_no_data_found = (TextView) findViewById(R.id.txt_no_data_found);

        uploade_homework_list = (RecyclerView) findViewById(R.id.student_list);
        linearLayoutManager = new LinearLayoutManager(this);
        // use a linear layout manager
        uploade_homework_list.setLayoutManager(linearLayoutManager);

        lyl_show_uploaded_homework = (LinearLayout) findViewById(R.id.lyl_show_uploaded_homework);
        lyl_show_uploaded_homework_parent = (LinearLayout) findViewById(R.id.lyl_show_uploaded_homework_parent);

        if (Utility.isTeacher(mContext)) {
            lyl_show_uploaded_homework.setVisibility(View.VISIBLE);
        } else {
            lyl_show_uploaded_homework_parent.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!Utility.isTeacher(mContext)) {
            return false;
        }
        getMenuInflater().inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.approved) {
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            FilterStudentList.clear();
            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
                for (int i = 0; i < StudentList.size(); i++) {
                    if (StudentList.get(i).getIsApprove() == true) {
                        FilterStudentList.add(StudentList.get(i));
                    }
                }
                if (FilterStudentList.size() > 0) {
                    UploadedHomeworkListAdapter hwListAdapter = new UploadedHomeworkListAdapter(mContext, FilterStudentList, onUploadedHomeworkItemClickListener);
                    uploade_homework_list.setAdapter(hwListAdapter);
                    uploade_homework_list.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);
                } else {
                    uploade_homework_list.setAdapter(null);
                    uploade_homework_list.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);
                }

            }

        } else if (item.getItemId() == R.id.unapproved) {
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            FilterStudentList.clear();
            if (StudentList.size() > 0 || !StudentList.isEmpty()) {

                for (int i = 0; i < StudentList.size(); i++) {
                    if (StudentList.get(i).getIsApprove() == false) {
                        FilterStudentList.add(StudentList.get(i));
                    }
                }
                if (FilterStudentList.size() > 0) {
                    UploadedHomeworkListAdapter hwListAdapter = new UploadedHomeworkListAdapter(mContext, FilterStudentList, onUploadedHomeworkItemClickListener);
                    uploade_homework_list.setAdapter(hwListAdapter);
                    uploade_homework_list.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);
                } else {
                    uploade_homework_list.setAdapter(null);
                    uploade_homework_list.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);
                }

            }
        } else if (item.getItemId() == R.id.nonsubmitted) {
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            FilterStudentList.clear();
            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
                for (int i = 0; i < StudentList.size(); i++) {
                    if (StudentList.get(i).getStudentReplayID().equalsIgnoreCase("00000000-0000-0000-0000-000000000000")) {
                        FilterStudentList.add(StudentList.get(i));
                        Log.d("filterstudent123", FilterStudentList.toString());
                    }
                }
                if (FilterStudentList.size() > 0) {
                    UploadedHomeworkListAdapter hwListAdapter = new UploadedHomeworkListAdapter(mContext, FilterStudentList, onUploadedHomeworkItemClickListener);
                    uploade_homework_list.setAdapter(hwListAdapter);
                    uploade_homework_list.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);
                } else {
                    uploade_homework_list.setAdapter(null);
                    uploade_homework_list.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);
                }

            }
        } else if (item.getItemId() == R.id.all) {
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
                UploadedHomeworkListAdapter hwListAdapter = new UploadedHomeworkListAdapter(mContext, StudentList, onUploadedHomeworkItemClickListener);
                uploade_homework_list.setAdapter(hwListAdapter);
                uploade_homework_list.setVisibility(View.VISIBLE);
                txt_no_data_found.setVisibility(View.GONE);
            } else {
                uploade_homework_list.setAdapter(null);
                uploade_homework_list.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.VISIBLE);

            }

        }

        return true;

    }


    @Override
    protected void onResume() {
        super.onResume();
        //get uploaded homework
        if (Utility.isTeacher(mContext)) {
            try {
                getUploadedHomework();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                getUploadedHomeworkForParent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getUploadedHomeworkForParent() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (!Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        }
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID,
                ASSOCIATIONID));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE,
                "Homework"));
        Log.d("getPrenbtRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GET_UPLOAD_HOMEWORK_FOR_PARENT_METHODNAME,
                ServiceResource.UPLOADHOMEWORK_URL);

    }

    private void getUploadedHomework() {

        Date starttime = Calendar.getInstance().getTime();
        Log.d("starttime", starttime.toString());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID,
                ASSOCIATIONID));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE,
                "Homework"));
        Log.d("getPrenbtRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GET_UPLOAD_HOMEWORK_FOR_TEACHER_METHODNAME,
                ServiceResource.UPLOADHOMEWORK_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GET_UPLOAD_HOMEWORK_FOR_TEACHER_METHODNAME.equals(methodName)) {

            Log.d("getuploadedresult", result);

            if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
                uploade_homework_list.setAdapter(null);
                uploade_homework_list.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.VISIBLE);
            } else if (result.contains("[{\"message\":\"Server is not responding, Please Try after some time.\",\"success\":0}]")) {
                uploade_homework_list.setAdapter(null);
                uploade_homework_list.setVisibility(View.GONE);
                Utility.showToast("Server is not responding, Please Try after some time", mContext);
            } else {

                JSONObject resultJson = new JSONObject();

                try {

                    Date starttime = Calendar.getInstance().getTime();
                    Log.d("end", starttime.toString());

                    resultJson.put("data", new JSONArray(result));
                    uploadhwListResModel = new Gson().fromJson(resultJson.toString(), uploadHomeworkResModel.class);
                    StudentList = uploadhwListResModel.getData();
                    UploadedHomeworkListAdapter hwListAdapter = new UploadedHomeworkListAdapter(mContext, uploadhwListResModel.getData(), onUploadedHomeworkItemClickListener);
                    uploade_homework_list.setAdapter(hwListAdapter);
                    uploade_homework_list.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);

                } catch (Exception e) {

                    e.printStackTrace();
                    uploade_homework_list.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);

                }

            }

        } else if (ServiceResource.GET_UPLOAD_HOMEWORK_FOR_PARENT_METHODNAME.equals(methodName)) {
            Log.d("uploadedHomework", result);
            if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
                uploade_homework_list.setAdapter(null);
                uploade_homework_list.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.VISIBLE);
            } else if (result.contains("[{\"message\":\"Server is not responding, Please Try after some time.\",\"success\":0}]")) {
                uploade_homework_list.setAdapter(null);
                uploade_homework_list.setVisibility(View.GONE);
                Utility.showToast("Server is not responding, Please Try after some time", mContext);
            } else {
                JSONObject resultJson = new JSONObject();
                try {
                    resultJson.put("data", new JSONArray(result));
                    uploadHomeworkResModel uploadhwListResModel = new Gson().fromJson(resultJson.toString(), uploadHomeworkResModel.class);
                    Log.d("getResultJson", uploadhwListResModel.getData().toString());
                    UploadedHomeworkListAdapterForParent hwlistforparentListAdapter = new UploadedHomeworkListAdapterForParent(mContext, uploadhwListResModel.getData(), onUploadedHomeworkItemClickListener);
                    uploade_homework_list.setAdapter(hwlistforparentListAdapter);
                    uploade_homework_list.setVisibility(View.VISIBLE);
                    txt_no_data_found.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                    uploade_homework_list.setVisibility(View.GONE);
                    txt_no_data_found.setVisibility(View.VISIBLE);
                }

            }

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                this.finish();
                break;
        }
    }

    private OnUploadedHomeworkItemClickListener onUploadedHomeworkItemClickListener = data -> {
//        Log.d(TAG, "onItemClick: " + data.getRouteName());
//        startActivity(RouteMapActivity.getInstance(this, data));
    };
}
