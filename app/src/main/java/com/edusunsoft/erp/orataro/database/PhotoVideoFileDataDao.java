package com.edusunsoft.erp.orataro.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PhotoVideoFileDataDao {

    @Insert
    void insertPhotoVideoFile(PhotoVideoFileModel photoVideoFileModel);

    @Delete
    void deletePhotoVideoFile(PhotoVideoFileModel photoVideoFileModel);

    @Query("SELECT * from PhotoVideoFileModel WHERE FileType = :Filetype")
    List<PhotoVideoFileModel> getPhotoVideoFileData(String Filetype);

    @Query("DELETE  FROM PhotoVideoFileModel where FileType =:Filetype")
    int deletePhotoVideoFileData(String Filetype);

    @Query("delete from PhotoVideoFileModel where AlbumPhotoID=:albmuphotoid")
    void deletePhotoVideoFileByID(String albmuphotoid);


}
