package com.edusunsoft.erp.orataro.util;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.ListSelectionActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.ListLeaveActivity;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;
import com.edusunsoft.erp.orataro.activities.SinglePostActivity;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

public class MultipleNotificationUtil {

    String Title = "", message = "";

    private static String TAG = MultipleNotificationUtil.class.getSimpleName();

    private Context mContext;
    PendingIntent pendingIntent;
    String ASSOCIATIONTYPE = "";

    public MultipleNotificationUtil(Context mContext) {
        this.mContext = mContext;
    }


    public static final String FCM_PARAM = "associationtype";
    private static final String CHANNEL_NAME = "ERP";
    private static final String CHANNEL_DESC = "ERP ORATARO";
    private int numMessages = 0;


    public void FireNotification(RemoteMessage remoteMessage, int notifyID) {

//        notifyID++;
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
        Log.d(":getData", data.toString());
        ASSOCIATIONTYPE = data.get(FCM_PARAM);

        if (ASSOCIATIONTYPE.equalsIgnoreCase("Post")) {

            Intent intent = new Intent(mContext, SinglePostActivity.class);
            intent.putExtra("notification", remoteMessage.getData().toString());
            intent.putExtra("isFrom", true);
            intent.putExtra("aId", data.get("associationid"));
            intent.putExtra("aType", data.get("associationtype"));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        } else if (ASSOCIATIONTYPE.equalsIgnoreCase("LeaveApplication")) {

            if (Utility.isTeacher(mContext)) {

                Intent intent = new Intent(mContext, ListSelectionActivity.class);
                intent.putExtra("isFrom", ServiceResource.LEAVE_FLAG);
                intent.putExtra("iswithoutsubject", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);


            } else {

                Intent intent = new Intent(mContext, ListLeaveActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            }

        } else if (ASSOCIATIONTYPE.equalsIgnoreCase("Circular") ||
                ASSOCIATIONTYPE.equalsIgnoreCase("Attencence") ||
                ASSOCIATIONTYPE.equalsIgnoreCase("HomeWork") ||
                ASSOCIATIONTYPE.equalsIgnoreCase("ClassWork")) {

            if (PreferenceData.getIsLogin() == true) {
                Intent intentHome = new Intent(mContext, HomeWorkFragmentActivity.class);
                if (PreferenceData.getIsSwitched()) {
                    intentHome.putExtra("position", mContext.getResources().getString(R.string.Wall));
                } else {
                    intentHome.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
                }
                intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(mContext, 0, intentHome, PendingIntent.FLAG_ONE_SHOT);
            } else {
                Intent intentRegister = new Intent(mContext, RegisterActivity.class);
                intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pendingIntent = PendingIntent.getActivity(mContext, 0, intentRegister, PendingIntent.FLAG_ONE_SHOT);
            }

        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, mContext.getString(R.string.default_notification_channel_id))
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setColor(mContext.getResources().getColor(R.color.blue))
                .setLights(Color.RED, 1000, 300)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.notification_icon);

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(
                    mContext.getString(R.string.default_notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );

            AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();
            channel.setDescription(CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), attributes);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);

        }

        assert notificationManager != null;
        notificationManager.notify(notifyID, notificationBuilder.build());

//        Uri urisound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/" + R.raw.notification_mp3);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, mContext.getString(R.string.default_notification_channel_id))
//                .setContentTitle(notification.getTitle())
//                .setContentText(notification.getBody())
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent)
//                .setColor(mContext.getResources().getColor(R.color.blue))
//                .setLights(Color.RED, 1000, 300)
//                .setNumber(++numMessages)
//                .setSound(urisound)
//                .setSmallIcon(R.drawable.notification_icon);
//
//        android.app.NotificationUtil notificationUtil = (android.app.NotificationUtil) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//            NotificationChannel channel = new NotificationChannel(
//                    mContext.getString(R.string.default_notification_channel_id), CHANNEL_NAME, android.app.NotificationUtil.IMPORTANCE_HIGH
//            );
//
//            AudioAttributes attributes = new AudioAttributes.Builder()
//                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
//                    .build();
//
//            channel.setDescription(CHANNEL_DESC);
//            channel.setShowBadge(true);
//            channel.canShowBadge();
//            channel.enableLights(true);
//            channel.enableVibration(true);
//            channel.setLightColor(Color.RED);
//            channel.setSound(urisound, attributes); // This is IMPORTANT
//
//            assert notificationUtil != null;
//
//            if (notificationUtil != null) {
//
//                List<NotificationChannel> channelList = notificationUtil.getNotificationChannels();
//
//                for (int i = 0; channelList != null && i < channelList.size(); i++) {
//                    notificationUtil.deleteNotificationChannel(channelList.get(i).getId());
//                }
//
//            }
//
//            notificationUtil.createNotificationChannel(channel);
//
//        }
//
//        assert notificationUtil != null;
//        notificationUtil.notify(notifyID, notificationBuilder.build());

    }


    /* commented By Krishna : 07-02-2019 Method to Check Application is in Foreground or background*/

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;

    }

    /*END*/


}
