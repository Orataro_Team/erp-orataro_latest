package com.edusunsoft.erp.orataro.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.VideoViewActivity;
import com.edusunsoft.erp.orataro.activities.ZoomImageAcitivity;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.PhotoVideoFileDataDao;
import com.edusunsoft.erp.orataro.database.PhotoVideoFileModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SaprateWallType extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private List<PhotoVideoFileModel> wallList;
    private LayoutInflater layoutInfalater;
    private String isDownload = "";
    private RefreshListner listner;
    public String DeleteDialogTitle = "";

    public Uri FileUri = null;
    public int posRemove;

    public String DELETE_ID = "";
    PhotoVideoFileDataDao photoVideoFileDataDao;
    public File file;

    public SaprateWallType(Context mContext, List<PhotoVideoFileModel> wallList, RefreshListner listner) {
        this.mContext = mContext;
        this.wallList = wallList;
        this.listner = listner;
    }

    @Override
    public int getCount() {
        return wallList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.sapratewall, parent, false);
        ImageView iv_fb = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        LinearLayout albumDetaillayout = (LinearLayout) convertView.findViewById(R.id.albumDetaillayout);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView txt_post_name = (TextView) convertView.findViewById(R.id.txt_post_name);
        date.setText(wallList.get(position).getDateOfPost());
        txt_post_name.setText(Html.fromHtml(wallList.get(position).getPostName()));

        ImageView btn_delete = (ImageView) convertView.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wallList.get(position).getPostName().equalsIgnoreCase("") || wallList.get(position).getPostName().equalsIgnoreCase("null")
                        || wallList.get(position).getPostName().equalsIgnoreCase(null)) {
                    DeleteDialogTitle = "";
                } else {
                    DeleteDialogTitle = wallList.get(position).getPostName();
                }

                Utility.deleteDialog(mContext, mContext.getResources().getString(R.string.photo), DeleteDialogTitle, new Popup() {

                    @Override
                    public void deleteYes() {

                        posRemove = position;
                        Log.d("getposition", String.valueOf(posRemove));
                        Log.d("getpositionFiletype", wallList.get(position).getFileType());
                        DELETE_ID = wallList.get(position).getAlbumPhotoID();
                        deletePhoto(wallList.get(position).getAlbumPhotoID(), position, wallList.get(position).getFileType());

                    }

                    @Override
                    public void deleteNo() {
                    }

                });

            }
        });


        if (wallList.get(position).getFileType().equalsIgnoreCase("FILE")) {

            Log.d("getwallposttype", wallList.get(position).getFileMimeType());

            iv_fb.setVisibility(View.VISIBLE);
            isDownload = "file";
            LayoutParams params = new LayoutParams(200, 200);
            iv_fb.setLayoutParams(params);
            iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            if (wallList.get(position).getFileMimeType().equalsIgnoreCase("text/plain")) {
                iv_fb.setImageResource(R.drawable.txt);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("PDF")) {
                iv_fb.setImageResource(R.drawable.pdf);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("TEXT")) {
                iv_fb.setImageResource(R.drawable.txt);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("application/pdf")) {
                iv_fb.setImageResource(R.drawable.pdf);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("DOCUMENT")) {
                iv_fb.setImageResource(R.drawable.doc);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("WORD")) {
                iv_fb.setImageResource(R.drawable.doc);
            } else if (wallList.get(position).getFileMimeType().equalsIgnoreCase("CSV")) {
                iv_fb.setImageResource(R.drawable.doc);
            } else {
                iv_fb.setImageResource(R.drawable.file);
            }
        } else if (wallList.get(position).getFileType().equalsIgnoreCase("Video")) {
            iv_fb.setVisibility(View.VISIBLE);
            LayoutParams params = new LayoutParams(200, 200);
            iv_fb.setLayoutParams(params);
            iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            if (wallList.get(position).getPhoto().contains(".mp3") ||
                    wallList.get(position).getPhoto().contains(".MP3") ||
                    wallList.get(position).getPhoto().contains(".wav") ||
                    wallList.get(position).getPhoto().contains(".WAV")
            ) {
                iv_fb.setImageResource(R.drawable.mpicon);
                isDownload = "mp3";
            } else {
                isDownload = "video";
                iv_fb.setImageResource(R.drawable.video);
            }

        } else if (wallList.get(position).getFileType().equalsIgnoreCase("IMAGE")) {

            try {
                if (wallList.get(position).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                    try {
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(wallList.get(position).getPhoto().replace("//DataFiles//", "/DataFiles/")
                                        .replace("//DataFiles/", "/DataFiles/"))
                                .apply(options)
                                .into(iv_fb);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(ServiceResource.BASE_IMG_URL
                                        + "/DataFiles/" + wallList.get(position).getPhoto().replace("//DataFiles//", "/DataFiles/")
                                        .replace("//DataFiles/", "/DataFiles/"))
                                .apply(options)
                                .into(iv_fb);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        albumDetaillayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickLayot(position, 1);
            }
        });

        iv_fb.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (wallList.get(position).getFileType().equalsIgnoreCase("Video")) {
                    onClickLayot(position, 2);
                } else if (wallList.get(position).getFileType().equalsIgnoreCase("IMAGE")) {
                    onClickLayot(position, 3);
                } else {
                    onClickLayot(position, 1);
                }
            }
        });

        return convertView;

    }

    private void deletePhoto(String photoalbumID, int position, String FileType) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ASSTYPE, FileType));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PhotoDelete_ID, photoalbumID));

        Log.d("getRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.DELETEPHOTOVIDEO_METHODNAME, ServiceResource.POST_URL);

    }

    public void onClickLayot(int pos, int type) {

        if (type == 1) {

            Log.d("GetVideo", wallList.get(pos).getPhoto());
            Log.d("GetFiletype", isDownload);

            if (isDownload.equalsIgnoreCase("file")) {

                if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                    saveDownload(wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                } else {
                    saveDownload(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                }

            } else if (isDownload.equalsIgnoreCase("mp3")) {

                if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                    saveDownload(wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                } else {

                    saveDownload(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));

                }

            } else if (isDownload.equalsIgnoreCase("mp4")) {

                if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                    saveDownload(wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));

                } else {

                    saveDownload(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                }

            } else if (isDownload.equalsIgnoreCase("video")) {

                if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                    saveDownload(wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                } else {

                    saveDownload(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                }

            } else {

                if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                    saveDownload(wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                } else {

                    saveDownload(ServiceResource.BASE_IMG_URL
                            + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                }

            }

        } else if (type == 3) {
            Intent in = new Intent(mContext,
                    ZoomImageAcitivity.class);
            if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                in.putExtra("img",
                        wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                                .replace("//DataFiles/", "/DataFiles/"));
            } else {
                in.putExtra("img", ServiceResource.BASE_IMG_URL
                        + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                        .replace("//DataFiles/", "/DataFiles/"));
            }
            in.putExtra("name", "Image");

            mContext.startActivity(in);

        } else {

            Log.d("videourl123", wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                    .replace("//DataFiles/", "/DataFiles/"));
            Intent i = new Intent(mContext, VideoViewActivity.class);
            i.putExtra("name", "Video");
            i.putExtra("FromStr", "");
            if (wallList.get(pos).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                i.putExtra("img",
                        wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                                .replace("//DataFiles/", "/DataFiles/"));
            } else {
                i.putExtra("img", ServiceResource.BASE_IMG_URL
                        + "/DataFiles/" + wallList.get(pos).getPhoto().replace("//DataFiles//", "/DataFiles/")
                        .replace("//DataFiles/", "/DataFiles/"));
            }

            mContext.startActivity(i);
            
        }
    }

    private void saveDownload(String url) {

        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");

        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }
//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//        Log.d("adzdfjnxv", destination.toString());
//        DownloadManager.Request request = new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//        long id = mgr.enqueue(request);
//        Utility.toast(mContext, mContext.getResources().getString(R.string.downloadstarting));

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, mContext.getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);


    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.DELETEPHOTOVIDEO_METHODNAME.equalsIgnoreCase(methodName)) {

            photoVideoFileDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).photoVideoFileDataDao();
            photoVideoFileDataDao.deletePhotoVideoFileByID(DELETE_ID);
            wallList.remove(posRemove);
            notifyDataSetChanged();

            if (wallList.get(posRemove).getFileType().equalsIgnoreCase("Video")) {

                Utility.showToast(mContext.getResources().getString(R.string.videodelete), mContext);

            } else if (wallList.get(posRemove).getFileType().equalsIgnoreCase("IMAGE")) {

                Utility.showToast(mContext.getResources().getString(R.string.photodelete), mContext);

            } else {

                Utility.showToast(mContext.getResources().getString(R.string.filedelete), mContext);

            }

        }


    }


}
