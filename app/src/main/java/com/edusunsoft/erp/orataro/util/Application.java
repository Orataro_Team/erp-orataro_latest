package com.edusunsoft.erp.orataro.util;


import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.edusunsoft.erp.orataro.Utilities.SharedPreferenceUtil;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;


public class Application extends MultiDexApplication {

    private int SelectionLimit = 5;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        SharedPreferenceUtil.init(this);
        SharedPreferenceUtil.save();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(Application.this);
    }

    public int getSelectionLimit() {

        return SelectionLimit;

    }

    public void setSelectionLimit(int selectionLimit)
    {
        SelectionLimit = selectionLimit;
    }

}
