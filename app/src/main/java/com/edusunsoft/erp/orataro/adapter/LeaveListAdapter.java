package com.edusunsoft.erp.orataro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddLeaveActivity;
import com.edusunsoft.erp.orataro.activities.ViewLeaveDetailActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.LeaveListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class LeaveListAdapter extends BaseAdapter implements ResponseWebServices, OnClickListener {

    private LayoutInflater layoutInfalater;
    private Context context;
    private TextView homeworkListSubject;
    private ImageView list_image;//, iv_cr_detail_chart;
    private TextView txt_date;
    private TextView txt_subject, txt_sub_detail;
    private LinearLayout ll_date, ll_date_img;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private RefreshListner listner;
    private ArrayList<LeaveListModel> leaveListModels, copyList;
    private int pos = 0;

    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private String memType;
    private TextView tv_approved;
    private TextView tv_pending;
    private TextView tv_rejected;
    private LinearLayout ll_view;
    private StdDivSubModel stdandardModel;

    public LeaveListAdapter(Context context,
                            ArrayList<LeaveListModel> mleaveListModels, RefreshListner listner, StdDivSubModel stdandardModel) {
        this.context = context;
        this.leaveListModels = mleaveListModels;
        this.listner = listner;
        this.stdandardModel = stdandardModel;
        copyList = new ArrayList<LeaveListModel>();
        copyList.addAll(mleaveListModels);
    }

    @Override
    public int getCount() {
        return leaveListModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.leave_listraw, parent, false);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        TextView txtyear = (TextView) convertView.findViewById(R.id.txt_date);
        TextView txtMonth = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.tv_date);
        txt_subject = (TextView) convertView.findViewById(R.id.tv_subject);
        txt_sub_detail = (TextView) convertView.findViewById(R.id.tv_sub_detail);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        tv_approved = (TextView) convertView.findViewById(R.id.tv_approved);
        ll_date_img.setTag(position);
        final QuickAction quickActionForEditOrDelete;
        ActionItem actionEdit, actionDelete;
        actionEdit = new ActionItem(ID_EDIT, "Edit", context.getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, "Delete", context.getResources().getDrawable(R.drawable.delete));
        quickActionForEditOrDelete = new QuickAction(context, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos,
                                    int actionId) {

                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_EDIT) {
                    int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    btnClick(editPos, 1);
                } else if (actionId == ID_DELETE) {
                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    Utility.deleteDialog(context, context.getResources().getString(R.string.leave), "", new Popup() {

                        @Override
                        public void deleteYes() {
                            deleteCircular(leaveListModels.get(deletepos).getSchoolLeaveNoteID());
                        }

                        @Override
                        public void deleteNo() {

                        }
                    });
                }
            }

        });

        quickActionForEditOrDelete.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        convertView.setBackgroundColor(colors[position % colors.length]);
        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);

        if (leaveListModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        ll_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        if (Utility.isTeacher(context)) {
            ll_date_img.setVisibility(View.GONE);
        } else {
            if (leaveListModels.get(position).getApplicationStatus_Term().equalsIgnoreCase("Pending")) {
                ll_date_img.setVisibility(View.VISIBLE);
            } else {
                ll_date_img.setVisibility(View.GONE);
            }
        }

        tv_approved.setText("" + leaveListModels.get(position).getApplicationStatus_Term());
        if (leaveListModels.get(position).getApplicationStatus_Term().equalsIgnoreCase("Pending")) {
            tv_approved.setTextColor(Color.BLUE);
        } else if (leaveListModels.get(position).getApplicationStatus_Term().equalsIgnoreCase("Approved")) {
            tv_approved.setTextColor(Color.parseColor("#008000"));
        } else {
            tv_approved.setTextColor(Color.RED);
        }

        txt_date.setText(leaveListModels.get(position).getCr_date());
        txt_subject.setText(leaveListModels.get(position).getApplicationBY());
        txt_sub_detail.setText(leaveListModels.get(position).getReasonForLeave());
        txtMonth.setText(leaveListModels.get(position).getCr_month());
        txtyear.setText(leaveListModels.get(position).getYear());

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                btnClick(position, 0);
            }

        });

        ll_date_img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickActionForEditOrDelete.show(v);
            }

        });

        return convertView;
    }

    public void deleteCircular(String id) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.LEAVE_SCHOOLLEAVENOTEID, id));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.LEAVE_DELETE_METHODNAME, ServiceResource.LEAVE_URL);

    }

    public void btnClick(int position, int i) {


//        if (i == 0) {

        if (new UserSharedPrefrence(context).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {

            Intent intent = new Intent(context, AddLeaveActivity.class);
            intent.putExtra("model", leaveListModels.get(position));
            intent.putExtra("isEdit", true);
            ((Activity) context).startActivityForResult(intent, 1);

        } else {

            Intent intent = new Intent(context, ViewLeaveDetailActivity.class);
            intent.putExtra("model", leaveListModels.get(position));
            intent.putExtra("stdmodel", stdandardModel);
            ((Activity) context).startActivityForResult(intent, 1);

        }

//        }

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            btnClick(pos, 0);
        } else if (ServiceResource.LEAVE_DELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            listner.refresh(methodName);
        } else if (ServiceResource.ASSOCIATIONDELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            listner.refresh(methodName);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_approved:
                break;
            default:
                break;
        }
    }

}
