package com.edusunsoft.erp.orataro.model;

public class PocketMoneyDetailModel {

    String StudentName, AcctNo, BuildingFloorRoom,SeqNo;
    Double CurrentBalance, OpeningBalance;

    public String getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(String seqNo) {
        SeqNo = seqNo;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getAcctNo() {
        return AcctNo;
    }

    public void setAcctNo(String acctNo) {
        AcctNo = acctNo;
    }

    public String getBuildingFloorRoom() {
        return BuildingFloorRoom;
    }

    public void setBuildingFloorRoom(String buildingFloorRoom) {
        BuildingFloorRoom = buildingFloorRoom;
    }

    public Double getCurrentBalance() {
        return CurrentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        CurrentBalance = currentBalance;
    }

    public Double getOpeningBalance() {
        return OpeningBalance;
    }

    public void setOpeningBalance(Double openingBalance) {
        OpeningBalance = openingBalance;
    }

}
