package com.edusunsoft.erp.orataro.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.NotificationListAdapter;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView.OnLoadMoreListener;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.NotificationListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;

@SuppressLint("InflateParams")
public class NotificationListFragment extends Fragment implements
        OnClickListener, ResponseWebServices {

    private PullAndLoadListView lst_notification;
    private ImageView img_back;
    private NotificationListModel notificationListModel;
    private NotificationListAdapter notification_ListAdapter;
    private String notificationIdParam;
    private Context mContext;
    private int count = 1;
    protected boolean isMoreData;
    private TextView txt_no_data_found;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View convertview = inflater.inflate(R.layout.activity_notification, null);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = getActivity();
        try {
            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Notification) + " (" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lst_notification = (PullAndLoadListView) convertview.findViewById(R.id.lst_notification);
        Global.notificationListModels = new ArrayList<NotificationListModel>();
        img_back = (ImageView) convertview.findViewById(R.id.img_back);

        txt_no_data_found = (TextView) convertview.findViewById(R.id.txt_no_data_found);
        img_back.setOnClickListener(this);

        if (Utility.isNetworkAvailable(mContext)) {

            notificationList(count, true);

        } else {

            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");

        }

        lst_notification.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                if (isMoreData) {

                    count += 15;
                    notificationList(count, false);

                } else {

                    count = 0;

                }
            }
        });

        lst_notification.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {

                if (Utility.isNetworkAvailable(mContext)) {

                    Global.notificationListModels.clear();
                    notificationList(count, false);

                } else {

                    Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");

                }

            }

        });

        return convertview;
    }

    private void notificationList(int count, boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.NOTIFICATIONS_ROWCOUNT, count));
        arrayList.add(new PropertyVo(ServiceResource.NOTIFICATIONS_ISVIEW, null));

        Log.d("getNotificationlist", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.NOTIFICATIONGETLIST_METHODNAME, ServiceResource.NOTIFICATIONS_URL);

    }

    void SetNotificationFlag(String notificationIdParam) {

        if (notificationIdParam.length() > 1) {


            notificationIdParam = notificationIdParam.substring(0, notificationIdParam.length() - 1);


        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MULTIDATAIDSEPRATBYCOMMA, notificationIdParam));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SETVIEWFLAGEONNOTIFICATION_METHODNAME, ServiceResource.NOTIFICATIONS_URL);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:


                break;

            default:

                break;

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.NOTIFICATIONGETLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName + count, mContext);
            parseNotification(result, ServiceResource.UPDATE);
            notificationCount();

        } else if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("getnotificationCount", result);

            JSONObject obj;
            JSONArray jsonObj;
            JSONArray jsonObjfriend;

            try {

                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                obj = new JSONObject(result);
                jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
                jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);
                JSONArray jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));

                }

                for (int i = 0; i < jsonObjfriend.length(); i++) {

                    JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));

                }

                for (int i = 0; i < jsonObjleave.length(); i++) {

                    JSONObject innerObj = jsonObjleave.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

        }

    }

    public void notificationCount() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.NOTIFICATIONCOUNT_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

    public void parseNotification(String result, String update) {

        if (result != null && !result.toString().equals("[{\"message\":\"No Data Found\",\"success\":0}]")) {

            isMoreData = true;
            notificationIdParam = "";

            Log.d("getnotificationresult", result);

            try {

                JSONArray mainJsonArray = new JSONArray(result);

                for (int i = 0; i < mainJsonArray.length(); i++) {

                    JSONObject innerObj = mainJsonArray.getJSONObject(i);
                    notificationListModel = new NotificationListModel();
                    notificationListModel.setRowNo(innerObj.getString(ServiceResource.ROWNO));
                    notificationListModel.setNotificationID(innerObj.getString(ServiceResource.NOTIFICATIONID));
                    notificationListModel.setAssociationID(innerObj.getString(ServiceResource.ASSOCIATIONIDPARS));
                    notificationListModel.setAssociationType(innerObj.getString(ServiceResource.ASSOCIATIONTYPEPARS));
                    notificationListModel.setDateofNotification(innerObj.getString(ServiceResource.DATEOFNOTIFICATION).replace("/Date(", "").replace(")/", ""));
                    notificationListModel.setFullName(innerObj.getString(ServiceResource.FULLNAME));
                    notificationListModel.setDateofNotificationAsText(innerObj.getString(ServiceResource.DATEOFNOTIFICATIONASTEXT));
                    notificationListModel.setNotificationText(innerObj.getString(ServiceResource.NOTIFICATIONTEXT));
                    notificationListModel.setNotificationType(innerObj.getString(ServiceResource.NOTIFICATIONTYPE));
                    notificationListModel.setDetail(innerObj.getString(ServiceResource.NOTIFICATION_DETAILS));
                    notificationListModel.setSendToMemberID(innerObj.getString(ServiceResource.SENDTOMEMBERIDPARS));

                    String isView = innerObj.getString(ServiceResource.ISVIEW);

                    if (isView.equalsIgnoreCase("false")) {

                        notificationIdParam = notificationIdParam + notificationListModel.getNotificationID() + ",";
                        notificationListModel.setIsview(false);

                    } else {

                        notificationListModel.setIsview(true);

                    }


                    Global.notificationListModels.add(notificationListModel);
//                    Collections.sort(Global.notificationListModels, new SortedDate());
//                    Collections.reverse(Global.notificationListModels);

                }

            } catch (JSONException e) {

                isMoreData = false;
                e.printStackTrace();

            }

            if (Global.notificationListModels != null && Global.notificationListModels.size() > 0) {

                notification_ListAdapter = new NotificationListAdapter(mContext, Global.notificationListModels);
                lst_notification.setAdapter(notification_ListAdapter);
                SetNotificationFlag(notificationIdParam);
                lst_notification.setVisibility(View.VISIBLE);
                txt_no_data_found.setVisibility(View.GONE);

            } else {

                txt_no_data_found.setVisibility(View.VISIBLE);
                lst_notification.setVisibility(View.GONE);
            }

        } else {

            isMoreData = false;

        }

    }

    public class SortedDate implements Comparator<NotificationListModel> {
        @Override
        public int compare(NotificationListModel o1, NotificationListModel o2) {
            return o1.getDateofNotification().compareTo(o2.getDateofNotification());
        }
    }

}
