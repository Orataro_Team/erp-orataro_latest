package com.edusunsoft.erp.orataro.model;

public class SubjectModel {
	
	private String subjectname,subjectimage,subjectId;

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectname() {
		return subjectname;
	}

	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}

	public String getSubjectimage() {
		return subjectimage;
	}

	public void setSubjectimage(String subjectimage) {
		this.subjectimage = subjectimage;
	}

}
