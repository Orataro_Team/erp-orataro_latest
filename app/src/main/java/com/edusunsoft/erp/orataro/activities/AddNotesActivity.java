package com.edusunsoft.erp.orataro.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.SelectStudentInterface;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.NotesListAdapter;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.adapter.StudentListAdapter;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.database.NoticeListModel;
import com.edusunsoft.erp.orataro.model.CircularNoteStudentModel;
import com.edusunsoft.erp.orataro.model.GetProjectTypeModel;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.NotesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.FileUtils;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.ImageAndroid11;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import id.zelory.compressor.Compressor;

//Add note
//Add Todo (reminder)

public class AddNotesActivity extends Activity implements OnClickListener, ResponseWebServices, SelectStudentInterface {

    private ListView noticeListView;
    private NotesListAdapter noticeListAdapter;
    private LinearLayout ll_add_new;
    private Dialog view;
    private LinearLayout ll_comming_soon;
    private FrameLayout fl_main;
    private Context mContext;
    private TextView txt_nodatafound;
    private ArrayList<NotesModel> noticeModels;
    private NotesModel noticeModel;
    private EditText edt_subject_name, edt_notes_title, edt_note_details;
    private Spinner edt_dress_code;
    private Spinner spn_status;
    private TextView tv_start_date, tv_end_date;
    private int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1, R.drawable.teacher_2};
    private int pos;
    private String subject_name, notes_title, note_details, dress_code;
    private int date;
    private boolean isEdit;
    private NoticeListModel model;
    private Calendar c;
    private int myear;
    private int month;
    private int day, mHour, mMinute;
    private String todayDate;
    private String gradeId;
    private String divisionId;
    private String subjectId;
    private String subjectName;
    private String isFrom;
    private boolean isTodo, isTodoEdit;
    private static final int DATE_DIALOG_ID = 111;
    private static final int REQUEST_PATH = 112;
    private int fileType = 0;
    private TextView header_text;
    private TodoListModel todoModel;
    private ArrayList<String> strings = new ArrayList<String>();
    private LinearLayout ll_imgattechment;
    private ImageView iv_attachment;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    public ArrayList<GetProjectTypeModel> getProjectTypes = new ArrayList<>();
    private byte[] byteArray;
    private String fielPath;
    private Uri fileUri;
    private TextView txtGradeDivision;
    private String gradeDivisionIdStr = "";
    private String gradeDivisionWallIdStr = "";
    protected static final int RESULT_SPEECH = 121, RESULT_SPEECH_DESCRIPTION = 122;
    private String filePath;

    Bitmap photoBitmap = null;

    protected static final int READ_EXTERNAL_REQUEST_CODE = 110;

    Bundle bundle;
    public boolean DateCompare;

    ImageView img_speack, img_speack_description;
    LinearLayout ll_save;

    // added to add link for online video url or link - 05-06-2020
    EditText edt_referanceLink;

    // variable declaration for select student functionality
    ArrayList<CircularNoteStudentModel> NoteStudentList;
    private ArrayList<CircularNoteStudentModel> localAddGroupMemberModels;
    private ArrayList<CircularNoteStudentModel> tempGroupMemberList;
    protected ArrayList<CircularNoteStudentModel> jobAllGroupMembers;
    private StudentListAdapter studentlistAdapter;
    private String groupMemberStudentStr = "", StrSelectedStudent = "";
    ImageView selectall;

    String BASE64_STRING = "", Path = "";

    final long MB = 1024 * 1024;
    public String file_size;
    final DecimalFormat format = new DecimalFormat("#.#");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);
        mContext = this;
        bundle = savedInstanceState;

        if (getIntent() != null) {

            gradeId = getIntent().getStringExtra("gradeId");
            divisionId = getIntent().getStringExtra("divisionId");
            subjectId = getIntent().getStringExtra("subjectId");
            subjectName = getIntent().getStringExtra("subjectName");
            isFrom = getIntent().getStringExtra("isFrom");
            isEdit = getIntent().getBooleanExtra("isEdit", false);
            if (!isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG) && isEdit == true) {
                model = (NoticeListModel) getIntent().getSerializableExtra("model");
            }

        }

        if (getIntent() == null) {
            if (savedInstanceState != null) {
                fileUri = Uri.parse(savedInstanceState.getString("file_uri"));
                Utility.getUserModelData(mContext);
                gradeId = savedInstanceState.getString("gradeId");
                divisionId = savedInstanceState.getString("divisionId");
                subjectId = savedInstanceState.getString("subjectId");
                subjectName = savedInstanceState.getString("subjectName");
                isFrom = savedInstanceState.getString("isFrom");
                isEdit = savedInstanceState.getBoolean("isEdit", false);
                if (!isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG) && isEdit == true) {
                    model = (NoticeListModel) savedInstanceState.getSerializable("model");
                }
            }
        }

        header_text = (TextView) findViewById(R.id.header_text);
        ll_imgattechment = (LinearLayout) findViewById(R.id.ll_imgattechment);
        iv_attachment = (ImageView) findViewById(R.id.iv_attachment);
        img_speack = (ImageView) findViewById(R.id.img_speack);
        img_speack_description = (ImageView) findViewById(R.id.img_speack_description);
        edt_subject_name = (EditText) findViewById(R.id.edt_subject_name);
        edt_notes_title = (EditText) findViewById(R.id.edt_notes_title);
        edt_note_details = (EditText) findViewById(R.id.edt_note_details);
        edt_referanceLink = (EditText) findViewById(R.id.edt_referanceLink);
        txtGradeDivision = (TextView) findViewById(R.id.txtGradeDivision);
        edt_dress_code = (Spinner) findViewById(R.id.edt_dress_code);
        spn_status = (Spinner) findViewById(R.id.spn_status);
        tv_start_date = (TextView) findViewById(R.id.tv_start_date);
        tv_end_date = (TextView) findViewById(R.id.tv_end_date);
        ImageView img_back = (ImageView) findViewById(R.id.img_back);
        ImageView iv_save = (ImageView) findViewById(R.id.iv_save);
        iv_save.setVisibility(View.GONE);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        img_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);

        if (isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG)) {
            ll_imgattechment.setVisibility(View.GONE);
            isTodo = true;
            isTodoEdit = getIntent().getBooleanExtra("isEdit", false);
            if (isTodoEdit) {
                todoModel = (TodoListModel) getIntent().getSerializableExtra("model");
                fiiTodoData(todoModel);
            }
            header_text.setText(mContext.getResources().getString(R.string.Todo));
        } else {
            ll_imgattechment.setVisibility(View.VISIBLE);
            txtGradeDivision.setVisibility(View.VISIBLE);
            if (Utility.isNetworkAvailable(mContext)) {
                getStandarDivisionList();
                parsestddivisionlist(Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext));
            } else {
                parsestddivisionlist(Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext));
            }
            edt_subject_name.setVisibility(View.GONE);
        }

        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        todayDate = (day < 10 ? "0" + day : day)
                + "-"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "-" + myear;

        if (isTodo) {
            edt_subject_name.setVisibility(View.GONE);
        } else {
            edt_subject_name.setText(subjectName);
            edt_subject_name.setEnabled(false);
        }
        tv_start_date.setOnClickListener(this);
        tv_end_date.setOnClickListener(this);
        iv_attachment.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        txtGradeDivision.setOnClickListener(this);
        img_speack.setOnClickListener(this);
        img_speack_description.setOnClickListener(this);

        if (isTodo) {

            ArrayList<String> strings = new ArrayList<String>();
            strings.add("Important");
            strings.add("Normal");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            edt_dress_code.setAdapter(adapter);
            if (isTodoEdit) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (todoModel.getTypeTerm().trim().equals(adapter.getItem(i).toString())) {
                        edt_dress_code.setSelection(i);
                        break;
                    }
                }
            }
            ArrayList<String> strings1 = new ArrayList<String>();
            strings1.add("Cancelled");
            strings1.add("Completed");
            strings1.add("In-Progress");
            strings1.add("Insufficient");
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings1);

            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //					Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            spn_status.setAdapter(adapter1);
            if (isTodoEdit) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (todoModel.getStatus().trim().equals(adapter.getItem(i).toString())) {
                        edt_dress_code.setSelection(i);
                        break;
                    }
                }
            }
        } else {

            spn_status.setVisibility(View.GONE);

            if (Utility.isNetworkAvailable(mContext)) {

                getProjectType();

            } else {

                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            }

            if (isEdit) {

                fillNote();

            }
        }
    }


    // set data of patcilar notes in edit text and spiner when it in edit mode
    /*
     *
     * */
    private void fillNote() {

        edt_notes_title.setText(model.getNoteTitle());
        edt_note_details.setText(model.getNoteDetails());
        tv_start_date.setText(model.getActionStartDate());
        tv_end_date.setText(model.getActionEndDate());
        edt_subject_name.setText(model.getSubjectName());
        edt_referanceLink.setText(model.getReferenceLink());


        if (Utility.isNull(model.getActionStartDate())) {
            tv_start_date.setText(Utility.dateFormate(model.getActionStartDate(), "dd-MM-yyyy", "dd/MM/yyyy"));
        }

        if (Utility.isNull(model.getActionEndDate())) {
            tv_end_date.setText(Utility.dateFormate(model.getActionEndDate(), "dd-MM-yyyy", "dd/MM/yyyy"));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do your stuff

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog datePickerDialog;

        switch (id) {

            case DATE_DIALOG_ID:

                datePickerDialog = new DatePickerDialog(this, datePickerListener,
                        myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;

        }

        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (date == 1) {
                tv_end_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            } else if (date == 2) {
                tv_start_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            }

            date = 0;
        }
    };
    private String curFileName;
    private String FileMineType;


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:
                finish();
                break;
            case R.id.img_speack:
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {
                    Utility.showToast("Opps! Your device doesn't support Speech to Text", mContext);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                    startActivity(browserIntent);
                    overridePendingTransition(0, 0);
                }
                break;
            case R.id.img_speack_description:
                Intent descintent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                descintent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(descintent, RESULT_SPEECH_DESCRIPTION);
                } catch (ActivityNotFoundException a) {
                    Utility.showToast("Opps! Your device doesn't support Speech to Text", mContext);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                    startActivity(browserIntent);
                    overridePendingTransition(0, 0);
                }
                break;

            case R.id.ll_save:

                if (isTodo) {

                    if (!Utility.isNull(edt_notes_title.getText().toString())) {
                        edt_notes_title.setError(mContext.getResources().getString(R.string.enternotetitle));
                    } else if (!Utility.isNull(tv_start_date.getText().toString())) {
                        tv_start_date.setError(mContext.getResources().getString(R.string.enterstartdate));
                    } else if (!Utility.isNull(tv_end_date.getText().toString())) {
                        tv_end_date.setError(mContext.getResources().getString(R.string.enterenddate));
                    } else {
                        addTodo();
                    }

                } else {
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        DateCompare = Utility.CompareDates(df, df.parse(tv_start_date.getText().toString()), df.parse(tv_end_date.getText().toString()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (!Utility.isNull(edt_notes_title.getText().toString())) {
                        edt_notes_title.setError(mContext.getResources().getString(R.string.enternotetitle));
                    } else if (txtGradeDivision.getText().toString().equalsIgnoreCase("")) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.eventstddivValidation));
                    } else if (!Utility.isNull(tv_start_date.getText().toString())) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.enterstartdate));
                    } else if (!Utility.isNull(tv_end_date.getText().toString())) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.enterenddate));
                    } else if (DateCompare) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.datevalidation));
                    }
//                    else if ((!tv_start_date.getText().toString().trim().isEmpty() && !tv_end_date.getText().toString().isEmpty()) && tv_start_date.getText().toString().compareTo(tv_end_date.getText().toString()) > 0) {
//                        Utility.showToast(mContext.getResources().getString(R.string.datevalidation), mContext);
//                    }
                    else {
                        addNote();
                    }
                }

                break;

            case R.id.iv_save:
                break;

            case R.id.tv_end_date:
                date = 1;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.tv_start_date:
                date = 2;
                showDialog(DATE_DIALOG_ID);
                break;

            case R.id.iv_attachment:
                selectImage(1);
                break;
            case R.id.txtGradeDivision:
                showStanderdPopup();
                break;
            default:
                break;
        }
    }

    public void fiiTodoData(TodoListModel model) {
        if (Utility.isNull(model.getTitle())) {
            edt_notes_title.setText(model.getTitle());
        }

        if (Utility.isNull(model.getDetails())) {
            edt_note_details.setText(model.getDetails());
        }

        if (Utility.isNull(model.getEndDate())) {
            tv_end_date.setText(model.getEndDate());
        }

        if (Utility.isNull(model.getCreateOn())) {
            tv_start_date.setText(model.getCreateOn());
        }
    }

    @Override
    public void getAllSelectedStudent(int pos, ArrayList<CircularNoteStudentModel> addselectedstudentModels) {
        jobAllGroupMembers = addselectedstudentModels;

    }

    // class FOr sort data in Arraylist
    public class SortedDate implements Comparator<NotesModel> {
        @Override
        public int compare(NotesModel o1, NotesModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }

    // add note webservice
    public void addNote() {

        Utility.getUserModelData(mContext);
        ContentValues values = new ContentValues();

        if (isEdit) {
            values.put(ServiceResource.NOTES_EDITID, model.getNotesID());
        } else {
            values.put(ServiceResource.NOTES_EDITID, "");
        }

        values.put(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString());
        values.put(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.BEATCH_ID, "");
        values.put(ServiceResource.GRADEDIVISOINID, StrSelectedStudent);
        values.put(ServiceResource.GRADEDIVISOINWALLID, gradeDivisionWallIdStr);
//        values.put(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr);
        values.put(ServiceResource.NOTES_SUBJECTID, "");

        if (Utility.BASE64_STRING != null) {
            Log.d("getfilename", new File(Utility.NewFileName).getName());
            if (fileType == 0) {
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, new File(Utility.NewFileName).getName());
                values.put(ServiceResource.FILETYPE, "IMAGE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, curFileName);
                values.put(ServiceResource.FILETYPE, "FILE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            }
        } else {
            values.put(ServiceResource.FILE, "");
            values.put(ServiceResource.FILENAME, "");
            values.put(ServiceResource.FILETYPE, "");
            values.put(ServiceResource.FILEMINETYPE, "");
        }

        String DressCode = "";

        try {
            DressCode = edt_dress_code.getSelectedItem().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        values.put(ServiceResource.NOTES_NOTETITLE, edt_notes_title.getText().toString());
        values.put(ServiceResource.NOTES_RATING, "0");
        values.put(ServiceResource.NOTES_DRESSCODE, DressCode);
        values.put(ServiceResource.NOTES_NOTEDETAILS, edt_note_details.getText().toString().replace("'", ""));
        values.put(ServiceResource.NOTES_ACTIONSTARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {

            values.put(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy"));
        } else {
            values.put(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        }
        values.put(ServiceResource.NOTES_INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId());


        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_EDITID, model.getNotesID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_EDITID, null));
        }


        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINID, StrSelectedStudent));
        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINWALLID, gradeDivisionWallIdStr));
//        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_SUBJECTID, null));

        if (Utility.BASE64_STRING != null) {
            if (fileType == 0) {
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, new File(Utility.NewFileName).getName()));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "IMAGE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, curFileName));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "FILE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            }
        } else {
            arrayList.add(new PropertyVo(ServiceResource.FILE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME, null));
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, null));
        }

        try {
            DressCode = edt_dress_code.getSelectedItem().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        arrayList.add(new PropertyVo(ServiceResource.NOTES_NOTETITLE, edt_notes_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_RATING, "0"));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_DRESSCODE, DressCode));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_NOTEDETAILS, edt_note_details.getText().toString().replace("'", "")));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONSTARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));

        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        }

        arrayList.add(new PropertyVo(ServiceResource.NOTES_INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId()));
        arrayList.add(new PropertyVo("Timing", "2:00 pm"));
        arrayList.add(new PropertyVo("EndTime", "3:00 pm"));

        Log.d("CreateNoteRequest", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CREATENOTESWITHMULTY_METHODNAME, ServiceResource.CHECK_URL);
        }

    }

    private void addTodo() {

        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (isTodoEdit) {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, todoModel.getTodosID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
        }

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TITLE, edt_notes_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_DETAILS, edt_note_details.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TYPETERM, edt_dress_code.getSelectedItem().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STATUS, spn_status.getSelectedItem().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_ENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_COMPLETDATE, ""));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDTODO_METHODNAME,
                ServiceResource.TODO_URL);

    }

    public void getProjectType() {

        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY, "DressCode,Rating"));

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.LOGIN_GETPROJECTTYPE, ServiceResource.LOGIN_URL);

    }

    public void response(String result, String methodName) {

        if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {
            parseprojecttype(result);
        }

        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parsestddivisionlist(result);
        }

        if (ServiceResource.ADDTODO_METHODNAME.equalsIgnoreCase(methodName)) {

            Intent i = new Intent(AddNotesActivity.this, HomeWorkFragmentActivity.class);
            i.putExtra("position", mContext.getResources().getString(R.string.Todo));
            startActivity(i);
            overridePendingTransition(0, 0);

        }

        if (ServiceResource.CREATENOTESWITHMULTY_METHODNAME.equalsIgnoreCase(methodName)) {

            getProjectTypes.clear();

            Utility.sendPushNotification(mContext);
            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);

                    for (int i = 0; i < hJsonArray.length(); i++) {

                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        GetProjectTypeModel model = new GetProjectTypeModel();
                        model.setCategory(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
                        model.setDefaultValue(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_DEFAULTVALUE));
                        model.setIsDefault(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                        model.setOrderNo(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                        model.setTerm(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERM));
                        model.setTermID(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERMID));
                        getProjectTypes.add(model);

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Utility.ISLOADNOTE = true;
            Utility.BASE64_STRING = "";
            Utility.showToast(getResources().getString(R.string.notesuccessmessage), mContext);
            finish();

        }

        if (ServiceResource.GETSTUDENTLIST.equals(methodName)) {

            Log.d("getstudlist", result);
            try {

                JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
                NoteStudentList = new ArrayList<CircularNoteStudentModel>();
                CircularNoteStudentModel model;

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    model = new CircularNoteStudentModel();
                    model.setMemberID(hJsonObject.getString(ServiceResource.STUDENT_MEMBERID));
                    model.setFullName(hJsonObject.getString(ServiceResource.STUDENT_FULLNAME));
                    model.setRegistrationNo(hJsonObject.getString(ServiceResource.STUDENT_REGNO));
                    model.setGradeName(hJsonObject.getString(ServiceResource.STUDENT_GRADENAME));
                    model.setDivisionName(hJsonObject.getString(ServiceResource.STUDENT_DIVISIONNAME));
                    model.setGradeID(hJsonObject.getString(ServiceResource.STUDENT_GRADEID));
                    model.setDivisionID(hJsonObject.getString(ServiceResource.STUDENT_DIVID));
                    model.setFName(hJsonObject.getString(ServiceResource.STUDENT_FNAME));
                    model.setFContactNo(hJsonObject.getString(ServiceResource.STUDENT_FCONTACTNO));
                    NoteStudentList.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            showListDialogToShowStudentlist(mContext, NoteStudentList);

        }

    }

    private void parseprojecttype(String result) {

        JSONArray hJsonArray;
        getProjectTypes.clear();
        try {
            if (result.contains("\"success\":0")) {

            } else {


                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    GetProjectTypeModel model = new GetProjectTypeModel();
                    model.setCategory(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
//                    model.setDefaultValue(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_DEFAULTVALUE));
                    model.setIsDefault(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                    model.setOrderNo(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                    model.setTerm(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERM));
                    model.setTermID(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERMID));
                    getProjectTypes.add(model);

                    Log.d("getProjectType", getProjectTypes.toString());

                }

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        ArrayList<GetProjectTypeModel> getProjectType_models = new ArrayList<GetProjectTypeModel>();

        if (getProjectTypes != null && getProjectTypes.size() > 0) {
            for (int i = 0; i < getProjectTypes.size(); i++) {
                if (getProjectTypes.get(i).getCategory().equals("DressCode")) {
                    getProjectType_models.add(getProjectTypes.get(i));
                }
            }
        }

        if (getProjectType_models != null && getProjectType_models.size() > 0) {
            for (int i = 0; i < getProjectType_models.size(); i++) {
                strings.add(getProjectType_models.get(i).getTerm());
            }
        }

        if (strings != null && strings.size() > 0) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            edt_dress_code.setAdapter(adapter);
        }
        if (isEdit) {
            for (int i = 0; i < strings.size(); i++) {
                if (strings.get(i).equalsIgnoreCase(model.getDressCode())) {
                    edt_dress_code.setSelection(i);
                }
            }
        }
    }

    private void parsestddivisionlist(String result) {

        JSONArray hJsonArray;

        try {

            Global.holidaysModel = new HolidaysModel();

            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);
                Global.StandardModels = new ArrayList<StandardModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    boolean isAlreadyInsert = false;
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StandardModel model = new StandardModel();
                    model.setStandardName(hJsonObject
                            .getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject
                            .getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject
                            .getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject
                            .getString(ServiceResource.DIVISION));
                    model.setSubjectId(hJsonObject
                            .getString(ServiceResource.STANDARD_SUBJECTID));
                    model.setSubjectName(hJsonObject
                            .getString(ServiceResource.SUBJECT));
                    model.setGradeWallID(hJsonObject.getString(ServiceResource.GRADEWALLID));
                    model.setGradeDivisionWallID(hJsonObject.getString(ServiceResource.GRADEDIVISIONWALLID));
                    model.setGradeDivisionSubjectWallID(hJsonObject.getString(ServiceResource.GRADEDIVSUBALLID));
                    if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
                        for (int j = 0; j < Global.StandardModels.size(); j++) {
                            if (Global.StandardModels.get(j).getDivisionId().equalsIgnoreCase(model.getDivisionId())
                                    && Global.StandardModels.get(j).getStandrdId().equalsIgnoreCase(model.getStandrdId())) {
                                isAlreadyInsert = true;
                            }
                        }
                    }

                    if (!isAlreadyInsert) {
                        Global.StandardModels.add(model);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    ///select image from gallary and capture image from camera
    private void selectImage(final int i) {
        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.fromlib),
                mContext.getResources().getString(R.string.File),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {


                    /*commented By Hardik : 29-04-2019 - Android 11 image capture*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        try {
                            startActivityForResult(takePictureIntent, ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11);
                        } catch (ActivityNotFoundException e) {
                            Log.e("PATH","PATH:: "+e.toString());
                        }
                    } else{
                        Intent intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);

                    }


                } else if (items[item].equals(mContext.getResources().getString(R.string.fromlib))) {



                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11);
                    } else{
                        Intent intent = new Intent(AddNotesActivity.this, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILE);
                    }

                } else if (items[item].equals(mContext.getResources().getString(R.string.File))) {

                    // change file picker from filechooserActivity

                    /*END*/

                    if (isPermissionGranted()) {
                        // change file picker from filechooserActivity



                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.setType("*/*"); // for all file
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);

                            String[] mimeTypes = {
                                    "application/pdf",
                                    "application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                    "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                    "text/plain"};
                            intent .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            startActivityForResult(intent, ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11);
                        } else{
                            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                            chooseFile.setType("*/*");
                            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
                            startActivityForResult(chooseFile, REQUEST_PATH);
                        }
                        /*END*/
                    }

                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {

                Utility.getUserModelData(mContext);

                try {
                    fielPath = data.getExtras().getString("imageData_uri");
                    Path = Utility.ImageCompress(fielPath, mContext);
//                    Utility.GetBASE64STRING(Path);
                    File compressedImageFile = null;
                    File file = new File(fielPath);
                    compressedImageFile = new Compressor(this).compressToFile(file);
//                        Log.d("getoriginalpath", compressedImageFile.getAbsolutePath());
////                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                    Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*commented By Krishna : 06-02-2019 convert Filepath to Bitmap amd Display Bitmap into Image*/

                Bitmap thePic;
                byteArray = null;
                byteArray = data.getExtras().getByteArray("imageData_byte");


                try {

                    GetBitmapFromFilePath(fielPath);
                    iv_attachment.setImageBitmap(photoBitmap);

                } catch (Exception e) {

                    e.printStackTrace();


                }


                /*END*/


            }else if (requestCode == ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11) {

                //handle Camera CAPTURE Image for ANDROID 11 device
                if (data != null){
                    fileType = 0;
                    handleCaptureImage_Android11(data);

                } else {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11) {

                //handle SELECTED image ANDROID 11 device
                if (data != null){
                    fileType = 0;
                    handleImageAfterSelection_Android11(data);
                } else {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }

            }  else if (requestCode == ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11) {

                if (data != null){
                    fileType = 0;
                    handleFileSelected_Android11(data);
                } else {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }

            }     else if (requestCode == SELECT_FILE) {

                if (data != null) {

                    ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                    if (imgList != null && imgList.size() > 0) {

                        fielPath = imgList.get(0).getUri().toString();

                        File file = new File(fielPath);
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(this).compressToFile(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                        Path = Utility.ImageCompress(fielPath, mContext);
                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                        /*END*/
                        GetBitmapFromFilePath(fielPath);
                        iv_attachment.setImageBitmap(photoBitmap);
                    }

                }

//                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
//                if (imgList != null && imgList.size() > 0) {
//                    try {
//                        fielPath = imgList.get(0).getUri().toString();
//                        GetBitmapFromFilePath(fielPath);
//                        iv_attachment.setImageBitmap(photoBitmap);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }

            } else if (requestCode == REQUEST_PATH) {

                fileUri = data.getData();
                filePath = fileUri.getPath();

                String selectdpath = FileUtils.getPath(AddNotesActivity.this, fileUri);
                Log.i("newImageFilePath", "" + selectdpath);

                Utility.convertToBase64String(mContext, fileUri);

                File f = new File("" + fileUri);
                curFileName = new File(selectdpath).getName();

                /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                File file = new File(curFileName);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension
                        = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());

                String mimeType
                        = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                /*END*/

                fileType = 2;
                iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

                if (curFileName.contains("pdf")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.pdf);
                } else if (curFileName.contains("PDF")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.pdf);
                } else if (curFileName.contains("txt")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.txt);
                } else if (curFileName.contains("TXT")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.txt);
                } else if (curFileName.contains("doc")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.doc);
                } else if (curFileName.contains("xlsx")) {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.excel_file);
                } else {
                    FileMineType = mimeType;
                    iv_attachment.setImageResource(R.drawable.doc);
                }


//                fileUri = data.getData();
//                filePath = fileUri.getPath();
//
//                // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                Utility.convertToBase64String(mContext, fileUri);
//                /*END*/
//
//                curFileName = new File(filePath).getName();
//                Log.d("curlfilename", curFileName);
//
//                fileType = 2;
//                iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
//                if (curFileName.contains("pdf")) {
//                    FileMineType = "PDF";
//                    iv_attachment.setImageResource(R.drawable.pdf);
//                } else if (curFileName.contains("PDF")) {
//                    FileMineType = "PDF";
//                    iv_attachment.setImageResource(R.drawable.pdf);
//                } else if (curFileName.contains("txt")) {
//                    FileMineType = "TEXT";
//                    iv_attachment.setImageResource(R.drawable.txt);
//                } else if (curFileName.contains("TXT")) {
//                    FileMineType = "TEXT";
//                    iv_attachment.setImageResource(R.drawable.txt);
//                } else if (curFileName.contains("doc")) {
//                    FileMineType = "WORD";
//                    iv_attachment.setImageResource(R.drawable.doc);
//                } else if (curFileName.contains("xlsx")) {
//                    FileMineType = "EXCEL";
//                    iv_attachment.setImageResource(R.drawable.excel_file);
//                } else {
//                    FileMineType = "DOCUMENT";
//                    iv_attachment.setImageResource(R.drawable.doc);
//                }

//                // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                Utility.GetBASE64STRINGFROMFILE(BitmapUtil.getRealPathFromURI(filePath, mContext));
//                /*END*/

            } else if (requestCode == RESULT_SPEECH) {

                ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edt_notes_title.setText(edt_notes_title.getText().toString() + "" + text.get(0));

            } else if (requestCode == RESULT_SPEECH_DESCRIPTION) {

                ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edt_note_details.setText(edt_note_details.getText().toString() + "" + text.get(0));

            }

        }


    }

    private void handleCaptureImage_Android11(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");

        fielPath = ImageAndroid11.getPathFromBitmap(this,imageBitmap);


        if (fielPath != null){
            Utility.BASE64_STRING = "";
            Utility.getUserModelData(mContext);

            try {
                Path = Utility.ImageCompress(fielPath, mContext);

                Utility.GetBASE64STRING(fielPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*commented By Krishna : 06-02-2019 convert Filepath to Bitmap amd Display Bitmap into Image*/


            try {
                // GetBitmapFromFilePath(fielPath);
                photoBitmap = imageBitmap;
                iv_attachment.setImageBitmap(photoBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void handleImageAfterSelection_Android11(Intent data) {
        if (data.getData() != null){

            try {
                Uri selectedImage = data.getData();
                fielPath = ImageAndroid11.getPathFromURI(this,selectedImage);
                Log.d("PATH","PATH:: "+fielPath);

                //Must apply base 64 string function before upload photo
                if (fielPath != null){
                    Path = fielPath;

                    Path = Utility.ImageCompress(fielPath, mContext);

                    Utility.GetBASE64STRING(fielPath);
                    Log.d("SS PATH","PATH:: "+fielPath);

                    iv_attachment.setImageURI(Uri.parse(fielPath));
//                    iv_attachment.setImageBitmap(photoBitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }


        } else  {
            if (data.getClipData() != null) {
                try {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();

                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                    }

                    for (int i = 0; i < mArrayUri.size(); i++) {
                        fielPath = ImageAndroid11.getPathFromURI(this,mArrayUri.get(i));
                        Log.d("PATH","PATH:: "+fielPath);

                        //Must apply base 64 string function before upload photo
                        if (fielPath != null){
                            Path = fielPath;

                            Path = Utility.ImageCompress(fielPath, mContext);

                            Utility.GetBASE64STRING(fielPath);
                            Log.d("SS PATH","PATH:: "+fielPath);

                            iv_attachment.setImageURI(Uri.parse(fielPath));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    private void handleFileSelected_Android11(Intent data) {
        if (data.getData() != null) {

            try {
                Uri uri_data = data.getData();

                Log.e("PATH","data:: "+data.toString());
                Log.e("PATH","fileUri:: "+uri_data);

                String selectdpath = ImageAndroid11.getPathFromFileURL(this,uri_data);

                Log.e("PATH","PATH:: "+selectdpath);




                File file2 = new File(selectdpath);
                final double length = file2.length();

                if (length > MB) {

                    file_size = format.format(length / MB) + " MB";
                    Log.d("PATH", "SIZE:: "+file_size);

                }

                if (Double.valueOf(format.format(length / MB)) > 20) { //file size change 5 to 20 by hardik_kanak 26-11-2020
                    //                Utility.toast(mContext, "Upload File with Maximum Size of 20 MB");
                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {

                    Uri uri = Uri.parse(selectdpath);

                    fileUri = uri;
                    filePath = selectdpath;

                    Log.i("uri", "" + uri);
                    Utility.convertToBase64String(mContext, uri_data);

                    File f = new File("" + uri);

                    String tempFileName = new File(selectdpath).getName();
                    curFileName = tempFileName;
                    Log.d("PATH getcurclfilename", tempFileName + " " + uri.toString());
                    Log.d("PATH getfilename", selectdpath + " " + tempFileName + " " + f.getName());

                    File file = new File(tempFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    fileType = 2;

                    Log.e("PATH", ""+curFileName.contains("pdf") );

                    iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

                    if (curFileName.contains("pdf")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.pdf);
                    } else if (curFileName.contains("PDF")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.pdf);
                    } else if (curFileName.contains("txt")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.txt);
                    } else if (curFileName.contains("TXT")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.txt);
                    } else if (curFileName.contains("doc")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.doc);
                    } else if (curFileName.contains("xlsx")) {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.excel_file);
                    } else {
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.doc);
                    }

                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }

        }
    }

    private Bitmap GetBitmapFromFilePath(String fielPath) {

        photoBitmap = Utility.getBitmap(fielPath, mContext);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        if (fielPath.contains(".png") || fielPath.contains(".PNG")) {

            photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        } else {

            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        }

        byteArray = stream.toByteArray();

        return photoBitmap;

    }

    //get  standard and division list from webservice
    public void getStandarDivisionList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        new AsynsTaskClass(mContext, arrayList, false, AddNotesActivity.this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);

    }

    // show standard  popup which have multiple selection and make string of division id and standard id  with standardId, divisionId # format
    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(mContext);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);
        final StanderdAdapter adapter = new StanderdAdapter(mContext, Global.StandardModels, true, true);
        TextView dialogTitle = (TextView) dialogStandard.findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);
        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard.findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard.findViewById(R.id.custome_Dialog_cancle_btn);
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(mContext.getResources().getString(R.string.selectgroupmember));
        edtSearch.setVisibility(View.GONE);


        dialogTitle.setText(mContext.getResources().getString(R.string.selectstandard));
        if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
            lv.setAdapter(adapter);
        }

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
                adapter.getSelectedDivision();
                String txtTempStr = "";
                String StrStdDivID = "";
                gradeDivisionIdStr = "";
                gradeDivisionWallIdStr = "";
                for (int i = 0; i < adapter.getSelectedDivision().size(); i++) {
                    if (i == adapter.getSelectedDivision().size() - 1) {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ".";
                        StrStdDivID = StrStdDivID + adapter.getSelectedDivision().get(i).getStandrdId() + "|"
                                + adapter.getSelectedDivision().get(i).getDivisionId();
                    } else {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ",";
                        StrStdDivID = StrStdDivID + adapter.getSelectedDivision().get(i).getStandrdId() + "|"
                                + adapter.getSelectedDivision().get(i).getDivisionId() + ",";
                    }
                    String tempStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + "#";


                    String srtddivwallidStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + ","
                            + adapter.getSelectedDivision().get(i).getGradeDivisionWallID() +
                            "#";

                    gradeDivisionIdStr = gradeDivisionIdStr + tempStr;
                    gradeDivisionWallIdStr = gradeDivisionWallIdStr + srtddivwallidStr;

                }

                if (adapter.getSelectedDivision().size() > 0) {
                    try {
                        GetStudentByStdDiv(StrStdDivID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

//                txtGradeDivision.setText(txtTempStr);
            }

        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        dialogStandard.show();

    }

    private void GetStudentByStdDiv(String strStdDivID) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.STANDARDDIVISION, strStdDivID));
        Log.d("getstudrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, AddNotesActivity.this).execute(ServiceResource.GETSTUDENTLIST, ServiceResource.NOTES_URL);


    }

    private void showListDialogToShowStudentlist(Context mContext, ArrayList<CircularNoteStudentModel> circularStudentList) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customelistdialog);

        TextView dialogTitle = (TextView) dialog
                .findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialog.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        final ListView lv = (ListView) dialog.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialog
                .findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialog
                .findViewById(R.id.custome_Dialog_cancle_btn);

        selectall = (ImageView) dialog
                .findViewById(R.id.selectall);
        selectall.setVisibility(View.VISIBLE);

        edtSearch.setHint(mContext.getResources().getString(R.string.Search));
        dialogTitle.setText(mContext.getResources().getString(R.string.SelectStudent));

        if (circularStudentList != null && circularStudentList.size() > 0) {
            studentlistAdapter = new StudentListAdapter(mContext, circularStudentList, AddNotesActivity.this);
            lv.setAdapter(studentlistAdapter);
            studentlistAdapter.notifyDataSetChanged();
        }

        // select all functionality to select All Student simultaneously
        selectall.setOnClickListener(v -> {

            studentlistAdapter.SelectAll(circularStudentList, selectall);

        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (studentlistAdapter != null) {
                    studentlistAdapter.filter(text);
                }
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<CircularNoteStudentModel> selectedGroupMemberList = new ArrayList<CircularNoteStudentModel>();
                selectedGroupMemberList = getSelectedStudentList(jobAllGroupMembers);
                tempGroupMemberList = getSelectedStudentList(jobAllGroupMembers);
//                isFirstTimeGroupMember = false;
                String selectedGroupMemberStr = "";
                groupMemberStudentStr = "";
                if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                    Collections.sort(selectedGroupMemberList, new CustomComparator());
                    for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                        selectedGroupMemberStr = selectedGroupMemberStr
                                + selectedGroupMemberList.get(i).getFullName();
                        groupMemberStudentStr = groupMemberStudentStr +
                                selectedGroupMemberList.get(i).getMemberID();
                        if (selectedGroupMemberList.size() > 1) {
                            if (i != selectedGroupMemberList.size() - 1) {
                                selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                groupMemberStudentStr = groupMemberStudentStr + ",";
                            }
                        }

                        String tempStr = selectedGroupMemberList.get(i).getGradeID() + ","
                                + selectedGroupMemberList.get(i).getDivisionID() + ","
                                + selectedGroupMemberList.get(i).getMemberID() + "#";

                        StrSelectedStudent = StrSelectedStudent + tempStr;

                    }

                }

                Log.d("selectedStudentstr", selectedGroupMemberStr);
                Log.d("selectedStudentstr1233", StrSelectedStudent);
                txtGradeDivision.setText(selectedGroupMemberStr);
                dialog.dismiss();

            }

        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * get selected studentlist
     *
     * @param addGroupMemberModels
     * @return
     */
    public ArrayList<CircularNoteStudentModel> getSelectedStudentList(
            ArrayList<CircularNoteStudentModel> addGroupMemberModels) {
        ArrayList<CircularNoteStudentModel> groupMemberModels = null;
        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<CircularNoteStudentModel>();
            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                if (addGroupMemberModels.get(i).isSelected()) {
                    groupMemberModels.add(addGroupMemberModels.get(i));
                }
            }
        }

        return groupMemberModels;

    }


    public class CustomComparator implements Comparator<CircularNoteStudentModel> {
        @Override
        public int compare(CircularNoteStudentModel o1, CircularNoteStudentModel o2) {
            return o1.getFullName().compareTo(o2.getFullName());
        }
    }

}
