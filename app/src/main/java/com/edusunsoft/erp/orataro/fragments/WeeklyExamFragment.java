package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ExpandableListAdapter;
import com.edusunsoft.erp.orataro.model.ExamModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WeeklyExamFragment extends Fragment implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    List<String> GroupExamList;
    HashMap<String, List<ExamModel>> ExamlistChild;
    ExpandableListView expListView;
    ExpandableListAdapter listAdapter;
    ExamModel examModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        View view = inflater.inflate(R.layout.exam_list_activity, null);
        mLayoutInflater = inflater;

        Initialization(view);

        return view;

    }

    private void Initialization(View view) {

        ServiceResource.FROMEXAMTYPE = "WeeklyExam";

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.ExamResult) + "(" + Utility.GetFirstName(mContext) + ")");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        // get the listview
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        GroupExamList = new ArrayList<String>();

//        /*Commented By Krishna : 29-04-2019 GetWeeklyExamLisr From API*/
//
        try {
            if (Utility.isNetworkAvailable(mContext)) {
                GetWeeklyExamList();
            } else {
                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        /*END*/

    }

    private void GetWeeklyExamList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo(ServiceResource.EXAMTYPE, ServiceResource.WEEKLYEXAM));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETEXAMSCHEDULELIST, ServiceResource.EXAM_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETEXAMSCHEDULELIST.equalsIgnoreCase(methodName)) {

            try {

                GroupExamList = new ArrayList<String>();
                ExamlistChild = new HashMap<String, List<ExamModel>>();

                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jop = jArray.getJSONObject(i);
                    GroupExamList.add(jop.getString("ExamName"));
                    JSONArray datajsonarray = jop.getJSONArray("data");
                    ArrayList<ExamModel> ExamList = new ArrayList<>();

                    for (int j = 0; j < datajsonarray.length(); j++) {

                        JSONObject childobj = datajsonarray.getJSONObject(j);

                        examModel = new ExamModel();
                        examModel.setPercentage(childobj.getDouble("Percentage"));
                        examModel.setTotalMarks(childobj.getDouble("TotalMarks"));
                        examModel.setObtainedMarks(childobj.getDouble("ObtainedMarks"));
                        examModel.setClassHighest(childobj.getDouble("ClassHighest"));
                        examModel.setClassLowest(childobj.getDouble("ClassLowest"));
                        examModel.setDivisionHighest(childobj.getDouble("DivisionHighest"));
                        examModel.setDivisionLowest(childobj.getDouble("DivsionLowest"));
                        examModel.setStr_DateOfExam(childobj.getString("Str_DateOfExam"));
                        examModel.setSubjectName(childobj.getString("SubjectName"));
                        examModel.setExamTypeName(childobj.getString("ExamTypeName"));
                        examModel.setExamName(childobj.getString("ExamName"));

                        ExamList.add(examModel);

                    }

                    ExamlistChild.put(GroupExamList.get(i), ExamList);

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

            listAdapter = new ExpandableListAdapter(getActivity(), GroupExamList, ExamlistChild);
////                // setting list adapter
            expListView.setAdapter(listAdapter);

        }

    }

}
