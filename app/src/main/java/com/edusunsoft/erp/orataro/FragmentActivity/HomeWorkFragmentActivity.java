package com.edusunsoft.erp.orataro.FragmentActivity;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.DashBoardActivity;
import com.edusunsoft.erp.orataro.activities.DynamicWallListActivity;
import com.edusunsoft.erp.orataro.activities.ListLeaveActivity;
import com.edusunsoft.erp.orataro.activities.ListWallActivity;
import com.edusunsoft.erp.orataro.activities.PageListActivity;
import com.edusunsoft.erp.orataro.activities.PocketMoneyActivity;
import com.edusunsoft.erp.orataro.activities.RouteListActivity;
import com.edusunsoft.erp.orataro.activities.SchoolGroupActivity;
import com.edusunsoft.erp.orataro.adapter.MenuAdapter;
import com.edusunsoft.erp.orataro.fragments.AboutUsFragment;
import com.edusunsoft.erp.orataro.fragments.AttendanceFragment;
import com.edusunsoft.erp.orataro.fragments.CalendarFragment;
import com.edusunsoft.erp.orataro.fragments.CircularFragment;
import com.edusunsoft.erp.orataro.fragments.ClassWorkListFragment;
import com.edusunsoft.erp.orataro.fragments.EventFragment;
import com.edusunsoft.erp.orataro.fragments.ExamFragment;
import com.edusunsoft.erp.orataro.fragments.ExamTimeTableFragment;
import com.edusunsoft.erp.orataro.fragments.FQAFragment;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.fragments.FeeReciptFragment;
import com.edusunsoft.erp.orataro.fragments.FeesFragment;
import com.edusunsoft.erp.orataro.fragments.HappygramFragment;
import com.edusunsoft.erp.orataro.fragments.HolidaysFragment;
import com.edusunsoft.erp.orataro.fragments.HomeWorkListFragment;
import com.edusunsoft.erp.orataro.fragments.ListselectionFragment;
import com.edusunsoft.erp.orataro.fragments.MyPollFragment;
import com.edusunsoft.erp.orataro.fragments.NotesFragment;
import com.edusunsoft.erp.orataro.fragments.PollTeacherFragment;
import com.edusunsoft.erp.orataro.fragments.ProfileMenuFragment;
import com.edusunsoft.erp.orataro.fragments.ReminderFragment;
import com.edusunsoft.erp.orataro.fragments.SettingFragment;
import com.edusunsoft.erp.orataro.fragments.StudentTeacherListFragment;
import com.edusunsoft.erp.orataro.fragments.SwitchStudentFragment;
import com.edusunsoft.erp.orataro.fragments.TimeTableFragment;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class HomeWorkFragmentActivity extends FragmentActivity implements ResponseWebServices, OnItemClickListener, OnClickListener {

    protected static final float MIN_DISTANCE = 1;
    private ListView menuList;
    private MenuAdapter adpter;
    public static ImageView btn_home, img_wallarrow;
    public static ImageView btn_menu;
    public static TextView txt_header;
    private Context mContext = HomeWorkFragmentActivity.this;
    private String[] actionlist;
    private int[] iconlist;
    private String[] actionlistTeacher;
    private int[] iconlistTeacher = {R.drawable.profile,
            R.drawable.circular, R.drawable.wall,
            R.drawable.homework,
            R.drawable.classwork, R.drawable.attendance,
            R.drawable.ptcommunication,
            R.drawable.examtiming,
            R.drawable.timetable, R.drawable.notes,
            R.drawable.holiday,
            R.drawable.calendar,
            R.drawable.poll, R.drawable.notification,
//            R.drawable.todo,
            R.drawable.aboutorataro, R.drawable.settings, R.drawable.faq, R.drawable.switchaccount};

    private Fragment fragment;
    private String position = "";
    private LinearLayout ll_menu;
    private DisplayMetrics metrics;
    private int panelWidth;
    private FrameLayout.LayoutParams slidingPanelParameters;
    private LinearLayout ll_container;
    private boolean isExpanded = false;
    boolean reload = false;
    private String isFrom = "NAN";
    public static LinearLayout mainlayout, ll_hideevent, headerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home_work);
        mContext = HomeWorkFragmentActivity.this;

        //        if (new UserSharedPrefrence(mContext).getIsERP()) {

        iconlist = new int[]{R.drawable.profile, R.drawable.circular, R.drawable.wall, R.drawable.homework,

                R.drawable.classwork, R.drawable.attendance,
                R.drawable.fees,
                R.drawable.receipt,
                R.drawable.examresult,
                R.drawable.ptcommunication,
                R.drawable.examtiming, R.drawable.timetable,
                R.drawable.notes,
                R.drawable.holiday,
                R.drawable.calendar, R.drawable.poll,
                R.drawable.notification,
//                R.drawable.todo,

                R.drawable.aboutorataro, R.drawable.settings,
                R.drawable.faq, R.drawable.switchaccount};

        actionlist = new String[]{getResources().getString(R.string.Profile), getResources().getString(R.string.Circular)
                , getResources().getString(R.string.Wall),
                getResources().getString(R.string.Homework),
                getResources().getString(R.string.Classwork),
                getResources().getString(R.string.Attendance),
                getResources().getString(R.string.Fees),
                getResources().getString(R.string.Receipt),
                getResources().getString(R.string.ExamResult),
                getResources().getString(R.string.PTCommunication),
                getResources().getString(R.string.ExamTiming), getResources().getString(R.string.TimeTable),
                getResources().getString(R.string.Notes), getResources().getString(R.string.Holiday),
                getResources().getString(R.string.Calendar), getResources().getString(R.string.Poll),
                getResources().getString(R.string.Notification),
//                getResources().getString(R.string.Todo),
                getResources().getString(R.string.Aboutorataro),
                getResources().getString(R.string.notificationsettings),
                getResources().getString(R.string.faq),
                getResources().getString(R.string.SwitchAccount)};


        actionlistTeacher = new String[]{getResources().getString(R.string.Profile), getResources().getString(R.string.Circular)
                , getResources().getString(R.string.Wall),
                getResources().getString(R.string.Homework),
                getResources().getString(R.string.Classwork),
                getResources().getString(R.string.Attendance),
                getResources().getString(R.string.PTCommunication),
                getResources().getString(R.string.ExamTiming), getResources().getString(R.string.TimeTable),
                getResources().getString(R.string.Notes), getResources().getString(R.string.Holiday),
                getResources().getString(R.string.Calendar), getResources().getString(R.string.Poll),
                getResources().getString(R.string.Notification),
//                getResources().getString(R.string.Todo),
                getResources().getString(R.string.Aboutorataro),
                getResources().getString(R.string.notificationsettings),
                getResources().getString(R.string.faq),
                getResources().getString(R.string.SwitchAccount)};


        btn_home = (ImageView) findViewById(R.id.img_home);
        btn_menu = (ImageView) findViewById(R.id.img_menu);
        // need to revert for thr back issue
        btn_menu.setVisibility(View.GONE);
        /*END*/
        txt_header = (TextView) findViewById(R.id.header_text);
        ll_menu = (LinearLayout) findViewById(R.id.ll_menu);
        img_wallarrow = (ImageView) findViewById(R.id.img_wallarrow);
        mainlayout = (LinearLayout) findViewById(R.id.ll_container);
        ll_hideevent = (LinearLayout) findViewById(R.id.ll_hideevent);
        mainlayout.setOnTouchListener(onTouchListener);

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);

        initializeUI();

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        panelWidth = (metrics.widthPixels);
        slidingPanelParameters = (FrameLayout.LayoutParams) ll_menu.getLayoutParams();
        slidingPanelParameters.width = metrics.widthPixels;
        ll_menu.setGravity(Gravity.RIGHT);
        slidingPanelParameters.width = (getWindowManager().getDefaultDisplay().getWidth());
        ll_menu.setLayoutParams(slidingPanelParameters);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((getWindowManager().getDefaultDisplay().getWidth()),
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);

        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        ll_container.setLayoutParams(layoutParams);


        /*Commented By Krishna : 08-05-2019 Update Firebase Token For Notification if Single User*/

        try {

            changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

        } catch (Exception e) {

            e.printStackTrace();

        }


        /*END*/

        if (getIntent() != null) {

            position = getIntent().getStringExtra("position");
            reload = getIntent().getBooleanExtra("reload", false);
            isFrom = getIntent().getStringExtra("isFrom");

            if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.FROMLOGIN) && position.equalsIgnoreCase(getResources().getString(R.string.SwitchAccount))) {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new SwitchStudentFragment());
                ft.commit();

            }

        }

        menuList.setOnItemClickListener(this);
        btn_home.setOnClickListener(this);

        ll_hideevent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }

        });

        txt_header.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("getCLickedtextview", "true");
                //// need to revert for back issue
                FacebookWallFragment2.showMenuHeader();
            }

        });

        btn_menu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, DynamicWallListActivity.class);
                mContext.startActivity(i);

//                if (!new UserSharedPrefrence(mContext).getLOGIN_DISPLAYNAME().equalsIgnoreCase("NAN")) {
//                    openCloseMenu();
//                }

            }

        });

        changeFragment(position);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public void openCloseMenu() {
        if (isMenuOPen()) {
            closeMenu();
        } else {
            ll_menu.setVisibility(View.VISIBLE);
            openMenu();
        }
    }


    public void changeGCMID(String UserId) {

        Log.d("getToken", PreferenceData.getToken());

        if (PreferenceData.getToken().equalsIgnoreCase(null) || PreferenceData.getToken().equalsIgnoreCase("null")

                || PreferenceData.getToken().equalsIgnoreCase("") || PreferenceData.getToken().isEmpty()) {

            try {

                Utility.RefreshFirebaseToken(mContext);

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        Log.d("requestlogin", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void closeMenu() {

        ll_hideevent.setVisibility(View.GONE);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                getWindow().getWindowManager().getDefaultDisplay().getWidth() * 0,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.RIGHT;
        ll_menu.setLayoutParams(layoutParams);
        if (Utility.isTeacher(mContext)) {
            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlistTeacher, actionlistTeacher,
                    false, true);
        } else {

            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlist, actionlist,
                    false, true);
        }

        menuList.setAdapter(adpter);
        adpter.notifyDataSetChanged();

    }

    public void openMenu() {

        ll_hideevent.setVisibility(View.VISIBLE);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                (int) (getWindow().getWindowManager().getDefaultDisplay().getWidth() * 0.53),
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);

        layoutParams.gravity = Gravity.RIGHT;
        ll_menu.setLayoutParams(layoutParams);
        if (Utility.isTeacher(mContext)) {
            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlistTeacher, actionlistTeacher,
                    true, true);
        } else {
            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlist, actionlist,
                    true, true);
        }
        menuList.setAdapter(adpter);
        adpter.notifyDataSetChanged();

    }

    public boolean isMenuOPen() {
        if (ll_menu.getWidth() > 200) {
            return true;
        }
        return false;
    }

    private void initializeUI() {

        // menu UI
        menuList = (ListView) findViewById(R.id.menu_list);
        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();
        }
        if (Utility.isTeacher(mContext)) {

            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlistTeacher, actionlistTeacher,
                    false, true);

        } else {

            Log.d("actionlist", actionlist.toString());
            adpter = new MenuAdapter(HomeWorkFragmentActivity.this, iconlist, actionlist,
                    false, true);
        }

        menuList.setAdapter(adpter);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        closeMenu();
        String i = "";

        if (Utility.isTeacher(mContext)) {

            i = actionlistTeacher[position].toString();

        } else {

            i = actionlist[position].toString();

        }

        changeFragment(i);

    }


    public void changeFragment(String i) {

        if (i.equalsIgnoreCase(getResources().getString(R.string.Profile))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new ProfileMenuFragment());
            ft.addToBackStack(null);
            ft.commit();

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.PocketMoney))) {
            Intent mintent = new Intent(mContext, PocketMoneyActivity.class);
            startActivity(mintent);
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Circular))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, CircularFragment.newInstance("", "", "", ""));
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, CircularFragment.newInstance("", "", "", ""));
                ft.commit();
            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.LMS))) {
            Intent mintent;
            PackageManager manager = getPackageManager();
            try {
                mintent = manager.getLaunchIntentForPackage("com.oratarolms");
                if (i == null)
                    throw new PackageManager.NameNotFoundException();
                if (mintent != null){
                    mintent.addCategory(Intent.CATEGORY_LAUNCHER);
                    startActivity(mintent);
                }

            } catch (PackageManager.NameNotFoundException e) {
                //if not found in device then will come here
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.oratarolms"));
                startActivity(intent);
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Events))) {

            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.EVENT).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, EventFragment.newInstance("", "", "", ""));
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, EventFragment.newInstance("", "", "", ""));
                ft.commit();
            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.MyHappygram))) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsView()) {

                    Intent intent = new Intent(mContext, ListSelectionActivity.class);
                    intent.putExtra("isFrom", ServiceResource.HAPPYGRAM_FLAG);
                    startActivity(intent);
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {
                Intent intent = new Intent(mContext, HappygramFragment.class);
                startActivity(intent);
            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.leave))) {
            if (Utility.isTeacher(mContext)) {
                Intent intent = new Intent(mContext, ListSelectionActivity.class);
                intent.putExtra("isFrom", ServiceResource.LEAVE_FLAG);
                intent.putExtra("iswithoutsubject", true);
                startActivity(intent);
            } else {
                Intent intent = new Intent(mContext, ListLeaveActivity.class);
                startActivity(intent);
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.SchoolGroups))) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {

                    Intent intent = new Intent(mContext, SchoolGroupActivity.class);
                    intent.putExtra("isFrom", ServiceResource.SCHOOLGROUP_FLAG);
                    startActivity(intent);

                } else {

                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                }

            } else {

                Intent intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                startActivity(intent);

            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Project))) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsView()) {
                    Intent intent = new Intent(mContext, SchoolGroupActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROJECT_FLAG);
                    startActivity(intent);
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {

                Intent intent = new Intent(mContext, ListWallActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                startActivity(intent);

            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Blogs))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.BLOG).getIsView()) {
                    Intent intent = new Intent(mContext, PageListActivity.class);
                    intent.putExtra("isFrom", ServiceResource.BLOGLIST);
                    startActivity(intent);
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                Intent intent = new Intent(mContext, PageListActivity.class);
                intent.putExtra("isFrom", ServiceResource.BLOGLIST);
                startActivity(intent);
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.InstitutePages))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.PAGE).getIsView()) {
                    Intent intent = new Intent(mContext, PageListActivity.class);
                    startActivity(intent);
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                Intent intent = new Intent(mContext, PageListActivity.class);
                startActivity(intent);
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.ExamTiming))) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new ExamTimeTableFragment());
//            ft.replace(R.id.homeworkActivity_containair, new ExamTimingFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Wall))) {
            ServiceResource.FOR_WALL_SELECTION = i;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getWallID(), ServiceResource.Wall));
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Homework))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.HOMEWORKSETTING).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new HomeWorkListFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new HomeWorkListFragment());
                ft.commit();
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Classwork))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new ClassWorkListFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new ClassWorkListFragment());
                ft.commit();
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Attendance))) {
            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.ATTENDANCE).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new AttendanceFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, CalendarFragment.newInstance("isAttandance"));
                ft.commit();
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Fees))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, FeesFragment.newInstance("", ""));
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Receipt))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, FeeReciptFragment.newInstance("", ""));//new ReciptListFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.ExamResult))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new ExamFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.transpotation))) {

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.PTCommunication))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.PTCOMMUNICATOIN).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, ListselectionFragment.newInstance(false, false, false, false, "", ServiceResource.PTCOMMUNICATION_FLAG, 0, false, false));
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, StudentTeacherListFragment.newInstance(null, ""));
                ft.commit();
            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.ExamTiming))) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new ExamTimeTableFragment());
//            ft.replace(R.id.homeworkActivity_containair, new ExamTimingFragment());
            ft.commit();

//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.homeworkActivity_containair, ExamTimingFragment.newInstance(ServiceResource.SCHOOLTIMEING_STRING, new UserSharedPrefrence(mContext).getLoginModel().getSchoolTiming()));
////            ft.replace(R.id.homeworkActivity_containair, PageDetailFragment.newInstance(ServiceResource.SCHOOLTIMEING_STRING, new UserSharedPrefrence(mContext).getLoginModel().getSchoolTiming()));
//            ft.commit();

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.TimeTable))) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new TimeTableFragment());
            ft.commit();

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Notes))) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.NOTE).getIsView()) {

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new NotesFragment());
                    ft.commit();

                } else {

                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                }

            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new NotesFragment());
                ft.commit();

            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Holiday))) {

            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.HOLIDAY).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new HolidaysFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new HolidaysFragment());
                ft.commit();

            }

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Calendar))) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new CalendarFragment());
            ft.commit();

        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Poll))) {

            if (Utility.isTeacher(mContext)) {

                if (Utility.ReadWriteSetting(ServiceResource.POLL).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager()
                            .beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new PollTeacherFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            } else {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, MyPollFragment.newInstance(true));
                ft.commit();

            }

        }
//        else if (i.equalsIgnoreCase(getResources().getString(R.string.Notification))) {
//
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.homeworkActivity_containair, new NotificationListFragment());
//            ft.commit();
//
//        }
        else if (i.equalsIgnoreCase(getResources().getString(R.string.Todo))) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.REMINDERS).getIsView()) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.homeworkActivity_containair, new ReminderFragment());
                    ft.commit();
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.homeworkActivity_containair, new ReminderFragment());
                ft.commit();
            }
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Aboutorataro))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new AboutUsFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.notificationsettings))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new SettingFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.faq))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new FQAFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.SwitchAccount))) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.homeworkActivity_containair, new SwitchStudentFragment());
            ft.commit();
        } else if (i.equalsIgnoreCase(getResources().getString(R.string.Transportation))) {
            Intent intent = new Intent(mContext, RouteListActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.FROMLOGIN) && position.equalsIgnoreCase(getResources().getString(R.string.SwitchAccount))) {
                btn_home.setClickable(false);
            } else {
                Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }

        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isMenuOPen()) {
                openCloseMenu();
            } else {

                if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.FROMLOGIN) && position.equalsIgnoreCase(getResources().getString(R.string.SwitchAccount))) {

                } else {

                    Intent i = new Intent(HomeWorkFragmentActivity.this, DashBoardActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);

                }

            }

            return true;

        }

        return super.onKeyDown(keyCode, event);

    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        try {
            // Commented By Krishna : 05-08-2020 - display full name instead of Firstname
            txt_header.setText(title + " \n" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
            txt_header.setPadding(40, 0, 40, 0);
            Log.d("getuserloginmodel", new UserSharedPrefrence(mContext).getLoginModel().getFullName());
//            String[] names = new UserSharedPrefrence(mContext).getLoginModel().getFullName().split(" ");
//            if (names != null && names.length >= 0) {
//                txt_header.setText(title + " (" + names[0] + ")");
//                txt_header.setPadding(40, 0, 40, 0);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    OnTouchListener onTouchListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            float x1 = 0;
            float x2;
            v.performClick();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x1 = event.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    x2 = event.getX();
                    float deltaX = x2 - x1;
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        if (x2 > x1) {
                            closeMenu();
                        } else {
                            ll_menu.setVisibility(View.VISIBLE);
                            openMenu();
                        }
                    }
                    break;
            }
            return v.onTouchEvent(event);
        }
    };

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("Response", "GCM TOKEN UPDATED SUCCESSFULLY");

        }

    }

}
