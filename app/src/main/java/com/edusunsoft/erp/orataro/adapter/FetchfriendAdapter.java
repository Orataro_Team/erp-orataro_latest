package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.SelectAllFromAdapterInterface;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class FetchfriendAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private ImageView profilePic;
    private TextView txtName;
    private ArrayList<PersonModel> personList, copyList;
    private CheckBox chb_friend;
    private SelectAllFromAdapterInterface selectAllFromAdapterInterface;

    public FetchfriendAdapter(Context context, ArrayList<PersonModel> personList,
                              SelectAllFromAdapterInterface pSelectAllFromAdapterInterface) {

        this.mContext = context;
        this.personList = personList;
        copyList = new ArrayList<PersonModel>();
        copyList.addAll(personList);
        selectAllFromAdapterInterface = pSelectAllFromAdapterInterface;
    }

    @Override
    public int getCount() {
        return personList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.fetchfriendlistraw, parent, false);
        profilePic = (ImageView) convertView.findViewById(R.id.iv_std_icon);
        txtName = (TextView) convertView.findViewById(R.id.iv_std_name);
        chb_friend = (CheckBox) convertView.findViewById(R.id.chb_friend);

        if (Utility.isNull(personList.get(position).getPersonName())) {
            txtName.setText(personList.get(position).getPersonName());
        }

        if (personList.get(position).isSelected()) {
            chb_friend.setChecked(true);
        }

        chb_friend.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                personList.get(position).setSelected(isChecked);
                selectAllFromAdapterInterface.selectedFriends(getSelectedFriendsCount());
            }
        });

        convertView.setBackgroundColor(colors[position % colors.length]);

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(personList.get(position).getProfileImg())
                    .apply(options)
                    .into(profilePic);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        personList.clear();
        if (charText.length() == 0) {
            personList.addAll(copyList);
        } else {
            for (PersonModel vo : copyList) {
                if (vo.getPersonName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    personList.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public int getSelectedFriendsCount() {
        int selected = 0;
        for (int i = 0; i < personList.size(); i++) {
            if (personList.get(i).isSelected()) {
                selected++;
            }
        }
        return selected;
    }

}
