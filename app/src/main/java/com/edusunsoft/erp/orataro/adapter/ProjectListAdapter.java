package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.CreateProjectActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.ProjectListDataDao;
import com.edusunsoft.erp.orataro.database.ProjectListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProjectListAdapter extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private List<ProjectListModel> projectList = new ArrayList<>();
    private ArrayList<ProjectListModel> copyList = new ArrayList<>();
    private TextView txtProjectTitle, txtProjectDetail, txtProjectUpdate;
    private LinearLayout ll_profilePic;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"), Color.parseColor("#F2F2F2")};
    private TextView img_grp;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private QuickAction quickActionForEditOrDelete;
    private ActionItem actionEdit, actionDelete;
    private RefreshListner listner;

    ProjectListDataDao projectListDataDao;
    public String DELETE_ID = "";

    public ProjectListAdapter(Context mContext, List<ProjectListModel> projectList, RefreshListner listner) {
        this.mContext = mContext;
        this.projectList = projectList;
        copyList = new ArrayList<ProjectListModel>();
        copyList.addAll(projectList);
        this.listner = listner;
    }

    @Override
    public int getCount() {
        return projectList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.project_list_raw, parent, false);
        convertView.setBackgroundColor(colors[position % colors.length]);
        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);
        txtProjectTitle = (TextView) convertView.findViewById(R.id.txt_prjct_title);
        txtProjectDetail = (TextView) convertView.findViewById(R.id.txt_prjct_description);
        txtProjectUpdate = (TextView) convertView.findViewById(R.id.txt_prjct_bfrtymuploaded);
        ll_profilePic = (LinearLayout) convertView.findViewById(R.id.ll_profilePic);
        img_grp = (TextView) convertView.findViewById(R.id.img_grp);

        if (Utility.isNull(projectList.get(position).getProjectTitle())) {
            txtProjectTitle.setText(projectList.get(position).getProjectTitle());
        }
        if (Utility.isNull(projectList.get(position).getProjectTitle())) {
            txtProjectDetail.setText(projectList.get(position).getProjectTitle());
        }
        if (Utility.isNull(projectList.get(position).getUserName())) {
            txtProjectUpdate.setText(projectList.get(position).getUserName());
        }
        if (ServiceResource.USER_TEACHER_INT != new UserSharedPrefrence(mContext).getLoginModel().getUserType()) {
            ll_editdelete.setVisibility(View.GONE);
        } else {
            if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsDelete() ||
                    Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsEdit()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.GONE);
            }
        }

        actionEdit = new ActionItem(ID_EDIT, mContext.getResources().getString(R.string.Edit), mContext
                .getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, mContext.getResources().getString(R.string.Delete), mContext
                .getResources().getDrawable(R.drawable.delete));
        quickActionForEditOrDelete = new QuickAction(mContext,
                QuickAction.VERTICAL);

        quickActionForEditOrDelete.addActionItem(actionEdit);
        quickActionForEditOrDelete.addActionItem(actionDelete);

        quickActionForEditOrDelete
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int _pos, int actionId) {
                        ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                        if (actionId == ID_EDIT) {
                            int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                        } else if (actionId == ID_DELETE) {
                            final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                            DELETE_ID = projectList.get(deletepos).getProjectID();
                            deleteProject(projectList.get(deletepos).getProjectID());
                        }
                    }

                });

        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utility.deleteDialog(mContext, " Project", "", new Popup() {

                    @Override
                    public void deleteYes() {

                        deleteProject(projectList.get(position).getProjectID());

                    }

                    @Override
                    public void deleteNo() {

                    }

                });

            }

        });

        try{
            if (!projectList.get(position).getProfilePicture().isEmpty() || !projectList.get(position).getProfilePicture().equalsIgnoreCase("null")
                    || !projectList.get(position).getProfilePicture().equalsIgnoreCase(null)) {
                String[] imageArray = projectList.get(position).getProfilePicture().split(",");
                if (imageArray != null && imageArray.length > 0) {
                    for (int i = 0; i < imageArray.length; i++) {
                        View v = layoutInfalater.inflate(R.layout.projectimageview, null);
                        final ImageView imgProfile = (ImageView) v.findViewById(R.id.imgprofile);

                        String url = ServiceResource.BASE_IMG_URL1 + imageArray[i];

                        Log.d("GetBaseURl", url);

                        if (url != null && !url.equalsIgnoreCase("")) {
                            url = url.replace(" ", "");
                        }

                        try {

                            RequestOptions options = new RequestOptions()
                                    .centerCrop()
                                    .placeholder(R.drawable.photo)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(Priority.HIGH)
                                    .dontAnimate()
                                    .dontTransform();

                            Glide.with(mContext)
                                    .load(url)
                                    .apply(options)
                                    .into(imgProfile);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ll_profilePic.addView(v);

                    }

                }

            }


        }catch (Exception e){
            e.printStackTrace();
        }


        if (projectList.get(position).getProjectdetail() != null) {

            if (projectList.get(position).getProjectdetail().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = projectList.get(position).getProjectdetail()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {
                        Intent i = new Intent(mContext, CreateProjectActivity.class);
                        i.putExtra("isEdit", true);
                        i.putExtra("model", projectList.get(position));
                        mContext.startActivity(i);
                    }
                };

                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                txtProjectDetail.setMovementMethod(LinkMovementMethod
                        .getInstance());
                txtProjectDetail.setText(text, BufferType.SPANNABLE);
            } else {
                txtProjectDetail.setText(projectList.get(position)
                        .getProjectdetail());
            }

        }

        img_grp.setVisibility(View.VISIBLE);

        img_grp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ServiceResource.FROM_SELECTED_WALL = "FromProjectList";
                ServiceResource.SELECTED_WALL_TITLE = projectList.get(position).getProjectTitle();
                ServiceResource.SELECTED_DYNAMIC_WALL_ID = projectList.get(position).getWallID();
                new UserSharedPrefrence(mContext).setCURRENTWALLID(projectList.get(position).getWallID());
                Intent i = new Intent(mContext, WallActivity.class);
                i.putExtra("isFrom", ServiceResource.PROJECTWALL);
                i.putExtra("title", projectList.get(position).getProjectTitle());
                ServiceResource.WALLTITLE = projectList.get(position).getProjectTitle();
                mContext.startActivity(i);

            }

        });

        txtProjectDetail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, CreateProjectActivity.class);
                i.putExtra("isEdit", true);
                i.putExtra("model", projectList.get(position));
                mContext.startActivity(i);
            }
        });

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, CreateProjectActivity.class);
                i.putExtra("isEdit", true);
                i.putExtra("model", projectList.get(position));
                mContext.startActivity(i);
            }
        });
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        projectList.clear();
        if (charText.length() == 0) {
            projectList.addAll(copyList);
        } else {

            for (ProjectListModel vo : copyList) {
                if (vo.getProjectName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    projectList.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public void deleteProject(String ProjectId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PROJECT_PROJECTID, ProjectId));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.DELETEPROJECTS_METHODNAME, ServiceResource.PROJECT_URL);
    }

    @Override
    public void response(String result, String methodName) {
        projectListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).projectListDataDao();
        projectListDataDao.deleteProjectByProjectID(DELETE_ID);
        listner.refresh(methodName);
    }
}
