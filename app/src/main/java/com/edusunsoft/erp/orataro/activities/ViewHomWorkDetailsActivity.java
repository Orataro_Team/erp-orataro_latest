package com.edusunsoft.erp.orataro.activities;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.ClassWorkListFragment;
import com.edusunsoft.erp.orataro.fragments.HomeWorkListFragment;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;

public class ViewHomWorkDetailsActivity extends AppCompatActivity implements OnClickListener, ResponseWebServices {
    Context mContext;
    ImageView img_back, img_profile;
    TextView txt_sub_name, txt_hw_details, txt_techerName;
    Intent intent;
    String sub_name, sub_details, teacher_name, reference_link;
    String teacher_img;
    TextView txt_title, txttime, txtlastdate;
    LinearLayout showHideLayout;
    boolean flag;
    String startTime, endTime, lastdate, title, enddate, READSTATUS = "";
    private TextView txt_hw_title;
    private TextView txt_enddate;
    LinearLayout timelayout, datelayout, ll_todo, ll_header;
    boolean isTodo;
    CheckBox chk_finish;
    String isFrom = "from";
    private TextView txtType;
    private TextView txt_status, txtenddate;
    private TextView txtstartdate;
    TodoListModel model;
    String id;
    boolean isApprove;
    private AdjustableImageView iv_cr_details;
    private RelativeLayout rl_view;
    private String imgUrl = "";
    private TextView txtdivstd;
    private String DivisionNamestr;
    private String GradeNamestr;
    boolean isFile = false;
    private TextView txt_cw_ref_link;

    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework_details);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ViewHomWorkDetailsActivity.this;


        if (getIntent() != null) {

//            imgUrl = "";
            id = getIntent().getStringExtra("id");
            isApprove = getIntent().getBooleanExtra("isApprove", false);
            title = getIntent().getStringExtra("Title");
            imgUrl = getIntent().getStringExtra("imgUrl");
            GradeNamestr = getIntent().getStringExtra("gradeName");
            DivisionNamestr = getIntent().getStringExtra("divisionName");
            sub_name = getIntent().getStringExtra("Subname");
            sub_details = getIntent().getStringExtra("Sub_details");
            reference_link = getIntent().getStringExtra("referencelink");
            teacher_name = getIntent().getStringExtra("Teacher_name");
            teacher_img = getIntent().getStringExtra("Teacher_img");
            startTime = getIntent().getStringExtra("starttime");
            endTime = getIntent().getStringExtra("endtime");
            lastdate = getIntent().getStringExtra("lastdate");
            isFrom = getIntent().getStringExtra("isFrom");
            flag = getIntent().getBooleanExtra("flag", false);
            READSTATUS = getIntent().getStringExtra("ReadStatus");
            Log.d("readstatus", READSTATUS);

        }

        if (isFrom != null) {

            if (isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG)) {

                isTodo = true;
                model = (TodoListModel) getIntent().getSerializableExtra("model");

            }

        }

        ll_header = (LinearLayout) findViewById(R.id.ll_header);
        ll_todo = (LinearLayout) findViewById(R.id.ll_todo);
        txtType = (TextView) findViewById(R.id.txtType);
        txt_status = (TextView) findViewById(R.id.txt_status);
        txtstartdate = (TextView) findViewById(R.id.txtstartdate);
        txtenddate = (TextView) findViewById(R.id.txtenddate);
        txt_sub_name = (TextView) findViewById(R.id.txt_sub_name);
        txt_hw_details = (TextView) findViewById(R.id.txt_hw_details);
        txt_cw_ref_link = (TextView) findViewById(R.id.txt_cw_ref_link);
        txt_techerName = (TextView) findViewById(R.id.txt_techerName);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_hw_title = (TextView) findViewById(R.id.txt_hw_title);
        txttime = (TextView) findViewById(R.id.txttime);
        txtlastdate = (TextView) findViewById(R.id.txtlastdate);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        showHideLayout = (LinearLayout) findViewById(R.id.layoutSub);
        txt_enddate = (TextView) findViewById(R.id.txt_enddate);
        timelayout = (LinearLayout) findViewById(R.id.timelayout);
        datelayout = (LinearLayout) findViewById(R.id.datelayout);
        txtdivstd = (TextView) findViewById(R.id.txtdivstd);
        iv_cr_details = (AdjustableImageView) findViewById(R.id.iv_cr_details);
        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        LinearLayout ll_finish = (LinearLayout) findViewById(R.id.ll_finish);
        chk_finish = (CheckBox) findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) findViewById(R.id.txtisfinish);

        if (isTodo) {

            ll_todo.setVisibility(View.VISIBLE);
            ll_header.setVisibility(View.GONE);
            showHideLayout.setVisibility(View.GONE);
            txt_sub_name.setText(getResources().getString(R.string.Todo));
            txt_title.setText(model.getTitle().substring(0, 1).toUpperCase() + model.getTitle().substring(1));
            txt_hw_details.setText(model.getDetails());
            txtType.setText(model.getTypeTerm());
            txt_status.setText(model.getStatus());
            txtstartdate.setText(model.getCreateOn());
            txtenddate.setText(model.getEndDate());

        } else {

            txtdivstd.setVisibility(View.VISIBLE);
            txtdivstd.setTextSize(14f);
            txtdivstd.setText(GradeNamestr + " " + DivisionNamestr);

            if (Utility.isTeacher(mContext)) {

                ll_finish.setVisibility(View.GONE);

            } else {

                ll_finish.setVisibility(View.VISIBLE);

            }

            if (isApprove) {

                chk_finish.setChecked(true);

            } else {

                chk_finish.setChecked(false);

            }

            ll_todo.setVisibility(View.GONE);

            if (flag) {

                showHideLayout.setVisibility(View.VISIBLE);
                txt_title.setVisibility(View.VISIBLE);
                ll_finish.setVisibility(View.GONE);
                txtisfinish.setText(getResources().getString(R.string.Checked));
                Log.d("flagfalse", "false");

            } else {

                txtisfinish.setText(READSTATUS);
                showHideLayout.setVisibility(View.VISIBLE);
                timelayout.setVisibility(View.GONE);
                Log.d("flagfalse", "true");

            }

        }

        if (!isTodo) {

            txt_sub_name.setText(sub_name);
            txt_hw_details.setText(sub_details);
            if (reference_link.equalsIgnoreCase("") || reference_link.equalsIgnoreCase(null) || reference_link.equalsIgnoreCase("null")) {
                txt_cw_ref_link.setVisibility(View.GONE);
            } else {
                txt_cw_ref_link.setText(reference_link);
            }

            txt_techerName.setText(teacher_name);
            if (!title.equalsIgnoreCase("") && title.length() > 2) {
                txt_title.setText(title.substring(0, 1).toUpperCase() + title.substring(1));
            } else {
                txt_title.setText(title);
            }

            if (isFrom.equalsIgnoreCase("Classwork")) {
                timelayout.setVisibility(View.VISIBLE);
                txtlastdate.setText(Utility.dateFormate(lastdate, "dd/MM/yyyy", "dd MMM yyyy"));
            } else {
                txtlastdate.setText(Utility.dateFormate(lastdate, "dd/MM/yyyy", "MM/dd/yyyy"));
            }

            try {
                //txttime.setText(Utility.dateFormate(startTime, "HH : mm : a", "hh:mm a") + "  TO  " + Utility.dateFormate(endTime, "HH : mm : a", "hh:mm a"));
                txttime.setText(String.format("%s  TO  %s", startTime, endTime));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();
                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1 + teacher_img)
                        .apply(options)
                        .into(img_profile);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (imgUrl != null && !imgUrl.contains("null")) {

                if (imgUrl.contains(".TEXT") || imgUrl.contains(".txt")) {
                    isFile = true;
                    iv_cr_details.setImageResource(R.drawable.txt);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                    iv_cr_details.setLayoutParams(params);
                    iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } else if (imgUrl.contains(".PDF") || imgUrl.contains(".pdf")) {
                    isFile = true;
                    iv_cr_details.setImageResource(R.drawable.pdf);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                    iv_cr_details.setLayoutParams(params);
                    iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } else if (imgUrl.contains(".doc") || imgUrl.contains(".doc")) {
                    isFile = true;
                    iv_cr_details.setImageResource(R.drawable.doc);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                    iv_cr_details.setLayoutParams(params);
                    iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } else if (imgUrl.contains(".docs") || imgUrl.contains(".docs")) {
                    isFile = true;
                    iv_cr_details.setImageResource(R.drawable.file);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                    iv_cr_details.setLayoutParams(params);
                    iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } else if (imgUrl.contains(".xlsx") || imgUrl.contains(".xls") || imgUrl.contains(".xlsm")) {
                    isFile = true;
                    iv_cr_details.setImageResource(R.drawable.excel_file);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                    iv_cr_details.setLayoutParams(params);
                    iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } else {

                    try {

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(imgUrl)
                                .apply(options)
                                .into(iv_cr_details);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                iv_cr_details.setVisibility(View.VISIBLE);

            } else {
                iv_cr_details.setVisibility(View.GONE);
            }

        }

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                classworkListIsApprove(id, isChecked);

            }

        });

        img_back.setOnClickListener(this);
        iv_cr_details.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:

//                if (Utility.isTeacher(mContext)) {
//                    if (Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsView()) {
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.homeworkActivity_containair, new ClassWorkListFragment());
//                        ft.commit();
//                    } else {
//                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
//                    }
//
//                } else {
//
//                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.homeworkActivity_containair, new ClassWorkListFragment());
//                    ft.commit();
//
//                }
                if (isFrom.equalsIgnoreCase("Homework")) {
                    Utility.ISLOADHOMEWORK = false;
                } else if (isFrom.equalsIgnoreCase("Classwork")) {
                    Utility.ISLOADCLASSWORK = false;
                }

                finish();
                break;

            case R.id.iv_cr_details:

                if (!isFile) {
                    Intent i = new Intent(mContext, ZoomImageAcitivity.class);
                    i.putExtra("img", imgUrl);
                    i.putExtra("name", title);
                    startActivity(i);
                    overridePendingTransition(0, 0);

                } else {
                    Log.d("getImageURL", imgUrl);
                    saveDownload(imgUrl.replace("//DataFiles//", "/DataFiles/"));
                }

                break;

            default:

                break;

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isFrom.equalsIgnoreCase("Homework")) {
            Utility.ISLOADHOMEWORK = false;
        } else if (isFrom.equalsIgnoreCase("Classwork")) {
            Utility.ISLOADCLASSWORK = false;
        }
    }

    public void classworkListIsApprove(String assosiationId, boolean isApprove) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, isFrom));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (isFrom.equalsIgnoreCase("Homework")) {
            HomeWorkListFragment.adapter.UpdateData(id, chk_finish.isChecked());
        } else if (isFrom.equalsIgnoreCase("Classwork")) {
            ClassWorkListFragment.adapter.NotifyClassworkData(id, chk_finish.isChecked());
        }

    }

    private void saveDownload(String url) {

        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);


    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

}
