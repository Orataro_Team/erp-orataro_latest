package com.edusunsoft.erp.orataro.model;

import java.util.ArrayList;

public class ResultQuestionModel {

    private String question, rightanswer;
    private ArrayList<AnsModel> ansList;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRightanswer() {
        return rightanswer;
    }

    public void setRightanswer(String rightanswer) {
        this.rightanswer = rightanswer;
    }

    public ArrayList<AnsModel> getAnsList() {
        return ansList;
    }

    public void setAnsList(ArrayList<AnsModel> ansList) {
        this.ansList = ansList;
    }

}
