package com.edusunsoft.erp.orataro.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TimeTableDataDao {

    @Insert
    void insertTimeTableList(TimeTableModel timeTableModel);

    @Query("SELECT * from TimeTable WHERE ForTeacherORStudent = :forteacherorstudent")
    List<TimeTableModel> getTimeTableList(String forteacherorstudent);

    @Query("DELETE  FROM TimeTable WHERE ForTeacherORStudent = :forteacherorstudent")
    int deleteTimeTable(String forteacherorstudent);


}
