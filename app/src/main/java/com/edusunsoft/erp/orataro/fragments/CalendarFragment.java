package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.CalenderEventInterface;
import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewEventsDetailsActivity;
import com.edusunsoft.erp.orataro.adapter.CalendarAdapter;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.CalendarEventModel;
import com.edusunsoft.erp.orataro.model.CalenderEventModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * calendar fragment
 *
 * @author admin
 */
public class CalendarFragment extends Fragment implements OnClickListener, CalenderEventInterface, ResponseWebServices {

    private Context mContext;
    public GregorianCalendar month, itemmonth;
    public CalendarAdapter adapter;
    public Handler handler;
    public ArrayList<String> items;
    private View v;
    private TextView txtToday, txtYear;
    private ArrayList<String> event;
    private LinearLayout rLayout;
    private ArrayList<String> date;
    private ArrayList<String> desc;
    private LinearLayout ll_events, ll_more;
    private ScrollView scrollView1;
    private LinearLayout ll_leagent;
    private ImageView iv_previous, iv_next, iv_previous_year, iv_next_year;
    private GridView mEventGridView;
    private TextView tv_count, txt_more;
    private TextView title;
    private String isAttandance = "";
    private View view_more;

    public static TextView txt_present, txt_absent, txt_seak_leave, txt_leave;
    public static TextView txt_total_present, txt_total_absent, txt_total_leave, txt_total_seakleave, txt_total;
    public static TextView txt_present_working, txt_present_nonworking, txt_sickleave_nonworking, txt_sickleave_working, txt_leave_nonworking, txt_leave_working, txt_absent_nonworking, txt_absent_working;
    public static TextView txt_total_nonworking, txt_total_working;

    public static TableLayout tableLayout1;

    public static final CalendarFragment newInstance(String isAttandance) {
        CalendarFragment f = new CalendarFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString("isAttandance", isAttandance);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        v = inflater.inflate(R.layout.calendar_fragment, container, false);

        if (getArguments() != null) {

            isAttandance = getArguments().getString("isAttandance");

        }

        mContext = getActivity();

        try {

            if (isAttandance.equalsIgnoreCase("")) {

                if (HomeWorkFragmentActivity.txt_header != null) {
                    HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Calendar) + " (" + Utility.GetFirstName(mContext) + ")");
                    HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
                }

            } else {

                if (HomeWorkFragmentActivity.txt_header != null) {
                    HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Attendance) + " (" + Utility.GetFirstName(mContext) + ")");
                    HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*commented By Krishna : 25-05-2019 - Get Textview for Display Count of Absent,Present,Leave,SeakLeave;*/

        txt_present = (TextView) v.findViewById(R.id.txt_present);
        txt_absent = (TextView) v.findViewById(R.id.txt_absent);
        txt_leave = (TextView) v.findViewById(R.id.txt_leave);
        txt_seak_leave = (TextView) v.findViewById(R.id.txt_seak_leave);
        txt_total_present = (TextView) v.findViewById(R.id.txt_total_present);
        txt_total_absent = (TextView) v.findViewById(R.id.txt_total_absent);
        txt_total_leave = (TextView) v.findViewById(R.id.txt_total_leave);
        txt_total_seakleave = (TextView) v.findViewById(R.id.txt_total_seakleave);
        txt_total = (TextView) v.findViewById(R.id.txt_total);
        txt_present_working = (TextView) v.findViewById(R.id.txt_present_working);
        txt_present_nonworking = (TextView) v.findViewById(R.id.txt_present_nonworking);
        txt_sickleave_nonworking = (TextView) v.findViewById(R.id.txt_sickleave_nonworking);
        txt_sickleave_working = (TextView) v.findViewById(R.id.txt_sickleave_working);
        txt_leave_nonworking = (TextView) v.findViewById(R.id.txt_leave_nonworking);
        txt_leave_working = (TextView) v.findViewById(R.id.txt_leave_working);
        txt_absent_nonworking = (TextView) v.findViewById(R.id.txt_absent_nonworking);
        txt_absent_working = (TextView) v.findViewById(R.id.txt_absent_working);
        txt_total_nonworking = (TextView) v.findViewById(R.id.txt_total_nonworking);
        txt_total_working = (TextView) v.findViewById(R.id.txt_total_working);
        tableLayout1 = (TableLayout) v.findViewById(R.id.tableLayout1);

        /*END*/

        rLayout = (LinearLayout) v.findViewById(R.id.text);
        month = (GregorianCalendar) Calendar.getInstance();
        itemmonth = (GregorianCalendar) month.clone();
//        scrollView1 = (ScrollView) v.findViewById(R.id.scrollView1);
        ll_leagent = (LinearLayout) v.findViewById(R.id.ll_leagent);
        items = new ArrayList<String>();
        title = (TextView) v.findViewById(R.id.title);
        txtYear = (TextView) v.findViewById(R.id.title_year);
        txtYear.setText(android.text.format.DateFormat.format("yyyy", month));
        title.setText(android.text.format.DateFormat.format("MMMM", month));

        mEventGridView = (GridView) v.findViewById(R.id.gridview);
        iv_previous = (ImageView) v.findViewById(R.id.iv_previous);
        iv_next = (ImageView) v.findViewById(R.id.iv_next);
        iv_previous_year = (ImageView) v.findViewById(R.id.iv_previous_year);
        iv_next_year = (ImageView) v.findViewById(R.id.iv_next_year);
        ll_events = (LinearLayout) v.findViewById(R.id.ll_events);
        ll_more = (LinearLayout) v.findViewById(R.id.ll_more);
        view_more = (View) v.findViewById(R.id.view_more);
        ll_more.setVisibility(View.GONE);
        view_more.setVisibility(View.GONE);
        tv_count = (TextView) v.findViewById(R.id.tv_count);
        txt_more = (TextView) v.findViewById(R.id.txt_more);
        ll_events.setClickable(false);
//        ll_events.setOnClickListener(this);
        iv_previous.setOnClickListener(this);
        iv_next.setOnClickListener(this);
        iv_previous_year.setOnClickListener(this);
        iv_next_year.setOnClickListener(this);
//        txtToday.setOnClickListener(this);

        if (Utility.isNetworkAvailable(mContext)) {

            if (isAttandance.equalsIgnoreCase("")) {
                calenderEvent(txtYear.getText().toString(), true);
            } else {
                AttendanceListStudent(true);
            }

        } else {

            if (isAttandance.equals("")) {
                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            } else {
                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

        }

        if (isAttandance.equalsIgnoreCase("")) {

//            scrollView1.setVisibility(View.VISIBLE);
            ll_events.setVisibility(View.VISIBLE);
            ll_leagent.setVisibility(View.GONE);
            tableLayout1.setVisibility(View.GONE);

        } else {

//            scrollView1.setVisibility(View.GONE);
            ll_events.setVisibility(View.GONE);
            tableLayout1.setVisibility(View.VISIBLE);
//            ll_leagent.setVisibility(View.VISIBLE);

        }

        setAdapter();

        return v;

    }

    /**
     * set next month on grid
     */
    protected void setNextMonth() {
        if (month.get(Calendar.MONTH) == month
                .getActualMaximum(Calendar.MONTH)) {
            month.set((month.get(Calendar.YEAR) + 1),
                    month.getActualMinimum(Calendar.MONTH), 1);
        } else {
            month.set(Calendar.MONTH,
                    month.get(Calendar.MONTH) + 1);
        }

    }

    /**
     * set next year on grid
     */
    protected void setNextYear() {
        if (month.get(Calendar.MONTH) == month
                .getActualMaximum(Calendar.MONTH)) {
            month.set((month.get(Calendar.YEAR) + 1),
                    month.getActualMinimum(Calendar.MONTH), 1);
        } else {
            month.set(Calendar.MONTH,
                    month.get(Calendar.MONTH) + 12);
        }

    }

    /**
     * set previous year on grid
     */
    protected void setPreviousYear() {
        if (month.get(Calendar.MONTH) == month
                .getActualMinimum(Calendar.MONTH)) {
            month.set((month.get(Calendar.YEAR) - 1),
                    month.getActualMaximum(Calendar.MONTH), 1);
        } else {
            month.set(Calendar.MONTH,
                    month.get(Calendar.MONTH) - 12);
        }

    }

    /**
     * set previous month on grid
     */
    protected void setPreviousMonth() {
        if (month.get(Calendar.MONTH) == month
                .getActualMinimum(Calendar.MONTH)) {

            month.set((month.get(Calendar.YEAR) - 1),
                    month.getActualMaximum(Calendar.MONTH), 1);

        } else {

            month.set(Calendar.MONTH,
                    month.get(Calendar.MONTH) - 1);

        }

    }

    protected void showToast(String string) {
        Utility.toast(getActivity(), string);
    }

    /**
     * refresh grid
     */

    public void refreshCalendar() {


        TextView title = (TextView) v.findViewById(R.id.title);
        if (adapter != null) {
            adapter.refreshDays();
            adapter.notifyDataSetChanged();
        }
        handler.post(calendarUpdater); // generate some calendar items
        txtYear.setText(android.text.format.DateFormat.format("yyyy", month));
        title.setText(android.text.format.DateFormat.format("MMMM", month));
    }

    /**
     * calendr uPDATE
     */

    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
            items.clear();

            // Print dates of the current week
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String itemvalue;
            event = Utility.readCalendarEvent(getActivity());
            Log.d("=====Event====", event.toString());
            Log.d("=====Date ARRAY====", Utility.startDates.toString());

            for (int i = 0; i < Utility.startDates.size(); i++) {

                itemvalue = df.format(itemmonth.getTime());
                itemmonth.add(Calendar.DATE, 1);
                items.add(Utility.startDates.get(i).toString());

            }

            //			adapter.notifyDataSetChanged();

            if (isAttandance.equals("")) {

                adapter = new CalendarAdapter(getActivity(), month, Global.calenderdDataList, CalendarFragment.this);

            } else {

                adapter = new CalendarAdapter(getActivity(), month, Global.attendancemodels);
            }

            adapter.setItems(items);
            mEventGridView.setAdapter(adapter);
            //			handler = new Handler();
            //			handler.post(calendarUpdater);
        }
    };

    public com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

    @Override
    public void onClick(View v) {

        // TODO Auto-generated method stub

        switch (v.getId()) {

            case R.id.ll_events:

                if (Utility.isTeacher(mContext)) {

                    if (Utility.ReadWriteSetting(ServiceResource.EVENT).getIsView()) {

                        Intent intent = new Intent(mContext, ViewEventsDetailsActivity.class);
                        intent.putExtra("month", title.getText().toString());
                        intent.putExtra("year", txtYear.getText().toString());
                        startActivity(intent);

                    } else {

                        Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                    }

                } else {

                    Intent intent = new Intent(mContext, ViewEventsDetailsActivity.class);
                    intent.putExtra("month", title.getText().toString());
                    intent.putExtra("year", txtYear.getText().toString());
                    startActivity(intent);

                }


                break;

            case R.id.iv_previous:

                ServiceResource.PresentStudentCount = 0;
                setPreviousMonth();
                refreshCalendar();

                break;

            case R.id.iv_next:

                ServiceResource.PresentStudentCount = 0;
                setNextMonth();
                refreshCalendar();

                break;
            case R.id.iv_previous_year:

                setPreviousYear();
                refreshCalendar();

                break;
            case R.id.iv_next_year:

                setNextYear();
                refreshCalendar();

                break;


            default:
                break;
        }

    }


    public class EventL extends AsyncTask<String, Void, Void> implements ICancleAsynkTask {


        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        //		private ProgressDialog progressDialog;
        String result;


        protected void onPreExecute() {
            super.onPreExecute();
            // do stuff before posting data
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
            //			progressDialog = new ProgressDialog(mContext);
            //			progressDialog.setMessage(mContext.getResources().getString(R.string.pleasewait));
            //			progressDialog.setCancelable(false);
            //			progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.CALENDEREVENT_USERID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            map1.put(ServiceResource.CALENDEREVENT_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.CALENDEREVENT_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            //	map1.put(ServiceResource.CALENDERDATA_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CALENDEREVENT_BEACHID, null);

            map.put(2, map1);

            result = webcall.getJSONFromSOAPWS(ServiceResource.CALENDEREVENT_METHODNAME, map, ServiceResource.CALENDEREVENT_URL);

            JSONArray jsonObj;
            try {
                Global.calenderdEventList = new ArrayList<CalendarEventModel>();


                jsonObj = new JSONArray(result);
                //JSONArray detailArrray= jsonObj.getJSONArray("");


                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    CalendarEventModel calendarEventModel = new CalendarEventModel();


                    String date = (Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.CALENDEREVENT_STARTDATE)
                                    .replace("/Date(", "").replace(")/", "")),
                            "dd/MMM/yyyy"));

                    String[] arraydate = date.split("[/]");

                    if (arraydate != null && arraydate.length > 0) {
                        Log.v("date", arraydate[0] + "" + arraydate[1] + "" + arraydate[2]);
                        calendarEventModel.setCl_date(arraydate[0]);
                        calendarEventModel.setMonth(arraydate[1]);
                        calendarEventModel.setYear(arraydate[2]);
                    }
                    calendarEventModel.setStartDate(innerObj.getString(ServiceResource.CALENDEREVENT_STARTDATE)
                            .replace("/Date(", "").replace(")/", ""));
                    calendarEventModel.setCl_subject(innerObj.getString(ServiceResource.CALENDEREVENT_TITLE));
                    calendarEventModel
                            .setCl_detail_txt(innerObj.getString(ServiceResource.CALENDEREVENT_DETAIL));
                    calendarEventModel.setUserName(innerObj.getString(ServiceResource.CALENDEREVENT_USERNAME));
                    calendarEventModel.setCreateOn(innerObj.getString(ServiceResource.CALENDEREVENT_CREATEON));
                    calendarEventModel.setYear(innerObj.getString(ServiceResource.CALENDEREVENT_YEAR));
                    calendarEventModel.setEventID(innerObj.getString(ServiceResource.CALENDEREVENT_EVENTID));
                    calendarEventModel.setClientID(innerObj.getString(ServiceResource.CALENDEREVENT_CLIENTID));
                    calendarEventModel.setInstituteID(innerObj.getString(ServiceResource.CALENDEREVENT_INSTITUTEIDPARSING));
                    calendarEventModel.setReminderDate(innerObj.getString(ServiceResource.CALENDEREVENT_REMINDERDATEPARSING));
                    calendarEventModel.setIsActive(innerObj.getString(ServiceResource.CALENDEREVENT_ISACTIVE));
                    calendarEventModel.setEndDate(innerObj.getString(ServiceResource.CALENDEREVENT_ENDDATE));
                    calendarEventModel.setUpdateBy(innerObj.getString(ServiceResource.CALENDEREVENT_UPDATEBY));
                    calendarEventModel.setUpdateOn(innerObj.getString(ServiceResource.CALENDEREVENT_UPDATEON));
                    calendarEventModel.setCreateOn(innerObj.getString(ServiceResource.CALENDEREVENT_CREATEON));
                    calendarEventModel.setSeqNo(innerObj.getString(ServiceResource.CALENDEREVENT_SEQNO));
                    calendarEventModel.setDivisionID(innerObj.getString(ServiceResource.CALENDEREVENT_DIVISIONID));
                    calendarEventModel.setGradeID(innerObj.getString(ServiceResource.CALENDEREVENT_GRADEID));
                    calendarEventModel.setSubjectId(innerObj.getString(ServiceResource.CALENDEREVENT_SUBJECTID));
                    calendarEventModel.setVisibleMonth(true);

                    //calendarEventModel.setCl_month(innerObj.getString(ServiceResource.CALENDEREVENT_));


                    Global.calenderdEventList.add(calendarEventModel);


                }


                Collections.sort(Global.calenderdEventList, new SortedDate());
                Collections.reverse(Global.calenderdEventList);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }
            for (int i = 0; i < Global.calenderdEventList.size(); i++) {
                if (i != 0) {
                    String temp = Global.calenderdEventList.get(i - 1).getMonth();
                    if (temp.equalsIgnoreCase(Global.calenderdEventList.get(i).getMonth())) {
                        Global.calenderdEventList.get(i).setVisibleMonth(false);
                    } else {
                        Global.calenderdEventList.get(i).setVisibleMonth(true);
                    }
                } else {
                    Global.calenderdEventList.get(i).setVisibleMonth(true);
                }


            }
        }

        @Override
        public void onCancleTask() {
            // TODO Auto-generated method stub
            cancel(true);
        }

    }

    /**
     * sort Arraylist using date
     *
     * @author admin
     */
    public class SortedDate implements Comparator<CalendarEventModel> {
        @Override
        public int compare(CalendarEventModel o1, CalendarEventModel o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    }

    /**
     * check event count and if count >0 texview visible otherwish gone
     */
    @Override
    public void getMonthEventCount(int count) {
        // TODO Auto-generated method stub


        if (!isAttandance.equals("")) {

        } else {

        }


        if (!isAttandance.equals("")) {
//            scrollView1.setVisibility(View.GONE);
            ll_events.setVisibility(View.GONE);
            ll_leagent.setVisibility(View.VISIBLE);
        } else {
//            scrollView1.setVisibility(View.VISIBLE);
            ll_events.setVisibility(View.VISIBLE);
            ll_leagent.setVisibility(View.GONE);

            if (count > 0) {

                tv_count.setText("+" + count);
                txt_more.setVisibility(View.VISIBLE);
                ll_events.setVisibility(View.VISIBLE);

            } else {

                tv_count.setText("");
                txt_more.setVisibility(View.GONE);

            }
        }
    }

    /**
     * get event list from year call webservice
     *
     * @param year
     */
    public void calenderEvent(String year, boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_YEAR, year));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));


        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.GradeID, null));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            Log.d("calenderRequest", arrayList.toString());

        }

        new AsynsTaskClass(getActivity(), arrayList, true, this).execute(ServiceResource.CALENDERDATA_METHODNAME, ServiceResource.CALENDERDATA_URL);

    }


    public void AttendanceListStudent(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Utility.getUserModelData(mContext);

        }

        arrayList.add(new PropertyVo(ServiceResource.YEARSTARTDATE, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.YEARENDDATE, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));

        arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));

        Log.d("studentlist", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.ATTENDANCELISTFORSTUDENT_METHODNAME, ServiceResource.ATTENDANCE_URL);


    }


    /**
     * get Response of webservice
     */


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.CALENDERDATA_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, methodName, mContext);
            parsecalendar(result);

        } else if (ServiceResource.CALENDERDATA_METHODNAME.equalsIgnoreCase(methodName)) {

        } else if (ServiceResource.ATTENDANCELISTFORSTUDENT_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, methodName, mContext);
            parseattendcelist(result);
            Log.d("parseresult", result);

        }

    }

    private void parseattendcelist(String result) {

        JSONArray hJsonArray;
        JSONObject jObj;
        JSONArray attendaceeditarray;

        try {


            if (result.contains("\"success\":0")) {


            } else {
                //			     jObj = new JSONObject(examresult);
                Global.attendancemodels = new ArrayList<AtteandanceModel>();

                hJsonArray = new JSONArray(result);// jObj.getJSONArray(ServiceResource.TABLE);

                for (int i = 0; i < hJsonArray.length(); i++) {

                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    AtteandanceModel model = new AtteandanceModel();

                    model.setStudentAttRegMasterID(hJsonObject
                            .getString(ServiceResource.STUDENTATTENDENCEID));
                    model.setDateOfAttendence(hJsonObject
                            .getString(ServiceResource.DATEOFATTENDENCE));
                    model.setAttencenceType_Term(hJsonObject
                            .getString(ServiceResource.ATTENCENCETYPE_TERM));
                    model.setWorkingDay(hJsonObject.getBoolean(ServiceResource.ISWORKINGDAY));

                    Global.attendancemodels.add(model);

                    Log.d("attendaclelist", Global.attendancemodels.toString());

                }

                if (Global.attendancemodels != null && Global.attendancemodels.size() > 0) {

                    adapter = new CalendarAdapter(getActivity(), month, Global.attendancemodels);
                    mEventGridView.setAdapter(adapter);

                }


                handler = new Handler();
                handler.post(calendarUpdater);
                mEventGridView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {
                        // removing the previous view if added
                        if (rLayout.getChildCount() > 0) {
                            rLayout.removeAllViews();
                        }

                        desc = new ArrayList<String>();
                        date = new ArrayList<String>();
                        ((CalendarAdapter) parent.getAdapter()).setSelected(v);
                        String selectedGridDate = CalendarAdapter.dayString
                                .get(position);
                        String[] separatedTime = selectedGridDate.split("-");
                        String gridvalueString = separatedTime[2].replaceFirst("^0*",
                                "");// taking last part of date. ie; 2 from 2012-12-02.
                        int gridvalue = Integer.parseInt(gridvalueString);
                        // navigate to next or previous month on clicking offdays.
                        if ((gridvalue > 10) && (position < 8)) {
                            setPreviousMonth();
                            refreshCalendar();
                        } else if ((gridvalue < 7) && (position > 28)) {
                            setNextMonth();
                            refreshCalendar();
                        }

                        ((CalendarAdapter) parent.getAdapter()).setSelected(v);

                        for (int i = 0; i < Utility.startDates.size(); i++) {

                            if (Utility.startDates.get(i).equals(selectedGridDate)) {

                                desc.add(Utility.nameOfEvent.get(i));

                            }

                        }

                        if (desc.size() > 0) {

                            for (int i = 0; i < desc.size(); i++) {
                                TextView rowTextView = new TextView(getActivity());

                                // set some properties of rowTextView or something
                                rowTextView.setText(getResources().getString(R.string.Event) + ":" + desc.get(i));
                                rowTextView.setTextColor(Color.BLACK);

                                // add the textview to the linearlayout
                                rLayout.addView(rowTextView);

                            }

                        }

                        desc = null;

                    }

                });


            }
        } catch (JSONException e) {
            e.printStackTrace();

        }


    }

    public void parsecalendar(String result) {

        Log.d("calenderResult", result);

        JSONArray jsonObj;

        try {

            Global.calenderdDataList = new ArrayList<CalenderEventModel>();
            Global.calenderdEventList = new ArrayList<CalendarEventModel>();


            jsonObj = new JSONArray(result);
            //JSONArray detailArrray= jsonObj.getJSONArray("");
            Global.calenderdDataList = new ArrayList<CalenderEventModel>();

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                CalenderEventModel eventModel = new CalenderEventModel();
                eventModel.setActivityId(innerObj.getString(ServiceResource.CALENDERDATA_ACTIVITYID));
                eventModel.setTitle(innerObj.getString(ServiceResource.CALENDERDATA_TITLE));
                eventModel.setActivityDetails(innerObj.getString(ServiceResource.CALENDERDATA_ACTIVITYDETAIL));
                eventModel.setStart(innerObj.getString(ServiceResource.CALENDERDATA_START));
                eventModel.setEnd(innerObj.getString(ServiceResource.CALENDERDATA_END));
                eventModel.setType(innerObj.getString(ServiceResource.CALENDERDATA_TYPE));
                eventModel.setClassName(innerObj.getString(ServiceResource.CALENDERDATA_CLASSNAME));
                eventModel.setColor(innerObj.getString(ServiceResource.CALENDERDATA_COLOR));
                Global.calenderdDataList.add(eventModel);
                CalendarEventModel calendarEventModel = new CalendarEventModel();
                Log.v("long date", innerObj.getString(ServiceResource.CALENDERDATA_START) + "");
                String date1 = (Utility.getDate(Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.CALENDERDATA_START), "yyyy/MM/dd"),
                        "EEEE/dd/MMM/yyyy"));

                String[] arraydate = date1.split("[/]");

                if (arraydate != null && arraydate.length > 0) {
                    Log.v("date", arraydate[0] + "" + arraydate[1] + "" + arraydate[2]);
                    calendarEventModel.setCl_date(arraydate[1] + " " + arraydate[0].substring(0, 3));
                    calendarEventModel.setMonth(arraydate[2]);
                    calendarEventModel.setYear(arraydate[3]);
                }
                calendarEventModel.setStartDate(String.valueOf(Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.CALENDERDATA_START), "yyyy/MM/dd"))
                        .replace("/Date(", "").replace(")/", ""));

                calendarEventModel.setType(innerObj.getString(ServiceResource.CALENDERDATA_TYPE));
                calendarEventModel.setCl_subject(innerObj.getString(ServiceResource.CALENDERDATA_TITLE));
                calendarEventModel
                        .setCl_detail_txt(innerObj.getString(ServiceResource.CALENDERDATA_ACTIVITYDETAIL));
                Global.calenderdEventList.add(calendarEventModel);


                if (Global.calenderdEventList != null && Global.calenderdEventList.size() > 0) {

                    Collections.sort(Global.calenderdEventList, new SortedDate());
                    Collections.reverse(Global.calenderdEventList);

                }


                for (int j = 0; j < Global.calenderdEventList.size(); j++) {
                    if (j != 0) {
                        Log.v("Month", Global.calenderdEventList.get(j).getMonth());
                        String temp = Global.calenderdEventList.get(j - 1).getMonth();
                        if (temp.equalsIgnoreCase(Global.calenderdEventList.get(j).getMonth())) {
                            Global.calenderdEventList.get(j).setVisibleMonth(false);
                        } else {
                            Global.calenderdEventList.get(j).setVisibleMonth(true);
                        }
                    } else {
                        Global.calenderdEventList.get(j).setVisibleMonth(true);
                    }


                }
                //				adapter = new CalendarAdapter(getActivity(), month,Global.calenderdDataList,CalendarFragment.this);


                setAdapter();
                //new EventList().execute();


            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    public void setAdapter() {

        if (isAttandance.equals("")) {
            adapter = new CalendarAdapter(getActivity(), month, Global.calenderdDataList, CalendarFragment.this);
        } else {
            adapter = new CalendarAdapter(getActivity(), month, Global.attendancemodels);
        }

        mEventGridView.setAdapter(adapter);
        handler = new Handler();
        handler.post(calendarUpdater);
        mEventGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                // removing the previous view if added
                if (rLayout.getChildCount() > 0) {
                    rLayout.removeAllViews();
                }

                desc = new ArrayList<String>();
                date = new ArrayList<String>();
                ((CalendarAdapter) parent.getAdapter()).setSelected(v);
                String selectedGridDate = CalendarAdapter.dayString
                        .get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*",
                        "");// taking last part of date. ie; 2 from 2012-12-02.
                int gridvalue = Integer.parseInt(gridvalueString);
                // navigate to next or previous month on clicking offdays.
                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }

                ((CalendarAdapter) parent.getAdapter()).setSelected(v);

                for (int i = 0; i < Utility.startDates.size(); i++) {

                    if (Utility.startDates.get(i).equals(selectedGridDate)) {

                        desc.add(Utility.nameOfEvent.get(i));

                    }

                }

                if (desc.size() > 0) {

                    for (int i = 0; i < desc.size(); i++) {

                        TextView rowTextView = new TextView(getActivity());

                        // set some properties of rowTextView or something
                        rowTextView.setText(getResources().getString(R.string.Event) + ":" + desc.get(i));
                        rowTextView.setTextColor(Color.BLACK);

                        // add the textview to the linearlayout
                        rLayout.addView(rowTextView);

                    }

                }

                desc = null;

            }

        });
    }

}
