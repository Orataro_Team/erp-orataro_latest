package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class SeenByAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private ImageView profilePic;
    private TextView txtName;
    private ArrayList<PersonModel> personList = new ArrayList<>();
    private ArrayList<PersonModel> copyList = new ArrayList<>();
    private int count;

    public SeenByAdapter(Context context, ArrayList<PersonModel> personList, int count) {
        this.mContext = context;
        this.personList = personList;
        this.count = count;
        copyList.addAll(personList);
    }

    @Override
    public int getCount() {
        return personList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.seenbylistraw, parent, false);
        profilePic = (ImageView) convertView.findViewById(R.id.iv_std_icon);
        txtName = (TextView) convertView.findViewById(R.id.iv_std_name);
        if (Utility.isNull(personList.get(position).getPersonName())) {
            txtName.setText(personList.get(position).getPersonName());
        }
        convertView.setBackgroundColor(colors[position % colors.length]);

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL + personList.get(position).getProfileImg())
                    .apply(options)
                    .into(profilePic);

        } catch (Exception e) {
            e.printStackTrace();
        }

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        return convertView;

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        personList.clear();
        if (charText.length() == 0) {
            personList.addAll(copyList);
        } else {
            for (PersonModel vo : copyList) {
                if (vo.getPersonName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    personList.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }
}
