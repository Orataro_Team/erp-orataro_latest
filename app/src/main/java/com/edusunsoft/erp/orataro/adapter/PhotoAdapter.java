package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ZoomImageAcitivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PhotoListModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class PhotoAdapter extends BaseAdapter {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private ImageView iv_profile_pic;
    private LinearLayout ll_option;
    private LinearLayout albumDetaillayout;
    private TextView txt_title, txtDetail;
    private ArrayList<PhotoListModel> photoModels;
    private static final int ID_LIKE = 1;
    private static final int ID_UNLIKE = 2;
    private static final int ID_FRIEND = 3;
    private static final int ID_PUBLIC = 4;
    private static final int ID_ONLY_ME = 5;
    private ActionItem actionLike, actionUnlike, actionPublic, actionFriend, actionOnlyMe;
    private QuickAction quickAction;
    private int flag = 0;

    public PhotoAdapter(Context context, ArrayList<PhotoListModel> photoModels, int flag) {
        mContext = context;
        this.photoModels = photoModels;
        this.flag = flag;
    }

    @Override
    public int getCount() {
        return photoModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.photos_listraw, parent, false);
        ll_option = (LinearLayout) convertView.findViewById(R.id.ll_option);
        iv_profile_pic = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        albumDetaillayout = (LinearLayout) convertView.findViewById(R.id.albumDetaillayout);
        txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        txtDetail = (TextView) convertView.findViewById(R.id.txt_desc);

        if (photoModels.get(position).getPhoto() != null && !photoModels.get(position).getPhoto().equalsIgnoreCase("")) {

            if (photoModels.get(position).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {
                try {
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.photo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(photoModels.get(position).getPhoto())
                            .apply(options)
                            .into(iv_profile_pic);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(photoModels.get(position).getPhoto(), options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE) scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(photoModels.get(position).getPhoto(), options);
                iv_profile_pic.setImageBitmap(bm);
            }
        }

        actionLike = new ActionItem(ID_LIKE, "Like", mContext.getResources().getDrawable(R.drawable.like_white));
        actionUnlike = new ActionItem(ID_UNLIKE, "Unlike", mContext.getResources().getDrawable(R.drawable.edit_profile));
        actionPublic = new ActionItem(ID_PUBLIC, "Public", mContext.getResources().getDrawable(R.drawable.fb_publics));
        actionFriend = new ActionItem(ID_FRIEND, "Friends", mContext.getResources().getDrawable(R.drawable.fb_friends));
        actionOnlyMe = new ActionItem(ID_ONLY_ME, "Only Me", mContext.getResources().getDrawable(R.drawable.fb_only_me));
        quickAction = new QuickAction(mContext, QuickAction.VERTICAL);
        quickAction.addActionItem(actionLike);
        quickAction.addActionItem(actionUnlike);
        quickAction.addActionItem(actionFriend);
        quickAction.addActionItem(actionPublic);
        quickAction.addActionItem(actionOnlyMe);
        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);

                if (actionId == ID_LIKE) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.Like));
//							Toast.makeText(mContext, mContext.getResources().getString(R.string.Like), Toast.LENGTH_SHORT).show();
                } else if (actionId == ID_UNLIKE) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.Unlike));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.Unlike), Toast.LENGTH_SHORT).show();
                } else if (actionId == ID_FRIEND) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.Friends));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.Friends), Toast.LENGTH_SHORT).show();
                } else if (actionId == ID_PUBLIC) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.Public));
//							Toast.makeText(mContext, mContext.getResources().getString(R.string.Public), Toast.LENGTH_SHORT).show();
                } else if (actionId == ID_ONLY_ME) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.onlyme));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.onlyme), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ll_option.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });

        iv_profile_pic.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (flag == 1) {
                    Intent in = new Intent(mContext, ZoomImageAcitivity.class);
                    in.putExtra("img", photoModels.get(position).getPhoto());
                    in.putExtra("name", "Photo");
                    mContext.startActivity(in);
                }
            }
        });

        if (flag == 1) {
            albumDetaillayout.setVisibility(View.GONE);
        }

        return convertView;
    }

}
