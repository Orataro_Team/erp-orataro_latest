package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ClassworkListDataDao {

    @Insert
    void insertClassworkList(ClassWorkListModel classWorkListModel);

    @Query("SELECT * from ClassworkList")
    List<ClassWorkListModel> getClassworkList();

    @Query("DELETE  FROM ClassworkList")
    int deleteClassworkListItem();

    @Query("delete from ClassworkList where ClassWorkID=:classworkid")
    void deleteclassworkItemByClassworkID(String classworkid);

}
