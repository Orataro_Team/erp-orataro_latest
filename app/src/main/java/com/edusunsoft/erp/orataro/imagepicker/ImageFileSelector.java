package com.edusunsoft.erp.orataro.imagepicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

@SuppressWarnings("unused")
public class ImageFileSelector {

    @Nullable
    private Callback mCallback = null;
    private ImagePickHelper mImagePickHelper;
    private ImageCaptureHelper mImageCaptureHelper;
    private ImageCompressHelper mImageCompressHelper;

    public ImageCompressHelper.CompressParams compressParams;

    public ImageFileSelector(Context context) {

        String defaultOutputPath = context.getExternalCacheDir() + "/images/";
        compressParams = new ImageCompressHelper.CompressParams();
        compressParams.outputPath = defaultOutputPath;

        mImageCompressHelper = new ImageCompressHelper();
        mImageCompressHelper.setCallback(new ImageCompressHelper.Callback() {
            @Override
            public void onError() {
                if (mCallback != null) {
                    mCallback.onError(ErrorResult.error);
                }
            }

            @Override
            public void onSuccess(String file) {
                if (mCallback != null) {
                    mCallback.onSuccess(file);
                }

            }
        });

        Callback callback = new Callback() {
            @Override
            public void onError(ErrorResult errorResult) {
                if (mCallback != null) {
                    mCallback.onError(errorResult);
                }
            }

            @Override
            public void onSuccess(String file) {
                if (mCallback != null) {
                    mCallback.onSuccess(file);
                }
            }
        };

        mImagePickHelper = new ImagePickHelper(context);
        mImagePickHelper.setCallback(callback);

        mImageCaptureHelper = new ImageCaptureHelper();
        mImageCaptureHelper.setCallback(callback);

    }


    void setOutPutPath(String outPutPath) {
        compressParams.outputPath = outPutPath;
    }

    void setSelectFileType(String type) {
        mImagePickHelper.setType(type);
    }

    /**
     * è®¾ç½®åŽ‹ç¼©å�Žçš„æ–‡ä»¶å¤§å°�
     *
     * @param maxWidth  åŽ‹ç¼©å�Žæ–‡ä»¶å®½åº¦
     *                  *
     * @param maxHeight åŽ‹ç¼©å�Žæ–‡ä»¶é«˜åº¦
     */
    @SuppressWarnings("unused")
    public void setOutPutImageSize(int maxWidth, int maxHeight) {
        compressParams.maxWidth = maxWidth;
        compressParams.maxHeight = maxHeight;
    }

    /**
     * è®¾ç½®åŽ‹ç¼©å�Žä¿�å­˜å›¾ç‰‡çš„è´¨é‡�
     *
     * @param quality å›¾ç‰‡è´¨é‡� 0 - 100
     */
    @SuppressWarnings("unused")
    void setQuality(int quality) {
        compressParams.saveQuality = quality;
    }

    /**
     * set image compress format
     *
     * @param compressFormat compress format
     */
    @SuppressWarnings("unused")
    void setCompressFormat(Bitmap.CompressFormat compressFormat) {
        compressParams.compressFormat = compressFormat;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mImagePickHelper.onActivityResult(requestCode, resultCode, data);
        mImageCaptureHelper.onActivityResult(requestCode, resultCode, data);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == mImagePickHelper.getRequestCode()) {
            mImagePickHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (requestCode == mImageCaptureHelper.getRequestCode()) {
            mImageCaptureHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
    }

    public void onRestoreInstanceState(Bundle outState) {
    }

    public void setCallback(@Nullable Callback callback) {
        mCallback = callback;
    }

    public void selectImage(Activity activity, int requestCode) {
        mImagePickHelper.selectorImage(activity, requestCode);
    }

   public void selectImage(Fragment fragment, int requestCode) {
        mImagePickHelper.selectImage(fragment, requestCode);
    }

    public void takePhoto(Activity activity, int requestCode) {
        mImageCaptureHelper.captureImage(activity, requestCode);
    }

    public void takePhoto(Fragment fragment, int requestCode) {
        mImageCaptureHelper.captureImage(fragment, requestCode);
    }


    public static void setDebug(Boolean debug) {
        AppLogger.DEBUG = debug;
    }

    /**
     * Created by sunwei on 10/11/2016 2:36 PM.
     */

    public interface Callback {

        void onError(ErrorResult errorResult);
        void onSuccess(String file);

    }
}
