package com.edusunsoft.erp.orataro.Interface;

import com.edusunsoft.erp.orataro.model.uploadHomeworkResModel;

public interface OnUploadedHomeworkItemClickListener {
    void onItemClick(uploadHomeworkResModel.Data data);
}
