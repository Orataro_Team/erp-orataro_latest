package com.edusunsoft.erp.orataro.util;

import android.graphics.Color;

public class Constants {

	public static int profile=0;
	public static int CONTINUEREADINGSIZE=100;
	public static int CONTINUEREADINGSIZEWITHCONTUNUEREADING=119; // + 19 with continue reading size
	public static String CONTINUEREADINGSTR="...Continue Reading";
	public static int CONTINUEREADINGTEXTCOLOR= Color.parseColor("#27305B");
	public static int DATATYPE_INT=1;
	public static int DATATYPE_STRING=2;
	public static int DATATYPE_DOUBLE=3;
	public static int DATATYPE_BYTE=4;
	public static String ForDialogStyle = "Default";
	
}
