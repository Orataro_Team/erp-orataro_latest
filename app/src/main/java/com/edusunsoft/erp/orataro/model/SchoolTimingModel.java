package com.edusunsoft.erp.orataro.model;

public class SchoolTimingModel {

    private String st_class;
    private String st_time;

    public String getSt_class() {
        return st_class;
    }

    public void setSt_class(String st_class) {
        this.st_class = st_class;
    }

    public String getSt_time() {
        return st_time;
    }

    public void setSt_time(String st_time) {
        this.st_time = st_time;
    }

}
