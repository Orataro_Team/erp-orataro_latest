package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ZoomImageAcitivity;
import com.edusunsoft.erp.orataro.database.GenerallWallData;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;

import java.util.ArrayList;

public class MultiplePhotoAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> list;
    private GenerallWallData postModel;
    private AdjustableImageView imgView;
    private LayoutInflater layoutInfalater;
    private LinearLayout ll_img;

    public MultiplePhotoAdapter(Context mContext, ArrayList<String> list, GenerallWallData postModel) {
        this.mContext = mContext;
        this.list = list;
        this.postModel = postModel;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.imageview, parent, false);
        imgView = (AdjustableImageView) convertView.findViewById(R.id.iv_fb_zoom);

        try {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL + "Datafiles/" + list.get(position).replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"))
                    .apply(options)
                    .into(imgView);

        } catch (Exception e) {

            e.printStackTrace();

        }

        imgView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(mContext, ZoomImageAcitivity.class);
                in.putExtra("img", ServiceResource.BASE_IMG_URL + "Datafiles/" + list.get(position).replace("//DataFiles//", "/DataFiles/")
                        .replace("//DataFiles/", "/DataFiles/"));
                in.putExtra("name", postModel.getFullName());
                in.putStringArrayListExtra("imgList", list);
                mContext.startActivity(in);

            }
            
        });

        return convertView;
    }

}
