package com.edusunsoft.erp.orataro.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoListModel implements Parcelable {

    private String PostCommentID;
    private String AlbumPhotosID;
    private String Photo;

    public PhotoListModel() {

    }

    public static Creator<PhotoListModel> CREATOR = new Creator<PhotoListModel>() {
        public PhotoListModel createFromParcel(Parcel source) {
            return new PhotoListModel(source);
        }
        public PhotoListModel[] newArray(int size) {
            return new PhotoListModel[size];
        }
    };

    private PhotoListModel(Parcel in) {
        this.PostCommentID = in.readString();
        this.AlbumPhotosID = in.readString();
        this.Photo = in.readString();
    }

    public String getAlbumPhotosID() {
        return AlbumPhotosID;
    }

    public void setAlbumPhotosID(String albumPhotosID) {
        AlbumPhotosID = albumPhotosID;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.PostCommentID);
        dest.writeString(this.AlbumPhotosID);
        dest.writeString(this.Photo);
    }

}
