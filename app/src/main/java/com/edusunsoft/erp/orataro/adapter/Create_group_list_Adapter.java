package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class Create_group_list_Adapter extends BaseAdapter {

	private LayoutInflater layoutInfalater;
	Context context;
	private int[] colors = new int[] { Color.parseColor("#EFEFEF"), Color.parseColor("#FFFFFF") };

	public Create_group_list_Adapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return 5;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderItem viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolderItem();
			layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInfalater.inflate(R.layout.group_participants_list_item, parent, false);
			int colorPos = position % colors.length;
			convertView.setBackgroundColor(colors[colorPos]);
			viewHolder.txt_contact_name = (TextView) convertView.findViewById(R.id.txt_contact_name);
			viewHolder.txt_contact_detail = (TextView) convertView.findViewById(R.id.txt_contact_detail);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderItem) convertView.getTag();
		}

		return convertView;
	}

	public static class ViewHolderItem {
		ImageView img_contact_pic;
		TextView txt_contact_name;
		TextView txt_contact_detail;
	}
}
