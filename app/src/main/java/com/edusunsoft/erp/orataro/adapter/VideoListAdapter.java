package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class VideoListAdapter extends BaseAdapter {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private CircleImageView iv_profile_pic;
    private LinearLayout ll_option;
    private ArrayList<PhotoModel> photoModels;
    private static final int ID_LIKE = 1;
    private static final int ID_UNLIKE = 2;
    private static final int ID_FRIEND = 3;
    private static final int ID_PUBLIC = 4;
    private static final int ID_ONLY_ME = 5;
    private ActionItem actionLike, actionUnlike, actionPublic, actionFriend,
            actionOnlyMe;
    private QuickAction quickAction;

    public VideoListAdapter(Context context, ArrayList<PhotoModel> photoModels) {
        mContext = context;
        this.photoModels = photoModels;
    }

    @Override
    public int getCount() {
        return photoModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.videilistraw, parent, false);
        ll_option = (LinearLayout) convertView.findViewById(R.id.ll_option);
        iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);
        iv_profile_pic.setImageBitmap(Utility.getBitmapFromAsset(mContext, photoModels.get(position).getPhotos()));
        actionLike = new ActionItem(ID_LIKE, "Like", mContext.getResources().getDrawable(R.drawable.like_white));
        actionUnlike = new ActionItem(ID_UNLIKE, "Unlike", mContext.getResources().getDrawable(R.drawable.edit_profile));
        actionPublic = new ActionItem(ID_PUBLIC, "Public", mContext.getResources().getDrawable(R.drawable.fb_publics));
        actionFriend = new ActionItem(ID_FRIEND, "Friends", mContext.getResources().getDrawable(R.drawable.fb_friends));
        actionOnlyMe = new ActionItem(ID_ONLY_ME, "Only Me", mContext.getResources().getDrawable(R.drawable.fb_only_me));
        quickAction = new QuickAction(mContext, QuickAction.VERTICAL);
        quickAction.addActionItem(actionLike);
        quickAction.addActionItem(actionUnlike);
        quickAction.addActionItem(actionFriend);
        quickAction.addActionItem(actionPublic);
        quickAction.addActionItem(actionOnlyMe);

        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);

                        if (actionId == ID_LIKE) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Like));
//							Toast.makeText(mContext,mContext. getResources().getString(R.string.Like), Toast.LENGTH_SHORT).show();
                        } else if (actionId == ID_UNLIKE) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Unlike));
//							Toast.makeText(mContext,mContext. getResources().getString(R.string.Unlike), Toast.LENGTH_SHORT).show();
                        } else if (actionId == ID_FRIEND) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Friends));
//							Toast.makeText(mContext, mContext.getResources().getString(R.string.Friends), Toast.LENGTH_SHORT).show();
                        } else if (actionId == ID_PUBLIC) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Public));
//							Toast.makeText(mContext, mContext.getResources().getString(R.string.Public), Toast.LENGTH_SHORT).show();
                        } else if (actionId == ID_ONLY_ME) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.onlyme));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.onlyme), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        ll_option.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });

        return convertView;
    }

}
