package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ExamTimingListAdapter;
import com.edusunsoft.erp.orataro.model.ExamTimingModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TermExamTimeTableFragment extends Fragment implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    List<String> GroupExamList = new ArrayList<>();
    ExamTimingModel examtimingModel;
    ArrayList<ExamTimingModel> ExamList = new ArrayList<>();

    RecyclerView examTimingList;
    private TextView txtNoDataFound;
    LinearLayout headerlayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_route_list, null);
        mContext = getActivity();
        ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Initialization(view);
        return view;

    }

    private void Initialization(View view) {

        mContext = getActivity();

        headerlayout = view.findViewById(R.id.headerLayout);
        headerlayout.setVisibility(View.GONE);
        examTimingList = view.findViewById(R.id.routelist);
        txtNoDataFound = view.findViewById(R.id.txt_nodatafound);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            getWeeklyExamTimeTableList(true);
        }

    }

    private void getWeeklyExamTimeTableList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID,
                new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.EXAMTYPE, ServiceResource.TERMEXAM));

        Log.d("weeklyexamtitmetableReq", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETEXAMRESULTTIMETABLE, ServiceResource.EXAM_URL);

    }

    @Override
    public void response(String result, String methodName) {

        Log.d("getExamResult", result);

        if (result.equals("[]")) {
            examTimingList.setAdapter(null);
            examTimingList.setVisibility(View.GONE);
            txtNoDataFound.setVisibility(View.VISIBLE);
        } else {
            try {
                GroupExamList.clear();
                ExamList.clear();
                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jop = jArray.getJSONObject(i);
                    GroupExamList.add(jop.getString("ExamName"));
                    JSONArray datajsonarray = jop.getJSONArray("data");

                    for (int j = 0; j < datajsonarray.length(); j++) {

                        JSONObject childobj = datajsonarray.getJSONObject(j);

                        examtimingModel = new ExamTimingModel();
                        examtimingModel.setStdExaScheduleID(childobj.getString("StdExaScheduleID"));
                        examtimingModel.setStartDate(childobj.getString("StartDate"));
                        examtimingModel.setDateOfExam(childobj.getString("DateOfExam"));
                        examtimingModel.setSubjectName(childobj.getString("EndDate"));
                        examtimingModel.setExamDayName(childobj.getString("ExamDayName"));
                        examtimingModel.setSubjectName(childobj.getString("SubjectName"));
                        examtimingModel.setPaperTypeName(childobj.getString("PaperTypeName"));
                        examtimingModel.setStartTime(childobj.getString("StartTime"));
                        examtimingModel.setEndTime(childobj.getString("EndTime"));
                        examtimingModel.setExamMasterID(childobj.getString("ExamMasterID"));
                        examtimingModel.setExamName(childobj.getString("ExamName"));

                        ExamList.add(examtimingModel);

                        Log.d("getAllExamList", ExamList.toString());

                    }
                    {
                        ExamTimingListAdapter examListAdapter = new ExamTimingListAdapter(mContext, (ArrayList<String>) GroupExamList, ExamList);
                        examTimingList.setAdapter(examListAdapter);
                        examTimingList.setVisibility(View.VISIBLE);
                        txtNoDataFound.setVisibility(View.GONE);
                    }
                    
                }

            } catch (JSONException e) {

                e.printStackTrace();

            }
        }
    }

}
