package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class LeaveListModel implements Serializable {

    private String SchoolLeaveNoteID;
    private String DateOfApplication;
    private String ApplicationStatus_Term;
    private String ReasonForLeave;
    private String TeacherComment;
    private String ApprovedOn;
    private String StartDate;
    private String EndDate;
    private String StudentName;
    private String ApplicationBY;
    private String TeacherName;
    private String ApplicationBy_Term;
    private String StudentID;
    private String DefStatus;
    private String IsPerApplication;
    private String ApplicationToTeacherID;
    private String cr_month;
    private String cr_date;
    private String year;
    private String milliSecond;
    private boolean isVisibleMonth = false;

    public String getApplicationToTeacherID() {
        return ApplicationToTeacherID;
    }

    public void setApplicationToTeacherID(String applicationToTeacherID) {
        ApplicationToTeacherID = applicationToTeacherID;
    }

    public String getMilliSecond() {
        return milliSecond;
    }

    public void setMilliSecond(String milliSecond) {
        this.milliSecond = milliSecond;
    }

    public boolean isVisibleMonth() {
        return isVisibleMonth;
    }

    public void setVisibleMonth(boolean isVisibleMonth) {
        this.isVisibleMonth = isVisibleMonth;
    }

    public String getCr_month() {
        return cr_month;
    }

    public void setCr_month(String cr_month) {
        this.cr_month = cr_month;
    }

    public String getCr_date() {
        return cr_date;
    }

    public void setCr_date(String cr_date) {
        this.cr_date = cr_date;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSchoolLeaveNoteID() {
        return SchoolLeaveNoteID;
    }

    public void setSchoolLeaveNoteID(String schoolLeaveNoteID) {
        SchoolLeaveNoteID = schoolLeaveNoteID;
    }

    public String getDateOfApplication() {
        return DateOfApplication;
    }

    public void setDateOfApplication(String dateOfApplication) {
        DateOfApplication = dateOfApplication;
    }

    public String getApplicationStatus_Term() {
        return ApplicationStatus_Term;
    }

    public void setApplicationStatus_Term(String applicationStatus_Term) {
        ApplicationStatus_Term = applicationStatus_Term;
    }

    public String getReasonForLeave() {
        return ReasonForLeave;
    }

    public void setReasonForLeave(String reasonForLeave) {
        ReasonForLeave = reasonForLeave;
    }

    public String getTeacherComment() {
        return TeacherComment;
    }

    public void setTeacherComment(String teacherComment) {
        TeacherComment = teacherComment;
    }

    public String getApprovedOn() {
        return ApprovedOn;
    }

    public void setApprovedOn(String approvedOn) {
        ApprovedOn = approvedOn;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getStudentName() {

        return StudentName;

    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getApplicationBY() {
        return ApplicationBY;
    }

    public void setApplicationBY(String applicationBY) {
        ApplicationBY = applicationBY;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getApplicationBy_Term() {
        return ApplicationBy_Term;
    }

    public void setApplicationBy_Term(String applicationBy_Term) {
        ApplicationBy_Term = applicationBy_Term;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getDefStatus() {
        return DefStatus;
    }

    public void setDefStatus(String defStatus) {
        DefStatus = defStatus;
    }

    public boolean getIsPerApplication() {
        if (IsPerApplication != null) {
            if (IsPerApplication.equalsIgnoreCase("true")) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public void setIsPerApplication(String isPerApplication) {
        IsPerApplication = isPerApplication;
    }


}
