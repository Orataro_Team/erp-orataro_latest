package com.edusunsoft.erp.orataro.model;

public class StudentModel {

    private String studentId, studentName, Absent;
    private String profilePic, attendancePersantage, fullname;
    private String MemberID, RegistrationNo, RollNo, SContactNo, MFullName, MContactNo, FFullName, FContactNo, Present;

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getRegistrationNo() {
        return RegistrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        RegistrationNo = registrationNo;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public String getSContactNo() {
        return SContactNo;
    }

    public void setSContactNo(String sContactNo) {
        SContactNo = sContactNo;
    }

    public String getMFullName() {
        return MFullName;
    }

    public void setMFullName(String mFullName) {
        MFullName = mFullName;
    }

    public String getMContactNo() {
        return MContactNo;
    }

    public void setMContactNo(String mContactNo) {
        MContactNo = mContactNo;
    }

    public String getFFullName() {
        return FFullName;
    }

    public void setFFullName(String fFullName) {
        FFullName = fFullName;
    }

    public String getFContactNo() {
        return FContactNo;
    }

    public void setFContactNo(String fContactNo) {
        FContactNo = fContactNo;
    }

    public String getPresent() {
        return Present;
    }

    public void setPresent(String present) {
        Present = present;
    }

    public String getAbsent() {
        return Absent;
    }

    public void setAbsent(String absent) {
        Absent = absent;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getAttendancePersantage() {
        return attendancePersantage;
    }

    public void setAttendancePersantage(String attendancePersantage) {
        this.attendancePersantage = attendancePersantage;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}
