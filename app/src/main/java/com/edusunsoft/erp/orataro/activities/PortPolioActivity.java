package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class PortPolioActivity extends Activity {
	
	ImageView imgLeftheader,imgRightheader;
	TextView txtHeader;
	private Context mContext;
	private WebView Wv_page;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.portpolio);
		mContext = PortPolioActivity.this;
		((Activity) mContext).getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		imgLeftheader = (ImageView) findViewById(R.id.img_home);
		imgRightheader = (ImageView) findViewById(R.id.img_menu);
		txtHeader  = (TextView) findViewById(R.id.header_text);
		
		
		Wv_page = (WebView) findViewById(R.id.webview);
		Wv_page.getSettings().setBuiltInZoomControls(true);
		Wv_page.getSettings().setSupportZoom(true); 
		Wv_page.getSettings().setUseWideViewPort(true);
		Wv_page.getSettings().setLoadWithOverviewMode(true);
		Wv_page.getSettings().setJavaScriptEnabled(true);
		
		Wv_page.loadUrl("https://play.google.com/store/search?q=sunsoft%20eduware&c=apps");
		imgLeftheader.setImageResource(R.drawable.back);
		imgRightheader.setVisibility(View.INVISIBLE);
		txtHeader.setText(getResources().getString(R.string.OurOtherFreeApps));
		
		
		imgLeftheader.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	

}
