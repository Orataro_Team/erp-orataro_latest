package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "DynamicWallSetting")
public class DynamicWallSettingModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "IsAllowPeopleToLikeThisWall")
    @SerializedName("IsAllowPeopleToLikeThisWall")
    public String IsAllowPeopleToLikeThisWall;

    @ColumnInfo(name = "IsAllowPeoplePostComment")
    @SerializedName("IsAllowPeoplePostComment")
    public String IsAllowPeoplePostComment;

    @ColumnInfo(name = "IsAllowPeopleToShareComment")
    @SerializedName("IsAllowPeopleToShareComment")
    public String IsAllowPeopleToShareComment;

    @ColumnInfo(name = "IsAllowPeopleToLikeAndDislikeComment")
    @SerializedName("IsAllowPeopleToLikeAndDislikeComment")
    public String IsAllowPeopleToLikeAndDislikeComment;

    @ColumnInfo(name = "IsAllowPeopleToPostStatus")
    @SerializedName("IsAllowPeopleToPostStatus")
    public String IsAllowPeopleToPostStatus;

    @ColumnInfo(name = "IsAllowPeopleToCreatePoll")
    @SerializedName("IsAllowPeopleToCreatePoll")
    public String IsAllowPeopleToCreatePoll;

    @ColumnInfo(name = "IsAllowPeopleToParticipateInPoll")
    @SerializedName("IsAllowPeopleToParticipateInPoll")
    public String IsAllowPeopleToParticipateInPoll;

    @ColumnInfo(name = "IsAllowPeopleToInviteOtherPeople")
    @SerializedName("IsAllowPeopleToInviteOtherPeople")
    public String IsAllowPeopleToInviteOtherPeople;

    @ColumnInfo(name = "IsAllowPeopleToTagOnPost")
    @SerializedName("IsAllowPeopleToTagOnPost")
    public String IsAllowPeopleToTagOnPost;

    @ColumnInfo(name = "IsAllowPeopleToUploadAlbum")
    @SerializedName("IsAllowPeopleToUploadAlbum")
    public String IsAllowPeopleToUploadAlbum = "";

    @ColumnInfo(name = "IsAllowPeopleToPutGeoLocationOnPost")
    @SerializedName("IsAllowPeopleToPutGeoLocationOnPost")
    public String IsAllowPeopleToPutGeoLocationOnPost;

    @ColumnInfo(name = "IsAllowPeopleToPostDocument")
    @SerializedName("IsAllowPeopleToPostDocument")
    public String IsAllowPeopleToPostDocument;

    @ColumnInfo(name = "IsAllowPeopleToPostVideos")
    @SerializedName("IsAllowPeopleToPostVideos")
    public String IsAllowPeopleToPostVideos;

    @ColumnInfo(name = "WallName")
    @SerializedName("WallName")
    public String WallName;

    @ColumnInfo(name = "WallImage")
    @SerializedName("WallImage")
    public String WallImage;

    @ColumnInfo(name = "WallID")
    @SerializedName("WallID")
    public String WallID;

    @ColumnInfo(name = "IsAutoApprovePost")
    @SerializedName("IsAutoApprovePost")
    public String IsAutoApprovePost;

    @ColumnInfo(name = "IsAutoApprovePostStatus")
    @SerializedName("IsAutoApprovePostStatus")
    public String IsAutoApprovePostStatus;

    @ColumnInfo(name = "IsAutoApproveAlbume")
    @SerializedName("IsAutoApproveAlbume")
    public String IsAutoApproveAlbume;

    @ColumnInfo(name = "IsAutoApproveVideos")
    @SerializedName("IsAutoApproveVideos")
    public String IsAutoApproveVideos;

    @ColumnInfo(name = "IsAutoApproveDocument")
    @SerializedName("IsAutoApproveDocument")
    public String IsAutoApproveDocument;

    @ColumnInfo(name = "IsAutoApprovePoll")
    @SerializedName("IsAutoApprovePoll")
    public String IsAutoApprovePoll;

    @ColumnInfo(name = "IsAdmin")
    @SerializedName("IsAdmin")
    public String IsAdmin;

    @ColumnInfo(name = "IsAllowLikeDislike")
    @SerializedName("IsAllowLikeDislike")
    public String IsAllowLikeDislike;

    @ColumnInfo(name = "IsAllowPostAlbum")
    @SerializedName("IsAllowPostAlbum")
    public String IsAllowPostAlbum;

    @ColumnInfo(name = "IsAllowPostComment")
    @SerializedName("IsAllowPostComment")
    public String IsAllowPostComment;

    @ColumnInfo(name = "IsAllowPostFile")
    @SerializedName("IsAllowPostFile")
    public String IsAllowPostFile;

    @ColumnInfo(name = "IsAllowPostPhoto")
    @SerializedName("IsAllowPostPhoto")
    public String IsAllowPostPhoto;

    @ColumnInfo(name = "IsAllowPostStatus")
    @SerializedName("IsAllowPostStatus")
    public String IsAllowPostStatus;

    @ColumnInfo(name = "IsAllowPostVideo")
    @SerializedName("IsAllowPostVideo")
    public String IsAllowPostVideo;

    @ColumnInfo(name = "IsAllowSharePost")
    @SerializedName("IsAllowSharePost")
    public String IsAllowSharePost;

    @ColumnInfo(name = "IsAllowPeopleToPostCommentOnPost")
    @SerializedName("IsAllowPeopleToPostCommentOnPost")
    public String IsAllowPeopleToPostCommentOnPost;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getIsAllowLikeDislike() {
        return Strtobool(IsAllowLikeDislike);
    }

    public void setIsAllowLikeDislike(String isAllowLikeDislike) {
        this.IsAllowLikeDislike = isAllowLikeDislike;
    }

    public String getIsAllowPostAlbum() {
        return IsAllowPostAlbum;
    }

    public void setIsAllowPostAlbum(String isAllowPostAlbum) {
        this.IsAllowPostAlbum = isAllowPostAlbum;
    }

    public boolean getIsAllowPostComment() {
        return Strtobool(IsAllowPostComment);
    }

    public void setIsAllowPostComment(String isAllowPostComment) {
        this.IsAllowPostComment = isAllowPostComment;
    }

    public boolean getIsAllowPostFile() {
        return Strtobool(IsAllowPostFile);
    }

    public void setIsAllowPostFile(String isAllowPostFile) {
        this.IsAllowPostFile = isAllowPostFile;
    }

    public boolean getIsAllowPostPhoto() {
        return Strtobool(IsAllowPostPhoto);
    }

    public void setIsAllowPostPhoto(String isAllowPostPhoto) {
        this.IsAllowPostPhoto = isAllowPostPhoto;
    }

    public boolean getIsAllowPostStatus() {
        return Strtobool(IsAllowPostStatus);
    }

    public void setIsAllowPostStatus(String isAllowPostStatus) {
        this.IsAllowPostStatus = isAllowPostStatus;
    }

    public boolean getIsAllowPostVideo() {
        return Strtobool(IsAllowPostVideo);
    }

    public void setIsAllowPostVideo(String isAllowPostVideo) {
        this.IsAllowPostVideo = isAllowPostVideo;
    }

    public boolean getIsAllowSharePost() {
        return Strtobool(IsAllowSharePost);
    }

    public void setIsAllowSharePost(String isAllowSharePost) {
        this.IsAllowSharePost = isAllowSharePost;
    }

    public boolean getIsAllowPeopleToPostCommentOnPost() {
        return Strtobool(IsAllowPeopleToPostCommentOnPost);
    }

    public void setIsAllowPeopleToPostCommentOnPost(String isAllowPeopleToPostCommentOnPost) {
        this.IsAllowPeopleToPostCommentOnPost = isAllowPeopleToPostCommentOnPost;
    }

    public boolean getIsAllowPeopleToLikeThisWall() {
        return Strtobool(IsAllowPeopleToLikeThisWall);
    }

    public void setIsAllowPeopleToLikeThisWall(String isAllowPeopleToLikeThisWall) {
        this.IsAllowPeopleToLikeThisWall = isAllowPeopleToLikeThisWall;
    }

    public boolean getIsAllowPeoplePostComment() {
        return Strtobool(IsAllowPeoplePostComment);
    }

    public void setIsAllowPeoplePostComment(String isAllowPeoplePostComment) {
        this.IsAllowPeoplePostComment = isAllowPeoplePostComment;
    }

    public boolean getIsAllowPeopleToShareComment() {
        return Strtobool(IsAllowPeopleToShareComment);
    }

    public void setIsAllowPeopleToShareComment(String isAllowPeopleToShareComment) {
        this.IsAllowPeopleToShareComment = isAllowPeopleToShareComment;
    }

    public boolean getIsAllowPeopleToLikeAndDislikeComment() {
        return Strtobool(IsAllowPeopleToLikeAndDislikeComment);
    }

    public void setIsAllowPeopleToLikeAndDislikeComment(
            String isAllowPeopleToLikeAndDislikeComment) {
        this.IsAllowPeopleToLikeAndDislikeComment = isAllowPeopleToLikeAndDislikeComment;
    }

    public boolean getIsAllowPeopleToPostStatus() {
        return Strtobool(IsAllowPeopleToPostStatus);
    }

    public void setIsAllowPeopleToPostStatus(String isAllowPeopleToPostStatus) {
        this.IsAllowPeopleToPostStatus = isAllowPeopleToPostStatus;
    }

    public boolean getIsAllowPeopleToCreatePoll() {
        return Strtobool(IsAllowPeopleToCreatePoll);
    }

    public void setIsAllowPeopleToCreatePoll(String isAllowPeopleToCreatePoll) {
        this.IsAllowPeopleToCreatePoll = isAllowPeopleToCreatePoll;
    }

    public boolean getIsAllowPeopleToParticipateInPoll() {
        return Strtobool(IsAllowPeopleToParticipateInPoll);
    }

    public void setIsAllowPeopleToParticipateInPoll(
            String isAllowPeopleToParticipateInPoll) {
        this.IsAllowPeopleToParticipateInPoll = isAllowPeopleToParticipateInPoll;
    }

    public boolean getIsAllowPeopleToInviteOtherPeople() {
        return Strtobool(IsAllowPeopleToInviteOtherPeople);
    }

    public void setIsAllowPeopleToInviteOtherPeople(
            String isAllowPeopleToInviteOtherPeople) {
        this.IsAllowPeopleToInviteOtherPeople = isAllowPeopleToInviteOtherPeople;
    }

    public boolean getIsAllowPeopleToTagOnPost() {
        return Strtobool(IsAllowPeopleToTagOnPost);
    }

    public void setIsAllowPeopleToTagOnPost(String isAllowPeopleToTagOnPost) {
        this.IsAllowPeopleToTagOnPost = isAllowPeopleToTagOnPost;
    }

    public boolean getIsAllowPeopleToUploadAlbum() {
        return Strtobool(IsAllowPeopleToUploadAlbum);
    }

    public void setIsAllowPeopleToUploadAlbum(String isAllowPeopleToUploadAlbum) {
        this.IsAllowPeopleToUploadAlbum = isAllowPeopleToUploadAlbum;
    }

    public boolean getIsAllowPeopleToPutGeoLocationOnPost() {
        return Strtobool(IsAllowPeopleToPutGeoLocationOnPost);
    }

    public void setIsAllowPeopleToPutGeoLocationOnPost(
            String isAllowPeopleToPutGeoLocationOnPost) {
        this.IsAllowPeopleToPutGeoLocationOnPost = isAllowPeopleToPutGeoLocationOnPost;
    }

    public boolean getIsAllowPeopleToPostDocument() {
        return Strtobool(IsAllowPeopleToPostDocument);
    }

    public void setIsAllowPeopleToPostDocument(String isAllowPeopleToPostDocument) {
        this.IsAllowPeopleToPostDocument = isAllowPeopleToPostDocument;
    }

    public boolean getIsAllowPeopleToPostVideos() {
        return Strtobool(IsAllowPeopleToPostVideos);
    }

    public void setIsAllowPeopleToPostVideos(String isAllowPeopleToPostVideos) {
        this.IsAllowPeopleToPostVideos = isAllowPeopleToPostVideos;
    }

    public boolean getWallName() {
        return Strtobool(WallName);
    }

    public void setWallName(String wallName) {
        this.WallName = wallName;
    }

    public boolean getWallImage() {
        return Strtobool(WallImage);
    }

    public void setWallImage(String wallImage) {
        this.WallImage = wallImage;
    }

    public boolean getWallID() {
        return Strtobool(WallID);
    }

    public void setWallID(String wallID) {
        this.WallID = wallID;
    }

    public boolean getIsAutoApprovePost() {
        return Strtobool(IsAutoApprovePost);
    }

    public void setIsAutoApprovePost(String isAutoApprovePost) {
        this.IsAutoApprovePost = isAutoApprovePost;
    }

    public boolean getIsAutoApprovePostStatus() {
        return Strtobool(IsAutoApprovePostStatus);
    }

    public void setIsAutoApprovePostStatus(String isAutoApprovePostStatus) {
        this.IsAutoApprovePostStatus = isAutoApprovePostStatus;
    }

    public boolean getIsAutoApproveAlbume() {
        return Strtobool(IsAutoApproveAlbume);
    }

    public void setIsAutoApproveAlbume(String isAutoApproveAlbume) {
        this.IsAutoApproveAlbume = isAutoApproveAlbume;
    }

    public boolean getIsAutoApproveVideos() {
        return Strtobool(IsAutoApproveVideos);
    }

    public void setIsAutoApproveVideos(String isAutoApproveVideos) {
        this.IsAutoApproveVideos = isAutoApproveVideos;
    }

    public boolean getIsAutoApproveDocument() {
        return Strtobool(IsAutoApproveDocument);
    }

    public void setIsAutoApproveDocument(String isAutoApproveDocument) {
        this.IsAutoApproveDocument = isAutoApproveDocument;
    }

    public boolean getIsAutoApprovePoll() {
        return Strtobool(IsAutoApprovePoll);
    }

    public void setIsAutoApprovePoll(String isAutoApprovePoll) {
        this.IsAutoApprovePoll = isAutoApprovePoll;
    }

    public boolean getIsAdmin() {
        if (IsAdmin != null && IsAdmin.equalsIgnoreCase("1")) {
            return true;
        } else {
            return false;
        }
    }

    public void setIsAdmin(String isAdmin) {
        IsAdmin = isAdmin;
    }

    public boolean Strtobool(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("0")) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "DynamicWallSetting{" +
                "IsAllowPeopleToLikeThisWall='" + IsAllowPeopleToLikeThisWall + '\'' +
                ", IsAllowPeoplePostComment='" + IsAllowPeoplePostComment + '\'' +
                ", IsAllowPeopleToShareComment='" + IsAllowPeopleToShareComment + '\'' +
                ", IsAllowPeopleToLikeAndDislikeComment='" + IsAllowPeopleToLikeAndDislikeComment + '\'' +
                ", IsAllowPeopleToPostStatus='" + IsAllowPeopleToPostStatus + '\'' +
                ", IsAllowPeopleToCreatePoll='" + IsAllowPeopleToCreatePoll + '\'' +
                ", IsAllowPeopleToParticipateInPoll='" + IsAllowPeopleToParticipateInPoll + '\'' +
                ", IsAllowPeopleToInviteOtherPeople='" + IsAllowPeopleToInviteOtherPeople + '\'' +
                ", IsAllowPeopleToTagOnPost='" + IsAllowPeopleToTagOnPost + '\'' +
                ", IsAllowPeopleToUploadAlbum='" + IsAllowPeopleToUploadAlbum + '\'' +
                ", IsAllowPeopleToPutGeoLocationOnPost='" + IsAllowPeopleToPutGeoLocationOnPost + '\'' +
                ", IsAllowPeopleToPostDocument='" + IsAllowPeopleToPostDocument + '\'' +
                ", IsAllowPeopleToPostVideos='" + IsAllowPeopleToPostVideos + '\'' +
                ", WallName='" + WallName + '\'' +
                ", WallImage='" + WallImage + '\'' +
                ", WallID='" + WallID + '\'' +
                ", IsAutoApprovePost='" + IsAutoApprovePost + '\'' +
                ", IsAutoApprovePostStatus='" + IsAutoApprovePostStatus + '\'' +
                ", IsAutoApproveAlbume='" + IsAutoApproveAlbume + '\'' +
                ", IsAutoApproveVideos='" + IsAutoApproveVideos + '\'' +
                ", IsAutoApproveDocument='" + IsAutoApproveDocument + '\'' +
                ", IsAutoApprovePoll='" + IsAutoApprovePoll + '\'' +
                ", IsAdmin='" + IsAdmin + '\'' +
                '}';
    }
}
