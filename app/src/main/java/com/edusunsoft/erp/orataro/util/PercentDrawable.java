package com.edusunsoft.erp.orataro.util;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class PercentDrawable extends Drawable {

    private final float percent;
    private final Paint paint;

    public PercentDrawable(float percent) {
        super();
        this.percent = percent;
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setColor(0xffff0000);

    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawRect(0, 0, percent * canvas.getWidth() / 100, percent * canvas.getWidth() / 100, paint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
    
}
