package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PollListAdapter;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

import java.util.ArrayList;
import java.util.Comparator;

public class StudentPollFragment extends Fragment implements ResponseWebServices {
    View view;
    LinearLayout addbtnlayout;
    PullAndLoadListView lv_postfragment;
    Context mContext;
    private PollListAdapter adapter;
    TextView txt_nodatafound;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.postfragment, null);
        mContext = getActivity();
        txt_nodatafound = (TextView) view.findViewById(R.id.txt_nodatafound);
        addbtnlayout = (LinearLayout) view.findViewById(R.id.addbtnlayout);
        lv_postfragment = (PullAndLoadListView) view.findViewById(R.id.lv_postfragment);


        getPollListStudent();

        lv_postfragment.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {
                // Do work to refresh the list here.
                getPollListStudent();

            }
        });

        return view;
    }


    public void getPollListStudent() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        Log.d("pollrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOLLSLISTFORPARTICIPANTPAGE_METHODNAME,
                ServiceResource.POLL_URL);
        
    }


    @Override
    public void response(String result, String methodName) {


    }


    public class SortedDate implements Comparator<PollModel> {
        @Override
        public int compare(PollModel o1, PollModel o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    }

}
