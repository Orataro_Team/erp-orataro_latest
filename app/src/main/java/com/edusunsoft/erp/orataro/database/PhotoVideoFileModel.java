package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "PhotoVideoFileModel")
public class PhotoVideoFileModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @ColumnInfo(name = "RowNo")
    @SerializedName("RowNo")
    private String RowNo;

    @ColumnInfo(name = "PostCommentID")
    @SerializedName("PostCommentID")
    private String PostCommentID;

    @ColumnInfo(name = "WallID")
    @SerializedName("WallID")
    private String WallID;

    @ColumnInfo(name = "MemberID")
    @SerializedName("MemberID")
    private String MemberID;

    @ColumnInfo(name = "PostCommentTypesTerm")
    @SerializedName("PostCommentTypesTerm")
    private String PostCommentTypesTerm;

    @ColumnInfo(name = "PostCommentNote")
    @SerializedName("PostCommentNote")
    private String PostCommentNote;

    @ColumnInfo(name = "TotalLikes")
    @SerializedName("TotalLikes")
    private String TotalLikes;

    @ColumnInfo(name = "TotalComments")
    @SerializedName("TotalComments")
    private String TotalComments;

    @ColumnInfo(name = "TotalDislike")
    @SerializedName("TotalDislike")
    private String TotalDislike;

    @ColumnInfo(name = "DateOfPost")
    @SerializedName("DateOfPost")
    private String DateOfPost;

    @ColumnInfo(name = "AssociationID")
    @SerializedName("AssociationID")
    private String AssociationID;

    @ColumnInfo(name = "AssociationType")
    @SerializedName("AssociationType")
    private String AssociationType;

    @ColumnInfo(name = "FullName")
    @SerializedName("FullName")
    private String FullName;

    @ColumnInfo(name = "IsDisLike")
    @SerializedName("IsDisLike")
    private String IsDisLike;

    @ColumnInfo(name = "IsLike")
    @SerializedName("IsLike")
    private String IsLike;

    @ColumnInfo(name = "ProfilePicture")
    @SerializedName("ProfilePicture")
    private String ProfilePicture;

    @ColumnInfo(name = "Photo")
    @SerializedName("Photo")
    private String Photo;

    @ColumnInfo(name = "TempDate")
    @SerializedName("TempDate")
    private String TempDate;

    @ColumnInfo(name = "PostDate")
    @SerializedName("PostDate")
    private String PostDate;

    @ColumnInfo(name = "PostCount")
    @SerializedName("PostCount")
    private String PostCount;

    @ColumnInfo(name = "PostUrls")
    @SerializedName("PostUrls")
    private String PostUrls;

    @ColumnInfo(name = "WallTypeTerm")
    @SerializedName("WallTypeTerm")
    private String WallTypeTerm;

    @ColumnInfo(name = "FileType")
    @SerializedName("FileType")
    private String FileType;

    @ColumnInfo(name = "PostedOn")
    @SerializedName("PostedOn")
    private String PostedOn;

    @ColumnInfo(name = "FileMimeType")
    @SerializedName("FileMimeType")
    private String FileMimeType;

    @ColumnInfo(name = "SendToMemberID")
    @SerializedName("SendToMemberID")
    private String SendToMemberID;

    @ColumnInfo(name = "PhotoCount")
    @SerializedName("PhotoCount")
    private String PhotoCount;

    @ColumnInfo(name = "PhotoUrls")
    @SerializedName("PhotoUrls")
    private String PhotoUrls;

    @ColumnInfo(name = "PostName")
    @SerializedName("PostName")
    private String PostName;

    @ColumnInfo(name = "AlbumPhotoID")
    @SerializedName("AlbumPhotoID")
    private String AlbumPhotoID;

    @ColumnInfo(name = "isUserComment")
    @SerializedName("isUserComment")
    private boolean isUserComment;

    @ColumnInfo(name = "isUserShare")
    @SerializedName("isUserShare")
    private boolean isUserShare;

    @ColumnInfo(name = "isUserLike")
    @SerializedName("isUserLike")
    private boolean isUserLike;

    @ColumnInfo(name = "isUserDisLike")
    @SerializedName("isUserDisLike")
    private boolean isUserDisLike;

    @ColumnInfo(name = "IsAllowLikeDislike")
    @SerializedName("IsAllowLikeDislike")
    private String IsAllowLikeDislike;

    @ColumnInfo(name = "IsAllowPostComment")
    @SerializedName("IsAllowPostComment")
    private String IsAllowPostComment;

    @ColumnInfo(name = "PostComment")
    @SerializedName("PostComment")
    private String PostComment;

    @ColumnInfo(name = "IsAllowSharePost")
    @SerializedName("IsAllowSharePost")
    private String IsAllowSharePost;

    @ColumnInfo(name = "IsAllowPeopleToPostCommentOnPostWall")
    @SerializedName("IsAllowPeopleToPostCommentOnPostWall")
    private String IsAllowPeopleToPostCommentOnPostWall;

    @ColumnInfo(name = "IsAllowPeopleToShareYourPost")
    @SerializedName("IsAllowPeopleToShareYourPost")
    private String IsAllowPeopleToShareYourPost;

    @ColumnInfo(name = "IsAllowPeopleToLikeOrDislikeOnYourPost")
    @SerializedName("IsAllowPeopleToLikeOrDislikeOnYourPost")
    private String IsAllowPeopleToLikeOrDislikeOnYourPost;

    @ColumnInfo(name = "IsAllowPeopleToPostMessageOnYourWall")
    @SerializedName("IsAllowPeopleToPostMessageOnYourWall")
    private String IsAllowPeopleToPostMessageOnYourWall;

    @ColumnInfo(name = "IsAllowPeopleToLikeAndDislikeCommentWall")
    @SerializedName("IsAllowPeopleToLikeAndDislikeCommentWall")
    private String IsAllowPeopleToLikeAndDislikeCommentWall;

    @ColumnInfo(name = "IsAllowPeopleToShareCommentWall")
    @SerializedName("IsAllowPeopleToShareCommentWall")
    private String IsAllowPeopleToShareCommentWall;

    @ColumnInfo(name = "IsAllowPeoplePostCommentWall")
    @SerializedName("IsAllowPeoplePostCommentWall")
    private String IsAllowPeoplePostCommentWall;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRowNo() {
        return RowNo;
    }

    public void setRowNo(String rowNo) {
        RowNo = rowNo;
    }

    public String getPostCommentID() {
        return PostCommentID;
    }

    public void setPostCommentID(String postCommentID) {
        PostCommentID = postCommentID;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getPostCommentTypesTerm() {
        return PostCommentTypesTerm;
    }

    public void setPostCommentTypesTerm(String postCommentTypesTerm) {
        PostCommentTypesTerm = postCommentTypesTerm;
    }

    public String getPostCommentNote() {
        return PostCommentNote;
    }

    public void setPostCommentNote(String postCommentNote) {
        PostCommentNote = postCommentNote;
    }

    public String getTotalLikes() {
        return TotalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        TotalLikes = totalLikes;
    }

    public String getTotalComments() {
        return TotalComments;
    }

    public void setTotalComments(String totalComments) {
        TotalComments = totalComments;
    }

    public String getTotalDislike() {
        return TotalDislike;
    }

    public void setTotalDislike(String totalDislike) {
        TotalDislike = totalDislike;
    }

    public String getDateOfPost() {
        return DateOfPost;
    }

    public void setDateOfPost(String dateOfPost) {
        DateOfPost = dateOfPost;
    }

    public String getAssociationID() {
        return AssociationID;
    }

    public void setAssociationID(String associationID) {
        AssociationID = associationID;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getIsDisLike() {
        return IsDisLike;
    }

    public void setIsDisLike(String isDisLike) {
        IsDisLike = isDisLike;
    }

    public String getIsLike() {
        return IsLike;
    }

    public void setIsLike(String isLike) {
        IsLike = isLike;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getTempDate() {
        return TempDate;
    }

    public void setTempDate(String tempDate) {
        TempDate = tempDate;
    }

    public String getPostDate() {
        return PostDate;
    }

    public void setPostDate(String postDate) {
        PostDate = postDate;
    }

    public String getPostCount() {
        return PostCount;
    }

    public void setPostCount(String postCount) {
        PostCount = postCount;
    }

    public String getPostUrls() {
        return PostUrls;
    }

    public void setPostUrls(String postUrls) {
        PostUrls = postUrls;
    }

    public String getWallTypeTerm() {
        return WallTypeTerm;
    }

    public void setWallTypeTerm(String wallTypeTerm) {
        WallTypeTerm = wallTypeTerm;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getPostedOn() {
        return PostedOn;
    }

    public void setPostedOn(String postedOn) {
        PostedOn = postedOn;
    }

    public String getFileMimeType() {
        return FileMimeType;
    }

    public void setFileMimeType(String fileMimeType) {
        FileMimeType = fileMimeType;
    }

    public String getSendToMemberID() {
        return SendToMemberID;
    }

    public void setSendToMemberID(String sendToMemberID) {
        SendToMemberID = sendToMemberID;
    }

    public String getPhotoCount() {
        return PhotoCount;
    }

    public void setPhotoCount(String photoCount) {
        PhotoCount = photoCount;
    }

    public String getPhotoUrls() {
        return PhotoUrls;
    }

    public void setPhotoUrls(String photoUrls) {
        PhotoUrls = photoUrls;
    }

    public String getPostName() {
        return PostName;
    }

    public void setPostName(String postName) {
        PostName = postName;
    }

    public String getAlbumPhotoID() {
        return AlbumPhotoID;
    }

    public void setAlbumPhotoID(String albumPhotoID) {
        AlbumPhotoID = albumPhotoID;
    }

    public boolean isUserComment() {
        return isUserComment;
    }

    public void setUserComment(boolean userComment) {
        isUserComment = userComment;
    }

    public boolean isUserShare() {
        return isUserShare;
    }

    public void setUserShare(boolean userShare) {
        isUserShare = userShare;
    }

    public boolean isUserLike() {
        return isUserLike;
    }

    public void setUserLike(boolean userLike) {
        isUserLike = userLike;
    }

    public boolean isUserDisLike() {
        return isUserDisLike;
    }

    public void setUserDisLike(boolean userDisLike) {
        isUserDisLike = userDisLike;
    }

    public String getIsAllowLikeDislike() {
        return IsAllowLikeDislike;
    }

    public void setIsAllowLikeDislike(String isAllowLikeDislike) {
        IsAllowLikeDislike = isAllowLikeDislike;
    }

    public String getIsAllowPostComment() {
        return IsAllowPostComment;
    }

    public void setIsAllowPostComment(String isAllowPostComment) {
        IsAllowPostComment = isAllowPostComment;
    }

    public String getPostComment() {
        return PostComment;
    }

    public void setPostComment(String postComment) {
        PostComment = postComment;
    }

    public String getIsAllowSharePost() {
        return IsAllowSharePost;
    }

    public void setIsAllowSharePost(String isAllowSharePost) {
        IsAllowSharePost = isAllowSharePost;
    }

    public String getIsAllowPeopleToPostCommentOnPostWall() {
        return IsAllowPeopleToPostCommentOnPostWall;
    }

    public void setIsAllowPeopleToPostCommentOnPostWall(String isAllowPeopleToPostCommentOnPostWall) {
        IsAllowPeopleToPostCommentOnPostWall = isAllowPeopleToPostCommentOnPostWall;
    }

    public String getIsAllowPeopleToShareYourPost() {
        return IsAllowPeopleToShareYourPost;
    }

    public void setIsAllowPeopleToShareYourPost(String isAllowPeopleToShareYourPost) {
        IsAllowPeopleToShareYourPost = isAllowPeopleToShareYourPost;
    }

    public String getIsAllowPeopleToLikeOrDislikeOnYourPost() {
        return IsAllowPeopleToLikeOrDislikeOnYourPost;
    }

    public void setIsAllowPeopleToLikeOrDislikeOnYourPost(String isAllowPeopleToLikeOrDislikeOnYourPost) {
        IsAllowPeopleToLikeOrDislikeOnYourPost = isAllowPeopleToLikeOrDislikeOnYourPost;
    }

    public String getIsAllowPeopleToPostMessageOnYourWall() {
        return IsAllowPeopleToPostMessageOnYourWall;
    }

    public void setIsAllowPeopleToPostMessageOnYourWall(String isAllowPeopleToPostMessageOnYourWall) {
        IsAllowPeopleToPostMessageOnYourWall = isAllowPeopleToPostMessageOnYourWall;
    }

    public String getIsAllowPeopleToLikeAndDislikeCommentWall() {
        return IsAllowPeopleToLikeAndDislikeCommentWall;
    }

    public void setIsAllowPeopleToLikeAndDislikeCommentWall(String isAllowPeopleToLikeAndDislikeCommentWall) {
        IsAllowPeopleToLikeAndDislikeCommentWall = isAllowPeopleToLikeAndDislikeCommentWall;
    }

    public String getIsAllowPeopleToShareCommentWall() {
        return IsAllowPeopleToShareCommentWall;
    }

    public void setIsAllowPeopleToShareCommentWall(String isAllowPeopleToShareCommentWall) {
        IsAllowPeopleToShareCommentWall = isAllowPeopleToShareCommentWall;
    }

    public String getIsAllowPeoplePostCommentWall() {
        return IsAllowPeoplePostCommentWall;
    }

    public void setIsAllowPeoplePostCommentWall(String isAllowPeoplePostCommentWall) {
        IsAllowPeoplePostCommentWall = isAllowPeoplePostCommentWall;
    }

    @Override
    public String toString() {
        return "GenerallWallData{" +
                "id=" + id +
                ", RowNo='" + RowNo + '\'' +
                ", PostCommentID='" + PostCommentID + '\'' +
                ", WallID='" + WallID + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", PostCommentTypesTerm='" + PostCommentTypesTerm + '\'' +
                ", PostCommentNote='" + PostCommentNote + '\'' +
                ", TotalLikes='" + TotalLikes + '\'' +
                ", TotalComments='" + TotalComments + '\'' +
                ", TotalDislike='" + TotalDislike + '\'' +
                ", DateOfPost='" + DateOfPost + '\'' +
                ", AssociationID='" + AssociationID + '\'' +
                ", AssociationType='" + AssociationType + '\'' +
                ", FullName='" + FullName + '\'' +
                ", IsDisLike='" + IsDisLike + '\'' +
                ", IsLike='" + IsLike + '\'' +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", Photo='" + Photo + '\'' +
                ", TempDate='" + TempDate + '\'' +
                ", PostDate='" + PostDate + '\'' +
                ", PostCount='" + PostCount + '\'' +
                ", PostUrls='" + PostUrls + '\'' +
                ", WallTypeTerm='" + WallTypeTerm + '\'' +
                ", FileType='" + FileType + '\'' +
                ", PostedOn='" + PostedOn + '\'' +
                ", FileMimeType='" + FileMimeType + '\'' +
                ", SendToMemberID='" + SendToMemberID + '\'' +
                ", PhotoCount='" + PhotoCount + '\'' +
                ", PhotoUrls='" + PhotoUrls + '\'' +
                ", PostName='" + PostName + '\'' +
                ", AlbumPhotoID='" + AlbumPhotoID + '\'' +
                ", isUserComment=" + isUserComment +
                ", isUserShare=" + isUserShare +
                ", isUserLike=" + isUserLike +
                ", isUserDisLike=" + isUserDisLike +
                ", IsAllowLikeDislike='" + IsAllowLikeDislike + '\'' +
                ", IsAllowPostComment='" + IsAllowPostComment + '\'' +
                ", PostComment='" + PostComment + '\'' +
                ", IsAllowSharePost='" + IsAllowSharePost + '\'' +
                ", IsAllowPeopleToPostCommentOnPostWall='" + IsAllowPeopleToPostCommentOnPostWall + '\'' +
                ", IsAllowPeopleToShareYourPost='" + IsAllowPeopleToShareYourPost + '\'' +
                ", IsAllowPeopleToLikeOrDislikeOnYourPost='" + IsAllowPeopleToLikeOrDislikeOnYourPost + '\'' +
                ", IsAllowPeopleToPostMessageOnYourWall='" + IsAllowPeopleToPostMessageOnYourWall + '\'' +
                ", IsAllowPeopleToLikeAndDislikeCommentWall='" + IsAllowPeopleToLikeAndDislikeCommentWall + '\'' +
                ", IsAllowPeopleToShareCommentWall='" + IsAllowPeopleToShareCommentWall + '\'' +
                ", IsAllowPeoplePostCommentWall='" + IsAllowPeoplePostCommentWall + '\'' +
                '}';
    }
}
