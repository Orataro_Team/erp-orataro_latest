package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;

public class ViewImpNotesDetailActivity extends Activity implements
        OnClickListener {
    Context mContext;
    ImageView img_back, img_profile;
    TextView txtheader;
    TextView txtPostBy;
    TextView txtPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewimpnotes);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ViewImpNotesDetailActivity.this;
        img_back = (ImageView) findViewById(R.id.img_back);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        txtheader = (TextView) findViewById(R.id.txt_sub_name);
        txtPostBy = (TextView) findViewById(R.id.txt_techerName);
        txtPost = (TextView) findViewById(R.id.txt_hw_details);

        if (getIntent() != null) {
            String name = getIntent().getStringExtra("name");
            String post = getIntent().getStringExtra("post");
            String post_img = getIntent().getStringExtra("post_img");

            txtheader.setText(name);
            txtPost.setText(post);
            txtPostBy.setText(name);
            img_profile.setImageBitmap(Utility.getBitmapFromAsset(mContext,
                    post_img));
        }

        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            default:
                break;
        }
    }

}