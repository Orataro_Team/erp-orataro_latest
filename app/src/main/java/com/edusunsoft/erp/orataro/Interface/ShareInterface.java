package com.edusunsoft.erp.orataro.Interface;

import com.edusunsoft.erp.orataro.model.WallPostModel;

public interface ShareInterface {

	public void shrePublic(WallPostModel wallPostModel, int position);

	public void shreOnlyMe(WallPostModel wallPostModel, int position);

	public void shreFriend(WallPostModel wallPostModel, int position);

	public void shreSpecialFriend(WallPostModel wallPostModel, int position);

}
