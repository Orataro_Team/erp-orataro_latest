package com.edusunsoft.erp.orataro.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.SelectStudentInterface;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.adapter.StudentListAdapter;
import com.edusunsoft.erp.orataro.database.ClassWorkListModel;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.database.HomeWorkListModel;
import com.edusunsoft.erp.orataro.model.CircularModel;
import com.edusunsoft.erp.orataro.model.CircularNoteStudentModel;
import com.edusunsoft.erp.orataro.model.GetProjectTypeModel;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.FileUtils;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.ImageAndroid11;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import id.zelory.compressor.Compressor;

import static com.edusunsoft.erp.orataro.util.Utility.FILE_PROVIDER;
import static com.edusunsoft.erp.orataro.util.Utility.showToast;

public class AddDataByTeacherActivity extends AppCompatActivity implements OnClickListener, OnItemSelectedListener, ResponseWebServices, SelectStudentInterface {
    private boolean isCircular, isHomework, isClassWork;
    private ImageView imgLeftHeader, imgRightHeader, iv_attachment, img_speack, img_speack_description;
    private TextView txt_header;
    private EditText edt_title, edt_detail, edt_referanceLink;
    private TextView edt_Enddate, edt_startDate, edt_startTime, edt_endTime;
    private Calendar c;
    private int myear;
    private String fielPath = "";
    private byte[] byteArray;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private int month;
    private int day, mHour, mMinute;
    static final int DATE_DIALOG_ID = 111;
    static final int STARTDATE_DIALOG_ID = 115;
    protected static final int REQUEST_PATH = 112;
    private LinearLayout layout_time;
    private String from;
    private String gradeId, divisionId, subjectId;
    private String todayDate, subjectName;
    private LinearLayout ll_save, ll_imgattechment;
    private Context mContext = AddDataByTeacherActivity.this;
    private Spinner spn_circulartype;
    private boolean isSucess;
    private String circularTypeStr = "";
    private boolean isEdit;
    protected static final int RESULT_SPEECH = 121, RESULT_SPEECH_DESCRIPTION = 122, READ_EXTERNAL_REQUEST_CODE = 110, READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY = 120, READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA = 130;
    private ClassWorkListModel classModel;
    private HomeWorkListModel homeworkModel;
    private CircularModel circularModel;
    private Uri fileUri;
    private boolean isStandard = true;
    private String gradeIdStr = "", divisionIdStr = "";
    private TextView edt_gradedivisionId;
    private String curFileName = "";
    private String gradeDivisionIdStr = "";
    private String gradeDivisionWallIdStr = "";
    private ArrayList<GetProjectTypeModel> getProjectTypes;
    private int fileType = 0;
    private String FileMineType = "";
    private TextView btn_continue;
    private String[] circularType = {"Circular on Examination", "Circular on Parents Meeting", "Circular on students Support"};

    Bitmap photoBitmap = null;
    ArrayAdapter<String> adapter;
    public boolean DateCompare, TimeCompare;

    public static final String URL_REGEX = "^(https?|s?ftp):\\/\\/(((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:)*@)?(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?)(:\\d*)?)(\\/((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)+(\\/(([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)*)*)?)?(\\?((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|[\\uE000-\\uF8FF]|\\/|\\?)*)?(#((([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|\\/|\\?)*)?$";

    // variable declaration for select student functionality
    TextView txt_select_student;
    ArrayList<CircularNoteStudentModel> CircularStudentList;
    protected ArrayList<CircularNoteStudentModel> jobAllGroupMembers;
    private ArrayList<CircularNoteStudentModel> localAddGroupMemberModels;
    private ArrayList<CircularNoteStudentModel> tempGroupMemberList;
    private StudentListAdapter studentlistAdapter;
    private String groupMemberIdStr = "", groupMemberStudentStr = "", StrSelectedStudent = "";
    ImageView selectall;

    String BASE64_STRING = "", Path = "";
    private String filePath;
    private static final int PICK_FROM_GALLERY = 1117;
    public String mCurrentPhotoPath = "";

    final long MB = 1024 * 1024;
    public String file_size;
    final DecimalFormat format = new DecimalFormat("#.#");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data_by_teacher);
        txt_header = (TextView) findViewById(R.id.header_text);
        imgLeftHeader = (ImageView) findViewById(R.id.img_back);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        layout_time = (LinearLayout) findViewById(R.id.layout_time);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        btn_continue = (TextView) findViewById(R.id.btn_continue);
        edt_title = (EditText) findViewById(R.id.edt_title);
        ll_imgattechment = (LinearLayout) findViewById(R.id.ll_imgattechment);
        iv_attachment = (ImageView) findViewById(R.id.iv_attachment);
        img_speack = (ImageView) findViewById(R.id.img_speack);
        img_speack_description = (ImageView) findViewById(R.id.img_speack_description);
        edt_gradedivisionId = (TextView) findViewById(R.id.edt_gradedivisionId);
        spn_circulartype = (Spinner) findViewById(R.id.spn_circulartype);
        edt_referanceLink = (EditText) findViewById(R.id.edt_referanceLink);
        edt_detail = (EditText) findViewById(R.id.edt_detail);
        edt_startTime = (TextView) findViewById(R.id.edt_startime);
        edt_endTime = (TextView) findViewById(R.id.edt_endtime);
        edt_Enddate = (TextView) findViewById(R.id.edt_endDate);
        edt_startDate = (TextView) findViewById(R.id.edt_startDate);


        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        if (Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext).equalsIgnoreCase("")) {
            getProjectType(true);
        } else {
            parseProjctType(Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext));
            getProjectType(true);
        }

        if (getIntent() != null) {
            gradeId = getIntent().getStringExtra("gradeId");
            divisionId = getIntent().getStringExtra("divisionId");
            subjectId = getIntent().getStringExtra("subjectId");
            from = getIntent().getStringExtra("isFrom");
            subjectName = getIntent().getStringExtra("subjectName");
            isEdit = getIntent().getBooleanExtra("isEdit", false);
        }

        try {

            if (from.equalsIgnoreCase(ServiceResource.CLASSWORK_FLAG)) {

                isClassWork = true;
                btn_continue.setText("POST");
                txt_header.setText(mContext.getResources().getString(R.string.AddClassWork) + " (" + Utility.GetFirstName(mContext) + ")");
                edt_referanceLink.setVisibility(View.VISIBLE);
                spn_circulartype.setVisibility(View.GONE);
                edt_Enddate.setVisibility(View.GONE);
                layout_time.setVisibility(View.VISIBLE);

                if (isEdit) {

                    txt_header.setText(mContext.getResources().getString(R.string.EditClassWork) + " (" + Utility.GetFirstName(mContext) + ")");
                    ll_imgattechment.setVisibility(View.GONE);
                    classModel = new ClassWorkListModel();
                    classModel = (ClassWorkListModel) getIntent().getSerializableExtra("ClassworkModel");
                    fillClassDate();

                }

            } else if (from.equalsIgnoreCase(ServiceResource.CIRCULAR_FLAG)) {

                Date c = Calendar.getInstance().getTime();
                System.out.println("Current time => " + c);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                String formattedDate = df.format(c);
                edt_Enddate.setText(formattedDate);

                isCircular = true;
                btn_continue.setText("POST");
                txt_header.setText(mContext.getResources().getString(R.string.AddCircular) + " (" + Utility.GetFirstName(mContext) + ")");
                edt_gradedivisionId.setVisibility(View.VISIBLE);
                if (Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext).equalsIgnoreCase("")) {
                    getStandarDivisionList(true);
                } else {
                    getStandarDivisionList(false);
                    parseStdDivSub(Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext));
                }

                spn_circulartype.setVisibility(View.VISIBLE);
                edt_referanceLink.setVisibility(View.VISIBLE);
                edt_startDate.setVisibility(View.GONE);
                layout_time.setVisibility(View.GONE);

                if (isEdit) {

                    ll_imgattechment.setVisibility(View.GONE);
                    txt_header.setText(mContext.getResources().getString(R.string.EditCircular) + " (" + Utility.GetFirstName(mContext) + ")");
                    circularModel = new CircularModel();
                    circularModel = (CircularModel) getIntent().getSerializableExtra("CircularModel");
                    fillCircularData();

                }

            } else if (from.equalsIgnoreCase(ServiceResource.HOMEWORK_FLAG)) {

                btn_continue.setText("POST");
                isHomework = true;
                txt_header.setText(mContext.getResources().getString(R.string.AddHomework) + " (" + Utility.GetFirstName(mContext) + ")");
                edt_title.setHint(mContext.getResources().getString(R.string.Title));
                edt_detail.setHint(mContext.getResources().getString(R.string.Description));
                edt_referanceLink.setVisibility(View.VISIBLE);
                layout_time.setVisibility(View.GONE);
                spn_circulartype.setVisibility(View.GONE);
                layout_time.setVisibility(View.GONE);

                if (isEdit) {

                    txt_header.setText(mContext.getResources().getString(R.string.EditHomework) + " (" + Utility.GetFirstName(mContext) + ")");
                    ll_imgattechment.setVisibility(View.GONE);
                    homeworkModel = new HomeWorkListModel();
                    homeworkModel = (HomeWorkListModel) getIntent().getSerializableExtra("HomeworkModel");
                    Log.d("homrwork", homeworkModel.toString());
                    fillHomeworkDate();

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        todayDate = (day < 10 ? "0" + day : day)
                + "-"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "-" + myear;

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);
        imgLeftHeader.setOnClickListener(this);
        edt_startDate.setOnClickListener(this);
        edt_Enddate.setOnClickListener(this);
        edt_startTime.setOnClickListener(this);
        edt_endTime.setOnClickListener(this);
        iv_attachment.setOnClickListener(this);
        img_speack.setOnClickListener(this);
        img_speack_description.setOnClickListener(this);

        edt_gradedivisionId.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                showStanderdPopup();

            }

        });

        ll_save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isClassWork) {

                    TimeCompare = Utility.CompareTime(mContext, edt_startTime.getText().toString(), edt_endTime.getText().toString());
                    if (!Utility.isNull(edt_title.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.entertitle));
                    } else if (!Utility.isNull(edt_startTime.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.enterstarttime));
                    } else if (!Utility.isNull(edt_endTime.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.enterendtime));
                    } else if (!edt_startTime.getText().toString().trim().isEmpty() && !edt_endTime.getText().toString().isEmpty() && !TimeCompare) {
                        toast(mContext.getResources().getString(R.string.timevalidation));
                    } else if (edt_startTime.getText().toString().compareTo(edt_endTime.getText().toString()) == 0) {
                        toast(mContext.getResources().getString(R.string.equaltimevalidation));
                    } else if (!Utility.isNull(edt_startDate.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.enterstartdate));
                    } else {
                        if (Utility.isTeacher(mContext)) {
                            if (Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsCreate()) {
                                addClasswork();
                            } else {
                                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                            }
                        }
                    }

                } else if (isHomework) {

                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        DateCompare = Utility.CompareDates(df, df.parse(edt_startDate.getText().toString()), df.parse(edt_Enddate.getText().toString()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (!Utility.isNull(edt_title.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.entertitle));
                    } else if (!Utility.isNull(edt_startDate.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.enterstartdate));
                    } else if (!Utility.isNull(edt_Enddate.getText().toString())) {
                        toast(mContext.getResources().getString(R.string.enterenddate));
                    } else if (DateCompare) {
                        toast(mContext.getResources().getString(R.string.datevalidation));
                    } else if (edt_startDate.getText().toString().compareTo(edt_Enddate.getText().toString()) == 0) {
                        toast(mContext.getResources().getString(R.string.equaldatevalidation));
                    } else {
                        if (Utility.isTeacher(mContext)) {
                            if (Utility.ReadWriteSetting(ServiceResource.HOMEWORKSETTING).getIsCreate()) {
                                Log.d("Teacher", "Teacher");
                                addHomework();
                            } else {
                                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                            }
                        }
                    }
                } else if (isCircular) {

                    if (!circularTypeStr.equalsIgnoreCase("")) {

                        if (!Utility.isNull(edt_title.getText().toString())) {
                            toast(mContext.getResources().getString(R.string.entertitle));
                        } else if (edt_gradedivisionId.getText().toString().equalsIgnoreCase("")) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.eventstddivValidation));
                        } else if (!Utility.isNull(edt_Enddate.getText().toString())) {
                            toast(mContext.getResources().getString(R.string.enterenddate));
                        } else {
                            if (Utility.isTeacher(mContext)) {
                                if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsCreate()) {
                                    addCircular();
                                } else {
                                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                                }
                            }
                        }
                    } else {
                        toast(mContext.getResources().getString(R.string.circulartype));
                    }

                }

            }

        });

    }

    private void fillHomeworkDate() {

        Log.d("gethomerokeditlist", homeworkModel.toString());

        if (homeworkModel != null) {

            if (Utility.isNull(homeworkModel.getTitle())) {
                edt_title.setText(homeworkModel.getTitle());
            }

            if (Utility.isNull(homeworkModel.getDateOfFinish())) {
                edt_Enddate.setText(Utility.dateFormate(homeworkModel.getDateOfFinish(), "dd-MM-yyyy", "MM/dd/yyyy"));
            }

            if (Utility.isNull(homeworkModel.getDateOfHomeWork())) {
                edt_startDate.setText(Utility.dateFormate(homeworkModel.getDateOfHomeWork(), "dd-MM-yyyy", "MM/dd/yyyy"));
            }

            if (Utility.isNull(homeworkModel.getHomeWorksDetails())) {
                edt_detail.setText(homeworkModel.getHomeWorksDetails());
            }

            if (Utility.isNull(homeworkModel.getReferenceLink())) {
                edt_referanceLink.setText(homeworkModel.getReferenceLink());
            }

        }

    }

    private void fillCircularData() {

        Log.d("getTypeTerm", circularModel.getTypeTerm());

        if (Utility.isNull(circularModel.getCr_subject())) {
            edt_title.setText(circularModel.getCr_subject());
        }
        if (Utility.isNull(circularModel.getCr_detail_txt())) {
            edt_detail.setText(circularModel.getCr_detail_txt());
        }

        Log.d("getEndDate", Utility.getDate(Long.valueOf(circularModel.getMilliSecond()), "MM-dd-yyyy"));
        if (Utility.isNull(circularModel.getMilliSecond())) {
            edt_Enddate.setText(Utility.getDate(Long.valueOf(circularModel.getMilliSecond()), "dd-MM-yyyy"));
        }

        if (Utility.isNull(circularModel.getReferenceLink())) {
            edt_referanceLink.setText(circularModel.getReferenceLink());
        }

    }

    private void fillClassDate() {

        Log.d("getStartTimeee", classModel.getStartTime());

        if (Utility.isNull(classModel.getClassWorkTitle())) {
            edt_title.setText(classModel.getClassWorkTitle());
        }

        if (Utility.isNull(classModel.getClassWorkDetails())) {
            edt_detail.setText(classModel.getClassWorkDetails());
        }

        if (Utility.isNull(classModel.getStartTime())) {

            //edt_startTime.setText(Utility.dateFormate(classModel.getStartTime(), "HH : mm : a", "hh:mm a"));
            edt_startTime.setText(classModel.getStartTime());

        }

        if (Utility.isNull(classModel.getEndTime())) {
            //edt_endTime.setText(Utility.dateFormate(classModel.getEndTime(), "HH : mm : a", "hh:mm a"));
            edt_endTime.setText(classModel.getEndTime());
        }

        if (Utility.isNull(classModel.getExpectingDateOfCompletion())) {
            edt_Enddate.setText(Utility.dateFormate(classModel.getExpectingDateOfCompletion(), "dd-MM-yyyy", "dd MMM yyyy"));
        }

        if (Utility.isNull(classModel.getDateOfClassWork())) {
            edt_startDate.setText(Utility.dateFormate(classModel.getDateOfClassWork(), "dd-MM-yyyy", "dd MMM yyyy"));
        }

        if (Utility.isNull(classModel.getReferenceLink())) {
            edt_referanceLink.setText(classModel.getReferenceLink());
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        DatePickerDialog datePickerDialog, datePickerDialog1;

        switch (id) {

            case DATE_DIALOG_ID:

                datePickerDialog = new DatePickerDialog(this, datePickerListener,
                        myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 2678400000l);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;

            case STARTDATE_DIALOG_ID:

                datePickerDialog1 = new DatePickerDialog(this, datePickerListener1,
                        myear, month, day);
                datePickerDialog1.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog1.getDatePicker().setMaxDate(System.currentTimeMillis() + 2678400000l);
                datePickerDialog1.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);

                return datePickerDialog1;

        }

        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            edt_startDate.setText((day < 10 ? "0" + day : day)
                    + "-"
                    + ((month + 1) < 10 ? "0" + (month + 1)
                    : (month + 1))
                    + "-" + myear);


        }


    };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            edt_Enddate.setText((day < 10 ? "0" + day : day)
                    + "-"
                    + ((month + 1) < 10 ? "0" + (month + 1)
                    : (month + 1))
                    + "-" + myear);

            Log.d("getSelecteEndDate", edt_Enddate.getText().toString());

        }

    };


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_speack) {

            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
            try {
                startActivityForResult(intent, RESULT_SPEECH);
            } catch (ActivityNotFoundException a) {
                Utility.showToast("Opps! Your device doesn't support Speech to Text", mContext);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                startActivity(browserIntent);
                overridePendingTransition(0, 0);
            }

        }

        if (v.getId() == R.id.img_speack_description) {

            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
            try {
                startActivityForResult(intent, RESULT_SPEECH_DESCRIPTION);
            } catch (ActivityNotFoundException a) {
                Utility.showToast("Opps! Your device doesn't support Speech to Text", mContext);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                startActivity(browserIntent);
                overridePendingTransition(0, 0);
            }
        }

        if (v.getId() == R.id.iv_attachment) {
            selectImage(1);
        }

        if (v.getId() == R.id.img_back) {
            if (isCircular) {
                Utility.ISLOADCIRCULAR = false;
                finish();
            } else if (isHomework) {
                Utility.ISLOADHOMEWORK = false;
                finish();
            } else if (isClassWork) {
                Utility.ISLOADCLASSWORK = false;
                finish();
            }
        }

        if (v.getId() == R.id.edt_startime) {

            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            //Change by Hardik Kanak
                            //String StartTime = Utility.GetTimewithAmPm(mContext, hourOfDay, minute, "");
                            String StartTime = getUpdateTime(hourOfDay, minute);


                            // Get the calling activity TextView reference
                            // Display the 12 hour format time in app interface
                            edt_startTime.setText(StartTime);
//                            edt_startTime.setText(hour_of_12_hour_format + " : " + minute + " : " + status);
//                            edt_startTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            tpd.show();
        }

        if (v.getId() == R.id.edt_endtime) {

            TimePickerDialog tpd = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                            //Change by Hardik Kanak
                            //String StartTime = Utility.GetTimewithAmPm(mContext, hourOfDay, minute, "");
                            String StartTime = getUpdateTime(hourOfDay, minute);

                            // Get the calling activity TextView reference
                            // Display the 12 hour format time in app interface
                            edt_endTime.setText(StartTime);

                            // Get the calling activity TextView reference
                            // Display the 12 hour format time in app interface
//                            edt_endTime.setText(hour_of_12_hour_format + " : " + minute + " : " + status);
//                            edt_endTime.setText(hourOfDay + ":" + minute);
                        }

                    }, mHour, mMinute, false);

            tpd.show();
        }

        if (v.getId() == R.id.edt_endDate) {
            showDialog(DATE_DIALOG_ID);
        }

        if (v.getId() == R.id.edt_startDate) {
            showDialog(STARTDATE_DIALOG_ID);
        }
    }

    private String getUpdateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String finalHours = "";
        if (hours < 10)
            finalHours = "0" + hours;
        else
            finalHours = String.valueOf(hours);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(finalHours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        if (position == 0) {
            circularTypeStr = "";
        } else {
            circularTypeStr = parent.getItemAtPosition(position).toString();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void toast(String msg) {

        Utility.showToast(msg, mContext);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isCircular) {
            Utility.ISLOADCIRCULAR = false;
            finish();
        } else if (isHomework) {
            Utility.ISLOADHOMEWORK = false;
            finish();
        } else if (isClassWork) {
            Utility.ISLOADCLASSWORK = false;
            finish();
        }
    }

    // For Add Classwork webservices call
    public void addClasswork() {

        ContentValues values = new ContentValues();

        if (isEdit) {

            values.put(ServiceResource.EDITID, classModel.getClassWorkID());

        } else {

            values.put(ServiceResource.EDITID, "");

        }

        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {

            values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID());
            values.put(ServiceResource.TITLE, edt_title.getText().toString());
            values.put(ServiceResource.GRADEID, gradeId);
            values.put(ServiceResource.DIVISIONID, divisionId);
            values.put(ServiceResource.SUBJECTID, subjectId);
            values.put(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString());
            values.put(ServiceResource.DATEOFCLASSWORK, Utility.dateFormate(edt_startDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
            values.put(ServiceResource.DATEOFCOMPLETION, Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
//            values.put(ServiceResource.STARTTIME, edt_startTime.getText().toString());
            values.put(ServiceResource.STARTTIME, Utility.dateFormate(edt_startTime.getText().toString(), "HH:mm", "HH : mm : aa"));
            values.put(ServiceResource.ENDTIME, Utility.dateFormate(edt_endTime.getText().toString(), "HH:mm", "HH : mm : aa"));
//            values.put(ServiceResource.ENDTIME, edt_endTime.getText().toString());
            values.put(ServiceResource.CLASSWORKDETAIL, edt_detail.getText().toString());
            values.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            values.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            values.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            values.put(ServiceResource.BEATCH_ID, "");

            if (Utility.BASE64_STRING != null) {
                Log.d("getfilename", new File(Utility.NewFileName).getName());
                if (fileType == 0) {
                    values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                    values.put(ServiceResource.FILENAME, new File(Utility.NewFileName).getName());
                    values.put(ServiceResource.FILETYPE, "IMAGE");
                    values.put(ServiceResource.FILEMINETYPE, FileMineType);
                } else if (fileType == 2) {
                    Log.d("curlfilename", curFileName);
                    values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                    values.put(ServiceResource.FILENAME, curFileName);
                    values.put(ServiceResource.FILETYPE, "FILE");
                    values.put(ServiceResource.FILEMINETYPE, FileMineType);
                }
            } else {
                values.put(ServiceResource.FILE, "");
                values.put(ServiceResource.FILENAME, "");
                values.put(ServiceResource.FILETYPE, "");
                values.put(ServiceResource.FILEMINETYPE, "");
            }

            ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
            if (isEdit) {
                arrayList.add(new PropertyVo(ServiceResource.EDITID, classModel.getClassWorkID()));
            } else {
                arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
            }

            if (new UserSharedPrefrence(mContext).getLoginModel() != null) {

                arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
                arrayList.add(new PropertyVo(ServiceResource.TITLE, edt_title.getText().toString()));
                arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
                arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, divisionId));
                arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, subjectId));
                arrayList.add(new PropertyVo(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString()));
                arrayList.add(new PropertyVo(ServiceResource.DATEOFCLASSWORK,
                        Utility.dateFormate(edt_startDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
                arrayList.add(new PropertyVo(ServiceResource.DATEOFCOMPLETION,
                        Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
                arrayList.add(new PropertyVo(ServiceResource.DATEOFCLASSWORK,
                        Utility.dateFormate(edt_startDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
                arrayList.add(new PropertyVo(ServiceResource.STARTTIME, Utility.dateFormate(edt_startTime.getText().toString(), "HH:mm", "hh:mm a")));
                arrayList.add(new PropertyVo(ServiceResource.ENDTIME, Utility.dateFormate(edt_endTime.getText().toString(), "HH:mm", "hh:mm a")));
                arrayList.add(new PropertyVo(ServiceResource.CLASSWORKDETAIL, edt_detail.getText().toString()));
                arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
                arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
                arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
                arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
                arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
                if (Utility.BASE64_STRING != null) {
                    if (fileType == 0) {
                        arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                        arrayList.add(new PropertyVo(ServiceResource.FILENAME, new File(Utility.NewFileName).getName()));
                        arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "IMAGE"));
                        arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
                    } else if (fileType == 2) {
                        Log.d("curlfilename", curFileName);
                        arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                        arrayList.add(new PropertyVo(ServiceResource.FILENAME, curFileName));
                        arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "FILE"));
                        arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
                    }
                } else {
                    arrayList.add(new PropertyVo(ServiceResource.FILE, null));
                    arrayList.add(new PropertyVo(ServiceResource.FILENAME, null));
                    arrayList.add(new PropertyVo(ServiceResource.FILETYPE, null));
                    arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, null));
                }

                Log.d("clasworkRequest", arrayList.toString());

                if (Utility.isNetworkAvailable(mContext)) {
                    new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.ADDCLASSWORK_METHODNAME, ServiceResource.ADDCLASSWORK);
                } else {
                    finish();
                }

            } else {

                Utility.showToast(getResources().getString(R.string.PleaseTryAgainLater), mContext);

            }

        }

    }

    public void addHomework() {

        ContentValues values = new ContentValues();

        if (isEdit) {
            values.put(ServiceResource.EDITID, homeworkModel.getAssignmentID());
        } else {
            values.put(ServiceResource.EDITID, "");
        }

        values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID());
        values.put(ServiceResource.TITLE, edt_title.getText().toString());
        values.put(ServiceResource.GRADEID, gradeId);
        values.put(ServiceResource.DIVISIONID, divisionId);
        values.put(ServiceResource.SUBJECTID, subjectId);
        values.put(ServiceResource.DATEOFHOMEWORK, Utility.dateFormate(edt_startDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        values.put(ServiceResource.DATEOFFINISH, Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        values.put(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString());
        values.put(ServiceResource.HOMEWORKDETAIL, edt_detail.getText().toString());
        values.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.BEATCH_ID, "");

        if (Utility.BASE64_STRING != null) {
            Log.d("getfilename", new File(Utility.NewFileName).getName());
            if (fileType == 0) {
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, new File(Utility.NewFileName).getName());
                values.put(ServiceResource.FILETYPE, "IMAGE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, curFileName);
                values.put(ServiceResource.FILETYPE, "FILE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            }
        } else {
            values.put(ServiceResource.FILE, "");
            values.put(ServiceResource.FILENAME, "");
            values.put(ServiceResource.FILETYPE, "");
            values.put(ServiceResource.FILEMINETYPE, "");
        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, homeworkModel.getAssignmentID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
        }

        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.TITLE, edt_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, divisionId));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, subjectId));
        arrayList.add(new PropertyVo(ServiceResource.DATEOFHOMEWORK, Utility.dateFormate(edt_startDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.DATEOFFINISH, Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HOMEWORKDETAIL, edt_detail.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        if (Utility.BASE64_STRING != null) {
            if (fileType == 0) {
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, new File(Utility.NewFileName).getName()));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "IMAGE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, curFileName));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "FILE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            }
        } else {
            arrayList.add(new PropertyVo(ServiceResource.FILE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME, null));
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, null));
        }

        Log.d("homeworkrequest", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.ADDHOMEWORK_METHODNAME,
                    ServiceResource.ADDHOMEWORK);
        } else {
            finish();
        }

    }

    // For Add Circular webservices call
    public void addCircular() {

//        Log.d("getEndDateValue", Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        Log.d("getEndDateValue", edt_Enddate.getText().toString());

        ContentValues values = new ContentValues();
        if (isEdit) {
            values.put(ServiceResource.EDITID, circularModel.getCircularID());
        } else {
            values.put(ServiceResource.EDITID, "");
        }

        Log.d("getselectedstudenttr", StrSelectedStudent);
        values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID());
        values.put(ServiceResource.TITLE, edt_title.getText().toString());
        values.put(ServiceResource.GRADEDIVISOINID, StrSelectedStudent);
        values.put(ServiceResource.GRADEDIVISOINWALLID, gradeDivisionWallIdStr);
//        values.put(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr);
        values.put(ServiceResource.CIRCULAR_TYPE, circularTypeStr);
        values.put(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString());
        values.put(ServiceResource.DATEOFCIRCULAR, Utility.dateFormate(edt_Enddate.getText().toString(), "dd-MMM-yyyy", "dd-MM-yyyy"));
        values.put(ServiceResource.CIRCULAR_DETAIL, edt_detail.getText().toString());
        values.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.BEATCH_ID, "");


        if (Utility.BASE64_STRING != null) {
            Log.d("getfilename", new File(Utility.NewFileName).getName());
            if (fileType == 0) {
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, new File(Utility.NewFileName).getName());
                values.put(ServiceResource.FILETYPE, "IMAGE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                values.put(ServiceResource.FILE, Utility.BASE64_STRING);
                values.put(ServiceResource.FILENAME, curFileName);
                values.put(ServiceResource.FILETYPE, "FILE");
                values.put(ServiceResource.FILEMINETYPE, FileMineType);
            }
        } else {
            values.put(ServiceResource.FILE, "");
            values.put(ServiceResource.FILENAME, "");
            values.put(ServiceResource.FILETYPE, "");
            values.put(ServiceResource.FILEMINETYPE, "");
        }

        values.put(ServiceResource.INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, circularModel.getCircularID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.TITLE, edt_title.getText().toString()));
//        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr));
        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINID, StrSelectedStudent));
        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINWALLID, gradeDivisionWallIdStr));
        arrayList.add(new PropertyVo(ServiceResource.DATEOFCIRCULAR, Utility.dateFormate(edt_Enddate.getText().toString(), "dd-MMM-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.CIRCULAR_TYPE, circularTypeStr));
//        arrayList.add(new PropertyVo(ServiceResource.DATEOFCIRCULAR, Utility.dateFormate(edt_Enddate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.REFERANCELINK, edt_referanceLink.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.CIRCULAR_DETAIL, edt_detail.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId()));

        if (Utility.BASE64_STRING != null) {
            if (fileType == 0) {
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, new File(Utility.NewFileName).getName()));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "IMAGE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            } else if (fileType == 2) {
                Log.d("curlfilename", curFileName);
                arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME, curFileName));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE, "FILE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, FileMineType));
            }
        } else {
            arrayList.add(new PropertyVo(ServiceResource.FILE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME, null));
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE, null));
        }

        Log.d("requestcircular", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.CREATECIRCULARWITHMULTY_METHODNAME, ServiceResource.CIRCULAR_URL);
        } else {
            finish();
        }

    }

    public void getProjectType(boolean isPopup) {

        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY, "CircularType"));
        new AsynsTaskClass(mContext, arrayList, isPopup, this, true).execute(ServiceResource.LOGIN_GETPROJECTTYPE, ServiceResource.LOGIN_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, ServiceResource.LOGIN_GETPROJECTTYPE, mContext);
            parseProjctType(result);
        }

        if (ServiceResource.ADDCLASSWORK_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result.contains("\"success\":0")) {
                isSucess = true;
            }

            if (isSucess) {
                Utility.ISLOADCLASSWORK = true;
                Utility.BASE64_STRING = "";
                Utility.sendPushNotification(mContext);
                toast(mContext.getResources().getString(R.string.classworksuccessmessage));
                finish();
            }

        }

        if (ServiceResource.CREATECIRCULARWITHMULTY_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result.contains("\"success\":0")) {
                isSucess = true;
            }

            if (isSucess) {

                Utility.ISLOADCIRCULAR = true;
                Utility.BASE64_STRING = "";
                Utility.sendPushNotification(mContext);
                Log.d("circular result", result);
                toast(mContext.getResources().getString(R.string.circularsuccessmessage));
                finish();

            }

        }

        if (ServiceResource.ADDHOMEWORK_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result.contains("\"success\":0")) {
                isSucess = true;
            }

            if (isSucess) {

                Utility.ISLOADHOMEWORK = true;
                Utility.BASE64_STRING = "";
                Utility.sendPushNotification(mContext);
                toast(mContext.getResources().getString(R.string.homeworksuccessmessage));
                finish();

            }


        }

        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext);
            parseStdDivSub(result);

        }

        if (ServiceResource.GETSTUDENTLIST.equals(methodName)) {
            Log.d("getstudlist", result);
            try {

                JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
                CircularStudentList = new ArrayList<CircularNoteStudentModel>();
                CircularNoteStudentModel model;

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    model = new CircularNoteStudentModel();
                    model.setMemberID(hJsonObject.getString(ServiceResource.STUDENT_MEMBERID));
                    model.setFullName(hJsonObject.getString(ServiceResource.STUDENT_FULLNAME));
                    model.setRegistrationNo(hJsonObject.getString(ServiceResource.STUDENT_REGNO));
                    model.setGradeName(hJsonObject.getString(ServiceResource.STUDENT_GRADENAME));
                    model.setDivisionName(hJsonObject.getString(ServiceResource.STUDENT_DIVISIONNAME));
                    model.setGradeID(hJsonObject.getString(ServiceResource.STUDENT_GRADEID));
                    model.setDivisionID(hJsonObject.getString(ServiceResource.STUDENT_DIVID));
                    model.setFName(hJsonObject.getString(ServiceResource.STUDENT_FNAME));
                    model.setFContactNo(hJsonObject.getString(ServiceResource.STUDENT_FCONTACTNO));
                    CircularStudentList.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            showListDialogToShowStudentlist(mContext, CircularStudentList);

        }

    }

    private void showListDialogToShowStudentlist(Context mContext, ArrayList<CircularNoteStudentModel> circularStudentList) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customelistdialog);

        TextView dialogTitle = (TextView) dialog
                .findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialog.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        final ListView lv = (ListView) dialog.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialog
                .findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialog
                .findViewById(R.id.custome_Dialog_cancle_btn);

        selectall = (ImageView) dialog
                .findViewById(R.id.selectall);
        selectall.setVisibility(View.VISIBLE);

        edtSearch.setHint(mContext.getResources().getString(R.string.Search));
        dialogTitle.setText(mContext.getResources().getString(R.string.SelectStudent));

        if (circularStudentList != null && circularStudentList.size() > 0) {
            studentlistAdapter = new StudentListAdapter(mContext, circularStudentList, AddDataByTeacherActivity.this);
            lv.setAdapter(studentlistAdapter);
            studentlistAdapter.notifyDataSetChanged();
        }

        // select all functionality to select All Student simultaneously
        selectall.setOnClickListener(v -> {

            studentlistAdapter.SelectAll(circularStudentList, selectall);

        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (studentlistAdapter != null) {
                    studentlistAdapter.filter(text);
                }
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<CircularNoteStudentModel> selectedGroupMemberList = new ArrayList<CircularNoteStudentModel>();
                selectedGroupMemberList = getSelectedStudentList(jobAllGroupMembers);
                tempGroupMemberList = getSelectedStudentList(jobAllGroupMembers);
//                isFirstTimeGroupMember = false;
                String selectedGroupMemberStr = "";
                groupMemberStudentStr = "";
                if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                    Collections.sort(selectedGroupMemberList, new CustomComparator());
                    for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                        selectedGroupMemberStr = selectedGroupMemberStr
                                + selectedGroupMemberList.get(i).getFullName();
                        groupMemberStudentStr = groupMemberStudentStr +
                                selectedGroupMemberList.get(i).getMemberID();
                        if (selectedGroupMemberList.size() > 1) {
                            if (i != selectedGroupMemberList.size() - 1) {
                                selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                groupMemberStudentStr = groupMemberStudentStr + ",";
                            }
                        }

                        String tempStr = selectedGroupMemberList.get(i).getGradeID() + ","
                                + selectedGroupMemberList.get(i).getDivisionID() + ","
                                + selectedGroupMemberList.get(i).getMemberID() + "#";

                        StrSelectedStudent = StrSelectedStudent + tempStr;

                    }

                }

                Log.d("selectedStudentstr", selectedGroupMemberStr);
                Log.d("selectedStudentstr1233", StrSelectedStudent);
                edt_gradedivisionId.setText(selectedGroupMemberStr);
                dialog.dismiss();

            }
        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();

    }

    public class CustomComparator implements Comparator<CircularNoteStudentModel> {
        @Override
        public int compare(CircularNoteStudentModel o1, CircularNoteStudentModel o2) {
            return o1.getFullName().compareTo(o2.getFullName());
        }
    }

    /**
     * get selected studentlist
     *
     * @param addGroupMemberModels
     * @return
     */
    public ArrayList<CircularNoteStudentModel> getSelectedStudentList(
            ArrayList<CircularNoteStudentModel> addGroupMemberModels) {
        ArrayList<CircularNoteStudentModel> groupMemberModels = null;
        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<CircularNoteStudentModel>();
            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                if (addGroupMemberModels.get(i).isSelected()) {
                    groupMemberModels.add(addGroupMemberModels.get(i));
                }
            }
        }
        return groupMemberModels;
    }

    // select image from camera and gallary
    private void selectImage(final int i) {

        final CharSequence[] items = {this.getResources().getString(R.string.takephoto),
                this.getResources().getString(R.string.ChoosefromLibrary),
                this.getResources().getString(R.string.File),
                this.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(this.getResources().getString(R.string.AddPhoto));

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {



                    /*commented By Hardik : 29-04-2019 - Android 11 image capture*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        try {
                            startActivityForResult(takePictureIntent, ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11);
                        } catch (ActivityNotFoundException e) {
                            Log.e("PATH", "PATH:: " + e.toString());
                        }
                    } else {
                        Intent intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);

                    }


//                    if (isPermissionGrantedForCamera()) {
//                        OpenCameraApp();
//                    }

                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {



                    /*commented By Hardik : 29-04-2019 - Android 11 image selection*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11);
                    } else {
                        Intent intent = new Intent(mContext, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILE);
                    }

//                    if (isPermissionGrantedForGallery()) {
//                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
//                    }

                } else if (items[item].equals(mContext.getResources().getString(R.string.File))) {
                    if (isPermissionGranted()) {
                        // change file picker from filechooserActivity


                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.setType("*/*"); // for all file
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);

                            String[] mimeTypes = {
                                    "application/pdf",
                                    "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                    "text/plain"};
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            startActivityForResult(intent, ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11);
                        } else {

                            //Old Code
//                            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                            chooseFile.setType("*/*");
//                            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
//                            startActivityForResult(chooseFile, REQUEST_PATH);



                            //By Hardik Kanak 27-06-2021
                            String[] mimeTypes = {
                                    "application/pdf",
                                    "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                    "text/plain"};

                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("*/*");
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            startActivityForResult(intent, REQUEST_PATH);
                        }
                        /*END*/
                    }
                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }

            }

        });

        builder.show();

    }

    private void OpenCameraApp() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utility.createImageFile(AddDataByTeacherActivity.this);
                mCurrentPhotoPath = Utility.mCurrentPhotoPath;
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(AddDataByTeacherActivity.this,
                        FILE_PROVIDER,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private boolean isPermissionGrantedForCamera() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    public boolean isPermissionGrantedForGallery() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {

                try {
                    Utility.BASE64_STRING = "";
                    Utility.getUserModelData(mContext);

                    try {
                        fielPath = data.getExtras().getString("imageData_uri");
                        Path = Utility.ImageCompress(fielPath, mContext);
    //                    Utility.GetBASE64STRING(Path);
                        File compressedImageFile = null;
                        File file = new File(fielPath);
                        compressedImageFile = new Compressor(this).compressToFile(file);
    //                        Log.d("getoriginalpath", compressedImageFile.getAbsolutePath());
    ////                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /*commented By Krishna : 06-02-2019 convert Filepath to Bitmap amd Display Bitmap into Image*/

                    Bitmap thePic;
                    byteArray = null;
                    byteArray = data.getExtras().getByteArray("imageData_byte");

                    try {
                        GetBitmapFromFilePath(fielPath);
                        iv_attachment.setImageBitmap(photoBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                Utility.BASE64_STRING = "";
//                File imageFile = new File(mCurrentPhotoPath);
//                Utility.GetBASE64STRING(mCurrentPhotoPath);
//                iv_attachment.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == PICK_FROM_GALLERY) {
                try {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    Log.d("getttt1234", picturePath);
                    Path = Utility.ImageCompress(picturePath, mContext);
                    Utility.GetBASE64STRING(picturePath);
                    GetBitmapFromFilePath(picturePath);
                    iv_attachment.setImageBitmap(photoBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11) {

                try {
                    //handle Camera CAPTURE Image for ANDROID 11 device
                    if (data != null) {
                        fileType = 0;
                        handleCaptureImage_Android11(data);

                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11) {

                try {
                    //handle SELECTED image ANDROID 11 device
                    if (data != null) {
                        fileType = 0;
                        handleImageAfterSelection_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11) {

                try {
                    if (data != null) {
                        fileType = 0;
                        handleFileSelected_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == SELECT_FILE) {

                try {
                    Utility.BASE64_STRING = "";

                    if (data != null) {

                        ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");

                        if (imgList != null && imgList.size() > 0) {

                            fielPath = imgList.get(0).getUri().toString();

                            File file = new File(fielPath);
                            File compressedImageFile = null;
                            try {
                                compressedImageFile = new Compressor(this).compressToFile(file);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                            Path = Utility.ImageCompress(fielPath, mContext);
                            Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                            /*END*/
                            GetBitmapFromFilePath(fielPath);
                            iv_attachment.setImageBitmap(photoBitmap);

                        }

                    }

//                Utility.BASE64_STRING = "";
//                if (data != null) {
//                    Uri selectedImage = data.getData();
//                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                    if (selectedImage != null) {
//                        Cursor cursor = getContentResolver().query(selectedImage,
//                                filePathColumn, null, null, null);
//                        if (cursor != null) {
//                            cursor.moveToFirst();
//
//                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                            String picturePath = cursor.getString(columnIndex);
//                            iv_attachment.setImageBitmap(BitmapFactory.decodeFile(picturePath));
//                            cursor.close();
//                        }
//                    }
//                }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_PATH) {

                try {
                    fileUri = data.getData();
                    filePath = fileUri.getPath();

                    String selectdpath = FileUtils.getPath(AddDataByTeacherActivity.this, fileUri);
                    Log.i("newImageFilePath", "" + selectdpath);

                    Utility.convertToBase64String(mContext, fileUri);

                    curFileName = new File(selectdpath).getName();
                    Log.i("newImageFileName", "" + curFileName);

                    /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                    File file = new File(curFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());

                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    /*END*/

                    fileType = 2;
                    iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

//                if (curFileName.contains("pdf")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.pdf);
//                } else if (curFileName.contains("PDF")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.pdf);
//                } else if (curFileName.contains("txt")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.txt);
//                } else if (curFileName.contains("TXT")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.txt);
//                } else if (curFileName.contains("doc")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.doc);
//                } else if (curFileName.contains("xlsx")) {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.excel_file);
//                } else {
//                    FileMineType = mimeType;
//                    iv_attachment.setImageResource(R.drawable.doc);
//                }


                    //By Hardik Kanak 27-06-2021
                    String selectedFileExtension = FileUtils.getExtension(curFileName);
                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.pdf);
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.txt);
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.doc);
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.excel_file);
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }


//                Utility.GetBASE64STRINGFROMFILE(fileUri.getPath());

                    /*END*/
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }

            } else if (requestCode == RESULT_SPEECH) {

                try {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    edt_title.setText(edt_title.getText().toString() + "" + text.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == RESULT_SPEECH_DESCRIPTION) {

                try {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    edt_detail.setText(edt_detail.getText().toString() + "" + text.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private void handleCaptureImage_Android11(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");

        fielPath = ImageAndroid11.getPathFromBitmap(this, imageBitmap);


        if (fielPath != null) {
            Utility.BASE64_STRING = "";
            Utility.getUserModelData(mContext);

            try {
                Path = Utility.ImageCompress(fielPath, mContext);

                Utility.GetBASE64STRING(fielPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*commented By Krishna : 06-02-2019 convert Filepath to Bitmap amd Display Bitmap into Image*/


            try {
                // GetBitmapFromFilePath(fielPath);
                photoBitmap = imageBitmap;
                iv_attachment.setImageBitmap(photoBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void handleImageAfterSelection_Android11(Intent data) {
        if (data.getData() != null) {

            try {
                Uri selectedImage = data.getData();
                fielPath = ImageAndroid11.getPathFromURI(this, selectedImage);
                Log.d("PATH", "PATH:: " + fielPath);

                //Must apply base 64 string function before upload photo
                if (fielPath != null) {
                    Path = fielPath;

                    Path = Utility.ImageCompress(fielPath, mContext);

                    Utility.GetBASE64STRING(fielPath);
                    Log.d("SS PATH", "PATH:: " + fielPath);

                    iv_attachment.setImageURI(null);
                    iv_attachment.setImageURI(Uri.parse(fielPath));

//                    iv_attachment.setImageBitmap(photoBitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }


        } else {
            if (data.getClipData() != null) {
                try {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();

                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                    }

                    for (int i = 0; i < mArrayUri.size(); i++) {
                        fielPath = ImageAndroid11.getPathFromURI(this, mArrayUri.get(i));
                        Log.d("PATH", "PATH:: " + fielPath);

                        //Must apply base 64 string function before upload photo
                        if (fielPath != null) {
                            Path = fielPath;

                            Path = Utility.ImageCompress(fielPath, mContext);

                            Utility.GetBASE64STRING(fielPath);
                            Log.d("SS PATH", "PATH:: " + fielPath);

                            iv_attachment.setImageURI(Uri.parse(fielPath));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }


    private void handleFileSelected_Android11(Intent data) {
        if (data.getData() != null) {

            try {
                Uri uri_data = data.getData();

                Log.e("PATH", "data:: " + data.toString());
                Log.e("PATH", "fileUri:: " + uri_data);

                String selectdpath = ImageAndroid11.getPathFromFileURL(this, uri_data);

                Log.e("PATH", "PATH:: " + selectdpath);


                File file2 = new File(selectdpath);
                final double length = file2.length();

                if (length > MB) {

                    file_size = format.format(length / MB) + " MB";
                    Log.d("PATH", "SIZE:: " + file_size);

                }

                if (Double.valueOf(format.format(length / MB)) > 20) { //file size change 5 to 20 by hardik_kanak 26-11-2020
                    //                Utility.toast(mContext, "Upload File with Maximum Size of 20 MB");
                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {

                    Uri uri = Uri.parse(selectdpath);

                    fileUri = uri;
                    filePath = selectdpath;

                    Log.i("uri", "" + uri);
                    Utility.convertToBase64String(mContext, uri_data);

                    File f = new File("" + uri);

                    String tempFileName = new File(selectdpath).getName();
                    curFileName = tempFileName;
                    Log.d("PATH getcurclfilename", tempFileName + " " + uri.toString());
                    Log.d("PATH getfilename", selectdpath + " " + tempFileName + " " + f.getName());

                    File file = new File(tempFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    fileType = 2;
                    iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

//                    if (curFileName.contains("pdf")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.pdf);
//                    } else if (curFileName.contains("PDF")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.pdf);
//                    } else if (curFileName.contains("txt")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.txt);
//                    } else if (curFileName.contains("TXT")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.txt);
//                    } else if (curFileName.contains("doc")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.doc);
//                    } else if (curFileName.contains("xlsx")) {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.excel_file);
//                    } else {
//                        FileMineType = mimeType;
//                        iv_attachment.setImageResource(R.drawable.doc);
//                    }

                    //By Hardik Kanak 27-06-2021
                    String selectedFileExtension = FileUtils.getExtension(curFileName);
                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.pdf);
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.txt);
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.doc);
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                        iv_attachment.setImageResource(R.drawable.excel_file);
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }


                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }

        }
    }

    public void uploadPhoto(int args, String filepathparam) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
//        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
//                new File(Utility.NewFileName).getName()));

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
                new File(filepathparam).getName()));

        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "IMAGE"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE,
                Utility.BASE64_STRING));


        Log.d("getimagerequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.UPLOAD_HOMEWORK_METHODNAME3,
                ServiceResource.UPLOADHOMEWORK_URL);

    }


    private Bitmap GetBitmapFromFilePath(String fielPath) {

        photoBitmap = Utility.getBitmap(fielPath, mContext);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        if (fielPath.contains(".png") || fielPath.contains(".PNG")) {

            photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        } else {

            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        }

        byteArray = stream.toByteArray();

        return photoBitmap;

    }

    //show popup for select stadard and division list which have multiple selection functionality
    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(mContext);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);
        final StanderdAdapter adapter = new StanderdAdapter(mContext, Global.StandardModels, true, true);
        TextView dialogTitle = (TextView) dialogStandard.findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);
        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard.findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard.findViewById(R.id.custome_Dialog_cancle_btn);
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(mContext.getResources().getString(R.string.selectgroupmember));
        edtSearch.setVisibility(View.GONE);

        dialogTitle.setText(mContext.getResources().getString(R.string.selectstandard));
        if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
            Log.d("getStdlist", Global.StandardModels.toString());
            lv.setAdapter(adapter);
        }

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
            }

        });


        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialogStandard.dismiss();
                adapter.getSelectedDivision();

                String txtTempStr = "";
                String StrStdDivID = "";
                gradeDivisionIdStr = "";
                gradeDivisionWallIdStr = "";

                for (int i = 0; i < adapter.getSelectedDivision().size(); i++) {

                    if (i == adapter.getSelectedDivision().size() - 1) {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ".";
                        StrStdDivID = StrStdDivID + adapter.getSelectedDivision().get(i).getStandrdId() + "|"
                                + adapter.getSelectedDivision().get(i).getDivisionId();
                    } else {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ",";
                        StrStdDivID = StrStdDivID + adapter.getSelectedDivision().get(i).getStandrdId() + "|"
                                + adapter.getSelectedDivision().get(i).getDivisionId() + ",";
                    }

                    String tempStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + "#";

                    String srtddivwallidStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + ","
                            + adapter.getSelectedDivision().get(i).getGradeDivisionWallID() +
                            "#";

                    gradeDivisionIdStr = gradeDivisionIdStr + tempStr;
                    gradeDivisionWallIdStr = gradeDivisionWallIdStr + srtddivwallidStr;

                }

                if (adapter.getSelectedDivision().size() > 0) {
                    try {
                        GetStudentByStdDiv(StrStdDivID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
//                edt_gradedivisionId.setText(txtTempStr);

            }

        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        dialogStandard.show();

    }

    private void GetStudentByStdDiv(String strStdDivID) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.STANDARDDIVISION, strStdDivID));
        Log.d("getstudrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, AddDataByTeacherActivity.this).execute(ServiceResource.GETSTUDENTLIST, ServiceResource.NOTES_URL);
    }

    // webservice for get division or standard list both in one webservice

    public void getStandarDivisionList(boolean isPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));
        new AsynsTaskClass(mContext, arrayList, isPopup, AddDataByTeacherActivity.this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);

    }

    public void parseProjctType(String result) {
        JSONArray hJsonArray;
        try {
            if (result.contains("\"success\":0")) {
            } else {
                hJsonArray = new JSONArray(result);
                getProjectTypes = new ArrayList<GetProjectTypeModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    GetProjectTypeModel model = new GetProjectTypeModel();
                    model.setCategory(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
                    model.setIsDefault(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                    model.setOrderNo(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                    model.setTerm(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERM));
                    model.setTermID(hJsonObject.getString(ServiceResource.GETPROJECTTYPE_TERMID));
                    getProjectTypes.add(model);

                    Log.d("getProjectType", getProjectTypes.toString());

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<GetProjectTypeModel> getProjectType_models = new ArrayList<GetProjectTypeModel>();

        if (getProjectTypes != null && getProjectTypes.size() > 0) {

            for (int i = 0; i < getProjectTypes.size(); i++) {
                if (getProjectTypes.get(i).getCategory().equalsIgnoreCase("CircularType")) {
                    getProjectType_models.add(getProjectTypes.get(i));
                }
            }
        }

        ArrayList<String> strings = new ArrayList<String>();
        strings.add(mContext.getResources().getString(R.string.circulartype));
        if (getProjectType_models != null && getProjectType_models.size() > 0) {
            for (int i = 0; i < getProjectType_models.size(); i++) {
                strings.add(getProjectType_models.get(i).getTerm());
                Log.d("getStringArray", strings.toString());
            }
        }

        if (strings != null && strings.size() > 0) {

            adapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spn_circulartype.setAdapter(adapter);
            spn_circulartype.setOnItemSelectedListener(this);

        }

        if (isEdit) {

            for (int i = 0; i < strings.size(); i++) {

                if (circularModel.getTypeTerm().equalsIgnoreCase(strings.get(i))) {

                    Log.d("getSelectedTypeTerm", strings.get(i));
                    spn_circulartype.setSelection(i);

                }

            }

        }


    }


    public void parseStdDivSub(String result) {

        Log.d("getstddivresult", result);

        JSONArray hJsonArray;

        try {

            Global.holidaysModel = new HolidaysModel();
            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);
                Global.StandardModels = new ArrayList<StandardModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {

                    boolean isAlreadyInsert = false;
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StandardModel model = new StandardModel();
                    model.setStandardName(hJsonObject.getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject.getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject.getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject.getString(ServiceResource.DIVISION));
                    model.setSubjectId(hJsonObject.getString(ServiceResource.STANDARD_SUBJECTID));
                    model.setSubjectName(hJsonObject.getString(ServiceResource.SUBJECT));
                    model.setGradeWallID(hJsonObject.getString(ServiceResource.GRADEWALLID));
                    model.setGradeDivisionWallID(hJsonObject.getString(ServiceResource.GRADEDIVISIONWALLID));
                    model.setGradeDivisionSubjectWallID(hJsonObject.getString(ServiceResource.GRADEDIVSUBALLID));
                    if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
                        for (int j = 0; j < Global.StandardModels.size(); j++) {
                            if (Global.StandardModels.get(j).getDivisionId().equalsIgnoreCase(model.getDivisionId())
                                    && Global.StandardModels.get(j).getStandrdId().equalsIgnoreCase(model.getStandrdId())) {
                                isAlreadyInsert = true;
                            }
                        }
                    }
                    if (!isAlreadyInsert) {
                        Global.StandardModels.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getAllSelectedStudent(int pos, ArrayList<CircularNoteStudentModel> addselectedstudentModels) {
        jobAllGroupMembers = addselectedstudentModels;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case READ_EXTERNAL_REQUEST_CODE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    // change file picker from filechooserActivity
                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.setType("*/*");
                    chooseFile = Intent.createChooser(chooseFile, "Choose a file");
                    startActivityForResult(chooseFile, REQUEST_PATH);
                    /*END*/
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    // change file picker from filechooserActivity
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                    /*END*/
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            case READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    OpenCameraApp();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }


//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
//                } else {
//                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
//                }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do your stuff

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}

