package com.edusunsoft.erp.orataro.model;

public class GetProjectTypeModel {

	private String TermID;
	private String Term;
	private String OrderNo;
	private String IsDefault;
	private String DefaultValue;
	private String Category;

	public String getTermID() {
		return TermID;
	}
	public void setTermID(String termID) {
		TermID = termID;
	}
	public String getTerm() {
		return Term;
	}
	public void setTerm(String term) {
		Term = term;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getIsDefault() {
		return IsDefault;
	}
	public void setIsDefault(String isDefault) {
		IsDefault = isDefault;
	}
	public String getDefaultValue() {
		return DefaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		DefaultValue = defaultValue;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}

	@Override
	public String toString() {

		return "GetProjectTypeModel{" +
				"TermID='" + TermID + '\'' +
				", Term='" + Term + '\'' +
				", OrderNo='" + OrderNo + '\'' +
				", IsDefault='" + IsDefault + '\'' +
				", DefaultValue='" + DefaultValue + '\'' +
				", Category='" + Category + '\'' +
				'}';
	}

}
