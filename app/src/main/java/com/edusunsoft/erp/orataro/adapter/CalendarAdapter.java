/*
 * Copyright (C) 2014 Mukesh Y authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.edusunsoft.erp.orataro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.CalenderEventInterface;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.CalendarFragment;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.CalenderEventModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.DateComparator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * @author Mukesh Y
 */
@SuppressLint("NewApi")
public class CalendarAdapter extends BaseAdapter {

    private Context mContext;
    private Calendar month;
    public GregorianCalendar pmonth; // calendar instance for previous month
    public GregorianCalendar pmonthmaxset;
    private GregorianCalendar selectedDate;
    private int firstDay;
    private int maxWeeknumber;
    private int maxP;
    private int calMaxP;
    private int lastWeekDay;
    private int leftDays;
    private int mnthlength;
    private String itemvalue, curentDateString;
    private DateFormat df;
    private ArrayList<String> items;
    public static List<String> dayString;
    private View previousView;
    private ArrayList<CalenderEventModel> calenderEventModels;
    private ArrayList<CalenderEventModel> CurrentMonthcalenderEventModels;
    private ArrayList<AtteandanceModel> atteandanceModels;
    private CalenderEventInterface calenderEventInterface;
    private boolean found;
    ArrayList<String> LeaveStudentArraylist = new ArrayList<>();
    ArrayList<String> PresentStudentArraylist = new ArrayList<>();
    ArrayList<String> AbsentStudentArraylist = new ArrayList<>();
    ArrayList<String> SeakLeaveStudentArraylist = new ArrayList<>();
    ArrayList<String> PresentWorkingStudentArraylist = new ArrayList<>();
    public int isPresentWorking = 0, isPresentNonWorking = 0, isAbsentWorking = 0, isAbsentNonWorking = 0, isSickLeaveWorking = 0, isSickLeaveNonWorking = 0, isLeaveWorking = 0, isLeaveNonWorking = 0;
    public int TotalWorkingDayStudent = 0, TotalNonWorkingDayStudent = 0;

    public CalendarAdapter(Context c, GregorianCalendar monthCalendar, ArrayList<CalenderEventModel> calenderEventModels, CalenderEventInterface pCalenderEventInterface) {
        CalendarAdapter.dayString = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        mContext = c;
        month.set(Calendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());
        this.calenderEventModels = calenderEventModels;
        calenderEventInterface = pCalenderEventInterface;
        if (this.calenderEventModels != null && this.calenderEventModels.size() > 0) {

            Log.d("getCalenderEvent", this.calenderEventModels.toString());

            CurrentMonthcalenderEventModels = new ArrayList<CalenderEventModel>();

            for (int i = 0; i < this.calenderEventModels.size(); i++) {

                if (Integer.parseInt(this.calenderEventModels.get(i).getStart().split("/")[1]) == (selectedDate.get(GregorianCalendar.MONTH) + 1)
                        && Integer.parseInt(this.calenderEventModels.get(i).getStart().split("/")[0]) == selectedDate.get(GregorianCalendar.YEAR)) {
                    CurrentMonthcalenderEventModels.add(this.calenderEventModels.get(i));

                }

            }

//            for (int i = 0; i < this.calenderEventModels.size(); i++) {
//
//                if (Integer.parseInt(this.calenderEventModels.get(i).getStart().split("/")[1]) == (selectedDate.get(GregorianCalendar.MONTH) + 1)
//                        && Integer.parseInt(this.calenderEventModels.get(i).getStart().split("/")[0]) == selectedDate.get(GregorianCalendar.YEAR)) {
//
//
//                }
//            }
        }

        Log.e("Month", String.valueOf(selectedDate.get(GregorianCalendar.MONTH)));
        Log.e("year", String.valueOf(selectedDate.get(GregorianCalendar.YEAR)));

        if (CurrentMonthcalenderEventModels != null && CurrentMonthcalenderEventModels.size() > 0) {
            calenderEventInterface.getMonthEventCount(CurrentMonthcalenderEventModels.size());
        } else {
            calenderEventInterface.getMonthEventCount(0);
        }

        if (this.calenderEventModels != null && this.calenderEventModels.size() > 0) {
            Collections.sort(CurrentMonthcalenderEventModels, new DateComparator());
        }
        refreshDays();
    }

    public CalendarAdapter(Context c, GregorianCalendar monthCalendar, ArrayList<AtteandanceModel> atteandanceModels) {

        CalendarAdapter.dayString = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        mContext = c;
        month.set(Calendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());
        this.atteandanceModels = atteandanceModels;
        refreshDays();

    }

    public void setItems(ArrayList<String> items) {
        for (int i = 0; i != items.size(); i++) {
            if (items.get(i).length() == 1) {
                items.set(i, "0" + items.get(i));
            }
        }
        this.items = items;
    }

    @Override
    public int getCount() {
        return dayString.size();
    }

    @Override
    public Object getItem(int position) {
        return dayString.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // create a new view for each item referenced by the Adapter
    @Override
    @SuppressWarnings("static-access")
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView dayView;

        if (convertView == null) {

            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.calendar_item, null);

        }

        RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.RelativeLayout1);
        new Color();
        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        dayView = (TextView) v.findViewById(R.id.date);
        // separates daystring into parts.
        String[] separatedTime = dayString.get(position).split("-");
        // taking last part of date. ie; 2 from 2012-12-02
        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        Log.d("gridvalue", gridvalue);
        // checking whether the day is in current month or not.
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            // setting offdays to white color.
            dayView.setTextColor(mContext.getResources().getColor(R.color.grey_list));
            dayView.setClickable(false);
            dayView.setFocusable(false);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
            dayView.setTextColor(mContext.getResources().getColor(R.color.grey_list));
            dayView.setClickable(false);
            dayView.setFocusable(false);
        } else {
            dayView.setTextColor(mContext.getResources().getColor(R.color.black));
            if (Integer.valueOf(gridvalue) == (new Date().getDate()) && Integer.valueOf(separatedTime[0]) == (new Date().getYear() + 1900) && Integer.valueOf(separatedTime[1]) == (new Date().getMonth() + 1)) {
                dayView.setTextColor(mContext.getResources().getColor(R.color.blue));
                relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.circle_blue));
            }
        }

        if (dayString.get(position).equals(curentDateString)) {

            setSelected(v);
            previousView = v;

        }

        dayView.setText(gridvalue);

        // create date string for comparison
        String date = dayString.get(position);

        if (date.length() == 1) {
            date = "0" + date;
        }
        String monthStr = "" + (month.get(Calendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }

        // show icon if date is not empty and it exists in the items array
        ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
        iw.setVisibility(View.INVISIBLE);
        if (calenderEventInterface != null) {
            String typeHoliday = "Holiday";
            if (CurrentMonthcalenderEventModels != null && CurrentMonthcalenderEventModels.size() > 0) {

                found = false;
                if (Integer.valueOf(gridvalue) < 10) {
                    gridvalue = "0" + gridvalue;
                }
                for (int i = 0; i < CurrentMonthcalenderEventModels.size(); i++) {


                    String month = "";
                    if ((selectedDate.get(GregorianCalendar.MONTH) + 1) < 10) {
                        month = "0" + String.valueOf(selectedDate.get(GregorianCalendar.MONTH) + 1);
                    } else {
                        month = String.valueOf(selectedDate.get(GregorianCalendar.MONTH) + 1);
                    }

                    if (CurrentMonthcalenderEventModels.get(i).getStart().split("/")[2].equals(gridvalue)
                            && CurrentMonthcalenderEventModels.get(i).getStart().split("/")[1].equals(month)) {

                        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
                            // setting offdays to white color.
                        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {

                        } else {

                            // setting curent month's days in blue color.
                            typeHoliday = CurrentMonthcalenderEventModels.get(i).getType();
                            found = true;
                            break;

                        }

                    }

                }

            }

            if (found) {

                if (typeHoliday.equalsIgnoreCase("Holiday")) {
                    iw.setVisibility(View.VISIBLE);
                } else if (typeHoliday.equalsIgnoreCase("Exam")) {
                    iw.setVisibility(View.VISIBLE);
                    iw.setImageResource(R.drawable.exam);
                } else {
                    iw.setVisibility(View.VISIBLE);
                    iw.setImageResource(R.drawable.notify);
                }

            }

        } else {
            Date d = new Date();
            SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");

            try {

                d = date_format.parse(dayString.get(position));

            } catch (java.text.ParseException e) {

                e.printStackTrace();

            }

            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek == 1 || dayOfWeek == 7) {
                relativeLayout.setBackgroundColor(Color.parseColor("#d3d3d3"));
            }

            if (atteandanceModels != null && atteandanceModels.size() > 0) {

                found = false;

                if (Integer.valueOf(gridvalue) < 10) {

                    gridvalue = "0" + gridvalue;

                }


                for (int i = 0; i < atteandanceModels.size(); i++) {

                    try {

                        String month = "";

                        if ((selectedDate.get(GregorianCalendar.MONTH) + 1) < 10) {

                            month = "0" + String.valueOf(selectedDate.get(GregorianCalendar.MONTH) + 1);

                        } else {

                            month = String.valueOf(selectedDate.get(GregorianCalendar.MONTH) + 1);

                        }

                        String year = separatedTime[0];

                        if (atteandanceModels.get(i).getDateOfAttendence().split("/")[2].equals(gridvalue)
                                && atteandanceModels.get(i).getDateOfAttendence().split("/")[1].equals(month)
                                && atteandanceModels.get(i).getDateOfAttendence().split("/")[0].equals(year)) {

                            if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
                                // setting offdays to white color.
                            } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
                            } else {
                                // setting curent month's days in blue color.

                                found = true;

                                ServiceResource.PresentStudent = "0";
                                ServiceResource.AbsentStudent = "0";
                                ServiceResource.SeakLeaveStudent = "0";
                                ServiceResource.LeaveStudent = "0";


                                if (atteandanceModels.get(i).getAttencenceType_Term().equalsIgnoreCase(ServiceResource.PRESENT)) {
                                    relativeLayout.setBackgroundColor(new Color().parseColor("#7BC664"));
                                    PresentStudentArraylist.add(atteandanceModels.get(i).getAttencenceType_Term());

                                    Log.d("getParseStuentArraylist", String.valueOf(PresentStudentArraylist.size()));

                                    if (atteandanceModels.get(i).isWorkingDay()) {

                                        isPresentWorking++;

                                    } else {

                                        isPresentNonWorking++;

                                    }


                                }

                                if (atteandanceModels.get(i).getAttencenceType_Term().equalsIgnoreCase(ServiceResource.ABSENT)) {
                                    relativeLayout.setBackgroundColor(new Color().parseColor("#D13A3A"));
                                    AbsentStudentArraylist.add(atteandanceModels.get(i).getAttencenceType_Term());

                                    if (atteandanceModels.get(i).isWorkingDay()) {
                                        isAbsentWorking++;
                                    } else {
                                        isAbsentNonWorking++;
                                    }
                                }

                                if (atteandanceModels.get(i).getAttencenceType_Term().equalsIgnoreCase(ServiceResource.LEAVE)) {
                                    relativeLayout.setBackgroundColor(new Color().parseColor("#3AB6D1"));
                                    LeaveStudentArraylist.add(atteandanceModels.get(i).getAttencenceType_Term());
                                    if (atteandanceModels.get(i).isWorkingDay()) {
                                        isLeaveWorking++;
                                    } else {
                                        isLeaveNonWorking++;
                                    }
                                }

                                if (atteandanceModels.get(i).getAttencenceType_Term().equalsIgnoreCase(ServiceResource.SICKLEAVE)) {
                                    relativeLayout.setBackgroundColor(new Color().parseColor("#E2C438"));
                                    SeakLeaveStudentArraylist.add(atteandanceModels.get(i).getAttencenceType_Term());
                                    if (atteandanceModels.get(i).isWorkingDay()) {
                                        isSickLeaveWorking++;
                                    } else {
                                        isSickLeaveNonWorking++;
                                    }

                                }

                                break;

                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                CalendarFragment.txt_total_present.setText(String.valueOf(PresentStudentArraylist.size()));
                CalendarFragment.txt_total_absent.setText(String.valueOf(AbsentStudentArraylist.size()));
                CalendarFragment.txt_total_leave.setText(String.valueOf(LeaveStudentArraylist.size()));
                CalendarFragment.txt_total_seakleave.setText(String.valueOf(SeakLeaveStudentArraylist.size()));

                CalendarFragment.txt_present_working.setText(String.valueOf(isPresentWorking));
                CalendarFragment.txt_present_nonworking.setText(String.valueOf(isPresentNonWorking));
                CalendarFragment.txt_absent_working.setText(String.valueOf(isAbsentWorking));
                CalendarFragment.txt_absent_nonworking.setText(String.valueOf(isAbsentNonWorking));
                CalendarFragment.txt_leave_working.setText(String.valueOf(isLeaveWorking));
                CalendarFragment.txt_leave_nonworking.setText(String.valueOf(isLeaveNonWorking));
                CalendarFragment.txt_sickleave_working.setText(String.valueOf(isSickLeaveWorking));
                CalendarFragment.txt_sickleave_nonworking.setText(String.valueOf(isSickLeaveNonWorking));


                TotalWorkingDayStudent = isPresentWorking + isAbsentWorking + isLeaveWorking + isSickLeaveWorking;
                TotalNonWorkingDayStudent = isPresentNonWorking + isAbsentNonWorking + isLeaveNonWorking + isSickLeaveNonWorking;
                CalendarFragment.txt_total_working.setText(String.valueOf(TotalWorkingDayStudent));
                CalendarFragment.txt_total_nonworking.setText(String.valueOf(TotalNonWorkingDayStudent));

                int TotalAttendance = SeakLeaveStudentArraylist.size() + LeaveStudentArraylist.size() + AbsentStudentArraylist.size() + PresentStudentArraylist.size();
                CalendarFragment.txt_total.setText(String.valueOf(TotalAttendance));

            }

        }

        return v;

    }

    public View setSelected(View view) {

        previousView = view;
        return view;

    }

    public void refreshDays() {

        // clear items
        items.clear();
        dayString.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        // month start day. ie; sun, mon, etc
        firstDay = month.get(Calendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(Calendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(Calendar.DAY_OF_MONTH, calMaxP + 1);

        /**
         * filling calendar gridview.
         */

        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(Calendar.DATE, 1);
            dayString.add(itemvalue);
            Log.d("fillcalender", dayString.toString());

        }

    }

    private int getMaxP() {

        int maxP;
        if (month.get(Calendar.MONTH) == month.getActualMinimum(Calendar.MONTH)) {
            pmonth.set((month.get(Calendar.YEAR) - 1), month.getActualMaximum(Calendar.MONTH), 1);
        } else {
            pmonth.set(Calendar.MONTH, month.get(Calendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(Calendar.DAY_OF_MONTH);

        return maxP;
    }

}