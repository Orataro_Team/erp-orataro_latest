package com.edusunsoft.erp.orataro.model;

public class ReadWriteSettingModel {

    private String RightID, RightName, IsView, IsEdit, IsCreate, IsDelete;

    public String getRightID() {
        return RightID;
    }

    public void setRightID(String rightID) {
        RightID = rightID;
    }

    public String getRightName() {
        return RightName;
    }

    public void setRightName(String rightName) {
        RightName = rightName;
    }

    public boolean getIsView() {
        return Strtobool(IsView);
    }

    public void setIsView(String isView) {
        IsView = isView;
    }

    public boolean getIsEdit() {
        return Strtobool(IsEdit);
    }

    public void setIsEdit(String isEdit) {
        IsEdit = isEdit;
    }

    public boolean getIsCreate() {
        return Strtobool(IsCreate);
    }

    public void setIsCreate(String isCreate) {
        IsCreate = isCreate;
    }

    public boolean getIsDelete() {
        return Strtobool(IsDelete);
    }

    public void setIsDelete(String isDelete) {
        IsDelete = isDelete;
    }

    public boolean Strtobool(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("false")) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "ReadWriteSettingModel{" +
                "RightID='" + RightID + '\'' +
                ", RightName='" + RightName + '\'' +
                ", IsView='" + IsView + '\'' +
                ", IsEdit='" + IsEdit + '\'' +
                ", IsCreate='" + IsCreate + '\'' +
                ", IsDelete='" + IsDelete + '\'' +
                '}';
    }
}
