package com.edusunsoft.erp.orataro.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.edusunsoft.erp.orataro.model.UserModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PreferenceData {

    private static SharedPreferences sharedPreferences = null;

    private static SharedPreferences.Editor editor = null;

    public static void init(Context mcontext) {

        if (sharedPreferences == null) {

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mcontext);
            editor = sharedPreferences.edit();

        }

    }


    public static void setToken(String token) {
        SharedPreferenceUtil.putValue("FCM_TOKEN", token);
    }


    public static String getToken() {

        return SharedPreferenceUtil.getString("FCM_TOKEN", "");

    }

    public static void setMultipleUser(boolean ismultiple) {
        SharedPreferenceUtil.putValue("ISMULTIPLE", ismultiple);
    }


    public static boolean getMultipleUser() {

        return SharedPreferenceUtil.getBoolean("ISMULTIPLE", false);

    }


    public static void setLogin(boolean token) {
        SharedPreferenceUtil.putValue("IS_USER_LOGIN", token);
    }

    public static boolean getIsLogin() {

        return SharedPreferenceUtil.getBoolean("IS_USER_LOGIN", false);

    }


    public static void setIsMember(boolean token) {
        SharedPreferenceUtil.putValue("IS_MEMBER", token);
    }

    public static boolean getIsMember() {
        return SharedPreferenceUtil.getBoolean("IS_MEMBER", false);
    }

    public static void setIsSwitched(boolean token) {
        SharedPreferenceUtil.putValue("IS_SWITCHED", token);
    }

    public static boolean getIsSwitched() {

        return SharedPreferenceUtil.getBoolean("IS_SWITCHED", false);
    }

    public static void setCTOKEN(String ctoken) {
        SharedPreferenceUtil.putValue("CTOKEN", ctoken);
    }

    public static String getCTOKEN() {

        return SharedPreferenceUtil.getString("CTOKEN", "");
    }


    public static void saveFirebaseMessageData(String firebaseResponse) {

        SharedPreferenceUtil.putValue("FIREBASEMESSAGERESPONSE", firebaseResponse);

    }

    public static void saveFirebaseMessageTitle(String firebasetitle) {

        SharedPreferenceUtil.putValue("FIREBASETITLE", firebasetitle);

    }


    public static void saveFirebaseMessageMessage(String firebasemessage) {

        SharedPreferenceUtil.putValue("FIREBASEMESSAGE", firebasemessage);

    }


    public static String getFirebaseMessageData() {

        String jsonOutput = SharedPreferenceUtil.getString("FIREBASEMESSAGERESPONSE", "");
        return jsonOutput;

    }


    public static String getFirebaseMessageTitle() {

        String jsonOutput = SharedPreferenceUtil.getString("FIREBASETITLE", "");
        return jsonOutput;

    }


    public static String getFirebaseMessageMessage() {

        String jsonOutput = SharedPreferenceUtil.getString("FIREBASEMESSAGE", "");
        return jsonOutput;

    }

    public static void saveLoginUserData(String userData) {

        SharedPreferenceUtil.putValue("LOGINUSERDATA", userData);

    }

    public static void switchsaveLoginUserData(String switchuserData) {

        SharedPreferenceUtil.putValue("SWITCHLOGINUSERDATA", switchuserData);

    }

    public static ArrayList<LoginUserModel> getSwitchLoginUserData() {

        Gson gson = new Gson();
        String jsonOutput = SharedPreferenceUtil.getString("SWITCHLOGINUSERDATA", "");
        Type loginuserlistType = new TypeToken<List<LoginUserModel>>() {
        }.getType();
        ArrayList<LoginUserModel> SwitchLoginUserList = gson.fromJson(jsonOutput, loginuserlistType);
        return SwitchLoginUserList;

    }


    public static ArrayList<UserModel> getLoginUserData() {

        Gson gson = new Gson();
        String jsonOutput = SharedPreferenceUtil.getString("LOGINUSERDATA", "");
        Type loginuserlistType = new TypeToken<List<UserModel>>() {
        }.getType();
        ArrayList<UserModel> LoginUserList = gson.fromJson(jsonOutput, loginuserlistType);
        return LoginUserList;

    }


}
