package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import androidx.core.content.FileProvider;

import com.edusunsoft.erp.orataro.BuildConfig;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.imagepicker.ErrorResult;
import com.edusunsoft.erp.orataro.imagepicker.ImageCropper;
import com.edusunsoft.erp.orataro.imagepicker.ImageFileSelector;
import com.edusunsoft.erp.orataro.imagepicker.ImageUtils;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static android.os.Build.VERSION_CODES.M;

public class PreviewImageActivity extends Activity {

    private static final int SELECT_PICTURE = 1;
    public static final int PICK_FROM_CAMERA = 1;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int PIC_CROP = 3000;
    static final int DATE_DIALOG_ID = 111;
    private static final int CROP_PIC = 300;


    private ImageFileSelector mImageFileSelector;
    private ImageCropper mImageCropper;
    private File mCurrentSelectFile;

    Context context;
    private ImageView iv_preview, iv_cancle;
    private String cardfilepath;
    private ImageView iv_crop;
    private ImageView iv_ok;
    private int getimage = 400;
    private int img_rotation = 0;

    ImageView iv_rotate;

    private void ImagePickerCallback() {

        // TODO Auto-generated method stub

        mImageFileSelector = new ImageFileSelector(context);
        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
            @Override
            public void onError(ErrorResult errorResult) {
                switch (errorResult) {

                    case permissionDenied:

                        Utility.toast(context, "Permission Denied");
//						Toast.makeText(context, "Permission Denied",
//								Toast.LENGTH_LONG).show();

                        break;

                    case canceled:
                        Utility.toast(context, "Canceled");
//						Toast.makeText(context, "Canceled",
//								Toast.LENGTH_LONG).show();
                        break;

                    case error:

                        Utility.toast(context, "Unknown Error");
//						Toast.makeText(context, "Unknown Error",
//								Toast.LENGTH_LONG).show();

                        break;

                }

            }

            @Override
            public void onSuccess(String file) {

                Log.d("filsuccess",file);
                loadImage(file);
                mCurrentSelectFile = new File(file);


            }


        });

        mImageCropper = new ImageCropper();

        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
            @Override
            public void onError(ImageCropper.CropperErrorResult result) {
                switch (result) {
                    case error:
                        Utility.toast(context, "crop image error");
//						Toast.makeText(context, "crop image error",
//								Toast.LENGTH_LONG).show();
                        break;
                    case canceled:
                        Utility.toast(context, "crop image cancelled");
//						Toast.makeText(context, "crop image canceled",
//								Toast.LENGTH_LONG).show();
                        break;
                    case notSupport:
                        Utility.toast(context, "crop image not support");
//						Toast.makeText(context, "crop image not support",
//								Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onSuccess(String outputFile) {

                loadImage(outputFile);
                mCurrentSelectFile = new File(outputFile);

            }

        });

    }


    private void loadImage(final String file) {

        new Thread(new Runnable() {

            @Override
            public void run() {

                final Bitmap bitmap = BitmapFactory.decodeFile(file);
                final File imageFile = new File(file);
                final StringBuilder builder = new StringBuilder();
                builder.append("path: ");
                builder.append(file);
                builder.append("\n\n");
                builder.append("length: ");
                builder.append((int) (imageFile.length() / 1024d));
                builder.append("KB");
                builder.append("\n\n");
                builder.append("image size: (");
                builder.append(bitmap.getWidth());
                builder.append(", ");
                builder.append(bitmap.getHeight());
                builder.append(")");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mImageView.setImageBitmap(bitmap);
                        // mTvPath.setText(builder.toString());

                        cardfilepath = imageFile.getPath();
                        Matrix matrix = new Matrix();

                        try {

                            cardfilepath = Utility.compressImage(cardfilepath);

                            Bitmap bitmap = BitmapFactory.decodeFile(cardfilepath);
                            matrix.postRotate(0);

                            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

                            bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

                            //	Bitmap	bitmap = BitmapFactory.decodeFile(cardfilepath);
                            iv_preview.setImageBitmap(bitmap);

                            //	Bitmap bitmapnew = ImageUtils.compressImageFile(file, 400, 800);

                            ImageUtils.saveBitmap(bitmap, mCurrentSelectFile.getAbsolutePath(), Bitmap.CompressFormat.JPEG, 80);

                            //bitmapnew.recycle();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //                            }
                        //                            cardfilepath = imageFile.getPath();
                        //                            iv_preview.setImageBitmap(bitmap);

                    }
                });
            }

        }).start();
    }

    private void initImageFileSelector() {

        mImageFileSelector.setOutPutImageSize(800, 800);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        context = this;

        int requestcode = getIntent().getIntExtra("RequestCode", -1);

        iv_preview = (ImageView) findViewById(R.id.iv_preview);
        iv_ok = (ImageView) findViewById(R.id.iv_ok);
        iv_crop = (ImageView) findViewById(R.id.iv_crop);
        iv_rotate = (ImageView) findViewById(R.id.iv_rotate);
        iv_cancle = (ImageView) findViewById(R.id.iv_cancle);

        ImagePickerCallback();

        if (requestcode != -1) {

            if (requestcode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

                initImageFileSelector();
                mImageFileSelector.takePhoto(PreviewImageActivity.this,
                        requestcode);

            } else if (requestcode == SELECT_PICTURE) {

                initImageFileSelector();
                mImageFileSelector.selectImage(PreviewImageActivity.this,
                        SELECT_PICTURE);

            }

        }


        iv_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("getcurrentfile",mCurrentSelectFile.toString());
                if (mCurrentSelectFile != null) {

                    Intent intent = new Intent();
                    //						intent.putExtra("imageData_uri", capturedImageFilePath);
                    //						setResult(RESULT_OK, intent);
                    //						bitmap.recycle();
                    //						finish();

                    byte[] byteArray = null;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                    FileInputStream fis;

                    try {

                        fis = new FileInputStream(new File(mCurrentSelectFile.getPath().toString()));
                        byte[] buf = new byte[1024];
                        int n;

                        while (-1 != (n = fis.read(buf)))

                            bytes.write(buf, 0, n);

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                    //					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    byteArray = bytes.toByteArray();//convertImageToByteArra(fileUri.getPath());
                    intent.putExtra("mCurrentSelectFile", byteArray); //send image byte to  main activity
                    intent.putExtra("imageData_uri", mCurrentSelectFile.getPath().toString());//send image Uri to  main activity
                    Log.d("previewimagepath", mCurrentSelectFile.getPath().toString());

                    //						intent.putExtra("imageData_bitmap",new BitmapUtil(CameraActivity.this).compressImage(uriTarget.toString()));//send image Bitmap to  main activity
                    setResult(RESULT_OK, intent);
                    //						bitmap.recycle();
                    finish();

                    //                   Intent intent=new Intent();
                    //                   intent.putExtra("Path",mCurrentSelectFile.getPath().toString());
                    //                   setResult(RESULT_OK,intent);
                    //                   finish();

                }

            }

        });

        iv_cancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_crop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

//                Uri photoURI = FileProvider.getUriForFile(PreviewImageActivity.this,
//                        BuildConfig.APPLICATION_ID + ".provider",
//                        createImageFile());

//                performCrop(Uri.fromFile(new File(cardfilepath)));

                performCrop(FileProvider.getUriForFile(PreviewImageActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider", new File(cardfilepath)));
//
//                FileProvider.getUriForFile(PreviewImageActivity.this,
//                        BuildConfig.APPLICATION_ID + ".provider", new File(cardfilepath));

//				}
            }
        });

        iv_rotate.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                float angle = 0;
                Bitmap bitmap = ((BitmapDrawable) iv_preview.getDrawable()).getBitmap();

                angle += 90;
                Bitmap rotatedImage = rotateImage(bitmap, angle);
                iv_preview.setImageBitmap(rotatedImage);

//                if (mCurrentSelectFile.getPath() != null) {
//
//                    Bitmap bitmap = ((BitmapDrawable) iv_preview.getDrawable()).getBitmap();
//                    Matrix matrix = new Matrix();
//
//                    img_rotation += 90;
//
//                    if (img_rotation > 360) {
//
//                        img_rotation = 90;
//
//                    }
//
//                    iv_preview.setRotation(iv_preview.getRotation() + 90);
//
////                  matrix.postRotate(img_rotation);
//
//                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
//
//                    bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
//
//                    iv_preview.setImageBitmap(bitmap);
//
//                    byte[] byteArray = null;
//                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                    FileOutputStream out = null;

//                    try {
//                        out = new FileOutputStream(mCurrentSelectFile.getPath().toString());
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//
////					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//                }
                
            }

        });

    }


    public static Bitmap rotateImage(Bitmap sourceImage, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(sourceImage, 0, 0, sourceImage.getWidth(), sourceImage.getHeight(), matrix, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            initImageFileSelector();
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                mImageFileSelector.onActivityResult(requestCode,
                        resultCode, data);
            } else if (requestCode == SELECT_PICTURE) {
                mImageFileSelector.onActivityResult(requestCode,
                        resultCode, data);
            } else if (requestCode == CROP_PIC) {
                mImageCropper.onActivityResult(requestCode,
                        resultCode, data);
            }
        } else if ((requestCode == PIC_CROP)
                && (resultCode == RESULT_OK)) {
            if (requestCode == PIC_CROP) {

                Bitmap bitmap = null;

                if (data != null) {

                    try {
                        // et the returned data
                        Bundle extras = data.getExtras();
                        // get the cropped bitmap
                        bitmap = extras.getParcelable("data");


                    } catch (Exception e) {

                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        try {
                            bitmap = BitmapFactory.decodeStream(getContentResolver()
                                    .openInputStream(selectedImage));
                        } catch (FileNotFoundException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                    if (bitmap != null) {
                        iv_preview.setImageBitmap(bitmap);
                    }
                    byte[] byteArray = null;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(mCurrentSelectFile.getPath().toString());
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

//			            imgView.setImageBitmap(selectedBitmap);
                }

            } else {
                finish();
            }
        } else {
            finish();
        }

    }

    private void performCrop(Uri picUri) {


        final int width = 400;
        final int height = 200;

        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            Uri contentUri;

            if (Build.VERSION.SDK_INT > M) {

                contentUri = FileProvider.getUriForFile(PreviewImageActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        new File(cardfilepath));//package.provider

                //TODO:  Permission..

                getApplicationContext().grantUriPermission("com.android.camera",
                        contentUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

                cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            } else {

                contentUri = Uri.fromFile(new File(cardfilepath));

            }

            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 2);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", width);
            cropIntent.putExtra("outputY", height);

            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, PIC_CROP);

        } catch (ActivityNotFoundException a) {

            Log.e("Activity Not Found", "" + a.toString());

        }

//        try {
//
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
////            cropIntent.setClassName("com.android.camera", "com.android.camera.CropImage");
//            // indicate image type and Uri
//            cropIntent.setDataAndType(picUri, "image/*");
//            // set crop properties
//            cropIntent.putExtra("crop", "true");
//            // indicate aspect of desired crop
//            cropIntent.putExtra("aspectX", 1);
//            cropIntent.putExtra("aspectY", 1);
//            // indicate output X and Y
//            cropIntent.putExtra("outputX", 256);
//            cropIntent.putExtra("outputY", 256);
//            // retrieve data on return
//            cropIntent.putExtra("return-data", true);
//            // start the activity - we handle returning in onActivityResult
//            startActivityForResult(cropIntent, PIC_CROP);
//
//        }
//
//        // respond to users whose devices do not support the crop action
//        catch (ActivityNotFoundException anfe) {
//            // display an error message
//            String errorMessage = "Whoops - your device doesn't support the crop action!";
//            Utility.toast(this, errorMessage);
////			Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
////			toast.show();
//        }
    }
}
