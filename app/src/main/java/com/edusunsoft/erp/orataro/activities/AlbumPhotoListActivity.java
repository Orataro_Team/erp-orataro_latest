package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PhotoAdapter;
import com.edusunsoft.erp.orataro.model.PhotoListModel;

import java.util.ArrayList;

import static com.edusunsoft.erp.orataro.services.ServiceResource.FROMUPLOADHOMEWORK;

public class AlbumPhotoListActivity extends FragmentActivity implements OnClickListener {

    private GridView grv_photos;
    private ImageView img_back;
    private PhotoAdapter photo_ListAdapter;
    private TextView header_text;
    private Context mContext;
    private ArrayList<PhotoListModel> photoListModel;
    LinearLayout lyl_approve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photo_list);

        mContext = AlbumPhotoListActivity.this;
        photoListModel = getIntent().getParcelableArrayListExtra("photoModels");
        grv_photos = (GridView) findViewById(R.id.grv_photos);
        header_text = (TextView) findViewById(R.id.header_text);
        img_back = (ImageView) findViewById(R.id.img_back);
        lyl_approve = (LinearLayout) findViewById(R.id.lyl_approve);
        header_text.setText(getResources().getString(R.string.Album));
        photo_ListAdapter = new PhotoAdapter(mContext, photoListModel, 1);
        grv_photos.setAdapter(photo_ListAdapter);
        img_back.setOnClickListener(this);

        if (FROMUPLOADHOMEWORK.equalsIgnoreCase("false")) {
            lyl_approve.setVisibility(View.GONE);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            default:
                break;
        }
    }


}
