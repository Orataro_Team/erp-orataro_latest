package com.edusunsoft.erp.orataro.util;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.SplashActivity;
import com.edusunsoft.erp.orataro.activities.WidgetManageActivity;
import com.edusunsoft.erp.orataro.adapter.SwitchStudentAdapter;
import com.edusunsoft.erp.orataro.model.SwitchStudentModel;
import com.edusunsoft.erp.orataro.services.WidgetService;

import java.util.ArrayList;

public class SwitchUserWidget extends AppWidgetProvider {

    private ArrayList<SwitchStudentModel> switchStudentModels;
    private SwitchStudentModel switchStudentModel;
    private SwitchStudentAdapter switchStudentAdapter;


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        // TODO Auto-generated method stub.
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; ++i) {
            RemoteViews remoteViews = updateWidgetListView(context,
                    appWidgetIds[i]);
//			appWidgetManager.updateAppWidget(appWidgetIds[i], 
//					null);
            appWidgetManager.updateAppWidget(appWidgetIds[i],
                    remoteViews);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }


    private RemoteViews updateWidgetListView(Context context,
                                             int appWidgetId) {

        //which layout to show on widget
        RemoteViews remoteViews = new RemoteViews(
                context.getPackageName(), R.layout.listview);

        //	   remoteViews.setViewVisibility(R.id.ll_extra, View.VISIBLE);
        //	   Intent intent = new Intent(context, HomeWorkFragmentActivity.class);
        //	   intent.putExtra("position", 1);
        //       PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        //       remoteViews.setOnClickPendingIntent(R.id.img_circular, pendingIntent);
        //RemoteViews Service needed to provide adapter for ListView
        Intent svcIntent = new Intent(context, WidgetService.class);
        //passing app widget id to that RemoteViews Service
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        //setting a unique Uri to the intent
        //don't know its purpose to me right now
        svcIntent.setData(Uri.parse(
                svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        //setting adapter to listview of the widget
        remoteViews.setRemoteAdapter(appWidgetId, R.id.listViewWidget, svcIntent);
        if (new UserSharedPrefrence(context).getLOGIN_JSON().equalsIgnoreCase("NAN")) {
            remoteViews.setEmptyView(R.id.listViewWidget, R.id.empty_view);
        }
//		else{
//			remoteViews.setRemoteAdapter(appWidgetId, R.id.listViewWidget,svcIntent);
//		}

//		remoteViews.setRemoteAdapter(appWidgetId, R.id.listViewWidget,
//				svcIntent);
        //setting an empty view in case of no data
//		remoteViews.setEmptyView(R.id.listViewWidget, R.id.empty_view);
        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra("position", 1);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.empty_view, pendingIntent);


        Intent clickIntent = new Intent(context, WidgetManageActivity.class);
        PendingIntent clickPI = PendingIntent
                .getActivity(context, 0,
                        clickIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        remoteViews.setPendingIntentTemplate(R.id.listViewWidget, clickPI);

//		Toast.makeText(context, "Reciver ", Toast.LENGTH_SHORT).show();

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int appWidgetIds[] = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, SwitchUserWidget.class));
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.listViewWidget);
        return remoteViews;
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onDisabled(Context context) {

        super.onDisabled(context);
    }
}
