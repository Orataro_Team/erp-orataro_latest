package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeworkParse implements Parcelable {

    @SerializedName("AssignmentID")
    @Expose
    private String assignmentID;
    @SerializedName("DateOfHomeWork")
    @Expose
    private String dateOfHomeWork;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("HomeWorksDetails")
    @Expose
    private String homeWorksDetails;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("DateOfFinish")
    @Expose
    private String dateOfFinish;
    @SerializedName("IsRead")
    @Expose
    private String isRead;
    @SerializedName("IsApprove")
    @Expose
    private String isApprove;
    @SerializedName("isWorkFinish")
    @Expose
    private String isWorkFinish;
    @SerializedName("subjectId")
    @Expose
    private String subjectId;
    @SerializedName("TeacherProfilePicture")
    @Expose
    private String teacherProfilePicture;
    @SerializedName("TeacherName")
    @Expose
    private String teacherName;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("GradeName")
    @Expose
    private String gradeName;
    @SerializedName("DivisionName")
    @Expose
    private String divisionName;
    public final static Parcelable.Creator<HomeworkParse> CREATOR = new Creator<HomeworkParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeworkParse createFromParcel(Parcel in) {
            HomeworkParse instance = new HomeworkParse();
            instance.assignmentID = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfHomeWork = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.homeWorksDetails = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectName = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfFinish = ((String) in.readValue((String.class.getClassLoader())));
            instance.isRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.isApprove = ((String) in.readValue((String.class.getClassLoader())));
            instance.isWorkFinish = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectId = ((String) in.readValue((String.class.getClassLoader())));
            instance.teacherProfilePicture = ((String) in.readValue((String.class.getClassLoader())));
            instance.teacherName = ((String) in.readValue((String.class.getClassLoader())));
            instance.photo = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileType = ((String) in.readValue((String.class.getClassLoader())));
            instance.gradeName = ((String) in.readValue((String.class.getClassLoader())));
            instance.divisionName = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public HomeworkParse[] newArray(int size) {
            return (new HomeworkParse[size]);
        }

    };

    public String getAssignmentID() {
        return assignmentID;
    }

    public void setAssignmentID(String assignmentID) {
        this.assignmentID = assignmentID;
    }

    public String getDateOfHomeWork() {
        return dateOfHomeWork;
    }

    public void setDateOfHomeWork(String dateOfHomeWork) {
        this.dateOfHomeWork = dateOfHomeWork;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHomeWorksDetails() {
        return homeWorksDetails;
    }

    public void setHomeWorksDetails(String homeWorksDetails) {
        this.homeWorksDetails = homeWorksDetails;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDateOfFinish() {
        return dateOfFinish;
    }

    public void setDateOfFinish(String dateOfFinish) {
        this.dateOfFinish = dateOfFinish;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIsApprove() {
        return isApprove;
    }

    public void setIsApprove(String isApprove) {
        this.isApprove = isApprove;
    }

    public String getIsWorkFinish() {
        return isWorkFinish;
    }

    public void setIsWorkFinish(String isWorkFinish) {
        this.isWorkFinish = isWorkFinish;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getTeacherProfilePicture() {
        return teacherProfilePicture;
    }

    public void setTeacherProfilePicture(String teacherProfilePicture) {
        this.teacherProfilePicture = teacherProfilePicture;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(assignmentID);
        dest.writeValue(dateOfHomeWork);
        dest.writeValue(title);
        dest.writeValue(homeWorksDetails);
        dest.writeValue(subjectName);
        dest.writeValue(dateOfFinish);
        dest.writeValue(isRead);
        dest.writeValue(isApprove);
        dest.writeValue(isWorkFinish);
        dest.writeValue(subjectId);
        dest.writeValue(teacherProfilePicture);
        dest.writeValue(teacherName);
        dest.writeValue(photo);
        dest.writeValue(fileType);
        dest.writeValue(gradeName);
        dest.writeValue(divisionName);
    }

    public int describeContents() {
        return 0;
    }

}