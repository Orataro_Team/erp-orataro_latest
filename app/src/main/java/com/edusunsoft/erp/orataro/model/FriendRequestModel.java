package com.edusunsoft.erp.orataro.model;

public class FriendRequestModel {

	private String FriendListID;
	private String RequestDate;
	private String FullName;
	private String ProfilePicture;
	private String RequestID;
	private String RequestWallID;
	private String DivisionName,GradeName,GradeID,DivisionID;

	public String getDivisionName() {
		return DivisionName;
	}

	public void setDivisionName(String divisionName) {
		DivisionName = divisionName;
	}

	public String getGradeName() {
		return GradeName;
	}

	public void setGradeName(String gradeName) {
		GradeName = gradeName;
	}

	public String getGradeID() {
		return GradeID;
	}

	public void setGradeID(String gradeID) {
		GradeID = gradeID;
	}

	public String getDivisionID() {
		return DivisionID;
	}

	public void setDivisionID(String divisionID) {
		DivisionID = divisionID;
	}

	public String getFriendListID() {
		return FriendListID;
	}

	public void setFriendListID(String friendListID) {
		FriendListID = friendListID;
	}

	public String getRequestDate() {
		return RequestDate;
	}

	public void setRequestDate(String requestDate) {
		RequestDate = requestDate;
	}

	public String getFullName() {
		return FullName;
	}

	public void setFullName(String fullName) {
		FullName = fullName;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public String getRequestID() {
		return RequestID;
	}

	public void setRequestID(String requestID) {
		RequestID = requestID;
	}

	public String getRequestWallID() {
		return RequestWallID;
	}

	public void setRequestWallID(String requestWallID) {
		RequestWallID = requestWallID;
	}

}
