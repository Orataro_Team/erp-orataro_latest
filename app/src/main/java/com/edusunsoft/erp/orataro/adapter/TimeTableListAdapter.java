package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.TimeTableModel;

import java.util.ArrayList;

public class TimeTableListAdapter extends BaseAdapter {

    private Context mContext;
    private String[] timeing = {"07:00 am  07:30 am", "07:30 am  08:00 am", "08:00 am  08:30 am", "08:30 am  09:00 am", "09:00 am  09:30 am", "09:30 am  10:30 am", "10:30 am  11:00 am", "11:00 am  11:30 am", "11:30 am  12:00 am"};
    private String[] subject = {"Maths", "Environment", "Gujrati", "English", "Break", "Hindi", "Drawing", "Science", "Maths"};
    private LayoutInflater layoutInfalater;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private ArrayList<TimeTableModel> timeTableModel;

    public TimeTableListAdapter(Context mContext, ArrayList<TimeTableModel> pTimeTableModel) {

        this.mContext = mContext;
        timeTableModel = pTimeTableModel;

    }

    @Override
    public int getCount() {
        return timeTableModel.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.timetablelistraw, parent, false);
        convertView.setBackgroundColor(colors[position % colors.length]);
        TextView txtSubject = (TextView) convertView.findViewById(R.id.txtSubject);
        TextView txtTiming = (TextView) convertView.findViewById(R.id.txtTiming);
        txtSubject.setText(timeTableModel.get(position).getSubjectName());
        txtTiming.setText(timeTableModel.get(position).getStartTime() + " " + timeTableModel.get(position).getEndTime());
        return convertView;

    }

}
