package com.edusunsoft.erp.orataro.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.SlidingTabLayout;

public class PollTeacherFragment extends Fragment {
    private View view;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    int icons[] = {R.drawable.pollico, R.drawable.voting};
    int selectedIcons[] = {R.drawable.pollico_shiw, R.drawable.voting_shoe, R.drawable.mother_show,
            R.drawable.gaurdian_show};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.groupviewfragment, container, false);

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        setupTabs();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    private void setupTabs() {
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(new BaseTabAdapter());
        mTabs = (SlidingTabLayout) view.findViewById(R.id.tabs);

        // mTabs.setCustomTabView(R.layout.custom_tab_view,
        // R.id.tabText/*,R.id.notificationCount*/);
        mTabs.setCustomTabView(R.layout.tab_img_layout, R.id.tab_name_img,
                R.id.notificationCount, 0);
        // make sure all tabs take the full horizontal screen space and divide
        // it equally amongst themselves
        mTabs.setDistributeEvenly(true);
        mTabs.setIconResourse(icons);
        mTabs.setSelectedIconResourse(selectedIcons);
        mTabs.setBackgroundColor(getResources().getColor(R.color.grey_list));
        // mTabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {

                return getResources().getColor(R.color.grp_txt_grey);

            }

        });

        // color of the tab indicator

        mTabs.setViewPager(mPager);
        mTabs.setSelectedTab(0);

    }


    public class BaseTabAdapter extends FragmentPagerAdapter implements
            SlidingTabLayout.TabIconProvider, SlidingTabLayout.TabCountProvider {
        private final int iconRes[] = {R.drawable.profileico_show, R.drawable.father,
                R.drawable.mother, R.drawable.gaurdian};
        private int count[] = {1, 2, 30, 40, 50};

        private final String iconTxt[] = {"Camera", "Viedeo"};

        public BaseTabAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

            super.registerDataSetObserver(observer);

        }

        @Override
        public Fragment getItem(int position) {

            Fragment f = null;

            if (position == 0) {
                f = MyPollFragment.newInstance(false);
                //				return new FatherProfileFragment();

            } else if (position == 1) {
                f = MyPollFragment.newInstance(true);
                //				return new FatherProfileFragment();

            } else if (position == 2) {
                f = new StudentPollFragment();
                //				return new FatherProfileFragment();

            }
            return f;
            //			else {
            //				ParentProfileActivity.type =ServiceResource.FATHER_PROFILE_INT;
            //				return new FatherProfileFragment();
            //
            //			}
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return iconTxt[position];
        }

        @Override
        public int getPageIconResId(int position) {
            return iconRes[position];
        }

        @Override
        public int getPageCount(int position) {
            // TODO Auto-generated method stub
            return count[position];
        }
    }
}
