package com.edusunsoft.erp.orataro.services;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.util.MultipleNotificationUtil;
import com.edusunsoft.erp.orataro.util.NotificationUtil;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static int notificationId = 0;

    public Bundle bundle;

    private NotificationUtil notificationUtils;


    public static final String FCM_PARAM = "associationtype";
    private static final String CHANNEL_NAME = "FCM";
    private static final String CHANNEL_DESC = "Firebase Cloud Messaging";
    private int numMessages = 0;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        notificationId++;
        FireNotification(remoteMessage);

    }

    public void FireNotification(RemoteMessage remoteMessage) {

        if (PreferenceData.getIsLogin()) {

            if (MultipleNotificationUtil.isAppIsInBackground(getApplicationContext())) {

                //playSound();
                notificationUtils = new NotificationUtil(this);
                notificationUtils.FireNotification(remoteMessage, notificationId);

//                scheduleJob(remoteMessage);

            } else {

                //playSound();
                notificationUtils = new NotificationUtil(this);
                notificationUtils.FireNotification(remoteMessage, notificationId);

            }

        }


    }


    public void playSound() {

        try {

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }


    @Override
    public void onNewToken(String token) {

        Log.e("NEW_TOKEN", token);
        PreferenceData.setToken(token);

    }

    /*\
     * Schedule a job using FirebaseJobDispatcher.
     */

    private void scheduleJob(RemoteMessage remoteMessage) {

        PreferenceData.saveFirebaseMessageTitle(remoteMessage.getNotification().getTitle());
        PreferenceData.saveFirebaseMessageMessage(remoteMessage.getNotification().getBody());

        Bundle bundle = new Bundle();

        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {

            bundle.putString(entry.getKey(), entry.getValue());

        }

//        sendNotification(Body);

//        PreferenceData.saveFirebaseMessageData(remoteMessage.getNotification().getBody());

        // [START dispatch_job]

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setLifetime(Lifetime.FOREVER)
                .setExtras(bundle)
                .setTag("my-job")
                .build();

        dispatcher.schedule(myJob);

        // [END dispatch_job]

    }


}
