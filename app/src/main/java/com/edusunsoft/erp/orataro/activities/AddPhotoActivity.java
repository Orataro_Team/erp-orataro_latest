package com.edusunsoft.erp.orataro.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.Interface.DeleteItemNewPost;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PhotoListAdapter;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingDataDao;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

public class AddPhotoActivity extends AppCompatActivity implements View.OnClickListener, DeleteItemNewPost, ResponseWebServices {

    LinearLayout ll_share, ll_fb_text, bottomlayout, ll_add_new;
    ImageView img_back, iv_post;
    TextView header_text;
    private static Context mContext;
    public GridView lv_multipleImg;
    private Dialog mPoweroffDialog;


    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;

    /*Variables for image/Photo Capture from library*/

    private String filePath, filename;
    private String[] filePathArray;
    private byte[] byteArray;
    private int fileType = -1;
    private boolean selectMultipleImage = false;
    private int imagePos = 0;
    String Path = "";

    /*END*/

    private PhotoListAdapter photo_ListAdapter;

    // intialize variable for dynamic setting
    public DynamicWallSettingDataDao dynamicWallSettingDataDao;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post_on_wall);

        Initialization();

    }

    private void Initialization() {

        mContext = AddPhotoActivity.this;
        Global.AlbumModels = new ArrayList<PhotoModel>();

        dynamicWallSettingDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).dynamicWallSettingDataDao();
        Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();

        lv_multipleImg = (GridView) findViewById(R.id.lv_multipleImg);

        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ll_fb_text = (LinearLayout) findViewById(R.id.ll_fb_text);
        bottomlayout = (LinearLayout) findViewById(R.id.bottomlayout);
        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);
        ll_add_new.setVisibility(View.VISIBLE);
        ll_fb_text.setVisibility(View.GONE);
        ll_share.setVisibility(View.GONE);
        bottomlayout.setVisibility(View.GONE);
        ll_add_new.setOnClickListener(this);

        /*findId of Header component*/

        img_back = (ImageView) findViewById(R.id.img_back);
        iv_post = (ImageView) findViewById(R.id.iv_post);
        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText(getResources().getString(R.string.AddPhoto));
        img_back.setOnClickListener(this);
        iv_post.setOnClickListener(this);

        /*END*/

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:
                finish();
                break;

            case R.id.ll_add_new:

                /*commented By Krishna : 20-05-2019 Select Image from Gallery or Capture photo From Camera */

                selectImage();

                /*END*/

                break;

            case R.id.iv_post:

                if (Global.AlbumModels == null
                        || Global.AlbumModels.size() <= 0) {

                    Utility.toast(mContext, getResources().getString(R.string.PleaseselectatleastoneImage));

                } else {

                    addPost();

                }

                break;

        }
    }

    private void addPost() {

        dynamicWallSettingDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).dynamicWallSettingDataDao();
        Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();

        Utility.getUserModelData(mContext);
        ContentValues values = new ContentValues();

        values.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId());
        values.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.BEATCH_ID, "");
        values.put(ServiceResource.POST_POSTSHARETYPE, "Public");
        values.put(ServiceResource.POST_POSTCOMMENTNOTE, filename);
        values.put(ServiceResource.POST_POSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType());

        String imgPath1 = "";
        if (Global.AlbumModels != null && Global.AlbumModels.size() > 0) {
            for (int i = 0; i < Global.AlbumModels.size(); i++) {
                if (Global.AlbumModels.size() - 1 == i) {
                    imgPath1 = imgPath1 + Global.AlbumModels.get(i).getNew_name();
                } else {
                    imgPath1 = imgPath1 + Global.AlbumModels.get(i).getNew_name() + ",";
                }
            }
        }
        values.put(ServiceResource.POST_IMAGEPATH, imgPath1);
//        if (Global.DynamicWallSetting == null) {
//            Global.DynamicWallSetting = new DynamicWallSetting();
//        }
        values.put(ServiceResource.POST_FILETYPE, "IMAGE");
        values.put(ServiceResource.POST_FILEMINETYPE, "IMAGE");
        values.put(ServiceResource.POST_APPROVED, "true");


        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId()));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTSHARETYPE, "Public"));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTCOMMENTNOTE, filename));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));
        String imgPath = "";
        if (Global.AlbumModels != null && Global.AlbumModels.size() > 0) {
            for (int i = 0; i < Global.AlbumModels.size(); i++) {
                if (Global.AlbumModels.size() - 1 == i) {
                    imgPath = imgPath + Global.AlbumModels.get(i).getNew_name();
                } else {
                    imgPath = imgPath + Global.AlbumModels.get(i).getNew_name() + ",";
                }
            }
        }

        arrayList.add(new PropertyVo(ServiceResource.POST_IMAGEPATH, imgPath));
        arrayList.add(new PropertyVo(ServiceResource.POST_FILETYPE, "IMAGE"));
        arrayList.add(new PropertyVo(ServiceResource.POST_FILEMINETYPE, "IMAGE"));
        arrayList.add(new PropertyVo(ServiceResource.POST_APPROVED, "true"));

        Log.d("getPhotoUpload", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {

            new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.POST_METHODNAME, ServiceResource.POST_URL);

        } else {

            /*commented By krishna : 05-03-2019 Remove Redirection of Each Wall Even if No Internet Connection and Add Message of No Internet Available*/

            Utility.showAlertDialog(mContext,
                    getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            /*END*/


        }
    }

    private void selectImage() {

        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {

                    dialog.dismiss();
                    Intent intent = new Intent(mContext, PreviewImageActivity.class);
                    intent.putExtra("FromIntent", "true");
                    intent.putExtra("RequestCode", 100);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {
                    dialog.dismiss();
                    Intent intent = new Intent(mContext, ImageSelectionActivity.class);
                    intent.putExtra("count", 10);
                    startActivityForResult(intent, SELECT_FILE);

                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == AddPostOnWallActivity.RESULT_OK) {

            selectMultipleImage = false;

            if (requestCode == REQUEST_CAMERA) {

                try {

                    filePath = data.getExtras().getString("imageData_uri");
                    Log.d("getFilepath", filePath);

                    File file = new File(filePath);
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(this).compressToFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                        Path = Utility.ImageCompress(filePath, mContext);
                    Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

//                    Path = Utility.ImageCompress(filePath, mContext);
//                    Utility.GetBASE64STRING(Path);
                    filename = filePath.substring(filePath.lastIndexOf("/") + 1);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*commented By Krishna -- 06-02-2019 : Get Bytearray From Captured ImagePAth from Camera*/

                fileType = 0;
                byteArray = convertImageToByteArra(filePath);

                /*END*/

                if (Utility.BASE64_STRING != null) {

                    uploadPhoto(fileType, byteArray);

                }

            } else if (requestCode == SELECT_FILE) {

                if (data != null) {

                    fileType = -1;
                    selectMultipleImage = true;
                    ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                    imagePos = 0;
                    filePathArray = new String[imgList.size()];

                    for (int k = 0; k < imgList.size(); k++) {

                        Bitmap thePic;
                        filePath = imgList.get(k).getUri().toString();
                        filePathArray[k] = filePath;
                        thePic = Utility.getBitmap(filePath, mContext);
                        byteArray = null;
                        fileType = 0;
                        filename = filePath.substring(filePath.lastIndexOf("/") + 1);
                        Log.d("getfilename", filename);

                        byteArray = convertImageToByteArra(filePath);

                        File file = new File(filePath);
                        File compressedImageFile = null;
                        try {
                            compressedImageFile = new Compressor(this).compressToFile(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                        Path = Utility.ImageCompress(filePath, mContext);
                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                        /*END*/

                        if (Utility.BASE64_STRING != null) {

                            uploadPhoto(fileType, byteArray);

                        }

                        byteArray = null;

                    }

                }

            }
        }
    }

    /**
     * get byte array from file path
     *
     * @param filePath
     * @return bytes array of image
     */
    public byte[] convertImageToByteArra(String filePath) {

        byte[] b = null;
        Bitmap thePic;
        thePic = Utility.getBitmap(filePath, mContext);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (filePath.contains(".png") || filePath.contains(".PNG")) {
            thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else {
            thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }

        b = null;
        b = stream.toByteArray();
        return b;

    }


    public void uploadPhoto(int args, byte[] byteArray1) {

        ContentValues values = new ContentValues();
        values.put(ServiceResource.PHOTO_FILENAME, new File(Utility.NewFileName).getName());

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();

        }

        values.put(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

        if (args == 0) {

            values.put(ServiceResource.PHOTO_FILETYPE, "IMAGE");

        } else if (args == 1) {

            values.put(ServiceResource.PHOTO_FILETYPE, "VIDEO");

        } else if (args == 2) {

            values.put(ServiceResource.PHOTO_FILETYPE, "FILE");

        }

        values.put(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME, new File(Utility.NewFileName).getName().replace(" ", "-")));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "IMAGE"));
        } else if (args == 1) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "VIDEO"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        if (Utility.BASE64_STRING != null)
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE, Utility.BASE64_STRING));

        Log.d("photolistreq", arrayList.toString());
        if (Utility.isNetworkAvailable(mContext)) {

            new AsynsTaskClass(mContext, arrayList, true, this, true, false).execute(
                    ServiceResource.UPLOAD_PHOTO_METHODNAME,
                    ServiceResource.ADDPHOTO_URL);

        } else {

            uploadpicResponse("");

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.UPLOAD_PHOTO_METHODNAME.equalsIgnoreCase(methodName)) {

            uploadpicResponse(result);

        }

        if (ServiceResource.POST_METHODNAME.equalsIgnoreCase(methodName)) {

            sendPushNotification();

        }

        if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            AddPhotoActivity.this.finish();

        }

    }

    public void sendPushNotification() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SENDNOTIFICATION_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

    private void uploadpicResponse(String result) {

        PhotoModel photoModel = new PhotoModel();
        String message = " ";

        if (result != null && !result.isEmpty()) {

            try {

                JSONArray obj = new JSONArray(result);
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject obj1 = obj.getJSONObject(i);
                    message = obj1.getString(ServiceResource.MESSAGE);
                }
            } catch (JSONException e) {

                e.printStackTrace();

            }

        }

        String[] newName = message.split(" ");

        if (newName.length >= 1) {

            if (newName[1] != null) {

                photoModel.setNew_name(newName[1]);

            }

        }

        if (selectMultipleImage) {

            if (imagePos < filePathArray.length) {

                photoModel.setPhoto(filePathArray[imagePos]);

            }

            imagePos++;

        } else {

            photoModel.setPhoto(filePath);

        }

        photoModel.setAlbumID(String.valueOf(fileType));
        Global.AlbumModels.add(photoModel);

        photo_ListAdapter = new PhotoListAdapter(mContext, Global.AlbumModels, 1, this);
        lv_multipleImg.setAdapter(photo_ListAdapter);
        lv_multipleImg.setVisibility(View.VISIBLE);


        byteArray = null;
    }

    @Override
    public void DeleteItem(int position) {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, false);

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog
                .findViewById(R.id.ll_submit);
        TextView tv_say_something = (TextView) mPoweroffDialog
                .findViewById(R.id.tv_say_something);

        tv_say_something.setText(getResources().getString(R.string.Areyousureyouwanttodelete));

        TextView tv_header = (TextView) mPoweroffDialog
                .findViewById(R.id.tv_header);

        tv_header.setText(getResources().getString(R.string.Delete));

        ll_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Global.AlbumModels.remove(position);

                if (Global.AlbumModels.size() == 0) {

                    byteArray = null;
                    fileType = -1;
                    filePath = "";

                }

                photo_ListAdapter.notifyDataSetChanged();
                mPoweroffDialog.dismiss();

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog.dismiss();

            }

        });
    }
}
