package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddDataByTeacherActivity;
import com.edusunsoft.erp.orataro.activities.ViewCircularDetailsActivity;
import com.edusunsoft.erp.orataro.adapter.CircularListAdapter;
import com.edusunsoft.erp.orataro.adapter.DateListAdapter;
import com.edusunsoft.erp.orataro.database.CircularListDataDao;
import com.edusunsoft.erp.orataro.database.CircularListModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.CircularModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Circular List
 *
 * @author admin
 */

public class CircularFragment extends Fragment implements OnItemClickListener, ResponseWebServices, RefreshListner {

    private PullAndLoadListView mCircularListView;
    public static CircularListAdapter adapter;
    private Context mContext;
    private DateListAdapter date_list_Adapter;
    private ListView lst_date;
    private Dialog mDialog;
    private TextView txt_nodatafound;
    private EditText edtTeacherName;
    private int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1, R.drawable.teacher_2};
    private ArrayList<CircularModel> circularModels;
    private CircularModel circularModel;
    private LinearLayout addbtnlayout;
    private String gradeId, divisionId, from, subjectId;
    public static String GRADEID = "gradeid";
    public static String DIVISIONID = "divisionid";
    public static String SUBJECTID = "subjectid";
    public static String FROM = "isFrom";

    // linearlayout for search
    LinearLayout searchlayout;

    // variable declaration for homeworklist from ofline database
    CircularListDataDao circularListDataDao;
    private List<CircularListModel> CircularList = new ArrayList<>();
    CircularListModel circularListModel = new CircularListModel();
    public String ISDataExist = "";
    /*END*/

    public static final CircularFragment newInstance(String gradeId, String divisionId, String subjectId, String from) {
        CircularFragment circularFragment = new CircularFragment();
        Bundle bdl = new Bundle();
        bdl.putString(GRADEID, gradeId);
        bdl.putString(DIVISIONID, divisionId);
        bdl.putString(SUBJECTID, subjectId);
        bdl.putString(FROM, from);
        circularFragment.setArguments(bdl);
        return circularFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.commonlist_fragment, container, false);
        mContext = getActivity();
        this.gradeId = getArguments().getString(GRADEID);
        this.divisionId = getArguments().getString(DIVISIONID);
        this.subjectId = getArguments().getString(SUBJECTID);
        this.from = getArguments().getString(FROM);

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Circular) + " (" + Utility.GetFirstName(mContext) + ")");
            }
        } catch (Exception e) {

        }

        circularListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).circularListDataDao();

        searchlayout = (LinearLayout) view.findViewById(R.id.searchlayout);

        addbtnlayout = (LinearLayout) view.findViewById(R.id.ll_add_new);
        mCircularListView = (PullAndLoadListView) view.findViewById(R.id.homeworklist);
        edtTeacherName = (EditText) view.findViewById(R.id.edtsearchStudent);
        txt_nodatafound = (TextView) view.findViewById(R.id.txt_nodatafound);

        Utility.ISLOADCIRCULAR = false;
        // Display HomeworkList from offline as well as online
        try {
            DisplayCircularList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*END*/

        // AddCircular();
        addbtnlayout.setVisibility(View.GONE);
        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
            if (Utility.isTeacher(mContext)) {
                if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsCreate()) {
                    addbtnlayout.setVisibility(View.VISIBLE);
                } else {
                    addbtnlayout.setVisibility(View.GONE);
                }

            } else {
                addbtnlayout.setVisibility(View.GONE);
            }
        }

        addbtnlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsCreate()) {
                    Intent intent = new Intent(getActivity(), AddDataByTeacherActivity.class);
                    intent.putExtra("isFrom", ServiceResource.CIRCULAR_FLAG);
                    startActivity(intent);
                } else {
                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                }

            }

        });

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);

            }
        });

        mCircularListView.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {

                // Display HomeworkList from offline as well as online
                try {
                    DisplayCircularList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/
            }

        });


        return view;

    }

    @Override
    public void onResume() {


        if (Utility.ISLOADCIRCULAR) {
            // Display CicrularList from offline as well as online
            try {
                DisplayCircularList();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        /*END*/
        } else {
        }

        super.onResume();

//        // Display HomeworkList from offline as well as online
//        try {
//            DisplayCircularList();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        /*END*/

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Circular) + "(" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void DisplayCircularList() {

        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            CircularList = circularListDataDao.getCircularList();
            if (CircularList.isEmpty() || CircularList.size() == 0 || CircularList == null) {
                ISDataExist = "true";
                Circular(true);
            } else {
                //set Adapter
                setCircularlistAdapter(CircularList);
                /*END*/
                Circular(false);
            }
        } else {
            CircularList = circularListDataDao.getCircularList();
            if (CircularList != null) {
                setCircularlistAdapter(CircularList);
            }
        }

    }

    private void setCircularlistAdapter(List<CircularListModel> circularList) {

        if (circularList != null && circularList.size() > 0) {

            searchlayout.setVisibility(View.VISIBLE);
            Collections.sort(circularList, new SortedDate());
            Collections.reverse(circularList);

            for (int i = 0; i < circularList.size(); i++) {

                if (i != 0) {

                    String temp = circularList.get(i - 1).getCr_month();

                    if (temp.equalsIgnoreCase(circularList.get(i).getCr_month())) {

                        if (circularList.get(i).getYear().equalsIgnoreCase(circularList.get(i - 1).getYear())) {
                            circularList.get(i).setVisibleMonth(false);
                        } else {
                            circularList.get(i).setVisibleMonth(true);
                        }

                    } else {

                        circularList.get(i).setVisibleMonth(true);

                    }

                } else {

                    circularList.get(i).setVisibleMonth(true);

                }

            }

        }

        if (circularList != null && circularList.size() > 0) {

            txt_nodatafound.setVisibility(View.GONE);
            adapter = new CircularListAdapter(getActivity(), circularList, this);
            mCircularListView.setAdapter(adapter);

        } else {

            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoCircularFound));
            mCircularListView.setAdapter(null);

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(mContext, ViewCircularDetailsActivity.class);
        intent.putExtra("Crname", Global.circularkModels.get(position).getCr_subject());
        intent.putExtra("Crdetails", Global.circularkModels.get(position).getCr_detail());
        intent.putExtra("Crdetails_txt", Global.circularkModels.get(position).getCr_detail_txt());
        intent.putExtra("CrTeacher_name", Global.circularkModels.get(position).getTeacher_name());
        intent.putExtra("CrTeacher_img", Global.circularkModels.get(position).getTeacher_img());
        startActivity(intent);

    }

    public void Circular(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        }

        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        Log.d("circularfragmentrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.CIRCULAR_METHODNAME, ServiceResource.CIRCULAR_URL);

    }

    public class SortedDate implements Comparator<CircularListModel> {
        @Override
        public int compare(CircularListModel o1, CircularListModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.CIRCULAR_METHODNAME.equalsIgnoreCase(methodName)) {
            parseCircular(result, ServiceResource.UPDATE);
        }

    }

    @Override
    public void refresh(String methodName) {
        if (Utility.isNetworkAvailable(getActivity())) {
            Circular(true);
        } else {
            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

    }

    public void parseCircular(String result, String update) {

        Log.d("getCircularResult", result);
        JSONArray jsonObj;
        circularListDataDao.deleteCircularListItem();
        try {

            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);

                // parse homework list data
                ParseCircularList(innerObj);
                /*END*/

            }

            if (circularListDataDao.getCircularList().size() > 0) {
                //set Adapter
                setCircularlistAdapter(circularListDataDao.getCircularList());
                /*END*/
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void ParseCircularList(JSONObject innerObj) {
        String dateParse = null;
        try {
            dateParse = innerObj
                    .getString(ServiceResource.CIRCULAR_DATEOFCIRCULAR);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (dateParse != null && !dateParse.equalsIgnoreCase("")
                && !dateParse.equalsIgnoreCase("null")) {
            String date = null;
            try {
                date = Utility
                        .getDate(
                                Long.valueOf(innerObj
                                        .getString(
                                                ServiceResource.CIRCULAR_DATEOFCIRCULAR)
                                        .replace("/Date(", "")
                                        .replace(")/", "")),
                                "EEEE/dd/MMM/yyyy/");
                String[] dateArray = date.split("/");
                if (dateArray != null && dateArray.length > 0) {
                    circularListModel.setCr_date(dateArray[1] + " " + dateArray[0].substring(0, 3));
                    circularListModel.setCr_month(dateArray[2]);
                    circularListModel.setYear(dateArray[3]);
                }

                circularListModel.setCircularID(innerObj.getString(ServiceResource.CIRCULAR_CIRCULARID));
                circularListModel.setMilliSecond(innerObj.getString(ServiceResource.CIRCULAR_DATEOFCIRCULAR).replace("/Date(", "").replace(")/", ""));
                circularListModel.setCr_detail_txt(innerObj.getString(ServiceResource.CIRCULAR_CIRCULARDETAIL));
                circularListModel.setCr_subject(innerObj.getString(ServiceResource.CIRCULAR_CIRCULARTITLE));
                circularListModel.setTeacher_name(innerObj.getString(ServiceResource.CIRCULAR_TEACHERNAME));
                circularListModel.setTeacher_img(innerObj.getString(ServiceResource.CIRCULAR_TEACHERPROFILEPIC));
                circularListModel.setSubjectID(innerObj.getString(ServiceResource.CIRCULAR_SUBJECTID));
                circularListModel.setReferenceLink(innerObj.getString(ServiceResource.CIRCULAR_REFERENCELINK));
                circularListModel.setDivisionID(innerObj.getString(ServiceResource.CIRCULAR_DIVISIONID));
                circularListModel.setGradeID(innerObj.getString(ServiceResource.CIRCULAR_GRADEID));
                circularListModel.setImgUrl(ServiceResource.BASE_IMG_URL + ServiceResource.DATAFILE + innerObj.getString(ServiceResource.PHOTOPARSE).replace(" ", "_"));
                circularListModel.setTypeTerm(innerObj.getString(ServiceResource.CIRCULAR_CIRCULARTUPETERM));
                circularListModel.setStr_dateofcircular(innerObj.getString(ServiceResource.CIRCULAR_DATE));

                String isApprove = innerObj.getString(ServiceResource.HOMEWORK_ISAPPROVED);
                if (isApprove.equalsIgnoreCase("true")) {
                    circularListModel.setAccept(true);
                } else {
                    circularListModel.setAccept(false);
                }

                String isRead = innerObj.getString(ServiceResource.HOMEWORK_ISREAD);

                if (isRead.equalsIgnoreCase("true")) {
                    circularListModel.setRead(true);
                } else {
                    circularListModel.setRead(false);
                }

                circularListDataDao.insertCircularList(circularListModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
