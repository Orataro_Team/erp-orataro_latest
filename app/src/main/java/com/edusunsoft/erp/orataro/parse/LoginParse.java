package com.edusunsoft.erp.orataro.parse;

import android.util.Log;

import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginParse {

    public static LoginModel PaserLoginModelFromJSONObject(JSONObject innerObj, String result) {

        LoginModel model = new LoginModel();

        try {

            model.setMobileNumber(innerObj.getString(ServiceResource.LOGIN_MOBILENUMBER));
            model.setStudentCode(innerObj.getString(ServiceResource.LOGIN_STUDENTCODE));
            model.setUserID(innerObj.getString(ServiceResource.LOGIN_USERID));
            model.setDisplayName(innerObj.getString(ServiceResource.LOGIN_DISPLAYNAME));
            model.setMemberID(innerObj.getString(ServiceResource.LOGIN_MEMBERID));
            model.setClientID(innerObj.getString(ServiceResource.LOGIN_CLIENTID));
            model.setERPMemberID(innerObj.getString(ServiceResource.LOGIN_ERP_MEMBER_ID));
            model.setInstituteID(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID));
            model.setRoleID(innerObj.getString(ServiceResource.LOGIN_ROLLID));
            model.setRoleName(innerObj.getString(ServiceResource.LOGIN_ROLLNAME));
            model.setWallID(innerObj.getString(ServiceResource.LOGIN_WALLID));
            model.setGradeID(innerObj.getString(ServiceResource.LOGIN_GRADEID));
            model.setDivisionID(innerObj.getString(ServiceResource.LOGIN_DIVISIONID));
            model.setInstituteCode(innerObj.getString(ServiceResource.LOGIN_INSTITUTECODE));
            model.setProfilePicture(innerObj
                    .getString(ServiceResource.LOGIN_PROFILEPIC));
            model.setWallImage(innerObj
                    .getString(ServiceResource.LOGIN_WALLIMAGE));
            model.setPrimaryName(innerObj
                    .getString(ServiceResource.LOGIN_PRIMARYNAME));
            model.setPostByType(innerObj
                    .getString(ServiceResource.LOGIN_POSTBYTYPE));
            model.setInstituteWallId(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLID));
            model.setDivisionWallId(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLID));
            model.setFullName(innerObj
                    .getString(ServiceResource.LOGIN_FULLNAME));

            String[] names = innerObj.getString(ServiceResource.LOGIN_FULLNAME).split(" ");
            if (names != null && names.length >= 0) {
                model.setFirstName(names[0]);
            }

            model.setMemberType(innerObj
                    .getString(ServiceResource.MEMBERTYPE));
            model.setInstituteName(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTENAME));
            model.setStandarWallId(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLID));
            model.setSubjectWallId(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLID));
            model.setProfileWallId(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLID));
            model.setDivisionName(innerObj
                    .getString(ServiceResource.LOGIN_DIVISIONNAME));
            model.setGradeName(innerObj
                    .getString(ServiceResource.LOGIN_GRADENAME));
            model.setInstitutionWallImage(innerObj
                    .getString(ServiceResource.LOGIN_INSTITUTIONWALLIMAGE));
            model.setPrayer(innerObj
                    .getString(ServiceResource.LOGIN_PRAYER));
            model.setInformation(innerObj
                    .getString(ServiceResource.LOGIN_INFORMATION));
            model.setSchoolTiming(innerObj.getString(ServiceResource.LOGIN_SCHOOLTIMING));

            if (innerObj.getString(ServiceResource.STUDENTHOSTELREGID).equalsIgnoreCase("") || innerObj.getString(ServiceResource.STUDENTHOSTELREGID).equalsIgnoreCase("null")
                    || innerObj.getString(ServiceResource.STUDENTHOSTELREGID).equalsIgnoreCase(null)) {
                model.setStudentHostelRegID("");
            } else {
                model.setStudentHostelRegID(innerObj.getString(ServiceResource.STUDENTHOSTELREGID));
            }

//            if (innerObj.getString(ServiceResource.STUDENTBUSREGMASTERID).equalsIgnoreCase("") || innerObj.getString(ServiceResource.STUDENTBUSREGMASTERID).equalsIgnoreCase("null")
//                    || innerObj.getString(ServiceResource.STUDENTBUSREGMASTERID).equalsIgnoreCase(null)) {
//                model.setStudentBusRegMasterID(null);
//            } else {
//                model.setStudentBusRegMasterID(innerObj.getString(ServiceResource.STUDENTBUSREGMASTERID));
//            }
            model.setStudentBusRegMasterID(innerObj.getString(ServiceResource.STUDENTBUSREGMASTERID));


            try {

                model.setBatchStart(innerObj
                        .getString(ServiceResource.BATCHSTART).replace("/Date(", "").replace(")/", ""));
                model.setBatchEnd(innerObj
                        .getString(ServiceResource.BATCHEND).replace("/Date(", "").replace(")/", ""));

            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d("getBatchID000", innerObj.getString(ServiceResource.BATCHID));
            model.setBatchID(innerObj.getString(ServiceResource.BATCHID));

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTSTATUS).equalsIgnoreCase("true")) {
                model.setIsAllowUserToPostStatus("true");
            } else {
                model.setIsAllowUserToPostStatus("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTCOMMENT).equalsIgnoreCase("true")) {
                model.setIsAllowUserToPostComment("true");
            } else {
                model.setIsAllowUserToPostComment("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTALBUM).equalsIgnoreCase("true")) {
                model.setAllowUserToPostAlbum("true");
            } else {
                model.setAllowUserToPostAlbum("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTPHOTO).equalsIgnoreCase("true")) {
                model.setIsAllowUserToPostPhoto("true");
            } else {
                model.setIsAllowUserToPostPhoto("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTVIDEO).equalsIgnoreCase("true")) {
                model.setIsAllowUserToPostVideo("true");
            } else {
                model.setIsAllowUserToPostVideo("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOPOSTFILES).equalsIgnoreCase("true")) {
                model.setIsAllowUserToPostFiles("true");
            } else {
                model.setIsAllowUserToPostFiles("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOSHAREPOST).equalsIgnoreCase("true")) {
                model.setIsAllowUserToSharePost("true");
            } else {
                model.setIsAllowUserToSharePost("false");
            }

            if (innerObj.getString(ServiceResource.ISALLOWUSERTOLIKEDISLIKES).equalsIgnoreCase("true")) {
                model.setIsAllowUserToLikeDislikes("true");
            } else {
                model.setIsAllowUserToLikeDislikes("false");
            }

            model.setInstituteLogo(ServiceResource.BASE_IMG_URL1 + innerObj.getString(ServiceResource.LOGIN_INSTITUTELOGO).replaceAll("//img/", "img/"));

            if (model.getInstitutionWallImage() != null && !model.getInstitutionWallImage().equals("")) {
                if (model.getInstitutionWallImage().contains("/img/"))
                    model.setInstitutionWallImage(ServiceResource.BASE_IMG_URL
                            + model
                            .getInstitutionWallImage());
                else if (model.getInstitutionWallImage().contains("/AvtarImages/"))
                    model.setInstitutionWallImage(ServiceResource.BASE_IMG_URL
                            + model
                            .getInstitutionWallImage()
                            .replaceAll(
                                    "//AvtarImages/",
                                    "AvtarImages/"));

                else
                    model
                            .setInstitutionWallImage(ServiceResource.BASE_IMG_URL
                                    + "/DataFiles/"
                                    + model
                                    .getInstitutionWallImage()
                                    .replaceAll(
                                            "DataFiles/",
                                            ""));
            }

            if (model
                    .getMemberType()
                    .contains(
                            ServiceResource.USER_STUDENT_STRING)) {
                model
                        .setUserType(ServiceResource.USER_STUDENT_INT);

            } else if (model
                    .getMemberType()
                    .contains(
                            ServiceResource.USER_TEACHER_STRING)) {
                model
                        .setUserType(ServiceResource.USER_TEACHER_INT);

            } else if (model
                    .getMemberType()
                    .contains(
                            ServiceResource.USER_PARENTS_STRING)) {
                model.setUserType(ServiceResource.USER_PARENTS_INT);
            }

            String url = "";
            if (model.getProfilePicture().contains(".png")) {
                url = Utility.getProfilePic(model.getUserID() + ".png");
            } else {
                url = Utility.getProfilePic(model.getUserID() + ".jpg");
            }
            model.setBtmProfilepic(url);
            model.setLoginJson(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;

    }

}
