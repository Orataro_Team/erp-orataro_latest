package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class CircularModel implements Serializable {

	private String cr_date;
	private String imgUrl;
	private String cr_subject;
	private int cr_detail;
	private String year;
	private String milliSecond;
		private String SubjectID,DivisionID,GradeID,CircularID,typeTerm;
	private boolean isAccept;
	private String cr_detail_txt;
	private String teacher_name;
	private String teacher_img;
	private boolean isVisibleMonth = false;
	private String cr_month;
	private String ReferenceLink;
	private String str_dateofcircular;
	private boolean IsRead;

	public String getReferenceLink() {
		return ReferenceLink;
	}

	public void setReferenceLink(String referenceLink) {
		ReferenceLink = referenceLink;
	}

	public String getStr_dateofcircular() {
		return str_dateofcircular;
	}

	public void setStr_dateofcircular(String str_dateofcircular) {
		this.str_dateofcircular = str_dateofcircular;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public boolean isAccept() {
		return isAccept;
	}

	public void setAccept(boolean isAccept) {
		this.isAccept = isAccept;
	}

	public String getTypeTerm() {
		return typeTerm;
	}

	public void setTypeTerm(String typeTerm) {
		this.typeTerm = typeTerm;
	}

	public boolean isIsRead() {
		return IsRead;
	}

	public void setIsRead(boolean isRead) {
		IsRead = isRead;
	}

	public String getCircularID() {
		return CircularID;
	}

	public void setCircularID(String circularID) {
		CircularID = circularID;
	}

	public String getSubjectID() {
		return SubjectID;
	}

	public void setSubjectID(String subjectID) {
		SubjectID = subjectID;
	}

	public String getDivisionID() {
		return DivisionID;
	}

	public void setDivisionID(String divisionID) {
		DivisionID = divisionID;
	}

	public String getGradeID() {
		return GradeID;
	}

	public void setGradeID(String gradeID) {
		GradeID = gradeID;
	}

	public String getMilliSecond() {
		return milliSecond;
	}

	public void setMilliSecond(String milliSecond) {
		this.milliSecond = milliSecond;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCr_detail_txt() {
		return cr_detail_txt;
	}

	public void setCr_detail_txt(String cr_detail_txt) {
		this.cr_detail_txt = cr_detail_txt;
	}

	public String getCr_date() {
		return cr_date;
	}

	public void setCr_date(String cr_date) {
		this.cr_date = cr_date;
	}

	public String getCr_subject() {
		return cr_subject;
	}

	public void setCr_subject(String cr_subject) {
		this.cr_subject = cr_subject;
	}

	public int getCr_detail() {
		return cr_detail;
	}

	public void setCr_detail(int cr_detail) {
		this.cr_detail = cr_detail;
	}

	public String getTeacher_name() {
		return teacher_name;
	}

	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}

	public String getTeacher_img() {
		return teacher_img;
	}

	public void setTeacher_img(String teacher_img) {
		this.teacher_img = teacher_img;
	}

	public boolean isVisibleMonth() {
		return isVisibleMonth;
	}

	public void setVisibleMonth(boolean isVisibleMonth) {
		this.isVisibleMonth = isVisibleMonth;
	}

	public String getCr_month() {
		return cr_month;
	}

	public void setCr_month(String cr_month) {
		this.cr_month = cr_month;
	}

	@Override
	public String toString() {
		return "CircularModel{" +
				"cr_date='" + cr_date + '\'' +
				", imgUrl='" + imgUrl + '\'' +
				", cr_subject='" + cr_subject + '\'' +
				", cr_detail=" + cr_detail +
				", year='" + year + '\'' +
				", milliSecond='" + milliSecond + '\'' +
				", SubjectID='" + SubjectID + '\'' +
				", DivisionID='" + DivisionID + '\'' +
				", GradeID='" + GradeID + '\'' +
				", CircularID='" + CircularID + '\'' +
				", typeTerm='" + typeTerm + '\'' +
				", isAccept=" + isAccept +
				", cr_detail_txt='" + cr_detail_txt + '\'' +
				", teacher_name='" + teacher_name + '\'' +
				", teacher_img='" + teacher_img + '\'' +
				", isVisibleMonth=" + isVisibleMonth +
				", cr_month='" + cr_month + '\'' +
				", IsRead=" + IsRead +
				'}';
	}
}
