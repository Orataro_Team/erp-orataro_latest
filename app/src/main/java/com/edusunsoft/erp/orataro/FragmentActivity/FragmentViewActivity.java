package com.edusunsoft.erp.orataro.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.AlbumFragment;
import com.edusunsoft.erp.orataro.fragments.PhotoVideoOnlyFragment;
import com.edusunsoft.erp.orataro.fragments.ReciptListFragment;
import com.edusunsoft.erp.orataro.model.FeesReciptModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

public class FragmentViewActivity extends FragmentActivity {
    ImageView img_back, img_menu;
    TextView header_text;
    String isFrom = "";
    FeesReciptModel.Table reciptModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_view);

        isFrom = getIntent().getStringExtra("isFrom");
        reciptModel = getIntent().getParcelableExtra("model");
        img_back = (ImageView) findViewById(R.id.img_back);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        header_text = (TextView) findViewById(R.id.header_text);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.PHOTO)) {

            header_text.setText(getResources().getString(R.string.photo) + " (" + Utility.GetFirstName(FragmentViewActivity.this) + ")");
            ft.replace(R.id.homeworkActivity_containair,
                    PhotoVideoOnlyFragment.newInstance(ServiceResource.IMAGE));

        } else if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.VIDEO)) {
            header_text.setText(getResources().getString(R.string.Video) + " (" + Utility.GetFirstName(FragmentViewActivity.this) + ")");
            ft.replace(R.id.homeworkActivity_containair,
                    PhotoVideoOnlyFragment.newInstance(ServiceResource.VIDEO));
        } else if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.FILE)) {
            header_text.setText(getResources().getString(R.string.File) + " (" + Utility.GetFirstName(FragmentViewActivity.this) + ")");
            ft.replace(R.id.homeworkActivity_containair,
                    PhotoVideoOnlyFragment.newInstance(ServiceResource.FILE));
        } else if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.RECIPTDETAIL)) {
            header_text.setText(getResources().getString(R.string.reciptdetail) + " (" + Utility.GetFirstName(FragmentViewActivity.this) + ")");
            ft.replace(R.id.homeworkActivity_containair,
                    ReciptListFragment.newInstance("", ServiceResource.RECIPTDETAIL, reciptModel));
        } else if (isFrom != null && isFrom.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
            header_text.setText(getResources().getString(R.string.Album));
            try {
                header_text.setText(getResources().getString(R.string.PhotosAlbum) + " (" + Utility.GetFirstName(FragmentViewActivity.this) + ")");
            } catch (Exception e) {
                e.printStackTrace();
            }

            ft.replace(R.id.homeworkActivity_containair,
                    AlbumFragment.newInstance(ServiceResource.PROFILEWALL));
        }

        ft.commit();

        img_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

}
