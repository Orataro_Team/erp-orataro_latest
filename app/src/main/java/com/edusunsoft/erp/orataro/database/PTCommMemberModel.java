package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "PTCommMemberModel")
public class PTCommMemberModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "MemberID")
    @SerializedName("MemberID")
    public String MemberID;

    @ColumnInfo(name = "FullName")
    @SerializedName("FullName")
    public String FullName;

    @ColumnInfo(name = "RegistrationNo")
    @SerializedName("RegistrationNo")
    public String RegistrationNo;

    @ColumnInfo(name = "ProfilePicture")
    @SerializedName("ProfilePicture")
    public String ProfilePicture;

    @ColumnInfo(name = "SubjectName")
    @SerializedName("SubjectName")
    public String SubjectName;

    @ColumnInfo(name = "SubjectID")
    @SerializedName("SubjectID")
    public String SubjectID;

    @ColumnInfo(name = "MemberType")
    @SerializedName("MemberType")
    public String MemberType;

    @ColumnInfo(name = "RollNo")
    @SerializedName("RollNo")
    public String RollNo;

    @ColumnInfo(name = "ForMember")
    @SerializedName("ForMember")
    public String ForMember;

    public PTCommMemberModel() {
    }

    public String getForMember() {
        return ForMember;
    }

    public void setForMember(String forMember) {
        ForMember = forMember;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getRegistrationNo() {
        return RegistrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        RegistrationNo = registrationNo;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getMemberType() {
        return MemberType;
    }

    public void setMemberType(String memberType) {
        MemberType = memberType;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    @Override
    public String toString() {
        return "StudentMemberModel{" +
                "id=" + id +
                ", MemberID='" + MemberID + '\'' +
                ", FullName='" + FullName + '\'' +
                ", RegistrationNo='" + RegistrationNo + '\'' +
                ", ProfilePicture=" + ProfilePicture +
                ", SubjectName='" + SubjectName + '\'' +
                ", SubjectID='" + SubjectID + '\'' +
                ", MemberType='" + MemberType + '\'' +
                ", RollNo='" + RollNo + '\'' +
                '}';
    }
}
