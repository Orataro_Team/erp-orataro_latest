package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AboutUsActivity;
import com.edusunsoft.erp.orataro.activities.CustomerSupportActivity;
import com.edusunsoft.erp.orataro.activities.PortPolioActivity;
import com.edusunsoft.erp.orataro.adapter.AboutusAdapter;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.List;

public class AboutUsFragment extends Fragment {

    private ListView lv_aboutus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.aboutus, null);
        lv_aboutus = (ListView) v.findViewById(R.id.lv_aboutus);

        AboutusAdapter adapter = new AboutusAdapter(getActivity());
        lv_aboutus.setAdapter(adapter);

        lv_aboutus.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    PackageManager pm = getActivity().getPackageManager();
                    PackageInfo pInfo = null;
                    String packageName = "";
                    try {
                        pInfo = pm.getPackageInfo(getActivity().getPackageName(), 0);
                        packageName = getActivity().getApplicationContext().getPackageName();
                    } catch (PackageManager.NameNotFoundException e1) {
                        e1.printStackTrace();
                    }

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + packageName);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                    getActivity().overridePendingTransition(0, 0);

                } else if (position == 1) {
                    PackageManager pm = getActivity().getPackageManager();
                    PackageInfo pInfo = null;
                    String packageName = "";
                    try {
                        pInfo = pm.getPackageInfo(getActivity().getPackageName(), 0);
                        packageName = getActivity().getApplicationContext().getPackageName();
                    } catch (PackageManager.NameNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                    getActivity().overridePendingTransition(0, 0);
                } else if (position == 2) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=sunsoft%20eduware%20Solutions%20LLP&c=apps")));
                        getActivity().overridePendingTransition(0, 0);
                    } catch (Exception e) {
                        Intent i = new Intent(getActivity(), PortPolioActivity.class);
                        startActivity(i);
                        getActivity().overridePendingTransition(0, 0);
                    }
                } else if (position == 3) {
                    startActivity(getOpenFacebookIntent(getActivity()));
                    getActivity().overridePendingTransition(0, 0);
                } else if (position == 4) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://aboutorataro?screen_name=" + "orataroapp")));
                        getActivity().overridePendingTransition(0, 0);
                    } catch (Exception e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + "orataroapp")));
                        getActivity().overridePendingTransition(0, 0);
                    }
                } else if (position == 5) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://orataro"));
                    final PackageManager packageManager = getActivity().getPackageManager();
                    final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                    if (list.isEmpty()) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/company/orataro"));
                    }
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                }

//                else if (position == 6) {
//                    try {
//                        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/communities/106006930296339413383"));
//                        intent.setPackage("com.google.android.apps.plus");
//                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
//                            startActivity(intent);
//                            getActivity().overridePendingTransition(0, 0);
//                        }
//                    } catch (Exception e) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/106006930296339413383")));
//                        getActivity().overridePendingTransition(0, 0);
//                    }
//                }
                else if (position == 6) {

                    Intent i = new Intent(getActivity(), AboutUsActivity.class);
                    startActivity(i);
                    getActivity().overridePendingTransition(0, 0);

                } else if (position == 7) {

                    Intent i = new Intent(getActivity(), CustomerSupportActivity.class);
                    startActivity(i);
                    getActivity().overridePendingTransition(0, 0);

                }


            }


        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Aboutorataro) + " (" + Utility.GetFirstName(getActivity()) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Intent getOpenFacebookIntent(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/890013191086690"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ORATARO/"));
        }
    }
}