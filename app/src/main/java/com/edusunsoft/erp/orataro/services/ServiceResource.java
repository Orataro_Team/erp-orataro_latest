package com.edusunsoft.erp.orataro.services;

import java.util.List;

public class ServiceResource {

    /* MultiDB URL*/
//    public static final String BASE_URL = "http://multidborataro.orataro.com/Services/";
//    public static final String BASE_URL_BETA = BASE_URL;
//    public static final String ERP_URL = "http:/multidberp.orataro.com/#";
//    public static final String BASE_URL_ERP = "http://multidberp.orataro.com/Services/ORATARO/";
//    public static final String BASE_IMG_URL = "http://multidborataro.orataro.com/";
//    public static final String BASE_IMG_URL1 = "http://multidberp.orataro.com/Files/";
//    public static final String CSSFILE = "\"http://multidborataro.orataro.com/Content/bootstrap.css\"";
//    public static final String FEESRECEIPTURL = "http://multidberp.orataro.com/mobile_api/FeesReceipt";
    /*END*/

    //ALL LIVE URL
    public static final String BASE_URL = "http://erporataro.orataro.com/Services/";
    public static final String BASE_URL_BETA = BASE_URL;
    public static final String ERP_URL = "http:/erp.orataro.com/#";
    public static final String BASE_URL_ERP = "http://erp.orataro.com/Services/ORATARO/";
    public static final String BASE_IMG_URL = "http://erporataro.orataro.com/";
    public static final String BASE_IMG_URL1 = "http://erp.orataro.com/Files/";
    public static final String CSSFILE = "\"http://erporataro.orataro.com/Content/bootstrap.css\"";
    public static final String FEESRECEIPTURL = "http://erp.orataro.com/mobile_api/FeesReceipt";
    /*END*/


    //ALL TEST URL
//	public static final String BASE_URL = "http://erporataro.orataro.com/Services/";
//	public static final String BASE_URL_BETA = BASE_URL;
//	public static final String ERP_URL ="http://erporataro.orataro.com";
//	public static final String BASE_URL_ERP ="http://erporataro.orataro.com/Services/ORATARO/";
//	public static final String BASE_IMG_URL = "http://erporataro.orataro.com/";
//	public static final String BASE_IMG_URL_WITHOUT = "http://erporataro.orataro.com";
//	public static final String CSSFILE = "\"http://erporataro.orataro.com/Content/bootstrap.css\"";

/*	//ALL BETA TEST URL : http://beta.orataro.com
	public static final String BASE_URL = "http://beta.orataro.com/Services/";
	public static final String BASE_URL_BETA = BASE_URL;
	public static final String ERP_URL ="http://beta.sunsofteduware.com";
	public static final String BASE_URL_ERP ="http://beta.sunsofteduware.com/Services/ORATARO/";
	public static final String BASE_IMG_URL = "http://beta.orataro.com/";
	public static final String BASE_IMG_URL_WITHOUT = "http://beta.orataro.com";
	public static final String CSSFILE = "\"http://beta.orataro.com/Content/bootstrap.css\"";*/

    public static final boolean SINGLE_DEVICE_FLAG = false;
    public static final int POSTLIMIT = 300;
    public static final String FROMLOGIN = "fromLogin";
    public static final long ANIMATIONDURATION = 500;
    public static final boolean isShowLog = true;
    public static final String DATAFILE = "DataFiles/";
    public static final String STATICINSTITUTEID = "4F4BBF0E-858A-46FA-A0A7-BF116F537653,4f4bbf0e-858a-46fa-a0a7-bf116f537653,3ccb88d9-f4bf-465d-b85a-5402871a0144,3CCB88D9-F4BF-465D-B85A-5402871A0144,678b9089-dd88-4fda-a781-1b3264a3b6e0,678B9089-DD88-4FDA-A781-1B3264A3B6E0,c9999483-0557-4498-90c5-127ec78ecab2,C9999483-0557-4498-90C5-127EC78ECAB2,5820c317-00bc-41ae-afc2-4409ae07a651,5820C317-00BC-41AE-AFC2-4409AE07A651";//4f4bbf0e-858a-46fa-a0a7-bf116f537653
    public static final String GURUKULINSTITUTEID = "fca54e19-7d1c-47db-a134-1fbb4e82f897,FCA54E19-7D1C-47DB-A134-1FBB4E82F897";
    public static final String TOASTPERMISSIONMSG = "Sorry , You do not have permission to visit this section.";

    public static final String DISPLAY_MESSAGE_ACTION = "com.sqt.preschool.DISPLAY_MESSAGE_ACTION";
    public static final String SENDERID = "917033482527";
    public static final String IMAGE_DIRECTORY_NAME = "Hello Camera";
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int USER_TEACHER_INT = 1;
    public static final int USER_PARENTS_INT = 2;
    public static final int USER_STUDENT_INT = 3;
    public static final int FATHER_PROFILE_INT = 1;
    public static final int MOTHER_PROFILE_INT = 2;
    public static final int GARDIAN_PROFILE_INT = 3;
    public static final String PRAYER_STRING = "Prayer";
    public static final String INFORMATION_STRING = "Information";
    public static final String SCHOOLTIMEING_STRING = "SchoolTiming";
    public static final String TIMETABLE_STRING = "TimeTable";
    public static final String OTHER_STRING = "Other";
    public static final String USER_TEACHER_STRING = "Teacher";
    public static final String USER_PARENTS_STRING = "Parent";
    public static final String USER_STUDENT_STRING = "Student";

    public static final String IMAGE = "IMAGE";
    public static final String VIDEO = "video";
    public static final String MP3 = "mp3";

    public static final String RECIPT = "Recipt";
    public static final String RECIPTDETAIL = "REciptDetail";
    public static final String STANDERD_WSCALL_TYPE_GRADE = "Grade";
    public static final String STANDERD_WSCALL_TYPE_DIVISION = "Division";
    public static final String STANDERD_WSCALL_TYPE_SUBJECT = "Subject";

    public static final String EXTRA_REG_MODE = "regmode";
    public static final String EXTRA_USER_NAME = "username";
    public static final String EXTRA_USER_PASSWORD = "password";
    public static final String EXTRA_IS_FROM = "isFrom";
    public static final String EXTRA_MOBILE = "MOBILE";


    public static final String FORGOTPASS = "forgotPassword";
    public static final String FORGOTPASS_AUTH = "forgotPasswordAuth";
    public static final String CONTINUE_PASS = "continuePass";

    public static final String NOTIFICATION_FLAG = "notification";
    public static final String CLASSWORK_FLAG = "classwork";
    public static final String INFORMATION_FLAG = "Information";
    public static final String SCHOOLPRAYER_FLAG = "schoolPrayer";
    public static final String HOMEWORK_FLAG = "homework";
    public static final String HAPPYGRAM_FLAG = "myhappygram";
    public static final String PTCOMMUNICATION_FLAG = "ptcommunication";
    public static final String CLASSWORK_FLAG_TEACHER = "classwork";
    public static final String HOMEWORK_FLAG_TEACHER = "homework";
    public static final String NOTE_FLAG_TEACHER = "Noteteacher";
    public static final String CIRCULAR_FLAG = "circular";
    public static final String TODO_FLAG = "todo";
    public static final String SCHOOLGROUP_FLAG = "schoolgroup";
    public static final String PROJECT_FLAG = "project";
    public static final String LEAVE_FLAG = "leave";
    public static final String STUDENT_INFORMATION = "studentInformation";

    public static final String MESSAGE = "message";
    public static final String OTP_ID = "id";
    public static final String WALLID = "WallID";
    public static final String CLIENT_ID = "ClientID";
    public static final String USER_ID = "UserID";
    public static final String PhotoDelete_ID = "id";
    public static final String USERTYPE = "UserType";
    public static final String BEATCH_ID = "BeachID";
    public static final String INSTITUTEID = "InstituteID";
    public static final String INSTITUTIONWALLID = "InstitutionWallID";
    public static final String MEMBERID = "MemberID";
    public static final String GRADEID = "GradeID";
    public static final String GRADEDIVISOINID = "GradeDivisoinID";
    public static final String GRADEDIVISOINWALLID = "GradeDivisoinIDWallID";

    // strings to get students from std and div in circular
    public static final String STANDARDDIVISION = "standardpipedivision";

    /**
     * STUDENT LIST FROM STD and DIV FOR CIRCULAR
     */
    public static final String GETSTUDENTLIST = "getstudentlist";

    // strings for student list
    public static final String STUDENT_MEMBERID = "MemberID";
    public static final String STUDENT_FULLNAME = "FullName";
    public static final String STUDENT_REGNO = "RegistrationNo";
    public static final String STUDENT_GRADENAME = "GradeName";
    public static final String STUDENT_DIVISIONNAME = "DivisionName";
    public static final String STUDENT_GRADEID = "GradeID";
    public static final String STUDENT_DIVID = "DivisionID";
    public static final String STUDENT_FNAME = "FName";
    public static final String STUDENT_FCONTACTNO = "FContactNo";

    // multidb connnection string string
    public static final String CTOKEN = "ctoken";
    public static final String SQLCONSTR = "SQLConStr";

    public static final String DIVISIONID = "DivisionID";
    public static final String TEACHERID = "TeacherID";
    public static final String SUBJECTID = "SubjectID";
    public static final String DIVISIONNAMETIMETABLE = "DivisionName";
    public static final String GRADENAMETIMETABLE = "GradeName";
    public static final String GROUPID = "GroupID";
    public static final String CATEGORY = "Category";
    public static final String TABLE = "Table";
    public static final String TABLE1 = "Table1";
    public static final String TABLE2 = "Table2";
    public static final String TABLE3 = "OnLineTransactionDetails";

    public static final String FROMDASHBOARD = "fromDashboard";
    public static final String EDITID = "EditID";
    public static final String TYPE = "Type";
    public static final String TODOID = "TodoID";
    public static final String TEACHERMEMBERID = "TeacherMemberID";
    public static final String HAPPYGRAMDATALIST = "HappyGramDataList";
    public static final String PHOTOPARSE = "Photo";
    public static final String GRADENAME = "GradeName";
    public static final String DIVISIONNAME = "DivisionName";
    public static final String LEAVECOUNT = "leaveCount";
    public static final String STRDATEOFCLASSWORK = "strDateOfClasswork";

    // make payment
    public static final String U = "U";
    public static final String S = "S";
    public static final String B = "B";
    public static final String C = "C";
    public static final String I = "I";
    public static final String FS = "FS";
    public static final String FSS = "FSS";
    public static final String a = "a";
    public static final String PGM = "PGM";
    public static final String REQUESTFROM = "RequestFrom";
    public static final String USERDISPLAYNAME = "UserDisplayName";
    public static final String PAY_AMT = "PayAmt"; // add by hardik_kanak 26-11-2020

    // Country Code

    public static final String COUNTRY_CODE = "TermCode";
    public static final String COUNTRY_NAME = "Term";
    public static final String COUNTRY_CATEGORY = "CountryCode";
    public static final String COUNTRY_ID = "00000000-0000-0000-0000-000000000000";

    /*END*/

    /**
     * ADD EVENT
     */

    public static final String GRADEIDDIVISIONID = "GradeIDDivisionID";
    public static final String EVENTTYPE = "EventType";
    public static final String REMINDERDATE = "ReminderDate";
    public static final String EVENTTITLE = "Title";
    public static final String EVENTDETAILS = "Details";
    public static final String DATE = "Date";

    /**
     * FROMSTR
     */

    public static String FROMACTIVITY = "";

    /**
     * FORGOTPASS
     */

    public static final String PUSHNOTIFICATIONFORAPPVERSION_METHODNAME = "PushNotificationForAppVersion";
    public static final String PUSHNOTIFICATIONFORAPPVERSION_BY_NAME_METHODNAME = "PushNotificationForAppVersionByAppName";
    public static final String APP_PACKAGE_NAME = "AppName";


    public static final String FORGOTPASS_UPDATEUSERPASSWORD = "UpdateUserPassword";

    public static final String CHANGEPASSWORD = "ChangePassword";
    public static final String CHANGEPASSWORD_ROLENAME = "RoleName";
    public static final String CHANGEPASSWORD_STUDENTCODE = "StudentCode";
    public static final String CHANGEPASSWORD_NEWPASS = "NewPass";
    public static final String CHANGEPASSWORD_OLDPASS = "OldPass";
    public static final String CHANGEPASSWORD_MOBILENUMBER = "MobileNumber";

    public static final String FORGOTPASS_CHECKSENTOTPDATA = "CheckSentOTPData";
    public static final String FORGOTPASS_NEWFOGOTPASSWORDCHECKUSERDATA = "NewFogotPasswordCheckUserData";
    public static final String FORGOTPASSFORGOTMODE = "ForgotMode";
    public static final String FORGOTPASSMOBILENUMBER = "MobileNumber";
    public static final String FORGOTPASSGRNUMBER = "GRNumber";
    public static final String FORGOTPASSOTPPASSWORD = "OTPPassword";
    public static final String FORGOTPASSNEWPASSWORD = "NewPassword";

    public static final String BLOGLIST = "blogs";
    public static final String GETBLOGLIST_METHODNAME = "GetBlogList";
    public static final String GETBLOGDETAIL_METHODNAME = "GetBlogDetail";
    public static final String BLOG_URL = BASE_URL_BETA + "apk_blogs.asmx";

    public static final String BLOGDETAILS = "BlogDetails";
    public static final String BLOG_BLOGID = "BlogID";
    public static final String BLOG_BLOGSID = "BlogsID";
    public static final String BLOG_BLOGTITLE = "BlogTitle";
    public static final String BLOG_BLOGIMAGE = "BlogImage";
    public static final String BLOG_BLOGDATE = "CreateOn";

    /**
     * Registation
     */

    public static final String CHECKUSERMOBILENUMBERFORREGISTRATION_METHODNAME = "CheckUserMobileNumberForRegistration";
    public static final String USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP_MANUALLY = "User_RegistrationAndLoginByOTP_SetOTP";
    public static final String USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP = "User_RegistrationAndLoginByOTP_SetOTPFireBase";

    public static final String REGISTATION_URL = BASE_URL_BETA + "apk_registration.asmx";
    public static final String MOBILENUMBER = "MobileNumber";
    public static final String DIVISREGID = "DivisRegID";
    public static final String PASSWORD = "Password";

    public static final String DEVICE_IDENTITY = "DeviceIdentity";
    public static final String DEVICE_TYPE = "DeviceTypeTerm";

    /**
     * Auth Regiaster
     */
    public static final String USERMOBILENUMBERREGISTRATION = "UserMobileNumberRegistration";
    public static final String USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN = "User_RegistrationAndLoginByOTP_Login";
    public static final String USER_SELECTION = "userselection";
    public static final String DIVISREGID_REGISTER = "DivisRegID";
    public static final String DIVITYPE = "DiviType";
    public static final String OTPNumber = "OTPNumber";

    /**
     * Hostel Pocket Money
     */

    public static final String HOSTELPOCKETMONEY_URL = BASE_URL_ERP + "apk_Hostel.asmx";
    public static final String POCKET_AMOUNT = "Amount";
    public static final String TRANSACTIONTYPE = "TransactionType_Term";
    public static final String NARRATION = "Narration";
    public static final String ASSOCIATIONTYPE = "AssociationType";
    public static final String DATEOFTRANSACTION = "strDateOfTransaction";
    //    public static final String DATEOFTRANSACTION = "DateOfTransaction";
    public static final String STUDENTNAME = "StudentName";
    public static final String BUILDINGFLOORROOM = "BuildingFloorRoom";
    public static final String ACCOUNTNO = "AcctNo";
    public static final String OPENINGBALANCE = "OpeningBalance";
    public static final String CURRENTBALANCE = "CurrentBalance";
    public static final String SEQNO = "SeqNo";
    public static Double TOTALBALANCE = 0.0;


    /**
     * ADD CLASSWORK
     */

    public static final String ADDCLASSWORK = BASE_URL_BETA + "apk_classwork.asmx";
    //    public static final String ADDCLASSWORK_METHODNAME = "CreateClassWork";
    public static final String ADDCLASSWORK_METHODNAME = "CreateClassWorkByBase64STR";
    public static final String TITLE = "title";
    public static final String REFERANCELINK = "ReferenceLink";
    public static final String DATEOFCLASSWORK = "DateOfClassWork";
    public static final String DATEOFCOMPLETION = "DateOfCompletion";
    public static final String STARTTIME = "StartTime";
    public static final String FILE = "File";
    public static final String FILENAME = "FileName";
    public static final String ENDTIME = "EndTime";
    public static final String FILETYPE = "FileType";
    public static final String FILEMINETYPE = "FileMineType";
    public static final String CLASSWORKDETAIL = "ClassWorksDetails";

    /**
     * ADD Circular
     */

    // change api to add reference link
//    public static final String CREATECIRCULARWITHMULTY_METHODNAME = "CreateCircularWithMulty";
//    public static final String CREATECIRCULARWITHMULTY_METHODNAME = "CreateCircularWithMulty_New";
    public static final String CREATECIRCULARWITHMULTY_METHODNAME = "CreateCircularWithMultyByBase64STRMemberSelection_New";
    public static final String DATEOFCIRCULAR = "DateOfCircular";
    public static final String CIRCULAR_TYPE = "CircularType";
    public static final String CIRCULAR_DETAIL = "CircularDetails";

    /**
     * ADD HOMEWORK
     */

    // chnage api methodname to add ref link
//    public static final String ADDHOMEWORK_METHODNAME = "CreateHomework_New";
    public static final String ADDHOMEWORK_METHODNAME = "CreateHomeworkByBase64STR_New";
//    public static final String ADDHOMEWORK_METHODNAME = "CreateHomework";

    public static final String ADDHOMEWORK = BASE_URL_BETA + "apk_homework.asmx";
    public static final String DATEOFHOMEWORK = "DateOfHomeWork";
    public static final String DATEOFFINISH = "DateOfFinish";
    public static final String HOMEWORKDETAIL = "HomeWorksDetails";

    /**
     * ATTENDANCE
     */

    public static final String ATTENDANCE_URL = BASE_URL_BETA + "apk_attendance.asmx";
    public static final String MEMBERS_GETSTUDENTLISTFORTEACHERLISTINGONLY = "Members_GetStudentListForTeacherListingOnly";
    public static final String ATTENDANCELISTFORTEACHER_METHODNAME = "AttendanceListForTeacher";
    public static final String ATTENDANCELISTFORSTUDENT_METHODNAME = "AttendanceListForStudent";
    public static final String ATTENDANCEREPORT_METHODNAME = "AttendanceReport";

    public static final String SAVEATTENDANCE_METHODNAME = "SaveAttendance";
    public static final String YEARSTARTDATE = "YearStartDate";
    public static final String YEARENDDATE = "YearEndDate";
    public static final String STUDENTATTREGMASTERID = "StudentAttRegMasterID";
    public static final String DATEOFATTENDENCE = "DateOfAttendence";
    public static final String ISWORKINGDAY = "IsWorkingDay";
    public static final String ISONLYWORKINGDAY = "IsOnlyWorkingDay";

    public static final String STANDARDID = "StandardID";
    public static final String ATTENDENCEBY = "AttendenceBy";
    public static final String STUDENTATTENDENCEID = "StudentAttendenceID";

    public static final String DAYOFATTENDENCE_TERM = "DayOfAttendence_Term";
    public static final String ATTENCENCETYPE_TERM = "AttendenceType_Term";
    public static final String TOTALSTUDENTS = "TotalStudents";
    public static final String PRESENTSTUDENTS = "PresentStudents";
    public static final String ABSENTSTUDENTS = "AbsentStudents";
    public static final String ONLEAVESTUDENTS = "OnLeaveStudents";
    public static final String STUDENTDATA = "StudentData";
    public static final String OLDSTUDATTEREGIMASTERID = "OldStudAtteRegiMasterID";

    public static final String MemberID = "MemberID";
    public static final String INSTITUTENAME = "InstituteName";
    public static final String STDDVINAME = "STDDVIName";
    public static final String SFULLNAME = "SFullName";
    ;
    public static final String REGISTRATIONNO = "RegistrationNo";
    public static final String RollNo = "RollNo";
    public static final String SCONTACTNO = "SContactNo";
    public static final String MFULLNAME = "MFullName";
    public static final String MCONTACTNO = "MContactNo";
    public static final String FFULLNAME = "FFullName";
    public static final String FCONTACTNO = "FContactNo";
    public static final String Present = "Present";
    public static final String Absent = "Absent";

    public static final String ProfilePicture = "ProfilePicture";

    public static String PresentStudent = "0";
    public static String AbsentStudent = "0";
    public static String LeaveStudent = "0";
    public static String SeakLeaveStudent = "0";
    public static int PresentStudentCount = 0;

    /**
     * STANDARD DIVISION SUBJECT
     */

    public static final String STANDERDDIVISIONSUBJECT_URL = BASE_URL_BETA + "apk_gradedivisionsubject.asmx";
    public static final String STANDERDDIVISIONSUBJECT_METHODNAME = "GetGradeDivisionSubjectbyTeacher";
    public static final String GRADE_DIVISION_FOR_CLASSTEACHER_METHODNAME = "GradeDivision_ForClassTeacher";
    public static final String PTCOMMUNICATION_URL = BASE_URL + "apk_ptcommunication.asmx";
    public static final String PTCOMMUNICATIONGETSTUDENTLIST_METHODNAME = "PTCommunicationGetStudentList";
    public static final String PTCOMMUNICATIONGETTEACHERLIST_METHODNAME = "PTCommunicationGetTeacherList";
    public static final String GETPTCOMMUNICATIONLIST_METHODNAME = "GetPTCommunicationList";
    public static final String PTCOMMUNICATIONCHATHISTORY_METHODNAME = "PTCommunicationChatHistory";
    public static final String PTCOMMUNICATIONCHATHISTORYSETVIEWFLAG = "PTCommunicationChatHistorySetViewFlag";
    public static final String CREATENEWPTCOMMNUNICATION_METHODNAME = "CreateNewPTCommnunication";
    public static final String WRITECOMMENTSINPTCOMMNUNICATION_METHODNAME = "WriteCommentsInPTCommnunication";

    public static final String TEACHERMEMBERID_PT = "TeacherMemberID";
    public static final String STUDENTMEMBERID = "StudentMemberID";
    public static final String PTSUJBECTID = "SujbectID";
    public static final String MEMBERTYPE = "MemberType";
    public static final String PTTITLE = "Title";

    public static final String GradeID = "GradeID";
    public static final String DivisionID = "DivisionID";
    public static final String SUBJECTID1 = "SubjectID";

    public static final String PTMEMBERID = "MemberID";
    public static final String PTFULLNAME = "FullName";
    public static final String PTREGISTRATIONNO = "RegistrationNo";
    public static final String PTPROFILEPICTURE = "ProfilePicture";
    public static final String PTSUBJECTNAME = "SubjectName";
    public static final String PTSUBJECTID = "SubjectID";
    public static final String PTROLLNO = "RollNo";

    public static final String PTCOMMUNICATIONID = "CommunicationID";
    public static final String PTCOMMUNICATIONDETAILIDBYCOMMA = "PTCommunicationDetailIDByComma";
    public static final String PTCOMMUNICATIONDETAIL = "CommunicationDetail";
    public static final String PTUSERNAME = "UserName";
    public static final String PTUNREADCOUNT = "UnReadCount";
    public static final String PTSEQNO = "SeqNo";
    public static final String PTCOMMUNICATIONIDPT = "PTCommunicationID";
    public static final String PTMEMBERTYPE = "MemberType";
    public static final String PTCOMMUNICATIONDETAILSID = "PTCommunicationDetailsID";
    public static final String PTMESSAGE = "Message";
    public static final String PTPOSTBYTYPE = "PostByType";
    public static final String PTTEACHERID = "TeacherID";

    public static final String PTCREATEDON = "CreatedOn";
    public static final String PTDETAILS = "Details";
    public static final String POSTUSERTYPE = "PostUserType";
    public static final String TEACHERACCEPTANCE = "TeacherAcceptance";
    public static final String PARENTACCEPTANCE = "ParentAcceptance";
    public static final String ISTEACHERREAD = "IsTeacherRead";
    public static final String ISPARENTREAD = "IsParentRead";

    /**
     * STANDARD
     */
    public static final String STANDERD_URL = BASE_URL + "apk_gradedivisionsubject.asmx";
    public static final String STANDERD_METHODNAME = "GetGradeDivisionSubject";
    public static final String ROLE = "Role";

    /**
     * STANDARD PARSING
     */
    public static final String GRADE = "Grade";
    public static final String STANDARD_GRADEID = "GradeID";
    public static final String STANDARD_GRADENAME = "GradeName";
    public static final String STANDARD_GRADEDIVISION_NAME = "GradeDivision";

    /**
     * DIVISION PARSING
     */

    public static final String DIVISION = "Division";
    public static final String STANDARD_DIVISIONID = "DivisionID";
    public static final String STANDARD_DIVISIONNAME = "DivisionName";

    /**
     * SUBJECT PARSING
     */
    public static final String SUBJECT = "Subject";
    public static final String STANDARD_SUBJECTID = "SubjectID";
    public static final String STANDARD_SUBJECTNAME = "SubjectName";

    // grade division wall strings
    public static final String GRADEWALLID = "GradeWallID";
    public static final String GRADEDIVISIONWALLID = "GradeDivisionWallID";
    public static final String GRADEDIVSUBALLID = "GradeDivisionSubjectWallID";
    /*END*/
    /**
     * Login
     */

    public static final String PROFILE_URL = BASE_URL_BETA + "apk_profile.asmx";
    public static final String GETSTUDENTPROFILEDATA_METHODNAME = "GetStudentProfileData";
    public static final String UPDATESTUDENTPROFILEDATA_METHODNAME = "UpdateStudentProfileData";

    public static final String PROFILE_FIRSTNAME = "FirstName";
    public static final String PROFILE_LASTNAME = "LastName";
    public static final String PROFILE_MIDDLENAME = "MiddleName";
    public static final String PROFILE_INITIAL = "Initial";
    public static final String PROFILE_FULLNAME = "FullName";
    public static final String PROFILE_DOB = "DOB";
    public static final String PROFILE_DATEOFANNIVERSARY = "DateOfAnniversary";
    public static final String PROFILE_DISPLAYNAME = "DisplayName";
    public static final String PROFILE_EMAILID = "EmailID";
    public static final String PROFILE_ADDRESS1 = "Address1";
    public static final String PROFILE_ADDRESS2 = "Address2";
    public static final String PROFILE_CITY = "City";
    public static final String PROFILE_STATE = "State";
    public static final String PROFILE_COUNTRY = "Country";
    public static final String PROFILE_ZIPCODE = "ZipCode";

    public static final String LOGIN_USERNAME = "UserName";
    public static final String LOGIN_PASSWORD = "Password";
    public static final String LOGIN_GCMID = "GCMID";
    public static final String LOGIN_DIVREGISTID = "DivRegistID";
    public static final String LOGIN_GETPROJECTTYPE = "GetProjectType";
    public static final String LOGINWITHGCM_METHODNAME = "LoginWithGCM";

    public static final String LOGIN_URL = BASE_URL_BETA + "apk_login.asmx";

    /**
     * Login parsing
     */
    public static final String LOGIN_ARRAY = "UserID";
    public static final String LOGIN_USERID = "UserID";

    public static final String LOGIN_STUDENTCODE = "StudentCode";
    public static final String LOGIN_MOBILENUMBER = "MobileNumber";
    public static final String LOGIN_DISPLAYNAME = "DisplayName";
    public static final String LOGIN_MEMBERID = "MemberID";
    public static final String LOGIN_CLIENTID = "ClientID";
    public static final String LOGIN_ERP_MEMBER_ID = "ERPMemberID";
    public static final String LOGIN_INSTITUTEID = "InstituteID";
    public static final String LOGIN_ROLLID = "RoleID";
    public static final String LOGIN_ROLLNAME = "RoleName";
    public static final String LOGIN_WALLID = "WallID";
    public static final String LOGIN_GRADEID = "GradeID";
    public static final String LOGIN_DIVISIONID = "DivisionID";
    public static final String LOGIN_INSTITUTECODE = "InstituteCode";

    public static final String LOGIN_PROFILEPIC = "ProfilePicture";
    public static final String LOGIN_WALLIMAGE = "WallImage";
    public static final String LOGIN_PRIMARYNAME = "PrimaryName";
    public static final String LOGIN_ROLENAME = "RoleName";
    public static final String LOGIN_MEMBERTYPE = "MemberType";
    public static final String LOGIN_USERTYPETERM = "UserType_Term ";
    public static final String LOGIN_GRADENAME = "GradeName";
    public static final String LOGIN_STANDARDNAME = "StandardName";
    public static final String LOGIN_DIVISIONNAME = "DivisionName";
    public static final String LOGIN_POSTBYTYPE = "PostByType";
    public static final String LOGIN_FULLNAME = "FullName";
    public static final String LOGIN_INSTITUTIONWALLID = "InstitutionWallID";
    public static final String LOGIN_INSTITUTELOGO = "InstituteLogo";
    public static final String LOGIN_INSTITUTENAME = "InstituteName";
    public static final String LOGIN_INSTITUTIONWALLIMAGE = "InstitutionWallImage";
    public static final String LOGIN_PRAYER = "Prayer";
    public static final String LOGIN_INFORMATION = "Information";
    public static final String STUDENTHOSTELREGID = "StudentHostelRegID";
    public static final String STUDENTBUSREGMASTERID = "StudentBusRegMasterID";
    public static final String BusRouteID = "BusRouteID";
    public static final String LOGIN_SCHOOLTIMING = "SchoolTiming";
    public static final String ISALLOWUSERTOPOSTSTATUS = "IsAllowUserToPostStatus";
    public static final String ISALLOWUSERTOPOSTCOMMENT = "IsAllowUserToPostComment";
    public static final String ISALLOWUSERTOPOSTPHOTO = "IsAllowUserToPostPhoto";
    public static final String ISALLOWUSERTOPOSTVIDEO = "IsAllowUserToPostVideo";
    public static final String ISALLOWUSERTOPOSTFILES = "IsAllowUserToPostFiles";
    public static final String ISALLOWUSERTOSHAREPOST = "IsAllowUserToSharePost";
    public static final String ISALLOWUSERTOPOSTALBUM = "IsAllowUserToPostAlbum";
    public static final String ISALLOWUSERTOLIKEDISLIKES = "IsAllowUserToLikeDislikes";
    public static final String DEVICEIDENTITY = "DeviceIdentity";
    public static final String BATCHSTART = "BatchStart";
    public static final String BATCHEND = "BatchEnd";
    public static final String BATCHID = "BatchID";
    public static final String BEACHID = "BeachID";
    public static final String EXAMTYPE = "ExamType";
    public static final String WEEKLYEXAM = "WEEKLY";
    public static final String TERMEXAM = "TERM";
    public static final String SEMESTEREXAM = "SEMESTER";
    public static final String CBSEEXAM = "CBSE";
    public static String FROMEXAMTYPE = "";
    public static int CURRENTEXAM = 0;

    /**
     * CHANGE GCMID
     */
//    public static final String CHANGEGCMID_METHODNAME = "ChangeGCMID";
    public static final String CHANGEGCMID_METHODNAME = "ChangeGCMID_In_All_DB";
    public static final String SAVELOGINLOG_METHODNAME = "SaveLoginLog";
    public static final String GETUSERROLERIGHTLIST_METHODNAME = "GetUserRoleRightList";
    public static final String LOGOUT_METHODNAME = "Logout";

    /**
     * CALENDER EVENT
     */

    public static final String CALENDERDATA_URL = BASE_URL + "apk_calendar.asmx";
    public static final String CALENDERDATA_METHODNAME = "GetCalendarData";
    public static final String CALENDERDATA_YEAR = "year";
    public static final String CALENDERDATA_CLIENTID = "ClientID";
    public static final String CALENDERDATA_INSTITUTEID = "InstituteID";
    public static final String CALENDERDATA_MEMBERID = "MemberID";
    public static final String CALENDERDATA_BEACHID = "BeachID";
    public static final String CALENDERDATA_ROLENAME = "Rolename";

    // string for multiple notiication
    public static String NOTIFICATION_USERID = "";

    /**
     * CALENDER EVENT PARSING DATA
     */

    public static final String CALENDERDATA_ACTIVITYID = "activityiD";
    public static final String CALENDERDATA_TITLE = "title";
    public static final String CALENDERDATA_ACTIVITYDETAIL = "activitydetails";
    public static final String CALENDERDATA_START = "start";
    public static final String CALENDERDATA_END = "end";
    public static final String CALENDERDATA_TYPE = "type";
    public static final String CALENDERDATA_IMAGE = "type";
    public static final String CALENDERDATA_CLASSNAME = "classname";
    public static final String CALENDERDATA_COLOR = "color";
    public static final String CALENDERDATA_CREATEDBY = "CreateBy";
    public static final String CALENDERDATA_REFERENCELINK = "ReferenceLink";

    /**
     * CALENDER EVENT
     */

    public static final String CALENDEREVENT_URL = BASE_URL + "apk_event.asmx";
    public static final String CALENDEREVENT_METHODNAME = "GetEvent";
    public static final String CALENDEREVENT_USERID = "UserID";
    public static final String CALENDEREVENT_CLIENTID = "ClientID";
    public static final String CALENDEREVENT_INSTITUTEID = "InstituteID";
    public static final String CALENDEREVENT_BEACHID = "BeachID";

    /**
     * CALENDER PARSING
     */

    public static final String CALENDEREVENT_YEAR = "Year";
    public static final String CALENDEREVENT_USERNAME = "UserName";
    public static final String CALENDEREVENT_EVENTID = "EventID";
    public static final String CALENDEREVENT_TITLE = "Title";
    public static final String CALENDEREVENT_DETAIL = "Details";
    public static final String CALENDEREVENT_INSTITUTEIDPARSING = "instituteID";
    public static final String CALENDEREVENT_REMINDERDATEPARSING = "ReminderDate";
    public static final String CALENDEREVENT_STARTDATE = "StartDate";
    public static final String CALENDEREVENT_ENDDATE = "EndDate";
    public static final String CALENDEREVENT_ISACTIVE = "IsActive";
    public static final String CALENDEREVENT_UPDATEBY = "UpdateBy";
    public static final String CALENDEREVENT_UPDATEON = "UpdateOn";
    public static final String CALENDEREVENT_CREATEON = "CreateOn";
    public static final String CALENDEREVENT_SEQNO = "SeqNo";
    public static final String CALENDEREVENT_GRADEID = "GradeID";
    public static final String CALENDEREVENT_DIVISIONID = "DivisionID";
    public static final String CALENDEREVENT_SUBJECTID = "SubjectID";

    /**
     * Holiday List
     */
    public static final String HOLIDAY_URL = BASE_URL + "apk_holiday.asmx";
    public static final String HOLIDAY_METHODNAME = "GetHoliday";
    public static final String HOLIDAY_DAY = "Hday";
    public static final String HOLIDAY_DATE = "StartDate";
    //    public static final String HOLIDAY_DATE = "HolidayDate";
    public static final String HOLIDAY_END_DATE = "EndDate";
    public static final String HOLIDAY_TITLE = "HolidayTitle";
    public static final String HOLIDAY_DAYCOUNT = "NoOfDay";

    /**
     * Menu Permission
     */

    public static final String MENUNAME = "MenuName";
    public static final String MENUVIEW = "IsView";
    public static final String MENU_DEFAULT_VALUE = "DefaultValue";
    public static final String MENU_ICON_NAME = "MenuIcon";

    /**
     * Health Record
     */

    public static final String HEALTH_RECORD_METHODNAME = "GetHealthRecord";
    public static final String HEALTH_RECORD_URL = BASE_URL + "apk_healthrecord.asmx";
    public static final String HOLIDAY_MEMBERID = "MemberID";
    public static final String HOLIDAY_CLIENTID = "ClientID";
    public static final String HOLIDAY_INSTITUTEID = "InstituteID";
    public static final String HOLIDAY_BEACHID = "BeachID";

    /**
     * ADD Health Recoed
     */

    public static final String HEALTH_RECORD_ADD_METHODNAME = "CreateHealthRecord";
    public static final String HEALTH_RECORD_ID = "EditID";
    public static final String HEALTH_RECORD_BLOODGROUP = "BloodGroup";
    public static final String HEALTH_RECORD_HEIGHT = "Height";
    public static final String HEALTH_RECORD_WEIGHT = "Weight";
    public static final String HEALTH_RECORD_ALLERGIES = "Allergies";
    public static final String HEALTH_RECORD_SPECIFICDISEASES = "SpecificDiseases";
    public static final String HEALTH_RECORD_OPERATIONUNDERGROUND = "OperationUndergone";
    public static final String HEALTH_RECORD_ANYOTHERDISEASE = "AnyOtherDisease";
    public static final String HEALTH_RECORD_MEDICATION = "Medication";
    public static final String HEALTH_RECORD_COLOUTFEAR = "ColoutFear";
    public static final String HEALTH_RECORD_FALILYDOCTORNAME = "FamilyDoctorName";
    public static final String HEALTH_RECORD_CONTACTNO = "ContactNo";
    public static final String HEALTH_RECORD_PATIENTID = "PatientID";
    public static final String HEALTH_RECORD_DATEOFLASTUPDATE = "DateOfLastUpdate";

    /**
     * Parsing Health Record
     */

    public static final String HEALTH_RECORD_MEMBERHEALTHID = "MemberHeathID";
    public static final String HEALTH_RECORD_BLOODGROOUPTERM = "BloodGroupTerm";
    public static final String HEALTH_RECORD_HEIGHT_PARSING = "Height";
    public static final String HEALTH_RECORD_WEIGHT_PARSING = "Weight";
    public static final String HEALTH_RECORD_ALLERGIESIFANY = "AllergiesIfAny";
    public static final String HEALTH_RECORD_SPECIFICDISEASSUFFERED = "SpecificDiseasSuffered";
    public static final String HEALTH_RECORD_OPERATIONUNDERGONE = "OperationUndergone";
    public static final String HEALTH_RECORD_ANYOTHEDISEASESFORWHICHTHECHILDISONREGULAR = "AnyOtheDiseasesForWhichTheChildIsOnRegular";
    public static final String HEALTH_RECORD_MEDICATOIN = "Medicatoin";
    public static final String HEALTH_RECORD_COLOURFEARIFANY = "ColourFearIfAny";
    public static final String HEALTH_RECORD_DATEOFLASTUPDATE_PARSING = "DateOfLastUpdate";
    public static final String HEALTH_RECORD_FAMILYDOCTORNAME = "FamilyDoctorName";
    public static final String HEALTH_RECORD_CONTACTNO_PARSING = "ContactNo";
    public static final String HEALTH_RECORD_PATIENTID_PARSING = "PatientID";
    public static final String HEALTH_RECORD_UPDATEON = "UpdateOn";
    public static final String HEALTH_RECORD_UPDATEBY = "UpdateBy";
    public static final String HEALTH_RECORD_CREATEON = "CreateOn";
    public static final String HEALTH_RECORD_CREATEBY = "CreateBy";

    /**
     * PROJECT
     */
    public static final String PROJECT_METHODNAME = "GetProjectList";
    public static final String PRJECTDATABYID_METHODNAME = "PrjectDataByID";
    public static final String DELETEPROJECTS_METHODNAME = "DeleteProjects";
    public static final String PROJECT_URL = BASE_URL + "apk_projects.asmx";
    public static final String STUDENTLIST_METHODNAME = "studentList";
    public static final String ADDPROJECT_METHODNAME = "saveupdateproject";

    public static final String ADDPROJECT_PROJECTSTARTDATE = "projectstartdate";
    public static final String ADDPROJECT_PROJECTENDDATE = "projectenddate";
    public static final String ADDPROJECT_PROJECTTITLE = "projecttitle";
    public static final String ADDPROJECT_PROJECTDEFINETION = "projectdefinetion";
    public static final String ADDPROJECT_PROJECTSCOPE = "projectscope";
    public static final String ADDPROJECT_GROUPMEMBERS = "groupmembers";

    public static final String PROJECT_INSTITITE_ID = "InstituteID";

    public static final String PROJECT_PROJECTID = "ProjectID";
    public static final String PROJECT_STARTDATE = "StartDate";
    public static final String PROJECT_ENDDATE = "EndDate";
    public static final String PROJECT_GROUPID = "GroupID";
    public static final String PROJECT_TITLE = "ProjectTitle";
    public static final String PROJECT_DEFINATION = "ProjectDefination";
    public static final String PROJECT_PROJECTSCOPE = "ProjectScope";

    public static final String PROJECT_CREATEON = "CreateOn";
    public static final String PROJECT_CREATEBY = "CreateBy";
    public static final String PROJECT_USERNAME = "UserName";
    public static final String PROJECT_UPDATEON = "UpdateOn";
    public static final String PROJECT_PROJECTTITLE = "ProjectTitle";
    public static final String PROJECT_PROFILEPIC = "ProfilePicture";

    /**
     * POLL
     */
    public static final String POLLDELETE_METHODNAME = "PollDelete";
    public static final String POLL_URL = BASE_URL + "apk_poll.asmx";
    public static final String GETPOLLSLISTFORADDPAGE_METHODNAME = "GetPollsListForAddPage";
    public static final String SAVEUPDATEPOLLS_METHODNAME = "SaveUpdatePolls";
    public static final String GETPOLLFOREDIT_METHODNAME = "GetPollForEdit";
    public static final String GETPOLLSLISTFORPARTICIPANTPAGE_METHODNAME = "GetPollsListForParticipantPage";
    public static final String SAVEPOLLOPTIONVOTE_METHODNAME = "SavePollOptionVote";
    public static final String GETPOLLRESULT_METHODNAME = "GetPollResult";
    public static final String SHARERESULT_METHODNAME = "ShareResult";

    public static final String POLLIDOPTIONIDBYCOMMAHASE = "PollIDOptionIDByCommaHase";
    public static final String VMEMBERID = "VMemberID";
    public static final String VUSERID = "VUserID";
    public static final String BACHID = "BachID";
    public static final String ISNOTIFIYTOALL = "IsNotifiyToAll";

    public static final String POLLOPTION = "PollOption";
    public static final String POLLOPTIONID = "PollOptionID";
    public static final String OPTION = "Option";

    public static final String POLLOPTIONBYHASE = "PollOptionByHase";
    public static final String ISPERCENTEGE = "IsPercentege";
    public static final String ISNOTIFYALL = "IsNotifyAll";
    public static final String TOTALVOTES = "TotalVotes";
    public static final String ISMULTICHOICE = "IsMultiChoice";
    public static final String TILTE = "Tilte";

    /**
     * POLL PARSING
     */
    public static final String POLL_POLLID = "PollID";
    public static final String POLL_STARTDATE = "StartDate";
    public static final String POLL_ENDDATE = "EndDate";
    public static final String POLL_TITLE = "Title";
    public static final String POLL_DETAIL = "Details";
    public static final String POLL_STARTIN = "StartIn";
    public static final String POLL_ENDIN = "EndIn";
    public static final String POLL_USERNAME = "UserName";
    public static final String POLL_PARTICIPANT = "Participant";

    /**
     * ALBUM
     */
    public static final String ALBUM_METHODNAME = "GetAlbumList";
    public static final String GETALLPHOTOSBYALBUM_METHODNAME = "GetAllPhotosByAlbum";
    public static final String ALBUM_URL = BASE_URL_BETA + "apk_albums.asmx";

    /**
     * ALBUM PARSING
     */
    public static final String ALBUM_ID = "AlbumID";
    public static final String ALBUM_TITLE = "AlbumTitle";
    public static final String ALBUM_DETAIL = "AlbumDetails";
    public static final String ALBUM_PHOTO = "Photo";
    public static final String ALBUM_TOTAL = "Total";

    /**
     * CIRCULAR
     */
    // get circular list with reference. // chnage api to get reference link
    public static final String CIRCULAR_METHODNAME = "GetCircularList_New";
    //    public static final String CIRCULAR_METHODNAME = "GetCircularList";
    public static final String CIRCULAR_URL = BASE_URL_BETA + "apk_circular.asmx";

    /**
     * CIRCULAR PARSING
     */

    public static final String UPDATE = "Update";
    public static final String CIRCULAR_CIRCULARID = "CircularID";
    public static final String CIRCULAR_DATEOFCIRCULAR = "DateOfCircular";
    public static final String CIRCULAR_GRADEID = "GradeID";
    public static final String CIRCULAR_CIRCULARTITLE = "CircularTitle";
    public static final String CIRCULAR_CIRCULARDETAIL = "CircularDetails";
    public static final String CIRCULAR_CIRCULARTUPETERM = "CircularTypeTerm";
    public static final String CIRCULAR_TEACHERNAME = "TeacherName";
    public static final String CIRCULAR_TEACHERPROFILEPIC = "ProfilePic";
    public static final String CIRCULAR_SUBJECTID = "SubjectID";
    public static final String CIRCULAR_REFERENCELINK = "ReferenceLink";
    public static final String CIRCULAR_DIVISIONID = "DivisionID";
    public static final String CIRCULAR_DATE = "strDateOfCircular";

    /**
     * WALL LIST
     */
    public static final String WALL_METHODNAME = "GetGeneralWallData";
    //    public static final String WALL_METHODNAME = "GetGeneralWallData_AllData";
    public static final String GETWALLMEMBER_METHODNAME = "GetWallMember";
    public static final String DELETEPOST_METHODNAME = "DeletePost";
    public static final String EDITPOSTDETAILS_METHODNAME = "EditPostDetails";
    public static final String DELETEPHOTOVIDEO_METHODNAME = "AlbumPhotosVideoFileDeleteByAssoIDAndType";
    public static final String WALL_URL = BASE_URL_BETA + "apk_generalwall.asmx";
    /**
     * DELTE POST
     */
    public static final String POSTID = "PostID";
    public static final String POSTCOMMENTNOTE = "PostCommentNote";
    public static final String MEMBER_MemberID = "MemberID";
    public static final String MEMBER_MEMBERTYPE = "MemberType";
    public static final String MEMBER_FULLNAME = "FullName";
    public static final String MEMBER_PROFILEPICTURE = "ProfilePicture";

    /**
     * SHARE POST
     */

    public static final String SHRE_POST_METHODNAME = "SharePost";
    public static final String SHRE_POST_URL = BASE_URL + "apk_Post.asmx";
    public static final String SHRE_POST_SHAREPOSTID = "SharePostID";
    public static final String SHRE_POST_SHARETYPE = "ShareType";
    public static final String SHRE_POST_TAGID = "TagID";

    /**
     * LIKE API
     */
    public static final String POST_URL = BASE_URL_BETA + "apk_Post.asmx";
    public static final String GETPOSTED_FILEIMGAEVIDEOS_METHODNAME = "GetPosted_FileImgaeVideos";
    public static final String GETPOSTED_FILEIMGAEVIDEOS_METHODNAMEFORALLDATA = "GetPosted_FileImgaeVideosForAllData";
    public static final String LIKE_METHODNAME = "LikePost";
    public static final String DISLIKE_METHODNAME = "DisLikePost";
    public static final String LIKE_PDATAID = "pdataID";
    public static final String LIKE_PTYPE = "ptype";
    public static final String LIKE_ISLIKEPARAM = "IsLike";
    public static final String LIKE_ISDISLIKE = "IsDislike";
    public static final String SENDTOMEMBERID = "SendToMemberID";

    /**
     * GetPostedPics
     */

    public static final String GETPOSTEDPICS_METHODNAME = "GetPostedPics";
    public static final String ASSOTYPE = "AssoType";
    public static final String ASSOID = "AssoID";
    public static final String PHOTO = "Photo";

    public static final String WALL_ROWNO_POST = "RowNo";
    public static final String WALL_WALLID = "WallID";
    public static final String WALL_POSTCOMMENTTYPESTERM = "PostCommentTypesTerm";
    public static final String WALL_POSTCOMMENTNOTE = "PostCommentNote";
    public static final String WALL_TOTALCOMMENTS = "TotalComments";
    public static final String WALL_DATEOFPOST = "DateOfPost";
    public static final String WALL_ASSOCIATIONID = "AssociationID";
    public static final String WALL_PHOTO = "Photo";
    public static final String WALL_PHOTOCOUNT = "PhotoCount";
    public static final String WALL_PHOTOURLS = "PhotoUrls";
    public static final String WALL_POSTCOUNT = "POSTCount";
    public static final String WALL_POSTURLS = "POSTUrls";
    public static final String WALL_POSTDATE = "PostDate";
    public static final String WALL_FILETYPE = "FileType";
    public static String WALL_TOTALCOMMENT = "";

    /**
     * COMMENT LIST
     */

    public static final String CMT_METHODNAME = "GetWallPostComments";
    public static final String CMT_URL = BASE_URL + "apk_getwallpostcomment.asmx";

    //  json arrays  name for get deleted post data
    public static final String POST = "Post";
    public static final String HOMEWORK = "HomeWork";
    public static final String DELETEDCLASSWORK = "ClassWork";
    public static final String CIRCULARS = "Circulars";
    public static final String EVENTS = "Events";
    public static final String HOLIDAYS = "Holidays";
    public static final String NOTES = "Notes";
    public static final String TODOS = "Todos";
    public static final String PTCOMMUNICATION = "PTCommunication";
    public static final String ASSIGNMENTID = "AssignmentID";
    public static final String CLASSWORKID = "ClassWorkID";
    public static final String CIRCULARID = "CircularID";
    public static final String NOTICEID = "NotesID";
    public static final String COMMUNICATIONID = "CommunicationID";
    public static final String PROJECTID = "ProjectID";
    public static final String DELETEGROUPID = "GropuID";


    /**
     * COMMENT PARSING
     */

    public static final String WALL_POSTDATA = "PostData";
    public static final String WALL_ROLEDATA = "RoleData";
    public static final String WALLROLEDATA = "WallRoleData";

    public static final String WALL_CMT_POSTID = "PostID";
    public static final String WALL_CMT_WALLID = "WallID";
    public static final String WALL_CMT_MEMBERID = "MemberID";
    public static final String WALL_CMT_ROWNO = "rowno";
    public static final String WALL_ROWNOSMALL = "rowno";
    public static final String WALL_POSTCOMMENTID = "PostCommentID";
    public static final String WALL_ASSOCIATIONTYPE = "AssociationType";
    public static final String WALL_COMMENTID = "CommentID";
    public static final String WALL_COMMENT = "Comment";
    public static final String WALL_COMMENTON = "CommentOn";
    public static final String WALL_ISDISLIKE = "IsDislike";
    public static final String WALL_ISDISLIKE_COMMENT = "IsDisLike";
    public static final String WALL_COMMENTISDISLIKE = "IsDisLike";
    public static final String WALL_ISDISLIKESIN = "IsDislike";
    public static final String WALL_ISDISLIKESCAPITAL = "IsDisLike";
    public static final String WALL_ISLIKE = "IsLike";
    public static final String WALL_SENDTOMEMBERID = "MemberID";
    public static final String WALL_SENDTOMEMBERIDSINGLE = "SendToMemberID";
    public static final String WALL_TEMPDATE = "TempDate";

    public static final String WALL_FULLNAME = "FullName";
    public static final String WALL_PROFILEPICTURE = "ProfilePicture";
    public static final String WALL_TOTALLIKES = "TotalLikes";
    public static final String WALL_TOTALREPLIES = "TotalReplies";
    public static final String WALL_TOTALDISLIKE = "TotalDislike";
    public static final String WALL_TYPE_TERM = "WallTypeTerm";
    public static final String WALL_ROW_ID = "RowNo";
    public static final String WALL_POSTEDON = "PostedOn";
    public static final String WALL_FILEMIMETYPE = "FileMimeType";

    public static final String IS_USER_LIKE_WALL = "IsAllowPeopleToLikeAndDislikeCommentWall";
    public static final String IS_USER_DISLIKE_WALL = "IsAllowPeopleToLikeAndDislikeCommentWall";
    public static final String IS_USER_SHARE_WALL = "IsAllowPeopleToShareCommentWall";
    public static final String IS_USER_COMMENT_WALL = "IsAllowPeoplePostCommentWall";

    public static final String IS_USER_LIKE = "IsAllowPeopleToLikeOrDislikeOnYourPost";
    public static final String IS_USER_DISLIKE = "IsAllowPeopleToLikeOrDislikeOnYourPost";
    public static final String IS_USER_COMMENT = "IsAllowPeopleToPostMessageOnYourWall";
    public static final String IS_USER_SHARE = "IsAllowPeopleToShareYourPost";

    public static final String ISALLOWPEOPLETOLIKEORDISLIKEONYOURPOST = "IsAllowPeopleToLikeOrDislikeOnYourPost";
    public static final String ISALLOWPEOPLETOSHAREYOURPOST = "IsAllowPeopleToShareYourPost";
    public static final String ISALLOWPEOPLETOPOSTMESSAGETOWALL = "IsAllowPeopleToPostMessageOnYourWall";
    public static final String ISALLOWPEOPLETOLIKEORDISLIKECOMMENTWALL = "IsAllowPeopleToLikeAndDislikeCommentWall";
    public static final String ISALLOWPEOPLETOSHARECOMMENTWALL = "IsAllowPeopleToShareCommentWall";
    public static final String ISALLOWPEOPLETOPOSTCOMMENTWALL = "IsAllowPeoplePostCommentWall";

    public static final String ISALLOWPEOPLETOLIKETHISWALL = "IsAllowPeopleToLikeThisWall";
    public static final String ISALLOWPEOPLEPOSTCOMMENT = "IsAllowPeoplePostComment";
    public static final String ISALLOWPEOPLETOSHARECOMMENT = "IsAllowPeopleToShareComment";
    public static final String ISALLOWPEOPLETOLIKEANDDISLIKECOMMENT = "IsAllowPeopleToLikeAndDislikeComment";
    public static final String ISALLOWPEOPLETOPOSTSTATUS = "IsAllowPeopleToPostStatus";
    public static final String ISALLOWPEOPLETOCREATEPOLL = "IsAllowPeopleToCreatePoll";
    public static final String ISALLOWPEOPLETOPARTICIPATEINPOLL = "IsAllowPeopleToParticipateInPoll";
    public static final String ISALLOWPEOPLETOINVITEOTHERPEOPLE = "IsAllowPeopleToInviteOtherPeople";
    public static final String ISALLOWPEOPLETOTAGONPOST = "IsAllowPeopleToTagOnPost";
    public static final String ISALLOWPEOPLETOUPLOADALBUM = "IsAllowPeopleToUploadAlbum";
    public static final String ISALLOWPEOPLETOPUTGEOLOCATIONONPOST = "IsAllowPeopleToPutGeoLocationOnPost";
    public static final String ISALLOWPEOPLETOPOSTDOCUMENT = "IsAllowPeopleToPostDocument";
    public static final String ISALLOWPEOPLETOPOSTVIDEOS = "IsAllowPeopleToPostVideos";
    public static final String ISALLOWPEOPLETOPOSTCOMMENTONPOSTWALL = "IsAllowPeopleToPostCommentOnPostWall";
    public static final String WALLNAME = "WallName";
    public static final String WALLIMAGE = "WallImage";

    public static final String ISAUTOAPPROVEPOST = "IsAutoApprovePost";
    public static final String ISAUTOAPPROVEPOSTSTATUS = "IsAutoApprovePostStatus";
    public static final String ISAUTOAPPROVEALBUME = "IsAutoApproveAlbume";
    public static final String ISAUTOAPPROVEVIDEOS = "IsAutoApproveVideos";
    public static final String ISAUTOAPPROVEDOCUMENT = "IsAutoApproveDocument";
    public static final String ISAUTOAPPROVEPOLL = "IsAutoApprovePoll";
    public static final String ISADMIN = "IsAdmin";

    public static final String ISALLOWPOSTSTATUS = "IsAllowPostStatus";
    public static final String ISALLOWPOSTPHOTO = "IsAllowPostPhoto";
    public static final String ISALLOWPOSTALBUM = "IsAllowPostAlbum";
    public static final String ISALLOWPOSTVIDEO = "IsAllowPostVideo";
    public static final String ISALLOWPOSTFILE = "IsAllowPostFile";
    public static final String ISALLOWLIKEDISLIKE = "IsAllowLikeDislike";
    public static final String ISALLOWPOSTCOMMENT = "IsAllowPostComment";
    public static final String ISALLOWSHAREPOST = "IsAllowSharePost";
    public static final String ISALLOWPEOPLETOPOSTCOMMENTONPOST = "IsAllowPeopleToPostCommentOnPost";

    public static final String ABSENT = "Absent";
    public static final String LEAVE = "Leave";
    public static final String PRESENT = "Present";
    public static final String SICKLEAVE = "Sick Leave";

    public static final String ROWNO = "RowNo";
    public static final String NOTIFICATIONID = "NotificationID";
    public static final String ASSOCIATIONIDPARS = "AssociationID";
    public static final String ASSOCIATIONTYPEPARS = "AssociationType";
    public static final String DATEOFNOTIFICATION = "DateofNotification";
    public static final String ISVIEW = "Isview";
    public static final String FULLNAME = "FullName";
    public static final String ROLLNO = "RollNo";
    public static final String DATEOFNOTIFICATIONASTEXT = "DateofNotificationAsText";
    public static final String NOTIFICATIONTEXT = "NotificationText";
    public static final String NOTIFICATIONTYPE = "NotificationType";
    public static final String SENDTOMEMBERIDPARS = "SendToMemberID";
    public static final String NOTIFICATION_DETAILS = "Details";


    /**
     * RIGHTS
     */

    public static final String ISVIEWSETTING = "IsView";
    public static final String ISEDIT = "IsEdit";
    public static final String ISCREATE = "IsCreate";
    public static final String ISDELETE = "IsDelete";
    public static final String RIGHTID = "RightID";
    public static final String RIGHTNAME = "RightName";
    public static final String BLOG = "Blog";
    public static final String CIRCULAR = "Circular";
    public static final String CLASSWORK = "Class Work";
    public static final String ATTENDANCE = "Attendance";
    public static final String HOMEWORKSETTING = "Home Work";

    public static final String GROUP = "Group";
    public static final String PROJECT = "Project";
    public static final String HappyGram = "Happy Gram";

    public static final String HOLIDAY = "Holiday";
    public static final String NOTE = "Note";
    public static final String PAGE = "Page";
    public static final String POLL = "Poll";

    public static final String PTCOMMUNICATOIN = "PT Communicatoin";
    public static final String REMINDERS = "Reminders";
    public static final String EVENT = "Event";
    public static final String EVENTID = "EventID";

    /**
     * ADD COMMENT
     */
    public static final String ADDCOMMENT_ASSOCIATIONID = "AssociationID";
    public static final String ADDCOMMENT_REFCOMMENTID = "RefCommentID";
    public static final String ADDCOMMENT_ASSOCIATIONTYPETERM = "AssociationTypeTerm";
    public static final String ADDCOMMENT_COMMENT = "Comment";
    public static final String ADDCOMMENT_POSTBYTYPE = "PostByType";
    public static final String ADDCOMMENT_METHODNAME = "AddComments";

    /**
     * CLASSWORK
     */

    public static final String CLASSWORK_METHODNAME = "GetClassWorkList";
    public static final String CLASSWORK_URL = BASE_URL_BETA + "apk_classwork.asmx";

    /**
     * CLASSWORK PARSING
     */
    public static final String CLASSWORK_CLASSWORKID = "ClassWorkID";
    public static final String CLASSWORK_DATEOFCLASSWORK = "DateOfClassWork";
    public static final String CLASSWORK_POSTID = "PostID";
    public static final String CLASSWORK_GRADEID = "GradeID";
    public static final String CLASSWORK_DIVISIONID = "DivisionID";
    public static final String CLASSWORK_SUBJECTID = "SubjectID";
    public static final String CLASSWORK_TEACHERID = "TeacherID";
    public static final String CLASSWORK_TEACHERNAME = "TecherName";
    public static final String CLASSWORK_TEACHERPROFILEPIC = "ProfilePicture";
    public static final String CLASSWORK_TITLE = "ClassWorkTitle";
    public static final String CLASSWORK_DETAIL = "ClassWorkDetails";
    public static final String CLASSWORK_REFERENCELINK = "ReferenceLink";
    public static final String CLASSWORK_EXPECTINGDATEOFCOMPLETION = "ExpectingDateOfCompletion";
    public static final String CLASSWORK_STARTTIME = "StartTime";
    public static final String CLASSWORK_ENDTIME = "EndTime";
    public static final String CLASSWORK_USERNAME = "UserName";
    public static final String CLASSWORK_SUBJECTNAME = "SubjectName";
    public static final String CLASSWORK_STR_STARTTIME = "strStartTime";
    public static final String CLASSWORK_STR_ENDTIME = "strEndTime";

    /**
     * Notifications
     */

    public static final String NOTIFICATIONS_URL = BASE_URL_BETA + "apk_notifications.asmx";
    public static final String NOTIFICATIONGETLIST_METHODNAME = "NotificationGetList";
    public static final String SETVIEWFLAGEONNOTIFICATION_METHODNAME = "SetViewFlageOnNotification";
    public static final String MULTIDATAIDSEPRATBYCOMMA = "multidataidsepratbycomma";
    public static final String NOTIFICATIONS_ROWCOUNT = "RowCount";
    public static final String NOTIFICATIONS_ISVIEW = "IsView";

    /**
     * Notification
     */

    public static final String NOTIFICATIONCOUNT_METHODNAME = "MemberAllTypeOfCounts";
    public static final String GETNOTIFICATIONDETAILS_METHODNAME = "GetNotificationDetails";
    public static final String NOTIFICATION_URL = BASE_URL_BETA + "apk_Notification.asmx";
    public static final String NOTIFICATION_ASSOCIATIONID = "AssociationID";
    public static final String NOTIFICATION_ASSOCIATIONTYPE = "AssociationType";
    public static final String SENDNOTIFICATION_METHODNAME = "SendPushNotification";
    public static final String NOTIFICATION_TABLE = "Table";
    public static final String NOTIFICATION_NOTIFICATIONCOUNT = "NotificationCount";
    public static final String NOTIFICATION_TABLE1 = "Table1";
    public static final String NOTIFICATION_TABLE2 = "Table2";
    public static final String NOTIFICATION_FRIENDCOUNT = "FriendCount";
    public static final String NOTIFICATION_LEAVEAPPLICATION = "LeaveApplication";

    /**
     * HOMEWORK
     */
    public static final String HOMEWORK_METHODNAME = "GetHomework";
    public static final String HOMEWORK_URL = BASE_URL_BETA + "apk_homework.asmx";

    /**
     * HOMEWORK PARSING
     */

    public static final String HOMEWORK_ASSINMENTID = "AssignmentID";
    public static final String HOMEWORK_CLIENTID = "ClientID";
    public static final String HOMEWORK_INSTITUTEID = "InstituteID";
    public static final String HOMEWORK_MEMBERID = "MemberID";
    public static final String HOMEWORK_GRADEID = "GradeID";
    public static final String HOMEWORK_DIVISIONID = "DivisionID";
    public static final String HOMEWORK_SUBJECTNAME = "SubjectName";
    public static final String HOMEWORK_FACULTYID = "FacultyID";
    public static final String HOMEWORK_DATEOFHOMEWORLK = "DateOfHomeWork";
    public static final String HOMEWORK_REFERENCELINK = "ReferenceLink";
    public static final String HOMEWORK_TITLE = "Title";
    public static final String HOMEWORK_TEACHERNAME = "TeacherName";
    public static final String HOMEWORK_TEACHERPROFILEPIC = "TeacherProfilePicture";
    public static final String HOMEWORK_SUBJECTID = "subjectId";
    public static final String HOMEWORK_ISREAD = "IsRead";
    public static final String HOMEWORK_ISAPPROVED = "IsApproved";
    public static final String HOMEWORK_ISWORKFINISHED = "isWorkFinish";
    public static final String ISREAD = "IsRead";
    public static final String ISAPPROVEPARAM = "IsApprove";
    public static final String HOMEWORK_HOMEWORKDETAILS = "HomeWorksDetails";
    public static final String HOMEWORK_ISMESSAGERECEIVED = "isMessageReceived";
    public static final String HOMEWORK_ISWORKFINISH = "isWorkFinish";
    public static final String HOMEWORK_DATEOFFINISH = "DateOfFinish";
    public static final String STRDATEOFFHOMEWORK = "strDateOfHomeWork";

    //	IsChecked
    public static final String HOMEWORK_ISCHECKED = "IsChecked";

    /**
     * TIMETABLE List
     */
    public static final String TIMETABLE_METHODNAME = "GetTimeTableList";
    public static final String TIMETABLETEACHER_METHODNAME = "GetTimeTableListForTeacher";
    public static final String TIMETABLE_URL = BASE_URL_BETA + "apk_timetable.asmx";

    /**
     * GROUP
     */

    public static final String GROUP_METHODNAME = "Group_List";
    // chnage methodname for base64
    public static final String SAVEUPDATE_GROUP_METHODNAME = "SaveUpdate_GroupByBase64STR";
    //    public static final String SAVEUPDATE_GROUP_METHODNAME = "SaveUpdate_Group";
    public static final String REMOVE_GROUP_METHODNAME = "Remove_Group";
    public static final String REMOVE_GROUPMEMBERS_METHODNAME = "Remove_GroupMembers";
    public static final String GROUPMEMBERID = "GroupMemberID";

    public static final String STUGROUP_METHODNAME = "StuGroup";
    public static final String WALLMEMBERS_METHODNAME = "WallMembers";
    public static final String GETTEACHERLISTNAMEANDMEMBERID_METHODNAME = "GetTeacherListNameAndMemberID";
    public static final String GETCLASSTEACHERLISTNAMEANDMEMBERID_METHODNAME = "GetClassTeacherListNameAndMemberID";
    public static final String GETSTUDENTSLISTNAMEANDMEMBERID_METHODNAME = "GetStudentsListNameAndMemberID";
    public static final String GROUP_URL = BASE_URL + "apk_group.asmx";

    // api method for check maintenance
    public static final String CHECK_CLIENT_IS_IN_MAINTENANCE = "CheckClientIsInMaintenance";
    public static final String INMAINTENANCE = "InMaintenance";

    /**
     * GROUP Member
     */

    public static final String FULLNAMEPARAM = "FullName";

    public static final String GROUP_TITLE_PARAM = "GroupTitle";
    public static final String GROUP_SUBJECT_PARAM = "GroupSubject";
    public static final String GROUP_ABOUTGROUP_PARAM = "AboutGroup";
    public static final String GROUPTYPEID_PARAM = "GroupTypeID";

    public static final String ISAUTOAPPROVEPENDINGMEMBER = "IsAutoApprovePendingMember";
    public static final String ISAUTOAPPROVEPENDINGPOST = "IsAutoApprovePendingPost";
    public static final String ISAUTOAPPROVEPENDINGALBUMS = "IsAutoApprovePendingAlbums";
    public static final String ISAUTOAPPROVEPENDINGATTACHMENT = "IsAutoApprovePendingAttachment";
    public static final String ISAUTOAPPROVEPENDINGPOLLS = "IsAutoApprovePendingPolls";
    public static final String ALLGROUPMEMBERS = "AllGroupMembers";

    /**
     * GROUP List
     */
    public static final String GROUP_ID = "GropuID";
    public static final String GROUP_TITLE = "GroupTitle";
    public static final String GROUP_SUBJECT = "GroupSubject";
    public static final String GROUP_ABOUTGROUP = "AboutGroup";
    public static final String GROUP_CREATEON = "GroupCreatedOn";
    public static final String GROUP_IMAGE = "GroupImage";
    public static final String GROUP_TOTALMEMEBER = "TotalMemeber";
    public static final String GroupTypeID = "GroupTypeID";
    public static final String WallID = "WallID";

    public static final String GROPUMEMBERID = "GropuMemberID";
    public static final String MEMBERFULLNAME = "MemberFullName";
    public static final String MEMBERPROFILECODE = "MemberProfileCode";
    public static final String GROUP_PROFILEPICTURE = "ProfilePicture";
    public static final String MEMBERROLE = "MemberRole";

    public static final String GROUP_USERNAME = "UserName";

    /**
     * MenuPermission
     */

    public static final String MENUPERMISSION_METHODNAME = "InstituteMenuPermission";

    /**
     * Get Deleted wall Post By web or Admin
     */

    public static final String GETDELETEPOSTDATA = "GetDeletePostData";

    /**
     * HAPPYGRAM
     */

    public static final String HAPPYGRAM_URL = BASE_URL_BETA + "apk_happygram.asmx";
    public static final String ADDHAPPYGRAM_METHODNAME = "AddHappyGramData";
    public static final String UPDATE_HAPPYGRAM_METHODNAME = "UpdateSingleStudentHappyGram";
    public static final String GETHAPPYGRAM_METHODNAME = "StudentHappyGramSelectForListing";
    public static final String GETHAPPYGRAMSTUDENTLIST_METHODNAME = "GetStudentListForAddNewHappyGram";
    public static final String ISSTUDNETS = "IsStudnets";
    public static final String FEES_URL = BASE_URL_ERP + "apk_feeCollection.asmx";
    public static final String EXAM_URL = BASE_URL_ERP + "apk_Exam.asmx";
    public static final String FEES_URL1 = BASE_URL_ERP + "apk_Fees.asmx";
    public static final String GETSTUDENTFEEHISTORYINFO_METHODNAME = "getstudentfeehistoryinfo";
    public static final String GETSTUDENTFEEINFOSTRUCTURE_METHODNAME = "GetStudentFeesInfoStructureWise";
    public static final String GETSTUDENTFEESTRUCTUREINFO_METHODNAME = "getstudentfeestructureinfo";
    public static final String STUDENTFEESCOLLECTIONLIST_METHODNAME = "StudentFeesCollectionList";
    public static final String CHKINSTITUTEORUSEREXISTINERP_METHODNAME = "ChkInstituteORUserExistInERP ";
    public static final String GETCBSEFINALMARKSHEETSTUDENTWISE_METHODNAME = "getCBSEFinalMarkSheetStudentWise ";
    public static final String GETFEESTRUCTUREWISESTUDENTFEECOLLECTIONDETAIL_METHODNAME = "GetFeeStructureWiseStudentFeeCollectionDetail";
    public static final String STUDENTID = "StudentID";
    public static final String FEESTRUCTUREID = "FeeStructureID";
    public static final String GETEXAMSCHEDULELIST = "GetExamsScheduleList";
    public static final String GETEXAMSRESULTLIST = "GetExamsResultList";
    public static final String GETEXAMRESULTTIMETABLE = "GetExamResultTimeTable";
    public static final String MAKEPAYMENT = "makePayment";
    public static final String MAKEPAYMENT_NEW = "makePayment_new"; // add PayAmt by hardik_kanak 26-11-2020


    public static final String GetStudentWiseBusPickupAndDropInformation = "GetStudentWiseBusPickupAndDropInformation";
    public static final String GetStudentWiseBusRouteInformation = "GetStudentWiseBusRouteInformation";

    //

    /**
     * HAPPYGRAM PARSING
     */

    public static final String HAPPYGRAM_ID = "HappyGramID";
    public static final String HAPPYGRAM_MEMBERID = "MemberID";
    public static final String HAPPYGRAM_FULLNAME = "FullName";
    public static final String HAPPYGRAM_REGISTRATIONNO = "RegistrationNo";
    public static final String HAPPYGRAM_ROLLNO = "RollNo";
    public static final String HAPPYGRAM_HAPPYGRAMID = "HappyGramID";
    public static final String HAPPYGRAM_APPRECIATION = "Appreciation";
    public static final String HAPPYGRAM_NOTE = "Note";
    public static final String HAPPYGRAM_EMOTION = "Emotion";
    public static final String HAPPYGRAM_DATEOFHAPPYGRAM = "DateOfHappyGram";
    public static final String HAPPYGRAM_SUBJECTID = "SubjectID";
    public static final String HAPPYGRAM_SUBJECTNAME = "SubjectName";
    public static final String HAPPYGRAM_DELETEMETHODNAME = "DeleteHappyGram";


    /**
     * PARENTPROFILE
     */

    public static final String PARENTPROFILE_METHODNAME = "GetParentProfile";
    public static final String CHNAGEPROFILEPHOTOS_METHODNAME = "ChnageProfilePhotos";
    public static final String PARENTPROFILE_URL = BASE_URL + "apk_parentprofile.asmx";
    public static final String NAMEPHOTO = "NamePhoto";
    public static final String PHOTOFILE = "PhotoFile";

    /**
     * PARENTPROFILE PARSING
     */
    public static final String PARENTPROFILE_PARENTPROFILEID = "ParentProfileID";
    public static final String PARENTPROFILE_FULLNAME = "FullName";
    public static final String PARENTPROFILE_AVTAR = "Avtar";
    public static final String PARENTPROFILE_RELATIONTYPETERM = "RelationTypeTerm";
    public static final String PARENTPROFILE_CONTACTNO = "ContactNo";
    public static final String PARENTPROFILE_MOBILENO = "MobileNo";
    public static final String PARENTPROFILE_OCCUPATIONTERM = "OccupationTerm";
    public static final String PARENTPROFILE_DESIGNATIONOCCUPATIONTERM = "DesignationOccupationTerm";
    public static final String PARENTPROFILE_OFFICEADDRESS = "OfficeAddress";
    public static final String PARENTPROFILE_ISPICKUP = "IsPickup";
    public static final String PARENTPROFILE_FIRSTNAME_PARSING = "FirstName";
    public static final String PARENTPROFILE_LASTNAME_PARSING = "LastName";
    public static final String PARENTPROFILE_DOB_PARSING = "DateOfBirth";
    public static final String PARENTPROFILE_DATEOFANEVERSARY = "DateOfAnniversary";
    public static final String PARENTPROFILE_HIGHESTSTUDY = "HighestStudyTerm";
    public static final String PARENTPROFILE_EMAILID = "EmailID";
    public static final String PARENTPROFILE_OCCDETAIL = "OccupationDetails";
    public static final String PARENTPROFILE_HOMEADDRESSpARSING = "HomeAddress";
    public static final String PARENTPROFILE_IDNOPARSING = "IDNo";
    public static final String PARENTPROFILE_TYPETERM = "IDTypeTerm";
    public static final String PARENTPROFILE_DATEOFISSUE = "DateOfIssue";
    public static final String PARENTPROFILE_DATEOFXEP = "DateOfExpiry";
    public static final String PARENTPROFILE_IDPROOFATTACHED = "IDProofAttached";

    /**
     * PARENTPROFILE ADD
     */

    public static final String PARENTPROFILE_ADD_METHODNAME = "CreateParentProfile";
    public static final String PARENTPROFILE_INITIALTERM = "InitialTerm";
    public static final String PARENTPROFILE_FIRSTNAME = "FirstName";
    public static final String PARENTPROFILE_LASTNAME = "LastName";
    public static final String PARENTPROFILE_FULLNAMEADD = "FullName";
    public static final String PARENTPROFILE_DOB = "DateOfBirth";
    public static final String PARENTPROFILE_DOA = "DateOfAnniversary";
    public static final String PARENTPROFILE_HIGHESTUDY = "HighestStudy";
    public static final String PARENTPROFILE_RELATION = "Relation";
    public static final String PARENTPROFILE_OCCUPATION = "Occupation";
    public static final String PARENTPROFILE_DESIGNATION = "Designation";
    public static final String PARENTPROFILE_OCCUPATIONDETAIL = "OccupationDetails";
    public static final String PARENTPROFILE_CONTACTNOADD = "ContactNo";
    public static final String PARENTPROFILE_MOBILENOADD = "MobileNo";
    public static final String PARENTPROFILE_EMAIL = "Email";
    public static final String PARENTPROFILE_HOMEADDRESS = "HomeAddress";
    public static final String PARENTPROFILE_OFFICENUMBER = "OfficeAddress";
    public static final String PARENTPROFILE_IDNO = "IDNo";
    public static final String PARENTPROFILE_IDTYPE = "IDType";
    public static final String PARENTPROFIL_DATEOFISSUE = "DateOfIssue";
    public static final String PARENTPROFILE_DATEOFEXPIRY = "DateOfExpiry";
    public static final String PARENTPROFILE_IDPROOFATTACH = "IDProofAttach";
    public static final String PARENTPROFILE_PARENTPHOTO = "ParentPhoto";
    public static final String PARENTPROFILE_ISPICK = "IsPickup";
    public static final String PARENTPROFILE_PARENTFILE = "ParentFile";
    public static final String PARENTPROFILE_IDPROOFFILE = "IDProofFile";

    /**
     * PHONEBOOK
     */
    public static final String PHONEBOOK_METHODNAME = "GetPhoneBook ";
    public static final String PHONEBOOK_URL = BASE_URL + "apk_phonebook.asmx";

    /**
     * PHONEBOOK PARSING
     */
    public static final String PHONEBOOK_PHONEBOOKID = "PhoneBookID";
    public static final String PHONEBOOK_CONTACTNAME = "ContactName";
    public static final String PHONEBOOK_CONTACTTYPETERM = "ContactTypeTerm";
    public static final String PHONEBOOK_CONTACTDETAIL = "ContactDetails";
    public static final String PHONEBOOK_CONTACTNO = "ContactNo";
    public static final String PHONEBOOK_PRIORITYLEVELTERM = "PriorityLevelTerm";

    /**
     * PHONEBOOK DELETE
     */
    public static final String PHONEBOOK_DELETE_METHODNAME = "DeletePhoneBook";
    public static final String PHONEBOOK_DELETE_PHONEBOOKID = "PhonebookID";

    /**
     * PHONEBOOK ADD
     */
    public static final String PHONEBOOK_ADD_METHODNAME = "CreatePhoneBook";
    public static final String PHONEBOOK_ADD_CONTACTNAME = "ContactName";
    public static final String PHONEBOOK_ADD_CONTACTDETAIL = "ContactDetails";
    public static final String PHONEBOOK_ADD_CONTACTNO = "ContactNo";
    public static final String PHONEBOOK_ADD_CONTACTTYPETERM = "ContactTypeTerm";
    public static final String PHONEBOOK_ADD_PRIORITYLEVEL = "PriorityLevelTerm";

    /**
     * GROUP List
     */
    public static final String CMSPAGE_ID = "CMSPageID";
    public static final String CMSPAGES_ID = "CMSPagesID";
    public static final String CMSPAGES_PAGENAME = "PageName";
    public static final String CMSPAGES_PAGE_TITLE = "PageTitle";
    public static final String CMSPAGES_PAGEDETAILS = "PageDetails";
    public static final String CMSPAGES_PAGETYPE = "PageType";

    /**
     * Cms Page List
     */
    public static final String CMSPAGE_URL = BASE_URL + "apk_cmspage.asmx";
    public static final String GETCMSPAGES_METHODNAME = "GetCmsPages";
    public static final String GETCMSPAGEDETAIL_METHODNAME = "GetCmsPageDetail";

    /**
     * Photo Upload
     */
//    public static final String UPLOAD_PHOTO_METHODNAME = "UploadFile";
    public static final String UPLOAD_PHOTO_METHODNAME = "UploadFileByBase64ST";
    public static final String GET_PHOTO_METHODNAME = "GetPhotoList";
    public static final String ADDPHOTO_URL = BASE_URL_BETA + "apk_Post.asmx";
    public static final String PHOTO_FILENAME = "FileName";
    public static final String PHOTO_FILE = "File";
    public static final String PHOTO_INSTITUTEID = "InstituteID";
    public static final String PHOTO_FILETYPE = "FileType";
    public static final String PHOTO_CLIENTID = "ClientID";
    public static final String PHOTO_MEMBERID = "MemberID";

    // homework upload
    public static final String UPLOADHOMEWORK_URL = BASE_URL_BETA + "apk_StudentReplay.asmx";


    // upload homework
    public static final String UPLOAD_HOMEWORK_METHODNAME = "UploadHomeworkImage";
    public static final String UPLOAD_HOMEWORK_METHODNAME2 = "UploadHomeworkImageByBase64STR";
    public static final String UPLOAD_HOMEWORK_METHODNAME3 = "UploadHomeworkImageByBase64STRWithFile";

    // get upload homework
    public static final String GET_UPLOAD_HOMEWORK_FOR_TEACHER_METHODNAME = "StudentReplay_GetAllMemberDetailsByAssociation";
    public static final String GET_UPLOAD_HOMEWORK_FOR_PARENT_METHODNAME = "StudentReplay_GetAssociationDetailsByMember";
    public static List<String> GetHwUploadedImageList;

    // save teacher comment onstudent uploaded homework
    public static final String SAVE_TEACHER_COMMENT_STUDNET_UPLODED_HOMEWORK = "SaveTeacherCommentOnStudentUploadedHomework";
    public static String STUDENT_REPLAY_ID = "StudentReplayID";


    // submit homework
    public static final String HOMEWORK_CREATED_BY = "CreatedBy";
    public static final String HOMEWORK_NOTE = "Note";
    public static final String HOMEWORK_TEXT_CONTAINS = "TextContaine";
    public static final String HOMEWORK_IMAGES_URL = "ImageURL";
    //    public static final String SUBMIT_HOMEWORK_METHODNAME = "UploadHomeworkFromParents";
    // chnaged api to add filetype
    public static final String SUBMIT_HOMEWORK_METHODNAME = "UploadHomeworkFromParentswithFileType";

    /**
     * Photo Post
     */
    public static final String ADDPHOTOS_METHODNAME = "AddPhotos";
    public static final String PHOTO_URL = BASE_URL + "apk_Photos.asmx";
    public static final String PHOTO_POST_INSTITUTEID = "InstituteID";
    public static final String PHOTO_POST_CLIENTID = "ClientID";
    public static final String PHOTO_POST_WALLID = "WallID";
    public static final String PHOTO_POST_MEMBERID = "MemberID";
    public static final String PHOTO_POST_USERID = "UserID";
    public static final String PHOTO_POST_BEACHID = "BeachID";
    public static final String PHOTO_POST_POSTSHARETYPE = "PostShareType";
    public static final String PHOTO_POST_IMAGEPATH = "ImagePath";
    public static final String PHOTO_POST_FILETYPE = "FileType";

    /**
     * album
     */
    public static final String UPLOAD_ALBUM_METHODNAME = "AddAlbum";
    public static final String ALBUM_INSTITUTEID = "InstituteID";
    public static final String ALBUM_CLIENTID = "ClientID";
    public static final String ALBUM_WALLID = "WallID";
    public static final String ALBUM_MEMBERID = "MemberID";
    public static final String ALBUM_USERID = "UserID";
    public static final String ALBUM_BEACHID = "BeachID";
    public static final String ALBUM_ALBUMSHARETYPE = "AlbumShareType";
    public static final String ALBUM_IMAGESPATH = "ImagesPath";
    public static final String ALBUM_ALBUMDETAILS = "AlbumDetails";
    public static final String ALBUM_ALBUMTITLE = "AlbumTitle";
    public static final String ALBUM_PLACES = "Places";
    public static final String ALBUM_POSTBYTYPE = "PostByType";
    public static final String ALBUM_APPROVED = "approved";

    /**
     * GetDynamic Data
     */
    public static final String GETUSERDYNAMICMENUDATA_METHODNAME = "GetUserDynamicMenuData";
    public static final String GETUSERDYNAMICMENUDATA_ROLENAME = "RoleName";

    // FromString for UploadHomeworkApproveal Layout
    public static String FROMUPLOADHOMEWORK = "";

    /**
     * GetDynamic Data PARSING
     */
    public static final String GETUSERDYNAMICMENUDATA_TABLE = "Table";
    public static final String GETUSERDYNAMICMENUDATA_WALLID = "WallID";
    public static final String GETUSERDYNAMICMENUDATA_WALLNAME = "WallName";
    public static final String GETUSERDYNAMICMENUDATA_WALLIMAGE = "WallImage";
    public static final String GETUSERDYNAMICMENUDATA_ASSOCIATIONTYPE = "AssociationType";

    /**
     * ADD Post
     */
    public static final String POST_METHODNAME = "Post";
    public static final String POST_POSTSHARETYPE = "PostShareType";
    public static final String POST_POSTCOMMENTNOTE = "PostCommentNote";
    public static final String POST_POSTBYTYPE = "PostByType";
    public static final String POST_IMAGEPATH = "ImagePath";
    public static final String POST_FILEMINETYPE = "FileMineType";
    public static final String POST_FILETYPE = "FileType";
    public static final String POST_APPROVED = "approved";
    public static final String STANDARDWALL = "standardwall";
    public static final String INSTITUTEWALL = "institutewall";
    public static final String DIVISIONWALL = "divisionwall";
    public static final String SUBJECTWALL = "subjectwall";
    public static final String PROJECTWALL = "projectwall";
    public static final String GROUPWALL = "groupwall";
    public static final String Wall = "wall";
    public static String WALLTITLE = "";

    // selected wall title

    public static String SELECTED_WALL_TITLE = "";
    public static String FROM_SELECTED_WALL = "";
    public static String FOR_WALL_SELECTION = "";
    public static String SELECTED_DYNAMIC_WALL_ID = "";


    /**
     * FRIENDS
     */

    public static final String FRIENDS_METHODNAME = "GetFriendList";
    public static final String FRIENDS_URL = BASE_URL_BETA + "apk_friends.asmx";

    /**
     * FRIENDS PARSING
     */

    public static final String FRIENDS_WALLID = "WallID";
    public static final String FRIENDS_FRIENDLIST = "FriendListID";
    public static final String FRIENDS_FRIENDID = "FriendID";
    public static final String FRIENDS_FULLNAME = "FullName";
    public static final String FRIENDS_PROFILEPIC = "ProfilePicture";
    public static final String FRIENDS_DIVISIONID = "DivisionID";
    public static final String FRIENDS_DIVISIONNAME = "DivisionName";
    public static final String FRIENDS_GRADEID = "GradeID";
    public static final String FRIENDS_GRADENAME = "GradeName";

    /**
     * SERACH FRIENDS
     */

    public static final String SEARCH_FRIENDS_METHODNAME = "Searchfriend";

    /**
     * SEARCH FRIENDS PARSING
     */

    public static final String SEARCH_SEARCHNAME = "SearchName";
    public static final String SEARCH_MEMBERID = "MemberID";
    public static final String SEARCH_ROLENAME = "RoleName";
    public static final String SEARCH_WALLID = "WallID";
    public static final String SEARCH_FULLNAME = "FullName";
    public static final String SEARCH_PROFILEPICTURE = "ProfilePicture";
    public static final String SEARCH_ISFRIEND = "IsFriend";
    public static final String SEARCH_ISREQUESTED = "IsRequested";

    /**
     * SEND REQUEST FRIENDS
     */

    public static final String SEND_FRIENDS_METHODNAME = "SendFriendRequest";

    /**
     * SEND REQUEST FRIEND PARSING
     */

    public static final String SEND_TOMEMBERID = "ToMemberID";
    public static final String SEND_BYMEMBERID = "ByMemberID";
    public static final String SEND_INSTITUTEID = "InstituteID";
    public static final String SEND_CLIENTID = "ClientID";
    public static final String SEND_WALLID = "WallID";
    public static final String SEND_USERID = "UserID";

    /**
     * GET REQUEST FRIENDS
     */

    public static final String GET_FRIENDS_METHODNAME = "GetFriendRequestList";

    /**
     * GET REQUEST FRIEND PARSING
     */

    public static final String GET_FRIENDLISTID = "FriendListID";
    public static final String GET_REQUESTDATE = "RequestDate";
    public static final String GET_FULLNAME = "FullName";
    public static final String GET_PROFILEPICTURE = "ProfilePicture";
    public static final String GET_REQUESTID = "RequestID";
    public static final String GET_REQUESTWALLID = "RequestWallID";

    /**
     * APPROVE REQUEST FRIENDS
     */

    public static final String APPROVE_REQUEST_FRIENDS_METHODNAME = "ApproveRequest";

    /**
     * DELETE REQUEST FRIENDS
     */

    public static final String DELETE_REQUEST_FRIENDS_METHODNAME = "DeleteRequest";

    // Timetable

    public static final String SUBJECTNAME = "SubjectName";
    public static final String DAYOFWEEK = "Dayofweek";


    /**
     * TODO
     */

    public static final String TODO_METHODNAME = "GetToDosList";
    public static final String TODO_URL = BASE_URL_BETA + "apk_todos.asmx";
    public static final String DELET_TODO_METHODNAME = "DeleteToDos";
    public static final String ADDTODO_METHODNAME = "SaveUpdateTodos";
    public static final String TODOSID = "TodosID";
    public static final String ENDDATE = "EndDate";
    public static final String TITLETODO = "Title";
    public static final String DETAILS = "Details";
    public static final String USERNAME = "UserName";
    public static final String TYPETERM = "TypeTerm";
    public static final String COMPLETDATE = "CompletDate";
    public static final String STATUS = "Status";
    public static final String ONCREATED = "OnCreated";
    public static final String YEAR = "Year";
    public static final String MONTH = "Month";
    public static final String CREATEON = "CreateOn";

    public static final String ADDTODO_TITLE = "title";
    public static final String ADDTODO_DETAILS = "details";
    public static final String ADDTODO_TYPETERM = "typeterm";
    public static final String ADDTODO_STATUS = "status";
    public static final String ADDTODO_STARTDATE = "startdate";
    public static final String ADDTODO_ENDDATE = "EndDate";
    public static final String ADDTODO_COMPLETDATE = "CompletDate";


    /**
     * NOTES
     */


    public static final String NOTES_METHODNAME = "GetNotesList";
    public static final String NOTES_URL = BASE_URL_BETA + "apk_notes.asmx";

    /**
     * NOTES PARSING
     */

    public static final String NOTES_NOTESID = "NotesID";
    public static final String NOTES_DATE = "NoteDate";
    public static final String NOTES_GRADEID = "GradeID";
    public static final String NOTES_DIVISIONID = "DivisionID";
    public static final String NOTES_SUBJECTID = "SubjectID";
    public static final String NOTES_SUBJECTNAME = "SubjectName";
    public static final String NOTES_NOTETITLE = "NoteTitle";
    public static final String NOTES_NOTEDETAILS = "NoteDetails";
    public static final String NOTES_ACTIONSTARTDATE = "ActionStartDate";
    public static final String NOTES_ACTIONENDDATE = "ActionEndDate";
    public static final String NOTES_DRESSCODE = "DressCode";
    public static final String NOTES_ISREAD = "IsRead";
    public static final String NOTES_IAMREADY = "IamReady";
    public static final String NOTES_PARENTNOTE = "ParentNote";
    public static final String NOTES_RATING = "Rating";
    public static final String NOTES_USERNAME = "UserName";
    public static final String NOTES_PROFILEPICTURE = "ProfilePicture";
    public static final String NOTES_INSTITUTIONWALLID = "InstitutionWallID";
    public static final String NOTES_STRACTIONSTARTDATE = "strActionStartDate";
    public static final String NOTES_REFERENCELINK = "ReferenceLink";

    /**
     * Hostel - Pocket Money
     */

    public static final String HOSTELPOCKETMONEY = "GetStudentPocketMoneyTransactionDetails";

    /**
     * CHECK IS READ
     */

    public static final String CHECK_METHODNAME = "CheckIread";
    public static final String APPROVE_METHODNAME = "CheckIsApprove";
    public static final String CREATENOTES_METHODNAME = "CreateNotes";
    // chnage to add ref link : Commented by krishna
    public static final String CREATENOTESWITHMULTY_METHODNAME = "CreateNotesWithMulty_Memberselection_New";
    //    public static final String CREATENOTESWITHMULTY_METHODNAME = "CreateNotesWithMulty_Memberselection";
    //    public static final String CREATENOTESWITHMULTY_METHODNAME = "CreateNotesWithMulty";
    public static final String ASSOCIATIONDELETE_METHODNAME = "AssociationDelete";
    public static final String LEAVE_DELETE_METHODNAME = "DeleteLeave";
    public static final String CHANGELEAVESTATUS_METHODNAME = "ChangeLeaveStatus";
    public static final String StudentLeaveNoteID = "StudentLeaveNoteID";
    public static final String status = "status";
    public static final String Note = "Note";
    public static final String CHECK_URL = BASE_URL_BETA + "apk_notes.asmx";
    public static final String EVENT_URL = BASE_URL_BETA + "apk_event.asmx";
    public static final String CHECKED_STUDENT_URL = BASE_URL_BETA + "apk_notifications.asmx";
    public static final String CHECKED_STUDENT_METHODNAME = "ReadApproveGetMemberReadApproveList";

    public static final String ASSID = "AssID";
    public static final String ASSTYPE = "AssType";

    /**
     * CHECK PARSING
     */

    public static final String ASSOCIATIONID = "AssociationID";
    public static final String PRIMARYID = "PrimaryID";
    public static final String HOSTEL_ASSOCIATIONTYPE = "AssociationType";
    // changed api to remove pagination after offline wall data display - Commented by Krishna : 30-07-2020
    public static final String GETDYNAMICWALLDATA_METHODNAME = "GetDynamicWallData";
    //    public static final String GETDYNAMICWALLDATA_METHODNAME = "GetDynamicWallData_AllData";
    public static final String GETMYWALLDATA_METHODNAME = "GetMyWallData";
    //    public static final String GETMYWALLDATA_METHODNAME = "GetMyWallData_AllData";
    public static final String PROFILEWALL = "profilewall";

    public static final String NOTES_EDITID = "EditID";
    public static final String GETPROJECTTYPE_TERMID = "TermID";
    public static final String GETPROJECTTYPE_TERM = "Term";
    public static final String GETPROJECTTYPE_ORDERNO = "OrderNo";
    public static final String GETPROJECTTYPE_ISDEFAULT = "IsDefault";
    public static final String GETPROJECTTYPE_DEFAULTVALUE = "DefaultValue";
    public static final String GETPROJECTTYPE_CATEGORY = "Category";

    /**
     * Leave
     */

    public static final String LEAVE_METHODNAME = "LeaveAppList";
    public static final String LEAVECOUNTBYGRADEDIVISION_METHODNAME = "LeaveCountByGradeDivision";
    public static final String LEAVE_URL = BASE_URL_BETA + "apk_leave.asmx";

    public static final String ROUTELIST_URL = BASE_URL_BETA + "GetAllData.asmx";

    /**
     * LEAVE PARSING
     */

    public static final String LEAVE_SAVEUPDATETODOS = "SaveUpdateTodos";
    public static final String LEAVE_SCHOOLLEAVENOTEID = "SchoolLeaveNoteID";
    public static final String LEAVE_DATEOFAPPLICATION = "DateOfApplication";
    public static final String LEAVE_APPLICATIONSTATUS_TERM = "ApplicationStatus_Term";
    public static final String LEAVE_REASONFORLEAVE = "ReasonForLeave";
    public static final String LEAVE_TEACHERCOMMENT = "TeacherComment";
    public static final String LEAVE_APPROVEDON = "ApprovedOn";
    public static final String LEAVE_STARTDATE = "StartDate";
    public static final String LEAVE_STUDENTNAME = "StudentName";
    public static final String LEAVE_APPLICATIONBY = "ApplicationBY";
    public static final String LEAVE_TEACHERNAME = "TeacherName";
    public static final String LEAVE_APPLICATIONBY_TERM = "ApplicationBy_Term";
    public static final String LEAVE_STUDENTID = "StudentID";
    public static final String LEAVE_DEFSTATUS = "DefStatus";
    public static final String LEAVE_ISPERAPPLICATION = "IsPerApplication";
    public static final String LEAVE_APPLICATIONTOTEACHERID = "ApplicationToTeacherID";
    public static final String SCHOOLLEAVENOTEID = "SchoolLeaveNoteID";
    public static final String POSTBYTYPE = "PostByType";
    public static final String LEAVE_TEACHERID = "TeacherID";
    public static final String ISPREAPPLICATION = "IsPreApplication";
    public static final String REASONFORLEAVE = "ReasonForLeave";
    public static final String STARTDATE = "StartDate";


    /**
     * Add Event Method
     **/

    // chnage to add ref link  : commented By Krishna 08-06-2020
//    public static final String ADD_EVENT = "AddEvent";
    public static final String ADD_EVENT = "AddEvent_New";
    public static final String DELETE_EVENT = "DeleteEvents";

}
