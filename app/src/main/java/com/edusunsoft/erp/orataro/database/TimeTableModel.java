package com.edusunsoft.erp.orataro.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "TimeTable")
public class TimeTableModel {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "StartTime")
    @SerializedName("StartTime")
    public String StartTime;

    @ColumnInfo(name = "EndTime")
    @SerializedName("EndTime")
    public String EndTime;

    @ColumnInfo(name = "Dayofweek")
    @SerializedName("Dayofweek")
    public String Dayofweek;

    @ColumnInfo(name = "SubjectName")
    @SerializedName("SubjectName")
    public String SubjectName;

    @ColumnInfo(name = "SubjectID")
    @SerializedName("SubjectID")
    public String SubjectID;

    @ColumnInfo(name = "totalTime")
    @SerializedName("totalTime")
    public long totalTime;

    @ColumnInfo(name = "ForTeacherORStudent")
    @SerializedName("ForTeacherORStudent")
    public String ForTeacherORStudent;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getForTeacherORStudent() {
        return ForTeacherORStudent;
    }

    public void setForTeacherORStudent(String forTeacherORStudent) {
        ForTeacherORStudent = forTeacherORStudent;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getDayofweek() {
        return Dayofweek;
    }

    public void setDayofweek(String dayofweek) {
        Dayofweek = dayofweek;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    @Override
    public String toString() {
        return "TimeTableModel{" +
                "id=" + id +
                ", StartTime='" + StartTime + '\'' +
                ", EndTime='" + EndTime + '\'' +
                ", Dayofweek='" + Dayofweek + '\'' +
                ", SubjectName='" + SubjectName + '\'' +
                ", SubjectID='" + SubjectID + '\'' +
                ", totalTime=" + totalTime +
                '}';
    }
}
