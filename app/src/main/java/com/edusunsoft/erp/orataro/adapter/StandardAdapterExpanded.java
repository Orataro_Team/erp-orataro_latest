package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.DivisionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StandardAdapterExpanded extends BaseAdapter implements ResponseWebServices {
	
	private Context mContext;
	private LayoutInflater layoutInfalater;
	private TextView txtDivision,txtSubject;
	private ArrayList<StandardModel> standardModels;
	private LinearLayout[] ll_division;
	private TextView txtStandard;
	private int pos= 0;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };
	boolean isShowCheckbox = false;
	boolean isDivisionShow = false;
	
	public StandardAdapterExpanded(Context context, ArrayList<StandardModel> standardModels) {
		this.mContext = context;
		this.standardModels = standardModels;
	}
	
	public StandardAdapterExpanded(Context context, ArrayList<StandardModel> standardModels, boolean isCheck) {
		this.mContext = context;
		this.standardModels = standardModels;
		isShowCheckbox = isCheck;
		ll_division= new LinearLayout[standardModels.size()];
	}
	
	public StandardAdapterExpanded(Context context, ArrayList<StandardModel> standardModels, boolean isCheck, boolean isDivisionShow) {
		this.mContext = context;
		this.standardModels = standardModels;
		isShowCheckbox = isCheck;
		this.isDivisionShow = isDivisionShow;
	}
	
	@Override
	public int getCount() {
		return standardModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.divisiongrade, parent, false);
		ll_division[position] = (LinearLayout) convertView.findViewById(R.id.ll_division);
		CheckBox chkdivision = (CheckBox) convertView.findViewById(R.id.chkdivision);
		txtStandard = (TextView) convertView.findViewById(R.id.txtStanderd);
		txtDivision =  (TextView) convertView.findViewById(R.id.txtDivision);
		txtSubject =   (TextView) convertView.findViewById(R.id.txtSubject);
		convertView.setBackgroundColor(colors[position % colors.length]);
		txtStandard.setText(standardModels.get(position).getStandardName());

		if(isShowCheckbox){
			txtDivision.setVisibility(View.GONE);
			txtSubject.setVisibility(View.GONE);
		}else{
			txtDivision.setText(standardModels.get(position).getDivisionName());
			txtSubject.setText(standardModels.get(position).getSubjectName());
		}

		if(isDivisionShow){
			txtDivision.setText(standardModels.get(position).getDivisionName());
			txtDivision.setVisibility(View.VISIBLE);
			chkdivision.setVisibility(View.VISIBLE);
		}
		if(standardModels.get(position).isChecked()){
			chkdivision.setChecked(true);
		}else{
			chkdivision.setChecked(false);
		}

		chkdivision.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				standardModels.get(position).setChecked(isChecked);
			}
		});

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gradeList(standardModels.get(position).getStandrdId());
				pos = position;
			}
		});

		return convertView;

	}

	public ArrayList<StandardModel> getSelectedDivision(){

		ArrayList<StandardModel> selectedDivision = new ArrayList<StandardModel>();
		if(standardModels != null && standardModels.size()>0){
			for(int i =0;i<standardModels.size();i++){
				if(standardModels.get(i).isChecked()){
					selectedDivision.add(standardModels.get(i));	
				}
			}
		}
		return selectedDivision;
	}

	public void gradeList(String gradeId){
		ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
		arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
		arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
		arrayList.add(new PropertyVo(ServiceResource.TEACHERID,
				new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
		arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
				new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
		arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
				new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
		arrayList.add(new PropertyVo(ServiceResource.TYPE,
				ServiceResource.STANDERD_WSCALL_TYPE_DIVISION));
		arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, null));

		new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STANDERD_METHODNAME, ServiceResource.STANDERD_URL);
	}

	@Override
	public void response(String result, String methodName) {
		JSONArray hJsonArray;
		try {

			if (result.contains("\"success\":0")) {

			} else {
				hJsonArray = new JSONArray(result);
				Global.divisionModels = new ArrayList<DivisionModel>();

				for (int i = 0; i < hJsonArray.length(); i++) {
					JSONObject hJsonObject = hJsonArray.getJSONObject(i);
					DivisionModel model = new DivisionModel();
					model.setDivisionId(hJsonObject.getString(ServiceResource.STANDARD_DIVISIONID));
					model.setDivisionName(hJsonObject.getString(ServiceResource.STANDARD_DIVISIONNAME));
					Global.divisionModels.add(model);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		View v = layoutInfalater.inflate(R.layout.divisiongrade, null);
		ll_division[pos].removeAllViews();
		LinearLayout layout=new LinearLayout(mContext);
		layout.removeAllViews();
		layout.setOrientation(LinearLayout.VERTICAL);
		for(int i = 0; i< Global.divisionModels.size(); i++){
			TextView textview = new TextView(mContext);
			textview.setText(Global.divisionModels.get(i).getDivisionName());
			layout.addView(textview);
		}
		ll_division[pos].addView(layout);
	}

}
