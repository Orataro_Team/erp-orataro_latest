package com.edusunsoft.erp.orataro.database;

import java.util.List;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface DynamicWallSettingDataDao {

    @Insert
    void insertDynamicSettingWall(DynamicWallSettingModel dynamicWallSettingModel);

    @Query("SELECT * from DynamicWallSetting")
    DynamicWallSettingModel getDynamicWallSettingData();

    @Query("DELETE  FROM DynamicWallSetting")
    int deleteDynamicWallSettingData();




}
