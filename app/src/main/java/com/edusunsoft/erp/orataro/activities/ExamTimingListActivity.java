package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ExamTimingDataListAdapter;
import com.edusunsoft.erp.orataro.util.Global;


public class ExamTimingListActivity extends AppCompatActivity {

    LinearLayout lyl_transaction_detail;
    private static final String TAG = "RouteListActivity";
    Context mContext;

    RecyclerView routeList;
    private TextView txtNoDataFound;

    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;
    public static String EXAMTITLE = "";

    public static Intent getInstance(Context mcontext, String s) {
        Intent i = new Intent(mcontext, ExamTimingListActivity.class);
        i.putExtra(EXAMTITLE, s);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);

        Initialization();

    }

    private void Initialization() {

        mContext = this;

        lyl_transaction_detail = (LinearLayout) findViewById(R.id.lyl_transaction_detail);
        routeList = findViewById(R.id.routelist);
        txtNoDataFound = findViewById(R.id.txt_nodatafound);
        imgLeftHeader = findViewById(R.id.img_home);
        imgLeftHeader.setImageResource(R.drawable.back);
        imgLeftHeader.setOnClickListener(v -> {
            this.finish();
        });
        imgRightHeader = findViewById(R.id.img_menu);
        txtHeader = findViewById(R.id.header_text);

        try {
            txtHeader.setText(getIntent().getStringExtra(EXAMTITLE));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("Examtiminglist", Global.examtimingList.toString());
        if (Global.examtimingList.size() > 0) {

            ExamTimingDataListAdapter examTimingDataListAdapter = new ExamTimingDataListAdapter(mContext, Global.examtimingList);
            routeList.setAdapter(examTimingDataListAdapter);
            lyl_transaction_detail.setVisibility(View.VISIBLE);
            routeList.setVisibility(View.VISIBLE);
            txtNoDataFound.setVisibility(View.GONE);

        } else {

            lyl_transaction_detail.setVisibility(View.GONE);
            routeList.setAdapter(null);
            txtNoDataFound.setVisibility(View.GONE);

        }


    }


}
