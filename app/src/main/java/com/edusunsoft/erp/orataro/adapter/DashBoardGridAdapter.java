package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.MenuPermissionModel;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class DashBoardGridAdapter extends BaseAdapter {

    private Context context;
    private String[] colorlist = {"#2D2079", "#582388", "#741A87", "#DC55A1",
            "#2D288A", "#2D68A0", "#6E649E", "#B089A9", "#E487A5", "#40AD9F",
            "#6BBE95", "#582388", "#741A87", "#E487A5", "#2D288A", "#6E649E", "#DC55A1", "#741A87", "#582388", "#2D2079", "#582388", "#E487A5", "#DC55A1"};
    private String[] actionlist = null;
    private int height = 0;
    private int[] iconlist;
    private String[] actionlistTeacher;
    private int[] iconlistTeacher = {R.drawable.profile,
            R.drawable.circular, R.drawable.wall,
            R.drawable.homework,
            R.drawable.classwork, R.drawable.attendance,
            R.drawable.ptcommunication, R.drawable.examtiming,
            R.drawable.timetable, R.drawable.notes,
            R.drawable.holiday,
            R.drawable.calendar,
            R.drawable.poll, R.drawable.notification, R.drawable.aboutorataro, R.drawable.settings,
            R.drawable.faq, R.drawable.switchaccount};
//    R.drawable.todo,

    ArrayList<MenuPermissionModel> menuPermissionList = new ArrayList<>();

    private int count = 0;
    private RefreshListner listner;
    private LayoutInflater layoutInfalater;

    public DashBoardGridAdapter(Context mcontext, RefreshListner listner, ArrayList<MenuPermissionModel> menuPermissionList) {

        this.context = mcontext;
        this.listner = listner;
        this.menuPermissionList = menuPermissionList;

        iconlist = new int[]{R.drawable.profile, R.drawable.circular,
                R.drawable.wall, R.drawable.homework,
                R.drawable.classwork, R.drawable.attendance,
                R.drawable.fees,
                R.drawable.receipt,
                R.drawable.examresult,
                //	R.drawable.minibus,
                R.drawable.ptcommunication,
                R.drawable.examtiming, R.drawable.timetable,
                R.drawable.notes,
                R.drawable.holiday,
                R.drawable.calendar, R.drawable.poll,
                R.drawable.notification,
//                R.drawable.todo,
                R.drawable.aboutorataro, R.drawable.settings,
                R.drawable.faq, R.drawable.switchaccount};


        actionlist = new String[]{context.getResources().getString(R.string.Profile),
                context.getResources().getString(R.string.Circular)
                , context.getResources().getString(R.string.Wall),
                context.getResources().getString(R.string.Homework),
                context.getResources().getString(R.string.Classwork),
                context.getResources().getString(R.string.Attendance),
                context.getResources().getString(R.string.Fees),
                context.getResources().getString(R.string.Receipt),
                context.getResources().getString(R.string.ExamResult),
                context.getResources().getString(R.string.PTCommunication),
                context.getResources().getString(R.string.ExamTiming), context.getResources().getString(R.string.TimeTable),
                context.getResources().getString(R.string.Notes), context.getResources().getString(R.string.Holiday),
                context.getResources().getString(R.string.Calendar), context.getResources().getString(R.string.Poll),
                context.getResources().getString(R.string.Notification),

//                context.getResources().getString(R.string.Todo),

                context.getResources().getString(R.string.Aboutorataro),
                context.getResources().getString(R.string.notificationsettings),
                context.getResources().getString(R.string.faq),
                context.getResources().getString(R.string.SwitchAccount)};


        actionlistTeacher = new String[]{context.getResources().getString(R.string.Profile), context.getResources().getString(R.string.Circular)
                , context.getResources().getString(R.string.Wall),
                context.getResources().getString(R.string.Homework),
                context.getResources().getString(R.string.Classwork),
                context.getResources().getString(R.string.Attendance),
                context.getResources().getString(R.string.PTCommunication),
                context.getResources().getString(R.string.ExamTiming), context.getResources().getString(R.string.TimeTable),
                context.getResources().getString(R.string.Notes), context.getResources().getString(R.string.Holiday),
                context.getResources().getString(R.string.Calendar), context.getResources().getString(R.string.Poll),
                context.getResources().getString(R.string.Notification),

//                context.getResources().getString(R.string.Todo),
                context.getResources().getString(R.string.Aboutorataro),
                context.getResources().getString(R.string.notificationsettings),
                context.getResources().getString(R.string.faq),
                context.getResources().getString(R.string.SwitchAccount)};

    }

    @Override
    public int getCount() {

        return menuPermissionList.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolderItem();
            layoutInfalater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfalater.inflate(R.layout.dashboard_menu_list,
                    parent, false);

            viewHolder.gridShellLayout = (LinearLayout) convertView
                    .findViewById(R.id.gridshelllayout);
            viewHolder.txt_action = (TextView) convertView
                    .findViewById(R.id.txt_action);

            viewHolder.txt_action.setInputType(EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE | EditorInfo.TYPE_TEXT_FLAG_IME_MULTI_LINE | EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT);
//            viewHolder.txt_count = (TextView) convertView.findViewById(R.id.txt_count);
            viewHolder.grid_icon = (ImageView) convertView
                    .findViewById(R.id.grid_icon);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolderItem) convertView.getTag();

        }

//        viewHolder.txt_count.setVisibility(View.INVISIBLE);
//        viewHolder.gridShellLayout.setBackgroundColor(Color
//                .parseColor(colorlist[position]));

        if (new UserSharedPrefrence(context).getLoginModel() == null) {
            Utility.getUserModelData(context);
        }

        MenuPermissionModel menuPermissionModel = menuPermissionList.get(position);

        int imageId = getResourseId(this, menuPermissionModel.getMenuIconName().toLowerCase().replace(" ", ""), "drawable", context.getPackageName());
        viewHolder.grid_icon.setImageResource(imageId);

        if (menuPermissionModel.isStatic()) {
            viewHolder.txt_action.setText(menuPermissionModel.getMenuName());
        } else {
            Utility.GetResIDFromStringXML(context, menuPermissionModel.getMenuIconName(), "string", context.getPackageName(), viewHolder.txt_action);
        }


        if (position == 2) {

            /*commented By Krishna : 24-04-2019 Hide Count of Post */

//            if (!new UserSharedPrefrence(context).getLOGIN_NOTIFICATIONCOUNT().equalsIgnoreCase("0")) {
//                viewHolder.txt_count.setVisibility(View.VISIBLE);
//                viewHolder.txt_count.setText(new UserSharedPrefrence(context).getLOGIN_NOTIFICATIONCOUNT());
//            } else {
//                viewHolder.txt_count.setVisibility(View.INVISIBLE);
//            }

            /*END*/

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context,
                        HomeWorkFragmentActivity.class);
                i.putExtra("position", viewHolder.txt_action.getText().toString());
                context.startActivity(i);

            }

        });

        return convertView;

    }

    private int getResourseId(DashBoardGridAdapter dashBoardGridAdapter, String IconName, String drawable, String packageName) {

        try {
            return context.getResources().getIdentifier(IconName, drawable, packageName);
        } catch (Exception e) {
            throw new RuntimeException("Error getting Resource ID.", e);
        }

    }


    public int getString(Context context, String strName, String PackageName) {
        return context.getResources().getIdentifier(strName, "string", PackageName);
    }

    public int getHeight() {
        return height;
    }

    public static class ViewHolderItem {
        LinearLayout gridShellLayout;
        TextView txt_action, txt_count;
        ImageView grid_icon;
    }

}
