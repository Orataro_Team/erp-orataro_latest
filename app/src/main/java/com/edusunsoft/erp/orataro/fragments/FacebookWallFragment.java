package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.PhotosListActivity;
import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.SelectAllFromAdapterInterface;
import com.edusunsoft.erp.orataro.Interface.ShareInterface;
import com.edusunsoft.erp.orataro.Interface.WallCustomeClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.AddPostOnWallActivity;
import com.edusunsoft.erp.orataro.activities.FriendListActivity;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;
import com.edusunsoft.erp.orataro.activities.VideoListActivity;
import com.edusunsoft.erp.orataro.activities.WallMemberActivity;
import com.edusunsoft.erp.orataro.adapter.FetchfriendAdapter;
import com.edusunsoft.erp.orataro.adapter.MyWallAdapter;
import com.edusunsoft.erp.orataro.adapter.WallListAdapter;
import com.edusunsoft.erp.orataro.database.DynamicWallListModel;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingModel;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallListVo;
import com.edusunsoft.erp.orataro.model.WallModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * All Wall
 *
 * @author admin
 */

public class FacebookWallFragment extends Fragment implements OnClickListener,
        WallCustomeClick, ShareInterface, SelectAllFromAdapterInterface, AnimationListener, ResponseWebServices {

    private static final String WALLIDDIFF = "WallIddiff";
    private static final String ISFROM = "isFrom";
    private static final String RELOAD = "reload";
    static View v;
    private static Context mContext;
    private PullAndLoadListView lst_fb_wall;
    private LinearLayout ll_add_new;
    private MyWallAdapter myWallAdapter;
    private ArrayList<WallModel> wallList;
    private TextView tv_post;
    private LinearLayout showHideLayoutstatus, showHidelayoutphotovideo, ll_Photo, ll_Video, ll_Friends;
    private Integer count = 1;
    public boolean isMoreData;
    private int ShareType;
    private Dialog dialog;
    private EditText edtTeacherName;
    private FetchfriendAdapter fetchfriendAdapter;
    private ListView lst_fetch_friend;
    private TextView txt_nodatafound;
    private ImageView iv_select_all;
    private ImageView img_save, img_back;
    private String wallId;
    private static String from;
    private LinearLayout addpostlayout;
    private CircleImageView iv_profile_pic;
    protected int refreshtype;
    private TextView txt_count;
    private boolean reload = false;
    private boolean isOffline = false;
    private static boolean headerClick = false;
    private static boolean isShowMenu = false;
    private static ListView lvbounce;
    private static ArrayList<DynamicWallListModel> subjectlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> standardlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> divisionlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> grouplist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> projectlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<WallListVo> list = new ArrayList<WallListVo>();
    private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;
    private boolean isReloadRequire = false;
    private Runnable mRunnable;
    WallPostModel wallPostModel;
    ImageView img_testing;

    public static final FacebookWallFragment newInstance(String WallIddiff, String isFrom) {

        FacebookWallFragment f = new FacebookWallFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(WALLIDDIFF, WallIddiff);
        bdl.putString(ISFROM, isFrom);
        f.setArguments(bdl);
        return f;

    }

    public static final FacebookWallFragment newInstance(String WallIddiff, String isFrom, boolean reload) {

        FacebookWallFragment f = new FacebookWallFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(WALLIDDIFF, WallIddiff);
        bdl.putString(ISFROM, isFrom);
        bdl.putBoolean(RELOAD, reload);
        f.setArguments(bdl);
        return f;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getActivity();

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(null)
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("null")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("NAN")) {

            new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
            new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
            Intent intent = new Intent(mContext, RegisterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            new UserSharedPrefrence(mContext).clearPrefrence();
            startActivity(intent);
            getActivity().finish();

        }

        v = inflater.inflate(R.layout.facebook_wall_fragment, container, false);
        this.wallId = getArguments().getString(WALLIDDIFF);

        this.from = getArguments().getString(ISFROM);
        this.reload = getArguments().getBoolean(RELOAD, false);
        new UserSharedPrefrence(mContext).getLoginModel().setCurrentWallId(wallId);

        Global.wallPostModels = new ArrayList<WallPostModel>();

        img_testing = (ImageView) v.findViewById(R.id.img_testing);

        lst_fb_wall = (PullAndLoadListView) v.findViewById(R.id.lst_fb_wall);
        tv_post = (TextView) v.findViewById(R.id.tv_post);
        showHideLayoutstatus = (LinearLayout) v.findViewById(R.id.showhideLayout);
        showHidelayoutphotovideo = (LinearLayout) v.findViewById(R.id.showhideLayoutphotovideo);
        addpostlayout = (LinearLayout) v.findViewById(R.id.addpostlayout);
        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);
        txt_count = (TextView) v.findViewById(R.id.txt_count);

        if (!new UserSharedPrefrence(mContext).getLOGIN_NOTIFICATIONCOUNT().equalsIgnoreCase("0")) {

            txt_count.setText(new UserSharedPrefrence(mContext).getLOGIN_FRIENDCOUNT());
            txt_count.setVisibility(View.VISIBLE);

        }

        ll_Photo = (LinearLayout) v.findViewById(R.id.ll_Photo);
        ll_Video = (LinearLayout) v.findViewById(R.id.ll_Video);
        ll_Friends = (LinearLayout) v.findViewById(R.id.ll_Friends);

        lvbounce = (ListView) v.findViewById(R.id.lvbounce);
        showHideLayoutstatus.setVisibility(View.GONE);
        showHidelayoutphotovideo.setVisibility(View.GONE);
        addpostlayout.setVisibility(View.GONE);
        tv_post.setOnClickListener(this);
        ll_Photo.setOnClickListener(this);
        ll_Video.setOnClickListener(this);
        wallList = new ArrayList<WallModel>();
        iv_profile_pic = (CircleImageView) v.findViewById(R.id.iv_profile_pic);


        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(iv_profile_pic);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        headerClick = true;
        isShowMenu = false;
        lvbounce.setVisibility(View.GONE);

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                if (from.equalsIgnoreCase(ServiceResource.Wall)) {

                    try {
                        HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Wall) + " (" + Utility.GetFirstName(mContext) + ")");
                        new UserSharedPrefrence(mContext).setCURRENTWALLID(new UserSharedPrefrence(mContext).getLOGIN_WALLID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Standard) + " (" + Utility.GetFirstName(mContext) + ")");
                }

                if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {

                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Institute) + " (" + Utility.GetFirstName(mContext) + ")");
                    new UserSharedPrefrence(mContext).setCURRENTWALLID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTIONWALLID());

                }

                if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {

                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Division) + " (" + Utility.GetFirstName(mContext) + ")");

                }

                if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {

                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Subjects) + " (" + Utility.GetFirstName(mContext) + ")");

                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }


        lst_fb_wall.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            public void onRefresh() {

                Global.wallPostModels.clear();
                new WallListAsync(true).execute(new Integer[]{1, 0});
                refreshtype = 1;

            }

        });

        lst_fb_wall.setOnLoadMoreListener(new PullAndLoadListView.OnLoadMoreListener() {

            @Override
            public void onLoadMore() {

                if (isMoreData) {

                    count += 10;
                    new WallListAsync(false).execute(new Integer[]{count, 0});
                    refreshtype = 0;

                } else {

                    count = 0;

                }

            }

        });

        lst_fb_wall.setSmoothScrollbarEnabled(true);

//        if (from.equalsIgnoreCase(ServiceResource.Wall)) {
//
//            dynamicSettingtrueall();
//
//        } else if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
//
//            dynamicSettingtrueall();
//
//        } else {
//
//            String examresult = "";
//
//            if (examresult != null || examresult.equalsIgnoreCase("")) {
//
//                examresult = Utility.readFromFile(wallId + count, mContext);
//
//            }
//
//            Log.d("getelsepart", examresult);
//            onlySetting(examresult);
//
//        }

        return v;

    }


    @Override
    public void onResume() {

        super.onResume();

        if (Utility.isNetworkAvailable(getActivity())) {

            // to reduce extra loading to get wall post
            DynamicMenuList();
            parseDaynamicList(Utility.readFromFile(ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME, mContext));

            if (myWallAdapter != null) {
                myWallAdapter.notifyDataSetChanged();
                Global.wallPostModels.clear();
                new WallListAsync(true).execute(1);

            } else {
                new WallListAsync(true).execute(1);

            }

        } else {

            parseDaynamicList(Utility.readFromFile(ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME, mContext));
            Utility.showAlertDialog(getActivity(), getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_post:

                /*commented By Krishna : 28-06-2019 Check Permission for Post Status on wall*/

                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                        Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                        intent.putExtra("isFrom", from);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                        ((Activity) mContext).finish();

                    } else {

                        Utility.toast(mContext, "You Dont have Permission");

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {

                        ll_Photo.setVisibility(View.INVISIBLE);

                    }

                } else {

                    if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") &&
                            Global.DynamicWallSetting.getIsAllowPeopleToPostStatus() &&
                            Global.DynamicWallSetting.getIsAllowPostStatus()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") &&
                                    Global.DynamicWallSetting.getIsAllowPeoplePostComment() &&
                                    Global.DynamicWallSetting.getIsAllowPostPhoto()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") &&
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() &&
                                    Global.DynamicWallSetting.getIsAllowPostVideo()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) &&
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() &&
                                    Global.DynamicWallSetting.getIsAllowPostFile()) {

                        Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                        intent.putExtra("isFrom", from);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        ((Activity) mContext).finish();
                        getActivity().overridePendingTransition(0, 0);

                    } else {

                        Utility.toast(mContext, "You Dont have Permission");

                    }

                }

                /*END*/


                break;

            case R.id.ll_Photo:

                Intent intent = new Intent(mContext, PhotosListActivity.class);
                intent.putExtra("isFrom", from);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            case R.id.ll_Video:

                intent = new Intent(mContext, VideoListActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            case R.id.ll_Friends:

                intent = new Intent(mContext, FriendListActivity.class);
                intent.putExtra("friend", true);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            default:
                break;

        }

    }

    @Override
    public void clickOnPhoto() {
        Intent intent = new Intent(mContext, PhotosListActivity.class);
        intent.putExtra("isFrom", from);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnVideo() {
        Intent intent = new Intent(mContext, VideoListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnFriends() {
        Intent intent = new Intent(mContext, FriendListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnPost() {

        /*commented By Krishna : 28-06-2019 Check Permission for Post Status on wall*/

        try {
            Utility.CheckPermission(getActivity(), from, ll_Photo, Global.DynamicWallSetting);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeletePost() {

    }

    @Override
    public void onEditPost() {

    }

    /**
     * getWalllist from webservice
     *
     * @author admin
     */
    public class WallListAsync extends AsyncTask<Integer, Void, Void> implements ICancleAsynkTask {

        String result = "";
        boolean isShow = false;

        public WallListAsync(boolean isShow) {

            this.isShow = isShow;

        }

        protected void onPreExecute() {

            super.onPreExecute();
            // do stuff before posting data
            if (count == 1) {

                if (isShow) {

                    if (from.equalsIgnoreCase(ServiceResource.Wall)) {

                        LoaderProgress = new LoaderProgress(mContext);
                        LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
                        LoaderProgress.setCancelable(false);
                        LoaderProgress.show();

                    } else {

                        LoaderProgress = new LoaderProgress(mContext, this);
                        LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
                        LoaderProgress.setCancelable(true);
                        LoaderProgress.show();

                    }

                }

            }

        }

        @Override
        protected Void doInBackground(Integer... params) {

            count = params[0];

            if (params.length > 1) {

            }

            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();
            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.WALLID, wallId);
            map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CTOKEN, PreferenceData.getCTOKEN());
            HashMap<String, String> map2 = new HashMap<String, String>();
            JSONArray jsonObj;

            if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {

                isMoreData = true;

                map2.put(ServiceResource.WALL_ROWNOSMALL, String.valueOf(count));
                map.put(1, map2);
                map.put(2, map1);

                if (Utility.isNetworkAvailable(mContext)) {

                    result = webcall.getJSONFromSOAPWS(ServiceResource.GETMYWALLDATA_METHODNAME, map, ServiceResource.WALL_URL);

                    // commented by Krishna  :  12-1-2019 Add JsonData into Wallpost Model and Set Model To arraylist

                    try {
                        jsonObj = new JSONArray(result);
                        for (int i = 0; i < jsonObj.length(); i++) {
                            JSONObject Obj = jsonObj.getJSONObject(i);

                            // method to store wallpost data from jsonarray
                            GetWallDataResponse(Obj);
                            /*END*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*END*/

                    if (count == 1) {

                        Utility.writeToFile(result, wallId + count, mContext);

                    }

                } else {

                    isOffline = true;

                    if (count == 1) {

                        result = Utility.readFromFile(wallId + count, mContext);

                    }

                }

            } else if (from.equalsIgnoreCase(ServiceResource.Wall)) {

                isMoreData = true;

                map2.put(ServiceResource.WALL_ROWNOSMALL, String.valueOf(count));
                map.put(1, map2);
                map.put(2, map1);

                if (Utility.isNetworkAvailable(mContext)) {

                    result = webcall.getJSONFromSOAPWS(ServiceResource.WALL_METHODNAME, map, ServiceResource.WALL_URL);

                    // commented by Krishna  :  12-1-2019 Add JsonData into Wallpost Model and Set Model To arraylist

                    try {

                        Log.d("getResult1234", result);
                        jsonObj = new JSONArray(result);

                        for (int i = 0; i < jsonObj.length(); i++) {

                            JSONObject Obj = jsonObj.getJSONObject(i);

                            // method to store wallpost data from jsonarray

                            GetWallDataResponse(Obj);

                            /*END*/

                        }

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                    /*END*/

//                    if (count == 1) {
//                        Utility.writeToFile(result, wallId + count, mContext);
//                    }


                }

//                else {
//
//                    isOffline = true;
//
//                    if (count == 1) {
//
//                        result = Utility.readFromFile(wallId + count, mContext);
//
//                    }
//
//                }

            } else {

                map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
                map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
                map2.put(ServiceResource.WALL_ROWNOSMALL, String.valueOf(count));
                map.put(1, map2);
                map.put(2, map1);

                Log.d("getelsewalLRequest", map.toString());
                result = webcall.getJSONFromSOAPWS(ServiceResource.GETDYNAMICWALLDATA_METHODNAME, map, ServiceResource.WALL_URL);

                //  commented by Krishna  :  12-1-2019 Add InstituteData into Wallpost Model and Set Model To arraylist for Institute wall

                try {

                    JSONArray mainJsonArray;
                    JSONObject jobj = new JSONObject(result);
                    mainJsonArray = jobj.getJSONArray(ServiceResource.WALL_POSTDATA);

                    for (int i = 0; i < mainJsonArray.length(); i++) {

                        JSONObject Obj = mainJsonArray.getJSONObject(i);

                        // method to store wall post data from jsonarray

                        GetWallDataResponse(Obj);

                        /*END*/

                    }

                    onlySetting(result);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                /*END*/

            }

            return null;

        }

        private void GetWallDataResponse(JSONObject Obj) {

            try {

                wallPostModel = new WallPostModel();
                wallPostModel.setRowNo(Obj.getString(ServiceResource.WALL_ROW_ID));
                wallPostModel.setTempDate(Obj.getString(ServiceResource.WALL_TEMPDATE));
                wallPostModel.setPostCommentID(Obj.getString(ServiceResource.WALL_POSTCOMMENTID));
                wallPostModel.setWallID(Obj.getString(ServiceResource.WALLID));
                wallPostModel.setPostCommentTypesTerm(Obj.getString(ServiceResource.WALL_POSTCOMMENTTYPESTERM));
                wallPostModel.setPostCommentNote(Obj.getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                wallPostModel.setTotalLikes(Obj.getString(ServiceResource.WALL_TOTALLIKES));
                wallPostModel.setTotalComments(Obj.getString(ServiceResource.WALL_TOTALCOMMENTS));
                wallPostModel.setTotalDislike(Obj.getString(ServiceResource.WALL_TOTALDISLIKE));
                wallPostModel.setMemberID(Obj.getString(ServiceResource.WALL_CMT_MEMBERID));
                wallPostModel.setDateOfPost(Obj.getString(ServiceResource.WALL_DATEOFPOST));
                wallPostModel.setAssociationID(Obj.getString(ServiceResource.WALL_ASSOCIATIONID));
                wallPostModel.setAssociationType(Obj.getString(ServiceResource.WALL_ASSOCIATIONTYPE));
                wallPostModel.setFullName(Obj.getString(ServiceResource.WALL_FULLNAME));
                wallPostModel.setIsDisLike(Obj.getString(ServiceResource.WALL_ISDISLIKE));
                wallPostModel.setIsLike(Obj.getString(ServiceResource.WALL_ISLIKE));
                wallPostModel.setProfilePicture(Obj.getString(ServiceResource.WALL_PROFILEPICTURE));
                wallPostModel.setPhoto(Obj.getString(ServiceResource.WALL_PHOTO));
                wallPostModel.setPhotoCount(Obj.getString(ServiceResource.WALL_PHOTOCOUNT));
                wallPostModel.setPhotoUrls(Obj.getString(ServiceResource.WALL_PHOTOURLS));
                wallPostModel.setPostCount(Obj.getString(ServiceResource.WALL_POSTCOUNT));
                wallPostModel.setPostUrls(Obj.getString(ServiceResource.WALL_POSTURLS));
                wallPostModel.setFileType(Obj.getString(ServiceResource.WALL_FILETYPE));
                wallPostModel.setFileMimeType(Obj.getString(ServiceResource.WALL_FILEMIMETYPE));
                wallPostModel.setWallTypeTerm(Obj.getString(ServiceResource.WALL_TYPE_TERM));
                wallPostModel.setPostedOn(Obj.getString(ServiceResource.WALL_POSTEDON));
                wallPostModel.setIsAllowPeopleToShareYourPost(Obj.getString(ServiceResource.ISALLOWPEOPLETOSHAREYOURPOST));
                wallPostModel.setIsAllowPeopleToLikeOrDislikeOnYourPost(Obj.getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKEONYOURPOST));
                wallPostModel.setIsAllowPeopleToPostMessageOnYourWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTMESSAGETOWALL));
                wallPostModel.setIsAllowPeopleToLikeAndDislikeCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKECOMMENTWALL));
                wallPostModel.setIsAllowPeopleToShareCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOSHARECOMMENTWALL));
                wallPostModel.setIsAllowPeoplePostCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTWALL));

                if (from.equalsIgnoreCase(ServiceResource.Wall) || from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {

                    wallPostModel.setIsAllowLikeDislike(Obj.getString(ServiceResource.ISALLOWLIKEDISLIKE));
                    wallPostModel.setIsAllowPostComment(Obj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                    wallPostModel.setPostComment(Obj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                    wallPostModel.setIsAllowSharePost(Obj.getString(ServiceResource.ISALLOWSHAREPOST));

                }

                wallPostModel.setIsAllowPeopleToPostCommentOnPostWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTONPOSTWALL));

                Global.wallPostModels.add(wallPostModel);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPostExecute(Void res) {
            super.onPostExecute(res);
            if (count == 1) {
                if (isShow) {
                    if (LoaderProgress != null)
                        LoaderProgress.dismiss();
                } else {
                    if (LoaderProgress != null)
                        LoaderProgress.dismiss();
                }
            } else {
                if (isShow) {
                    if (LoaderProgress != null)
                        LoaderProgress.dismiss();
                } else {
                    if (LoaderProgress != null)
                        LoaderProgress.dismiss();
                }
            }


            setAdapterData();

            // Commented By Krishna : 15-05-2020 -  to reduce loading of wall data
//            if (isOffline) {
//
//                if (count == 1) {
//
//                    setAdapterData();
//
//                } else {
//
//                    setAdapterData();
//                }
//
//            } else {
//
//                setAdapterData();
//
//            }
        }

        @Override
        protected void onCancelled() {
            lst_fb_wall.onLoadMoreComplete();
            if (lst_fb_wall != null) {
                lst_fb_wall.onRefreshComplete();
            }
        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }
    }


    /**
     * share public webservice call reuslt
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shrePublic(WallPostModel, int)
     */
    @Override
    public void shrePublic(WallPostModel wallPostModel, int position) {

        ShareType = 1;
        new SharePost().execute(wallPostModel);

    }

    /**
     * share onlyMe webservice call examresult
     */

    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreOnlyMe(WallPostModel wallPostModel, int position) {

        ShareType = 2;
        new SharePost().execute(wallPostModel);

    }

    /**
     * share public webservice call examresult
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreFriend(WallPostModel wallPostModel, int position) {

        ShareType = 3;
        new SharePost().execute(wallPostModel);
    }

    /**
     * share special friend webservice call examresult
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreSpecialFriend(WallPostModel wallPostModel, int position) {

        ShareType = 4;
        new ShareSpecialFriend().execute(wallPostModel);

    }

    /**
     * webservice call for special friends
     */
    public class ShareSpecialFriend extends
            AsyncTask<WallPostModel, Void, Void> {

        //		ProgressDialog progressDialog = null;
        private String result;
        private boolean success;
        WallPostModel wallPostModel;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext);
            LoaderProgress.setMessage("Fetching Friends...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
        }

        @Override
        protected Void doInBackground(WallPostModel... params) {

            // TODO Auto-generated method stub
            wallPostModel = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map1.put(ServiceResource.BEATCH_ID, null);

            map.put(2, map1);

            result = webcall.getJSONFromSOAPWS(
                    ServiceResource.FRIENDS_METHODNAME, map,
                    ServiceResource.FRIENDS_URL);

            JSONArray jsonObj;
            try {
                Global.FriendsList = new ArrayList<PersonModel>();

                jsonObj = new JSONArray(result);
                // JSONArray detailArrray= jsonObj.getJSONArray("");

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    PersonModel model = new PersonModel();
                    model.setPersonName(innerObj
                            .getString(ServiceResource.FRIENDS_FULLNAME));
                    model.setProfileImg(innerObj
                            .getString(ServiceResource.FRIENDS_PROFILEPIC));
                    if (model.getProfileImg() != null
                            && !model.getProfileImg().equals("")) {
                        model.setProfileImg(ServiceResource.BASE_IMG_URL
                                + model.getProfileImg().substring(1,
                                model.getProfileImg().length()));
                    }
                    model.setFriendId(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDID));
                    model.setFriendListID(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDLIST));
                    model.setWallID(innerObj
                            .getString(ServiceResource.FRIENDS_WALLID));
                    Global.FriendsList.add(model);

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }

            dialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_fetch_friends, true);
//            dialog = new Dialog(mContext);
//
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            dialog.getWindow().setBackgroundDrawableResource(
//                    android.R.color.transparent);
//            dialog.setContentView(R.layout.dialog_fetch_friends);
//            dialog.setCancelable(true);
//            dialog.show();

            img_save = (ImageView) dialog.findViewById(R.id.img_save);
            img_back = (ImageView) dialog.findViewById(R.id.img_back);

            img_back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }

            });

            img_save.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    new SharePost().execute(wallPostModel);

                }

            });

            LinearLayout searchlayout = (LinearLayout) dialog
                    .findViewById(R.id.searchlayout);
            searchlayout.setVisibility(View.VISIBLE);

            edtTeacherName = (EditText) dialog
                    .findViewById(R.id.edtsearchStudent);

            lst_fetch_friend = (ListView) dialog
                    .findViewById(R.id.lst_fetch_friend);
            TextView txt_nodatafound = (TextView) dialog
                    .findViewById(R.id.txt_nodatafound);

            if (Global.FriendsList != null && Global.FriendsList.size() > 0) {
                fetchfriendAdapter = new FetchfriendAdapter(mContext,
                        Global.FriendsList, FacebookWallFragment.this);
                lst_fetch_friend.setAdapter(fetchfriendAdapter);
            } else {
                searchlayout.setVisibility(View.GONE);
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoFriendsAvailable));
            }

            iv_select_all = (ImageView) dialog.findViewById(R.id.iv_select_all);
            iv_select_all.setTag("0");
            iv_select_all.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (iv_select_all.getTag().equals("0")) {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(true);
                                iv_select_all.setTag("1");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(R.color.blue));
                            fetchfriendAdapter.notifyDataSetChanged();
                        }

                    } else {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(false);
                                iv_select_all.setTag("0");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(
                                            R.color.grey_fb_color));

                            fetchfriendAdapter.notifyDataSetChanged();
                        }
                    }

                }
            });

            edtTeacherName.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {

                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {


                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

                    if (fetchfriendAdapter != null) {

                        String text = edtTeacherName.getText().toString()
                                .toLowerCase(Locale.getDefault());
                        fetchfriendAdapter.filter(text);

                    }

                }

            });

        }

    }

    /**
     * webservice call for share post
     *
     * @author admin
     */
    public class SharePost extends AsyncTask<WallPostModel, Void, Void> implements ICancleAsynkTask {

        //		ProgressDialog progressDialog = null;
        private String result;
        private boolean success;


        @Override
        protected void onPreExecute() {
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage("Sharing Post...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(WallPostModel... params) {
            try {

                WebserviceCall webcall = new WebserviceCall();
                HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();
                HashMap<String, String> map1 = new HashMap<String, String>();
                map1.put(ServiceResource.WALLID, wallId);
                map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
                map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
                map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
                map1.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
                map1.put(ServiceResource.SENDTOMEMBERID, params[0].getSendToMemberID());
                map1.put(ServiceResource.SHRE_POST_SHAREPOSTID, params[0].getPostCommentID());
                if (ShareType == 1) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Public");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 2) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Only Me");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 3) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Friend");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 4) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Special Friend");
                    String selectFriend = "";
                    for (int i = 0; i < Global.FriendsList.size(); i++) {
                        if (Global.FriendsList.get(i).isSelected()) {
                            selectFriend += Global.FriendsList.get(i).getFriendId() + ",";
                            map1.put(ServiceResource.SHRE_POST_TAGID, selectFriend);
                        }
                    }
                    selectFriend.substring(0, selectFriend.length() - 1);

                }

                map.put(2, map1);

                result = webcall.getJSONFromSOAPWS(ServiceResource.SHRE_POST_METHODNAME, map, ServiceResource.SHRE_POST_URL);
                success = true;
            } catch (Exception e) {
                success = false;
                e.printStackTrace();
            }
            if (ServiceResource.isShowLog) {
                Log.e("dsds", result);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }
            Utility.sendPushNotification(mContext);

            if (success) {
                Utility.toast(mContext, getResources().getString(R.string.ShareSuccessfull));
//                Toast.makeText(mContext, getResources().getString(R.string.ShareSuccessfull), Toast.LENGTH_SHORT).show();
            } else {
                Utility.toast(mContext, getResources().getString(R.string.Error));
//                Toast.makeText(mContext, getResources().getString(R.string.Error), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }

    }

    @Override
    public void selectedFriends(int selectedFriendCount) {
        if (Global.FriendsList.size() == selectedFriendCount) {
            iv_select_all.setColorFilter(mContext.getResources().getColor(R.color.blue));
            iv_select_all.setTag("1");
        } else {
            iv_select_all.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
            iv_select_all.setTag("0");
        }
    }


    /**
     * @param tv
     * @param isVisible
     */

    public static void menuChangeWall(View tv, boolean isVisible) {

        Animation animBounce;

        if (isVisible) {

            list.clear();
            WallListVo vo;
            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Wall));
            vo.setIco(R.drawable.wall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.mywall));
            vo.setIco(R.drawable.mywall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Institutewall));
            vo.setIco(R.drawable.dash_institute);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Standardwall));
            vo.setIco(R.drawable.dash_grade);
            vo.setDynamicWallList(standardlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Divisionwall));
            vo.setIco(R.drawable.dash_division);
            vo.setDynamicWallList(divisionlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Subjectswall));
            vo.setIco(R.drawable.dash_subject);
            vo.setDynamicWallList(subjectlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.groupwall));
            vo.setIco(R.drawable.schoolgroups);
            vo.setDynamicWallList(grouplist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.projectwall));
            vo.setIco(R.drawable.project);
            vo.setDynamicWallList(projectlist);
            list.add(vo);

            WallListAdapter adapter = new WallListAdapter(mContext, list, from, true);
            lvbounce.setAdapter(adapter);
            lvbounce.setVisibility(View.VISIBLE);
            animBounce = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
            lvbounce.startAnimation(animBounce);

        } else {

            lvbounce.endViewTransition(tv);
            lvbounce.clearAnimation();
            lvbounce.setVisibility(View.GONE);

        }

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /**
     * dynamic menu webservice
     */

    public void DynamicMenuList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));

        }

        arrayList.add(new PropertyVo(ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        Log.d("getDynamicMenuDatareq", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME, ServiceResource.LOGIN_URL);

    }

    /**
     * get response for all webservice
     */

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parseDaynamicList(result);

        }
    }

    /**
     * show hide menu click on header
     */

    public static void showMenuHeader() {

        if (headerClick) {

            if (!isShowMenu) {

                isShowMenu = true;
                menuChangeWall(v, true);

            } else {

                isShowMenu = false;
                menuChangeWall(v, false);
                lvbounce.setVisibility(View.GONE);

            }
        }
    }

    public void setAdapterData() {

        if (myWallAdapter != null) {

            myWallAdapter.notifyDataSetChanged();

        } else {

            try {

                if (Global.wallPostModels.size() > 0) {

                    Collections.sort(Global.wallPostModels, new SortedDate());
                    Collections.reverse(Global.wallPostModels);
                    addpostlayout.setVisibility(View.GONE);
                    txt_nodatafound.setVisibility(View.GONE);

                    if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

                        Date currentTime = Calendar.getInstance().getTime();
                        Log.d("getStartTime2345", currentTime.toString());
                        myWallAdapter = new MyWallAdapter(getActivity(), Global.wallPostModels, FacebookWallFragment.this, FacebookWallFragment.this, false, from);

                    } else {

                        myWallAdapter = new MyWallAdapter(getActivity(), Global.wallPostModels, FacebookWallFragment.this, FacebookWallFragment.this, true, from);

                    }

                    if (lst_fb_wall != null) {

                        txt_nodatafound.setVisibility(View.GONE);
                        lst_fb_wall.setAdapter(myWallAdapter);

                    }

                } else {

                    showHideLayoutstatus.setVisibility(View.GONE);
                    showHidelayoutphotovideo.setVisibility(View.GONE);

                    txt_nodatafound.setVisibility(View.VISIBLE);

                    if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

                        if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {
                            addpostlayout.setVisibility(View.VISIBLE);
                            tv_post.setVisibility(View.VISIBLE);

                        } else {

                            addpostlayout.setVisibility(View.INVISIBLE);

                        }

                        if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {

                            ll_Photo.setVisibility(View.INVISIBLE);

                        }

                    } else {

                        if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                                new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                            if ((Global.DynamicWallSetting != null)) {

                                if (Global.DynamicWallSetting.getIsAllowPeopleToUploadAlbum() ||
                                        Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() ||
                                        Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() ||
                                        Global.DynamicWallSetting.getIsAllowPeopleToPostStatus()) {

                                    addpostlayout.setVisibility(View.VISIBLE);

                                } else {

                                    addpostlayout.setVisibility(View.VISIBLE);
                                    tv_post.setVisibility(View.INVISIBLE);

                                }

                            }

                        } else {

                            addpostlayout.setVisibility(View.VISIBLE);
                            tv_post.setVisibility(View.INVISIBLE);

                        }

                        if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                            ll_Photo.setVisibility(View.INVISIBLE);
                        }

                    }

                    try {
                        txt_nodatafound.setText(mContext.getResources().getString(R.string.NoWallDataAvailable));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        if (refreshtype == 0) {
            lst_fb_wall.onLoadMoreComplete();
        } else if (refreshtype == 1) {
            lst_fb_wall.onRefreshComplete();
        }
        refreshtype = -1;

        if (refreshtype == 0) {
            lst_fb_wall.onLoadMoreComplete();
        } else if (refreshtype == 1) {
            lst_fb_wall.onRefreshComplete();
        }

        refreshtype = -1;
    }

    public class SortedDate implements Comparator<WallPostModel> {
        @Override
        public int compare(WallPostModel o1, WallPostModel o2) {
            return o1.getTempDate().compareTo(o2.getTempDate());
        }
    }

    public void parseDaynamicList(String result) {

        ArrayList<DynamicWallListModel> templist = new ArrayList<DynamicWallListModel>();

        try {

            if (ServiceResource.isShowLog) {

            }

            JSONObject jsonobj = new JSONObject(result);
            JSONArray table = jsonobj.getJSONArray(ServiceResource.GETUSERDYNAMICMENUDATA_TABLE);

            for (int i = 0; i < table.length(); i++) {

                JSONObject obj = table.getJSONObject(i);
                DynamicWallListModel model = new DynamicWallListModel();
                model.setWallID(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLID));
                model.setWallName(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLNAME));
                model.setWallImage(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLIMAGE));
                model.setAssociationType(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_ASSOCIATIONTYPE));
                templist.add(model);

            }

            subjectlist.clear();
            standardlist.clear();
            divisionlist.clear();
            grouplist.clear();
            projectlist.clear();


        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < templist.size(); i++) {
            if (templist.get(i).getAssociationType().equalsIgnoreCase("Subject")) {
                subjectlist.add(templist.get(i));
            } else if (templist.get(i).getAssociationType().equalsIgnoreCase("Grade")) {
                standardlist.add(templist.get(i));
            } else if (templist.get(i).getAssociationType().equalsIgnoreCase("Division")) {
                divisionlist.add(templist.get(i));
            } else if (templist.get(i).getAssociationType().equalsIgnoreCase("Group")) {
                grouplist.add(templist.get(i));
            } else if (templist.get(i).getAssociationType().equalsIgnoreCase("project")) {
                projectlist.add(templist.get(i));
            }
        }
    }

    @Override
    public void onStop() {

        super.onStop();

        if (HomeWorkFragmentActivity.img_wallarrow != null) {

            HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.GONE);

        }

    }

    @Override
    public void onStart() {

        super.onStart();

        if (HomeWorkFragmentActivity.img_wallarrow != null) {

            HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.GONE);

            HomeWorkFragmentActivity.img_wallarrow.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getActivity(), WallMemberActivity.class);
                    mContext.startActivity(i);

                }

            });

        }

    }

    public void onlySetting(String result) {

        try {

            if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

            } else {

                JSONObject jobj = new JSONObject(result);
                JSONArray jsonArray = jobj.getJSONArray(ServiceResource.WALL_ROLEDATA);
                JSONArray jsonArrayAdminsetting = jobj.optJSONArray(ServiceResource.WALLROLEDATA);

                Global.DynamicWallSetting = new DynamicWallSettingModel();

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject innerObj = jsonArray.getJSONObject(i);
                    Global.DynamicWallSetting.setIsAllowPeopleToLikeThisWall(innerObj.getString(ServiceResource.ISALLOWPEOPLETOLIKETHISWALL));
                    Global.DynamicWallSetting.setIsAllowPeoplePostComment(innerObj.getString(ServiceResource.ISALLOWPEOPLEPOSTCOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToShareComment(innerObj.getString(ServiceResource.ISALLOWPEOPLETOSHARECOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToLikeAndDislikeComment(innerObj.getString(ServiceResource.ISALLOWPEOPLETOLIKEANDDISLIKECOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostStatus(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAllowPeopleToCreatePoll(innerObj.getString(ServiceResource.ISALLOWPEOPLETOCREATEPOLL));
                    Global.DynamicWallSetting.setIsAllowPeopleToParticipateInPoll(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPARTICIPATEINPOLL));
                    Global.DynamicWallSetting.setIsAllowPeopleToInviteOtherPeople(innerObj.getString(ServiceResource.ISALLOWPEOPLETOINVITEOTHERPEOPLE));
                    Global.DynamicWallSetting.setIsAllowPeopleToTagOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOTAGONPOST));
                    Global.DynamicWallSetting.setIsAllowPeopleToUploadAlbum(innerObj.getString(ServiceResource.ISALLOWPEOPLETOUPLOADALBUM));
                    Global.DynamicWallSetting.setIsAllowPeopleToPutGeoLocationOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPUTGEOLOCATIONONPOST));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostDocument(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTDOCUMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostVideos(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTVIDEOS));
                    Global.DynamicWallSetting.setWallName(innerObj.getString(ServiceResource.WALLNAME));
                    Global.DynamicWallSetting.setWallImage(innerObj.getString(ServiceResource.WALLIMAGE));
                    Global.DynamicWallSetting.setWallID(innerObj.getString(ServiceResource.WALLID));
                    Global.DynamicWallSetting.setIsAutoApprovePost(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOST));
                    Global.DynamicWallSetting.setIsAutoApprovePostStatus(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAutoApproveAlbume(innerObj.getString(ServiceResource.ISAUTOAPPROVEALBUME));
                    Global.DynamicWallSetting.setIsAutoApproveVideos(innerObj.getString(ServiceResource.ISAUTOAPPROVEVIDEOS));
                    Global.DynamicWallSetting.setIsAutoApproveDocument(innerObj.getString(ServiceResource.ISAUTOAPPROVEDOCUMENT));
                    Global.DynamicWallSetting.setIsAutoApprovePoll(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOLL));
                    Global.DynamicWallSetting.setIsAdmin(innerObj.getString(ServiceResource.ISADMIN));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostCommentOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTONPOST));

                }

                for (int j = 0; j < jsonArrayAdminsetting.length(); j++) {

                    JSONObject innerObj = jsonArrayAdminsetting.getJSONObject(j);
                    Global.DynamicWallSetting.setIsAllowLikeDislike(innerObj.getString(ServiceResource.ISALLOWLIKEDISLIKE));
                    Global.DynamicWallSetting.setIsAllowPostAlbum(innerObj.getString(ServiceResource.ISALLOWPOSTALBUM));
                    Global.DynamicWallSetting.setIsAllowPostComment(innerObj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                    Global.DynamicWallSetting.setIsAllowPostFile(innerObj.getString(ServiceResource.ISALLOWPOSTFILE));
                    Global.DynamicWallSetting.setIsAllowPostPhoto(innerObj.getString(ServiceResource.ISALLOWPOSTPHOTO));
                    Global.DynamicWallSetting.setIsAllowPostStatus(innerObj.getString(ServiceResource.ISALLOWPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAllowPostVideo(innerObj.getString(ServiceResource.ISALLOWPOSTVIDEO));
                    Global.DynamicWallSetting.setIsAllowSharePost(innerObj.getString(ServiceResource.ISALLOWSHAREPOST));

                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }


    }

}
