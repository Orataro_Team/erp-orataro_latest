package com.edusunsoft.erp.orataro.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesPaymentCharges implements Parcelable {

    @SerializedName("IntegrationID")
    @Expose
    private String integrationID;
    @SerializedName("Charges")
    @Expose
    private String charges;
    @SerializedName("IsChargeFlat")
    @Expose
    private String isChargeFlat;
    @SerializedName("DefaultMDD_Term")
    @Expose
    private String defaultMDDTerm;
    @SerializedName("TotalChargesPaid")
    @Expose
    private String totalChargesPaid;
    @SerializedName("TotalPaymentReceived")
    @Expose
    private String totalPaymentReceived;
    @SerializedName("Term")
    @Expose
    private String term;
    @SerializedName("Term1")
    @Expose
    private String term1;
    @SerializedName("MerchantType_Term")
    @Expose
    private String merchantTypeTerm;
    public final static Parcelable.Creator<FeesPaymentCharges> CREATOR = new Creator<FeesPaymentCharges>() {
        public FeesPaymentCharges createFromParcel(Parcel in) {
            FeesPaymentCharges instance = new FeesPaymentCharges();
            instance.integrationID = ((String) in.readValue((String.class.getClassLoader())));
            instance.charges = ((String) in.readValue((String.class.getClassLoader())));
            instance.isChargeFlat = ((String) in.readValue((String.class.getClassLoader())));
            instance.defaultMDDTerm = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalChargesPaid = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalPaymentReceived = ((String) in.readValue((String.class.getClassLoader())));
            instance.term = ((String) in.readValue((String.class.getClassLoader())));
            instance.term1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.merchantTypeTerm = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public FeesPaymentCharges[] newArray(int size) {
            return (new FeesPaymentCharges[size]);
        }

    };

    public String getIntegrationID() {
        return integrationID;
    }

    public void setIntegrationID(String integrationID) {
        this.integrationID = integrationID;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getIsChargeFlat() {
        return isChargeFlat;
    }

    public void setIsChargeFlat(String isChargeFlat) {
        this.isChargeFlat = isChargeFlat;
    }

    public String getDefaultMDDTerm() {
        return defaultMDDTerm;
    }

    public void setDefaultMDDTerm(String defaultMDDTerm) {
        this.defaultMDDTerm = defaultMDDTerm;
    }

    public Object getTotalChargesPaid() {
        return totalChargesPaid;
    }

    public void setTotalChargesPaid(String totalChargesPaid) {
        this.totalChargesPaid = totalChargesPaid;
    }

    public Object getTotalPaymentReceived() {
        return totalPaymentReceived;
    }

    public void setTotalPaymentReceived(String totalPaymentReceived) {
        this.totalPaymentReceived = totalPaymentReceived;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTerm1() {
        return term1;
    }

    public void setTerm1(String term1) {
        this.term1 = term1;
    }

    public String getMerchantTypeTerm() {
        return merchantTypeTerm;
    }

    public void setMerchantTypeTerm(String merchantTypeTerm) {
        this.merchantTypeTerm = merchantTypeTerm;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(integrationID);
        dest.writeValue(charges);
        dest.writeValue(isChargeFlat);
        dest.writeValue(defaultMDDTerm);
        dest.writeValue(totalChargesPaid);
        dest.writeValue(totalPaymentReceived);
        dest.writeValue(term);
        dest.writeValue(term1);
        dest.writeValue(merchantTypeTerm);
    }

    public int describeContents() {
        return 0;
    }

}