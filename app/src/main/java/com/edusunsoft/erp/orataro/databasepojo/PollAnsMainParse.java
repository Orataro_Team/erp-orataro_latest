package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollAnsMainParse implements Parcelable {

    @SerializedName("PollID")
    @Expose
    private String pollID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Details")
    @Expose
    private String details;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("IsMultiChoice")
    @Expose
    private String isMultiChoice;
    @SerializedName("SeqNo")
    @Expose
    private Integer seqNo;
    public final static Parcelable.Creator<PollAnsMainParse> CREATOR = new Creator<PollAnsMainParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PollAnsMainParse createFromParcel(Parcel in) {
            PollAnsMainParse instance = new PollAnsMainParse();
            instance.pollID = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.details = ((String) in.readValue((String.class.getClassLoader())));
            instance.startDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.endDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.fullName = ((String) in.readValue((String.class.getClassLoader())));
            instance.isMultiChoice = ((String) in.readValue((String.class.getClassLoader())));
            instance.seqNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
            return instance;
        }

        public PollAnsMainParse[] newArray(int size) {
            return (new PollAnsMainParse[size]);
        }

    };

    public String getPollID() {
        return pollID;
    }

    public void setPollID(String pollID) {
        this.pollID = pollID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIsMultiChoice() {
        return isMultiChoice;
    }

    public void setIsMultiChoice(String isMultiChoice) {
        this.isMultiChoice = isMultiChoice;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pollID);
        dest.writeValue(title);
        dest.writeValue(details);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
        dest.writeValue(fullName);
        dest.writeValue(isMultiChoice);
        dest.writeValue(seqNo);
    }

    public int describeContents() {
        return 0;
    }

}
