package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ExamListAdapter;
import com.edusunsoft.erp.orataro.model.Exam;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SemesterExamFragment extends Fragment implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    Exam examModel;
    ArrayList<Exam> ExamList = new ArrayList<>();
    ExamListAdapter examListAdapter;
    RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    LinearLayout headerLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        View view = inflater.inflate(R.layout.checked_student_list_activity, null);
        mLayoutInflater = inflater;

        Initialization(view);

        return view;

    }

    private void Initialization(View view) {

        headerLayout = (LinearLayout) view.findViewById(R.id.headerLayout);
        headerLayout.setVisibility(View.GONE);
        recyclerView = (RecyclerView) view.findViewById(R.id.student_list);
        mLayoutManager = new LinearLayoutManager(getActivity());
        // use a linear layout manager
        recyclerView.setLayoutManager(mLayoutManager);
        ServiceResource.FROMEXAMTYPE = "Semester";

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.ExamResult) + "(" + Utility.GetFirstName(mContext) + ")");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

//        /*Commented By Krishna : 29-04-2019 GetSemesterExamLisr From API*/
//
        try {


            if (Utility.isNetworkAvailable(mContext)) {

                GetSemesterExamList();

            } else {

                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

//        /*END*/

    }

    private void GetSemesterExamList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
//        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
//        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchId()));
        arrayList.add(new PropertyVo(ServiceResource.EXAMTYPE, ServiceResource.SEMESTEREXAM));

        Log.d("ExamRequest", arrayList.toString());
//        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETEXAMSCHEDULELIST, ServiceResource.EXAM_URL);
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETEXAMSRESULTLIST, ServiceResource.EXAM_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETEXAMSRESULTLIST.equalsIgnoreCase(methodName)) {

            try {

                ExamList.clear();


                JSONArray jArray = new JSONArray(result);
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jop = jArray.getJSONObject(i);
                    JSONArray datajsonarray = jop.getJSONArray("data");

                    for (int j = 0; j < datajsonarray.length(); j++) {

                        JSONObject childobj = datajsonarray.getJSONObject(j);

                        examModel = new Exam();
                        examModel.setID(childobj.getString("ID"));
                        examModel.setExamName(childobj.getString("ExamName"));
                        examModel.setExamType(childobj.getString("ExamType"));
                        examModel.setStandard(childobj.getString("Standard"));
                        examModel.setDivision(childobj.getString("Division"));
                        examModel.setYear(childobj.getString("Year"));
                        examModel.setPublishDate(childobj.getString("PublishDate"));
                        examModel.setClientID(childobj.getString("ClientID"));
                        examModel.setInstituteID(childobj.getString("InstituteID"));

                        ExamList.add(examModel);

                    }

                }

                if (ExamList.size() > 0) {

                    // create an Object for Adapter
                    examListAdapter = new ExamListAdapter(getActivity(), ExamList);
                    // set the adapter object to the Recyclerview
                    recyclerView.setAdapter(examListAdapter);

                }


            } catch (JSONException e) {

                e.printStackTrace();

            }


        }


    }


}
