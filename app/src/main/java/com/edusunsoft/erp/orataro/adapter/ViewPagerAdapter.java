package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener.Anchor;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> list;
    private ImageView iv_fb;
    private int mPreviousPosition;

    public ViewPagerAdapter(Context mContext, ArrayList<String> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.zoomimage, container, false);
        iv_fb = (ImageView) layout.findViewById(R.id.iv_fb_zoom);
        iv_fb.setScaleType(ImageView.ScaleType.MATRIX);
        LinearLayout ll_fb = (LinearLayout) layout.findViewById(R.id.ll_fb);
        ll_fb.setOnTouchListener(new PanAndZoomListener(ll_fb, iv_fb, Anchor.TOPLEFT));

        if (list.get(position) != null && !list.get(position).equalsIgnoreCase("")) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL + "Datafiles/" +
                                list.get(position).replace("//DataFiles//", "/DataFiles/")
                                        .replace("//DataFiles/", "/DataFiles/"))
                        .apply(options)
                        .into(iv_fb);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeViewInLayout((View) object);
    }

}
