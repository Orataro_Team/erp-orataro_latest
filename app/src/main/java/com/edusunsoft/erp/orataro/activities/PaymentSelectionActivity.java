package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ButtonClick;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FessPaymentChargesAdapter;
import com.edusunsoft.erp.orataro.model.FeesAtomResponse;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.model.FeesPaymentType;
import com.edusunsoft.erp.orataro.model.FeesStructureModel;
import com.edusunsoft.erp.orataro.model.FeesSuccessResponseSend;
import com.edusunsoft.erp.orataro.services.HTTPURLConnection;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.XMLParsers;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class PaymentSelectionActivity extends Activity implements ButtonClick, ResponseWebServices {

    //  Button btncreditcard,btnDebitcard,btnNetbanking,btnImps;
    Context mContext;
    ImageView img_home, img_menu;
    TextView header_text;
    ListView lvwithextracharge;
    public static final String KEY_ATOM2REQUEST = "Atom2Request";
    String DoubleAmountStr = "0";
    FeesModel model;
    String paymentMethod = "NB";
    ArrayList<FeesStructureModel> listFeesstructure;
    ArrayList<FeesPaymentType> listFeespaymenttype;
    String patmentUsing = "";
    String AccountId = "";
    boolean ispaymentWOrking = false;
    public File file;

    // ArrayList<FeesPaymentCharges> listFeesscharges;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_selection);
        mContext = PaymentSelectionActivity.this;

        listFeesstructure = getIntent().getParcelableArrayListExtra("list");
        listFeespaymenttype = getIntent().getParcelableArrayListExtra("feestype");

        Log.d("listfeespymntype", listFeespaymenttype.toString());
        Log.d("listFeesstructure", listFeesstructure.toString());
        //  listFeesscharges = getIntent().getParcelableArrayListExtra("listFeesscharges");
        DoubleAmountStr = getIntent().getStringExtra("price");
        model = getIntent().getParcelableExtra("model");
        img_home = (ImageView) findViewById(R.id.img_home);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        header_text = (TextView) findViewById(R.id.header_text);

        lvwithextracharge = (ListView) findViewById(R.id.lvwithextracharge);


        TextView intxtfees = (TextView) findViewById(R.id.txtfees);
        TextView intxtammount = (TextView) findViewById(R.id.txtammount);
        TextView intxtdue = (TextView) findViewById(R.id.txtdue);
        TextView intxtaction = (TextView) findViewById(R.id.txtaction);

        intxtfees.setText("Payment Mode");
        intxtammount.setText("charges");
        intxtdue.setText("Payable Amount");
        intxtaction.setText("Pay");


//        btncreditcard = (Button) findViewById(R.id.btncreditcard);
//        btnDebitcard = (Button) findViewById(R.id.btnDebitcard);
//        btnNetbanking = (Button) findViewById(R.id.btnNetbanking);
//        btnImps = (Button) findViewById(R.id.btnImps);


//        btncreditcard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                paymentMethod="CC";
//                new StartPayment().execute();
//            }
//        });
//
//        btnDebitcard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                paymentMethod="DC";
//                new StartPayment().execute();
//            }
//        });
//
//        btnNetbanking.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                paymentMethod="NB";
//                new StartPayment().execute();
//            }
//        });
//
//        btnImps.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                paymentMethod="IMPS";
//                new StartPayment().execute();
//            }
//        });

        img_menu.setVisibility(View.INVISIBLE);
        img_home.setImageResource(R.drawable.back);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        header_text.setText("Payment Mode");
        FessPaymentChargesAdapter adapter = new FessPaymentChargesAdapter(mContext, listFeespaymenttype, DoubleAmountStr, this);
        lvwithextracharge.setAdapter(adapter);

    }

    @Override
    public void buttonClick(int i, String paymentmode, double totalAmount) {
        patmentUsing = listFeespaymenttype.get(i).getAcctName();
        AccountId = listFeespaymenttype.get(i).getAcctID();
        paymentMethod = paymentmode;
        DoubleAmountStr = totalAmount + "";
        new StartPayment().execute();

    }


    private class StartPayment extends AsyncTask<String, Void, String> {

        String Atom2Request;
        ProgressDialog pDialog;
        private String vVenderURL;
        private String xmlttype;

        private String xmltempTxnId, xmltoken, xmltxnStage, xmlURL;

        @Override
        protected String doInBackground(String... params) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            // vVenderURL = "https://paynetzuat.atomtech.in/paynetz/epi/fts?login=160&pass=Test@123&ttype=NBFundTransfer&prodid=NSE&amt="+DoubleAmountStr+"&txncurr=INR&txnscamt=0&clientcode=TkFWSU4%3d&txnid=123&date=03/07/2015&custacc=1234567890&udf1=Customer&udf2=rajtufan@gmail.com&udf3=8485835654&udf4=pune&ru=http://example.webservice/response.aspx?";
//            vVenderURL = "https://payment.atomtech.in/paynetz/epi/fts?" +
//                    "login=23930" +
//                    "&pass=Suns@123" +
//                    "&ttype=NBFundTransfer" +
//                    "&prodid=SUNSOFT" +
//                    "&amt=51"+//+DoubleAmountStr+
//                    "&txncurr=INR" +
//                    "&txnscamt=0" +
//
//                    "&mdd="+paymentMethod+
//                    "&clientcode=007" +
//                    "&txnid=123" +
//                    "&date="+ Utility.getDate(System.currentTimeMillis(),"dd/MM/yyyy")+
//                    "&custacc=1234567890" +
//                    "&udf1=Customer" +
//                    "&udf2=rajtufan@gmail.com" +
//                    "&udf3=" +new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER()+
//                    "&udf4=pune&ru=http://www.softqubedesign.com/IosPrectical/atomResponse.php";

            FeesSuccessResponseSend feesSucessResponse = new FeesSuccessResponseSend();
            FeesSuccessResponseSend.Table table = new FeesSuccessResponseSend.Table();
            FeesSuccessResponseSend.Table1 table1 = new FeesSuccessResponseSend.Table1();
            ArrayList<FeesSuccessResponseSend.Table> tableList = new ArrayList<FeesSuccessResponseSend.Table>();
            ArrayList<FeesSuccessResponseSend.Table1> table1List = new ArrayList<FeesSuccessResponseSend.Table1>();


//            StudentID:        aca15143-044a-4d73-9b19-576ee8199995
//            FeeStrucureID:  4803d58e-a3eb-4dde-b264-3a1515898e72
//            BatchID:           440b0343-4c52-4e69-8557-82a239ef52e3
//            ClientID:           a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c
//            InstituteID:        4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4
//            UserID: 1212f4dc-ff08-4acc-ad38-739e6b28b247


            table.setStudentFeesCollectionID("");
            table.setBatchID(new UserSharedPrefrence(mContext).getBATCHID());//"440b0343-4c52-4e69-8557-82a239ef52e3");//
            table.setStudentID(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            //  table.setStudentID("aca15143-044a-4d73-9b19-576ee8199995");

            table.setFeesStructureID(isNull(listFeesstructure.get(0).getFeesStructuteID()));//model.getFeesStructuteID());//"4803d58e-a3eb-4dde-b264-3a1515898e72");//
            table.setFeesStructureScheduleID(isNull(model.getFeesStructureScheduleID()));
            table.setStandardID(new UserSharedPrefrence(mContext).getLOGIN_GRADEID());
            table.setDateOfFees("");
            table.setAmountToBePaid(DoubleAmountStr);
            table.setTotalDue("0");
            table.setIsPaid("True");
            table.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());//"a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c");//
            table.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());//"4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4");//
            table.setIsActive("True");
            table.setPaidAmounts(DoubleAmountStr);
            table.setUserID(new UserSharedPrefrence(mContext).getLOGIN_USERID());//"1212f4dc-ff08-4acc-ad38-739e6b28b247");//new UserSharedPrefrence(mContext).getLOGIN_USERID());
            table.setBookID("");
            table.setRefStudentFeesCollectionID(isNull(model.getStudentFeesCollectionID()));
            table.setMOPTypeTerm(patmentUsing);
            table.setBankName("");
            table.setReferenceNo("");


            for (int j = 0; j < listFeesstructure.size(); j++) {
                table1 = new FeesSuccessResponseSend.Table1();
                table1.setStudentFeesCollectionDetailID("");
                table1.setStudentFeesCollectionID("");
                table1.setFeesStructureID(isNull(listFeesstructure.get(j).getFeesStructuteID()));//"4803d58e-a3eb-4dde-b264-3a1515898e72");//listFeesstructure.get(j).getFeesStructuteID());
                table1.setFeesHeadID(isNull(listFeesstructure.get(j).getFeesHeadID()));
                table1.setOriginalAmount(isNull(listFeesstructure.get(j).getTotalDue()));
                table1.setReceivedAmount(isNull(listFeesstructure.get(j).getTotalDue()));
                table1.setBookID("");
                table1.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());//"a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c");//new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());
                table1.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());//"4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4");//new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());
                table1.setIsActive("True");
                table1.setIsPaid("True");
                table1.setTotalDue("0");
                table1.setIsOptionalFeeHead("False");
                table1List.add(table1);
            }


            tableList.add(table);


            feesSucessResponse.setTable(tableList);
            feesSucessResponse.setTable1(table1List);


            String feesIdJson = new Gson().toJson(tableList);
            String feesDetailJson = new Gson().toJson(table1List);
            try {
//                UUID uid = UUID.fromString("384000008cf011bdb23e10b96e4ef00d10cf");
//                String    deviceId = uid.randomUUID().toString();


//                vVenderURL = "https://paynetzuat.atomtech.in/paynetz/epi/fts?" +
//                        "login=160" +
//                        "&pass=Test@123" +
//                        "&ttype=NBFundTransfer" +
//                        "&prodid=NSE" +
//                        "&amt="+DoubleAmountStr+
//                        "&txncurr=INR" +
//                        "&txnscamt=0"+
//                        "&clientcode=121"+
//                        "&txnid="+System.currentTimeMillis()+new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER()+
//                        "&date="+URLEncoder.encode( Utility.getDate(System.currentTimeMillis(),"dd/MM/yyyy HH:mm:ss"), "UTF-8")+
//                        "&custacc="+URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER(), "UTF-8")+
//                        "&udf1="+URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME(), "UTF-8")+
//                        // "&udf2=" +new UserSharedPrefrence(mContext).get+
//                        "&udf3="+URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER(), "UTF-8")+
//                         "&udf9="+model.getFeesStructuteID()+"@"+AccountId+"@"+new UserSharedPrefrence(mContext).getLOGIN_USERID()+"@"+new UserSharedPrefrence(mContext).getLOGIN_MEMBERID()+
//                        "&ru=http://www.softqubedesign.com/IosPrectical/atomResponse.php"+
//                        "&mdd="+paymentMethod;
                //    vVenderURL = "https://paynetzuat.atomtech.in/paynetz/epi/fts?login=160&pass=Test@123&ttype=NBFundTransfer&prodid=NSE&amt=51&txncurr=INR&txnscamt=0&clientcode=007&txnid=123&date=24/11/2017&custacc=1234567890&udf1=Customer&udf2=rajtufan@gmail.com&udf3=9123456789&udf4=pune&ru=http://www.softqubedesign.com/IosPrectical/atomResponse.php&mdd=DC";

//                vVenderURL = "https://paynetzuat.atomtech.in/paynetz/epi/fts?" +
//                "login=160"+
//                        "&pass=Test@123"+
//                        "&ttype=NBFundTransfer"+
//                        "&prodid=NSE"+
//                        "&amt=50.02"+
//                        "&txncurr=INR"+
//                        "&txnscamt=0"+
//                        "&clientcode=VGc9PQ=="+
//                        "&txnid=1234567"+
//                        "&date="+Utility.getDate(System.currentTimeMillis(),"dd/MM/yyyy")+
//                        "&custacc=123456789012"+
//                        "&ru=http://www.softqubedesign.com/IosPrectical/atomResponse.php"+
//                        "&mdd=CC" +
//                        "&udf1=Manish" +
//                        "&udf2=Manish@mailinator.com" +
//                        "&udf3=9988552244" +
//                        "&udf4=SuratGujarat";

                //    https://paynetzuat.atomtech.in/paynetz/epi/fts?ttype=NBFundTransfer&txnStage=1&tempTxnId=100000505749&token=krkpZuk8paSIgycNVCodViOl5K0o%2BFm6%2BhfttoS0n%2Bs%3DModel

//
                vVenderURL = "https://payment.atomtech.in/paynetz/epi/fts?" +
                        "login=23930" +
                        "&pass=Suns@123" +
                        "&ttype=NBFundTransfer" +
                        "&prodid=SUNSOFT" +
                        "&amt=" + DoubleAmountStr +
                        "&txncurr=INR" +
                        "&txnscamt=0" +
                        "&mdd=" + paymentMethod +
                        "&clientcode=00785" +
                        "&txnid=" + System.currentTimeMillis() + new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER() +
                        "&date=" + URLEncoder.encode(Utility.getDate(System.currentTimeMillis(), "dd/MM/yyyy HH:mm:ss"), "UTF-8") +
                        "&custacc=" + URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER(), "UTF-8") +
                        "&udf1=" + URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME(), "UTF-8") +
                        // "&udf2=" +new UserSharedPrefrence(mContext).get+
                        "&udf3=" + URLEncoder.encode(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER(), "UTF-8") +
                        "&udf9=" + model.getFeesStructuteID() + "@" + AccountId + "@" + new UserSharedPrefrence(mContext).getLOGIN_USERID() + "@" + new UserSharedPrefrence(mContext).getLOGIN_MEMBERID() +
                        "&ru=http://www.softqubedesign.com/IosPrectical/atomResponse.php";
            } catch (Exception e) {
                e.printStackTrace();
            }
            //	vVenderURL ="";
            Log.d("Vvendor URL", vVenderURL);
            XMLParsers parser = new XMLParsers();
            HashMap<String, String> postDataParams = new HashMap<String, String>();
            String xml = new HTTPURLConnection().ServerData(vVenderURL.trim(), postDataParams);//parser.getXmlFromUrl(vVenderURL); // getting XML
            if (xml != null) {

                Document doc = parser.getDomElement(xml); // getting DOM element
                if (doc != null) {
                    Log.d("XML URL", xml);
                    NodeList nList = doc.getElementsByTagName("RESPONSE");

                    for (int tempN = 0; tempN < nList.getLength(); tempN++) {
                        Node nNode = nList.item(tempN);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode;
                            System.out.println("URL : " + eElement.getElementsByTagName("url").item(0).getChildNodes().item(0).getNodeValue());
                            xmlURL = eElement.getElementsByTagName("url").item(0).getChildNodes().item(0).getNodeValue();

                            NodeList aList = eElement.getElementsByTagName("param");
                            String vParamName;
                            for (int atrCnt = 0; atrCnt < aList.getLength(); atrCnt++) {
                                vParamName = aList.item(atrCnt).getAttributes().getNamedItem("name").getNodeValue();
                                System.out.println("<br>paramName : : " + vParamName);

                                if (vParamName.equals("ttype")) {
                                    xmlttype = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
                                } else if (vParamName.equals("tempTxnId")) {
                                    xmltempTxnId = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
                                } else if (vParamName.equals("token")) {
                                    xmltoken = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
                                } else if (vParamName.equals("txnStage")) {
                                    xmltxnStage = aList.item(atrCnt).getChildNodes().item(0).getNodeValue();
                                }
                            }
                            Log.d("XML URL", xmlURL);
                            Log.d("XML TRANS TYPE", xmlttype);
                            Log.d("tempTxnId : ", xmltempTxnId);
                            Log.d("param : token     :", xmltoken);
                            Log.d("param : txnStage  : ", xmltxnStage);
                        }
                    }//for
                }
            }

            Atom2Request = xmlURL + "?ttype=" + "NBFundTransfer" + "&tempTxnId=" + xmltempTxnId + "&token=" + xmltoken + "&txnStage=" + xmltxnStage;
            Atom2Request = Atom2Request.replace(" ", "%20");
            Log.d("ATOM 2nd Request URl", Atom2Request);


            return Atom2Request;
        }

        @Override
        protected void onPostExecute(String result) {


            if (pDialog != null) {
                pDialog.dismiss();
                Intent intent = new Intent(mContext, WebContentActivity.class);
                intent.putExtra(KEY_ATOM2REQUEST, result);
                startActivityForResult(intent, 3);
                //finish();..........................................................................
            }

        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(mContext, R.style.AppCompatAlertDialogStyle);
            pDialog.setMessage("Processing Request...");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
            super.onPreExecute();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed here it is 2


        if (data != null) {
            String response = data.getStringExtra("Result");


            Type collectionType = new TypeToken<FeesAtomResponse>() {
            }.getType();
            FeesAtomResponse responseobject = new Gson().fromJson(response, collectionType);

            if (responseobject.getFCode().equalsIgnoreCase("Ok")) {
                Utility.toast(mContext, getResources().getString(R.string.paymentnotsucess));
            } else {
                Utility.toast(mContext, getResources().getString(R.string.paymentnotunsucess));
            }

            finish();
//
//                FeesSuccessResponseSend feesSucessResponse = new FeesSuccessResponseSend();
//                FeesSuccessResponseSend.Table table = new FeesSuccessResponseSend.Table();
//                FeesSuccessResponseSend.Table1 table1 = new FeesSuccessResponseSend.Table1();
//                ArrayList<FeesSuccessResponseSend.Table> tableList = new ArrayList<FeesSuccessResponseSend.Table>();
//                ArrayList<FeesSuccessResponseSend.Table1> table1List = new ArrayList<FeesSuccessResponseSend.Table1>();
//
//
////            StudentID:        aca15143-044a-4d73-9b19-576ee8199995
////            FeeStrucureID:  4803d58e-a3eb-4dde-b264-3a1515898e72
////            BatchID:           440b0343-4c52-4e69-8557-82a239ef52e3
////            ClientID:           a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c
////            InstituteID:        4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4
////            UserID: 1212f4dc-ff08-4acc-ad38-739e6b28b247
//
//
//                table.setStudentFeesCollectionID("");
//                table.setBatchID(new UserSharedPrefrence(mContext).getBATCHID());//"440b0343-4c52-4e69-8557-82a239ef52e3");//
//                table.setStudentID(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
//                //  table.setStudentID("aca15143-044a-4d73-9b19-576ee8199995");
//
//                table.setFeesStructureID(isNull(listFeesstructure.get(0).getFeesStructuteID()));//model.getFeesStructuteID());//"4803d58e-a3eb-4dde-b264-3a1515898e72");//
//                table.setFeesStructureScheduleID(isNull(model.getFeesStructureScheduleID()));
//                table.setStandardID(new UserSharedPrefrence(mContext).getLOGIN_GRADEID());
//                table.setDateOfFees(responseobject.getDate());
//                table.setAmountToBePaid(DoubleAmountStr);
//                table.setTotalDue("0");
//                table.setIsPaid("True");
//                table.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());//"a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c");//
//                table.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());//"4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4");//
//                table.setIsActive("True");
//                table.setPaidAmounts(DoubleAmountStr);
//                table.setUserID(new UserSharedPrefrence(mContext).getLOGIN_USERID());//"1212f4dc-ff08-4acc-ad38-739e6b28b247");//new UserSharedPrefrence(mContext).getLOGIN_USERID());
//                table.setBookID("");
//                table.setRefStudentFeesCollectionID(isNull(model.getStudentFeesCollectionID()));
//                table.setMOPTypeTerm(patmentUsing);
//                table.setBankName(responseobject.getBankName());
//                table.setReferenceNo(responseobject.getMmpTxn());
//
//
//                for (int j = 0; j < listFeesstructure.size(); j++) {
//                    table1 = new FeesSuccessResponseSend.Table1();
//                    table1.setStudentFeesCollectionDetailID("");
//                    table1.setStudentFeesCollectionID("");
//                    table1.setFeesStructureID(isNull(listFeesstructure.get(j).getFeesStructuteID()));//"4803d58e-a3eb-4dde-b264-3a1515898e72");//listFeesstructure.get(j).getFeesStructuteID());
//                    table1.setFeesHeadID(isNull(listFeesstructure.get(j).getFeesHeadID()));
//                    table1.setOriginalAmount(isNull(listFeesstructure.get(j).getTotalDue()));
//                    table1.setReceivedAmount(isNull(listFeesstructure.get(j).getTotalDue()));
//                    table1.setBookID("");
//                    table1.setClientID(new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());//"a1c4ceb1-0bfe-46f9-8fba-71441dda2b5c");//new UserSharedPrefrence(mContext).getLOGIN_CLIENTID());
//                    table1.setInstituteID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());//"4fd9e5f3-dbd7-4d48-9ebb-3400a36df0b4");//new UserSharedPrefrence(mContext).getLOGIN_INSTITUTEID());
//                    table1.setIsActive("True");
//                    table1.setIsPaid("True");
//                    table1.setTotalDue("0");
//                    table1.setIsOptionalFeeHead("False");
//                    table1List.add(table1);
//                }
//
//
//                tableList.add(table);
//
//
//                feesSucessResponse.setTable(tableList);
//                feesSucessResponse.setTable1(table1List);
//
//
//                String feesIdJson = new Gson().toJson(tableList);
//                String feesDetailJson = new Gson().toJson(table1List);
//
//                ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
//
//                arrayList.add(new PropertyVo(ServiceResource.FEECOLLECTION,
//                        feesIdJson));
//                arrayList.add(new PropertyVo(ServiceResource.FEECOLLECTIONDETAILS,
//                        feesDetailJson));
//
//                arrayList.add(new PropertyVo(ServiceResource.USER_ID,
//                        new UserSharedPrefrence(mContext).getLoginModel().getUserID()/*"c9af3280-e559-4a12-b42e-a211136ca3e0"*/));
//                arrayList.add(new PropertyVo(ServiceResource.ACOUNTID,
//                        AccountId));
//                new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDFEECOLLECTION_METHODNAME,
//                        ServiceResource.FEES_URL);
//
//
////                String message = data.getStringExtra("status");
////                String[] resKey = data.getStringArrayExtra("responseKeyArray");
////                String[] resValue = data.getStringArrayExtra("responseValueArray");
////
////                if(resKey!=null && resValue!=null)
////                {
////                    for(int i=0; i<resKey.length; i++)
////                        Log.v("Payment","  "+i+" resKey : "+resKey[i]+" resValue : "+resValue[i]);
////                }
////                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
////                Log.v("Payment","RECEIVED BACK--->" + message);
//
//
//            } else {
//                Utility.toast(mContext, getResources().getString(R.string.paymentnotsucess));
//            }

        }
    }


    @Override
    public void response(String result, String methodName) {

        try {
            JSONObject innerObject = new JSONObject(result);

            String filepath = innerObject.getString("ReceiptPath");
            if (!filepath.equalsIgnoreCase("")) {
                filepath = ServiceResource.ERP_URL + filepath;
                saveDownload(filepath);
                Utility.toast(mContext, "Payment sucessfully.");
                finish();
            } else {
                Utility.toast(mContext, "Payment Failed.\n Please Try again ");
//                    finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDownload(String url) {
        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];

        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
////        if (!isDownloading) {
//
//        DownloadManager mgr = (DownloadManager)
//                mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//
//        DownloadManager.Request request =
//                new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding Receipt...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request.setNotificationVisibility(DownloadManager
//                    .Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request.allowScanningByMediaScanner();
//        }
//
//        long id = mgr.enqueue(request);
//        Utility.toast(mContext, mContext.getResources().getString(R.string.downloadstarting));

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);
//            Toast.makeText(mContext, mContext.getResources().getString(R.string.downloadstarting), Toast.LENGTH_SHORT).show();
//        }

//        else {
//            isDownloading = false;
//            Utility.toast(mContext, mContext.getResources().getString(R.string.uarealreadydownload) + " File Name : " + fileName);
////            Toast.makeText(mContext, mContext.getResources().getString(R.string.uarealreadydownload) + "File Name :" + fileName, Toast.LENGTH_SHORT).show();
//            MimeTypeMap myMime = MimeTypeMap.getSingleton();
//            Intent newIntent = new Intent(Intent.ACTION_VIEW);
//            String mimeType = myMime.getMimeTypeFromExtension(fileExt(new File(Utility.getDownloadFilename(fileName)).getAbsolutePath()).substring(0));
//            newIntent.setDataAndType(Uri.fromFile(new File(Utility.getDownloadFilename(fileName))), mimeType);
//            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            try {
//                mContext.startActivity(newIntent);
//            } catch (ActivityNotFoundException e) {
//                Utility.toast(mContext, "No handler for this type of file.");
////                Toast.makeText(mContext, "No handler for this type of file.", Toast.LENGTH_LONG).show();
//            }
//
//        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }


    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

    public String isNull(String string) {
        if (string != null && !string.equalsIgnoreCase("") && !string.equalsIgnoreCase("null")) {

        } else {
            string = "";
        }

        return string;
    }
}
