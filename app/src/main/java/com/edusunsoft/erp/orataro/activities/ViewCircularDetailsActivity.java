package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.CircularFragment;
import com.edusunsoft.erp.orataro.fragments.HomeWorkListFragment;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;

public class ViewCircularDetailsActivity extends Activity implements OnClickListener, ResponseWebServices {
    Context mContext;
    AdjustableImageView iv_cr_details;
    ImageView img_profile, img_back;
    TextView txt_sub_name, txt_techerName, txt_cr_details;
    String cr_name, teacher_name, crdetails_txt, ReferenceLink;
    String cr_details;
    String teacher_img;
    RelativeLayout rl_view;
    boolean isApporve;
    String circularId;
    private String imgUrl;
    boolean isFile = false;
    TextView txt_reference_link;
    public File file;
    CheckBox chk_finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_circular_details);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ViewCircularDetailsActivity.this;
        LinearLayout ll_finish = (LinearLayout) findViewById(R.id.ll_finish);
        chk_finish = (CheckBox) findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) findViewById(R.id.txtisfinish);
        txtisfinish.setText(getResources().getString(R.string.isAccept));
        if (Utility.isTeacher(mContext)) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }
        cr_name = getIntent().getStringExtra("Crname");
        cr_details = getIntent().getStringExtra("Crdetails");
        imgUrl = cr_details;
        Log.d("getimageurl", imgUrl);
        ReferenceLink = getIntent().getStringExtra("referencelink");
        teacher_name = getIntent().getStringExtra("CrTeacher_name");
        crdetails_txt = getIntent().getStringExtra("Crdetails_txt");
        teacher_img = getIntent().getStringExtra("CrTeacher_img");
        isApporve = getIntent().getBooleanExtra("isApporve", false);
        circularId = getIntent().getStringExtra("id");
        img_back = (ImageView) findViewById(R.id.img_back);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        iv_cr_details = (AdjustableImageView) findViewById(R.id.iv_cr_details);
        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        txt_sub_name = (TextView) findViewById(R.id.txt_sub_name);
        txt_techerName = (TextView) findViewById(R.id.txt_techerName);
        txt_cr_details = (TextView) findViewById(R.id.txt_cr_details);
        txt_reference_link = (TextView) findViewById(R.id.txt_reference_link);
        txt_sub_name.setText(cr_name);

        if (cr_details != null) {

//			iv_cr_details.setImageResource(cr_details);

        } else {

            iv_cr_details.setVisibility(View.VISIBLE);

        }

        txt_techerName.setText(teacher_name);
        txt_cr_details.setText(crdetails_txt);
        if (ReferenceLink.equalsIgnoreCase("") || ReferenceLink.equalsIgnoreCase("null") || ReferenceLink.equalsIgnoreCase(null)) {
            txt_reference_link.setVisibility(View.GONE);
        } else {
            txt_reference_link.setText(ReferenceLink);
        }


        try {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL1 + teacher_img)
                    .apply(options)
                    .into(img_profile);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isApporve) {

            chk_finish.setChecked(true);

        } else {

            chk_finish.setChecked(false);

        }

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                circularListIsApprove(circularId, isChecked);
            }
        });

        if (imgUrl != null && !imgUrl.contains("null")) {

            if (imgUrl.contains(".TEXT") || imgUrl.contains(".txt")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.txt);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

            } else if (imgUrl.contains(".PDF") || imgUrl.contains(".pdf")) {

                isFile = true;
                iv_cr_details.setImageResource(R.drawable.pdf);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (imgUrl.contains(".doc") || imgUrl.contains(".doc")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.doc);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (imgUrl.contains(".docs") || imgUrl.contains(".docs")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.file);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

            } else if (imgUrl.contains(".xlsx") || imgUrl.contains(".xls") || imgUrl.contains(".xlsm")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.excel_file);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {

                try {

                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.photo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(imgUrl.replace("//DataFiles//", "/DataFiles/"))
                            .apply(options)
                            .into(iv_cr_details);

                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
            iv_cr_details.setVisibility(View.VISIBLE);
        } else {
            iv_cr_details.setVisibility(View.GONE);
        }


        img_back.setOnClickListener(this);
        iv_cr_details.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                Utility.ISLOADCIRCULAR = false;
                finish();
                break;
            case R.id.iv_cr_details:

                if (!isFile) {
                    Intent i = new Intent(mContext, ZoomImageAcitivity.class);
                    i.putExtra("img", imgUrl);
                    i.putExtra("name", cr_name);
                    startActivity(i);
                } else {
                    Log.d("downloadurl", imgUrl.replace("//DataFiles//",
                            "/DataFiles/"));

                    saveDownload(imgUrl.replace("//DataFiles//",
                            "/DataFiles/"));
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADCIRCULAR = false;
    }


    public void circularListIsApprove(String assosiationId, boolean isApprove) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Circular"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);
    }

    @Override
    public void response(String result, String methodName) {
        CircularFragment.adapter.NotifyCircularItemData(circularId, chk_finish.isChecked());
    }

    private void saveDownload(String url) {

        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }

        }
//
//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager)
//                mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//        Log.d("getdestinationurl", destination.toString());
//        DownloadManager.Request request =
//                new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager
//                .Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//        long id = mgr.enqueue(request);
//        Utility.toast(mContext, getResources().getString(R.string.downloadstarting));


        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);

    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

}
