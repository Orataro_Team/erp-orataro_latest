package com.edusunsoft.erp.orataro.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.OpenableColumns;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.loader.content.CursorLoader;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.coursion.freakycoder.mediapicker.galleries.Gallery;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.DeleteItemNewPost;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PhotoListAdapter;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingDataDao;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.FileCompressor;
import com.edusunsoft.erp.orataro.util.FileUtils;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.ImageAndroid11;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.UUID;

import id.zelory.compressor.Compressor;

import static com.edusunsoft.erp.orataro.services.ServiceResource.PROFILEWALL;
import static com.edusunsoft.erp.orataro.util.Utility.FILE_PROVIDER;
import static com.edusunsoft.erp.orataro.util.Utility.showToast;


public class AddPostOnWallActivity extends Activity implements OnClickListener, DeleteItemNewPost, ResponseWebServices {

    private static Context mContext;
    private ImageView img_back, iv_post, iv_share;
    private LinearLayout ll_photo, ll_video, ll_link, ll_more, ll_fb_post_sharing;
    private QuickAction quickAction, quickActionForShare;
    private EditText edt_post;
    private TextView tv_share;
    private static final int ID_EVENT = 1;
    private static final int ID_POLL = 2;
    private static final int REQUEST_PATH = 123;
    private static final int ID_FRIEND = 1;
    private static final int ID_PUBLIC = 2;
    private static final int ID_ONLY_ME = 3;
    protected static final int RESULT_SPEECH = 121;
    private Uri fileUri;
    private ActionItem actionEvent, actionPoll, actionPublic, actionFriend, actionOnlyMe;
    private GridView lv_multipleImg;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private String filePath;
    private String[] filePathArray;
    private byte[] byteArray;
    private PhotoListAdapter photo_ListAdapter;
    protected int REQUEST_CAMERA_VIDEO = 3;
    protected int SELECT_FILE_VIDEO = 4;
    private int fileType = -1;
    private CircleImageView img_profilepic;
    private Dialog mPoweroffDialog;
    private String from = ServiceResource.Wall;
    private boolean isEdit;
    private WallPostModel postModel;
    private TextView header_text;
    private LinearLayout ll_share, ll_file;
    private String curFileName;
    private String FileMineType = "TEXT";
    private ImageView img_file;
    private int imagePos = 0;
    private boolean selectMultipleImage = false;
    protected Uri imageToUploadUri;
    private UUID idOne = UUID.randomUUID();
    private boolean ismp3 = false;
    private ImageView img_speack;
    private LinearLayout dropPreview;
    private TextView editTextTitlePost, editTextDescriptionPost;
    private TextCrawler textCrawler;
    private Bitmap[] currentImageSet;
    private Bitmap currentImage;
    private int currentItem = 0;
    private int countBigImages = 0;
    private boolean noThumb;
    private String currentTitle, currentUrl, currentCannonicalUrl, currentDescription;
    private ImageView img_facebook;
    private ArrayList<PhotoModel> photoModels_add;

    private String fielPath;

    /*declaration of byte array for uploadphoto*/

    private byte[] photoArray;
    Uri mCapturedImageURI;

    /*END*/

    // variable declareation for dynmicwallsetting
    DynamicWallSettingDataDao dynamicWallSettingDataDao;
    /*END*/

    // variable for add base64 while add photo to wall
    ArrayList<String> GetImagename = new ArrayList<>();
    Bitmap bitmap = null;
    String BASE64_STRING = "", Path = "";
    /*END*/

    final DecimalFormat format = new DecimalFormat("#.#");
    final long MB = 1024 * 1024;
    public String file_size;
    public String FileNamewithoutExtension = "", imageEncoded = "";

    public static final int READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY = 140, READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA = 150, OPEN_MEDIA_PICKER = 30;
    private static final int PICK_FROM_GALLERY = 1112;
    public static ArrayList<String> imagesEncodedList;

    public String mCurrentPhotoPath = "";
    private PhotoModel photoModel;

    File mPhotoFile;
    FileCompressor mCompressor;

    public String FILENAME = "", FILEIMAGE = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post_on_wall);

        mContext = AddPostOnWallActivity.this;

        if (getIntent() != null) {
            from = getIntent().getStringExtra("isFrom");
            if (from == null || from.equalsIgnoreCase("")) {
                from = ServiceResource.Wall;
            }
            isEdit = getIntent().getBooleanExtra("isEdit", false);
            if (isEdit) {
                postModel = (WallPostModel) getIntent().getSerializableExtra("model");
            }

        }

        mCompressor = new FileCompressor(this);
        photoModels_add = new ArrayList<PhotoModel>();
        Global.AlbumModels = new ArrayList<PhotoModel>();
        editTextTitlePost = null;
        editTextDescriptionPost = null;
        textCrawler = new TextCrawler();

        dynamicWallSettingDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).dynamicWallSettingDataDao();
        Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();


        ll_photo = (LinearLayout) findViewById(R.id.ll_photo);
        ll_video = (LinearLayout) findViewById(R.id.ll_video);
        ll_link = (LinearLayout) findViewById(R.id.ll_link);
        ll_more = (LinearLayout) findViewById(R.id.ll_more);
        ll_file = (LinearLayout) findViewById(R.id.ll_file);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ll_fb_post_sharing = (LinearLayout) findViewById(R.id.ll_fb_post_sharing);
        header_text = (TextView) findViewById(R.id.header_text);
        img_back = (ImageView) findViewById(R.id.img_back);
        iv_post = (ImageView) findViewById(R.id.iv_post);
        iv_share = (ImageView) findViewById(R.id.iv_share);
        img_file = (ImageView) findViewById(R.id.img_file);
        tv_share = (TextView) findViewById(R.id.tv_share);
        img_speack = (ImageView) findViewById(R.id.img_speack);
        dropPreview = (LinearLayout) findViewById(R.id.drop_preview);
        img_facebook = (ImageView) findViewById(R.id.img_facebook);

        if (edt_post == null) {

            edt_post = (EditText) findViewById(R.id.edt_post);

        }

        lv_multipleImg = (GridView) findViewById(R.id.lv_multipleImg);

        if (getIntent() != null) {

            Intent intent = getIntent();
            String action = intent.getAction();
            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true")) {
                    if ("text/plain".equals(type)) {
                        handleSendText(intent); // Handle text being sent
                    }
                } else {
                    Utility.toast(mContext, getResources().getString(R.string.youhavenotpermission));
                }

                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true")) {
                    if (type.startsWith("image/")) {
                        handleSendImage(intent); // Handle single image being sent
                    }
                } else {
                    Utility.toast(mContext, getResources().getString(R.string.youhavenotpermission));
                }
                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true")) {
                    if (type.startsWith("video/")) {
                        handleVideoFile(intent); // Handle multiple images being sent
                    }
                } else {
                    Utility.toast(mContext, getResources().getString(R.string.youhavenotpermission));
                }
                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {
                    if (type.startsWith("application/pdf")) {
                        handleSendFile(intent); // Handle single image being sent
                    }
                    if (type.startsWith("application/msword")) {
                        handleSendFile(intent); // Handle single image being sent
                    }

                } else {
                    Utility.toast(mContext, getResources().getString(R.string.youhavenotpermission));
                }

            }

            if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true")) {
                    if (type.startsWith("image/")) {
                        handleSendMultipleImages(intent); // Handle multiple images being sent
                    }

                } else {

                    Utility.toast(mContext, getResources().getString(R.string.youhavenotpermission));

                }

            }

        }

        try {
            header_text.setText(getResources().getString(R.string.AddPost) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            if (isEdit) {

                ll_photo.setVisibility(View.INVISIBLE);
                ll_video.setVisibility(View.INVISIBLE);
                ll_file.setVisibility(View.INVISIBLE);
                ll_share.setVisibility(View.GONE);
                edt_post.setText(Html.fromHtml(postModel.getPostCommentNote()));
                header_text.setText(getResources().getString(R.string.EditPost) + " (" + Utility.GetFirstName(mContext) + ")");

            } else {

                if (from.equalsIgnoreCase(PROFILEWALL)
                        || from.equalsIgnoreCase(ServiceResource.Wall)) {

                    if (Utility.isTeacher(mContext)) {
                        tv_share.setText(getResources().getString(R.string.Public));
                    } else {
                        tv_share.setText(getResources().getString(R.string.onlyme));
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                        ll_photo.setVisibility(View.INVISIBLE);
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("false")) {
                        ll_video.setVisibility(View.INVISIBLE);
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("false")) {

                        ll_file.setVisibility(View.INVISIBLE);

                    }

                } else {

                    if (Global.DynamicWallSetting != null) {

                        if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") &&
                                Global.DynamicWallSetting.getIsAllowPeoplePostComment() &&
                                Global.DynamicWallSetting.getIsAllowPostPhoto())) {

                            ll_photo.setVisibility(View.VISIBLE);

                        } else {

                            ll_photo.setVisibility(View.INVISIBLE);

                        }


                        if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") &&
                                Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() &&
                                Global.DynamicWallSetting.getIsAllowPostVideo())) {

                            ll_video.setVisibility(View.VISIBLE);

                        } else {

                            ll_video.setVisibility(View.INVISIBLE);

                        }

                        if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) &&
                                Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() &&
                                Global.DynamicWallSetting.getIsAllowPostFile()) {

                            ll_file.setVisibility(View.VISIBLE);

                        } else {

                            ll_file.setVisibility(View.INVISIBLE);

                        }


                    }

                }


            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        edt_post.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                /*Commented By Krishna - 01-08-2019 : Remove Hint While Type/Add Url (HTTP)*/

//                if (s.length() != 0)
//                    if (s.toString().contains("http")) {
//                        textCrawler.makePreview(callback, edt_post.getText().toString(), dropPreview);
//                    }

                /*END*/

            }

        });

        ll_photo.setOnClickListener(this);
        ll_link.setOnClickListener(this);
        ll_video.setOnClickListener(this);
        ll_more.setOnClickListener(this);
        ll_fb_post_sharing.setOnClickListener(this);
        img_back.setOnClickListener(this);
        iv_post.setOnClickListener(this);
        ll_file.setOnClickListener(this);
        img_speack.setOnClickListener(this);
        actionEvent = new ActionItem(ID_EVENT, "Event", mContext.getResources().getDrawable(R.drawable.fb_event));
        actionPoll = new ActionItem(ID_POLL, "Poll", mContext.getResources().getDrawable(R.drawable.fb_poll));
        quickAction = new QuickAction(mContext, QuickAction.VERTICAL);
        quickAction.addActionItem(actionEvent);
        quickAction.addActionItem(actionPoll);

        actionPublic = new ActionItem(ID_PUBLIC, "Public", mContext
                .getResources().getDrawable(R.drawable.fb_publics));
//        actionFriend = new ActionItem(ID_FRIEND, "Friend", mContext
//                .getResources().getDrawable(R.drawable.fb_friends));
        actionOnlyMe = new ActionItem(ID_ONLY_ME, "Only Me", mContext
                .getResources().getDrawable(R.drawable.fb_only_me));

        quickActionForShare = new QuickAction(mContext, QuickAction.VERTICAL);

//        quickActionForShare.addActionItem(actionFriend);
        quickActionForShare.addActionItem(actionPublic);
        quickActionForShare.addActionItem(actionOnlyMe);

        // Set listener for action item clicked
        quickActionForShare
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        ActionItem actionItem = quickActionForShare
                                .getActionItem(pos);

//                        if (actionId == ID_FRIEND) {
//
//                            tv_share.setText("Friend");
//                            iv_share.setImageResource(R.drawable.fb_friends);
//
//                        }
                        if (actionId == ID_PUBLIC) {

                            tv_share.setText("Public");
                            iv_share.setImageResource(R.drawable.fb_public);

                        } else if (actionId == ID_ONLY_ME) {

                            tv_share.setText("Only Me");
                            iv_share.setImageResource(R.drawable.fb_only_me);

                        }

                    }

                });

        img_profilepic = (CircleImageView) findViewById(R.id.img_profilepic);

        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(img_profilepic);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        img_facebook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setPackage("com.facebook.katana");
                intent.setType("text/plain");

                intent.putExtra(Intent.EXTRA_TEXT, edt_post.getText().toString());
                intent.putExtra(Intent.EXTRA_SUBJECT, edt_post.getText().toString());
                if (fileType == 0) {
                    intent.setType("image/jpeg"); /* This example is sharing jpeg images. */
                    ArrayList<Uri> files = new ArrayList<Uri>();

                    for (PhotoModel path : Global.AlbumModels /* List of the files you want to send */) {
                        File file = new File(path.getPhoto());
                        Uri uri = Uri.fromFile(file);
                        files.add(uri);
                    }

                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);

                } else if (fileType == 1) {
                    intent.setType("video/*");
                    ArrayList<Uri> files = new ArrayList<Uri>();
                    for (PhotoModel path : Global.AlbumModels /* List of the files you want to send */) {
                        File file = new File(path.getPhoto());
                        Uri uri = Uri.fromFile(file);
                        files.add(uri);
                    }
                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
                } else if (fileType == 2) {
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    ArrayList<Uri> files = new ArrayList<Uri>();

                    for (PhotoModel path : Global.AlbumModels /* List of the files you want to send */) {
                        File file = new File(path.getPhoto());
                        Uri uri = Uri.fromFile(file);
                        files.add(uri);
                    }

                    intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
                    String mimeType = myMime.getMimeTypeFromExtension(fileExt(Global.AlbumModels.get(0).getPhoto().substring(1)));
                    intent.setDataAndType(Uri.fromFile(new File(Global.AlbumModels.get(0).getPhoto())), mimeType);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                }
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Utility.toast(mContext, "No handler for this type of file.");
//                    Toast.makeText(mContext, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                if (from.equalsIgnoreCase(ServiceResource.Wall)) {
                    Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
                    i.putExtra("position", mContext.getResources().getString(R.string.Wall));
                    startActivity(i);
                    overridePendingTransition(0, 0);
                } else if (from.equalsIgnoreCase(PROFILEWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                    ServiceResource.WALLTITLE = "My Wall";
                    intent.putExtra("isGotoWall", false);
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                    ServiceResource.WALLTITLE = "Institute wall";
                    intent.putExtra("isGotoWall", false);
                    mContext.startActivity(intent);
                    ((Activity) mContext).finish();
                } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
                    intent.putExtra("isGotoWall", false);
                    intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                    mContext.startActivity(intent);
                } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                    intent.putExtra("isGotoWall", false);
                    intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                    mContext.startActivity(intent);
                } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                    intent.putExtra("isGotoWall", false);
                    intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                    mContext.startActivity(intent);
                } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                    intent.putExtra("isGotoWall", false);
                    intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                    mContext.startActivity(intent);
                } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                    intent.putExtra("isGotoWall", false);
                    intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                    mContext.startActivity(intent);
                }
                break;

            case R.id.ll_file:

                edt_post.setHint(getResources().getString(R.string.sayabouthisfile));

                // change file picker from filechooserActivity



                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.setType("*/*");
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    //By Hardik Kanak 27-06-2021
                    String[] mimeTypes = {
                            "application/pdf",
                            "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "text/plain"};
                    intent .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    startActivityForResult(Intent.createChooser(intent, "Choose a file"), ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11);
                } else{
//                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                    chooseFile.setType("*/*");
//                    chooseFile = Intent.createChooser(chooseFile, "Choose a file");
//                    startActivityForResult(chooseFile, REQUEST_PATH);


                    //By Hardik Kanak 27-06-2021
                    String[] mimeTypes = {
                            "application/pdf",
                            "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                            "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "text/plain"};

                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    startActivityForResult(intent, REQUEST_PATH);
                }

                /*END*/



                break;

            case R.id.iv_post:

                if (Global.AlbumModels == null
                        || Global.AlbumModels.size() <= 0) {
                    if (!Utility.isNull(edt_post.getText().toString())) {
                        Utility.toast(mContext, getResources().getString(R.string.PleaseAddText));
                    } else {
                        if (isEdit) {
                            if (Utility.isNetworkAvailable(mContext)) {
                                editPost();
                            } else {
                                Utility.showAlertDialog(mContext,
                                        getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                            }

                        } else {

                            addPost();

                        }
                    }
                } else {

                    if (!Utility.isNull(edt_post.getText().toString())) {


                        Utility.toast(AddPostOnWallActivity.this, getResources().getString(R.string.PleaseAddText));

                    } else {

                        addPost();

                    }

                }

                break;

            case R.id.ll_photo:

                edt_post.setHint(getResources().getString(R.string.sayabouthisphoto));

                if (fileType != 1) {

                    selectImage();

                } else {

                    Utility.toast(mContext, getResources().getString(R.string.Sorrybutyoucanuploadeitherimageorvideo));

                }

                break;

            case R.id.ll_video:

                edt_post.setHint(getResources().getString(R.string.sayabouthisvideo));

                if (fileType != 0)

                    if (byteArray == null) {

                        selectVideo();

                    } else {

                        Utility.toast(mContext, getResources().getString(R.string.Onlyonevideosupportedforpostyet));

                    }

                else {

                    Utility.toast(mContext,
                            getResources().getString(R.string.Sorrybutyoucanuploadeitherimageorvideo));
//                    Toast.makeText(mContext,
//                            getResources().getString(R.string.Sorrybutyoucanuploadeitherimageorvideo), Toast.LENGTH_SHORT)
//                            .show();
                }
                break;
            case R.id.ll_link:
                break;
            case R.id.ll_more:
                quickAction.show(v);
                break;

            case R.id.ll_fb_post_sharing:

                if (from.equalsIgnoreCase(ServiceResource.Wall) || from.equalsIgnoreCase(PROFILEWALL)) {

                    quickActionForShare.show(v);
                }

                break;

            case R.id.img_speack:
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {
                    Utility.showToast("Opps! Your device doesn't support Speech to Text", mContext);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.googlequicksearchbox"));
                    startActivity(browserIntent);
                    overridePendingTransition(0, 0);
                }
                break;
            default:
                break;

        }

    }

    public static String ConvertBitmapToString(Bitmap bitmap) {
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        try {
            encodedImage = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.NO_WRAP), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encodedImage;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        ismp3 = false;
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    try {
                        ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        edt_post.setText(edt_post.getText().toString() + "" + text.get(0));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
        }

        if (resultCode == AddPostOnWallActivity.RESULT_OK) {

            selectMultipleImage = false;

            if (requestCode == REQUEST_CAMERA) {

                try {
                    try {

                        Utility.BASE64_STRING = "";
                        Utility.getUserModelData(mContext);

                        try {

                            filePath = data.getExtras().getString("imageData_uri");
                            Path = Utility.ImageCompress(filePath, mContext);
    //                        Utility.GetBASE64STRING(Path);

                            File file = new File(filePath);
                            File compressedImageFile = null;

                            try {
                                compressedImageFile = new Compressor(this).compressToFile(file);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
    //                        Path = Utility.ImageCompress(filePath, mContext);
                            Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


    //                    Utility.BASE64_STRING = "";
    //                    File imageFile = new File(mCurrentPhotoPath);
    //                    Log.d("getabsolutepath", imageFile.getAbsolutePath());
    //                    filePath = imageFile.getAbsolutePath();
    //                    Path = Utility.ImageCompress(imageFile.getAbsolutePath(), mContext);
    //                    Log.d("getFilepath", Path);
    //                    Utility.GetBASE64STRING(Path);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*commented By Krishna -- 06-02-2019 : Get Bytearray From Captured ImagePAth from Camera*/

                    fileType = 0;
                    byteArray = convertImageToByteArra(Path);

                    /*END*/

                    if (Utility.BASE64_STRING != null) {
                        uploadPhoto(fileType, byteArray);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == OPEN_MEDIA_PICKER) {

                try {
                    ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
                    for (int i = 0; i < selectionResult.size(); i++) {
                        Uri uri = Uri.fromFile(new File(selectionResult.get(i)));
                        Log.d("getFileeee", uri.getPath());
                        Bitmap bitmap;
                        try {
                            bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                            FILEIMAGE = ConvertBitmapToString(resizedBitmap);
                            Log.d("getFilebase", FILEIMAGE);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }


    //                    Utility.GetBASE64STRING(uri.getPath());
                        File f = new File(uri.getPath());
                        FILENAME = f.getName();
                        uploadPhoto(fileType, byteArray);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_FROM_GALLERY) {

                try {
                    Uri selectedImage = data.getData();
                    try {
                        mPhotoFile = mCompressor.compressToFile(new File(getRealPathFromUri(selectedImage)));
                        Log.d("getRelPath", mPhotoFile.getAbsolutePath());
                        Log.d("getRelPathname", mPhotoFile.getName());
                        Utility.GetBASE64STRING(mPhotoFile.getPath());
                        if (Utility.BASE64_STRING != null) {
                            Log.d("getnameee", new File(Utility.NewFileName).getName());
                            uploadPhoto(fileType, byteArray);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11) {

                try {
                    //handle Camera CAPTURE Image for ANDROID 11 device
                    if (data != null){
                        fileType = 0;
                        handleCaptureImage_Android11(data);

                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11) {

                try {
                    //handle SELECTED image ANDROID 11 device
                    if (data != null){
                        fileType = 0;
                        handleImageAfterSelection_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11) {
                try {
                    if (data != null && resultCode == RESULT_OK){
                        fileType = 0;
                        handleFileSelected_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_VIDEO_ANDROID_11) {
                try {
                    if (data != null && resultCode == RESULT_OK){
                        fileType = 0;
                        handleVideoSelected_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == ImageAndroid11.REQUEST_VIDEO_CAPTURE_ANDROID_11) {
                try {
                    if (data != null ){
                        fileType = 0;
                        handleCaptureVideo_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_FILE) {

                try {
                    if (data != null) {

                        fileType = -1;
                        selectMultipleImage = true;
                        ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                        Log.d("selectfilelist", imgList.toString());
                        imagePos = 0;
                        filePathArray = new String[imgList.size()];

                        for (int k = 0; k < imgList.size(); k++) {

                            Bitmap thePic;
                            filePath = imgList.get(k).getUri().toString();
                            filePathArray[k] = filePath;
                            thePic = Utility.getBitmap(filePath, mContext);
                            byteArray = null;
                            fileType = 0;
                            Log.d("selectfilepathhh", filePath);

                            byteArray = convertImageToByteArra(filePath);

                            File file = new File(filePath);
                            File compressedImageFile = null;
                            try {
                                compressedImageFile = new Compressor(this).compressToFile(file);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                            Path = Utility.ImageCompress(filePath, mContext);
                            Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

                            // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
    //                        Path = Utility.ImageCompress(filePath, mContext);
    //                        Log.d("selectfilepathhh", Path);
    //                        Utility.GetBASE64STRING(Path);
                            /*END*/

                            if (Utility.BASE64_STRING != null) {
                                uploadPhoto(fileType, byteArray);
                            }

                            byteArray = null;

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_CAMERA_VIDEO) {

                try {
                    byteArray = null;
                    fileType = 1;
                    filePath = getRealPathFromVideoUri(this, data.getData());
                    Log.d("videofilepath", filePath);
                    byteArray = convertVideoToByteArra(filePath);
                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.GetBASE64STRINGFROMFILE(filePath);
                    Log.d("getCamerabase64", Utility.BASE64_STRING);
                    /*END*/
                    if (Utility.BASE64_STRING != null)
                        uploadPhoto(fileType, filePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == SELECT_FILE_VIDEO) {

                try {
                    byteArray = null;
                    fileType = 1;

                    filePath = getRealPathFromVideoUri(this, data.getData());

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.GetBASE64STRINGFROMFILE(filePath);
                    Log.d("getCamerabase64", Utility.BASE64_STRING);
                    /*END*/

                    /* commented by Krishna : 04-02-2019 Get Selected Video File Size*/

                    File file = new File(filePath);
                    final double length = file.length();

                    if (length > MB) {
                        file_size = format.format(length / MB) + " MB";
                    }

                    /*END*/

                    if (Double.valueOf(format.format(length / MB)) > 20) {

                        Utility.toast(mContext, "Upload Video with Maximum Size of 20 MB");

                    } else {

    //                    byteArray = convertVideoToByteArra(filePath);

                        if (Utility.BASE64_STRING != null)

                            uploadPhoto(fileType, filePath);

                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }

        }


        /*22-01-2019 --- commented by krishna : Get Cropped Image Result here and convert bitmap to byte array*/

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            try {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {

                    img_file.setVisibility(View.VISIBLE);
                    img_file.setImageURI(result.getUri());


                    try {

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(AddPostOnWallActivity.this.getContentResolver(), result.getUri());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
    //                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    //                        photoArray = stream.toByteArray();

                        mCapturedImageURI = Utility.getMediaFileUri(1, AddPostOnWallActivity.this);


                    } catch (IOException e) {

                        e.printStackTrace();

                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                    Utility.toast(mContext, getString(R.string.valid_profile_pic));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        /*END*/


        if (requestCode == REQUEST_PATH) {

            if (resultCode == RESULT_OK) {

                try {
                    fileUri = data.getData();
                    filePath = fileUri.getPath();

                    System.out.println(fileUri.toString());

                    String selectdpath = FileUtils.getPath(AddPostOnWallActivity.this, fileUri);
                    Log.i("newImageFilePath", "" + selectdpath);

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.convertToBase64String(mContext, fileUri);
                    /*END*/

                    File f = new File("" + fileUri);

                    curFileName = new File(selectdpath).getName();
                    Log.d("getcurclfilename", curFileName + " " + fileUri.toString());
                    Log.d("getfilename", filePath + " " + curFileName + " " + f.getName());

                    /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                    File file = new File(curFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    /*END*/


                    fileType = 2;


//                if (curFileName.contains("pdf")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("txt")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("doc")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("mp3")) {
//                    FileMineType = mimeType;
//                    fileType = 1;
//                    ismp3 = true;
//                } else if (curFileName.contains("wav")) {
//                    FileMineType = mimeType;
//                    fileType = 1;
//                    ismp3 = true;
//                } else {
//                    FileMineType = mimeType;
//                }

                    String selectedFileExtension = FileUtils.getExtension(curFileName);

                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }

                    Log.d("filepathhhhhh", filePath);
//                // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                Utility.GetBASE64STRINGFROMFILE(BitmapUtil.getRealPathFromURI(filePath, mContext));
//                /*END*/

                    /* commented by Krishna : 04-02-2019 Get Selected Video File Size*/

                    File file2 = new File(selectdpath);
                    final double length = file2.length();

                    if (length > MB) {

                        file_size = format.format(length / MB) + " MB";
                        Log.d("getFilesize", file_size);

                    }

                    if (Double.valueOf(format.format(length / MB)) > 20) {
                        Utility.toast(mContext, "Upload File with Maximum Size of 5 MB");
                    } else {
                        if (Utility.BASE64_STRING != null)
                            uploadPhoto(fileType, selectdpath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private void handleVideoSelected_Android11(Intent data) {

        try {
            if (data.getData() != null){
                Uri uri = data.getData();
                byteArray = null;
                fileType = 1;

    //            filePath = getRealPathFromVideoUri(AddPostOnWallActivity.this,uri);
                Log.e("PATH","fileUri:: "+uri);

                filePath = ImageAndroid11.getPathFromFileURL(this,uri);

                Log.e("PATH","filePath:: "+filePath);

                File file = new File(filePath);
                final double length = file.length();

                if (length > MB) {
                    file_size = format.format(length / MB) + " MB";
                }

                if (Double.valueOf(format.format(length / MB)) > 20) {

                    //Utility.toast(mContext, "Upload Video with Maximum Size of 20 MB");

                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();

                } else {

                    Utility.GetBASE64STRINGFROMFILE(filePath);
                    Log.d("getCamerabase64", Utility.BASE64_STRING);

                    if (Utility.BASE64_STRING != null)

                        uploadPhoto(fileType, filePath);

                }
            }
        } catch (Exception e) {
            Log.e("PATH","fileUri:: "+e.toString());
        }

    }

    private void handleCaptureVideo_Android11(Intent data) {

        try {
            if (data.getData() != null){
                Uri uri = data.getData();
                byteArray = null;
                fileType = 1;
                filePath = getRealPathFromVideoUri(AddPostOnWallActivity.this,uri);
                Log.e("PATH","path:: "+filePath);

                File file = new File(filePath);
                final double length = file.length();

                if (length > MB) {
                    file_size = format.format(length / MB) + " MB";
                }


                if (Double.valueOf(format.format(length / MB)) > 20) {

                    //Utility.toast(mContext, "Upload Video with Maximum Size of 20 MB");

                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();

                } else {

                    byteArray = convertVideoToByteArra(filePath);
                    Utility.GetBASE64STRINGFROMFILE(filePath);

                    Log.e("PATH","getCamerabase64:: "+Utility.BASE64_STRING);


                    if (Utility.BASE64_STRING != null){
                        uploadPhoto(fileType, filePath);
                    }


                }


            }
        } catch (NumberFormatException e) {
            Log.e("PATH","fileUri:: "+e.toString());
        } catch (Exception e) {
            Log.e("PATH","fileUri:: "+e.toString());
        }

    }

    private void handleCaptureImage_Android11(Intent data) {


        try {
            selectMultipleImage = false;
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String path = ImageAndroid11.getPathFromBitmap(this,imageBitmap);

            //Must apply base 64 string function before upload photo
            if (path != null){
                Log.e("PATH","PATH:: "+path);
                Path = path;

                Utility.BASE64_STRING = "";
                Utility.getUserModelData(mContext);


                Utility.GetBASE64STRING(path);

                fileType = 0;
               // byteArray = convertImageToByteArra(path);

                if (Utility.BASE64_STRING != null) {
                    filePath = path;
                    uploadPhotoAndroid11(fileType, byteArray,path);
                }
            } else  {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }

    private void handleImageAfterSelection_Android11(Intent data) {

        try {

            if (data.getData() != null){

                selectMultipleImage = false;

                try {
                    Uri selectedImage = data.getData();
                    String path = getPathFromURI(this,selectedImage,"image_tmp.jpg");
                    Log.d("PATH","PATH:: "+path);

                    //Must apply base 64 string function before upload photo
                    if (path != null){
                        Path = path;
                        Utility.BASE64_STRING = "";
                        Utility.getUserModelData(mContext);


                        Utility.GetBASE64STRING(path);

                        fileType = 0;
                        // byteArray = convertImageToByteArra(path);

                        if (Utility.BASE64_STRING != null) {
                            filePath = path;
                            uploadPhotoAndroid11(fileType, byteArray,path);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }


            } else  {
                if (data.getClipData() != null) {
                    try {
                        selectMultipleImage = true;
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                        }

                        filePathArray = new String[mArrayUri.size()];
                        for (int i = 0; i < mArrayUri.size(); i++) {
                            String path = getPathFromURI(this,mArrayUri.get(i),"image_tmp_"+i+".jpg");
                            Log.d("PATH","PATH:: "+path);

                            //Must apply base 64 string function before upload photo
                            if (path != null){
                                Path = path;
                                Utility.BASE64_STRING = "";
                                Utility.getUserModelData(mContext);


                                Utility.GetBASE64STRING(path);

                                fileType = 0;
                                // byteArray = convertImageToByteArra(path);

                                if (Utility.BASE64_STRING != null) {
                                    filePath = path;
                                    filePathArray[i] = filePath;
                                    uploadPhotoAndroid11(fileType, byteArray,path);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                }
            }

        } catch (NumberFormatException e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }

    private void handleFileSelected_Android11(Intent data) {

        if (data.getData() != null) {

            try {
                Uri uri = data.getData();

                Log.e("PATH","data:: "+data.toString());
                Log.e("PATH","fileUri:: "+uri);

                String selectdpath = ImageAndroid11.getPathFromFileURL(this,uri);


                File file2 = new File(selectdpath);
                final double length = file2.length();

                if (length > MB) {

                    file_size = format.format(length / MB) + " MB";
                    Log.d("PATH", "SIZE:: "+file_size);

                }

                if (Double.valueOf(format.format(length / MB)) > 20) {

                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else  {
                    fileUri = uri;
                    filePath = selectdpath;

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.convertToBase64String(mContext, fileUri);
                    /*END*/

                    File f = new File("" + fileUri);

                    curFileName = new File(selectdpath).getName();

                    Log.e("PATH","getcurclfilename:: "+curFileName + " " + fileUri.toString());
                    Log.e("PATH","getfilename:: "+filePath + " " + curFileName + " " + f.getName());

                    /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                    File file = new File(curFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    /*END*/


                    fileType = 2;

//                    if (curFileName.contains("pdf")) {
//                        FileMineType = mimeType;
//                    } else if (curFileName.contains("txt")) {
//                        FileMineType = mimeType;
//                    } else if (curFileName.contains("doc")) {
//                        FileMineType = mimeType;
//                    } else if (curFileName.contains("mp3")) {
//                        FileMineType = mimeType;
//                        fileType = 1;
//                        ismp3 = true;
//                    } else if (curFileName.contains("wav")) {
//                        FileMineType = mimeType;
//                        fileType = 1;
//                        ismp3 = true;
//                    } else {
//                        FileMineType = mimeType;
//                    }

                    String selectedFileExtension = FileUtils.getExtension(curFileName);

                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }


                    Log.e("PATH","filepathhhhhh:: "+filePath);

                    if (Utility.BASE64_STRING != null){
                        uploadPhoto(fileType, selectdpath);
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }
        }

    }

    /**
     * get byte array from file path
     *
     * @param filePath
     * @return bytes array of image
     */
    public byte[] convertImageToByteArra(String filePath) {

        byte[] b = null;
        Bitmap thePic;
        thePic = Utility.getBitmap(filePath, mContext);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (filePath.contains(".png") || filePath.contains(".PNG")) {
            thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
        } else {
            thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }

        b = null;
        b = stream.toByteArray();
        return b;

    }

    private boolean isPermissionGrantedForCamera() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    /**
     * get btte array of video from file path
     *
     * @param filePath
     * @return byte array of video
     */
    public byte[] convertVideoToByteArra(String filePath) {

        File file = new File(filePath);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        return bytes;

    }

    /**
     * it return content path to real path of sdcard
     *
     * @param activity
     * @param contentUri content path
     * @return real path path
     */
    public String getRealPathFromVideoUri(Activity activity, Uri contentUri) {

        String[] filePath = {MediaStore.Video.Media.DATA};
        Cursor c = getContentResolver().query(contentUri, filePath,
                null, null, null);
        c.moveToFirst();
        int columnIndex = c.getColumnIndex(filePath[0]);
        String videoPath = c.getString(columnIndex);
        c.close();
        return videoPath;


    }

    /**
     * take image from camera or gallary
     */
    private void selectImage() {


        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {

//                    Intent intent = new Intent(mContext, PreviewImageActivity.class);
//                    intent.putExtra("FromIntent", "true");
//                    intent.putExtra("RequestCode", 100);
//                    startActivityForResult(intent, REQUEST_CAMERA);

                    /*commented By Hardik : 29-04-2019 - Android 11 image capture*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        try {
                            startActivityForResult(takePictureIntent, ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11);
                        } catch (ActivityNotFoundException e) {
                            Log.e("PATH","PATH:: "+e.toString());
                        }
                    } else{
                        Intent intent = new Intent(AddPostOnWallActivity.this, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

//                    if (isPermissionGrantedForCamera()) {
//                        OpenCameraApp();
//                    }

//                    dialog.dismiss();
//                    Intent intent = new Intent(mContext, PreviewImageActivity.class);
//                    intent.putExtra("FromIntent", "true");
//                    intent.putExtra("RequestCode", 100);
//                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {
                    dialog.dismiss();
                    if (isPermissionGrantedForGallery()) {

//                        Intent intent = new Intent(AddPostOnWallActivity.this, Gallery.class);
//                        intent.putExtra("title", "Select Media");
//                        intent.putExtra("mode", 1);
//                        intent.putExtra("maxSelection", 5);
//                        startActivityForResult(intent, OPEN_MEDIA_PICKER);

//                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        startActivityForResult(pickPhoto, PICK_FROM_GALLERY);

//                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                        startActivityForResult(galleryIntent, PICK_FROM_GALLERY);

                    }
//                    Intent intent = new Intent(AddPostOnWallActivity.this, ImageSelectionActivity.class);
//                    intent.putExtra("count", 10);
//                    startActivityForResult(intent, SELECT_FILE);

                    /*commented By Hardik : 29-04-2019 - Android 11 image selection*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11);
                    } else{
                        Intent intent = new Intent(AddPostOnWallActivity.this, ImageSelectionActivity.class);
                        intent.putExtra("count", 5);
                        startActivityForResult(intent, SELECT_FILE);
                    }

                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void OpenCameraApp() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utility.createImageFile(AddPostOnWallActivity.this);
                mCurrentPhotoPath = Utility.mCurrentPhotoPath;
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(AddPostOnWallActivity.this,
                        FILE_PROVIDER,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    public boolean isPermissionGrantedForGallery() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {
                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case READ_EXTERNAL_REQUEST_CODE_FOR_GALLERY: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    // change file picker from filechooserActivity
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                    /*END*/
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case READ_EXTERNAL_REQUEST_CODE_FOR_CAMERA: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    // change file picker from filechooserActivity
                    OpenCameraApp();
                    /*END*/
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }

            }

        }

    }

    /**
     * select video from gallary
     */
    private void selectVideo() {

//        final CharSequence[] items = {"Take Video", "Choose from Library",
//                "Cancel"};

        /*commented By Krishna : 01-05-2019 - Solved the language issue*/

        final CharSequence[] items = {mContext.getResources().getString(R.string.capturevideo),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};

        /*END*/


        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.addvideo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.capturevideo))) {



                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                        try {
                            startActivityForResult(intent, ImageAndroid11.REQUEST_VIDEO_CAPTURE_ANDROID_11);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                        } catch (ActivityNotFoundException e) {
                            Log.e("PATH","PATH:: "+e.toString());
                        }
                    } else{
                        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 20);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                        startActivityForResult(intent, REQUEST_CAMERA_VIDEO);
                    }



                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {



                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.setType("*/*");
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                        String[] mimeTypes = {"video/*"};
                        intent .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(Intent.createChooser(intent, "Choose a Video"), ImageAndroid11.REQUEST_PICK_VIDEO_ANDROID_11);
                    } else{
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("video/*");
//                    long maxVideoSize = 5 * 1024 * 1024; // 10 MB
//                    intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, maxVideoSize);
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_FILE_VIDEO);
                    }



                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {

                    dialog.dismiss();

                }


            }


        });


        builder.show();


    }

    /**
     * delete image  from position
     * remove Image from grid
     */

    @Override
    public void DeleteItem(final int position) {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, false);


        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog
                .findViewById(R.id.ll_submit);
        TextView tv_say_something = (TextView) mPoweroffDialog
                .findViewById(R.id.tv_say_something);

        tv_say_something.setText(getResources().getString(R.string.Areyousureyouwanttodelete));

        TextView tv_header = (TextView) mPoweroffDialog
                .findViewById(R.id.tv_header);

        tv_header.setText(getResources().getString(R.string.Delete));

        ll_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Global.AlbumModels.remove(position);

                if (Global.AlbumModels.size() == 0) {

                    byteArray = null;
                    fileType = -1;
                    filePath = "";

                }

                photo_ListAdapter.notifyDataSetChanged();
                mPoweroffDialog.dismiss();

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog.dismiss();

            }

        });

    }

    /**
     * call Webservice for Add Post on wall
     */
    public void addPost() {

        Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();

        Utility.getUserModelData(mContext);
        ContentValues values = new ContentValues();

        values.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        if ((from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL))) {
            values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId());
        } else {
            values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId());
        }

        values.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.BEATCH_ID, "");
        values.put(ServiceResource.POST_POSTSHARETYPE, tv_share.getText().toString());
        values.put(ServiceResource.POST_POSTCOMMENTNOTE, edt_post.getText().toString());
        values.put(ServiceResource.POST_POSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType());

        String imgPath1 = "";
        if (Global.AlbumModels != null && Global.AlbumModels.size() > 0) {

            for (int i = 0; i < Global.AlbumModels.size(); i++) {
                Log.d("getfilenamefromalbum", Global.AlbumModels.get(i).getNew_name());
                if (Global.AlbumModels.size() - 1 == i) {
                    imgPath1 = imgPath1 + Global.AlbumModels.get(i).getNew_name();
                } else {
                    imgPath1 = imgPath1 + Global.AlbumModels.get(i).getNew_name() + ",";
                }
            }

        }

        values.put(ServiceResource.POST_IMAGEPATH, imgPath1);
//        if (Global.DynamicWallSetting == null) {
//            Global.DynamicWallSetting = new DynamicWallSetting();
//        }
        if (fileType == 0) {
            values.put(ServiceResource.POST_FILETYPE, "IMAGE");
            values.put(ServiceResource.POST_FILEMINETYPE, "IMAGE");
            values.put(ServiceResource.POST_APPROVED, true);
        } else if (fileType == 1) {
            values.put(ServiceResource.POST_FILETYPE, "VIDEO");
            values.put(ServiceResource.POST_FILEMINETYPE, "VIDEO");
            values.put(ServiceResource.POST_APPROVED, true);
        } else if (fileType == 2) {
            values.put(ServiceResource.POST_FILETYPE, "FILE");
            values.put(ServiceResource.POST_FILEMINETYPE, FileMineType);
            values.put(ServiceResource.POST_APPROVED, true);
        } else {
            values.put(ServiceResource.POST_FILEMINETYPE, FileMineType);
            values.put(ServiceResource.POST_APPROVED, true);
        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        if ((from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL))) {
            arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId()));
        }
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTSHARETYPE, tv_share.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTCOMMENTNOTE, edt_post.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.POST_POSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));
        String imgPath = "";
        if (Global.AlbumModels != null && Global.AlbumModels.size() > 0) {
            for (int i = 0; i < Global.AlbumModels.size(); i++) {
                if (Global.AlbumModels.size() - 1 == i) {
                    imgPath = imgPath + Global.AlbumModels.get(i).getNew_name();
                } else {
                    imgPath = imgPath + Global.AlbumModels.get(i).getNew_name() + ",";
                }
            }
        }

        arrayList.add(new PropertyVo(ServiceResource.POST_IMAGEPATH, imgPath));

//        if (Global.DynamicWallSetting == null) {
//            Global.DynamicWallSetting = new DynamicWallSetting();
//        }
        if (fileType == 0) {
            arrayList.add(new PropertyVo(ServiceResource.POST_FILETYPE, "IMAGE"));
            arrayList.add(new PropertyVo(ServiceResource.POST_FILEMINETYPE, "IMAGE"));
            arrayList.add(new PropertyVo(ServiceResource.POST_APPROVED, true));
        } else if (fileType == 1) {
            arrayList.add(new PropertyVo(ServiceResource.POST_FILETYPE, "VIDEO"));
            arrayList.add(new PropertyVo(ServiceResource.POST_FILEMINETYPE, "VIDEO"));
            arrayList.add(new PropertyVo(ServiceResource.POST_APPROVED, true));
        } else if (fileType == 2) {
            arrayList.add(new PropertyVo(ServiceResource.POST_FILETYPE, "FILE"));
            arrayList.add(new PropertyVo(ServiceResource.POST_FILEMINETYPE, FileMineType));
            arrayList.add(new PropertyVo(ServiceResource.POST_APPROVED, true));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.POST_FILEMINETYPE, FileMineType));
            arrayList.add(new PropertyVo(ServiceResource.POST_APPROVED, true));
        }

        Log.d("postRequest", arrayList.toString());
        if (Utility.isNetworkAvailable(mContext)) {

            new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.POST_METHODNAME, ServiceResource.POST_URL);

        } else {

            /*commented By krishna : 05-03-2019 Remove Redirection of Each Wall Even if No Internet Connection and Add Message of No Internet Available*/

            Utility.showAlertDialog(mContext,
                    getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            /*END*/

        }

    }

    /**
     * call Webservice for Edit Post on wall
     */

    public void editPost() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.POSTID, postModel.getPostCommentID()));
        arrayList.add(new PropertyVo(ServiceResource.POSTCOMMENTNOTE, edt_post.getText().toString()));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(
                ServiceResource.EDITPOSTDETAILS_METHODNAME,
                ServiceResource.WALL_URL);

    }

    /**
     * @param args          filetype image or video,pdf,doc
     * @param filepathparam
     */

    public void uploadPhoto(int args, String filepathparam) {

        ContentValues values = new ContentValues();
        values.put(ServiceResource.PHOTO_FILENAME, new File(filepathparam).getName());
        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();
        }

        values.put(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

        if (args == 0) {
            values.put(ServiceResource.PHOTO_FILETYPE, "IMAGE");
        } else if (args == 1) {
            values.put(ServiceResource.PHOTO_FILETYPE, "VIDEO");
        } else if (args == 2) {
            values.put(ServiceResource.PHOTO_FILETYPE, "FILE");
        }

        values.put(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME, new File(filepathparam).getName().replace(" ", "-")));

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();

        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "IMAGE"));
        } else if (args == 1) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "VIDEO"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        if (Utility.BASE64_STRING != null)

            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE, Utility.BASE64_STRING));

        Log.d("videorequest", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this, true, false).execute(
                    ServiceResource.UPLOAD_PHOTO_METHODNAME,
                    ServiceResource.ADDPHOTO_URL);
        } else {
            uploadpicResponse("");
        }


    }

    /**
     * @param args       filetype image or video,pdf,doc
     * @param byteArray1 byte array of
     */
    public void uploadPhoto(int args, byte[] byteArray1) {
        Log.d("getnameee", new File(Utility.NewFileName).getName());
        ContentValues values = new ContentValues();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();

        }

        values.put(ServiceResource.PHOTO_FILENAME, new File(Utility.NewFileName).getName());
        values.put(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

        if (args == 0) {
            values.put(ServiceResource.PHOTO_FILETYPE, "IMAGE");
        } else if (args == 1) {
            values.put(ServiceResource.PHOTO_FILETYPE, "VIDEO");
        } else if (args == 2) {
            values.put(ServiceResource.PHOTO_FILETYPE, "FILE");
        }

        values.put(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME, new File(Utility.NewFileName).getName().replace(" ", "-")));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "IMAGE"));
        } else if (args == 1) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "VIDEO"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        if (Utility.BASE64_STRING != null) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE, Utility.BASE64_STRING));
        }

        Log.d("photoreq", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this, true, false).execute(
                    ServiceResource.UPLOAD_PHOTO_METHODNAME,
                    ServiceResource.ADDPHOTO_URL);
        } else {
            uploadpicResponse("");
        }

    }


    public void uploadPhotoAndroid11(int args, byte[] byteArray1,String filepathparam) {
        Log.d("getnameee", new File(filepathparam).getName());
        ContentValues values = new ContentValues();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();

        }

        values.put(ServiceResource.PHOTO_FILENAME,  new File(filepathparam).getName());
        values.put(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

        if (args == 0) {
            values.put(ServiceResource.PHOTO_FILETYPE, "IMAGE");
        } else if (args == 1) {
            values.put(ServiceResource.PHOTO_FILETYPE, "VIDEO");
        } else if (args == 2) {
            values.put(ServiceResource.PHOTO_FILETYPE, "FILE");
        }

        values.put(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,  new File(filepathparam).getName()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PHOTO_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "IMAGE"));
        } else if (args == 1) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "VIDEO"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILETYPE, "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        if (Utility.BASE64_STRING != null) {
            arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE, Utility.BASE64_STRING));
        }

        Log.d("photoreq", arrayList.toString());

        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this, true, false).execute(
                    ServiceResource.UPLOAD_PHOTO_METHODNAME,
                    ServiceResource.ADDPHOTO_URL);
        } else {
            uploadpicResponse("");
        }

    }


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            if (from.equalsIgnoreCase(ServiceResource.Wall)) {
                Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
                i.putExtra("position", mContext.getResources().getString(R.string.Wall));
                startActivity(i);
                overridePendingTransition(0, 0);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(PROFILEWALL)) {
                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                ServiceResource.WALLTITLE = "My Wall";
                intent.putExtra("isGotoWall", false);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                ServiceResource.WALLTITLE = "Institute wall";
                intent.putExtra("isGotoWall", false);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
                intent.putExtra("isGotoWall", false);
                intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                intent.putExtra("isGotoWall", false);
                intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                intent.putExtra("isGotoWall", false);
                intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
//                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                intent.putExtra("isGotoWall", false);
                intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
//                ServiceResource.FROM_SELECTED_WALL = "";
                Intent intent = new Intent(mContext, WallActivity.class);
                intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                intent.putExtra("isGotoWall", false);
                intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
                mContext.startActivity(intent);
                ((Activity) mContext).finish();
            }

        }

        if (ServiceResource.POST_METHODNAME.equalsIgnoreCase(methodName)) {

//            sendNotification();
            sendPushNotification();

        }

        if (ServiceResource.EDITPOSTDETAILS_METHODNAME.equalsIgnoreCase(methodName)) {

            sendPushNotification();
//            finish();

        }

        if (ServiceResource.UPLOAD_PHOTO_METHODNAME.equalsIgnoreCase(methodName)) {
            Log.d("getResultfile", result);
            if (result.contains("[{\"message\":\"Path file name must be less than 260 characters\",\"success\":0}]")) {
                Utility.Longtoast(mContext, getResources().getString(R.string.filevalidationmsg));
            } else {
                uploadpicResponse(result);
            }


        }

    }

    String getPathFromURI(Context context, Uri selectedImage, String newFileName) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath());
            folder.mkdirs();
           // File file = new File(folder, "image_tmp.jpg");
            File file = new File(folder, newFileName);
            if (file.exists())
                file.delete();
            file.createNewFile();

            int maxBufferSize = 1024 * 1024;

            try {
                InputStream inputStream = context.getContentResolver().openInputStream(selectedImage);
                Log.e("InputStream Size","Size " + inputStream);
                int  bytesAvailable = inputStream.available();
                int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                final byte[] buffers = new byte[bufferSize];

                FileOutputStream outputStream = new FileOutputStream(file);
                int read = 0;
                while ((read = inputStream.read(buffers)) != -1) {
                    outputStream.write(buffers, 0, read);
                }
                inputStream.close();
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

//    private void sendNotification() {
//
//        WebServiceCaller.ApiInterface service = WebServiceCaller.getClient();
//        Call<StoreListRequstResponse> call = service.storeList("1", "10");
//        call.enqueue(new Callback<StoreListRequstResponse>() {
//            @Override
//            public void onResponse(Call<StoreListRequstResponse> call, Response<StoreListRequstResponse> response) {
//                if (response.isSuccessful()) {
//                    if (Integer.parseInt(response.body().getSuccess()) == ResponseType.SUCCESS.value) {
//                        if (response.body().getRes().size() > 0) {
//                            storeList.addAll(response.body().getRes());
//                            StoreAddressListAdapter storeAddressListAdapter = new StoreAddressListAdapter(StoreListActivity.this, storeList, StoreListActivity.this);
//                            rv_store_list.setAdapter(storeAddressListAdapter);
//                        }
//                    }
//                }
//
//                Utility.hideProgress();
//
//            }
//
//            @Override
//            public void onFailure(Call<StoreListRequstResponse> call, Throwable t) {
//                Utility.log("" + t.getMessage());
//                Utility.showError(getResources().getString(R.string.internal_server_error));
//                hideProgress();
//
//            }

//        });

//    }


    private void uploadpicResponse(String result) {

        PhotoModel photoModel = new PhotoModel();
        String message = " ";

        if (result != null && !result.isEmpty()) {

            try {
                JSONArray obj = new JSONArray(result);
                for (int i = 0; i < obj.length(); i++) {
                    JSONObject obj1 = obj.getJSONObject(i);
                    message = obj1.getString(ServiceResource.MESSAGE);
                    Log.d("getNewName12", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        String[] newName = message.split(" ");
        photoModel.setNew_name(newName.toString());
        Log.d("getNewName12345", photoModel.getNew_name());

        if (newName.length >= 1) {
            if (newName[1] != null) {
                photoModel.setNew_name(newName[1]);
                Log.d("getNewName", newName[1]);
            }
        }

        if (selectMultipleImage) {
            photoModel.setPhoto(filePathArray[imagePos]);
            if (imagePos < filePathArray.length) {
                photoModel.setPhoto(filePathArray[imagePos]);
            }
            imagePos++;
        } else {
            photoModel.setPhoto(filePath);
        }

        photoModel.setAlbumID(String.valueOf(fileType));
        Global.AlbumModels.add(photoModel);

        if (ismp3) {

            lv_multipleImg.setVisibility(View.INVISIBLE);
            img_file.setVisibility(View.VISIBLE);
            img_file.setImageResource(R.drawable.mpicon);

        } else if (fileType != 2) {

            photo_ListAdapter = new PhotoListAdapter(mContext, Global.AlbumModels, 1, this);
            lv_multipleImg.setAdapter(photo_ListAdapter);
            lv_multipleImg.setVisibility(View.VISIBLE);

        } else {

            try {
                JSONArray jsonArr = new JSONArray(result);

                String msg = "";
                JSONObject json_data = new JSONObject();
                for (int i = 0; i < jsonArr.length(); i++) {
                    json_data = jsonArr.getJSONObject(i);

                    msg = json_data.getString("message");

                }

                lv_multipleImg.setVisibility(View.INVISIBLE);
                img_file.setVisibility(View.VISIBLE);

                String selectedFileExtension = FileUtils.getExtension(msg);

                Log.d("uploadphotoresult", selectedFileExtension);


                if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                    img_file.setImageResource(R.drawable.pdf);
                } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                    img_file.setImageResource(R.drawable.txt);
                } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                    img_file.setImageResource(R.drawable.doc);
                } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                    img_file.setImageResource(R.drawable.excel_file);
                } else {
                    showToast(getResources().getString(R.string.unknown_extension), this);
                }

//            if (FileMineType.equalsIgnoreCase("text/plain")) {
//                img_file.setImageResource(R.drawable.txt);
//            } else if (FileMineType.equalsIgnoreCase("application/msword")) {
//                img_file.setImageResource(R.drawable.doc);
//            } else if (FileMineType.equalsIgnoreCase("application/doc")) {
//                img_file.setImageResource(R.drawable.doc);
//            } else if (FileMineType.equalsIgnoreCase("application/pdf")) {
//                img_file.setImageResource(R.drawable.pdf);
//            } else if (FileMineType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
//                img_file.setImageResource(R.drawable.excel_file);
//            } else if (FileMineType.equalsIgnoreCase("application/vnd.openxmlform")) {
//                img_file.setImageResource(R.drawable.excel_file);
//            }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        byteArray = null;

    }


    public void sendPushNotification() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SENDNOTIFICATION_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

    void handleSendText(Intent intent) {

        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
            if (edt_post == null) {
                edt_post = (EditText) findViewById(R.id.edt_post);
            }
            edt_post.setText(sharedText);
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            filePathArray = new String[1];
            Uri selectedImageUri = imageUri; // data.getData();
            String[] projection = {MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(mContext, selectedImageUri, projection, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            Bitmap thePic;
            filePath = selectedImagePath;
            filePathArray[0] = filePath;
            thePic = Utility.getBitmap(selectedImagePath, mContext);
            byteArray = null;
            fileType = 0;
            byteArray = convertImageToByteArra(filePath);
            if (byteArray != null) {
                uploadPhoto(fileType, byteArray);
            }
        }
    }

    void handleSendMultipleImages(Intent intent) {

        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            selectMultipleImage = true;
            filePathArray = new String[imageUris.size()];
            for (int k = 0; k < imageUris.size(); k++) {
                Bitmap thePic;
                filePath = imageUris.get(k).toString();
                filePathArray[k] = filePath;
                thePic = Utility.getBitmap(filePath, mContext);
                byteArray = null;
                fileType = 0;
                byteArray = convertImageToByteArra(filePath);
                if (byteArray != null) {
                    uploadPhoto(fileType, byteArray);
                }
            }
        }
    }

    void handleSendFile(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            if (imageUri.toString().contains(".pdf") ||
                    imageUri.toString().contains(".doc") ||
                    imageUri.toString().contains(".docx") ||
                    imageUri.toString().contains(".txt") ||
                    imageUri.toString().contains(".mp3")
            ) {
                String path = imageUri.getPath();
                fileType = 2;


                /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                File file = new File(path);
                Uri selectedUri = Uri.fromFile(file);
                String fileExtension
                        = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                String mimeType
                        = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                /*END*/


                if (imageUri.toString().contains("pdf")) {
                    FileMineType = mimeType;
                } else if (imageUri.toString().contains("txt")) {
                    FileMineType = mimeType;
                } else if (imageUri.toString().contains("doc")) {
                    FileMineType = mimeType;
                } else {
                    FileMineType = mimeType;
                }
                byteArray = convertVideoToByteArra(path);
                filePath = path;
                if (byteArray != null)
                    uploadPhoto(fileType, filePath);
            } else {
                Utility.toast(mContext, getResources().getString(R.string.erromsg));
            }
        }
    }

    void handleVideoFile(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            byteArray = null;
            fileType = 1;
            filePath = getRealPathFromVideoUri(this, imageUri);
            byteArray = convertVideoToByteArra(filePath);
            if (byteArray != null)
                uploadPhoto(fileType, filePath);
        }

    }


    private LinkPreviewCallback callback = new LinkPreviewCallback() {
        private View mainView;
        private LinearLayout linearLayout;
        private View loading;
        private ImageView imageView;
        LinearLayout drop_preview1;

        @Override
        public void onPre(View v) {

            hideSoftKeyboard();
            currentImageSet = null;
            currentItem = 0;
            drop_preview1 = (LinearLayout) v;
            currentImage = null;
            noThumb = false;
            currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";
            mainView = getLayoutInflater().inflate(R.layout.main_view, null);
            linearLayout = (LinearLayout) mainView.findViewById(R.id.external);
            loading = getLayoutInflater().inflate(R.layout.loading, linearLayout);
            drop_preview1.addView(mainView);
            drop_preview1.setVisibility(View.VISIBLE);

        }


        @Override
        public void onPos(final SourceContent sourceContent, boolean isNull) {

            /** Removing the loading layout */
            linearLayout.removeAllViews();

            if (isNull || sourceContent.getFinalUrl().equals("")) {

                View failed = getLayoutInflater().inflate(R.layout.failed, linearLayout);
                TextView titleTextView = (TextView) failed.findViewById(R.id.text);
                titleTextView.setText(getString(R.string.failed_preview) + "\n" + sourceContent.getFinalUrl());
                failed.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        drop_preview1.setVisibility(View.GONE);
                    }
                });
            } else {
                currentImageSet = new Bitmap[sourceContent.getImages().size()];
                final View content = getLayoutInflater().inflate(R.layout.preview_content, linearLayout);
                final LinearLayout infoWrap = (LinearLayout) content.findViewById(R.id.info_wrap);
                final LinearLayout titleWrap = (LinearLayout) infoWrap.findViewById(R.id.title_wrap);
                final LinearLayout thumbnailOptions = (LinearLayout) content.findViewById(R.id.thumbnail_options);
                final LinearLayout noThumbnailOptions = (LinearLayout) content.findViewById(R.id.no_thumbnail_options);
                final ImageView imageSet = (ImageView) content.findViewById(R.id.image_post_set);
                final ImageView close = (ImageView) titleWrap.findViewById(R.id.close);
                final TextView titleTextView = (TextView) titleWrap.findViewById(R.id.title);
                final TextView titleEditText = (TextView) titleWrap.findViewById(R.id.input_title);
                final TextView urlTextView = (TextView) content.findViewById(R.id.url);
                final TextView descriptionTextView = (TextView) content.findViewById(R.id.description);
                final TextView descriptionEditText = (TextView) content.findViewById(R.id.input_description);
                final TextView countTextView = (TextView) thumbnailOptions.findViewById(R.id.count);
                final CheckBox noThumbCheckBox = (CheckBox) noThumbnailOptions.findViewById(R.id.no_thumbnail_checkbox);
                final Button previousButton = (Button) thumbnailOptions.findViewById(R.id.post_previous);
                final Button forwardButton = (Button) thumbnailOptions.findViewById(R.id.post_forward);
                editTextTitlePost = titleEditText;
                editTextDescriptionPost = descriptionEditText;
                thumbnailOptions.setVisibility(View.GONE);
                close.setVisibility(View.GONE);
                titleTextView.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        titleTextView.setVisibility(View.GONE);
                        titleEditText.setText(TextCrawler.extendedTrim(titleTextView.getText().toString()));
                        titleEditText.setVisibility(View.VISIBLE);
                    }
                });

                titleEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {

                        if (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            titleEditText.setVisibility(View.GONE);

                            currentTitle = TextCrawler
                                    .extendedTrim(titleEditText
                                            .getText().toString());

                            titleTextView.setText(currentTitle);
                            titleTextView.setVisibility(View.VISIBLE);

                            hideSoftKeyboard();
                        }

                        return false;
                    }
                });
                descriptionTextView.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        descriptionTextView.setVisibility(View.GONE);

                        descriptionEditText.setText(TextCrawler
                                .extendedTrim(descriptionTextView.getText()
                                        .toString()));
                        descriptionEditText.setVisibility(View.VISIBLE);
                    }
                });
                descriptionEditText
                        .setOnEditorActionListener(new TextView.OnEditorActionListener() {

                            @Override
                            public boolean onEditorAction(TextView arg0,
                                                          int arg1, KeyEvent arg2) {

                                if (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                                    descriptionEditText
                                            .setVisibility(View.GONE);

                                    currentDescription = TextCrawler
                                            .extendedTrim(descriptionEditText
                                                    .getText().toString());

                                    descriptionTextView
                                            .setText(currentDescription);
                                    descriptionTextView
                                            .setVisibility(View.VISIBLE);

                                    hideSoftKeyboard();
                                }

                                return false;
                            }
                        });

                close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // releasePreviewArea();
                        drop_preview1.setVisibility(View.GONE);

                    }
                });

                noThumbCheckBox
                        .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(CompoundButton arg0,
                                                         boolean arg1) {
                                noThumb = arg1;

                                if (sourceContent.getImages().size() > 1)
                                    if (noThumb)
                                        thumbnailOptions
                                                .setVisibility(View.GONE);
                                    else
                                        thumbnailOptions
                                                .setVisibility(View.GONE);

                                showHideImage(imageSet, infoWrap, !noThumb);
                            }
                        });

                previousButton.setEnabled(false);
                previousButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (currentItem > 0)
                            changeImage(previousButton, forwardButton,
                                    currentItem - 1, sourceContent,
                                    countTextView, imageSet, sourceContent
                                            .getImages().get(currentItem - 1),
                                    currentItem);
                    }
                });
                forwardButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (currentItem < sourceContent.getImages().size() - 1)
                            changeImage(previousButton, forwardButton,
                                    currentItem + 1, sourceContent,
                                    countTextView, imageSet, sourceContent
                                            .getImages().get(currentItem + 1),
                                    currentItem);
                    }
                });

                if (sourceContent.getImages().size() > 0) {

                    if (sourceContent.getImages().size() > 1) {
                        countTextView.setText("1 " + getString(R.string.of)
                                + " " + sourceContent.getImages().size());

                        thumbnailOptions.setVisibility(View.GONE);
                    }
                    noThumbnailOptions.setVisibility(View.GONE);

                    UrlImageViewHelper.setUrlDrawable(imageSet, sourceContent
                            .getImages().get(0), new UrlImageViewCallback() {

                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {
                            if (loadedBitmap != null) {
                                currentImage = loadedBitmap;
                                currentImageSet[0] = loadedBitmap;
                            }
                        }
                    });

                } else {
                    showHideImage(imageSet, infoWrap, false);
                }

                if (sourceContent.getTitle().equals(""))
                    sourceContent.setTitle(getString(R.string.enter_title));
                if (sourceContent.getDescription().equals(""))
                    sourceContent
                            .setDescription(getString(R.string.enter_description));

                titleTextView.setText(sourceContent.getTitle());
                urlTextView.setText(sourceContent.getCannonicalUrl());
                descriptionTextView.setText(sourceContent.getDescription());

                //.setVisibility(View.VISIBLE);
            }

            currentTitle = sourceContent.getTitle();
            currentDescription = sourceContent.getDescription();
            currentUrl = sourceContent.getUrl();
            currentCannonicalUrl = sourceContent.getCannonicalUrl();
        }
    };

    /**
     * Change the current image in image set
     */
    private void changeImage(Button previousButton, Button forwardButton,
                             final int index, SourceContent sourceContent,
                             TextView countTextView, ImageView imageSet, String url,
                             final int current) {

        if (currentImageSet[index] != null) {
            currentImage = currentImageSet[index];
            imageSet.setImageBitmap(currentImage);
        } else {
            UrlImageViewHelper.setUrlDrawable(imageSet, url,
                    new UrlImageViewCallback() {

                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {
                            if (loadedBitmap != null) {
                                currentImage = loadedBitmap;
                                currentImageSet[index] = loadedBitmap;
                            }
                        }
                    });

        }

        currentItem = index;

        if (index == 0)
            previousButton.setEnabled(false);
        else
            previousButton.setEnabled(true);

        if (index == sourceContent.getImages().size() - 1)
            forwardButton.setEnabled(false);
        else
            forwardButton.setEnabled(true);

        countTextView.setText((index + 1) + " " + getString(R.string.of) + " "
                + sourceContent.getImages().size());
    }

    /**
     * Show or hide the image layout according to the "No Thumbnail" ckeckbox
     */

    private void showHideImage(View image, View parent, boolean show) {
        if (show) {
            image.setVisibility(View.VISIBLE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LinearLayout.LayoutParams(0,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 2f));
        } else {
            image.setVisibility(View.GONE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LinearLayout.LayoutParams(0,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 3f));
        }
    }

    /**
     * Hide keyboard
     */

    private void hideSoftKeyboard() {
        hideSoftKeyboard(edt_post);
    }

    private void hideSoftKeyboard(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager
                .hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (from.equalsIgnoreCase(ServiceResource.Wall)) {
            Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
            i.putExtra("position", mContext.getResources().getString(R.string.Wall));
            startActivity(i);
            overridePendingTransition(0, 0);
        } else if (from.equalsIgnoreCase(PROFILEWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
            ServiceResource.WALLTITLE = "My Wall";
            intent.putExtra("isGotoWall", false);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
            ServiceResource.WALLTITLE = "Institute wall";
            intent.putExtra("isGotoWall", false);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.GROUPWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        }

    }

}
