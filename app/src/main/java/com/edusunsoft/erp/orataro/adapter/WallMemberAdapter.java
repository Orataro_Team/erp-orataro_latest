package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.MemberModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;

import java.util.ArrayList;

public class WallMemberAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<MemberModel> list = new ArrayList<>();
    private LayoutInflater layoutInfalater;

    public WallMemberAdapter(Context mContext, ArrayList<MemberModel> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.wallmemberraw, parent, false);

        CircleImageView iv_std_icon = (CircleImageView) convertView.findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) convertView.findViewById(R.id.tv_std_name);
        TextView tv_usertype = (TextView) convertView.findViewById(R.id.tv_usertype);

        tv_std_name.setText(list.get(position).getFullName());
        tv_usertype.setText(list.get(position).getMemberType());

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL1
                            + list.get(position).getProfilePicture()
                            .replace("//", "/")
                            .replace("//", "/"))
                    .apply(options)
                    .into(iv_std_icon);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

}
