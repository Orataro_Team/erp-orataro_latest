package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.WallMemberAdapter;
import com.edusunsoft.erp.orataro.model.MemberModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WallMemberActivity extends Activity implements ResponseWebServices {

    private ImageView imgHome, imgMenu;
    private TextView headerText, txt_nodatafound;
    private Context mContext;
    private ListView lv_member;
    private JSONArray jsonObj;
    private ArrayList<MemberModel> memberList = new ArrayList<>();
    private WallMemberAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallmember);

        mContext = WallMemberActivity.this;
        imgHome = (ImageView) findViewById(R.id.img_home);
        imgMenu = (ImageView) findViewById(R.id.img_menu);
        headerText = (TextView) findViewById(R.id.header_text);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        lv_member = (ListView) findViewById(R.id.lv_member);
        headerText.setText(getResources().getString(R.string.wallmember) + " (" + Utility.GetFirstName(mContext) + ")");
        WallMemberWS();
        imgHome.setImageResource(R.drawable.back);
        imgMenu.setVisibility(View.INVISIBLE);
        imgHome.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }

        });

    }

    public void WallMemberWS() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getCURRENTWALLID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, true, this).
                execute(ServiceResource.GETWALLMEMBER_METHODNAME, ServiceResource.WALL_URL);
    }

    @Override
    public void response(String result, String methodName) {

        if (methodName.equalsIgnoreCase(ServiceResource.GETWALLMEMBER_METHODNAME)) {

            try {
                jsonObj = new JSONArray(result);
                memberList = new ArrayList<MemberModel>();

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    MemberModel model = new MemberModel();
                    model.setMemberID(innerObj.getString(ServiceResource.MEMBER_MemberID));
                    model.setFullName(innerObj.getString(ServiceResource.MEMBER_FULLNAME));
                    model.setMemberType(innerObj.getString(ServiceResource.MEMBER_MEMBERTYPE));
                    model.setProfilePicture(innerObj.getString(ServiceResource.MEMBER_PROFILEPICTURE));
                    memberList.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (memberList != null && memberList.size() > 0) {
                adapter = new WallMemberAdapter(mContext, memberList);
                lv_member.setAdapter(adapter);
                txt_nodatafound.setVisibility(View.GONE);
                lv_member.setVisibility(View.VISIBLE);
            } else {
                txt_nodatafound.setText(mContext.getResources().getString(R.string.wammmemberavailable));
                txt_nodatafound.setVisibility(View.VISIBLE);
                lv_member.setVisibility(View.GONE);
            }

        }

    }

}
