package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.ListInterface;
import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddReminderActivity;
import com.edusunsoft.erp.orataro.activities.ViewHomWorkDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class TodoListAdapter extends BaseAdapter implements ResponseWebServices {
    private static final int ID_EDIT = 1;
    private static final int ID_DELETE = 2;
    private static final int ID_COMPLETE = 3;
    private LayoutInflater layoutInfalater;
    ListInterface mListInterface;
    Context context;
    ArrayList<TodoListModel> todoModels, copyList;
    TextView tv_date, tv_subject, tv_sub_detail;
    ImageView list_image, img_delete;
    TextView txt_month;
    TextView txt_date;
    LinearLayout ll_view, ll_weight, editDelet;
    LinearLayout ll_date, ll_date_img, addbtnlayout, ll_editdelete;

    int pos = 0;
    RefreshListner listner;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private Dialog mDeleteDialog;
    private ImageView img_edit;

    public TodoListAdapter(Context context,
                           ArrayList<TodoListModel> todoModels, RefreshListner listner) {
        this.context = context;
        this.todoModels = todoModels;
        this.listner = listner;
        // mListInterface = pListInterface;
        copyList = new ArrayList<TodoListModel>();
        copyList.addAll(todoModels);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return todoModels.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.homework_listraw,
                parent, false);
        img_edit = (ImageView) convertView.findViewById(R.id.img_edit);
        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        editDelet = (LinearLayout) convertView.findViewById(R.id.editDelet);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        img_delete = (ImageView) convertView.findViewById(R.id.img_delete);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        ll_weight = (LinearLayout) convertView.findViewById(R.id.ll_weight);
        tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
        tv_sub_detail = (TextView) convertView.findViewById(R.id.tv_sub_detail);
        addbtnlayout = (LinearLayout) convertView.findViewById(R.id.addbtnlayout);
        ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);

        ll_editdelete.setVisibility(View.VISIBLE);


        ActionItem actionEdit = new ActionItem(ID_EDIT, "Edit",
                context.getResources()
                        .getDrawable(R.drawable.edit_profile));
        ActionItem actionDelete = new ActionItem(ID_DELETE, "Delete",
                context.getResources()
                        .getDrawable(R.drawable.delete));
        ActionItem actionComplete;
        if (todoModels.get(position).getStatus().equalsIgnoreCase("Completed")) {
            actionComplete = new ActionItem(ID_COMPLETE, "Complete",
                    context.getResources()
                            .getDrawable(R.drawable.tick_mark));
        } else {
            actionComplete = new ActionItem(ID_COMPLETE, "Complete",
                    context.getResources()
                            .getDrawable(R.drawable.uncheck));
        }


        final QuickAction quickAction = new QuickAction(context,
                QuickAction.VERTICAL);

        // add action items into QuickAction
        quickAction.addActionItem(actionEdit);
        // quickAction.addActionItem(actionConfiguration);
        quickAction.addActionItem(actionDelete);
        quickAction.addActionItem(actionComplete);
        // Set listener for action item clicked
        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);
                        quickAction.dismiss();
                        // here we can filter which action item was clicked with
                        // pos or actionId parameter
                        if (actionId == ID_EDIT) {
                            //									csa
                            Intent i = new Intent(context, AddReminderActivity.class);
                            i.putExtra("isFrom", ServiceResource.TODO_FLAG);
                            i.putExtra("isEdit", true);
                            i.putExtra("model", todoModels.get(position));
                            context.startActivity(i);
                        } else if (actionId == ID_DELETE) {

                            Utility.deleteDialog(context, context.getResources().getString(R.string.Todo),
                                    todoModels.get(position).getTitle(), new Popup() {

                                        @Override
                                        public void deleteYes() {
                                            deleteTodoList(todoModels.get(position).getTodosID());
                                            //	todoDelete(todoModels.get(position).getTodosID(), position);
                                            //												deleteClassWork(homeWorkModels.get(deletepos).getClassWorkID());
                                        }

                                        @Override
                                        public void deleteNo() {
                                            // TODO Auto-generated method stub

                                        }
                                    });


                            //									deleteTodoList(todoModels.get(position).getTodosID());
                        } else if (actionId == ID_COMPLETE) {
                            addTodo(position);
                        } else {
                            // Toast.makeText(context, actionItem.getTitle() +
                            // " selected", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
                // Toast.makeText(context, "Dismissed",
                // Toast.LENGTH_SHORT).show();
            }
        });


        //		LayoutParams param = new LinearLayout.LayoutParams(
        //				0,
        //				LayoutParams.WRAP_CONTENT, 0.70f);

        //		ll_weight.setLayoutParams(param);
        editDelet.setVisibility(View.GONE);

        if (todoModels.get(position).getStatus().equalsIgnoreCase("Completed")) {
            list_image.setImageResource(R.drawable.double_tick_sky_blue);
        } else {
            list_image.setImageResource(R.drawable.tick_sky_blue);
        }


        convertView.setBackgroundColor(colors[position % colors.length]);

        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);

        if (todoModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        ll_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        //		if (todoModels.get(position).isIsRead()) {
        //			list_image.setImageResource(R.drawable.double_tick_sky_blue);
        //		} else {
        //			list_image.setImageResource(R.drawable.tick_sky_blue);
        //		}
        txt_date.setText(todoModels.get(position).getYear());
        tv_date.setText(todoModels.get(position).getDay());
        tv_subject.setText(todoModels.get(position).getTitle());
        if (Utility.isNull(todoModels.get(position).getDetails())) {

            if (todoModels.get(position).getDetails().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = todoModels.get(position)
                        .getDetails()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {

                    }
                };
                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                tv_sub_detail.setMovementMethod(LinkMovementMethod
                        .getInstance());
                tv_sub_detail.setText(text, BufferType.SPANNABLE);
            } else {
                tv_sub_detail.setText(todoModels.get(position)
                        .getDetails());
            }

        }
        tv_sub_detail.setText(todoModels.get(position)
                .getDetails());
        txt_month
                .setText(todoModels.get(position).getMonth().toUpperCase());

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewHomWorkDetailsActivity.class);
                i.putExtra("model", todoModels.get(position));
                i.putExtra("isFrom", ServiceResource.TODO_FLAG);
                context.startActivity(i);


            }

        });
        
        //		img_edit.setVisibility(View.INVISIBLE);
        img_edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AddReminderActivity.class);
                i.putExtra("isFrom", ServiceResource.TODO_FLAG);
                i.putExtra("isEdit", true);
                i.putExtra("model", todoModels.get(position));
                context.startActivity(i);
            }
        });


        ll_date_img.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // mListInterface.DateListOpen();

            }
        });
        //		img_delete.setVisibility(View.INVISIBLE);
        img_delete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //				pos = position;
                //				todoDelete(todoModels.get(position).getTodosID());
            }
        });


        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });

        if (Utility.isTeacher(context)) {
            if (Utility.ReadWriteSetting(ServiceResource.REMINDERS).getIsEdit()
                    || Utility.ReadWriteSetting(ServiceResource.REMINDERS).getIsDelete()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.INVISIBLE);
            }
        } else {
            ll_editdelete.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        todoModels.clear();
        if (charText.length() == 0) {
            todoModels.addAll(copyList);
        } else {

            for (TodoListModel vo : copyList) {

                if (vo.getTitle().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    // Log.v("matcher",vo.getSkillName());
                    todoModels.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }


    private void deleteTodoList(String todoId) {
//		DataBaseHelper dbHelper = DataBaseHelper.newInstance(context);
//		dbHelper.deletePostall(DataBaseHelper.TBL_TODOS, DataBaseHelper.TOADO_TODOSID+"='"+todoId+"'");
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();


        arrayList.add(new PropertyVo(ServiceResource.TODOID,
                todoId));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));


        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.DELET_TODO_METHODNAME,
                ServiceResource.TODO_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.DELET_TODO_METHODNAME.equalsIgnoreCase(methodName)) {
            //			Global.todolist.remove(pos);
            listner.refresh(methodName);
        }
        if (ServiceResource.ADDTODO_METHODNAME.equalsIgnoreCase(methodName)) {

            listner.refresh(methodName);

        }

    }

    private void addTodo(int pos) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();


        arrayList.add(new PropertyVo(ServiceResource.EDITID, todoModels.get(pos).getTodosID()));

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(context).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TITLE, todoModels.get(pos).getTitle()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_DETAILS,
                todoModels.get(pos).getDetails()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TYPETERM, todoModels.get(pos).getTypeTerm()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STATUS, "Completed"));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STARTDATE, todoModels.get(pos).getCreateOn()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_ENDDATE, todoModels.get(pos).getEndDate()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_COMPLETDATE, ""));

        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.ADDTODO_METHODNAME,
                ServiceResource.TODO_URL);
    }

}
