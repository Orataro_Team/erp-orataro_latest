package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.MultiplePhotoAdapter;
import com.edusunsoft.erp.orataro.database.GenerallWallData;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.edusunsoft.erp.orataro.services.ServiceResource.PROFILEWALL;

public class ImageListActivity extends Activity implements ResponseWebServices {

    ListView lv_Image;
    ImageView imgLeftHeade, imgRightHeader;
    TextView txtHeader;
    Context mContext;
    ArrayList<String> photoList;
    GenerallWallData postModel;
    boolean isALready = false;
    //LinearLayout ll_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_list);
        mContext = ImageListActivity.this;

        try {
            if (getIntent() != null) {

                postModel = (GenerallWallData) getIntent().getSerializableExtra("model");
                isALready = getIntent().getBooleanExtra("isAlredy", false);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgLeftHeade = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        lv_Image = (ListView) findViewById(R.id.lvList);
        lv_Image.setBackgroundColor(getResources().getColor(R.color.grey_fb_color));

        imgLeftHeade.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);

        try {

            txtHeader.setText(getResources().getString(R.string.PostImages) + " (" + Utility.GetFirstName(mContext) + ")");

        } catch (Exception e) {
            e.printStackTrace();
        }

        imgLeftHeade.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    RedirectToPArticularWall();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // TODO Auto-generated method stub
//                finish();

            }

        });

        if (!isALready) {

            morePicList();

        } else {

            photoList = new ArrayList<String>();
            String[] photoUrls = postModel.getPostUrls().split(",");

            photoList.clear();

            for (int i = 0; i < photoUrls.length; i++) {

                photoList.add(photoUrls[i]);
                Log.d("getPhotoList", photoList.toString());

            }

            if (photoList != null && photoList.size() > 0) {

                MultiplePhotoAdapter photoAdapter = new MultiplePhotoAdapter(mContext, photoList, postModel);
                lv_Image.setAdapter(photoAdapter);

            } else {

                Intent in = new Intent(mContext,
                        ZoomImageAcitivity.class);

                if (postModel.getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                    in.putExtra("img", postModel.getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                    in.putExtra("name", postModel.getFullName());


                } else {

                    in.putExtra("img", ServiceResource.BASE_IMG_URL + postModel.getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                    in.putExtra("name", postModel.getFullName());

                }

                mContext.startActivity(in);
                finish();

            }

        }

    }

    public void morePicList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOTYPE, postModel.getAssociationType()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOID, postModel.getAssociationID()));


        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOSTEDPICS_METHODNAME,
                ServiceResource.POST_URL);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            RedirectToPArticularWall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void RedirectToPArticularWall() {
        if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.Wall)) {
            finish();
//            Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
//            i.putExtra("position", mContext.getResources().getString(R.string.Wall));
//            startActivity(i);
//            overridePendingTransition(0, 0);
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(PROFILEWALL)) {
            finish();
//            Intent intent = new Intent(mContext, WallActivity.class);
//            intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
//            ServiceResource.WALLTITLE = "My Wall";
//            intent.putExtra("isGotoWall", false);
//            mContext.startActivity(intent);
//            ((Activity) mContext).finish();
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
            ServiceResource.WALLTITLE = "Institute wall";
            intent.putExtra("isGotoWall", false);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.GROUPWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (ServiceResource.FOR_WALL_SELECTION.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        }
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETPOSTEDPICS_METHODNAME.equalsIgnoreCase(methodName)) {

            photoList = new ArrayList<String>();

            try {

                JSONArray array = new JSONArray(result);

                photoList.clear();

                for (int i = 0; i < array.length(); i++) {

                    JSONObject obj = array.getJSONObject(i);
                    String photo = obj.getString(ServiceResource.PHOTO);
                    photoList.add(photo);
                    Log.v("Photolist234", photoList.toString());

                }

            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();


            }

            //			ll_menu.setVisibility(View.VISIBLE);

            if (photoList != null && photoList.size() > 0) {

                MultiplePhotoAdapter photoAdapter = new MultiplePhotoAdapter(mContext, photoList, postModel);
                lv_Image.setAdapter(photoAdapter);

            } else {

                Intent in = new Intent(mContext,
                        ZoomImageAcitivity.class);

                if (postModel.getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                    in.putExtra("img", postModel.getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                    in.putExtra("name", postModel.getFullName());


                } else {

                    in.putExtra("img", ServiceResource.BASE_IMG_URL + postModel.getPhoto().replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"));
                    in.putExtra("name", postModel.getFullName());


                }

                //		in.putExtra("model", wallList.get(pos));
                mContext.startActivity(in);
                finish();

            }


        }


    }

}
