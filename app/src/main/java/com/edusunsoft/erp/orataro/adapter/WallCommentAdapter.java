package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ImageListActivity;
import com.edusunsoft.erp.orataro.activities.VideoViewActivity;
import com.edusunsoft.erp.orataro.model.WallCommentModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class WallCommentAdapter extends BaseAdapter implements OnClickListener {

    private Context mContext;
    private ArrayList<WallCommentModel> wallCommentModels;
    private LayoutInflater layoutInfalater;
    private CircleImageView iv_profile_pic;
    private TextView tv_full_name, tv_comment, tv_comment_on;
    boolean isViewSinglePost;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private TextView txtSubDetail;
    private TextView txtUserName;
    private TextView timeBeforepost;
    private AdjustableImageView iv_fb;
    private CircleImageView iv_profilePic;
    private TextView txt_likes;
    private TextView txt_unlikes;
    private TextView txt_comments;
    private LinearLayout ll_like;
    private LinearLayout ll_Unlike;
    private LinearLayout ll_share;
    private LinearLayout ll_comment;
    private ImageView iv_like;
    private ImageView iv_Unlike;
    private TextView tv_like;
    private TextView tv_Unlike;
    private TextView txt_nodata;
    private WallPostModel wallPostModel;
    private int ShareType;
    private static final int ID_PUBLIC = 1;
    private static final int ID_FRIEND = 2;
    private static final int ID_SP_FRIEND = 3;
    private static final int ID_ONLY_ME = 4;
    private boolean isCall;
    private boolean isLikeSucess;
    private int pos;

    public WallCommentAdapter(Context context, ArrayList<WallCommentModel> wallCommentModels) {
        this.mContext = context;
        this.wallCommentModels = wallCommentModels;
    }

    public WallCommentAdapter(Context context, ArrayList<WallCommentModel> wallCommentModels, boolean isViewSinglePost, WallPostModel wallmodel, int pos) {
        this.mContext = context;
        this.wallCommentModels = wallCommentModels;
        this.isViewSinglePost = isViewSinglePost;
        this.wallPostModel = wallmodel;
        this.pos = pos;
    }

    @Override
    public int getCount() {
        if (isViewSinglePost) {
            if (wallCommentModels.size() == 0) {
                return 1 + 1;
            } else {
                return wallCommentModels.size() + 1;
            }
        } else {
            return wallCommentModels.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (position == 0) {
            if (isViewSinglePost) {
                convertView = layoutInfalater.inflate(R.layout.singlepostlistraw, parent, false);
                txtSubDetail = (TextView) convertView.findViewById(R.id.txt_sub_detail);
                txtUserName = (TextView) convertView.findViewById(R.id.nameUser);
                timeBeforepost = (TextView) convertView.findViewById(R.id.timebeforepost);
                iv_fb = (AdjustableImageView) convertView.findViewById(R.id.iv_fb);
                iv_profilePic = (CircleImageView) convertView.findViewById(R.id.profilePicUser);
                txt_likes = (TextView) convertView.findViewById(R.id.txt_likes);
                txt_unlikes = (TextView) convertView.findViewById(R.id.txt_unlikes);
                txt_comments = (TextView) convertView.findViewById(R.id.txt_comments);

                Log.d("getSinglePostData", wallPostModel.toString());

                onCreateRefresh();
                refreshdata();
                iv_fb.setOnClickListener(this);

            } else {
                convertView = addComment(convertView, position, parent);
            }
        } else if (wallCommentModels.size() == 0) {
            if (position == 1) {
                convertView = layoutInfalater.inflate(R.layout.textviewnodata, parent, false);
                txt_nodata = (TextView) convertView.findViewById(R.id.txt_nodata);
            }
        } else {
            convertView = addComment(convertView, position, parent);
        }

        return convertView;
    }

    public View addComment(View convertView, int position, ViewGroup parent) {
        if (isViewSinglePost) {
            position = position - 1;
        }
        convertView = layoutInfalater.inflate(R.layout.commentlistraw, parent, false);
        tv_full_name = (TextView) convertView.findViewById(R.id.tv_full_name);
        tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
        tv_comment_on = (TextView) convertView.findViewById(R.id.tv_comment_on);
        iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);

        if (wallCommentModels.get(position).getFullName() != null) {
            tv_full_name.setText(wallCommentModels.get(position).getFullName());
        }

        if (wallCommentModels.get(position).getComment() != null) {
            tv_comment.setText(wallCommentModels.get(position).getComment());
        }

        if (wallCommentModels.get(position).getCommentOn() != null) {
            tv_comment_on.setText(wallCommentModels.get(position)
                    .getCommentOn());
        }

        if (wallCommentModels.get(position).getProfilePicture() != null) {

            if (wallCommentModels.get(position).getProfilePicture() != null
                    && !wallCommentModels.get(position).getProfilePicture()
                    .equalsIgnoreCase("")) {

                try {
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(ServiceResource.BASE_IMG_URL1 + wallCommentModels.get(position).getProfilePicture())
                            .apply(options)
                            .into(iv_profile_pic);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        convertView.setBackgroundColor(colors[position % colors.length]);

        return convertView;
    }

    public void onCreateRefresh() {
        try {
            if (wallPostModel.getDateOfPost() != null) {
                timeBeforepost.setText(wallPostModel.getPostCommentTypesTerm() + " " + wallPostModel.getDateOfPost());
            }

            if (Utility.isNull(wallPostModel.getFullName())) {
                txtUserName.setText(wallPostModel.getFullName());
            }

            if (Utility.isNull(wallPostModel.getPostCommentNote())) {
                txtSubDetail.setText(wallPostModel.getPostCommentNote());
            }

            if (Utility.isNull(wallPostModel.getPhoto())) {

                if (wallPostModel.getFileType().equals("VIDEO")) {
                    iv_fb.setImageResource(R.drawable.dummy_video);
                } else if (wallPostModel.getFileType().equalsIgnoreCase("FILE")) {
                    iv_fb.setVisibility(View.VISIBLE);
                    LayoutParams params = new LayoutParams(200, 200);
                    iv_fb.setLayoutParams(params);
                    try {
                        iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (wallPostModel.getFileMimeType().equalsIgnoreCase("TEXT") || wallPostModel.getFileMimeType().equalsIgnoreCase("text/plain")) {
                        iv_fb.setImageResource(R.drawable.txt);
                    } else if (wallPostModel.getFileMimeType().equalsIgnoreCase("PDF") || wallPostModel.getFileMimeType().equalsIgnoreCase("application/pdf")) {
                        iv_fb.setVisibility(View.VISIBLE);
                        iv_fb.setImageResource(R.drawable.pdf);
                    } else if (wallPostModel.getFileMimeType().equalsIgnoreCase("WORD") ||
                            wallPostModel.getFileMimeType().equalsIgnoreCase("application/msword")) {
                        iv_fb.setImageResource(R.drawable.doc);
                    } else {
                        iv_fb.setImageResource(R.drawable.file);
                    }

//					if (wallPostModel.getFileMimeType() != null && wallPostModel.getFileMimeType().equalsIgnoreCase("TEXT") ||
//							wallPostModel.getFileMimeType().equalsIgnoreCase("text/plain")) {
//						iv_fb.setImageResource(R.drawable.txt);
//					} else if (wallPostModel.getFileMimeType() != null &&  wallPostModel.getFileMimeType().equalsIgnoreCase("PDF")) {
//						iv_fb.setImageResource(R.drawable.pdf);
//					} else if (wallPostModel.getFileMimeType() != null && wallPostModel.getFileMimeType().equalsIgnoreCase("WORD")) {
//						iv_fb.setImageResource(R.drawable.doc);
//					} else {
//						iv_fb.setImageResource(R.drawable.file);
//					}
                } else {

                    try {
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(wallPostModel.getPhoto()
                                        .replace("//DataFiles//", "/DataFiles/")
                                        .replace("//DataFiles/", "/DataFiles/"))
                                .apply(options)
                                .into(iv_fb);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } else {
                iv_fb.setVisibility(View.GONE);
            }

            if (Utility.isNull(wallPostModel.getProfilePicture())) {

                try {
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Log.d("getProfilePicturesingle", ServiceResource.BASE_IMG_URL1
                            + wallPostModel.getProfilePicture()
                            .replace("//", "/")
                            .replace("//", "/"));

                    Glide.with(mContext)
                            .load(wallPostModel.getProfilePicture()
                                    .replace("//", "/"))
                            .apply(options)
                            .into(iv_profilePic);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void refreshdata() {
        if (wallPostModel.getTotalLikes() != null
                && Integer.parseInt(wallPostModel.getTotalLikes()) > 0
                && !wallPostModel.getTotalLikes().equals("")) {

            if (Integer.parseInt(wallPostModel.getTotalLikes()) > 1) {
                txt_likes.setText(wallPostModel.getTotalLikes() + " likes");
            } else {
                txt_likes.setText(wallPostModel.getTotalLikes() + " like");
            }
            txt_likes.setVisibility(View.VISIBLE);
        } else {
            txt_likes.setVisibility(View.GONE);
        }

        if (wallPostModel.getTotalDislike() != null
                && Integer.parseInt(wallPostModel.getTotalDislike()) > 0
                && !wallPostModel.getTotalDislike().equals("")) {

            if (Integer.parseInt(wallPostModel.getTotalDislike()) > 1) {
                txt_unlikes.setText(wallPostModel.getTotalDislike() + " unlikes");
            } else {
                txt_unlikes.setText(wallPostModel.getTotalDislike() + " unlike");
            }

            txt_unlikes.setVisibility(View.VISIBLE);
        } else {
            txt_unlikes.setVisibility(View.GONE);
        }

        if (wallPostModel.getTotalComments() != null
                && Integer.parseInt(wallPostModel.getTotalComments()) > 0
                && !wallPostModel.getTotalComments().equals("")) {

            if (Integer.parseInt(wallPostModel.getTotalComments()) > 1) {
                txt_comments.setText(wallPostModel.getTotalComments() + " comments");
            } else {
                txt_comments.setText(wallPostModel.getTotalComments() + " comment");
            }
            txt_comments.setVisibility(View.VISIBLE);
        } else {
            txt_comments.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_fb:

                if (wallPostModel.getFileType().equals("VIDEO")) {

                    Log.d("getPhoto", wallPostModel.getPhoto());

                    Intent in = new Intent(mContext, VideoViewActivity.class);
                    in.putExtra("img", wallPostModel.getPhoto());
                    in.putExtra("name", wallPostModel.getFullName());
                    in.putExtra("FromStr", "");
                    mContext.startActivity(in);

                } else if (wallPostModel.getFileType().equals("FILE")) {

//                    String SELECTEDFILEURL = ServiceResource.BASE_IMG_URL + "DataFiles/" + wallPostModel.getPhoto();
//                    Log.d("getFileurl", SELECTEDFILEURL);
                    Log.d("getFileurl23", wallPostModel.getPhoto());
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(wallPostModel.getPhoto()));
                    mContext.startActivity(i);
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(wallPostModel.getPhoto()));
//                    mContext.startActivity(i);

                } else {

                    /*commented BY Krishna : 16-07-2019 Redirect to Imagelist Instead of Only Single Image*/

                    Intent in = new Intent(mContext, ImageListActivity.class);
                    in.putExtra("model", wallPostModel);
                    in.putExtra("isAlredy", false);
                    mContext.startActivity(in);

                    /*END*/

//                    Intent in = new Intent(mContext, ZoomImageAcitivity.class);
//                    in.putExtra("img", wallPostModel.getPhoto());
//                    in.putExtra("name", wallPostModel.getFullName());
//                    mContext.startActivity(in);
                }

                break;

            default:

                break;

        }

    }

}
