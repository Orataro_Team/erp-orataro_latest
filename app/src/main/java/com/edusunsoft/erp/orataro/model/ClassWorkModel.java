package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class ClassWorkModel implements Serializable {

    private String ClassWorkID, DateOfClassWork, PostID, GradeID, DivisionID, SubjectID, TeacherID, ClassWorkTitle, ClassWorkDetails, strDateOfClasswork;
    private String ReferenceLink, ExpectingDateOfCompletion, StartTime, EndTime, UserName, SubjectName, techearName, teacherImg;
    private String dateInMillisecond;
    private boolean IsRead, isCheck;
    private String imgUrl, GradeName, DivisionName;
    private boolean visibleMonth;
    private String month, day, year;

    public String getStrDateOfClasswork() {
        return strDateOfClasswork;
    }

    public void setStrDateOfClasswork(String strDateOfClasswork) {
        this.strDateOfClasswork = strDateOfClasswork;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

    public boolean isIsRead() {
        return IsRead;
    }

    public void setIsRead(boolean isRead) {
        IsRead = isRead;
    }

    public String getDateInMillisecond() {
        return dateInMillisecond;
    }

    public void setDateInMillisecond(String dateInMillisecond) {
        this.dateInMillisecond = dateInMillisecond;
    }

    public String getTechearName() {
        return techearName;
    }

    public void setTechearName(String techearName) {
        this.techearName = techearName;
    }

    public String getTeacherImg() {
        return teacherImg;
    }

    public void setTeacherImg(String teacherImg) {
        this.teacherImg = teacherImg;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isVisibleMonth() {
        return visibleMonth;
    }

    public void setVisibleMonth(boolean visibleMonth) {
        this.visibleMonth = visibleMonth;
    }

    public String getClassWorkID() {
        return ClassWorkID;
    }

    public void setClassWorkID(String classWorkID) {
        ClassWorkID = classWorkID;
    }

    public String getDateOfClassWork() {
        return DateOfClassWork;
    }

    public void setDateOfClassWork(String dateOfClassWork) {
        DateOfClassWork = dateOfClassWork;
    }

    public String getPostID() {
        return PostID;
    }

    public void setPostID(String postID) {
        PostID = postID;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getTeacherID() {
        return TeacherID;
    }

    public void setTeacherID(String teacherID) {
        TeacherID = teacherID;
    }

    public String getClassWorkTitle() {
        return ClassWorkTitle;
    }

    public void setClassWorkTitle(String classWorkTitle) {
        ClassWorkTitle = classWorkTitle;
    }

    public String getClassWorkDetails() {
        return ClassWorkDetails;
    }

    public void setClassWorkDetails(String classWorkDetails) {
        ClassWorkDetails = classWorkDetails;
    }

    public String getReferenceLink() {
        return ReferenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        ReferenceLink = referenceLink;
    }

    public String getExpectingDateOfCompletion() {
        return ExpectingDateOfCompletion;
    }

    public void setExpectingDateOfCompletion(String expectingDateOfCompletion) {
        ExpectingDateOfCompletion = expectingDateOfCompletion;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

}
