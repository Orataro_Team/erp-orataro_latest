package com.edusunsoft.erp.orataro.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;

import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.R;


public class LoaderProgress {

	Context mContext;
	Dialog dialog;
	boolean state=true;
	boolean isCanlable = true;
	ICancleAsynkTask iCancleAsynkTask;
	public LoaderProgress(Context pContext, ICancleAsynkTask iCancleAsynkTask) {
		mContext = pContext;
		state=true;
		this.iCancleAsynkTask= iCancleAsynkTask;
		dialog=new Dialog(mContext);
		//dialog.setCancelable(false);

	}

	public LoaderProgress(Context pContext) {
		mContext = pContext;
		state=true;
		dialog=new Dialog(mContext);
	}

	public void setMessage(String str){

	}

	public void show() {

		try{
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.loader);
			Activity activity=((Activity)mContext);
			dialog.setCancelable(isCanlable);
			//dialog.getWindow().setBackgroundDrawable(mContext.getColor(R.color.transparent));
			dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
			ImageView imageView=(ImageView)dialog.findViewById(R.id.iv_load_img);
			//imageView.setBackgroundResource(R.drawable.loading);
			//AnimationDrawable gyroAnimation = (AnimationDrawable) imageView.getBackground();
			//gyroAnimation.start();
			dialog.show();
			state=false;
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void setCancelable(boolean value) {
		isCanlable = value;
		dialog.setCancelable(value);
	}

	public void dismiss() {
		try{
			if(!state) {
				if(iCancleAsynkTask != null ){
					iCancleAsynkTask.onCancleTask();
				}
				state=true;
				if(dialog != null && dialog.isShowing()){
					dialog.dismiss();
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
