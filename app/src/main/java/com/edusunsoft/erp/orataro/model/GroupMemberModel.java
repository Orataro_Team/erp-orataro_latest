package com.edusunsoft.erp.orataro.model;

public class GroupMemberModel {

    private String memberName;
    private String memberId;
    private String GropuMemberID, MemberProfileCode, MemberRole;
    private boolean isSelected = false;

    public String getGropuMemberID() {
        return GropuMemberID;
    }

    public void setGropuMemberID(String gropuMemberID) {
        GropuMemberID = gropuMemberID;
    }

    public String getMemberProfileCode() {
        return MemberProfileCode;
    }

    @Override
    public String toString() {

        return "GroupMemberModel{" +
                "memberName='" + memberName + '\'' +
                ", memberId='" + memberId + '\'' +
                ", GropuMemberID='" + GropuMemberID + '\'' +
                ", MemberProfileCode='" + MemberProfileCode + '\'' +
                ", MemberRole='" + MemberRole + '\'' +
                ", isSelected=" + isSelected +
                ", FriendID='" + FriendID + '\'' +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", WallID='" + WallID + '\'' +
                '}';
    }

    public void setMemberProfileCode(String memberProfileCode) {
        MemberProfileCode = memberProfileCode;


    }

    public String getMemberRole() {
        return MemberRole;
    }

    public void setMemberRole(String memberRole) {
        MemberRole = memberRole;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    String FriendID, ProfilePicture, WallID;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }


}
