package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DynamicWallListDataDao {

    @Insert
    void insertDynamicWallList(DynamicWallListModel dynamicWallListModel);

    @Query("SELECT * from DynamicWall")
    List<DynamicWallListModel> getDynamicWallList();

    @Query("DELETE  FROM DynamicWall")
    int deleteDynamicWallList();


}
