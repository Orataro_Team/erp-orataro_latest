package com.edusunsoft.erp.orataro.Interface;

import android.text.TextPaint;
import android.text.style.ClickableSpan;

public abstract class MyClickableSpan extends ClickableSpan {// extend ClickableSpan

    String clicked;
    public MyClickableSpan(String string) {
        super();
        clicked = string;
    }

   
    @Override
	public void updateDrawState(TextPaint ds) {// override updateDrawState
        ds.setUnderlineText(false); // set to false to remove underline
    }
}