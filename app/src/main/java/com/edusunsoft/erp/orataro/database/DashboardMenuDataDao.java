package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DashboardMenuDataDao {

    @Insert
    void insertDashboardMenuList(DashboardMenuModel dashboardMenuPermissionModel);

    @Query("SELECT * from DashboardMenu")
    List<DashboardMenuModel> getDashboardMenuList();

    @Query("DELETE  FROM DashboardMenu")
    int deleteDashboardMenu();

}
