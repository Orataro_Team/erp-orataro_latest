package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.WallCustomeClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.SinglePostActivity;
import com.edusunsoft.erp.orataro.activities.ZoomImageAcitivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.DivisionWallModel;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class DivisionWallAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private ImageView iv_fb;
    CircleImageView profilePicUser;
    private TextView userName, timeBeforepost, postContent;
    private LinearLayout ll_share, ll_comment, ll_Unlike, ll_like;
    private ArrayList<DivisionWallModel> divisionWallModels;
    private TextView tv_post;
    private LinearLayout showHideLayoutstatus, showHidelayoutphotovideo, ll_Photo, ll_Video, ll_Friends;
    private WallCustomeClick interfaceClick;
    private static final int ID_FRIEND = 1;
    private static final int ID_PUBLIC = 2;
    private static final int ID_ONLY_ME = 3;
    private ActionItem actionPublic, actionFriend, actionOnlyMe;
    private QuickAction quickActionForShare;

    public DivisionWallAdapter(Context context, ArrayList<DivisionWallModel> wallList, WallCustomeClick interfaceClick) {
        this.mContext = context;
        this.divisionWallModels = wallList;
        this.interfaceClick = interfaceClick;
    }

    @Override
    public int getCount() {
        return divisionWallModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (position == 0) {
            convertView = layoutInfalater.inflate(R.layout.walllayout, parent, false);
            showHideLayoutstatus = (LinearLayout) convertView.findViewById(R.id.showhideLayout);
            showHidelayoutphotovideo = (LinearLayout) convertView.findViewById(R.id.showhideLayoutphotovideo);
            tv_post = (TextView) convertView.findViewById(R.id.tv_post);
            ll_Photo = (LinearLayout) convertView.findViewById(R.id.ll_Photo);
            ll_Video = (LinearLayout) convertView.findViewById(R.id.ll_Video);
            ll_Friends = (LinearLayout) convertView.findViewById(R.id.ll_Friends);
            showHideLayoutstatus.setVisibility(View.GONE);
            showHidelayoutphotovideo.setVisibility(View.VISIBLE);
            tv_post.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    interfaceClick.clickOnPost();
                }
            });

            ll_Photo.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    interfaceClick.clickOnPhoto();
                }
            });

            ll_Video.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    interfaceClick.clickOnVideo();
                }
            });

            ll_Friends.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    interfaceClick.clickOnFriends();
                }
            });

        } else {
            position = position - 1;
            final int pos = position;
            convertView = layoutInfalater.inflate(R.layout.whats_on_mind_listraw, parent, false);

            profilePicUser = (CircleImageView) convertView.findViewById(R.id.profilePicUser);
            iv_fb = (ImageView) convertView.findViewById(R.id.iv_fb);
            userName = (TextView) convertView.findViewById(R.id.nameUser);
            timeBeforepost = (TextView) convertView.findViewById(R.id.timebeforepost);
            postContent = (TextView) convertView.findViewById(R.id.txt_sub_detail);
            ll_like = (LinearLayout) convertView.findViewById(R.id.ll_like);
            ll_Unlike = (LinearLayout) convertView.findViewById(R.id.ll_Unlike);
            ll_comment = (LinearLayout) convertView.findViewById(R.id.ll_comment);
            ll_share = (LinearLayout) convertView.findViewById(R.id.ll_share);

            if (divisionWallModels.get(position).isAvilablepostImg()) {
                iv_fb.setVisibility(View.VISIBLE);
            } else {
                iv_fb.setVisibility(View.GONE);
            }
            if (divisionWallModels.get(position).isAvailPostContent()) {
                postContent.setVisibility(View.VISIBLE);
            } else {
                postContent.setVisibility(View.GONE);
            }
            if (divisionWallModels.get(position).getPersonName() != null) {
                userName.setText(divisionWallModels.get(position).getPersonName());
            }
            if (divisionWallModels.get(position).getTimeBeforePost() != null) {
                timeBeforepost.setText(divisionWallModels.get(position)
                        .getTimeBeforePost());
            }

            if (divisionWallModels.get(position).getPostContent() != null) {

                if (divisionWallModels.get(position).getPostContent().length() > Constants.CONTINUEREADINGSIZE) {
                    String continueReadingStr = Constants.CONTINUEREADINGSTR;
                    String tempText = divisionWallModels.get(position).getPostContent().substring(0, Constants.CONTINUEREADINGSIZE) + continueReadingStr;
                    SpannableString text = new SpannableString(tempText);
                    text.setSpan(new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR), Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);
                    MyClickableSpan clickfor = new MyClickableSpan(tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                        @Override
                        public void onClick(View widget) {

                            Intent i = new Intent(mContext, SinglePostActivity.class);
                            i.putExtra("profilepic", divisionWallModels.get(pos).getProfilePicImg());
                            i.putExtra("username", divisionWallModels.get(pos).getPersonName());
                            i.putExtra("imgurl", divisionWallModels.get(pos).getPostImg());
                            i.putExtra("postcontent", divisionWallModels.get(pos).getPostContent());
                            mContext.startActivity(i);
                        }
                    };

                    text.setSpan(clickfor, Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);
                    postContent.setMovementMethod(LinkMovementMethod.getInstance());
                    postContent.setText(text, BufferType.SPANNABLE);
                } else {
                    postContent.setText(divisionWallModels.get(position).getPostContent());
                }

            }

            if (divisionWallModels.get(position).getPostImg() != null) {
                iv_fb.setImageBitmap(Utility.getBitmapFromAsset(mContext, divisionWallModels.get(position).getPostImg()));
            }
            if (divisionWallModels.get(position).getProfilePicImg() != null) {
                profilePicUser.setImageBitmap(Utility.getBitmapFromAsset(mContext, divisionWallModels.get(position).getProfilePicImg()));
            }

            iv_fb.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent in = new Intent(mContext, ZoomImageAcitivity.class);
                    in.putExtra("img", divisionWallModels.get(pos).getPostImg());
                    in.putExtra("name", divisionWallModels.get(pos).getPersonName());
                    mContext.startActivity(in);
                }
            });

            postContent.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, SinglePostActivity.class);
                    i.putExtra("profilepic", divisionWallModels.get(pos).getProfilePicImg());
                    i.putExtra("username", divisionWallModels.get(pos).getPersonName());
                    i.putExtra("imgurl", divisionWallModels.get(pos).getPostImg());
                    i.putExtra("postcontent", divisionWallModels.get(pos).getPostContent());
                    mContext.startActivity(i);
                }
            });

            actionPublic = new ActionItem(ID_PUBLIC, "Public", mContext
                    .getResources().getDrawable(R.drawable.fb_publics));
            actionFriend = new ActionItem(ID_FRIEND, "Friends", mContext
                    .getResources().getDrawable(R.drawable.fb_friends));
            actionOnlyMe = new ActionItem(ID_ONLY_ME, "Only Me", mContext
                    .getResources().getDrawable(R.drawable.fb_only_me));

            quickActionForShare = new QuickAction(mContext, QuickAction.VERTICAL);

            quickActionForShare.addActionItem(actionFriend);
            quickActionForShare.addActionItem(actionPublic);
            quickActionForShare.addActionItem(actionOnlyMe);

            // Set listener for action item clicked
            quickActionForShare
                    .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                        @Override
                        public void onItemClick(QuickAction source, int pos,
                                                int actionId) {
                            ActionItem actionItem = quickActionForShare.getActionItem(pos);

                            if (actionId == ID_FRIEND) {

                            } else if (actionId == ID_PUBLIC) {

                            } else if (actionId == ID_ONLY_ME) {

                            }
                        }
                    });

            quickActionForShare
                    .setOnDismissListener(new QuickAction.OnDismissListener() {
                        @Override
                        public void onDismiss() {

                        }
                    });

            ll_share.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    quickActionForShare.show(v);
                }
            });
        }
        return convertView;

    }

}
