package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class StandardModel implements Serializable {

    private String standrdId, standardName;
    private String divisionId, divisionName;
    private String subjectId, subjectName;
    private String GradeWallID,GradeDivisionWallID,GradeDivisionSubjectWallID;
    private String countleave = "";
    private boolean isChecked;


    public String getGradeWallID() {
        return GradeWallID;
    }

    public void setGradeWallID(String gradeWallID) {
        GradeWallID = gradeWallID;
    }

    public String getGradeDivisionWallID() {
        return GradeDivisionWallID;
    }

    public void setGradeDivisionWallID(String gradeDivisionWallID) {
        GradeDivisionWallID = gradeDivisionWallID;
    }

    public String getGradeDivisionSubjectWallID() {
        return GradeDivisionSubjectWallID;
    }

    public void setGradeDivisionSubjectWallID(String gradeDivisionSubjectWallID) {
        GradeDivisionSubjectWallID = gradeDivisionSubjectWallID;
    }

    public String getCountleave() {
        return countleave;
    }

    public void setCountleave(String countleave) {
        this.countleave = countleave;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getStandrdId() {
        return standrdId;
    }

    public void setStandrdId(String standrdId) {
        this.standrdId = standrdId;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return "StandardModel{" +
                "standrdId='" + standrdId + '\'' +
                ", standardName='" + standardName + '\'' +
                ", divisionId='" + divisionId + '\'' +
                ", divisionName='" + divisionName + '\'' +
                ", subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", countleave='" + countleave + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
