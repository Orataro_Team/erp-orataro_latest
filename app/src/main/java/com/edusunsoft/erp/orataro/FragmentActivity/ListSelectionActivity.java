package com.edusunsoft.erp.orataro.FragmentActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.ListselectionFragment;
import com.edusunsoft.erp.orataro.util.Utility;

public class ListSelectionActivity extends FragmentActivity implements OnClickListener {

    private boolean isDivision;
    private boolean isSubject;
    private boolean isClassWork;
    private boolean isHomeWork;
    private String gradeId;
    private String from;
    private int pos;
    private boolean isStanderd;
    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    LinearLayout headerLayout;
    boolean isWithoutSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);

        headerLayout = (LinearLayout) findViewById(R.id.headerLayout);

        if (getIntent() != null) {

            isDivision = getIntent().getBooleanExtra("isDivision", false);
            isSubject = getIntent().getBooleanExtra("isSubject", false);
            isClassWork = getIntent().getBooleanExtra("isClassWork", false);
            isHomeWork = getIntent().getBooleanExtra("isHomeWork", false);
            gradeId = getIntent().getStringExtra("gradeId");
            from = getIntent().getStringExtra("isFrom");
            isWithoutSubject = getIntent().getBooleanExtra("iswithoutsubject", false);
            pos = getIntent().getIntExtra("pos", 0);
            if (isDivision || isSubject) {
                isStanderd = false;
            }
        }

        headerLayout.setVisibility(View.GONE);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.homeworkActivity_containair, ListselectionFragment.newInstance(isDivision, isSubject, isClassWork, isHomeWork, gradeId, from, pos, true, isWithoutSubject));
        ft.commit();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Utility.RedirectToDashboard(this);

    }

}
