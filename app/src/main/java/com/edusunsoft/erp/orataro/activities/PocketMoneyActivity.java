package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PocketMoneyAdapter;
import com.edusunsoft.erp.orataro.model.PocketMoneyDetailModel;
import com.edusunsoft.erp.orataro.model.PocketMoneyModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.edusunsoft.erp.orataro.util.Utility.isNull;

public class PocketMoneyActivity extends AppCompatActivity implements ResponseWebServices, View.OnClickListener {

    TextView header_text, txt_name, txt_accnt_number, txt_floor_room, txt_opening_balance, txt_closing_balance, txt_nodatafound;
    RecyclerView recycerlview;
    private LinearLayoutManager mLayoutManager;
    Context mContext;
    ImageView img_back;

    ArrayList<PocketMoneyModel> PocketMoneyList = new ArrayList<>();
    PocketMoneyModel pocketmoneyModel;
    PocketMoneyDetailModel pocketMoneyDetailModel;
    PocketMoneyAdapter pocketmoneyListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pocketmoney_activity);

        Initialization();

    }

    private void Initialization() {

        mContext = PocketMoneyActivity.this;

        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText(getResources().getString(R.string.PocketMoney));
        img_back = (ImageView) findViewById(R.id.img_back);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_accnt_number = (TextView) findViewById(R.id.txt_accnt_number);
        txt_floor_room = (TextView) findViewById(R.id.txt_floor_room);
        txt_opening_balance = (TextView) findViewById(R.id.txt_opening_balance);
        txt_closing_balance = (TextView) findViewById(R.id.txt_closing_balance);

        recycerlview = (RecyclerView) findViewById(R.id.recycerlview);
        mLayoutManager = new LinearLayoutManager(this);
        // use a linear layout manager
        recycerlview.setLayoutManager(mLayoutManager);

        img_back.setOnClickListener(this);

        // get Pocket Money Detail
        try {
            getPocketMoneyDetail();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getPocketMoneyDetail() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        Log.d("getRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.HOSTELPOCKETMONEY, ServiceResource.HOSTELPOCKETMONEY_URL);

    }

    @Override
    public void response(String result, String methodName) {

        JSONArray pocketmoneyArray, pocketmoneydetailArray;


        try {

            PocketMoneyList.clear();

            JSONObject jsonObj = new JSONObject(result);
            pocketmoneyArray = jsonObj.getJSONArray(ServiceResource.TABLE);
            pocketmoneydetailArray = jsonObj.optJSONArray(ServiceResource.TABLE1);

            if (pocketmoneyArray.length() > 0) {

                for (int i = 0; i < pocketmoneyArray.length(); i++) {

                    JSONObject pocketMoneyObj = pocketmoneyArray.getJSONObject(i);
                    pocketmoneyModel = new PocketMoneyModel();
                    pocketmoneyModel.setSeqNo(pocketMoneyObj.getString(ServiceResource.SEQNO));
                    pocketmoneyModel.setAmount(pocketMoneyObj.getDouble(ServiceResource.POCKET_AMOUNT));
                    pocketmoneyModel.setTransactionType_Term(pocketMoneyObj.getString(ServiceResource.TRANSACTIONTYPE));
                    pocketmoneyModel.setNarration(pocketMoneyObj.getString(ServiceResource.NARRATION));
                    pocketmoneyModel.setAssociationType(pocketMoneyObj.getString(ServiceResource.HOSTEL_ASSOCIATIONTYPE));
                    pocketmoneyModel.setDateOfTransaction(pocketMoneyObj.getString(ServiceResource.DATEOFTRANSACTION));

                    if (pocketMoneyObj.getString(ServiceResource.TRANSACTIONTYPE).equalsIgnoreCase("deposit")) {
                        pocketmoneyModel.setDeposit(pocketMoneyObj.getDouble(ServiceResource.POCKET_AMOUNT));
                        pocketmoneyModel.setWithdraw(0.00);
                        ServiceResource.TOTALBALANCE = ServiceResource.TOTALBALANCE + pocketMoneyObj.getDouble(ServiceResource.POCKET_AMOUNT);
                        pocketmoneyModel.setTotalBalance(ServiceResource.TOTALBALANCE);
                    } else {
                        pocketmoneyModel.setWithdraw(pocketMoneyObj.getDouble(ServiceResource.POCKET_AMOUNT));
                        pocketmoneyModel.setDeposit(0.00);
                        ServiceResource.TOTALBALANCE = ServiceResource.TOTALBALANCE - pocketMoneyObj.getDouble(ServiceResource.POCKET_AMOUNT);
                        pocketmoneyModel.setTotalBalance(ServiceResource.TOTALBALANCE);
                    }
                    PocketMoneyList.add(pocketmoneyModel);

                }

            }

            if (pocketmoneydetailArray.length() > 0) {

                for (int i = 0; i < pocketmoneydetailArray.length(); i++) {

                    JSONObject pocketMoneydetailObj = pocketmoneydetailArray.getJSONObject(i);
                    pocketMoneyDetailModel = new PocketMoneyDetailModel();
                    pocketMoneyDetailModel.setStudentName(pocketMoneydetailObj.getString(ServiceResource.STUDENTNAME));
                    pocketMoneyDetailModel.setAcctNo(pocketMoneydetailObj.getString(ServiceResource.ACCOUNTNO));
                    pocketMoneyDetailModel.setBuildingFloorRoom(pocketMoneydetailObj.getString(ServiceResource.BUILDINGFLOORROOM));
                    pocketMoneyDetailModel.setOpeningBalance(pocketMoneydetailObj.getDouble(ServiceResource.OPENINGBALANCE));
                    pocketMoneyDetailModel.setCurrentBalance(pocketMoneydetailObj.getDouble(ServiceResource.CURRENTBALANCE));
                    ServiceResource.TOTALBALANCE = pocketMoneydetailObj.getDouble(ServiceResource.OPENINGBALANCE);

                }

            }

            // fill Pocket Money Student Detail
            FillPocketMoneyDetail(pocketMoneyDetailModel.getStudentName(), pocketMoneyDetailModel.getAcctNo(), pocketMoneyDetailModel.getBuildingFloorRoom(), pocketMoneyDetailModel.getCurrentBalance(), pocketMoneyDetailModel.getOpeningBalance());

            if (PocketMoneyList.size() > 0) {

                Collections.sort(PocketMoneyList, new SortedDate());
                Collections.reverse(PocketMoneyList);
                //create an Object for Adapter
                pocketmoneyListAdapter = new PocketMoneyAdapter(mContext, PocketMoneyList);
                // set the adapter object to the Recyclerview
                recycerlview.setAdapter(pocketmoneyListAdapter);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class SortedDate implements Comparator<PocketMoneyModel> {
        @Override
        public int compare(PocketMoneyModel o1, PocketMoneyModel o2) {
            return o1.getSeqNo().compareTo(o2.getSeqNo());
        }
    }

    private void FillPocketMoneyDetail(String studentName, String acctNo, String buildingFloorRoom, Double currentBalance, Double openingBalance) {

        if (!isNull(studentName)) {
            txt_name.setText("");
        } else {
            txt_name.setText(studentName);
        }

        if (!isNull(acctNo)) {
            txt_accnt_number.setText("");
        } else {
            txt_accnt_number.setText(acctNo);
        }

        if (!isNull(buildingFloorRoom)) {
            txt_floor_room.setText("");
        } else {
            txt_floor_room.setText(buildingFloorRoom);
        }

        txt_opening_balance.setText(String.valueOf(openingBalance));
        txt_closing_balance.setText(String.valueOf(currentBalance));

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:
                Utility.RedirectToDashboard(this);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.RedirectToDashboard(this);
    }
}
