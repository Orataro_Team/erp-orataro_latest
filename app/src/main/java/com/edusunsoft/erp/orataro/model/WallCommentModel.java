package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class WallCommentModel implements Serializable {

    private String PostCommentID;
    private String AssociationType;
    private String CommentID;
    private String Comment;
    private String CommentOn;
    private String IsDisLike;
    private String IsLike;
    private String FullName;
    private String ProfilePicture;
    private String TotalLikes;
    private String TotalReplies;
    private String TotalDislike;
    private String PostDate;
    private String Row_ID;

    public String getPostCommentID() {
        return PostCommentID;
    }

    public void setPostCommentID(String postCommentID) {
        PostCommentID = postCommentID;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    public String getCommentID() {
        return CommentID;
    }

    public void setCommentID(String commentID) {
        CommentID = commentID;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getCommentOn() {
        return CommentOn;
    }

    public void setCommentOn(String commentOn) {
        CommentOn = commentOn;
    }

    public String getIsDisLike() {
        return IsDisLike;
    }

    public void setIsDisLike(String isDisLike) {
        IsDisLike = isDisLike;
    }

    public String getIsLike() {
        return IsLike;
    }

    public void setIsLike(String isLike) {
        IsLike = isLike;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getTotalLikes() {
        return TotalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        TotalLikes = totalLikes;
    }

    public String getTotalReplies() {
        return TotalReplies;
    }

    public void setTotalReplies(String totalReplies) {
        TotalReplies = totalReplies;
    }

    public String getTotalDislike() {
        return TotalDislike;
    }

    public void setTotalDislike(String totalDislike) {
        TotalDislike = totalDislike;
    }

    public String getPostDate() {
        return PostDate;
    }

    public void setPostDate(String postDate) {
        PostDate = postDate;
    }

    public String getRow_ID() {
        return Row_ID;
    }

    public void setRow_ID(String row_ID) {
        Row_ID = row_ID;
    }

}
