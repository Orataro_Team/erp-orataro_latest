package com.edusunsoft.erp.orataro.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FriendRequestModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class FriendRequestAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private ArrayList<FriendRequestModel> friendRequestModels, copyList;
    private TextView tv_frnd_name, txt_mutual_frnd, tv_reuest_send, tv_add_friend;
    private ImageView iv_add_friend;
    private CircleImageView iv_profile_pic;
    private LinearLayout ll_add_frnd, ll_frnd;
    private Button btn_accept, btn_decline;
    private TextView txtGradename, txtDivision;
    private int pos;

    public FriendRequestAdapter(Context context, ArrayList<FriendRequestModel> pFriendRequestModels) {
        mContext = context;
        friendRequestModels = pFriendRequestModels;
        copyList = new ArrayList<FriendRequestModel>();
        copyList.addAll(friendRequestModels);
    }

    @Override
    public int getCount() {
        return friendRequestModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.friend_request_listraw, parent, false);

        tv_frnd_name = (TextView) convertView.findViewById(R.id.tv_frnd_name);
        tv_add_friend = (TextView) convertView.findViewById(R.id.tv_add_friend);
        tv_reuest_send = (TextView) convertView.findViewById(R.id.tv_reuest_send);
        iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);
        iv_add_friend = (ImageView) convertView.findViewById(R.id.iv_add_friend);
        txtGradename = (TextView) convertView.findViewById(R.id.txtGradename);
        txtDivision = (TextView) convertView.findViewById(R.id.txtDivision);
        btn_accept = (Button) convertView.findViewById(R.id.btn_accept);
        btn_decline = (Button) convertView.findViewById(R.id.btn_decline);

        if (friendRequestModels.get(position).getFullName() != null
                && !friendRequestModels.get(position).getFullName().equals("")) {
            tv_frnd_name.setText(friendRequestModels.get(position).getFullName());
        }

        if (isValid(friendRequestModels.get(position).getGradeName())) {
            txtGradename.setText(mContext.getResources().getString(R.string.Standard) + " :" + friendRequestModels.get(position).getGradeName());
        } else {
            txtGradename.setVisibility(View.INVISIBLE);
        }
        if (isValid(friendRequestModels.get(position).getDivisionName())) {
            txtDivision.setText(mContext.getResources().getString(R.string.Division) + " :" + friendRequestModels.get(position).getDivisionName());
        } else {
            txtDivision.setVisibility(View.INVISIBLE);
        }

        CircleImageView iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);

        if (friendRequestModels.get(position).getProfilePicture() != null) {

            if (friendRequestModels.get(position).getProfilePicture() != null
                    && !friendRequestModels.get(position).getProfilePicture().equalsIgnoreCase("")) {

                try {
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(ServiceResource.BASE_IMG_URL1 + friendRequestModels.get(position)
                                    .getProfilePicture())
                            .apply(options)
                            .into(iv_profile_pic);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        btn_accept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pos = position;
                AcceptRequestFriend();
            }
        });

        btn_decline.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pos = position;
                DeclineRequestFriend();
            }
        });

        return convertView;
    }

    public boolean isValid(String str) {
        boolean isValid = true;
        if (str != null) {
            if (str.equalsIgnoreCase("") || str.equalsIgnoreCase("null")) {
                isValid = false;
            }
        } else {
            isValid = false;
        }

        return isValid;
    }

    public void AcceptRequestFriend() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GET_FRIENDLISTID,
                Global.friendRequestModels.get(pos).getFriendListID()));
        arrayList.add(new PropertyVo(ServiceResource.GET_REQUESTID, Global.friendRequestModels.get(pos).getRequestID()));
        arrayList.add(new PropertyVo(ServiceResource.GET_REQUESTWALLID,
                Global.friendRequestModels.get(pos).getRequestWallID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_WALLID,
                new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_USERID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.APPROVE_REQUEST_FRIENDS_METHODNAME, ServiceResource.FRIENDS_URL);
    }

    public void DeclineRequestFriend() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GET_FRIENDLISTID,
                Global.friendRequestModels.get(pos).getFriendListID()));
        arrayList.add(new PropertyVo(ServiceResource.GET_REQUESTWALLID,
                Global.friendRequestModels.get(pos).getRequestWallID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_WALLID,
                new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_USERID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.DELETE_REQUEST_FRIENDS_METHODNAME,
                ServiceResource.FRIENDS_URL);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        friendRequestModels.clear();
        if (charText.length() == 0) {
            friendRequestModels.addAll(copyList);
        } else {
            for (FriendRequestModel vo : copyList) {
                if (vo.getFullName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    friendRequestModels.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.APPROVE_REQUEST_FRIENDS_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.sendPushNotification(mContext);
            if (Global.friendRequestModels != null
                    && Global.friendRequestModels.size() > 0)
                Global.friendRequestModels.remove(pos);
            ((Activity) mContext).finish();
        }

        if (ServiceResource.DELETE_REQUEST_FRIENDS_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.sendPushNotification(mContext);
            if (Global.friendRequestModels != null && Global.friendRequestModels.size() > 0)
                Global.friendRequestModels.remove(pos);
            ((Activity) mContext).finish();
        }
    }
}
