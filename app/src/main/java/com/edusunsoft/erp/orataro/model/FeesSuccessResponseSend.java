package com.edusunsoft.erp.orataro.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeesSuccessResponseSend implements Parcelable {

    public final static Parcelable.Creator<FeesSuccessResponseSend> CREATOR = new Creator<FeesSuccessResponseSend>() {
        public FeesSuccessResponseSend createFromParcel(Parcel in) {
            FeesSuccessResponseSend instance = new FeesSuccessResponseSend();
            in.readList(instance.table, (FeesSuccessResponseSend.Table.class.getClassLoader()));
            in.readList(instance.table1, (FeesSuccessResponseSend.Table1.class.getClassLoader()));
            return instance;
        }

        public FeesSuccessResponseSend[] newArray(int size) {
            return (new FeesSuccessResponseSend[size]);
        }

    };

    @SerializedName("table")
    @Expose
    private List<Table> table = null;
    @SerializedName("table1")
    @Expose
    private List<Table1> table1 = null;

    public List<Table> getTable() {
        return table;
    }

    public void setTable(List<Table> table) {
        this.table = table;
    }

    public List<Table1> getTable1() {
        return table1;
    }

    public void setTable1(List<Table1> table1) {
        this.table1 = table1;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(table);
        dest.writeList(table1);
    }

    public int describeContents() {
        return 0;
    }

    public static class Table implements Parcelable {

        public final static Parcelable.Creator<Table> CREATOR = new Creator<Table>() {

            public Table createFromParcel(Parcel in) {
                Table instance = new Table();
                instance.amountToBePaid = ((String) in.readValue((String.class.getClassLoader())));
                instance.bankName = ((String) in.readValue((String.class.getClassLoader())));
                instance.feesStructureScheduleID = ((String) in.readValue((String.class.getClassLoader())));
                instance.batchID = ((String) in.readValue((String.class.getClassLoader())));
                instance.bookID = ((String) in.readValue((String.class.getClassLoader())));
                instance.clientID = ((String) in.readValue((String.class.getClassLoader())));
                instance.dateOfFees = ((String) in.readValue((String.class.getClassLoader())));
                instance.feesStructureID = ((String) in.readValue((String.class.getClassLoader())));
                instance.instituteID = ((String) in.readValue((String.class.getClassLoader())));
                instance.isActive = ((String) in.readValue((String.class.getClassLoader())));
                instance.isPaid = ((String) in.readValue((String.class.getClassLoader())));
                instance.mOPTypeTerm = ((String) in.readValue((String.class.getClassLoader())));
                instance.paidAmounts = ((String) in.readValue((String.class.getClassLoader())));
                instance.refStudentFeesCollectionID = ((String) in.readValue((String.class.getClassLoader())));
                instance.referenceNo = ((String) in.readValue((String.class.getClassLoader())));
                instance.standardID = ((String) in.readValue((String.class.getClassLoader())));
                instance.studentFeesCollectionID = ((String) in.readValue((String.class.getClassLoader())));
                instance.studentID = ((String) in.readValue((String.class.getClassLoader())));
                instance.totalDue = ((String) in.readValue((String.class.getClassLoader())));
                instance.userID = ((String) in.readValue((String.class.getClassLoader())));
                return instance;
            }

            public Table[] newArray(int size) {
                return (new Table[size]);
            }

        };

        @SerializedName("AmountToBePaid")
        @Expose
        private String amountToBePaid;
        @SerializedName("BankName")
        @Expose
        private String bankName;
        @SerializedName("FeesStructureScheduleID")
        @Expose
        private String feesStructureScheduleID;
        @SerializedName("BatchID")
        @Expose
        private String batchID;
        @SerializedName("BookID")
        @Expose
        private String bookID;
        @SerializedName("ClientID")
        @Expose
        private String clientID;
        @SerializedName("DateOfFees")
        @Expose
        private String dateOfFees;
        @SerializedName("FeesStructureID")
        @Expose
        private String feesStructureID;
        @SerializedName("InstituteID")
        @Expose
        private String instituteID;
        @SerializedName("IsActive")
        @Expose
        private String isActive;
        @SerializedName("IsPaid")
        @Expose
        private String isPaid;
        @SerializedName("MOPType_Term")
        @Expose
        private String mOPTypeTerm;
        @SerializedName("PaidAmounts")
        @Expose
        private String paidAmounts;
        @SerializedName("RefStudentFeesCollectionID")
        @Expose
        private String refStudentFeesCollectionID;
        @SerializedName("ReferenceNo")
        @Expose
        private String referenceNo;
        @SerializedName("StandardID")
        @Expose
        private String standardID;
        @SerializedName("studentFeesCollectionID")
        @Expose
        private String studentFeesCollectionID;
        @SerializedName("StudentID")
        @Expose
        private String studentID;
        @SerializedName("TotalDue")
        @Expose
        private String totalDue;
        @SerializedName("UserID")
        @Expose
        private String userID;

        public String getAmountToBePaid() {
            return amountToBePaid;
        }

        public void setAmountToBePaid(String amountToBePaid) {
            this.amountToBePaid = amountToBePaid;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getFeesStructureScheduleID() {
            return feesStructureScheduleID;
        }

        public void setFeesStructureScheduleID(String feesStructureScheduleID) {
            this.feesStructureScheduleID = feesStructureScheduleID;
        }

        public String getBatchID() {
            return batchID;
        }

        public void setBatchID(String batchID) {
            this.batchID = batchID;
        }

        public String getBookID() {
            return bookID;
        }

        public void setBookID(String bookID) {
            this.bookID = bookID;
        }

        public String getClientID() {
            return clientID;
        }

        public void setClientID(String clientID) {
            this.clientID = clientID;
        }

        public String getDateOfFees() {
            return dateOfFees;
        }

        public void setDateOfFees(String dateOfFees) {
            this.dateOfFees = dateOfFees;
        }

        public String getFeesStructureID() {
            return feesStructureID;
        }

        public void setFeesStructureID(String feesStructureID) {
            this.feesStructureID = feesStructureID;
        }

        public String getInstituteID() {
            return instituteID;
        }

        public void setInstituteID(String instituteID) {
            this.instituteID = instituteID;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getIsPaid() {
            return isPaid;
        }

        public void setIsPaid(String isPaid) {
            this.isPaid = isPaid;
        }

        public String getMOPTypeTerm() {
            return mOPTypeTerm;
        }

        public void setMOPTypeTerm(String mOPTypeTerm) {
            this.mOPTypeTerm = mOPTypeTerm;
        }

        public String getPaidAmounts() {
            return paidAmounts;
        }

        public void setPaidAmounts(String paidAmounts) {
            this.paidAmounts = paidAmounts;
        }

        public String getRefStudentFeesCollectionID() {
            return refStudentFeesCollectionID;
        }

        public void setRefStudentFeesCollectionID(String refStudentFeesCollectionID) {
            this.refStudentFeesCollectionID = refStudentFeesCollectionID;
        }

        public String getReferenceNo() {
            return referenceNo;
        }

        public void setReferenceNo(String referenceNo) {
            this.referenceNo = referenceNo;
        }

        public String getStandardID() {
            return standardID;
        }

        public void setStandardID(String standardID) {
            this.standardID = standardID;
        }

        public String getStudentFeesCollectionID() {
            return studentFeesCollectionID;
        }

        public void setStudentFeesCollectionID(String studentFeesCollectionID) {
            this.studentFeesCollectionID = studentFeesCollectionID;
        }

        public String getStudentID() {
            return studentID;
        }

        public void setStudentID(String studentID) {
            this.studentID = studentID;
        }

        public String getTotalDue() {
            return totalDue;
        }

        public void setTotalDue(String totalDue) {
            this.totalDue = totalDue;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(amountToBePaid);
            dest.writeValue(bankName);
            dest.writeValue(feesStructureScheduleID);
            dest.writeValue(batchID);
            dest.writeValue(bookID);
            dest.writeValue(clientID);
            dest.writeValue(dateOfFees);
            dest.writeValue(feesStructureID);
            dest.writeValue(instituteID);
            dest.writeValue(isActive);
            dest.writeValue(isPaid);
            dest.writeValue(mOPTypeTerm);
            dest.writeValue(paidAmounts);
            dest.writeValue(refStudentFeesCollectionID);
            dest.writeValue(referenceNo);
            dest.writeValue(standardID);
            dest.writeValue(studentFeesCollectionID);
            dest.writeValue(studentID);
            dest.writeValue(totalDue);
            dest.writeValue(userID);
        }

        public int describeContents() {
            return 0;
        }

    }


    public static class Table1 implements Parcelable {

        public final static Parcelable.Creator<Table1> CREATOR = new Creator<Table1>() {

            public Table1 createFromParcel(Parcel in) {
                Table1 instance = new Table1();
                instance.studentFeesCollectionDetailID = ((String) in.readValue((String.class.getClassLoader())));
                instance.studentFeesCollectionID = ((String) in.readValue((String.class.getClassLoader())));
                instance.feesStructureID = ((String) in.readValue((String.class.getClassLoader())));
                instance.feesHeadID = ((String) in.readValue((String.class.getClassLoader())));
                instance.originalAmount = ((String) in.readValue((String.class.getClassLoader())));
                instance.receivedAmount = ((String) in.readValue((String.class.getClassLoader())));
                instance.bookID = ((String) in.readValue((String.class.getClassLoader())));
                instance.clientID = ((String) in.readValue((String.class.getClassLoader())));
                instance.instituteID = ((String) in.readValue((String.class.getClassLoader())));
                instance.isActive = ((String) in.readValue((String.class.getClassLoader())));
                instance.isPaid = ((String) in.readValue((String.class.getClassLoader())));
                instance.totalDue = ((String) in.readValue((String.class.getClassLoader())));
                instance.isOptionalFeeHead = ((String) in.readValue((String.class.getClassLoader())));
                return instance;
            }

            public Table1[] newArray(int size) {
                return (new Table1[size]);
            }

        };
        @SerializedName("StudentFeesCollectionDetailID ")
        @Expose
        private String studentFeesCollectionDetailID = "";
        @SerializedName("StudentFeesCollectionID ")
        @Expose
        private String studentFeesCollectionID = "";
        @SerializedName("FeesStructureID")
        @Expose
        private String feesStructureID = "";
        @SerializedName("FeesHeadID")
        @Expose
        private String feesHeadID = "";
        @SerializedName("OriginalAmount")
        @Expose
        private String originalAmount = "";
        @SerializedName("ReceivedAmount")
        @Expose
        private String receivedAmount = "";
        @SerializedName("BookID")
        @Expose
        private String bookID = "";
        @SerializedName("ClientID")
        @Expose
        private String clientID = "";
        @SerializedName("InstituteID")
        @Expose
        private String instituteID = "";
        @SerializedName("IsActive")
        @Expose
        private String isActive = "";
        @SerializedName("IsPaid")
        @Expose
        private String isPaid = "";
        @SerializedName("TotalDue")
        @Expose
        private String totalDue = "";
        @SerializedName("IsOptionalFeeHead")
        @Expose
        private String isOptionalFeeHead = "";

        public String getStudentFeesCollectionDetailID() {
            return studentFeesCollectionDetailID;
        }

        public void setStudentFeesCollectionDetailID(String studentFeesCollectionDetailID) {
            this.studentFeesCollectionDetailID = studentFeesCollectionDetailID;
        }

        public String getStudentFeesCollectionID() {
            return studentFeesCollectionID;
        }

        public void setStudentFeesCollectionID(String studentFeesCollectionID) {
            this.studentFeesCollectionID = studentFeesCollectionID;
        }

        public String getFeesStructureID() {
            return feesStructureID;
        }

        public void setFeesStructureID(String feesStructureID) {
            this.feesStructureID = feesStructureID;
        }

        public String getFeesHeadID() {
            return feesHeadID;
        }

        public void setFeesHeadID(String feesHeadID) {
            this.feesHeadID = feesHeadID;
        }

        public String getOriginalAmount() {
            return originalAmount;
        }

        public void setOriginalAmount(String originalAmount) {
            this.originalAmount = originalAmount;
        }

        public String getReceivedAmount() {
            return receivedAmount;
        }

        public void setReceivedAmount(String receivedAmount) {
            this.receivedAmount = receivedAmount;
        }

        public String getBookID() {
            return bookID;
        }

        public void setBookID(String bookID) {
            this.bookID = bookID;
        }

        public String getClientID() {
            return clientID;
        }

        public void setClientID(String clientID) {
            this.clientID = clientID;
        }

        public String getInstituteID() {
            return instituteID;
        }

        public void setInstituteID(String instituteID) {
            this.instituteID = instituteID;
        }

        public String getIsActive() {
            return isActive;
        }

        public void setIsActive(String isActive) {
            this.isActive = isActive;
        }

        public String getIsPaid() {
            return isPaid;
        }

        public void setIsPaid(String isPaid) {
            this.isPaid = isPaid;
        }

        public String getTotalDue() {
            return totalDue;
        }

        public void setTotalDue(String totalDue) {
            this.totalDue = totalDue;
        }

        public String getIsOptionalFeeHead() {
            return isOptionalFeeHead;
        }

        public void setIsOptionalFeeHead(String isOptionalFeeHead) {
            this.isOptionalFeeHead = isOptionalFeeHead;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(studentFeesCollectionDetailID);
            dest.writeValue(studentFeesCollectionID);
            dest.writeValue(feesStructureID);
            dest.writeValue(feesHeadID);
            dest.writeValue(originalAmount);
            dest.writeValue(receivedAmount);
            dest.writeValue(bookID);
            dest.writeValue(clientID);
            dest.writeValue(instituteID);
            dest.writeValue(isActive);
            dest.writeValue(isPaid);
            dest.writeValue(totalDue);
            dest.writeValue(isOptionalFeeHead);
        }

        public int describeContents() {
            return 0;
        }

    }

}




