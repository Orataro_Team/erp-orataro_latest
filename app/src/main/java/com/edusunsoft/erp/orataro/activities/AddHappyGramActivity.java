package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.EmojiesAdapter;
import com.edusunsoft.erp.orataro.adapter.HappyGramStudentAdapter;
import com.edusunsoft.erp.orataro.adapter.HappygramAdapter;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
// Add Happygram
//Edit Happygram 


public class AddHappyGramActivity extends Activity implements ResponseWebServices, OnClickListener {


    private String gradeId, divisionId, subjectId, subjectName, isFrom;
    private ImageView imgLeftHeader, imgRightHeader;
    private TextView txtHeader;
    private Context mContext = AddHappyGramActivity.this;
    private TextView txtNodatafound;
    private ListView lv_happygram;
    private HappyGramStudentAdapter studentAdapter;
    public HappygramAdapter happygramAdapter;
    private ImageView img_smile, img_emojies;
    private CheckBox chkAllStudent;
    private JSONArray jsonObj;
    private AutoCompleteTextView edt_appretion;
    private EditText edt_note;
    private String[] emojiesClassName = {"emotion1", "emotion2", "emotion3", "emotion4", "emotion5",
            "emotion6", "emotion7", "emotion8", "emotion9", "emotion10",
            "emotion11", "emotion12", "emotion13", "emotion14", "emotion15",
            "emotion16", "emotion17", "emotion18", "emotion19", "emotion20",
            "emotion21", "emotion22", "emotion23", "emotion24", "emotion25"};

    private String imgClassname = "";
    private String datalistParam = "";

    private int[] emojies = {R.drawable.emotion_01, R.drawable.emotion_02, R.drawable.emotion_03
            , R.drawable.emotion_04, R.drawable.emotion_05, R.drawable.emotion_06
            , R.drawable.emotion_07, R.drawable.emotion_08, R.drawable.emotion_09
            , R.drawable.emotion_10, R.drawable.emotion_11, R.drawable.emotion_12
            , R.drawable.emotion_13, R.drawable.emotion_14, R.drawable.emotion_15
            , R.drawable.emotion_16, R.drawable.emotion_17, R.drawable.emotion_18
            , R.drawable.emotion_19, R.drawable.emotion_20, R.drawable.emotion_21
            , R.drawable.emotion_22, R.drawable.emotion_23, R.drawable.emotion_24
            , R.drawable.emotion_25};
    private boolean isEdit;
    private MyHappyGramModel model;
    private LinearLayout ll_list, ll_editLayout;
    private View header;
    private StdDivSubModel stadardModel;
    private ImageView imgcross;

    /// comment by Krishna : Global Dialog Declaration;

    public Dialog mPoweroffDialog;

    /*END*/

    // search layout
    LinearLayout searchlayout;
    EditText edtsearchStudent;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addhappygram);
        if (getIntent() != null) {
            gradeId = getIntent().getStringExtra("gradeId");
            divisionId = getIntent().getStringExtra("divisionId");//subjectId,subjectName,isFrom
            subjectId = getIntent().getStringExtra("subjectId");
            subjectName = getIntent().getStringExtra("subjectName");
            isFrom = getIntent().getStringExtra("isFrom");
            isEdit = getIntent().getBooleanExtra("isEdit", false);
            stadardModel = (StdDivSubModel) getIntent().getSerializableExtra("stdmodel");
            if (isEdit) {
                model = (MyHappyGramModel) getIntent().getSerializableExtra("model");
            }
        }

        searchlayout = (LinearLayout) findViewById(R.id.searchlayout);
        searchlayout.setVisibility(View.GONE);
        edtsearchStudent = (EditText) findViewById(R.id.edtsearchStudent);
        edtsearchStudent.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtsearchStudent.getText().toString().toLowerCase(Locale.getDefault());
                happygramAdapter.filter(text);
            }
        });

        ll_editLayout = (LinearLayout) findViewById(R.id.ll_editLayout);
        ll_list = (LinearLayout) findViewById(R.id.ll_list);
        imgcross = (ImageView) findViewById(R.id.imgcross);
        header = findViewById(R.id.header);
        chkAllStudent = (CheckBox) findViewById(R.id.chkstudent);
        txtNodatafound = (TextView) findViewById(R.id.txtnodatafound);
        lv_happygram = (ListView) findViewById(R.id.lv_happygram);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        img_smile = (ImageView) findViewById(R.id.img_smile);
        img_emojies = (ImageView) findViewById(R.id.img_emojies);
        edt_appretion = (AutoCompleteTextView) findViewById(R.id.edt_appretion);
        edt_note = (EditText) findViewById(R.id.edt_note);

        imgLeftHeader.setVisibility(View.VISIBLE);
        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.VISIBLE);
        imgRightHeader.setImageResource(R.drawable.tick);

        try {
            if (!isEdit) {
                studentList();
                txtHeader.setText(mContext.getResources().getString(R.string.addhapygram) + " (" + Utility.GetFirstName(mContext) + ")");
                if (Utility.isTeacher(mContext)) {
                    txtHeader.setText(stadardModel.getStandardName() + " "
                            + stadardModel.getDivisionName() + " "
                            + stadardModel.getSubjectName());
                }
            } else {
                txtHeader.setText(mContext.getResources().getString(R.string.edithapygram) + " (" + Utility.GetFirstName(mContext) + ")");
                ll_editLayout.setVisibility(View.VISIBLE);
                txtHeader.setText(model.getFullName());
                ll_list.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                fillData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!Utility.isTeacher(mContext)) {
            imgRightHeader.setVisibility(View.INVISIBLE);
            img_smile.setVisibility(View.INVISIBLE);
            imgcross.setVisibility(View.INVISIBLE);
            edt_appretion.setEnabled(false);
            edt_note.setEnabled(false);
            img_smile.setEnabled(false);
            img_emojies.setEnabled(false);
            edt_appretion.clearFocus();
            edt_note.clearFocus();
        } else {
            if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsEdit()) {

            } else {
                imgRightHeader.setVisibility(View.INVISIBLE);
                img_smile.setVisibility(View.INVISIBLE);
                imgcross.setVisibility(View.INVISIBLE);
                edt_appretion.setEnabled(false);
                edt_note.setEnabled(false);
                img_smile.setEnabled(false);
                img_emojies.setEnabled(false);
                edt_appretion.clearFocus();
                edt_note.clearFocus();
            }
        }

        imgLeftHeader.setOnClickListener(this);
        imgRightHeader.setOnClickListener(this);
        img_smile.setOnClickListener(this);
        img_emojies.setOnClickListener(this);
        chkAllStudent.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectAll(true);
                } else {
                    selectAll(false);
                }
            }
        });
    }


    // fill data when edit myhappygram
    public void fillData() {
        if (model != null) {
            if (!Utility.isTeacher(mContext)) {
                txtHeader.setText(model.getSubjectName());
            }
            edt_appretion.setText(model.getHg_apprication());
            edt_note.setText(model.getHg_note());
            if (model.getEmotion() != null && !model.getEmotion().equalsIgnoreCase("")) {
                img_emojies.setImageResource(strngtointemojies(model.getEmotion()));
                img_emojies.setVisibility(View.VISIBLE);
                imgcross.setVisibility(View.VISIBLE);
                imgClassname = model.getEmotion();
            }
        }
    }


    public void selectAll(boolean select) {
        for (int i = 0; i < Global.myHappygramViewStudentList.size(); i++) {
            Global.myHappygramViewStudentList.get(i).setChecked(select);
            happygramAdapter.notifyDataSetChanged();
        }
    }

    //student list
    public void studentList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID,
                divisionId));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID,
                gradeId));


        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETHAPPYGRAMSTUDENTLIST_METHODNAME,
                ServiceResource.HAPPYGRAM_URL);
    }

    //Edit Happygram webservice
    public void editHappyGram() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAM_HAPPYGRAMID, model.getHappyGramID()));
        arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAM_APPRECIATION, edt_appretion.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAM_EMOTION, imgClassname));
        arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAM_NOTE, edt_note.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.UPDATE_HAPPYGRAM_METHODNAME, ServiceResource.HAPPYGRAM_URL);
    }


    public void addHappygram() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, subjectId));
        ArrayList<MyHappyGramModel> selectedList = new ArrayList<MyHappyGramModel>();
        selectedList = happygramAdapter.getSelectedStudentList();

        if (selectedList == null || selectedList.isEmpty()) {

            Utility.toast(AddHappyGramActivity.this, "Please Select Atleast One Student");

        } else {

            datalistParam = "";

            for (int i = 0; i < selectedList.size(); i++) {

                String note = "", appriciation = "";

                if (selectedList.get(i).getHg_apprication() != null && !selectedList.get(i).getHg_apprication().equalsIgnoreCase("")) {
                    appriciation = selectedList.get(i).getHg_apprication().replace("_", "");
                } else {
                    appriciation = "";
                }

                if (selectedList.get(i).getHg_note() != null && !selectedList.get(i).getHg_note().equalsIgnoreCase("")) {
                    note = selectedList.get(i).getHg_note().replace("_", "");
                } else {
                    note = "";
                }

                datalistParam = datalistParam + selectedList.get(i).getMemberID() + "_" +
                        appriciation + "_" +
                        selectedList.get(i).getEmotion() + "_" +
                        note + "#";

            }

            arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAMDATALIST, datalistParam));
            new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDHAPPYGRAM_METHODNAME, ServiceResource.HAPPYGRAM_URL);

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETHAPPYGRAMSTUDENTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            Global.myHappygramViewStudentList = new ArrayList<MyHappyGramModel>();

            try {

                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    MyHappyGramModel model = new MyHappyGramModel();
                    model.setMemberID(innerObj.getString(ServiceResource.HAPPYGRAM_MEMBERID));
                    model.setFullName(innerObj.getString(ServiceResource.HAPPYGRAM_FULLNAME));
                    model.setRegistationNo(innerObj.getString(ServiceResource.HAPPYGRAM_REGISTRATIONNO));
                    model.setRollNo(innerObj.getString(ServiceResource.HAPPYGRAM_ROLLNO));
                    Global.myHappygramViewStudentList.add(model);
                }
            } catch (JSONException e) {

                e.printStackTrace();

            }

            if (Global.myHappygramViewStudentList != null && Global.myHappygramViewStudentList.size() > 0) {

//                studentAdapter = new HappyGramStudentAdapter(mContext, Global.myHappygramViewStudentList);
//                lv_happygram.setAdapter(studentAdapter);


                happygramAdapter = new HappygramAdapter(AddHappyGramActivity.this, Global.myHappygramViewStudentList);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.setAdapter(happygramAdapter);

            }

        } else if (ServiceResource.UPDATE_HAPPYGRAM_METHODNAME.equalsIgnoreCase(methodName)) {
            finish();
        } else if (ServiceResource.ADDHAPPYGRAM_METHODNAME.equalsIgnoreCase(methodName)) {
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            finish();

        } else if (v.getId() == R.id.img_menu) {

            if (isEdit) {

                editHappyGram();

            } else {

                Log.d("myhappygramlist", Global.myHappygramViewStudentList.toString());
                if (Global.myHappygramViewStudentList != null && Global.myHappygramViewStudentList.size() > 0) {

                    addHappygram();

                } else {

                    finish();

                }
            }

        } else if (v.getId() == R.id.img_smile) {
            dialogSmile();
        } else if (v.getId() == R.id.img_emojies) {
            Utility.deleteDialog(mContext, " Emojies", "", new Popup() {

                @Override
                public void deleteYes() {
                    img_emojies.setVisibility(View.INVISIBLE);
                    imgcross.setVisibility(View.INVISIBLE);
                    imgClassname = "";
                }

                @Override
                public void deleteNo() {

                }

            });

        }
    }

    // dialog smiley which show custome smiley dialog
    public void dialogSmile() {

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_emojies, true);

//        final Dialog mPoweroffDialog = new Dialog(mContext);
//        mPoweroffDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mPoweroffDialog.getWindow().setFlags(
//                WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        mPoweroffDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        mPoweroffDialog.setContentView(R.layout.dialog_emojies);
//        mPoweroffDialog.setCanceledOnTouchOutside(true);
//        mPoweroffDialog.setCancelable(true);
//        mPoweroffDialog.show();


        GridView grd_emojies = (GridView) mPoweroffDialog.findViewById(R.id.grd_emojies);

        EmojiesAdapter emojieAdapter = new EmojiesAdapter(mContext);
        grd_emojies.setAdapter(emojieAdapter);

        grd_emojies.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                img_emojies.setVisibility(View.VISIBLE);
                imgcross.setVisibility(View.VISIBLE);
                img_emojies.setImageResource(emojies[position]);
                imgClassname = emojiesClassName[position];
                mPoweroffDialog.dismiss();
            }
        });
    }

    public int strngtointemojies(String str) {
        if (str != null && !str.equalsIgnoreCase("") && !str.equalsIgnoreCase("null")) {
            int pos = 0;
            try {
                pos = Integer.valueOf(str.substring(str.length() - 2, str.length()));
            } catch (NumberFormatException e) {
                pos = Integer.valueOf(str.substring(str.length() - 1));
            }
            imgClassname = emojiesClassName[pos - 1];
            return emojies[pos - 1];
        } else {
            imgClassname = emojiesClassName[0];
            return emojies[0];
        }
    }
}
