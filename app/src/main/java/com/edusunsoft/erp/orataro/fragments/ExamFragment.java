package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import static com.edusunsoft.erp.orataro.services.ServiceResource.CURRENTEXAM;

public class ExamFragment extends Fragment {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        View view = inflater.inflate(R.layout.exam_activity, null);
        mLayoutInflater = inflater;

        Initialization(view);

        return view;

    }

    private void Initialization(View view) {

        /*Set HeaderName*/

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {

                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.ExamResult) + "(" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        /*END*/

        /*commented By Krishna : 29-04-2019 - Bind Viewpager For Tab*/

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        CURRENTEXAM = 0;

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {

                    CURRENTEXAM = position;

                } else if (position == 1) {

                    CURRENTEXAM = position;

                } else if (position == 2) {

                    CURRENTEXAM = position;

                } else if (position == 3) {

                    CURRENTEXAM = position;

                } else if (position == 4) {

                    CURRENTEXAM = position;

                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }

        });

        /*END*/

    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new WeeklyExamFragment(), "Weekly");
        adapter.addFragment(new TermExamFragment(), "Terms");
        adapter.addFragment(new SemesterExamFragment(), "Semester");
        adapter.addFragment(new CBSEExamFragment(), "CBSE");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {

            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

}
