package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ExamTimingListActivity;
import com.edusunsoft.erp.orataro.model.ExamTimingModel;
import com.edusunsoft.erp.orataro.util.Global;

import java.util.ArrayList;

public class ExamTimingListAdapter extends RecyclerView.Adapter<ExamTimingListAdapter.MyViewHolder> {

    public Context mcontext;
    private ArrayList<String> dataArrayList;
    private ArrayList<ExamTimingModel> examTimingList;
    private ArrayList<ExamTimingModel> examTimingList2;

    int selectedPosition = 0;

    public ExamTimingListAdapter(Context mContext, ArrayList<String> groupExamList, ArrayList<ExamTimingModel> examList) {

        this.dataArrayList = groupExamList;
        this.examTimingList = examList;
        this.mcontext = mContext;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_exam_name;

        public MyViewHolder(View view) {

            super(view);

            txt_exam_name = (TextView) view.findViewById(R.id.txt_exam_name);

            Global.examtimingList.clear();
            view.setOnClickListener(v -> {
                selectedPosition = getLayoutPosition();
                for (int i = 0; i < examTimingList.size(); i++) {

                    String[] separated = dataArrayList.get(selectedPosition).split(" \\(");
                    Log.d("Examtiminglist1234567", separated[0]);
                    Log.d("Examtiminglist123456789", examTimingList.get(i).getExamName());
                    if (separated[0].equals(examTimingList.get(i).getExamName())) {
                        Global.examtimingList.add(examTimingList.get(i));
                    }
                }

                Log.d("Examtiminglist123", dataArrayList.get(selectedPosition).toString());
                Log.d("Examtiminglist12345",  Global.examtimingList.toString());
                mcontext.startActivity(ExamTimingListActivity.getInstance(mcontext, dataArrayList.get(selectedPosition)));

            });

        }

    }

    @Override
    public ExamTimingListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weekly_exam_list_row, parent, false);

        return new ExamTimingListAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ExamTimingListAdapter.MyViewHolder holder, int position) {

        String ExamName = dataArrayList.get(position);

        /*Fillup Detail with Validation */

        holder.txt_exam_name.setText(ExamName);

        /*END*/

    }

    @Override
    public int getItemCount() {

        return dataArrayList.size();

    }


}
