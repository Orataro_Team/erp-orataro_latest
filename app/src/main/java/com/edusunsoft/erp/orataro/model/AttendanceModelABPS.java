package com.edusunsoft.erp.orataro.model;

import com.edusunsoft.erp.orataro.services.ServiceResource;

public class AttendanceModelABPS {

	private boolean isPresent=true,isAbsant,isSikleave,isLeave;

	public AttendanceModelABPS(String s) {
		if(s != null){
			if(s.equalsIgnoreCase(ServiceResource.PRESENT)){ 
				isPresent= true;
			}else if(s.equalsIgnoreCase(ServiceResource.ABSENT)){
				isAbsant= true;
				isPresent= false;
			}else if(s.equalsIgnoreCase(ServiceResource.LEAVE)){
				isLeave = true;
				isPresent= false;
			}else if(s.equalsIgnoreCase(ServiceResource.SICKLEAVE)){
				isSikleave = true;
				isPresent= false;
			}
		}
	}

	public boolean isPresent() {
		return isPresent;
	}

	public boolean isAbsant() {
		return isAbsant;
	}

	public boolean isSikleave() {
		return isSikleave;
	}

	public boolean isLeave() {
		return isLeave;
	}

}
