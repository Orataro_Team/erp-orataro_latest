package com.edusunsoft.erp.orataro.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.OnUploadedHomeworkItemClickListener;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewUploadedHWImageActivityActivity;
import com.edusunsoft.erp.orataro.databinding.UploadedHwListRowBinding;
import com.edusunsoft.erp.orataro.model.uploadHomeworkResModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UploadedHomeworkListAdapter extends RecyclerView.Adapter<UploadedHomeworkListAdapter.RouteListAdapterHolder> {
    private ArrayList<uploadHomeworkResModel.Data> dataArrayList;
    private OnUploadedHomeworkItemClickListener onRouteItemClickListener;
    Context context;
    File file;

    public UploadedHomeworkListAdapter(Context mContext, ArrayList<uploadHomeworkResModel.Data> data, OnUploadedHomeworkItemClickListener onRouteItemClickListener) {
        this.context = mContext;
        this.dataArrayList = data;
        this.onRouteItemClickListener = onRouteItemClickListener;
    }


    @NonNull
    @Override
    public RouteListAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new RouteListAdapterHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.uploaded_hw_list_row, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RouteListAdapterHolder routeListAdapterHolder, int position) {
        routeListAdapterHolder.bindView(dataArrayList.get(position));
        routeListAdapterHolder.uploadhomeworkListRowBinding.txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> items = Arrays.asList(dataArrayList.get(position).getImageURL().split("\\s*,\\s*"));
                ServiceResource.GetHwUploadedImageList = items;
                Intent studentlistintent = new Intent(context, ViewUploadedHWImageActivityActivity.class);
                studentlistintent.putExtra("StudentReplayID", dataArrayList.get(position).getStudentReplayID());
                studentlistintent.putExtra("FileType", dataArrayList.get(position).getFileType());
                if (dataArrayList.get(position).getFileType().equalsIgnoreCase("FILE")) {
                    studentlistintent.putExtra("FileURL", ServiceResource.BASE_IMG_URL + dataArrayList.get(position).getImageURL().replace("//DataFiles//", "/DataFiles/"));
                }
                studentlistintent.putExtra("Note", dataArrayList.get(position).getNote());
                studentlistintent.putExtra("isApproved", dataArrayList.get(position).getIsApprove());
                context.startActivity(studentlistintent);

            }

        });

    }


    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

    @Override
    public int getItemCount() {
        return dataArrayList != null ? dataArrayList.size() : 0;
    }

    class RouteListAdapterHolder extends RecyclerView.ViewHolder {
        UploadedHwListRowBinding uploadhomeworkListRowBinding;

        RouteListAdapterHolder(@NonNull UploadedHwListRowBinding uploadehwListRowBinding) {
            super(uploadehwListRowBinding.getRoot());
            this.uploadhomeworkListRowBinding = uploadehwListRowBinding;
        }

        void bindView(uploadHomeworkResModel.Data data) {

            uploadhomeworkListRowBinding.txtrollno.setText(data.getRollNo());
            uploadhomeworkListRowBinding.txtstudname.setText(data.getFullName());
            uploadhomeworkListRowBinding.txtcreatedby.setText(data.getDisplayName());
            uploadhomeworkListRowBinding.txtcreatedon.setText(data.getCreatedOn());
            if (data.getStudentReplayID().equalsIgnoreCase("00000000-0000-0000-0000-000000000000")) {
                uploadhomeworkListRowBinding.txtview.setVisibility(View.INVISIBLE);
            } else {
                uploadhomeworkListRowBinding.txtview.setVisibility(View.VISIBLE);
            }

        }

    }

}
