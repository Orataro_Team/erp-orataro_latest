package com.edusunsoft.erp.orataro.model;

public class CircularNoteStudentModel {
    public String MemberID,FullName,RegistrationNo,GradeName,DivisionName,GradeID,DivisionID,FName,FContactNo;
    private boolean isSelected = false;

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getRegistrationNo() {
        return RegistrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        RegistrationNo = registrationNo;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getFContactNo() {
        return FContactNo;
    }

    public void setFContactNo(String FContactNo) {
        this.FContactNo = FContactNo;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "CircularNoteStudentModel{" +
                "MemberID='" + MemberID + '\'' +
                ", FullName='" + FullName + '\'' +
                ", RegistrationNo='" + RegistrationNo + '\'' +
                ", GradeName='" + GradeName + '\'' +
                ", DivisionName='" + DivisionName + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", FName='" + FName + '\'' +
                ", FContactNo='" + FContactNo + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
