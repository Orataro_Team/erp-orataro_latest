package com.edusunsoft.erp.orataro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class VideoViewActivity extends Activity {

    // Declare variables
    //	ProgressDialog pDialog;
    VideoView videoview;
    ImageView img_back;

    private NotificationManager mNotifyManager;
    private Notification.Builder mBuilder;
    int id = 1;

    // Insert your Video URL
//    String VideoURL = "https://archive.org/download/Architects_of_Tomorrow/2007-01-23-02-13-12.avi";
    String VideoURL;
    private TextView txtHeader;
    private ProgressBar spinnerView;
    Button btn_download;


    LinearLayout ll_download;

    QuickAction quickActionForEditOrDelete;
    ActionItem actionEdit;
    private static final int ID_SAVE = 5;


    private final int progress_bar_type = 0;
    private ProgressDialog pDialog;
    Context mContext = VideoViewActivity.this;

    /*commneted By krishna :  Declaration of videourl to be play video*/

    String VIDEOURL = "", FROMSTR;

    /*END*/

    private ArrayList<File> fileList = new ArrayList<File>();
    // OnInfoListener onInfoToPlayStateListener = null;

    public File file;

    public OnInfoListener onInfoToPlayStateListener = new OnInfoListener() {

        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {

            if (MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START == what) {

                spinnerView.setVisibility(View.GONE);

            }

            if (MediaPlayer.MEDIA_INFO_BUFFERING_START == what) {

                spinnerView.setVisibility(View.VISIBLE);

            }

            if (MediaPlayer.MEDIA_INFO_BUFFERING_END == what) {

                spinnerView.setVisibility(View.GONE);

            }

            return false;

        }

    };

    protected boolean isDownloading;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the layout from video_main.xml
        setContentView(R.layout.videoview_main);
        // Find your VideoView in your video_main.xml layout
        spinnerView = (ProgressBar) findViewById(R.id.my_spinner);
        txtHeader = (TextView) findViewById(R.id.header_text);

        if (getIntent() != null) {

            String img = getIntent().getStringExtra("img");
            String name = getIntent().getStringExtra("name");
            FROMSTR = getIntent().getStringExtra("FromStr");


            // commented by Krishna  : 31-01-2019 Check Str from where to play Video with Direct url or with Datafiles

            if (FROMSTR.equalsIgnoreCase("MyWall")) {

                VIDEOURL = ServiceResource.BASE_IMG_URL + "Datafiles/" + img.replace("//DataFiles//", "/DataFiles/")
                        .replace("//DataFiles/", "/DataFiles/");

                VideoURL = VIDEOURL;

            } else {

                VideoURL = img;

            }

            /*END*/

            try {
                txtHeader.setText(Utility.GetFirstName(mContext));
            } catch (Exception e) {
            }

//            VideoURL = img;

            // iv_fb.setImageBitmap(Utility.getBitmapFromAsset(mContext, img));

        }

        ll_download = (LinearLayout) findViewById(R.id.ll_download);
        img_back = (ImageView) findViewById(R.id.img_back);
        btn_download = (Button) findViewById(R.id.btn_download);

        saveImageMenu();

        ll_download.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                quickActionForEditOrDelete.show(ll_download);

            }

        });

        btn_download.setVisibility(View.GONE);
        btn_download.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                //				DownloadManager mgr = (DownloadManager)
                //					    VideoViewActivity.this.getSystemService(Context.DOWNLOAD_SERVICE);
                //				 Uri downloadUri = Uri.parse(VideoURL);
                //			        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                //			        request.setDescription("Downloading a file");
                //			        long id =  mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |DownloadManager.Request.NETWORK_MOBILE)
                //			                .setAllowedOverRoaming(false)
                //			                .setTitle("File Downloading...")
                //			                .setDescription("Image File Download")
                //			                .setDestinationInExternalPublicDir(Utility.getVideoFilename(fileName), fileName));
                //				Intent intent = new Intent(VideoViewActivity.this, DownloadVideoService.class);
                //		        intent.putExtra(DownloadVideoService.FILENAME, "index.html");
                //		        intent.putExtra(DownloadVideoService.URL,
                //		                VideoURL);
                //		        startService(intent);
                //				mNotifyManager = (NotificationUtil) getSystemService(Context.NOTIFICATION_SERVICE);
                //				mBuilder = new NotificationCompat.Builder(VideoViewActivity.this);
                //				mBuilder.setContentTitle("Download")
                //						.setContentText("Download in progress")
                //						.setSmallIcon(R.drawable.orataro);
                //
                //				// TODO Auto-generated method stub
                ////				new ProgressBack().execute();
                //				new DownloadFileFromURL().execute(VideoURL);
            }
        });


        img_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        videoview = (VideoView) findViewById(R.id.VideoView);
        // Execute StreamVideo AsyncTask

        // Create a progressbar
        //		pDialog = new ProgressDialog(VideoViewActivity.this);
        //		// Set progressbar title
        //		pDialog.setTitle("Please wait Video Streaming...");
        //		// Set progressbar message
        //		pDialog.setMessage("Buffering...");
        //		pDialog.setIndeterminate(false);
        //		pDialog.setCancelable(false);
        //		// Show progressbar
        //		pDialog.show();

        try {

            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    VideoViewActivity.this);
            mediacontroller.setAnchorView(videoview);

            // Get the URL from String VideoURL

            Log.d("getvideourl", VideoURL);
            Uri video = Uri.parse(VideoURL);
            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(video);
            videoview.seekTo(100);

        } catch (Exception e) {

            Log.e("Error", e.getMessage());
            e.printStackTrace();

        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {

                videoview.start();

            }

        });

        int sdk = android.os.Build.VERSION.SDK_INT;
        int jellyBean = android.os.Build.VERSION_CODES.KITKAT;

        if (sdk > jellyBean) {

            videoview.setOnInfoListener(onInfoToPlayStateListener);

        }

        videoview.setOnErrorListener(new OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                return false;

            }

        });

    }


    /**
     * Showing Dialog
     */


    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {

            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(mContext.getResources().getString(R.string.pleasewait) + mContext.getResources().getString(R.string.dowloadFile));
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;

            default:

                return null;

        }

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        //        private ProgressDialog pDialog;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //            pDialog = new ProgressDialog(VideoViewActivity.this);
            //            pDialog.setMessage("Downloading file. Please wait...");
            //            pDialog.setIndeterminate(false);
            //            pDialog.setMax(100);
            //            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            //            pDialog.setCancelable(true);
            //            pDialog.show();
            mBuilder.setProgress(100, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream("/sdcard/demo.mp4");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);

                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            mBuilder.setContentTitle("Download")
                    .setContentText("Download in progress" + Integer.parseInt(progress[0]))
                    .setSmallIcon(R.drawable.orataro);
            mBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
            mNotifyManager.notify(id, mBuilder.build());
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            mBuilder.setContentText("Download complete");
            // Removes the progress bar
            mBuilder.setProgress(0, 0, false);
            mNotifyManager.notify(id, mBuilder.build());
            //        	pDialog.dismiss();
            // Displaying downloaded image into image view
            // Reading image path from sdcard
            String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            //            my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }

    }

    class ProgressBack extends AsyncTask<String, String, String> {
        ProgressDialog PD;

        @Override
        protected void onPreExecute() {
            PD = ProgressDialog.show(VideoViewActivity.this, null, mContext.getResources().getString(R.string.pleasewait), true);
            PD.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... arg0) {
            DownloadFile(VideoURL, "demo.mp4");
            return VideoURL;

        }

        protected void onPostExecute(Boolean result) {
            PD.dismiss();

        }

    }

    public void DownloadFile(String fileURL, String fileName) {
        try {
            String RootDir = Environment.getExternalStorageDirectory()
                    + File.separator + "Video";
            File RootFile = new File(RootDir);
            RootFile.mkdir();
            // File root = Environment.getExternalStorageDirectory();
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(new File(RootFile,
                    fileName));
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;

            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();


        } catch (Exception e) {

            Log.d("Error....", e.toString());//`enter code here`
        }

    }


    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {


                fileList.add(listFile[i]);


            }
        }
        return fileList;
    }

    private void saveImageMenu() {


        actionEdit = new ActionItem(ID_SAVE, "Save", mContext
                .getResources().getDrawable(R.drawable.save31));


        quickActionForEditOrDelete = new QuickAction(mContext,
                QuickAction.VERTICAL);

        quickActionForEditOrDelete.addActionItem(actionEdit);
        //		quickActionForEditOrDelete.addActionItem(actionDelete);

        quickActionForEditOrDelete
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int _pos,
                                            int actionId) {
                        ActionItem actionItem = quickActionForEditOrDelete
                                .getActionItem(_pos);

                        if (actionId == ID_SAVE) {

                            saveVideo();

                        }
                    }


                });


        quickActionForEditOrDelete
                .setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        // Toast.makeText(context, "Dismissed",
                        // Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void saveVideo() {

        String fileName = System.currentTimeMillis() + ".mp4";
        String[] fNamearray = VideoURL.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];

        }

        File f = new File(Utility.getVideoFilename(""));
        getfile(f);

        for (int i = 0; i < fileList.size(); i++) {

            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }

        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager)
//                VideoViewActivity.this.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(VideoURL);
//
//        Uri destination = Uri.fromFile(new File(Utility.getVideoFilename(fileName)));

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(VideoURL);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);

//        DownloadManager.Request request =
//                new DownloadManager.Request(source);
//        request.setTitle("Orataro Video");
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager
//                .Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//
//        long id = mgr.enqueue(request);
//        Utility.toast(mContext, getResources().getString(R.string.downloadstarting));

    }


}
