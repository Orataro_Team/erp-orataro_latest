package com.edusunsoft.erp.orataro.model;

public class PropertyVo {

	String name;
	Object value;
	
	
	public PropertyVo(String name, Object value) {
		// TODO Auto-generated constructor stub
		setName(name);
		setValue(value);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "PropertyVo{" +
				"name='" + name + '\'' +
				", value=" + value +
				'}';
	}
}
