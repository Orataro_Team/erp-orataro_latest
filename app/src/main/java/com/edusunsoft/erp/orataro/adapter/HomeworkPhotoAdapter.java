package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.HomeworkZoomImageActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.List;

public class HomeworkPhotoAdapter extends BaseAdapter {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private ImageView iv_profile_pic;
    private LinearLayout ll_option;
    private LinearLayout albumDetaillayout;
    private TextView txt_title, txtDetail;
    private List<String> photoModels;
    private static final int ID_LIKE = 1;
    private static final int ID_UNLIKE = 2;
    private static final int ID_FRIEND = 3;
    private static final int ID_PUBLIC = 4;
    private static final int ID_ONLY_ME = 5;
    private ActionItem actionLike, actionUnlike, actionPublic, actionFriend, actionOnlyMe;
    private QuickAction quickAction;
    private int flag = 0;


    public HomeworkPhotoAdapter(Context mContext, List<String> getHwUploadedImageList) {

        this.mContext = mContext;
        this.photoModels = getHwUploadedImageList;
    }

    @Override
    public int getCount() {
        return photoModels == null ? 0 : photoModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.photos_listraw, parent, false);
        ll_option = (LinearLayout) convertView.findViewById(R.id.ll_option);
        iv_profile_pic = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        albumDetaillayout = (LinearLayout) convertView.findViewById(R.id.albumDetaillayout);
        albumDetaillayout.setVisibility(View.GONE);
        txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        txtDetail = (TextView) convertView.findViewById(R.id.txt_desc);

        String photo = photoModels.get(position);

        Log.d("gethomeworkphoto", photoModels.get(position));


        // Advanced usage
//        ImageLoader.with(mContext)
//                /*Create new load request for specified data.
//                  Data types supported by default: URIs (remote and local),
//                  files, file descriptors, resources and byte arrays.*/
//                .from(ServiceResource.BASE_IMG_URL + photo)
//                /*Required image size (to load sampled bitmaps)*/
//                .size(1000, 1000)
//                /*Display loaded image with rounded corners, optionally, specify corner radius*/
//
//                /*Placeholder drawable*/
//                .placeholder(new ColorDrawable(Color.LTGRAY))
//                /*Error drawable*/
//                .errorDrawable(R.drawable.ic_launcher)
//                /*Apply transformations*/
//                .transform(ImageUtils.cropCenter())
//                .transform(ImageUtils.convertToGrayScale())
//                /*Load image into view*/
//                .load(iv_profile_pic);


//        Glide.with(mContext).asBitmap()
//                //[for new glide versions]
//                .load(ServiceResource.BASE_IMG_URL + photo)
//                //.asBitmap()[for older glide versions]
//                //.placeholder(R.drawable.default_placeholder)
//                .override(1600, 1600) // Can be 2000, 2000
//                .into(new BitmapImageViewTarget(iv_profile_pic) {
//                    @Override
//                    public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
//                        super.onResourceReady(drawable, anim);
//                        progressBar.setVisibility(View.GONE);
//                    }
//                });

//        Glide.with(mContext).load(ServiceResource.BASE_IMG_URL + photo)
//                .placeholder(R.drawable.photo)
//                .into(iv_profile_pic);


//        Picasso.get().load(ServiceResource.BASE_IMG_URL + photo).placeholder(R.drawable.photo).
//                into(iv_profile_pic);

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL + photo)
                    .thumbnail(.1f)
                    .apply(options)
                    .into(iv_profile_pic);

        } catch (Exception e) {
            e.printStackTrace();
        }

        iv_profile_pic.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent in = new Intent(mContext, HomeworkZoomImageActivity.class);
                in.putExtra("img", ServiceResource.BASE_IMG_URL + photoModels.get(position));
                in.putExtra("name", "Photo");
                mContext.startActivity(in);

            }

        });

        return convertView;
    }

}
