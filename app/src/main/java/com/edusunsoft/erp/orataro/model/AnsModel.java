package com.edusunsoft.erp.orataro.model;

public class AnsModel {
	
	String ansStr,percentage;

	public String getAnsStr() {
		return ansStr;
	}

	public void setAnsStr(String ansStr) {
		this.ansStr = ansStr;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

}
