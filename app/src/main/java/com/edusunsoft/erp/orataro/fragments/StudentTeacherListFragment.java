package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.StudentTeacherAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.PTCommMemberModel;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.database.StudentMemberListDataDao;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.MemberModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StudentTeacherListFragment extends Fragment implements ResponseWebServices, OnClickListener {

    StdDivSubModel model;
    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    private Context mContext;
    LinearLayout addLayout;
    PullAndLoadListView homeworklist;
    private View header;
    private static final String modelStr = "model";
    LinearLayout ll_header_std;

    // variable declaration for studentmember list from ofline database
    StudentMemberListDataDao studentMemberListDataDao;
    private List<com.edusunsoft.erp.orataro.database.PTCommMemberModel> studentMemberList = new ArrayList<>();
    com.edusunsoft.erp.orataro.database.PTCommMemberModel PTCommMemberModel = new PTCommMemberModel();
    public String ISDataExist = "";
    /*END*/

    public static final StudentTeacherListFragment newInstance(
            StdDivSubModel model, String isFrom) {
        StudentTeacherListFragment f = new StudentTeacherListFragment();
        Bundle bdl = new Bundle(2);
        bdl.putSerializable(modelStr, model);
        f.setArguments(bdl);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.homeworklist_fragment, null);

        mContext = getActivity();
        model = (StdDivSubModel) getArguments().getSerializable("model");

        addLayout = (LinearLayout) v.findViewById(R.id.ll_add_new);
        addLayout.setVisibility(View.GONE);
        ll_header_std = (LinearLayout) v.findViewById(R.id.ll_header_std);
        homeworklist = (PullAndLoadListView) v.findViewById(R.id.homeworklist);

        studentMemberListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).studentMemberListDataDao();

        if (Utility.isTeacher(mContext)) {
            ll_header_std.setVisibility(View.VISIBLE);
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.StudentList));
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 70, 0);
            }
        } else {
            ll_header_std.setVisibility(View.GONE);
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.TeacherList));
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 70, 0);
            }
        }


        homeworklist.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {

                if (Utility.isTeacher(mContext)) {

                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.StudentList));
                    if (HomeWorkFragmentActivity.txt_header != null) {
                        HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.StudentList));
                        HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 70, 0);
                    }
                    // display StudentList from offline database
                    GetStudentList();
                    /*END*/
                } else {

                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.TeacherList));
                    if (HomeWorkFragmentActivity.txt_header != null) {
                        HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.TeacherList));
                        HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 70, 0);
                    }
                    // display StudentList from offline database
                    GetTeacherList();
                    /*END*/
                }

            }

        });

        return v;

    }

    @Override
    public void onResume() {

        super.onResume();

        if (Utility.isTeacher(mContext)) {

            // display StudentList from offline database
            GetStudentList();
            /*END*/
        } else {

            // display StudentList from offline database
            GetTeacherList();
            /*END*/

        }

    }

    private void GetTeacherList() {

        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            studentMemberList = studentMemberListDataDao.getStudentMemberList("ForStudent");
            if (studentMemberList.isEmpty() || studentMemberList.size() == 0 || studentMemberList == null) {
                getTeachherList(true);
            } else {
                //set Adapter
                setStudentMemberlistAdapter(studentMemberList);
                /*END*/
                getTeachherList(false);
            }
        } else {
            studentMemberList = studentMemberListDataDao.getStudentMemberList("ForStudent");
            if (studentMemberList != null) {
                setStudentMemberlistAdapter(studentMemberList);
            }

        }

    }

    private void GetStudentList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            studentMemberList = studentMemberListDataDao.getStudentMemberList("ForTeacher");
            if (studentMemberList.isEmpty() || studentMemberList.size() == 0 || studentMemberList == null) {
                getStudentList(true);
            } else {
                //set Adapter
                setStudentMemberlistAdapter(studentMemberList);
                /*END*/
                getStudentList(false);
            }
        } else {
            studentMemberList = studentMemberListDataDao.getStudentMemberList("ForTeacher");
            if (studentMemberList != null) {
                setStudentMemberlistAdapter(studentMemberList);
            }
        }
    }

    private void setStudentMemberlistAdapter(List<com.edusunsoft.erp.orataro.database.PTCommMemberModel> studentMemberList) {

        if (studentMemberList != null && studentMemberList.size() > 0) {

            StudentTeacherAdapter adapter = new StudentTeacherAdapter(mContext, studentMemberList, model);
            homeworklist.setAdapter(adapter);

        }

    }

    private void getTeachherList(boolean isViewPopup) {
        // TODO Auto-generated method stub
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.PTCOMMUNICATIONGETTEACHERLIST_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);
    }

    private void getStudentList(boolean isViewPopup) {

        // TODO Auto-generated method stub

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, model.getStandrdId()));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, model.getDivisionId()));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, model.getSubjectId()));

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.PTCOMMUNICATIONGETSTUDENTLIST_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            getActivity().finish();
        }
    }

    @Override
    public void response(String result, String methodName) {
        if (methodName.equalsIgnoreCase(ServiceResource.PTCOMMUNICATIONGETSTUDENTLIST_METHODNAME)) {
            parsestudentlist(result);
        } else if (methodName.equalsIgnoreCase(ServiceResource.PTCOMMUNICATIONGETTEACHERLIST_METHODNAME)) {
            parseteacherlist(result);
        }
    }

    private void parsestudentlist(String result) {
        // TODO Auto-generated method stub

        JSONArray jsonObj;
        studentMemberListDataDao.deletememberlist("ForTeacher");
        try {
            jsonObj = new JSONArray(result);
            Global.ptTeacherlistmodels = new ArrayList<MemberModel>();
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                // parse homework list data
                ParseStudentMemberList(innerObj);
                /*END*/
            }

            if (studentMemberListDataDao.getStudentMemberList("ForTeacher").size() > 0) {
                //set Adapter
                setStudentMemberlistAdapter(studentMemberListDataDao.getStudentMemberList("ForTeacher"));
                /*END*/
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void ParseStudentMemberList(JSONObject innerObj) {
        try {
            PTCommMemberModel.setMemberID(innerObj.getString(ServiceResource.PTMEMBERID));
            PTCommMemberModel.setFullName(innerObj.getString(ServiceResource.PTFULLNAME));
            PTCommMemberModel.setProfilePicture(innerObj.getString(ServiceResource.PTPROFILEPICTURE));
            PTCommMemberModel.setRegistrationNo(innerObj.getString(ServiceResource.PTREGISTRATIONNO));
            PTCommMemberModel.setRollNo(innerObj.getString(ServiceResource.PTROLLNO));
            PTCommMemberModel.setForMember("ForTeacher");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        studentMemberListDataDao.insertStudentMemberList(PTCommMemberModel);
    }

    private void parseteacherlist(String result) {
        // TODO Auto-generated method stub

        JSONArray jsonObj;
        studentMemberListDataDao.deletememberlist("ForStudent");
        try {
            jsonObj = new JSONArray(result);
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);

                // parse homework list data
                ParseTeacherMemberList(innerObj);
                /*END*/
            }

            if (studentMemberListDataDao.getStudentMemberList("ForStudent").size() > 0) {
                //set Adapter
                setStudentMemberlistAdapter(studentMemberListDataDao.getStudentMemberList("ForStudent"));
                /*END*/
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//
//        if (Global.ptTeacherlistmodels != null && Global.ptTeacherlistmodels.size() > 0) {
//
//            StudentTeacherAdapter adapter = new StudentTeacherAdapter(mContext, Global.ptTeacherlistmodels, model);
//            homeworklist.setAdapter(adapter);
//
//        }

    }

    private void ParseTeacherMemberList(JSONObject innerObj) {
        try {
            PTCommMemberModel.setMemberID(innerObj.getString(ServiceResource.PTMEMBERID));
            PTCommMemberModel.setFullName(innerObj.getString(ServiceResource.PTFULLNAME));
            PTCommMemberModel.setProfilePicture(innerObj.getString(ServiceResource.PTPROFILEPICTURE));
            PTCommMemberModel.setRegistrationNo(innerObj.getString(ServiceResource.PTREGISTRATIONNO));
            PTCommMemberModel.setSubjectID(innerObj.getString(ServiceResource.PTSUBJECTID));
            PTCommMemberModel.setSubjectName(innerObj.getString(ServiceResource.PTSUBJECTNAME));
            PTCommMemberModel.setForMember("ForStudent");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        studentMemberListDataDao.insertStudentMemberList(PTCommMemberModel);
    }

}
