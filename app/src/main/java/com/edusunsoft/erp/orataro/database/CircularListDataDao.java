package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CircularListDataDao {

    @Insert
    void insertCircularList(CircularListModel circularListModel);

    @Query("SELECT * from CircularList")
    List<CircularListModel> getCircularList();

    @Query("DELETE  FROM CircularList")
    int deleteCircularListItem();

    @Query("delete from CircularList where CircularID=:circularid")
    void deletecircularItemByCircularID(String circularid);

}
