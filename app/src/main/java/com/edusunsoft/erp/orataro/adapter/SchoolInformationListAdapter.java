package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewSchoolInfoDetailsActivity;
import com.edusunsoft.erp.orataro.model.SchoolInformationModel;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class SchoolInformationListAdapter extends BaseAdapter {
	
	private LayoutInflater layoutInfalater;
	private Context context;
	private TextView txt_title, txt_info;
	private ArrayList<SchoolInformationModel> schoolInformationModels = new ArrayList<>();
	private ArrayList<SchoolInformationModel> copyList = new ArrayList<>();

	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	public SchoolInformationListAdapter(Context context, ArrayList<SchoolInformationModel> schoolInformationModels) {
		this.context = context;
		this.schoolInformationModels = schoolInformationModels;
		copyList = new ArrayList<SchoolInformationModel>();
		copyList.addAll(schoolInformationModels);
	}

	@Override
	public int getCount() {
		return schoolInformationModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.school_info_listraw, parent, false);
		convertView.setBackgroundColor(colors[position % colors.length]);
		txt_title = (TextView) convertView.findViewById(R.id.txt_title);
		txt_info = (TextView) convertView.findViewById(R.id.txt_info);
		txt_title.setText(schoolInformationModels.get(position).getTitle());

		if(Utility.isNull(schoolInformationModels.get(position).getInformation())){
			if(schoolInformationModels.get(position).getInformation().length()> Constants.CONTINUEREADINGSIZE){
			String continueReadingStr = Constants.CONTINUEREADINGSTR;
			String tempText =schoolInformationModels.get(position).getInformation().substring(0, Constants.CONTINUEREADINGSIZE )+continueReadingStr;
			SpannableString text = new SpannableString(tempText);
			text.setSpan(new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR), Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZE+continueReadingStr.length(), 0);
			MyClickableSpan clickfor = new MyClickableSpan(tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {
				    
				   @Override
				   public void onClick(View widget) {
					   Intent intent = new Intent(context, ViewSchoolInfoDetailsActivity.class);
						intent.putExtra("Title", schoolInformationModels.get(position).getTitle());
						intent.putExtra("Info", schoolInformationModels.get(position).getInformation());
						context.startActivity(intent);
				   }
				  };
				  text.setSpan(clickfor, Constants.CONTINUEREADINGSIZE,  Constants.CONTINUEREADINGSIZE+continueReadingStr.length(), 0);
				  txt_info.setMovementMethod(LinkMovementMethod.getInstance());
				  txt_info.setText(text, BufferType.SPANNABLE);
		}else{
			txt_info.setText(schoolInformationModels.get(position).getInformation());	
		}

		}
		
		txt_info.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent intent = new Intent(context, ViewSchoolInfoDetailsActivity.class);
					intent.putExtra("Title", schoolInformationModels.get(position).getTitle());
					intent.putExtra("Info", schoolInformationModels.get(position).getInformation());
					context.startActivity(intent);
			}
		});

		return convertView;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		schoolInformationModels.clear();
		if (charText.length() == 0) {
			schoolInformationModels.addAll(copyList);
		} else {
			for (SchoolInformationModel vo : copyList) {
				if (vo.getInformation().toLowerCase(Locale.getDefault()).contains(charText)) {
					schoolInformationModels.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}

}
