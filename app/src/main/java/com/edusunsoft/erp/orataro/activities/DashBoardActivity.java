package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.adapter.DashBoardGridAdapter;
import com.edusunsoft.erp.orataro.database.DashboardMenuDataDao;
import com.edusunsoft.erp.orataro.database.DashboardMenuModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.MenuPermissionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.edusunsoft.erp.orataro.util.Global.menuPermissionList;

import java.util.ArrayList;

public class DashBoardActivity extends Activity implements OnClickListener, RefreshListner, ResponseWebServices {

    private GridView gridView;
    private DashBoardGridAdapter adapter;
    private ImageView img_power_off;
    private Context mContext;
    private Dialog mPoweroffDialog;
    private ImageView img_back;
    CircleImageView iv_school_icon;
    private TextView txt_instituteName;

    MenuPermissionModel menuPermissionModel;

    // variable declaration for dashboadmenu
    DashboardMenuModel dashboardMenuModel = new DashboardMenuModel();
    DashboardMenuDataDao dashboardMenuDataDao;
    private List<DashboardMenuModel> dashboardmenuList = new ArrayList<>();

    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        mContext = DashBoardActivity.this;

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(null)
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("null")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("NAN")) {

            new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
            new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
            Intent intent = new Intent(mContext, RegisterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            new UserSharedPrefrence(mContext).clearPrefrence();
            startActivity(intent);
            finish();

        }

        gridView = (GridView) findViewById(R.id.dashboard_gridView);
        txt_instituteName = (TextView) findViewById(R.id.txt_instituteName);
        img_back = (ImageView) findViewById(R.id.img_back);

        img_power_off = (ImageView) findViewById(R.id.img_power_off);
        iv_school_icon = (CircleImageView) findViewById(R.id.iv_school_icon);

        // initialization for offline support of dashboard menu
        dashboardMenuDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).dashboardMenuDataDao();

        if (Utility.isNetworkAvailable(mContext)) {

            try {
                CheckAPIForMaintenance(new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            } catch (Exception e) {
                e.printStackTrace();
            }

            GetUserRoleRightList();
            // call api to get dashboardmenu
            dashboardmenuList = dashboardMenuDataDao.getDashboardMenuList();
            if (dashboardmenuList.size() > 0) {
                DisplayDashboardMenu(dashboardmenuList);
                GetMenuPermissionList(false);
            } else {
                GetMenuPermissionList(true);
            }


        } else {

            // DisplayDashboardMenu from offline database if internet not available
            dashboardmenuList = dashboardMenuDataDao.getDashboardMenuList();
            if (dashboardmenuList.size() > 0) {
                DisplayDashboardMenu(dashboardmenuList);
            }
            /*END*/
            RollWriteParsing(Utility.readFromFile(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, mContext));
        }

        Log.d("getOTPNumber", new UserSharedPrefrence(mContext).getOTP());
        Log.d("getMobileNumber", new UserSharedPrefrence(mContext).get_MOBILENUMBER());


        try {

            /*commented By Krishna : 24-06-2019 - Change InstituteWallLogo Url */

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            //remove image base url in load() by hardik_kanak 26-11-2020
            Glide.with(mContext)
                   // .load(ServiceResource.BASE_IMG_URL1 + new UserSharedPrefrence(mContext).getLoginModel().getInstituteLogo().replaceAll("//img/", "img/"))
                    .load(new UserSharedPrefrence(mContext).getLoginModel().getInstituteLogo().replaceAll("//img/", "img/"))
                    .apply(options)
                    .into(iv_school_icon);

            /*END*/


            // Commented By Krishna : - 4-07-2019 - API for Menu Permission
            if (Utility.isNetworkAvailable(mContext)) {
                // Call Login response API for Refresh App Permission of User EveryDay at Once.
                if (new UserSharedPrefrence(mContext).getDATE_FOR_API_CALL_ONCE().equalsIgnoreCase(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"))) {
                } else {
//                    GetLoginResponse();
                    CheckAPIForUserLoggedIn(UserSharedPrefrence.loadLoginData(mContext).getUserID(), "", "");
                }
            } else {
                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

            /*END*/
            txt_instituteName.setText(new UserSharedPrefrence(mContext).getLoginModel().getInstituteName());

        } catch (Exception e) {
            e.printStackTrace();
        }

        img_power_off.setOnClickListener(this);
        img_power_off.setVisibility(View.INVISIBLE);
        img_back.setImageResource(R.drawable.langchange);
        img_back.setOnClickListener(this);

    }

    private void CheckAPIForMaintenance(String clientID) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, clientID));

        Log.d("userselectionrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this, false).execute(ServiceResource.CHECK_CLIENT_IS_IN_MAINTENANCE, ServiceResource.LOGIN_URL);

    }

    private void DisplayDashboardMenu(List<DashboardMenuModel> dashboardmenuList) {

        menuPermissionList.clear();

        for (int i = 0; i < dashboardmenuList.size(); i++) {

            if (Utility.isTeacher(mContext)) {

                if (dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Fees")
                        || dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Receipt")
                        || dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Exam Result")
                        || dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Pocket Money")
                        || dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Transportation")
                        || dashboardmenuList.get(i).getMenuName().equalsIgnoreCase("Exam Timing")) {

                } else {

                    menuPermissionModel = new MenuPermissionModel();

                    if (dashboardmenuList.get(i).IsView == true) {
                        if (dashboardmenuList.get(i).getDefaultValue().equalsIgnoreCase(getResources().getString(R.string.pocketmoney))) {
                            if (!new UserSharedPrefrence(mContext).getLoginModel().getStudentHostelRegID().equalsIgnoreCase("")) {
                                menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                                menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                                menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                                menuPermissionModel.setStatic(false);
                                menuPermissionList.add(menuPermissionModel);
                            }
                        } else if (dashboardmenuList.get(i).getDefaultValue().equalsIgnoreCase(getResources().getString(R.string.transpotation))) {
                            if (new UserSharedPrefrence(mContext).getLoginModel().getStudentBusRegMasterID().equalsIgnoreCase("")) {
                                menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                                menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                                menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                                menuPermissionModel.setStatic(false);
                                menuPermissionList.add(menuPermissionModel);
                            }
                        } else {
                            menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                            menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                            menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                            menuPermissionModel.setStatic(false);
                            menuPermissionList.add(menuPermissionModel);
                        }

                    }

                }

            } else {

                menuPermissionModel = new MenuPermissionModel();

                if (dashboardmenuList.get(i).isView() == true) {
                    if (dashboardmenuList.get(i).getDefaultValue().equalsIgnoreCase(getResources().getString(R.string.pocketmoney))) {

                        if (new UserSharedPrefrence(mContext).getLoginModel().getStudentHostelRegID().equalsIgnoreCase("")
                                || new UserSharedPrefrence(mContext).getLoginModel().getStudentHostelRegID().equalsIgnoreCase("null")
                                || new UserSharedPrefrence(mContext).getLoginModel().getStudentHostelRegID().equalsIgnoreCase(null)) {
                        } else {
                            menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                            menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                            menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                            menuPermissionModel.setStatic(false);
                            menuPermissionList.add(menuPermissionModel);
                        }

                    } else if (dashboardmenuList.get(i).getDefaultValue().equalsIgnoreCase("Transportation")) {
                        if (new UserSharedPrefrence(mContext).getLoginModel().getStudentBusRegMasterID().equalsIgnoreCase("null")) {
                        } else {
                            menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                            menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                            menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                            menuPermissionModel.setStatic(false);
                            menuPermissionList.add(menuPermissionModel);
                        }
                    } else {
                        menuPermissionModel.setMenuName(dashboardmenuList.get(i).getMenuName());
                        menuPermissionModel.setMenuIconName(dashboardmenuList.get(i).getDefaultValue());
                        menuPermissionModel.setView(dashboardmenuList.get(i).isView());
                        menuPermissionModel.setStatic(false);
                        menuPermissionList.add(menuPermissionModel);
                    }

                }

            }

        }
        if (new UserSharedPrefrence(mContext).getISMULTIPLE()) {
            menuPermissionList.add(menuPermissionList.size(), new MenuPermissionModel(mContext.getResources().getString(R.string.SwitchAccount), "Switch Account", true, true));
        }

        menuPermissionList.add(0, new MenuPermissionModel(mContext.getResources().getString(R.string.Profile), "profile", true, true));
        menuPermissionList.add(1, new MenuPermissionModel(mContext.getResources().getString(R.string.Wall), "wall", true, true));

        // Commented By Krishna : 10-07-2020 - Remove Notification menu which unused.
//        menuPermissionList.add(new MenuPermissionModel(mContext.getResources().getString(R.string.Notification), "notification", true, true));
        /*END*/
        menuPermissionList.add(new MenuPermissionModel(mContext.getResources().getString(R.string.Aboutorataro), "aboutorataro", true, true));
        // Commented By Hardik Kanak : 06-02-2021 - Remove faq
        //menuPermissionList.add(new MenuPermissionModel(mContext.getResources().getString(R.string.faq), "faq", true, true));
        menuPermissionList.add(new MenuPermissionModel(mContext.getResources().getString(R.string.notificationsettings), "settings", true, true));

        if (menuPermissionList.size() > 0) {
            adapter = new DashBoardGridAdapter(DashBoardActivity.this, this, menuPermissionList);
            gridView.setAdapter(adapter);
        }

    }

    private void CheckAPIForUserLoggedIn(String userID, String device_identity, String device_type) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, userID));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_IDENTITY, device_identity));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, device_type));

        Log.d("userselectionrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this, false).execute(ServiceResource.USER_SELECTION, ServiceResource.REGISTATION_URL);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Exit from application
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Utility.showToast("Please click BACK again to exit", mContext);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        /*END*/
    }

    private void GetLoginResponse() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, new UserSharedPrefrence(mContext).get_MOBILENUMBER()));
        arrayList.add(new PropertyVo(ServiceResource.OTPNumber, new UserSharedPrefrence(mContext).getOTP()));
        Log.d("otprequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, false, this, false).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN, ServiceResource.REGISTATION_URL);

    }

    private void GetMenuPermissionList(boolean isLoader) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        Log.d("menurequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isLoader, this, false, false).execute(ServiceResource.MENUPERMISSION_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void GetUserRoleRightList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, new UserSharedPrefrence(mContext).getLoginModel().getRoleID()));
        new AsynsTaskClass(mContext, arrayList, false, this, false, false).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:
                Intent i1 = new Intent(DashBoardActivity.this, ChangeLangActivity.class);
                i1.putExtra("isFrom", ServiceResource.FROMDASHBOARD);
                startActivity(i1);
                finish();
                break;

            case R.id.img_power_off:

                Constants.ForDialogStyle = "Logout";

                mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, true);

                LinearLayout ll_submit = (LinearLayout) mPoweroffDialog.findViewById(R.id.ll_submit);

                ll_submit.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
                        new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
                        Intent intent = new Intent(mContext, RegisterActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        new UserSharedPrefrence(mContext).clearPrefrence();
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    }

                });

                ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);

                img_close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mPoweroffDialog.dismiss();
                    }
                });
                break;
            default:
                break;

        }

    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            finish();
//            return true;
//        }
//
//        return super.onKeyDown(keyCode, event);
//
//    }


    @Override
    public void refresh(String methodName) {

    }

    public void RollWriteParsing(String result) {
        try {
            JSONArray jsonObj = new JSONArray(result);
            Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
            ReadWriteSettingModel model;
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                model = new ReadWriteSettingModel();
                model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));
                if (!new UserSharedPrefrence(mContext).getLoginModel().getRoleName().equalsIgnoreCase("teacher")) {
                    model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                    model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                    model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                    model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));
                } else {
                    model.setIsView("true");
                    model.setIsEdit("true");
                    model.setIsCreate("true");
                    model.setIsDelete("true");
                }

                Global.readWriteSettingList.add(model);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("GetResult1", result);
            Utility.writeToFile(result, ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, mContext);
            RollWriteParsing(result);

        } else if (ServiceResource.CHECK_CLIENT_IS_IN_MAINTENANCE.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            Log.d("getResult", result);
            try {
                jsonObj = new JSONArray(result);
                JSONObject innerObj = jsonObj.getJSONObject(0);

                boolean isinMaintenance = innerObj.getBoolean(ServiceResource.INMAINTENANCE);
                if (isinMaintenance == true) {
                    OpenDialogForMaintenanceMessage();
                } else {
                    Log.d("yes", "no");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (ServiceResource.USER_SELECTION.equalsIgnoreCase(methodName)) {

            Log.d("GetResult2", result);
            if (result.contains("[{\"message\":\"No data found...!!!\",\"success\":0}]")) {
                Utility.Longtoast(mContext, "No Data Found");
            } else {
                JSONArray jsonObj;
                try {
                    new UserSharedPrefrence(mContext).setDATE_FOR_API_CALL_ONCE(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));
                    jsonObj = new JSONArray(result);
                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        if (Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID).equalsIgnoreCase(innerObj.getString(ServiceResource.DEVICE_IDENTITY))) {
                        } else {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, mContext.getResources().getString(R.string.loginintootherdevice));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        } else if (ServiceResource.MENUPERMISSION_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("GetResult3", result);
            parseMenuPermission(result);

        } else if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN.equalsIgnoreCase(methodName)) {

            Global.userdataModel = new LoginModel();
            Global.userdataList = new ArrayList<>();

            JSONArray jsonObj;

            try {

                new UserSharedPrefrence(mContext).setDATE_FOR_API_CALL_ONCE(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));

                jsonObj = new JSONArray(result);

                Global.userdataModel.setLoginJson(result);
                PreferenceData.saveLoginUserData(result);

                Log.d("getData", PreferenceData.getLoginUserData().toString());
                Log.d("result123", result);

                if (jsonObj.length() == 1) {

                    PreferenceData.setLogin(true);

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        String Message = innerObj.getString(ServiceResource.MESSAGE);
                        if (Message.equalsIgnoreCase("No Data Found")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, Message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (Message.equalsIgnoreCase("Invalid Mobile Number And OTP Number...!!!")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, mContext.getResources().getString(R.string.loginintootherdevice));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {

                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            Global.userdataModel.setLoginJson(result);
                            new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);

                            PreferenceData.saveLoginUserData(result);
                            Log.d("getData", PreferenceData.getLoginUserData().toString());
                            Log.d("result123", result);

                        }


                    }


                } else {

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);
                        String Message = innerObj.getString(ServiceResource.MESSAGE);
                        if (Message.equalsIgnoreCase("No Data Found")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, Message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Message.equalsIgnoreCase("Invalid Mobile Number And OTP Number...!!!")) {
                            try {
                                RemoveSavedLoginData();
                                Utility.Longtoast(mContext, mContext.getResources().getString(R.string.loginintootherdevice));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            PreferenceData.saveLoginUserData(result);
                            Global.userdataList.add(Global.userdataModel);
                            for (int k = 0; k < Global.userdataList.size(); k++) {

                                if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(Global.userdataList.get(k).getUserID())) {

                                    new UserSharedPrefrence(mContext).setLoginModel(Global.userdataList.get(k));
                                    Global.userdataModel = Global.userdataList.get(k);

                                }

                            }

                        }

                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private void OpenDialogForMaintenanceMessage() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.error_dialog);
        TextView tv_alert_title = (TextView) dialog.findViewById(R.id.tv_alert_title);
        TextView txtdetail = (TextView) dialog.findViewById(R.id.txtdetail);
        TextView txtno = (TextView) dialog.findViewById(R.id.txtno);
        tv_alert_title.setText(getResources().getString(R.string.maintenance));
        txtdetail.setText(getResources().getString(R.string.maintenance_message));
        TextView txtyes = (TextView) dialog.findViewById(R.id.txtyes);
        txtyes.setText(getResources().getString(R.string.ok));
        txtno.setText(getResources().getString(R.string.Later));
        txtno.setVisibility(View.GONE);
        txtyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                try {
                    CheckAPIForMaintenance(new UserSharedPrefrence(mContext).getLoginModel().getClientID());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

        dialog.show();

    }

    private void RemoveSavedLoginData() {

        new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
        new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
        Intent intent = new Intent(mContext, RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        new UserSharedPrefrence(mContext).clearPrefrence();
        SharedPreferences preferences = mContext.getSharedPreferences("LOGIN_USER_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);

    }

    private void parseMenuPermission(String result) {

        JSONArray jsonArray;

        try {

            if (result.contains("\"success\":0")) {

            } else {

                Log.d("getMenuResult", result);

                menuPermissionList.clear();
                dashboardMenuDataDao.deleteDashboardMenu();
                jsonArray = new JSONArray(result);

                for (int i = 0; i < jsonArray.length(); i++) {

                    //TODO need to parse new response.
                    JSONObject hJsonObject = jsonArray.getJSONObject(i);

                    dashboardMenuModel.setDefaultValue(hJsonObject.getString(ServiceResource.MENU_DEFAULT_VALUE));
                    dashboardMenuModel.setMenuName(hJsonObject.getString(ServiceResource.MENUNAME));
                    dashboardMenuModel.setView(hJsonObject.getBoolean(ServiceResource.MENUVIEW));

                    /*insert dashboardmenulist into database.*/
                    dashboardMenuDataDao.insertDashboardMenuList(dashboardMenuModel);
                    /*END*/

                }


                // DisplayDashboardMenu from offline database
                dashboardmenuList = dashboardMenuDataDao.getDashboardMenuList();
                DisplayDashboardMenu(dashboardmenuList);
                /*END*/

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
