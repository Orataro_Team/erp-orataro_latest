package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FQAModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class FQAListAdapter  extends BaseExpandableListAdapter {
	Context mContext;
	List<FQAModel> list;

	private int[] colors2 = new int[] { Color.parseColor("#F2F2F2"),
			Color.parseColor("#FFFFFF") };
	
	private int[] colors1 = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	
	public FQAListAdapter(Context mContext, List<FQAModel> list)  {
		this.mContext = mContext;
		this.list = list;
	}

	@Override
	public int getGroupCount() {
		return list.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return list.get(groupPosition).getChildList().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return list.get(groupPosition).getChildList();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = infalInflater.inflate(R.layout.fqaheader, null);
		TextView txtTitle= (TextView) convertView.findViewById(R.id.txtStanderd);
		txtTitle.setText(list.get(groupPosition).getTitle());
		int colorPos = groupPosition % colors2.length;
		convertView.setBackgroundColor(colors2[colorPos]);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = infalInflater.inflate(R.layout.childfqa, null);
		TextView txtDescription = (TextView) convertView.findViewById(R.id.txtStanderd);
		ImageView imgscreen =(ImageView) convertView.findViewById(R.id.imgscreen);
		txtDescription.setText(Html.fromHtml(list.get(groupPosition).getChildList().get(childPosition).getDescription()));
		if(!list.get(groupPosition).getImageName().equalsIgnoreCase("")){

		try {
			InputStream ims = mContext.getAssets().open(list.get(groupPosition).getImageName());
		    Drawable d = Drawable.createFromStream(ims, null);
		    imgscreen.setImageDrawable(d);
		    imgscreen.setVisibility(View.VISIBLE);
		}
		catch(IOException ex) {
		 	ex.printStackTrace();
		}
		}
		int colorPos = groupPosition % colors2.length;
		convertView.setBackgroundColor(colors2[colorPos]);
		
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

}
