package com.edusunsoft.erp.orataro.model;

public class ChatModel {

	private String PTCommunicationDetailsID,
	CreatedOn,Details,
	PostUserType,
	TeacherAcceptance,ParentAcceptance,
	ProfilePicture,MemberID,
	MemberType,
	IsTeacherRead,
	IsParentRead;
	public String getMemberID() {
		return MemberID;
	}
	public void setMemberID(String memberID) {
		MemberID = memberID;
	}
	String time;
	public String getPTCommunicationDetailsID() {
		return PTCommunicationDetailsID;
	}
	public void setPTCommunicationDetailsID(String pTCommunicationDetailsID) {
		PTCommunicationDetailsID = pTCommunicationDetailsID;
	}
	public String getCreatedOn() {
		return CreatedOn;
	}
	public void setCreatedOn(String createdOn) {
		CreatedOn = createdOn;
	}
	public String getDetails() {
		return Details;
	}
	public void setDetails(String details) {
		Details = details;
	}
	public String getPostUserType() {
		return PostUserType;
	}
	public void setPostUserType(String postUserType) {
		PostUserType = postUserType;
	}
	public boolean getTeacherAcceptance() {
		return stringToboolean(TeacherAcceptance);
	}
	public void setTeacherAcceptance(String teacherAcceptance) {
		TeacherAcceptance = teacherAcceptance;
	}
	public boolean getParentAcceptance() {
		return stringToboolean(ParentAcceptance);
	}
	public void setParentAcceptance(String parentAcceptance) {
		ParentAcceptance = parentAcceptance;
	}
	public String getProfilePicture() {
		return ProfilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}
	public String getMemberType() {
		return MemberType;
	}
	public void setMemberType(String memberType) {
		MemberType = memberType;
	}
	public boolean getIsTeacherRead() {
		return stringToboolean(IsTeacherRead);
	}
	public void setIsTeacherRead(String isTeacherRead) {
		IsTeacherRead = isTeacherRead;
	}
	public boolean getIsParentRead() {
		return stringToboolean(IsParentRead);
	}
	public void setIsParentRead(String isParentRead) {
		IsParentRead = isParentRead;
	}
	boolean isSelf;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}

	public boolean isSelf() {
		return isSelf;
	}
	public void setSelf(boolean isSelf) {
		this.isSelf = isSelf;
	}

	public boolean stringToboolean(String str){
		if(str != null && !str.equalsIgnoreCase("")){
		if(str.equalsIgnoreCase("true")){
			return true;
		}else{
			return false;
		}
		}else{
			return false;
		}

	}
}
