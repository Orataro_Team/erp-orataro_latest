package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.FragmentViewActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FeesReciptModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

/**
 * Created by admin on 12-06-2017.
 */

public class FeesReciptAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<FeesReciptModel.Table> list ;

    public FeesReciptAdapter(Context mContext , ArrayList<FeesReciptModel.Table> list ){
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInfalater.inflate(R.layout.feesreciptraw, viewGroup, false);

        TextView txtfees = (TextView) v.findViewById(R.id.txtfees);
        TextView txtammount = (TextView) v.findViewById(R.id.txtammount);
        TextView txtdue = (TextView) v.findViewById(R.id.txtdue);
        TextView txtaction = (TextView) v.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) v.findViewById(R.id.imgremove);
        LinearLayout ll_remove= (LinearLayout) v.findViewById(R.id.ll_remove);
        final LinearLayout ll_bottom = (LinearLayout) v.findViewById(R.id.ll_bottom);
        final ListView lv_structure = (ListView) v.findViewById(R.id.lv_structure);

        txtaction.setText("View");
        txtfees.setText(list.get(i).getDisplayLabel());
        txtammount.setText(list.get(i).getAmountToBePaid()+"");
        txtdue.setText(list.get(i).getPaidAmounts()+"");
        txtaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, FragmentViewActivity.class);
                intent.putExtra("model",list.get(i));
                intent.putExtra("isFrom", ServiceResource.RECIPTDETAIL);
                mContext.startActivity(intent);
            }
        });

        return v;
    }
}
