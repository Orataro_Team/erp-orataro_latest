package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CMSPageListDataDao {

    @Insert
    void insertCMSPage(CmsPageListModel cmsPageListModel);

    @Query("SELECT * from CMSPage")
    List<CmsPageListModel> getCMSPageList();

    @Query("DELETE  FROM CMSPage")
    int deleteCMSPageList();


}
