package com.edusunsoft.erp.orataro.FragmentActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ListWallActivity;
import com.edusunsoft.erp.orataro.activities.SchoolGroupActivity;
import com.edusunsoft.erp.orataro.activities.WallMemberActivity;
import com.edusunsoft.erp.orataro.adapter.WallListAdapter;
import com.edusunsoft.erp.orataro.database.DynamicWallListDataDao;
import com.edusunsoft.erp.orataro.database.DynamicWallListModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.HomeWorkListModel;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallListVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WallActivity extends FragmentActivity implements AnimationListener, ResponseWebServices {

    private ImageView imgHome, imgMenu;
    private TextView headerText;
    private String from;
    private String title;
    private FrameLayout containar;
    private boolean isGotoWall = false;
    private Context mContext = WallActivity.this;
    public static boolean isShowMenu = false;
    public static ListView lvbounce;
    private ArrayList<DynamicWallListModel> subjectlist = new ArrayList<DynamicWallListModel>();
    private ArrayList<DynamicWallListModel> standardlist = new ArrayList<DynamicWallListModel>();
    private ArrayList<DynamicWallListModel> divisionlist = new ArrayList<DynamicWallListModel>();
    private ArrayList<DynamicWallListModel> grouplist = new ArrayList<DynamicWallListModel>();
    private ArrayList<DynamicWallListModel> projectlist = new ArrayList<DynamicWallListModel>();
    private ArrayList<WallListVo> list = new ArrayList<WallListVo>();

    // variable declaration for dynamicwallsetting
    DynamicWallListDataDao dynamicWallListDataDao;
    private List<DynamicWallListModel> dynamicWallList = new ArrayList<>();
    /*END*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wall);

        imgHome = (ImageView) findViewById(R.id.img_home);
        imgMenu = (ImageView) findViewById(R.id.img_menu);
        headerText = (TextView) findViewById(R.id.header_text);
        lvbounce = (ListView) findViewById(R.id.lvbounce);
        containar = (FrameLayout) findViewById(R.id.containar);

        dynamicWallListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(WallActivity.this).dynamicWallListDataDao();

        if (getIntent() != null) {

            from = getIntent().getStringExtra("isFrom");
            title = getIntent().getStringExtra("title");
            isGotoWall = getIntent().getBooleanExtra("isGotoWall", false);

        }

        try {
            /*for display dynamic wall list*/
            if (Utility.isNetworkAvailable(mContext)) {
                DynamicMenuList();
            } else {
                dynamicWallList = dynamicWallListDataDao.getDynamicWallList();
                if (dynamicWallList.size() > 0) {
                    for (int i = 0; i < dynamicWallList.size(); i++) {
                        if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Subject")) {
                            subjectlist.add(dynamicWallList.get(i));
                        } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Grade")) {
                            standardlist.add(dynamicWallList.get(i));
                        } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Division")) {
                            divisionlist.add(dynamicWallList.get(i));
                        } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Group")) {
                            grouplist.add(dynamicWallList.get(i));
                        } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("project")) {
                            projectlist.add(dynamicWallList.get(i));
                        }
                    }
                }
            }
            /*END*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgHome.setImageResource(R.drawable.back);
        imgMenu.setVisibility(View.GONE);
        imgMenu.setImageResource(R.drawable.back);
        imgMenu.setRotation(270);

        imgMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(WallActivity.this, WallMemberActivity.class);
                startActivity(i);
                overridePendingTransition(0, 0);

            }

        });

        imgHome.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (isGotoWall) {

                    Intent i = new Intent(WallActivity.this, HomeWorkFragmentActivity.class);
                    i.putExtra("position", getResources().getString(R.string.Wall));
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);

                } else {

                    if (ServiceResource.FROM_SELECTED_WALL.equalsIgnoreCase("FromGroupList")) {


                        if (Utility.isTeacher(mContext)) {

                            if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {

                                // Added to resolve the issue of back from wall as it is position of Group.
                                Utility.ISLOADGROUP = false;
                                finish();

//                                Intent intent = new Intent(mContext, SchoolGroupActivity.class);
//                                intent.putExtra("isFrom", ServiceResource.SCHOOLGROUP_FLAG);
//                                startActivity(intent);

                            } else {

                                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                            }

                        } else {

                            Utility.ISLOADGROUP = false;
                            finish();
//                            Intent intent = new Intent(mContext, ListWallActivity.class);
//                            intent.putExtra("isFrom", ServiceResource.GROUPWALL);
//                            startActivity(intent);

                        }

                    } else if (ServiceResource.FROM_SELECTED_WALL.equalsIgnoreCase("FromProjectList")) {

                        if (Utility.isTeacher(mContext)) {

                            if (Utility.ReadWriteSetting(ServiceResource.PROJECT).getIsView()) {

                                // Added to resolve the issue of back from wall as it is position of Project.
                                Utility.ISLOADPROJECT = false;
                                finish();

//                                Intent intent = new Intent(mContext, SchoolGroupActivity.class);
//                                intent.putExtra("isFrom", ServiceResource.PROJECT_FLAG);
//                                startActivity(intent);

                            } else {

                                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);

                            }

                        } else {

                            Utility.ISLOADPROJECT = false;
                            finish();
//                            Intent intent = new Intent(mContext, ListWallActivity.class);
//                            intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
//                            startActivity(intent);

                        }

                    } else {

                        Utility.RedirectToDashboard(WallActivity.this);

                    }


                }


            }


        });


        headerText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // need to revert for the back issue
//
//                if (Utility.ISHEADERCLICKED == false) {
//                    Utility.ISHEADERCLICKED = true;
//                    lvbounce.setVisibility(View.VISIBLE);
//                    menuChangeWall(v, true);
//                } else {
////                    lvbounce.endViewTransition(v);
////                    lvbounce.clearAnimation();
//                    lvbounce.setVisibility(View.GONE);
//                    Utility.ISHEADERCLICKED = false;
//                }

                if (!isShowMenu) {

                    isShowMenu = true;
                    menuChangeWall(v, true);
                    lvbounce.setVisibility(View.VISIBLE);

                } else {

                    isShowMenu = false;
                    menuChangeWall(v, false);
                    lvbounce.setVisibility(View.GONE);

                }

            }

        });

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do your stuff

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onResume() {

        super.onResume();

        try {
            if (FacebookWallFragment2.lvbounce != null){
                if (FacebookWallFragment2.lvbounce.getVisibility() == View.VISIBLE) {
                    FacebookWallFragment2.lvbounce.setVisibility(View.GONE);
                    FacebookWallFragment2.lvbounce.clearAnimation();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ServiceResource.FOR_WALL_SELECTION = from;

        if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
            headerText.setText(getResources().getString(R.string.Standard));
            headerText.setText(getResources().getString(R.string.Standard) + "(" + title + ")");
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId(), ServiceResource.STANDARDWALL));
        }

        if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
            try {
                Log.d("insideinstwall", ServiceResource.INSTITUTEWALL + " " + from);
                headerText.setText(getResources().getString(R.string.Institute));
                ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId(), ServiceResource.INSTITUTEWALL));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
            headerText.setText(getResources().getString(R.string.Division) + "(" + title + ")");
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId(), ServiceResource.DIVISIONWALL));
        }

        if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
            headerText.setText(getResources().getString(R.string.Subjects) + "(" + title + ")");
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId(), ServiceResource.SUBJECTWALL));
        }

        if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
//            FacebookWallFragment2.showMenuHeader();
            headerText.setText(getResources().getString(R.string.mywall));
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getWallID(), ServiceResource.PROFILEWALL));
        }

        if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
            Log.d("fromgroupwall", ServiceResource.GROUPWALL);
            headerText.setText(getResources().getString(R.string.Group) + "(" + title + ")");
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId(), ServiceResource.GROUPWALL));
        }

        if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
            headerText.setText(getResources().getString(R.string.Project) + "(" + title + ")");
            ft.replace(R.id.containar, FacebookWallFragment2.newInstance(new UserSharedPrefrence(mContext).getLoginModel().getCurrentWallId(), ServiceResource.PROJECTWALL));
        }
        ft.commit();

    }

    public void menuChangeWall(View tv, boolean isVisible) {

        if (isVisible) {
            list.clear();
            WallListVo vo;
            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Wall));
            vo.setIco(R.drawable.wall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.mywall));
            vo.setIco(R.drawable.mywall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Institutewall));
            vo.setIco(R.drawable.dash_institute);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Standardwall));
            vo.setIco(R.drawable.dash_grade);
            vo.setDynamicWallList(standardlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Divisionwall));
            vo.setIco(R.drawable.dash_division);
            vo.setDynamicWallList(divisionlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Subjectswall));
            vo.setIco(R.drawable.dash_subject);
            vo.setDynamicWallList(subjectlist);
            list.add(vo);
            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.groupwall));
            vo.setIco(R.drawable.schoolgroups);
            vo.setDynamicWallList(grouplist);
            list.add(vo);
            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.projectwall));
            vo.setIco(R.drawable.project);
            vo.setDynamicWallList(projectlist);
            list.add(vo);

            WallListAdapter adapter = new WallListAdapter(mContext, list, from, true);
            lvbounce.setAdapter(adapter);
            lvbounce.setVisibility(View.VISIBLE);

        } else {

            lvbounce.endViewTransition(tv);
            lvbounce.setVisibility(View.GONE);

        }


    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void DynamicMenuList() {

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Utility.getUserModelData(mContext);
        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        }
        arrayList.add(new PropertyVo(ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));


        new AsynsTaskClass(mContext, arrayList, false, this).
                execute(ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME, ServiceResource.LOGIN_URL);


    }

    @Override
    public void response(String result, String methodName) {
        PArseMenu(result);
    }

    public void PArseMenu(String result) {

        ArrayList<DynamicWallListModel> templist = new ArrayList<DynamicWallListModel>();
        try {

            dynamicWallListDataDao.deleteDynamicWallList();
            Log.e("Update Profile", result);
            JSONObject jsonobj = new JSONObject(result);
            JSONArray table = jsonobj
                    .getJSONArray(ServiceResource.GETUSERDYNAMICMENUDATA_TABLE);
            for (int i = 0; i < table.length(); i++) {
                JSONObject obj = table.getJSONObject(i);
                DynamicWallListModel model = new DynamicWallListModel();
                model.setWallID(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLID));
                model.setWallName(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLNAME));
                model.setWallImage(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLIMAGE));
                model.setAssociationType(obj
                        .getString(ServiceResource.GETUSERDYNAMICMENUDATA_ASSOCIATIONTYPE));
                templist.add(model);

                /*insert dynamicwallmenulist into database.*/
                dynamicWallListDataDao.insertDynamicWallList(model);
                /*END*/

            }
            subjectlist.clear();
            standardlist.clear();
            divisionlist.clear();
            grouplist.clear();
            projectlist.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dynamicWallList = dynamicWallListDataDao.getDynamicWallList();
        for (int i = 0; i < dynamicWallList.size(); i++) {
            if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Subject")) {
                subjectlist.add(dynamicWallList.get(i));
            } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Grade")) {
                standardlist.add(dynamicWallList.get(i));
            } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Division")) {
                divisionlist.add(dynamicWallList.get(i));
            } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Group")) {
                grouplist.add(dynamicWallList.get(i));
            } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("project")) {
                projectlist.add(dynamicWallList.get(i));
            }
        }
    }

}
