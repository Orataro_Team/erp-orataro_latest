package com.edusunsoft.erp.orataro.util;

import com.edusunsoft.erp.orataro.database.TimeTableModel;

import java.util.Comparator;

public class timeComperator implements Comparator<TimeTableModel> {
	@Override
	public int compare(TimeTableModel lhs, TimeTableModel rhs) {
		return lhs.getTotalTime() < rhs.getTotalTime() ? 1 : 0;
	}

}