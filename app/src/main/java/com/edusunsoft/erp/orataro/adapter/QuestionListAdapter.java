package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.QuestionModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class QuestionListAdapter extends BaseAdapter {
	
	private Context mContext;
	private ArrayList<QuestionModel> questionList = new ArrayList<>();
	
	public QuestionListAdapter(Context mContext, ArrayList<QuestionModel> questionList) {
		this.mContext = mContext;
		this.questionList=questionList;
	}
	
	@Override
	public int getCount() {
		return questionList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.questionlistraw, parent, false);
		TextView txtQuestionList = (TextView) convertView.findViewById(R.id.txtquestion);
		LinearLayout ansLayout = (LinearLayout) convertView.findViewById(R.id.ansLayout);
		if(Utility.isNull(questionList.get(position).getQuestion())){
			txtQuestionList.setText(questionList.get(position).getQuestion());
		}

		if(questionList.get(position).getAnsList() != null && questionList.get(position).getAnsList().size()>0){
			for(int i=0;i<questionList.get(position).getAnsList().size(); i++){
				View view = inflater.inflate(R.layout.radiobutton, null);
				RadioButton rdtbtn = (RadioButton) view.findViewById(R.id.radioBtn);
				rdtbtn.setText(questionList.get(position).getAnsList().get(i));
				ansLayout.addView(view);
			}
		}
		
		return convertView;
	}

}
