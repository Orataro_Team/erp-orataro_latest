package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class PageDetailFragment extends Fragment implements ResponseWebServices {

    private static final String TYPE = "type";
    private static final String PAGEID = "pageid";
    private LinearLayout headerLayout;
    private Context mContext;
    private WebView Wv_page;
    private TextView txt_nodatafound;
    private String type, pageId;

    public static final PageDetailFragment newInstance(String type, String pageId) {
        PageDetailFragment f = new PageDetailFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(TYPE, type);
        bdl.putString(PAGEID, pageId);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_page_detail_, null);
        mContext = getActivity();
        this.type = getArguments().getString(TYPE);
//        Log.d("examttype", this.type);
        this.pageId = getArguments().getString(PAGEID);
//        Log.d("PAGEID", this.pageId);
        headerLayout = (LinearLayout) view.findViewById(R.id.headerLayout);
        headerLayout.setVisibility(View.GONE);
        ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Wv_page = (WebView) view.findViewById(R.id.Wv_page);
        Wv_page.getSettings().setBuiltInZoomControls(true);
        Wv_page.getSettings().setSupportZoom(true);
        Wv_page.getSettings().setUseWideViewPort(true);
        Wv_page.getSettings().setLoadWithOverviewMode(true);
        txt_nodatafound = (TextView) view.findViewById(R.id.txt_nodatafound);
        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                if (type.equalsIgnoreCase(ServiceResource.INFORMATION_STRING)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Information) + " (" + Utility.GetFirstName(mContext) + ")");
                    HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
                } else if (type.equalsIgnoreCase(ServiceResource.PRAYER_STRING)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.SchoolPrayer) + " (" + Utility.GetFirstName(mContext) + ")");
                    HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
                } else if (type.equalsIgnoreCase(ServiceResource.SCHOOLTIMEING_STRING)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.ExamTiming) + " (" + Utility.GetFirstName(mContext) + ")");
                    HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.isNetworkAvailable(mContext)) {
            pageDetail(true);
        } else {
            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
        return view;
    }

    public void pageDetail(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CMSPAGE_ID, pageId));
        Log.d("getpagerequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETCMSPAGEDETAIL_METHODNAME, ServiceResource.CMSPAGE_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.GETCMSPAGEDETAIL_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, pageId, mContext);
            Log.d("examresult", result);
            parsePageDetail(result);
        }
    }

    public void showTextView() {
        txt_nodatafound.setVisibility(View.VISIBLE);
        if (type.equalsIgnoreCase(ServiceResource.INFORMATION_STRING)) {
            txt_nodatafound.setText(getResources().getString(R.string.NoInformationAvailble));
        } else if (type.equalsIgnoreCase(ServiceResource.PRAYER_STRING)) {
            txt_nodatafound.setText(getResources().getString(R.string.NoPrayerListAvailble));
        } else if (type.equalsIgnoreCase(ServiceResource.TIMETABLE_STRING)) {
            txt_nodatafound.setText(getResources().getString(R.string.NoTimeTableListAvailble));
        } else if (type.equalsIgnoreCase(ServiceResource.SCHOOLTIMEING_STRING)) {
            txt_nodatafound.setText(getResources().getString(R.string.NoSchoolTimingAvailble));
        }

    }

    public void parsePageDetail(String result) {
        String pageDetail = null;
        JSONArray hJsonArray;
        try {
            if (result.contains("\"success\":0")) {
            } else {
                hJsonArray = new JSONArray(result);
                pageDetail = hJsonArray.getJSONObject(0).getString(ServiceResource.CMSPAGES_PAGEDETAILS);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (pageDetail != null && !pageDetail.equals("") && !pageDetail.equals("null")) {
            Wv_page.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);
            Wv_page.getSettings().setJavaScriptEnabled(true);
            StringBuilder sb = new StringBuilder();
            sb.append("<HTML><HEAD><LINK href=" + ServiceResource.CSSFILE + " type=\"text/css\" rel=\"stylesheet\"/></HEAD><body>");
            sb.append(pageDetail.toString().replaceAll("/DataFiles", ServiceResource.BASE_IMG_URL + "DataFiles"));
            sb.append("</body></HTML>");
            Log.v("html", sb.toString());
            Wv_page.loadDataWithBaseURL("", sb.toString(), "text/html", "UTF-8", "");
            Wv_page.getSettings().setBuiltInZoomControls(true);
        } else {
            Wv_page.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            showTextView();
        }
    }

}
