package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPollActivity;
import com.edusunsoft.erp.orataro.adapter.DateListAdapter;
import com.edusunsoft.erp.orataro.adapter.PollListAdapter;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.PollOptionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class MyPollFragment extends Fragment implements OnClickListener, ResponseWebServices, RefreshListner {

    private static final String ISSTUDENT = "isStudent";
    private Context mContext;
    private View view;
    private PullAndLoadListView lvPoll;
    private PollListAdapter adapter;
    private LinearLayout ll_addBtn;
    private ArrayList<PollModel> pollList;
    private EditText edtTeacherName;
    private ListView lst_date;
    private Dialog mDialog;
    private DateListAdapter date_list_Adapter;
    private TextView txt_nodatafound;
    private LinearLayout ll_comming_soon, addbtnlayout;
    private FrameLayout fl_main;
    private boolean isStudent;
    private ArrayList<PollModel> pollModels, pollModelStudent;
    private ArrayList<PollOptionModel> optionList;

    public static final MyPollFragment newInstance(boolean type) {

        MyPollFragment f = new MyPollFragment();
        Bundle bdl = new Bundle(2);
        bdl.putBoolean(ISSTUDENT, type);
        f.setArguments(bdl);
        return f;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.postfragment, null);
        mContext = getActivity();
        isStudent = getArguments().getBoolean(ISSTUDENT);
        fl_main = (FrameLayout) view.findViewById(R.id.fl_main);
        ll_comming_soon = (LinearLayout) view.findViewById(R.id.ll_comming_soon);
        fl_main.setVisibility(View.VISIBLE);
        ll_comming_soon.setVisibility(View.GONE);
        lvPoll = (PullAndLoadListView) view.findViewById(R.id.lv_postfragment);
        ll_addBtn = (LinearLayout) view.findViewById(R.id.addbtnlayout);
        edtTeacherName = (EditText) view.findViewById(R.id.edtsearchStudent);
        ll_addBtn.setVisibility(View.VISIBLE);
        txt_nodatafound = (TextView) view.findViewById(R.id.txt_nodatafound);
        ll_addBtn.setOnClickListener(this);

        if (isStudent) {
            ll_addBtn.setVisibility(View.GONE);
        }

        if (Utility.isTeacher(mContext)) {
            if (!Utility.ReadWriteSetting(ServiceResource.POLL).getIsCreate()) {
                ll_addBtn.setVisibility(View.GONE);
            }
        }

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Poll) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {


            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
            }
        });

        lvPoll.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {
                if (Utility.isNetworkAvailable(getActivity())) {
                    if (!isStudent) {
                        getPollList(true);
                    }
                } else {
                    Utility.showAlertDialog(getActivity(), getResources().getString(R.string.no_internet_connection), "Error");
                }

            }

        });

        return view;

    }


    @Override
    public void onResume() {

        if (Utility.isNetworkAvailable(getActivity())) {

            if (!isStudent) {

                getPollList(true);

            } else {

                getPollListStudent(true);

            }

        } else {

            Utility.showAlertDialog(getActivity(), getResources().getString(R.string.no_internet_connection), "Error");

        }


        super.onResume();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addbtnlayout:
                Intent i = new Intent(getActivity(), AddPollActivity.class);
                startActivity(i);
                break;

            default:
                break;
        }
    }

    @Override
    public void refresh(String methodName) {


        if (Utility.isNetworkAvailable(getActivity())) {

            if (!isStudent) {

                getPollList(true);

            } else {

                getPollListStudent(true);

            }

        } else {

            Utility.showAlertDialog(getActivity(), getResources().getString(R.string.no_internet_connection), "Error");

        }

    }

    public class SortedDate implements Comparator<PollModel> {
        @Override
        public int compare(PollModel o1, PollModel o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    }

    public void getPollList(boolean isViewPopup) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETPOLLSLISTFORADDPAGE_METHODNAME, ServiceResource.POLL_URL);
    }

    public void getPollListStudent(boolean isViewPopup) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        Log.d("getrequestpoll", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETPOLLSLISTFORPARTICIPANTPAGE_METHODNAME, ServiceResource.POLL_URL);
    }


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETPOLLSLISTFORADDPAGE_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, ServiceResource.GETPOLLSLISTFORADDPAGE_METHODNAME, mContext);
            ParseAddpage(result, ServiceResource.UPDATE);
        } else if (ServiceResource.GETPOLLSLISTFORPARTICIPANTPAGE_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, ServiceResource.GETPOLLSLISTFORPARTICIPANTPAGE_METHODNAME, mContext);
            parseaddparticularpage(result, ServiceResource.UPDATE);
        }

    }

    public void ParseAddpage(String result, String update) {

        JSONArray jsonObj;

        try {
            jsonObj = new JSONArray(result);
            pollModels = new ArrayList<PollModel>();
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                PollModel model = new PollModel();

                String date = Utility.getDate(
                        Long.valueOf(innerObj
                                .getString(ServiceResource.POLL_STARTDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "EEEE/dd/MMM/yyyy");
                String[] dateArray = date.split("/");
                if (dateArray != null && dateArray.length > 0) {
                    Log.v("date",
                            (dateArray[0] + "" + dateArray[1] + "" + dateArray[2]));
                    model.setDate(dateArray[1] + " "
                            + dateArray[0].substring(0, 3));
                    model.setMonth(dateArray[2]);
                    model.setYear(dateArray[3]);
                }
                model.setStartDate(innerObj
                        .getString(ServiceResource.POLL_STARTDATE)
                        .replace("/Date(", "").replace(")/", ""));
                model.setPollID(innerObj
                        .getString(ServiceResource.POLL_POLLID));
                model.setEndDate(innerObj
                        .getString(ServiceResource.POLL_ENDDATE)
                        .replace("/Date(", "").replace(")/", ""));
                // model.setMonth(month);
                model.setTitle(innerObj
                        .getString(ServiceResource.POLL_TITLE));
                model.setDetails(innerObj.getString(ServiceResource.POLL_DETAIL));
                model.setStartIn(innerObj.getString(ServiceResource.POLL_STARTIN));
                model.setEndIn(innerObj.getString(ServiceResource.POLL_ENDIN));
                model.setUserName(innerObj.getString(ServiceResource.POLL_USERNAME));
                model.setParticipateCOunt(innerObj.getString(ServiceResource.POLL_PARTICIPANT));
                pollModels.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (pollModels != null && pollModels.size() > 0) {
            Collections.sort(pollModels, new SortedDate());
            Collections.reverse(pollModels);
            for (int i = 0; i < pollModels.size(); i++) {
                if (i != 0) {
                    String temp = pollModels.get(i - 1).getMonth();
                    if (temp.equalsIgnoreCase(pollModels.get(i)
                            .getMonth())) {
                        pollModels.get(i).setChangeDate(false);
                    } else {
                        pollModels.get(i).setChangeDate(true);
                    }
                } else {
                    pollModels.get(i).setChangeDate(true);
                }

            }

        }

        if (pollModels != null && pollModels.size() > 0) {
            adapter = new PollListAdapter(getActivity(), pollModels, isStudent, this);
            lvPoll.setAdapter(adapter);
            lvPoll.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);
        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoPollListFound));
        }


    }


    public void parseaddparticularpage(String result, String update) {

        Log.d("getResultpoll", result);
        JSONArray jsonObj;
        JSONObject jobj;
        JSONArray OptionArray;
        try {

            if (update.equalsIgnoreCase(ServiceResource.UPDATE)) {
                jobj = new JSONObject(result);
                jsonObj = jobj.getJSONArray(ServiceResource.TABLE);
                OptionArray = jobj.getJSONArray(ServiceResource.TABLE1);

            } else {

                String[] saparatebyhase = result.split("#");

                if (saparatebyhase != null && saparatebyhase.length > 2) {

                    jsonObj = new JSONArray(saparatebyhase[0]);
                    OptionArray = new JSONArray(saparatebyhase[1]);

                } else {

                    jsonObj = new JSONArray();
                    OptionArray = new JSONArray();

                }

            }

            pollModelStudent = new ArrayList<PollModel>();

            for (int i = 0; i < jsonObj.length(); i++) {

                JSONObject innerObj = jsonObj.getJSONObject(i);
                PollModel model = new PollModel();
                String date = Utility.getDate(
                        Long.valueOf(innerObj.getString(ServiceResource.POLL_STARTDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "EEEE/dd/MMM/yyyy");
                String[] dateArray = date.split("/");
                if (dateArray != null && dateArray.length > 0) {
                    model.setDate(dateArray[1] + " " + dateArray[0].substring(0, 3));
                    model.setMonth(dateArray[2]);
                    model.setYear(dateArray[3]);
                }

                model.setStartDate(innerObj
                        .getString(ServiceResource.POLL_STARTDATE)
                        .replace("/Date(", "").replace(")/", ""));
                model.setPollID(innerObj.getString(ServiceResource.POLL_POLLID));
                model.setEndDate(innerObj.getString(ServiceResource.POLL_ENDDATE)
                        .replace("/Date(", "").replace(")/", ""));
                model.setTitle(innerObj.getString(ServiceResource.POLL_TITLE));
                model.setDetails(innerObj.getString(ServiceResource.POLL_DETAIL));
                model.setUserName(innerObj.getString(ServiceResource.FULLNAME));
                model.setIsMultichoice(innerObj.getString(ServiceResource.ISMULTICHOICE));
                pollModelStudent.add(model);

            }

            optionList = new ArrayList<PollOptionModel>();
            for (int i = 0; i < OptionArray.length(); i++) {
                JSONObject innerObj = OptionArray.getJSONObject(i);
                PollOptionModel optionModel = new PollOptionModel();
                optionModel.setPollOptionID(innerObj.getString(ServiceResource.POLLOPTIONID));
                optionModel.setPollID(innerObj.getString(ServiceResource.POLL_POLLID));
                optionModel.setOption(innerObj.getString(ServiceResource.OPTION));
                optionList.add(optionModel);
                Log.d("getoptionlist", optionList.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (pollModelStudent != null && pollModelStudent.size() > 0) {
            Collections.sort(pollModelStudent, new SortedDate());
            Collections.reverse(pollModelStudent);
            for (int i = 0; i < pollModelStudent.size(); i++) {
                if (i != 0) {
                    String temp = pollModelStudent.get(i - 1).getMonth();
                    if (temp.equalsIgnoreCase(pollModelStudent.get(i)
                            .getMonth())) {
                        pollModelStudent.get(i).setChangeDate(false);
                    } else {
                        pollModelStudent.get(i).setChangeDate(true);
                    }
                } else {
                    pollModelStudent.get(i).setChangeDate(true);
                }

            }

        } else {

            lvPoll.setAdapter(null);
        }

        if (pollModelStudent != null && pollModelStudent.size() > 0) {
            Log.d("getOptionlist", optionList.toString());
            adapter = new PollListAdapter(getActivity(), pollModelStudent, isStudent, optionList, this);
            lvPoll.setAdapter(adapter);
            lvPoll.setVisibility(View.VISIBLE);
            txt_nodatafound.setVisibility(View.GONE);
        } else {
            lvPoll.setAdapter(null);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoPollListFound));
        }

    }

}
