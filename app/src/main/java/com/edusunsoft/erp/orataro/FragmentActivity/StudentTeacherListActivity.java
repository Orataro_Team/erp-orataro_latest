package com.edusunsoft.erp.orataro.FragmentActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.fragments.StudentTeacherListFragment;
import com.edusunsoft.erp.orataro.util.Utility;

public class StudentTeacherListActivity extends FragmentActivity implements OnClickListener {
    StdDivSubModel model;
    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    private Context mContext;
    LinearLayout addLayout;
    ListView homeworklist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);
        mContext = StudentTeacherListActivity.this;
        model = (StdDivSubModel) getIntent().getSerializableExtra("model");

        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);


        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);

        if (Utility.isTeacher(mContext)) {
            txtHeader.setText(getResources().getString(R.string.StudentList));
        } else {
            txtHeader.setText(getResources().getString(R.string.TeacherList));
        }

        imgLeftheader.setOnClickListener(this);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.homeworkActivity_containair, StudentTeacherListFragment.newInstance(model, ""));
        ft.commit();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            finish();
        }
    }

}
