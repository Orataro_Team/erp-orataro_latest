package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.Item;
import com.edusunsoft.erp.orataro.util.FileUtils;

import java.util.List;


public class FileArrayAdapter extends ArrayAdapter<Item> {

    private Context c;
    private int id;
    private List<Item> items;

    public FileArrayAdapter(Context context, int textViewResourceId, List<Item> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }

    public Item getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(id, null);
        }

        final Item o = items.get(position);

        if (o != null) {

            TextView t1 = (TextView) v.findViewById(R.id.TextView01);
            TextView t2 = (TextView) v.findViewById(R.id.TextView02);
            TextView t3 = (TextView) v.findViewById(R.id.TextViewDate);
            /* Take the ImageView from layout and set the city's image */
            ImageView imageCity = (ImageView) v.findViewById(R.id.fd_Icon1);
            String uri = "drawable/" + o.getImage();

//	               		if(uri != null){
//
//							int imageResource = c.getResources().getIdentifier(uri, null, c.getPackageName());
//							Drawable image = c.getResources().getDrawable(imageResource);
//							imageCity.setImageDrawable(image);
//						}

            imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.folder_icon2));

//            if (o.getName().contains("pdf") || o.getName().contains("PDF")) {
//                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.pdf));
//            }
//            if (o.getName().contains("doc") || o.getName().contains("DOC") || o.getName().contains("docs")) {
//                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.doc));
//            }
//            if (o.getName().contains("txt") || o.getName().contains("TXT")) {
//                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.txt));
//            }


            //By Hardik Kanak
            String selectedFileExtension = FileUtils.getExtension(o.getName());
            if (FileUtils.isPDF(selectedFileExtension)) {
                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.pdf));
            }
            if (FileUtils.isDOC(selectedFileExtension)) {
                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.doc));
            }
            if (FileUtils.isTXT(selectedFileExtension)) {
                imageCity.setImageDrawable(c.getResources().getDrawable(R.drawable.txt));
            }

            if (t1 != null)
                t1.setText(o.getName());
            if (t2 != null)
                t2.setText(o.getData());
            if (t3 != null)
                t3.setText(o.getDate());

        }

        return v;

    }

}
