/*
 * Copyright 2013 - learnNcode (learnncode@gmail.com)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.edusunsoft.erp.orataro.gallery;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;

import java.io.File;

public class ImageLoadAsync extends MediaAsync<String, String, String> {

	private ImageView mImageView;
	private Context mContext;
	private int mWidth;

	public ImageLoadAsync(Context context, ImageView imageView, int width) {
		mImageView = imageView;
		mContext   = context;
		mWidth     = width;
	}

	@Override
	protected String doInBackground(String... params) {
		String url = params[0].toString();
		return url;
	}

	@Override
	protected void onPostExecute(String result) {
		mImageView.getLayoutParams().height = mWidth;
		mImageView.getLayoutParams().width = mWidth;
		mImageView.requestLayout();

		try {
			RequestOptions options = new RequestOptions()
					.centerCrop()
					.placeholder(R.drawable.photo)
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.priority(Priority.HIGH)
					.dontAnimate()
					.dontTransform();

			Glide.with(mContext)
					.load(Uri.fromFile(new File(result)).toString().replace("%20", " "))
					.apply(options)
					.into(mImageView);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class ViewImage extends AsyncTask<String, Void, Void> {
		@Override
		protected Void doInBackground(String... params) {
			mImageView.getLayoutParams().height = mWidth;
			mImageView.getLayoutParams().width = mWidth;
			mImageView.requestLayout();
			return null;
		}
	}

}
