package com.edusunsoft.erp.orataro.Interface;

import com.edusunsoft.erp.orataro.model.CircularNoteStudentModel;

import java.util.ArrayList;


public interface SelectStudentInterface {
	public void getAllSelectedStudent(int pos, ArrayList<CircularNoteStudentModel> addselectedstudentModels);
}
