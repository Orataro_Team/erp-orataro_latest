package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.SchoolTimingModel;

import java.util.ArrayList;

public class SchoolTimeListAdapter extends BaseAdapter {
	
	private LayoutInflater layoutInfalater;
	private Context context;
	private TextView tv_class, tv_time;
	private ArrayList<SchoolTimingModel> schoolTimingModels = new ArrayList<>();
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	
	public SchoolTimeListAdapter(Context context, ArrayList<SchoolTimingModel> schoolTimingModels) {
		this.context = context;
		this.schoolTimingModels = schoolTimingModels;
	}

	@Override
	public int getCount() {
		return schoolTimingModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.timing_listraw, parent, false);
		tv_class = (TextView) convertView.findViewById(R.id.tv_class);
		tv_time = (TextView) convertView.findViewById(R.id.tv_time);
		tv_class.setText(schoolTimingModels.get(position).getSt_class());
		tv_time.setText(schoolTimingModels.get(position).getSt_time());
		return convertView;
	}

}
