package com.edusunsoft.erp.orataro.adapter;

/**
 * Created by admin on 31-07-2017.
 */

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ReciptDetailModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by admin on 12-06-2017.
 */

public class ReciptDetailAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<ReciptDetailModel.Table> list;
    public File file;

    public ReciptDetailAdapter(Context mContext, ArrayList<ReciptDetailModel.Table> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInfalater.inflate(R.layout.feesreciptraw, viewGroup, false);

        TextView txtfees = (TextView) v.findViewById(R.id.txtfees);
        TextView txtammount = (TextView) v.findViewById(R.id.txtammount);
        TextView txtdue = (TextView) v.findViewById(R.id.txtdue);
        TextView txtaction = (TextView) v.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) v.findViewById(R.id.imgremove);
        LinearLayout ll_remove = (LinearLayout) v.findViewById(R.id.ll_remove);
        final LinearLayout ll_bottom = (LinearLayout) v.findViewById(R.id.ll_bottom);
        final ListView lv_structure = (ListView) v.findViewById(R.id.lv_structure);

        txtaction.setText("Download");
        txtfees.setText(list.get(i).getDisplayLabel());
        txtammount.setText(list.get(i).getAmountToBePaid() + "");
        txtdue.setText(list.get(i).getPaidAmounts() + "");

        txtaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filepath = list.get(i).getReceiptPath();
                if (!filepath.equalsIgnoreCase("")) {
                    if (!filepath.contains(ServiceResource.ERP_URL)) {
                        filepath = ServiceResource.ERP_URL + filepath;
                    }
                    saveDownload(filepath);
                    Utility.toast(mContext, "Start Downloding....");
                } else {
                    Utility.toast(mContext, "Receipt Can not download .\n Please Try again ");
                }
            }
        });

        return v;
    }

    private void saveDownload(String url) {

        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//
//        DownloadManager.Request request = new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding Receipt...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        }
//        if (file.exists())
//            file.delete();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request.allowScanningByMediaScanner();
//        }
//
//        long id = mgr.enqueue(request);
//
//        Utility.toast(mContext, mContext.getResources().getString(R.string.downloadstarting));

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, mContext.getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);

    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

}
