package com.edusunsoft.erp.orataro.model;

public class PostModel {

	boolean changeMonth;
	String senderName, post, date, time, viewby, month, year;
	String post_img;

	public String getPost_img() {
		return post_img;
	}

	public void setPost_img(String post_img) {
		this.post_img = post_img;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public boolean isChangeMonth() {
		return changeMonth;
	}

	public void setChangeMonth(boolean changeMonth) {
		this.changeMonth = changeMonth;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getViewby() {
		return viewby;
	}

	public void setViewby(String viewby) {
		this.viewby = viewby;
	}

}
