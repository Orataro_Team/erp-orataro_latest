package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.GroupMemberInterface;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.GroupMemberModel;

import java.util.ArrayList;
import java.util.Locale;

public class GroupMemberAdapter extends BaseAdapter implements ResponseWebServices {

    private final Context context;
    private ArrayList<GroupMemberModel> allSkillVo = new ArrayList<>();
    private ArrayList<GroupMemberModel> tempList = new ArrayList<>();
    private ArrayList<GroupMemberModel> copyList = new ArrayList<>();
    private ArrayList<GroupMemberModel> SelectMemberList = new ArrayList<>();
    private LayoutInflater layoutInfalater;
    private GroupMemberInterface jobSkillInterface;

    public GroupMemberAdapter(Context context, ArrayList<GroupMemberModel> addSkillModels, GroupMemberInterface pJobSkillInterface) {
        this.context = context;
        this.allSkillVo = addSkillModels;
        this.tempList = addSkillModels;
        copyList.addAll(tempList);
        jobSkillInterface = pJobSkillInterface;
    }

    @Override
    public int getCount() {
        return tempList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolderItem();
            layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfalater.inflate(R.layout.add_groupmember_list_dialog, parent, false);
            viewHolder.txt_skill = (TextView) convertView.findViewById(R.id.dialog_skill_list_txt);
            viewHolder.chk_selected = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.txt_skill.setText(tempList.get(position).getMemberName());
        if (tempList.get(position).isSelected()) {
            jobSkillInterface.getAllGroupMember(position, tempList);
            viewHolder.txt_skill.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.tick_sky_blue), null);
        } else {
            jobSkillInterface.getAllGroupMember(position, tempList);
            viewHolder.txt_skill.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }


        viewHolder.txt_skill.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                for (int i = 0; i < allSkillVo.size(); i++) {
                    if (!allSkillVo.get(i).isSelected()) {
                        if (allSkillVo.get(i).getMemberId().equalsIgnoreCase(tempList.get(position).getMemberId())) {
                            allSkillVo.get(position).setSelected(true);
                            viewHolder.txt_skill.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.tick_sky_blue), null);
                        }
                        jobSkillInterface.getAllGroupMember(i, allSkillVo);

                    } else {
                        if (allSkillVo.get(i).getMemberId().equalsIgnoreCase(tempList.get(position).getMemberId())) {
                            allSkillVo.get(position).setSelected(false);
                            viewHolder.txt_skill.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        }
                        jobSkillInterface.getAllGroupMember(i, allSkillVo);
                    }
                }
            }
        });

        return convertView;
    }

    public void SelectAll(ArrayList<GroupMemberModel> localAddGroupMemberModels, ImageView selectall) {

        for (int i = 0; i < localAddGroupMemberModels.size(); i++) {
            if (localAddGroupMemberModels.get(i).isSelected() == false) {
                localAddGroupMemberModels.get(i).setSelected(true);
                selectall.setImageResource(R.drawable.tick_icon);
                SelectMemberList.add(localAddGroupMemberModels.get(i));
                Log.d("getSelectedListadd", SelectMemberList.toString());
                jobSkillInterface.getAllGroupMember(i, SelectMemberList);
            } else {
                localAddGroupMemberModels.get(i).setSelected(false);
                selectall.setImageResource(R.drawable.untick_icon);
                SelectMemberList.remove(localAddGroupMemberModels.get(i));
                Log.d("getSelectedListremove", SelectMemberList.toString());
                jobSkillInterface.getAllGroupMember(i, SelectMemberList);
            }

        }

        notifyDataSetChanged();

    }

    public static class ViewHolderItem {
        public TextView txt_skill;
        public CheckBox chk_selected;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        tempList.clear();
        if (charText.length() == 0) {
            tempList.addAll(copyList);
        } else {
            for (GroupMemberModel vo : copyList) {
                if (vo.getMemberName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    tempList.add(vo);
                }
            }
        }

        this.notifyDataSetChanged();

    }

    @Override
    public void response(String result, String methodName) {

    }

}
