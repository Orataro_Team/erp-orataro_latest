package com.edusunsoft.erp.orataro.imagepicker;

import android.graphics.Bitmap;


class ImageCompressHelper {

    static String TAG = ImageCompressHelper.class.getSimpleName();

    public static class CompressParams {
        String outputPath;
        int maxWidth = 1000;
        int maxHeight = 1000;
        int saveQuality = 80;
        Bitmap.CompressFormat compressFormat = null;
    }

    public static class CompressJop {
        String inputFile;
        CompressParams params;
    }

    private Callback mCallback;

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    interface Callback {
        void onError();
        void onSuccess(String file);
    }

}
