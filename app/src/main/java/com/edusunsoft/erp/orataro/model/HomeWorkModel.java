package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class HomeWorkModel implements Serializable {

    private String AssignmentID,ReferenceLink, ClientID, InstituteID, MemberID, GradeID, DivisionID, SubjectID,
            SubjectName,strDateOfHomeWork;
    private boolean IsRead, isFinish;
    private String imgUrl, GradeName, DivisionName;
    private boolean VisibleMonth;
    private String FacultyID, DateOfHomeWork, Title, HomeWorksDetails;
    private boolean isMessageReceived, IsWorkFinished, IsFacultyChecked;
    private String DateOfFinish, MemberComments, TeacherRated,
            TeacherComment;
    private String IsActive, IsArchive, SeqNo, UpdateOn, UpdateBy, CreateOn, CreateBy,
            BeachID;
    private String month, year, day, MilliSecond, TeacherProfilePicture, TecherName;

    public String getReferenceLink() {
        return ReferenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        ReferenceLink = referenceLink;
    }

    public String getStrDateOfHomeWork() {
        return strDateOfHomeWork;
    }

    public void setStrDateOfHomeWork(String strDateOfHomeWork) {
        this.strDateOfHomeWork = strDateOfHomeWork;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean isFinish) {
        this.isFinish = isFinish;
    }

    public boolean isIsRead() {
        return IsRead;
    }

    public void setIsRead(boolean isRead) {
        IsRead = isRead;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getTeacherProfilePicture() {
        return TeacherProfilePicture;
    }

    public void setTeacherProfilePicture(String teacherProfilePicture) {
        TeacherProfilePicture = teacherProfilePicture;
    }

    public String getTecherName() {
        return TecherName;
    }

    public void setTecherName(String techerName) {
        TecherName = techerName;
    }

    public String getMilliSecond() {
        return MilliSecond;
    }

    public void setMilliSecond(String milliSecond) {
        MilliSecond = milliSecond;
    }

    public boolean isVisibleMonth() {
        return VisibleMonth;
    }

    public void setVisibleMonth(boolean visibleMonth) {
        VisibleMonth = visibleMonth;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getAssignmentID() {
        return AssignmentID;
    }

    public void setAssignmentID(String assignmentID) {
        AssignmentID = assignmentID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getFacultyID() {
        return FacultyID;
    }

    public void setFacultyID(String facultyID) {
        FacultyID = facultyID;
    }

    public String getDateOfHomeWork() {
        return DateOfHomeWork;
    }

    public void setDateOfHomeWork(String dateOfHomeWork) {
        DateOfHomeWork = dateOfHomeWork;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getHomeWorksDetails() {
        return HomeWorksDetails;
    }

    public void setHomeWorksDetails(String homeWorksDetails) {
        HomeWorksDetails = homeWorksDetails;
    }

    public boolean isMessageReceived() {
        return isMessageReceived;
    }

    public void setMessageReceived(boolean isMessageReceived) {
        this.isMessageReceived = isMessageReceived;
    }

    public boolean isIsWorkFinished() {
        return IsWorkFinished;
    }

    public void setIsWorkFinished(boolean isWorkFinished) {
        IsWorkFinished = isWorkFinished;
    }

    public String getDateOfFinish() {
        return DateOfFinish;
    }

    public void setDateOfFinish(String dateOfFinish) {
        DateOfFinish = dateOfFinish;
    }

    public String getMemberComments() {
        return MemberComments;
    }

    public void setMemberComments(String memberComments) {
        MemberComments = memberComments;
    }

    public boolean getIsFacultyChecked() {
        return IsFacultyChecked;
    }

    public void setIsFacultyChecked(boolean isFacultyChecked) {
        IsFacultyChecked = isFacultyChecked;
    }

    public String getTeacherRated() {
        return TeacherRated;
    }

    public void setTeacherRated(String teacherRated) {
        TeacherRated = teacherRated;
    }

    public String getTeacherComment() {
        return TeacherComment;
    }

    public void setTeacherComment(String teacherComment) {
        TeacherComment = teacherComment;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getIsArchive() {
        return IsArchive;
    }

    public void setIsArchive(String isArchive) {
        IsArchive = isArchive;
    }

    public String getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(String seqNo) {
        SeqNo = seqNo;
    }

    public String getUpdateOn() {
        return UpdateOn;
    }

    public void setUpdateOn(String updateOn) {
        UpdateOn = updateOn;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String updateBy) {
        UpdateBy = updateBy;
    }

    public String getCreateOn() {
        return CreateOn;
    }

    public void setCreateOn(String createOn) {
        CreateOn = createOn;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getBeachID() {
        return BeachID;
    }

    public void setBeachID(String beachID) {
        BeachID = beachID;
    }


    @Override
    public String toString() {
        return "HomeWorkModel{" +
                "AssignmentID='" + AssignmentID + '\'' +
                ", ClientID='" + ClientID + '\'' +
                ", InstituteID='" + InstituteID + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", SubjectID='" + SubjectID + '\'' +
                ", SubjectName='" + SubjectName + '\'' +
                ", IsRead=" + IsRead +
                ", isFinish=" + isFinish +
                ", imgUrl='" + imgUrl + '\'' +
                ", GradeName='" + GradeName + '\'' +
                ", DivisionName='" + DivisionName + '\'' +
                ", VisibleMonth=" + VisibleMonth +
                ", FacultyID='" + FacultyID + '\'' +
                ", DateOfHomeWork='" + DateOfHomeWork + '\'' +
                ", Title='" + Title + '\'' +
                ", HomeWorksDetails='" + HomeWorksDetails + '\'' +
                ", isMessageReceived=" + isMessageReceived +
                ", IsWorkFinished=" + IsWorkFinished +
                ", IsFacultyChecked=" + IsFacultyChecked +
                ", DateOfFinish='" + DateOfFinish + '\'' +
                ", MemberComments='" + MemberComments + '\'' +
                ", TeacherRated='" + TeacherRated + '\'' +
                ", TeacherComment='" + TeacherComment + '\'' +
                ", IsActive='" + IsActive + '\'' +
                ", IsArchive='" + IsArchive + '\'' +
                ", SeqNo='" + SeqNo + '\'' +
                ", UpdateOn='" + UpdateOn + '\'' +
                ", UpdateBy='" + UpdateBy + '\'' +
                ", CreateOn='" + CreateOn + '\'' +
                ", CreateBy='" + CreateBy + '\'' +
                ", BeachID='" + BeachID + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", day='" + day + '\'' +
                ", MilliSecond='" + MilliSecond + '\'' +
                ", TeacherProfilePicture='" + TeacherProfilePicture + '\'' +
                ", TecherName='" + TecherName + '\'' +
                '}';
    }
}
