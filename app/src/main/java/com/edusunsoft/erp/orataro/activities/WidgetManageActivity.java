package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WidgetManageActivity extends Activity implements ResponseWebServices {

    private int position = 0;
    private int userposition = 0;
    private boolean issameUser;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        mContext = WidgetManageActivity.this;

        if (getIntent() != null) {
            position = getIntent().getIntExtra("position", 0);
            userposition = getIntent().getIntExtra("userposition", 0);
        }

        if (!new UserSharedPrefrence(mContext).getLOGIN_JSON().equalsIgnoreCase("NAN")) {
            AddUserData(mContext);
            Utility.getUserModelData(mContext);

            if (Global.userdataList != null) {
                if (new UserSharedPrefrence(mContext).getLoginModel().getUserID() != null) {
                    if (Global.userdataList.get(userposition).getUserID() != null) {
                        if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(Global.userdataList.get(userposition).getUserID())) {
                            issameUser = true;
                        } else {
                            issameUser = false;
                        }
                    } else {
                        issameUser = false;
                    }
                } else {
                    issameUser = false;
                }
            } else {
                issameUser = false;
            }

            if (issameUser) {
                Intent i = new Intent(WidgetManageActivity.this, HomeWorkFragmentActivity.class);
                i.putExtra("position", getResources().getString(R.string.Wall));
                startActivity(i);
                finish();
                overridePendingTransition(0,0);
            } else {
                new UserSharedPrefrence(mContext).setLoginModel(Global.userdataList.get(userposition));
                Global.userdataModel = Global.userdataList.get(userposition);
            }
        } else {
            Intent i = new Intent(WidgetManageActivity.this, RegisterActivity.class);
            startActivity(i);
            finish();
            overridePendingTransition(0, 0);
        }
    }

    public void AddUserData(Context mContext) {
        String result = new UserSharedPrefrence(mContext).getLOGIN_JSON();

        JSONArray jsonObj;
        try {
            Global.userdataList = new ArrayList<LoginModel>();

            if (result.contains("\"success\":0")) {

            } else {
                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    LoginModel model = new LoginModel();
                    model = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                    if (ServiceResource.SINGLE_DEVICE_FLAG) {
                        if (!(ServiceResource.STATICINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                            if (innerObj.getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID))) {
                                Global.userdataList.add(model);
                            }
                        } else {
                            Global.userdataList.add(model);
                        }
                    } else {
                        Global.userdataList.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result.contains("Updated")) {
                GetUserRoleRightList();

            }
        } else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            try {
                JSONArray jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).setREADWRITESETTING(result);
                Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
                ReadWriteSettingModel model;
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new ReadWriteSettingModel();
                    model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                    model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));
                    if (!new UserSharedPrefrence(mContext).getLoginModel().getRoleName().equalsIgnoreCase("teacher")) {
                        model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                        model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                        model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                        model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));
                    } else {
                        model.setIsView("true");
                        model.setIsEdit("true");
                        model.setIsCreate("true");
                        model.setIsDelete("true");
                    }
                    Global.readWriteSettingList.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            saveLoginLog();

        } else if (ServiceResource.SAVELOGINLOG_METHODNAME.equalsIgnoreCase(methodName)) {
            Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
            i.putExtra("position", position);
            mContext.startActivity(i);
            ((Activity) mContext).finish();
        }
    }

    public void saveLoginLog() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SAVELOGINLOG_METHODNAME, ServiceResource.LOGIN_URL);
    }

    public void GetUserRoleRightList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, new UserSharedPrefrence(mContext).getLoginModel().getRoleID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);
    }

}
