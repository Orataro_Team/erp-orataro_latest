package com.edusunsoft.erp.orataro.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ExamModel;
import com.edusunsoft.erp.orataro.model.FeeTransactionDetailModel;

import static com.edusunsoft.erp.orataro.services.ServiceResource.CURRENTEXAM;

public class ExamDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ExamModel examModel;
    FeeTransactionDetailModel feeTransactionDetailModel;
    ImageView imgLeftHeader, imgRightHeader;
    LinearLayout lyl_std_lowest, lyl_std_highest, lyl_result_type;
    TextView header_text, txt_result_type, txt_exam_date, txt_subjectname, txt_exam_type, txt_total_marks, txt_obtained_mark, txt_percentage, txt_std_highest, txt_std_lowest, txt_section_highest, txt_section_lowest;
    String FROMSTRING = "";
    CardView card_view, card_view2;
    TextView txt_status, txt_receipt_no, txt_fees_name, txt_date, txt_amount, txt_payment_id, txt_paid_by, txt_from;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_detail_activity);

        Initialization();

    }

    private void Initialization() {

        header_text = (TextView) findViewById(R.id.header_text);
        imgLeftHeader = (ImageView) findViewById(R.id.img_back);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);
        imgLeftHeader.setOnClickListener(this);

        card_view = (CardView) findViewById(R.id.card_view);
        card_view2 = (CardView) findViewById(R.id.card_view2);

        // bind All Textview for

        txt_status = (TextView) findViewById(R.id.txt_status);
        txt_receipt_no = (TextView) findViewById(R.id.txt_receipt_no);
        txt_fees_name = (TextView) findViewById(R.id.txt_fees_name);
        txt_date = (TextView) findViewById(R.id.txt_date);
        txt_amount = (TextView) findViewById(R.id.txt_amount);
        txt_payment_id = (TextView) findViewById(R.id.txt_payment_id);
        txt_paid_by = (TextView) findViewById(R.id.txt_paid_by);
        txt_from = (TextView) findViewById(R.id.txt_from);


        /*END*/

        lyl_std_highest = (LinearLayout) findViewById(R.id.lyl_std_highest);
        lyl_std_lowest = (LinearLayout) findViewById(R.id.lyl_std_lowest);
        lyl_result_type = (LinearLayout) findViewById(R.id.lyl_result_type);

        /*Find All Textview*/

        txt_exam_date = (TextView) findViewById(R.id.txt_exam_date);
        txt_subjectname = (TextView) findViewById(R.id.txt_subjectname);
        txt_exam_type = (TextView) findViewById(R.id.txt_exam_type);
        txt_total_marks = (TextView) findViewById(R.id.txt_total_marks);
        txt_obtained_mark = (TextView) findViewById(R.id.txt_obtained_mark);
        txt_percentage = (TextView) findViewById(R.id.txt_percentage);
        txt_std_highest = (TextView) findViewById(R.id.txt_std_highest);
        txt_std_lowest = (TextView) findViewById(R.id.txt_std_lowest);
        txt_section_highest = (TextView) findViewById(R.id.txt_section_highest);
        txt_section_lowest = (TextView) findViewById(R.id.txt_section_lowest);
        txt_result_type = (TextView) findViewById(R.id.txt_result_type);

        /*END*/

        FROMSTRING = getIntent().getStringExtra("FromString");

        if (getIntent() != null) {

            if (FROMSTRING.equalsIgnoreCase("Exam")) {

                card_view2.setVisibility(View.GONE);
                card_view.setVisibility(View.VISIBLE);
                examModel = (ExamModel) getIntent().getSerializableExtra(
                        "exadetailmodel");

                /*Fill the Exam Detail*/

                if (CURRENTEXAM == 1 || CURRENTEXAM == 2 || CURRENTEXAM == 3) {

                    lyl_std_highest.setVisibility(View.GONE);
                    lyl_std_lowest.setVisibility(View.GONE);
                    lyl_result_type.setVisibility(View.VISIBLE);

                }

                try {

                    FillDetail(examModel.getStr_DateOfExam(), examModel.getSubjectName(), examModel.getExamTypeName(),
                            examModel.getTotalMarks(), examModel.getObtainedMarks(), examModel.getPercentage(),
                            examModel.getDivisionHighest(), examModel.getDivisionLowest(),
                            examModel.getClassHighest(), examModel.getClassLowest());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                header_text.setText(getResources().getString(R.string.fee_transaction_detail));
                card_view.setVisibility(View.GONE);
                card_view2.setVisibility(View.VISIBLE);
                feeTransactionDetailModel = (FeeTransactionDetailModel) getIntent().getSerializableExtra(
                        "feetransactiondetailmodel");

                /*Fill the Exam Detail*/

                try {

                    FillReceiptTransactionDetail(feeTransactionDetailModel.getTransactionStatus(), feeTransactionDetailModel.getReceiptNo(),
                            feeTransactionDetailModel.getStructureDisplayName(), feeTransactionDetailModel.getStrTransactionDate(),
                            feeTransactionDetailModel.getAmount(), feeTransactionDetailModel.getPaymentGetWayTranID(), feeTransactionDetailModel.getUserName(),
                            feeTransactionDetailModel.getTransactionFrom());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            /*END*/

        }

    }

    private void FillReceiptTransactionDetail(String transactionStatus, String receiptNo, String structureDisplayName, String strTransactionDate, double amount,
                                              String paymentGetWayTranID, String userName, String transactionFrom) {

        txt_status.setText(transactionStatus);

        if (receiptNo.equalsIgnoreCase("null") || receiptNo.equalsIgnoreCase(null)) {
            txt_receipt_no.setText("");
        } else {
            txt_receipt_no.setText(receiptNo);
        }

        if (paymentGetWayTranID.equalsIgnoreCase("null") || paymentGetWayTranID.equalsIgnoreCase(null)) {
            txt_payment_id.setText("");
        } else {
            txt_payment_id.setText(paymentGetWayTranID);
        }

        txt_fees_name.setText(structureDisplayName);
        txt_date.setText(strTransactionDate);
        txt_amount.setText(String.valueOf(amount));
        txt_paid_by.setText(userName);
        txt_from.setText(transactionFrom);

    }


    private void FillDetail(String str_dateOfExam, String subjectName, String examTypeName, Double totalMarks, Double obtainedMarks,
                            Double percentage, Double divisionHighest, Double divisionLowest, Double classHighest, Double classLowest) {

        header_text.setText(subjectName + "(" + examModel.getExamName() + ")");

        txt_exam_date.setText(str_dateOfExam);
        txt_subjectname.setText(subjectName);
        txt_exam_type.setText(examTypeName);
        txt_total_marks.setText(String.valueOf(totalMarks));
        txt_obtained_mark.setText(String.valueOf(obtainedMarks));
        txt_percentage.setText(String.valueOf(percentage));
        txt_std_highest.setText(String.valueOf(divisionHighest));
        txt_std_lowest.setText(String.valueOf(divisionLowest));
        txt_section_highest.setText(String.valueOf(classHighest));
        txt_section_lowest.setText(String.valueOf(classLowest));
        txt_result_type.setText(examModel.getResultTypeTerm());


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:

                ExamDetailActivity.this.finish();

                break;

        }

    }

}
