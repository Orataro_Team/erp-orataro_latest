package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class NotesModel implements Serializable {

	private String NotesID;
	private String GradeID;
	private String DivisionID;
	private String SubjectID;
	private String SubjectName;
	private String DateOfNotes;
	private String NoteTitle;
	private String NoteDetails;
	private String ActionStartDate;
	private String ActionEndDate;
	private String DressCode;
	private boolean IsRead = false;
	private boolean IamReady;
	private String ParentNote;
	private String Rating;
	private String UserName;
	private String ProfilePicture;
	private boolean isVisible,isCheck;
	private String imgUrl;
	private String NoteDate;
	private String strActionStartDate;
	private String ReferenceLink;


	public String getReferenceLink() {
		return ReferenceLink;
	}

	public void setReferenceLink(String referenceLink) {
		ReferenceLink = referenceLink;
	}

	public String getStrActionStartDate() {
		return strActionStartDate;
	}

	public void setStrActionStartDate(String strActionStartDate) {
		this.strActionStartDate = strActionStartDate;
	}

	public String getNoteDate() {
		return NoteDate;
	}

	public void setNoteDate(String noteDate) {
		NoteDate = noteDate;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	String month, year, day, MilliSecond;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMilliSecond() {
		return MilliSecond;
	}

	public void setMilliSecond(String milliSecond) {
		MilliSecond = milliSecond;
	}

	public String getNotesID() {
		return NotesID;
	}

	public void setNotesID(String notesID) {
		NotesID = notesID;
	}

	public String getGradeID() {
		return GradeID;
	}

	public void setGradeID(String gradeID) {
		GradeID = gradeID;
	}

	public String getDivisionID() {
		return DivisionID;
	}

	public void setDivisionID(String divisionID) {
		DivisionID = divisionID;
	}

	public String getSubjectID() {
		return SubjectID;
	}

	public void setSubjectID(String subjectID) {
		SubjectID = subjectID;
	}

	public String getSubjectName() {
		return SubjectName;
	}

	public void setSubjectName(String subjectName) {
		SubjectName = subjectName;
	}

	public String getDateOfNotes() {
		return DateOfNotes;
	}

	public void setDateOfNotes(String dateOfNotes) {
		DateOfNotes = dateOfNotes;
	}

	public String getNoteTitle() {
		return NoteTitle;
	}

	public void setNoteTitle(String noteTitle) {
		NoteTitle = noteTitle;
	}

	public String getNoteDetails() {
		return NoteDetails;
	}

	public void setNoteDetails(String noteDetails) {
		NoteDetails = noteDetails;
	}

	public String getActionStartDate() {
		return ActionStartDate;
	}

	public void setActionStartDate(String actionStartDate) {
		ActionStartDate = actionStartDate;
	}

	public String getActionEndDate() {
		return ActionEndDate;
	}

	public void setActionEndDate(String actionEndDate) {
		ActionEndDate = actionEndDate;
	}

	public String getDressCode() {
		return DressCode;
	}

	public void setDressCode(String dressCode) {
		DressCode = dressCode;
	}

	public boolean isIsRead() {
		return IsRead;
	}

	public void setIsRead(boolean isRead) {
		IsRead = isRead;
	}

	public boolean isIamReady() {
		return IamReady;
	}

	public void setIamReady(boolean iamReady) {
		IamReady = iamReady;
	}

	public String getParentNote() {
		return ParentNote;
	}

	public void setParentNote(String parentNote) {
		ParentNote = parentNote;
	}

	public String getRating() {
		return Rating;
	}

	public void setRating(String rating) {
		Rating = rating;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

}
