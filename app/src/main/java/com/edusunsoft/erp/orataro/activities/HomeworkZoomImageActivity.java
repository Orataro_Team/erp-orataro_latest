package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener;
import com.edusunsoft.erp.orataro.model.WallPostModel;

public class HomeworkZoomImageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView img_back, iv_fb, iv_save;
    Context mContext;
    TextView txtHeader;
    LinearLayout ll_fb;
    WallPostModel wallPostModel;

    LinearLayout ll_save;
    ViewPager vpPager;
    public String img;
    public Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_img);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = HomeworkZoomImageActivity.this;

        vpPager = (ViewPager) findViewById(R.id.vpPager);
        vpPager.setVisibility(View.GONE);
        img_back = (ImageView) findViewById(R.id.img_back);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        ll_save.setVisibility(View.GONE);

        ll_fb = (LinearLayout) findViewById(R.id.ll_fb);
        iv_fb = (ImageView) findViewById(R.id.iv_fb_zoom);
        ll_fb.setVisibility(View.VISIBLE);
        txtHeader = (TextView) findViewById(R.id.header_text);
        txtHeader.setText("Homework Images");

        String img = getIntent().getStringExtra("img");
        Log.d("getimagess", img);


        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(img)
                    .apply(options)
                    .into(iv_fb);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        iv_fb.setScaleType(ImageView.ScaleType.MATRIX);
        ll_fb.setOnTouchListener(new PanAndZoomListener(ll_fb, iv_fb, PanAndZoomListener.Anchor.TOPLEFT));
        img_back.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            default:
                break;
        }
    }


}
