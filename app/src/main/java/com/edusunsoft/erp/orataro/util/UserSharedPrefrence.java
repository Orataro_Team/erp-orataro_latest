package com.edusunsoft.erp.orataro.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.gaurav.badgelibproject.provider.Badges;
import com.gaurav.badgelibproject.provider.BadgesNotSupportedException;
import com.google.firebase.database.annotations.NotNull;
import com.google.gson.Gson;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class UserSharedPrefrence {

    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    public String LOGIN_JSON = "loginJson";
    public String LOGIN_USERID = "UserID";
    public String LOGIN_DISPLAYNAME = "DisplayName";
    public String LOGIN_MEMBERID = "MemberID";
    public String LOGIN_CLIENTID = "ClientID";
    public String LOGIN_CTOKEN = "ctoken";
    public String LOGIN_INSTITUTEID = "InstituteID";
    public String LOGIN_ROLLID = "RoleID";
    public String LOGIN_ROLLNAME = "RoleName";
    public String LOGIN_WALLID = "WallID";
    public String LOGIN_PROFILEPIC = "ProfilePicture";
    public String LOGIN_WALLIMAGE = "WallImage";
    public String LOGIN_PRIMARYNAME = "PrimaryName";
    public String LOGIN_GRADEID = "GradeID";
    public String LOGIN_USERTYPE = "UserType";
    public String LOGIN_POSTBYTYPE = "PostByType";
    public String LOGIN_INSTITUTIONWALLID = "InstitutionWallID";
    public String LOGIN_STANDERDWALLID = "StanderdWallId";
    public String LOGIN_DIVISIONWALLID = "DivisionWallId";
    public String LOGIN_SUBJECTWALLID = "SubjectWallId";
    public String LOGIN_PROFILEWALLID = "ProfileWallId";
    public String LOGIN_PROFILEWALLIDIMAGE = "ProfileWallIdImage";
    public String LOGIN_INSTITUTELOGO = "InstituteLogo";
    public String LOGIN_GRADENAME = "GradeName";
    public String LOGIN_NOTIFICATIONCOUNT = "NotificationCount";
    public String LOGIN_FRIENDCOUNT = "FriendCount";
    public String LOGIN_PRAYER = "Prayer";
    public String LOGIN_FULLNAME = "FullName";
    public String ISALLOWUSERTOPOSTSTATUS = "IsAllowUserToPostStatus";
    public String ISALLOWUSERTOPOSTCOMMENT = "IsAllowUserToPostComment";
    public String ISALLOWUSERTOPOSTPHOTO = "IsAllowUserToPostPhoto";
    public String ISALLOWUSERTOPOSTVIDEO = "IsAllowUserToPostVideo";
    public String ISALLOWUSERTOPOSTFILES = "IsAllowUserToPostFiles";
    public String ISALLOWUSERTOSHAREPOST = "IsAllowUserToSharePost";
    public String ISALLOWUSERTOLIKEDISLIKES = "IsAllowUserToLikeDislikes";
    public String ISALLOWUSERTOPOSTALBUM = "IsAllowUserToPostAlbum";
    public String ISMULTIPLE = "isMultiple";
    public String INSTITUTENAME = "InstituteName";
    public String NOTIFICATIONMESSAGE = "NotificationMessage";
    public String LOGIN_MEMBERTYPE = "MemberType";
    public String READWRITESETTING = "ReadWriteSetting";
    public String LOGIN_FIRSTNAME = "firstName";
    public String LOGIN_STUDENTCODE = "StudentCode";
    public String LOGIN_MOBILENUMBER = "MobileNumber";
    public String LOGIN_PASSWORD = "password";
    public String LOGIN_USERNAME = "username";
    public String OTP = "otp";
    public String DATE_FOR_API_CALL_ONCE = "";
    public String MOBILENUMBER = "mobilenumber";
    public String CTOKEN = "ctoken";
    public String LOGIN_BTMPROFILEPIC = "btmprofilepic";
    public String LOGIN_INSTITUTECODE = "InstituteCode";
    public String CALENDAREVENT = "calendarevent";
    public String CURRENTWALLID = "CurrentWallId";
    public String STUDENTHOSTELREGID = "StudentHostelRegID";
    public String STUDENTBUSREGMASTERID = "StudentBusRegMasterID";
    public String BATCHID = "batchId";
    public String BATCHSTART = "BatchStart";
    public String BATCHEND = "BatchEnd";
    public String ISREMEMBAR = "isRemember";
    public String LEAVECOUNT = "leavecount";
    public String isSoundNotification = "isSoundNotification";
    public String isVibrationNotification = "isVibrationNotification";
    public String isLedNotification = "isLedNotification";
    public String isERP = "isERP";
    public String LOGIN_INFORMATION = "Information";
    public String LOGIN_SCHOOLTIMING = "SchoolTiming";
    public String LOGIN_DIVISIONNAME = "DivisionName";
    public String LOGIN_DIVISIONID = "DivisionID";
    public String LOGIN_ERP_MEMBER_ID = "ERP_MEMBER_ID";
    private String PREF_NAME = "SQTUSER";


    public boolean getIsERP() {
        return pref.getBoolean(isERP, false);
    }

    public void setIsERP(boolean isERP) {
        editor = pref.edit();
        editor.putBoolean(this.isERP, isERP);
        editor.commit();
    }

    public String getLEAVECOUNT() {
        return pref.getString(LEAVECOUNT, "0");
    }

    public void setLEAVECOUNT(String lEAVECOUNT) {
        editor = pref.edit();
        editor.putString(LEAVECOUNT, lEAVECOUNT);
        editor.commit();
    }

    public boolean getISREMEMBAR() {

        return pref.getBoolean(ISREMEMBAR, false);
    }

    public void setISREMEMBAR(boolean iSREMEMBAR) {

        editor = pref.edit();
        editor.putBoolean(ISREMEMBAR, iSREMEMBAR);
        editor.commit();

    }

    public String getCURRENTWALLID() {
        return pref.getString(CURRENTWALLID, "NAN");
    }

    public void setCURRENTWALLID(String cURRENTWALLID) {
        editor = pref.edit();
        editor.putString(CURRENTWALLID, cURRENTWALLID);
        editor.commit();
    }

    public void setSTUDENTHOSTELREGID(String studhostelregid) {
        editor = pref.edit();
        editor.putString(STUDENTHOSTELREGID, studhostelregid);
        editor.commit();
    }

    public String getSTUDENTHOSTELREGID() {
        return pref.getString(STUDENTHOSTELREGID, null);
    }

    public void setSTUDENTBUSREGMASTERID(String studentbusregmasterid) {
        editor = pref.edit();
        editor.putString(STUDENTBUSREGMASTERID, studentbusregmasterid);
        editor.commit();
    }

    public String getSTUDENTBUSREGMASTERID() {
        return pref.getString(STUDENTBUSREGMASTERID, null);
    }

    public void setSTUDENTBUSREGID(String studbusregid) {
        editor = pref.edit();
        editor.putString(STUDENTBUSREGMASTERID, studbusregid);
        editor.commit();
    }

    public String getSTUDENTBUSREGID() {
        return pref.getString(STUDENTBUSREGMASTERID, null);
    }

    public String getBATCHID() {
        return pref.getString(BATCHID, "0");
    }

    public void setBATCHID(String bATCHID) {
        editor = pref.edit();
        editor.putString(BATCHID, bATCHID);
        editor.commit();
    }

    public String getBATCHSTART() {
        return pref.getString(BATCHSTART, "0");
    }

    public void setBATCHSTART(String bATCHSTART) {
        editor = pref.edit();
        editor.putString(BATCHSTART, bATCHSTART);
        editor.commit();
    }

    public String getBATCHEND() {
        return pref.getString(BATCHEND, "0");
    }

    public void setBATCHEND(String bATCHEND) {
        editor = pref.edit();
        editor.putString(BATCHEND, bATCHEND);
        editor.commit();
    }

    public boolean getIsSoundNotification() {
        return pref.getBoolean(this.isSoundNotification, true);
    }

    public void setIsSoundNotification(boolean isSoundNotification) {
        editor = pref.edit();
        editor.putBoolean(this.isSoundNotification, isSoundNotification);
        editor.commit();
    }

    public boolean getIsVibrationNotification() {
        return pref.getBoolean(this.isVibrationNotification, true);
    }

    public void setIsVibrationNotification(boolean isVibrationNotification) {
        editor = pref.edit();
        editor.putBoolean(this.isVibrationNotification, isVibrationNotification);
        editor.commit();
    }

    public boolean getIsLedNotification() {
        return pref.getBoolean(this.isLedNotification, true);
    }

    public void setIsLedNotification(boolean isLedNotification) {
        editor = pref.edit();
        editor.putBoolean(this.isLedNotification, isLedNotification);
        editor.commit();
    }

    public String getLOGIN_BTMPROFILEPIC() {
        return pref.getString(LOGIN_BTMPROFILEPIC, "0");
    }

    public void setLOGIN_BTMPROFILEPIC(String lOGIN_BTMPROFILEPIC) {
        editor = pref.edit();
        editor.putString(LOGIN_BTMPROFILEPIC, lOGIN_BTMPROFILEPIC);
        editor.commit();
    }

    public String getCALENDAREVENT() {
        return pref.getString(CALENDAREVENT, "0");
    }

    public void setCALENDAREVENT(String cALENDAREVENT) {
        editor = pref.edit();
        editor.putString(CALENDAREVENT, cALENDAREVENT);
        editor.commit();
    }

    public String getDATE_FOR_API_CALL_ONCE() {
        return pref.getString(DATE_FOR_API_CALL_ONCE, "");
    }

    public void setDATE_FOR_API_CALL_ONCE(String date_for_api_call_once) {
        editor = pref.edit();
        editor.putString(DATE_FOR_API_CALL_ONCE, date_for_api_call_once);
        editor.commit();
    }


    public String getOTP() {
        return pref.getString(OTP, "");
    }

    public void setOTP(String otp) {
        editor = pref.edit();
        editor.putString(OTP, otp);
        editor.commit();
    }

    public String get_MOBILENUMBER() {
        return pref.getString(MOBILENUMBER, "");
    }

    public void setMOBILENUMBER(String mobilenumber) {
        editor = pref.edit();
        editor.putString(MOBILENUMBER, mobilenumber);
        editor.commit();
    }

    public String get_CTOKEN() {
        return pref.getString(CTOKEN, "");
    }

    public void setCTOKEN(String ctoken) {
        editor = pref.edit();
        editor.putString(CTOKEN, ctoken);
        editor.commit();
    }

    public String getLOGIN_USERNAME() {
        return pref.getString(LOGIN_USERNAME, "");
    }

    public void setLOGIN_USERNAME(String lOGIN_USERNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_USERNAME, lOGIN_USERNAME);
        editor.commit();
    }

    public String getLOGIN_INSTITUTECODE() {
        return pref.getString(LOGIN_INSTITUTECODE, "");
    }

    public void setLOGIN_INSTITUTECODE(String lOGIN_INSTITUTECODE) {
        editor = pref.edit();
        editor.putString(LOGIN_INSTITUTECODE, lOGIN_INSTITUTECODE);
        editor.commit();
    }

    public String getLOGIN_PASSWORD() {
        return pref.getString(LOGIN_PASSWORD, "");
    }

    public void setLOGIN_PASSWORD(String lOGIN_PASSWORD) {
        editor = pref.edit();
        editor.putString(LOGIN_PASSWORD, lOGIN_PASSWORD);
        editor.commit();
    }

    public String getLOGIN_STUDENTCODE() {
        return pref.getString(LOGIN_STUDENTCODE, "");
    }

    public void setLOGIN_STUDENTCODE(String lOGIN_STUDENTCODE) {
        editor = pref.edit();
        editor.putString(LOGIN_STUDENTCODE, lOGIN_STUDENTCODE);
        editor.commit();
    }

    public String getLOGIN_MOBILENUMBER() {
        return pref.getString(LOGIN_MOBILENUMBER, "");
    }

    public void setLOGIN_MOBILENUMBER(String lOGIN_MOBILENUMBER) {
        editor = pref.edit();
        editor.putString(LOGIN_MOBILENUMBER, lOGIN_MOBILENUMBER);
        editor.commit();
    }

    public String getLOGIN_FIRSTNAME() {
        return pref.getString(LOGIN_FIRSTNAME, "");
    }

    public void setLOGIN_FIRSTNAME(String lOGIN_FIRSTNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_FIRSTNAME, lOGIN_FIRSTNAME);
        editor.commit();
    }

    public String getREADWRITESETTING() {
        return pref.getString(READWRITESETTING, "");
    }

    public void setREADWRITESETTING(String rEADWRITESETTING) {
        editor = pref.edit();
        editor.putString(READWRITESETTING, rEADWRITESETTING);
        editor.commit();
    }

    public String getLOGIN_MEMBERTYPE() {
        return pref.getString(LOGIN_MEMBERTYPE, "");
    }

    public void setLOGIN_MEMBERTYPE(String lOGIN_MEMBERTYPE) {
        //		LOGIN_MEMBERTYPE = lOGIN_MEMBERTYPE;
        editor = pref.edit();
        editor.putString(LOGIN_MEMBERTYPE, lOGIN_MEMBERTYPE);
        editor.commit();
    }

    public String getNOTIFICATIONMESSAGE() {
        return pref.getString(NOTIFICATIONMESSAGE, "");
    }

    public void setNOTIFICATIONMESSAGE(String nOTIFICATIONMESSAGE) {
        editor = pref.edit();
        editor.putString(NOTIFICATIONMESSAGE, nOTIFICATIONMESSAGE);
        editor.commit();
    }

    public String getINSTITUTENAME() {
        return pref.getString(INSTITUTENAME, "NAN");
    }

    public void setINSTITUTENAME(String iNSTITUTENAME) {

        editor = pref.edit();
        editor.putString(INSTITUTENAME, iNSTITUTENAME);
        editor.commit();
    }

    public boolean getISMULTIPLE() {
        return pref.getBoolean(ISMULTIPLE, false);
    }

    public void setISMULTIPLE(boolean iSMULTIPLE) {

        editor = pref.edit();
        editor.putBoolean(ISMULTIPLE, iSMULTIPLE);
        editor.commit();

    }

    public String getISALLOWUSERTOPOSTSTATUS() {

        return pref.getString(ISALLOWUSERTOPOSTSTATUS, "false");

    }

    public void setISALLOWUSERTOPOSTSTATUS(String iSALLOWUSERTOPOSTSTATUS) {

        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTSTATUS, iSALLOWUSERTOPOSTSTATUS);
        editor.commit();

    }

    public String getISALLOWUSERTOPOSTCOMMENT() {
        return pref.getString(ISALLOWUSERTOPOSTCOMMENT, "false");
    }

    public void setISALLOWUSERTOPOSTCOMMENT(String iSALLOWUSERTOPOSTCOMMENT) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTCOMMENT, iSALLOWUSERTOPOSTCOMMENT);
        editor.commit();
    }

    public String getISALLOWUSERTOPOSTPHOTO() {
        return pref.getString(ISALLOWUSERTOPOSTPHOTO, "false");
    }

    public void setISALLOWUSERTOPOSTPHOTO(String iSALLOWUSERTOPOSTPHOTO) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTPHOTO, iSALLOWUSERTOPOSTPHOTO);
        editor.commit();
    }

    public String getISALLOWUSERTOPOSTVIDEO() {
        return pref.getString(ISALLOWUSERTOPOSTVIDEO, "false");
    }

    public void setISALLOWUSERTOPOSTVIDEO(String iSALLOWUSERTOPOSTVIDEO) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTVIDEO, iSALLOWUSERTOPOSTVIDEO);
        editor.commit();
    }

    public String getISALLOWUSERTOPOSTFILES() {
        return pref.getString(ISALLOWUSERTOPOSTFILES, "false");
    }

    public void setISALLOWUSERTOPOSTFILES(String iSALLOWUSERTOPOSTFILES) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTFILES, iSALLOWUSERTOPOSTFILES);
        editor.commit();
    }

    public String getISALLOWUSERTOSHAREPOST() {
        return pref.getString(ISALLOWUSERTOSHAREPOST, "false");
    }

    public void setISALLOWUSERTOSHAREPOST(String iSALLOWUSERTOSHAREPOST) {
        //	ISALLOWUSERTOSHAREPOST = iSALLOWUSERTOSHAREPOST;
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOSHAREPOST, iSALLOWUSERTOSHAREPOST);
        editor.commit();
    }

    public String getISALLOWUSERTOLIKEDISLIKES() {
        return pref.getString(ISALLOWUSERTOLIKEDISLIKES, "false");
    }

    public void setISALLOWUSERTOLIKEDISLIKES(String iSALLOWUSERTOLIKEDISLIKES) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOLIKEDISLIKES, iSALLOWUSERTOLIKEDISLIKES);
        editor.commit();
    }

    public String getISALLOWUSERTOPOSTALBUM() {
        return pref.getString(ISALLOWUSERTOPOSTALBUM, "false");
    }

    public void setISALLOWUSERTOPOSTALBUM(String iSALLOWUSERTOPOSTALBUM) {
        editor = pref.edit();
        editor.putString(ISALLOWUSERTOPOSTALBUM, iSALLOWUSERTOPOSTALBUM);
        editor.commit();
    }


    public String getLOGIN_FULLNAME() {
        return pref.getString(LOGIN_FULLNAME, "NAN");
    }

    public void setLOGIN_FULLNAME(String lOGIN_FULLNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_FULLNAME, lOGIN_FULLNAME);
        editor.commit();
    }

    public String getLOGIN_PRAYER() {
        return pref.getString(LOGIN_PRAYER, "NAN");
    }

    public void setLOGIN_PRAYER(String lOGIN_PRAYER) {
        editor = pref.edit();
        editor.putString(LOGIN_PRAYER, lOGIN_PRAYER);
        editor.commit();
    }

    public String getLOGIN_INFORMATION() {
        return pref.getString(LOGIN_INFORMATION, "NAN");
    }

    public void setLOGIN_INFORMATION(String lOGIN_INFORMATION) {
        editor = pref.edit();
        editor.putString(LOGIN_INFORMATION, lOGIN_INFORMATION);
        editor.commit();
    }

    public String getLOGIN_SCHOOLTIMING() {
        return pref.getString(LOGIN_SCHOOLTIMING, "NAN");
    }

    public void setLOGIN_SCHOOLTIMING(String lOGIN_SCHOOLTIMING) {
        editor = pref.edit();
        editor.putString(LOGIN_SCHOOLTIMING, lOGIN_SCHOOLTIMING);
        editor.commit();
    }


    public String getLOGIN_GRADENAME() {
        return pref.getString(LOGIN_GRADENAME, "NAN");
    }

    public void setLOGIN_GRADENAME(String lOGIN_GRADENAME) {
        editor = pref.edit();
        editor.putString(LOGIN_GRADENAME, lOGIN_GRADENAME);
        editor.commit();
    }

    public String getLOGIN_DIVISIONNAME() {
        return pref.getString(LOGIN_DIVISIONNAME, "NAN");
    }

    public void setLOGIN_DIVISIONNAME(String lOGIN_DIVISIONNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_DIVISIONNAME, lOGIN_DIVISIONNAME);
        editor.commit();
    }

    public String getLOGIN_NOTIFICATIONCOUNT() {
        return pref.getString(LOGIN_NOTIFICATIONCOUNT, "0");
    }

    public void setLOGIN_NOTIFICATIONCOUNT(String lOGIN_NOTIFICATIONCOUNT) {
        try {
            try {
                Badges.removeBadge(_context);
                Badges.setBadge(_context, Integer.valueOf(lOGIN_NOTIFICATIONCOUNT));
            } catch (BadgesNotSupportedException badgesNotSupportedException) {
                Log.d("Badge provider", badgesNotSupportedException.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        editor = pref.edit();
        editor.putString(LOGIN_NOTIFICATIONCOUNT, lOGIN_NOTIFICATIONCOUNT);
        editor.commit();
    }

    public String getLOGIN_FRIENDCOUNT() {
        return pref.getString(LOGIN_FRIENDCOUNT, "0");
    }

    public void setLOGIN_FRIENDCOUNT(String lOGIN_FRIENDCOUNT) {
        editor = pref.edit();
        editor.putString(LOGIN_FRIENDCOUNT, lOGIN_FRIENDCOUNT);
        editor.commit();
    }

    public String getLOGIN_JSON() {
        return pref.getString(LOGIN_JSON, "NAN");
    }

    public void setLOGIN_JSON(String lOGIN_JSON) {
        editor = pref.edit();
        editor.putString(LOGIN_JSON, lOGIN_JSON);
        editor.commit();
    }

    public String getLOGIN_PROFILEWALLIDIMAGE() {
        return pref.getString(LOGIN_PROFILEWALLIDIMAGE, "NAN");
    }

    public String getLOGIN_INSTITUTELOGO() {
        return pref.getString(LOGIN_INSTITUTELOGO, "NAN");
    }

    public void setLOGIN_PROFILEWALLIDIMAGE(String lOGIN_PROFILEWALLIDIMAGE) {
        editor = pref.edit();
        editor.putString(LOGIN_PROFILEWALLIDIMAGE, lOGIN_PROFILEWALLIDIMAGE);
        editor.commit();
    }

    public void setLOGIN_INSTITUTELOGO(String lOGIN_INSTITUTELOGO) {
        editor = pref.edit();
        editor.putString(LOGIN_INSTITUTELOGO, lOGIN_INSTITUTELOGO);
        editor.commit();
    }

    public UserSharedPrefrence(Context pContext) {

        _context = pContext;
        pref = _context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);

    }

    public int getLOGIN_USERTYPE() {

        return pref.getInt(LOGIN_USERTYPE, 100000);

    }

    public void setLOGIN_USERTYPE(int lOGIN_USERTYPE) {
        editor = pref.edit();
        editor.putInt(LOGIN_USERTYPE, lOGIN_USERTYPE);
        editor.commit();
    }

    public String getLOGIN_POSTBYTYPE() {
        return pref.getString(LOGIN_POSTBYTYPE, "NAN");
    }

    public void setLOGIN_POSTBYTYPE(String lOGIN_POSTBYTYPE) {
        editor = pref.edit();
        editor.putString(LOGIN_POSTBYTYPE, lOGIN_POSTBYTYPE);
        editor.commit();
    }

    public String getLOGIN_INSTITUTIONWALLID() {
        return pref.getString(LOGIN_INSTITUTIONWALLID, null);
    }

    public void setLOGIN_INSTITUTIONWALLID(String lOGIN_INSTITUTIONWALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_INSTITUTIONWALLID, lOGIN_INSTITUTIONWALLID);
        editor.commit();
    }

    public String getLOGIN_STANDERDWALLID() {
        return pref.getString(LOGIN_STANDERDWALLID, null);
    }

    public void setLOGIN_STANDERDWALLID(String lOGIN_STANDERDWALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_STANDERDWALLID, lOGIN_STANDERDWALLID);
        editor.commit();
    }

    public String getLOGIN_DIVISIONWALLID() {
        return pref.getString(LOGIN_DIVISIONWALLID, null);
    }

    public void setLOGIN_DIVISIONWALLID(String lOGIN_DIVISIONWALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_DIVISIONWALLID, lOGIN_DIVISIONWALLID);
        editor.commit();
    }

    public String getLOGIN_SUBJECTWALLID() {
        return pref.getString(LOGIN_SUBJECTWALLID, null);
    }

    public void setLOGIN_SUBJECTWALLID(String lOGIN_SUBJECTWALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_SUBJECTWALLID, lOGIN_SUBJECTWALLID);
        editor.commit();
    }

    public String getLOGIN_PROFILEWALLID() {
        return pref.getString(LOGIN_PROFILEWALLID, "NAN");
    }

    public void setLOGIN_PROFILEWALLID(String lOGIN_PROFILEWALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_PROFILEWALLID, lOGIN_PROFILEWALLID);
        editor.commit();
    }

    public String getLOGIN_USERID() {
        return pref.getString(LOGIN_USERID, "NAN");
    }

    public void setLOGIN_USERID(String lOGIN_USERID) {
        editor = pref.edit();
        editor.putString(LOGIN_USERID, lOGIN_USERID);
        editor.commit();
    }

    public String getLOGIN_DISPLAYNAME() {
        return pref.getString(LOGIN_DISPLAYNAME, "NAN");
    }

    public void setLOGIN_DISPLAYNAME(String lOGIN_DISPLAYNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_DISPLAYNAME, lOGIN_DISPLAYNAME);
        editor.commit();
    }

    public String getLOGIN_GRADEID() {
        return pref.getString(LOGIN_GRADEID, null);
    }

    public void setLOGIN_GRADEID(String lOGIN_GRADEID) {
        editor = pref.edit();
        editor.putString(LOGIN_GRADEID, lOGIN_GRADEID);
        editor.commit();
    }

    public void setERPMemberID(String erpMemberID) {
        editor = pref.edit();
        editor.putString(LOGIN_ERP_MEMBER_ID, erpMemberID);
        editor.commit();
    }

    public String getErpMemberID() {
        return pref.getString(LOGIN_ERP_MEMBER_ID, "NAN");
    }

    public String getLOGIN_DIVISIONID() {
        return pref.getString(LOGIN_DIVISIONID, null);
    }

    public void setLOGIN_DIVISIONID(String lOGIN_DIVISIONID) {
        editor = pref.edit();
        editor.putString(LOGIN_DIVISIONID, lOGIN_DIVISIONID);
        editor.commit();
    }

    public String getLOGIN_MEMBERID() {
        return pref.getString(LOGIN_MEMBERID, "NAN");
    }

    public void setLOGIN_MEMBERID(String lOGIN_MEMBERID) {
        editor = pref.edit();
        editor.putString(LOGIN_MEMBERID, lOGIN_MEMBERID);
        editor.commit();
    }

    public String getLOGIN_CLIENTID() {
        return pref.getString(LOGIN_CLIENTID, "NAN");
    }

    public void setLOGIN_CLIENTID(String lOGIN_CLIENTID) {
        editor = pref.edit();
        editor.putString(LOGIN_CLIENTID, lOGIN_CLIENTID);
        editor.commit();
    }

    public String getLOGIN_CTOKEN() {
        return pref.getString(LOGIN_CTOKEN, "");
    }

    public void setLOGIN_CTOKEN(String lOGIN_CTOKEN) {
        editor = pref.edit();
        editor.putString(LOGIN_CTOKEN, lOGIN_CTOKEN);
        editor.commit();
    }

    public String getLOGIN_INSTITUTEID() {
        return pref.getString(LOGIN_INSTITUTEID, "NAN");
    }

    public void setLOGIN_INSTITUTEID(String lOGIN_INSTITUTEID) {
        editor = pref.edit();
        editor.putString(LOGIN_INSTITUTEID, lOGIN_INSTITUTEID);
        editor.commit();
    }

    public String getLOGIN_ROLLID() {
        return pref.getString(LOGIN_ROLLID, "NAN");
    }

    public void setLOGIN_ROLLID(String lOGIN_ROLLID) {
        editor = pref.edit();
        editor.putString(LOGIN_ROLLID, lOGIN_ROLLID);
        editor.commit();
    }

    public String getLOGIN_ROLLNAME() {
        return pref.getString(LOGIN_ROLLNAME, "NAN");
    }

    public void setLOGIN_ROLLNAME(String lOGIN_ROLLNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_ROLLNAME, lOGIN_ROLLNAME);
        editor.commit();
    }

    public String getLOGIN_WALLID() {
        return pref.getString(LOGIN_WALLID, "NAN");
    }

    public void setLOGIN_WALLID(String lOGIN_WALLID) {
        editor = pref.edit();
        editor.putString(LOGIN_WALLID, lOGIN_WALLID);
        editor.commit();
    }

    public String getLOGIN_PROFILEPIC() {
        return pref.getString(LOGIN_PROFILEPIC, "NAN");
    }

    public void setLOGIN_PROFILEPIC(String lOGIN_PROFILEPIC) {
        editor = pref.edit();
        editor.putString(LOGIN_PROFILEPIC, lOGIN_PROFILEPIC);
        editor.commit();
    }

    public String getLOGIN_WALLIMAGE() {
        return pref.getString(LOGIN_WALLIMAGE, "NAN");
    }

    public void setLOGIN_WALLIMAGE(String lOGIN_WALLIMAGE) {
        editor = pref.edit();
        editor.putString(LOGIN_WALLIMAGE, lOGIN_WALLIMAGE);
        editor.commit();
    }

    public String getLOGIN_PRIMARYNAME() {
        return pref.getString(LOGIN_PRIMARYNAME, "NAN");
    }

    public void setLOGIN_PRIMARYNAME(String lOGIN_PRIMARYNAME) {
        editor = pref.edit();
        editor.putString(LOGIN_PRIMARYNAME, lOGIN_PRIMARYNAME);
        editor.commit();
    }

    public LoginModel getLoginModel() {

        LoginModel loginModel = new LoginModel();
        loginModel.setUserID(getLOGIN_USERID());
        loginModel.setDisplayName(getLOGIN_DISPLAYNAME());
        loginModel.setMemberID(getLOGIN_MEMBERID());
        loginModel.setClientID(getLOGIN_CLIENTID());
        loginModel.setERPMemberID(getErpMemberID());
        loginModel.setInstituteID(getLOGIN_INSTITUTEID());
        loginModel.setRoleID(getLOGIN_ROLLID());
        loginModel.setRoleName(getLOGIN_ROLLNAME());
        loginModel.setWallID(getLOGIN_WALLID());
        loginModel.setProfilePicture(getLOGIN_PROFILEPIC());
        loginModel.setWallImage(getLOGIN_WALLIMAGE());
        loginModel.setPrimaryName(getLOGIN_PRIMARYNAME());
        loginModel.setGradeID(getLOGIN_GRADEID());
        loginModel.setDivisionID(getLOGIN_DIVISIONID());
        loginModel.setUserType(getLOGIN_USERTYPE());
        loginModel.setPostByType(getLOGIN_POSTBYTYPE());
        loginModel.setInstituteWallId(getLOGIN_INSTITUTIONWALLID());
        loginModel.setDivisionWallId(getLOGIN_DIVISIONWALLID());
        loginModel.setSubjectWallId(getLOGIN_SUBJECTWALLID());
        loginModel.setStandarWallId(getLOGIN_STANDERDWALLID());
        loginModel.setProfileWallId(getLOGIN_PROFILEWALLID());
        loginModel.setInstitutionWallImage(getLOGIN_PROFILEWALLIDIMAGE());
        loginModel.setInstituteLogo(getLOGIN_INSTITUTELOGO());
        loginModel.setLoginJson(getLOGIN_JSON());
        loginModel.setGradeName(getLOGIN_GRADENAME());
        loginModel.setDivisionName(getLOGIN_DIVISIONNAME());
        loginModel.setPrayer(getLOGIN_PRAYER());
        loginModel.setInformation(getLOGIN_INFORMATION());
        loginModel.setSchoolTiming(getLOGIN_SCHOOLTIMING());
        loginModel.setFullName(getLOGIN_FULLNAME());

        loginModel.setIsAllowUserToPostStatus(getISALLOWUSERTOPOSTSTATUS());
        loginModel.setIsAllowUserToPostComment(getISALLOWUSERTOPOSTCOMMENT());
        loginModel.setIsAllowUserToPostPhoto(getISALLOWUSERTOPOSTPHOTO());
        loginModel.setIsAllowUserToPostVideo(getISALLOWUSERTOPOSTVIDEO());
        loginModel.setIsAllowUserToPostFiles(getISALLOWUSERTOPOSTFILES());
        loginModel.setIsAllowUserToSharePost(getISALLOWUSERTOSHAREPOST());
        loginModel.setIsAllowUserToLikeDislikes(getISALLOWUSERTOLIKEDISLIKES());
        loginModel.setAllowUserToPostAlbum(getISALLOWUSERTOPOSTALBUM());

        loginModel.setInstituteName(getINSTITUTENAME());
        loginModel.setMemberType(getLOGIN_MEMBERTYPE());
        loginModel.setFirstName(getLOGIN_FIRSTNAME());
        loginModel.setMobileNumber(getLOGIN_MOBILENUMBER());
        loginModel.setStudentCode(getLOGIN_STUDENTCODE());
        loginModel.setInstituteCode(getLOGIN_INSTITUTECODE());
        loginModel.setBtmProfilepic(getLOGIN_BTMPROFILEPIC());
        loginModel.setBatchID(getBATCHID());
        loginModel.setBatchStart(getBATCHSTART());
        loginModel.setBatchEnd(getBATCHEND());

        loginModel.setCurrentWallId(getCURRENTWALLID());
        loginModel.setStudentHostelRegID(getSTUDENTHOSTELREGID());
        loginModel.setStudentBusRegMasterID(getSTUDENTBUSREGMASTERID());

        return loginModel;

    }

    public void setLoginModel(LoginModel loginModel) {

        setLOGIN_CLIENTID(loginModel.getClientID());
        setLOGIN_DISPLAYNAME(loginModel.getDisplayName());
        setLOGIN_INSTITUTEID(loginModel.getInstituteID());
        setLOGIN_MEMBERID(loginModel.getMemberID());
        setLOGIN_PRIMARYNAME(loginModel.getPrimaryName());
        setLOGIN_PROFILEPIC(loginModel.getProfilePicture());
        setLOGIN_ROLLID(loginModel.getRoleID());
        setLOGIN_ROLLNAME(loginModel.getRoleName());
        setLOGIN_USERID(loginModel.getUserID());
        setLOGIN_WALLID(loginModel.getWallID());
        setLOGIN_WALLIMAGE(loginModel.getWallImage());
        setLOGIN_GRADEID(loginModel.getGradeID());
        setERPMemberID(loginModel.getERPMemberID());
        setLOGIN_DIVISIONID(loginModel.getDivisionID());
        setLOGIN_USERTYPE(loginModel.getUserType());
        setLOGIN_POSTBYTYPE(loginModel.getPostByType());
        setLOGIN_INSTITUTIONWALLID(loginModel.getInstituteWallId());
        setLOGIN_DIVISIONWALLID(loginModel.getDivisionWallId());
        setLOGIN_STANDERDWALLID(loginModel.getStandarWallId());
        setLOGIN_SUBJECTWALLID(loginModel.getSubjectWallId());
        setLOGIN_PROFILEWALLID(loginModel.getProfileWallId());
        setLOGIN_PROFILEWALLIDIMAGE(loginModel.getInstitutionWallImage());
        setLOGIN_INSTITUTELOGO(loginModel.getInstituteLogo());
        setLOGIN_JSON(loginModel.getLoginJson());
        setLOGIN_DIVISIONNAME(loginModel.getDivisionName());
        setLOGIN_GRADENAME(loginModel.getGradeName());
        setLOGIN_PRAYER(loginModel.getPrayer());
        setLOGIN_INFORMATION(loginModel.getInformation());
        setLOGIN_SCHOOLTIMING(loginModel.getSchoolTiming());
        setLOGIN_FULLNAME(loginModel.getFullName());
        setISALLOWUSERTOPOSTSTATUS(loginModel.isIsAllowUserToPostStatus());
        setISALLOWUSERTOPOSTCOMMENT(loginModel.isIsAllowUserToPostComment());
        setISALLOWUSERTOPOSTPHOTO(loginModel.isIsAllowUserToPostPhoto());
        setISALLOWUSERTOPOSTVIDEO(loginModel.isIsAllowUserToPostVideo());
        setISALLOWUSERTOPOSTFILES(loginModel.isIsAllowUserToPostFiles());
        setISALLOWUSERTOSHAREPOST(loginModel.isIsAllowUserToSharePost());
        setISALLOWUSERTOLIKEDISLIKES(loginModel.isIsAllowUserToLikeDislikes());
        setISALLOWUSERTOPOSTALBUM(loginModel.isAllowUserToPostAlbum());
        setINSTITUTENAME(loginModel.getInstituteName());
        setLOGIN_MEMBERTYPE(loginModel.getMemberType());
        setLOGIN_FIRSTNAME(loginModel.getFirstName());
        setLOGIN_MOBILENUMBER(loginModel.getMobileNumber());
        setLOGIN_STUDENTCODE(loginModel.getStudentCode());
        setLOGIN_INSTITUTECODE(loginModel.getInstituteCode());
        setLOGIN_BTMPROFILEPIC(loginModel.getBtmProfilepic());
        setBATCHID(loginModel.getBatchID());
        setBATCHSTART(loginModel.getBatchStart());
        setBATCHEND(loginModel.getBatchEnd());
        setCURRENTWALLID(loginModel.getCurrentWallId());
        setSTUDENTHOSTELREGID(loginModel.getStudentHostelRegID());
        setSTUDENTBUSREGMASTERID(loginModel.getStudentBusRegMasterID());

    }

    public void clearPrefrence() {
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Save parent data from login
     */

    public static void saveLoginData(@NotNull Context context, @NotNull LoginUserModel loginuserModel) {
        String parentData = (new Gson()).toJson(loginuserModel);
        context.getSharedPreferences("LOGIN_USER_DATA", 0).edit().putString("LOGIN_DATA", parentData).apply();
    }

    @NotNull
    public static final LoginUserModel loadLoginData(@NotNull Context context) {
        LoginUserModel userModel = (new Gson()).fromJson(context.getSharedPreferences("LOGIN_USER_DATA", 0).getString("LOGIN_DATA", ""), LoginUserModel.class);
        return userModel;
    }

    @NotNull
    public static ArrayList<LoginUserModel> getSwitchUserListData(@NotNull Context context) {
        LoginUserModel userModel = (new Gson()).fromJson(context.getSharedPreferences("LOGIN_USER_DATA", 0).getString("LOGIN_DATA", ""), LoginUserModel.class);
        Global.loginuserList.add(userModel);
        return Global.loginuserList;
    }

}
