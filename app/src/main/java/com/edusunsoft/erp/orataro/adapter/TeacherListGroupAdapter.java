package com.edusunsoft.erp.orataro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.TeacherModel;

import java.util.ArrayList;
import java.util.Locale;

public class TeacherListGroupAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater layoutInfalater;
	private TextView tv_class, tv_teacher_name, tv_sub_name;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	private ArrayList<TeacherModel> list = new ArrayList<>();
	private ArrayList<TeacherModel> copyList = new ArrayList<>();

	public TeacherListGroupAdapter(Context context, ArrayList<TeacherModel> list) {
		this.mContext = context;
		this.list = list;
		copyList.addAll(list);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.teachers_list_items, parent, false);
		convertView.setBackgroundColor(colors[position % colors.length]);
		tv_class = (TextView) convertView.findViewById(R.id.tv_class);
		tv_teacher_name = (TextView) convertView.findViewById(R.id.tv_teacher_name);
		tv_sub_name = (TextView) convertView.findViewById(R.id.tv_sub_name);
		if(list.get(position).getTeacherId() != null){
			tv_class.setText(list.get(position).getTeacherId() );
		}
		if(list.get(position).getTeacherName() != null){
			tv_teacher_name.setText(list.get(position).getTeacherName());
		}
		if(list.get(position).getTeacherSubject() != null){
			tv_sub_name.setText(list.get(position).getTeacherSubject());
		}

		return convertView;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		list.clear();
		if (charText.length() == 0) {
			list.addAll(copyList);
		} else {
			for (TeacherModel vo : copyList) {
				if (vo.getTeacherName().toLowerCase(Locale.getDefault()).contains(charText)) {
					list.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}
}
