package com.edusunsoft.erp.orataro.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class AsynsTaskClass extends AsyncTask<String, Integer, String> implements ICancleAsynkTask {

    private ArrayList<PropertyVo> arraylist;
    private Context mContext;
    private String result = "";
    private boolean isShowLoader;
    private ResponseWebServices iResult;
    private boolean isInternetAvailable;
    private String methodName;
    private int i = 0;
    private boolean isInternet;
    private boolean extraParam;
    private long startTimeMillSceond, endTimeMillSceond;
    private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;
    private boolean isCalling = true;
    private boolean isCancleDialog = true;
    private boolean isPersentageDialog = false;
    private ProgressDialog progress;
    private String servermessage = " [{\"message\":\"Server is not responding, Please Try after some time.\",\"success\":0}]";

    private String Canclemessage = " [{\"message\":\"You are cancle this task.\",\"success\":0}]";

    private Handler handler;
    private Runnable runnable;
    private int prog;

    public AsynsTaskClass(Context mContext, ArrayList<PropertyVo> arraylist, boolean isShowLoader, ResponseWebServices iResult) {
        try {
            Log.d("getSingleCtoken", PreferenceData.getCTOKEN());
            Utility.GetSQLConnStr(mContext, arraylist);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("getSQLStrasync", arraylist.toString());
        this.mContext = mContext;
        this.arraylist = arraylist;
        this.iResult = iResult;
        this.isShowLoader = isShowLoader;
    }

    public AsynsTaskClass(Context mContext, ArrayList<PropertyVo> arraylist, boolean isShowLoader, ResponseWebServices iResult, boolean isPersentageDialog, boolean isCancleDialog) {
        try {
            Utility.GetSQLConnStr(mContext, arraylist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mContext = mContext;
        this.arraylist = arraylist;
        this.iResult = iResult;
        this.isShowLoader = isShowLoader;
        this.isPersentageDialog = isPersentageDialog;
        this.isCancleDialog = isCancleDialog;

        Log.d("getAsyncRequest", arraylist.toString());
    }

    public AsynsTaskClass(Context mContext, ArrayList<PropertyVo> arraylist, boolean isShowLoader, ResponseWebServices iResult, boolean extraParam, int i) {
        try {
            Utility.GetSQLConnStr(mContext, arraylist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mContext = mContext;
        this.arraylist = arraylist;
        this.iResult = iResult;
        this.isShowLoader = isShowLoader;
        this.i = i;
        this.extraParam = extraParam;
    }

    public AsynsTaskClass(Context mContext, ArrayList<PropertyVo> arraylist, boolean isShowLoader, ResponseWebServices iResult, boolean isCancleDialog) {
        try {
            Utility.GetSQLConnStr(mContext, arraylist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("getAsyncRequest", arraylist.toString());
        this.mContext = mContext;
        this.arraylist = arraylist;
        this.iResult = iResult;
        this.isShowLoader = isShowLoader;
        this.isCancleDialog = isCancleDialog;
    }

    public AsynsTaskClass(String withctoken, Context mContext, ArrayList<PropertyVo> arraylist, boolean isShowLoader, ResponseWebServices iResult, boolean isCancleDialog) {

        arraylist.add(new PropertyVo(ServiceResource.CTOKEN, withctoken));
        Log.d("getAsyncRequest", arraylist.toString());
        this.mContext = mContext;
        this.arraylist = arraylist;
        this.iResult = iResult;
        this.isShowLoader = isShowLoader;
        this.isCancleDialog = isCancleDialog;
    }

    @Override
    protected void onPreExecute() {

        if (isShowLoader) {

            if (isPersentageDialog) {

                try {

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {

                        progress = new ProgressDialog(mContext, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);

                    } else {

                        progress = new ProgressDialog(mContext);

                    }

                    progress.setMessage("Uploading");
                    progress.setMax(100);
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.setProgress(0);
                    progress.setCancelable(isCancleDialog);
                    Drawable customDrawable = mContext.getResources().getDrawable(R.drawable.custom_progressbar);
                    progress.setProgressDrawable(customDrawable);
                    progress.show();
                    updateDialog();
                    handler.sendEmptyMessage(0);

                } catch (Exception e) {

                    e.printStackTrace();

                }

            } else {

                try {

                    LoaderProgress = new LoaderProgress(mContext, this);
                    LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
                    LoaderProgress.setCancelable(isCancleDialog);
                    LoaderProgress.show();

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

        }

        startTimeMillSceond = System.currentTimeMillis();
        super.onPreExecute();

    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

        super.onProgressUpdate(progress);

        if (isPersentageDialog) {

            this.progress.setProgress(progress[0]);

        }
    }

    @Override
    protected String doInBackground(String... params) {
        if (Utility.isNetworkAvailable(mContext)) {
            if (isPersentageDialog) {
                if (isShowLoader) {
                    if (progress != null) {
                        publishProgress(0);
                    }
                }
            }
            isInternetAvailable = true;
            SoapParser primary = new SoapParser(arraylist,
                    params[0],//MethodName
                    params[1]);//URL
            methodName = params[0];
            result = primary.buildData(mContext).toString();
        } else {
            isInternetAvailable = false;
        }
        return result;
    }

    @Override
    protected void onPostExecute(String _result) {
        try {
            endTimeMillSceond = System.currentTimeMillis();

            if (ServiceResource.isShowLog) {

                Log.v("Time ", (endTimeMillSceond - startTimeMillSceond) + "");

            }

            if (isCalling) {

                if (result.equalsIgnoreCase("")) {

                    result = servermessage;
                    Log.d("ServerMessage", result);

                }

                if (isPersentageDialog) {

                    if (isShowLoader) {

                        if (progress != null) {

                            handler.removeCallbacks(runnable);
                            progress.dismiss();

                        }

                    }

                } else {

                    if (isShowLoader) {

                        if (LoaderProgress != null) {

                            LoaderProgress.dismiss();

                        }

                    }

                }

                if (!isInternetAvailable) {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                } else {
                    if (iResult != null) {
                        Log.d("getURLResponse", result);
                        this.iResult.response(result, methodName);
                    }
                }
            } else {
                result = Canclemessage;
                if (!isInternetAvailable) {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                } else {

                    if (iResult != null) {

                        this.iResult.response(result, methodName);

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPostExecute(_result);

    }

    @Override
    public void onCancleTask() {
        if (!this.isCancelled()) {
            this.cancel(true);
        }
    }

    @Override
    protected void onCancelled() {

        super.onCancelled();
    }

    public void updateDialog() {

        handler = new Handler() {

            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (prog >= 100) {
                } else {
                    prog++;
                    handler.sendEmptyMessageDelayed(0, 100);
                    publishProgress(prog);
                }
            }
        };

        prog = 0;

    }

}
