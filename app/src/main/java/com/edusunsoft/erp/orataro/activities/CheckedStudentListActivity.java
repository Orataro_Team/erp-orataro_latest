package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.CheckedStudentListAdapter;
import com.edusunsoft.erp.orataro.model.CheckedStudentModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckedStudentListActivity extends AppCompatActivity implements ResponseWebServices, View.OnClickListener {

    RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    private Context mContext = CheckedStudentListActivity.this;
    String FROMSTR = "", AssociationID = "", AssociationType = "", GradeID = "", DivisionID = "";
    ArrayList<CheckedStudentModel> StudentList = new ArrayList<>();
    CheckedStudentModel studentModel;
    CheckedStudentListAdapter studentListAdapter;
    LinearLayout lyl_checked_student_header;
    private ImageView imgLeftheader;
    public TextView header_text, txt_no_data_found;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checked_student_list_activity);

        Initialization();

    }

    private void Initialization() {

        /*getAssociationID and AssociationType*/

        Intent i = getIntent();

        if (i != null) {

            AssociationID = i.getStringExtra("AssociationID");
            AssociationType = i.getStringExtra("AssociationType");
            FROMSTR = i.getStringExtra("FROMSSTR");
            GradeID = i.getStringExtra("GradeID");
            DivisionID = i.getStringExtra("DivisionID");

        }

        /*END*/

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setTitle("View And Checked List");
//        setSupportActionBar(toolbar);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//
//        });

        imgLeftheader = (ImageView) findViewById(R.id.img_back);
        imgLeftheader.setOnClickListener(this);

        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText(getResources().getString(R.string.viewandcheckedlist));

        txt_no_data_found = (TextView) findViewById(R.id.txt_no_data_found);

        lyl_checked_student_header = (LinearLayout) findViewById(R.id.lyl_checked_student_header);
        lyl_checked_student_header.setVisibility(View.VISIBLE);
        recyclerView = (RecyclerView) findViewById(R.id.student_list);
        mLayoutManager = new LinearLayoutManager(this);
        // use a linear layout manager
        recyclerView.setLayoutManager(mLayoutManager);


        if (Utility.isNetworkAvailable(mContext)) {

            try {
                CheckedStudentList(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        super.onOptionsItemSelected(item);
//        if (item.getItemId() == R.id.approved) {
//            FilterStudentList.clear();
//            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
//
//                for (int i = 0; i < StudentList.size(); i++) {
//                    if (StudentList.get(i).isApprove() == true) {
//                        FilterStudentList.add(StudentList.get(i));
//                    }
//                }
//                if (FilterStudentList.size() > 0) {
//                    txt_no_data_found.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.VISIBLE);
//                    studentListAdapter = new CheckedStudentListAdapter(mContext, FilterStudentList);
//                    // set the adapter object to the Recyclerview
//                    recyclerView.setAdapter(studentListAdapter);
//                } else {
//                    txt_no_data_found.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.GONE);
//                }
//            }
//
//        } else if (item.getItemId() == R.id.unapproved) {
//
//            FilterStudentList.clear();
//            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
//
//                for (int i = 0; i < StudentList.size(); i++) {
//                    if (StudentList.get(i).isApprove() == false) {
//                        FilterStudentList.add(StudentList.get(i));
//                    }
//                }
//
//                studentListAdapter = new CheckedStudentListAdapter(mContext, FilterStudentList);
//                // set the adapter object to the Recyclerview
//                recyclerView.setAdapter(studentListAdapter);
//                if (FilterStudentList.size() > 0) {
//                    txt_no_data_found.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.VISIBLE);
//                    studentListAdapter = new CheckedStudentListAdapter(mContext, FilterStudentList);
//                    // set the adapter object to the Recyclerview
//                    recyclerView.setAdapter(studentListAdapter);
//                } else {
//                    txt_no_data_found.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.GONE);
//                }
//
//            }
//        } else if (item.getItemId() == R.id.all) {
//            if (StudentList.size() > 0 || !StudentList.isEmpty()) {
//
//                studentListAdapter = new CheckedStudentListAdapter(mContext, StudentList);
//                // set the adapter object to the Recyclerview
//                recyclerView.setAdapter(studentListAdapter);
//            } else {
//                txt_no_data_found.setVisibility(View.VISIBLE);
//                recyclerView.setVisibility(View.GONE);
//            }
//
//        }
//        return true;
//
//    }

    private void CheckedStudentList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ASSID, AssociationID));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, GradeID));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, DivisionID));
        arrayList.add(new PropertyVo(ServiceResource.ASSTYPE, AssociationType));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        Log.d("getRequestchecked", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.CHECKED_STUDENT_METHODNAME, ServiceResource.CHECKED_STUDENT_URL);

    }

    @Override
    public void response(String result, String methodName) {

        Log.d("getResultchecked", result);
        JSONArray jsonObj;

        try {

            StudentList.clear();
            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {

                JSONObject studentObj = jsonObj.getJSONObject(i);
                studentModel = new CheckedStudentModel();
                studentModel.setContactNo1(studentObj.getString("ContactNo1"));
                studentModel.setFullName(studentObj.getString("FullName"));
                studentModel.setReadApproveID(studentObj.getString("ReadApproveID"));
                studentModel.setApprove(studentObj.getBoolean("IsApprove"));
                studentModel.setRead(studentObj.getBoolean("IsRead"));
                studentModel.setRollNo(studentObj.getString("RollNo"));

                StudentList.add(studentModel);
                Log.d("getstudentlist", StudentList.toString());

            }

            if (StudentList.size() > 0) {
                // create an Object for Adapter
                studentListAdapter = new CheckedStudentListAdapter(mContext, StudentList);
                // set the adapter object to the Recyclerview
                recyclerView.setAdapter(studentListAdapter);
            } else {
                txt_no_data_found.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

//            case R.id.img_back:
//
//                finish();
//
//                break;

        }

    }

}
