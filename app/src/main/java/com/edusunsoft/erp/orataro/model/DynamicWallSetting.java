package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class DynamicWallSetting implements Serializable {

    private String IsAllowPeopleToLikeThisWall;
    private String IsAllowPeoplePostComment;
    private String IsAllowPeopleToShareComment;
    private String IsAllowPeopleToLikeAndDislikeComment;
    private String IsAllowPeopleToPostStatus;
    private String IsAllowPeopleToCreatePoll;
    private String IsAllowPeopleToParticipateInPoll;
    private String IsAllowPeopleToInviteOtherPeople;
    private String IsAllowPeopleToTagOnPost;
    private String IsAllowPeopleToUploadAlbum;
    private String IsAllowPeopleToPutGeoLocationOnPost;
    private String IsAllowPeopleToPostDocument;
    private String IsAllowPeopleToPostVideos;
    private String WallName;
    private String WallImage;
    private String WallID;
    private String IsAutoApprovePost;
    private String IsAutoApprovePostStatus;
    private String IsAutoApproveAlbume;
    private String IsAutoApproveVideos;
    private String IsAutoApproveDocument;
    private String IsAutoApprovePoll;
    private String IsAdmin;
    public String IsAllowLikeDislike, IsAllowPostAlbum, IsAllowPostComment, IsAllowPostFile, IsAllowPostPhoto, IsAllowPostStatus, IsAllowPostVideo, IsAllowSharePost, IsAllowPeopleToPostCommentOnPost;


    public boolean getIsAllowLikeDislike() {
        return Strtobool(IsAllowLikeDislike);
    }

    public void setIsAllowLikeDislike(String isAllowLikeDislike) {
        IsAllowLikeDislike = isAllowLikeDislike;
    }

    public String getIsAllowPostAlbum() {
        return IsAllowPostAlbum;
    }

    public void setIsAllowPostAlbum(String isAllowPostAlbum) {
        IsAllowPostAlbum = isAllowPostAlbum;
    }

    public boolean getIsAllowPostComment() {
        return Strtobool(IsAllowPostComment);
    }

    public void setIsAllowPostComment(String isAllowPostComment) {
        IsAllowPostComment = isAllowPostComment;
    }

    public boolean getIsAllowPostFile() {
        return Strtobool(IsAllowPostFile);
    }

    public void setIsAllowPostFile(String isAllowPostFile) {
        IsAllowPostFile = isAllowPostFile;
    }

    public boolean getIsAllowPostPhoto() {
        return Strtobool(IsAllowPostPhoto);
    }

    public void setIsAllowPostPhoto(String isAllowPostPhoto) {
        IsAllowPostPhoto = isAllowPostPhoto;
    }

    public boolean getIsAllowPostStatus() {
        return Strtobool(IsAllowPostStatus);
    }

    public void setIsAllowPostStatus(String isAllowPostStatus) {
        IsAllowPostStatus = isAllowPostStatus;
    }

    public boolean getIsAllowPostVideo() {
        return Strtobool(IsAllowPostVideo);
    }

    public void setIsAllowPostVideo(String isAllowPostVideo) {
        IsAllowPostVideo = isAllowPostVideo;
    }

    public boolean getIsAllowSharePost() {
        return Strtobool(IsAllowSharePost);
    }

    public void setIsAllowSharePost(String isAllowSharePost) {
        IsAllowSharePost = isAllowSharePost;
    }

    public boolean getIsAllowPeopleToPostCommentOnPost() {
        return Strtobool(IsAllowPeopleToPostCommentOnPost);
    }

    public void setIsAllowPeopleToPostCommentOnPost(String isAllowPeopleToPostCommentOnPost) {
        IsAllowPeopleToPostCommentOnPost = isAllowPeopleToPostCommentOnPost;
    }

    public boolean getIsAllowPeopleToLikeThisWall() {
        return Strtobool(IsAllowPeopleToLikeThisWall);
    }

    public void setIsAllowPeopleToLikeThisWall(String isAllowPeopleToLikeThisWall) {
        IsAllowPeopleToLikeThisWall = isAllowPeopleToLikeThisWall;
    }

    public boolean getIsAllowPeoplePostComment() {
        return Strtobool(IsAllowPeoplePostComment);
    }

    public void setIsAllowPeoplePostComment(String isAllowPeoplePostComment) {
        IsAllowPeoplePostComment = isAllowPeoplePostComment;
    }

    public boolean getIsAllowPeopleToShareComment() {
        return Strtobool(IsAllowPeopleToShareComment);
    }

    public void setIsAllowPeopleToShareComment(String isAllowPeopleToShareComment) {
        IsAllowPeopleToShareComment = isAllowPeopleToShareComment;
    }

    public boolean getIsAllowPeopleToLikeAndDislikeComment() {
        return Strtobool(IsAllowPeopleToLikeAndDislikeComment);
    }

    public void setIsAllowPeopleToLikeAndDislikeComment(
            String isAllowPeopleToLikeAndDislikeComment) {
        IsAllowPeopleToLikeAndDislikeComment = isAllowPeopleToLikeAndDislikeComment;
    }

    public boolean getIsAllowPeopleToPostStatus() {
        return Strtobool(IsAllowPeopleToPostStatus);
    }

    public void setIsAllowPeopleToPostStatus(String isAllowPeopleToPostStatus) {
        IsAllowPeopleToPostStatus = isAllowPeopleToPostStatus;
    }

    public boolean getIsAllowPeopleToCreatePoll() {
        return Strtobool(IsAllowPeopleToCreatePoll);
    }

    public void setIsAllowPeopleToCreatePoll(String isAllowPeopleToCreatePoll) {
        IsAllowPeopleToCreatePoll = isAllowPeopleToCreatePoll;
    }

    public boolean getIsAllowPeopleToParticipateInPoll() {
        return Strtobool(IsAllowPeopleToParticipateInPoll);
    }

    public void setIsAllowPeopleToParticipateInPoll(
            String isAllowPeopleToParticipateInPoll) {
        IsAllowPeopleToParticipateInPoll = isAllowPeopleToParticipateInPoll;
    }

    public boolean getIsAllowPeopleToInviteOtherPeople() {
        return Strtobool(IsAllowPeopleToInviteOtherPeople);
    }

    public void setIsAllowPeopleToInviteOtherPeople(
            String isAllowPeopleToInviteOtherPeople) {
        IsAllowPeopleToInviteOtherPeople = isAllowPeopleToInviteOtherPeople;
    }

    public boolean getIsAllowPeopleToTagOnPost() {
        return Strtobool(IsAllowPeopleToTagOnPost);
    }

    public void setIsAllowPeopleToTagOnPost(String isAllowPeopleToTagOnPost) {
        IsAllowPeopleToTagOnPost = isAllowPeopleToTagOnPost;
    }

    public boolean getIsAllowPeopleToUploadAlbum() {
        return Strtobool(IsAllowPeopleToUploadAlbum);
    }

    public void setIsAllowPeopleToUploadAlbum(String isAllowPeopleToUploadAlbum) {
        IsAllowPeopleToUploadAlbum = isAllowPeopleToUploadAlbum;
    }

    public boolean getIsAllowPeopleToPutGeoLocationOnPost() {
        return Strtobool(IsAllowPeopleToPutGeoLocationOnPost);
    }

    public void setIsAllowPeopleToPutGeoLocationOnPost(
            String isAllowPeopleToPutGeoLocationOnPost) {
        IsAllowPeopleToPutGeoLocationOnPost = isAllowPeopleToPutGeoLocationOnPost;
    }

    public boolean getIsAllowPeopleToPostDocument() {
        return Strtobool(IsAllowPeopleToPostDocument);
    }

    public void setIsAllowPeopleToPostDocument(String isAllowPeopleToPostDocument) {
        IsAllowPeopleToPostDocument = isAllowPeopleToPostDocument;
    }

    public boolean getIsAllowPeopleToPostVideos() {
        return Strtobool(IsAllowPeopleToPostVideos);
    }

    public void setIsAllowPeopleToPostVideos(String isAllowPeopleToPostVideos) {
        IsAllowPeopleToPostVideos = isAllowPeopleToPostVideos;
    }

    public boolean getWallName() {
        return Strtobool(WallName);
    }

    public void setWallName(String wallName) {
        WallName = wallName;
    }

    public boolean getWallImage() {
        return Strtobool(WallImage);
    }

    public void setWallImage(String wallImage) {
        WallImage = wallImage;
    }

    public boolean getWallID() {
        return Strtobool(WallID);
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public boolean getIsAutoApprovePost() {
        return Strtobool(IsAutoApprovePost);
    }

    public void setIsAutoApprovePost(String isAutoApprovePost) {
        IsAutoApprovePost = isAutoApprovePost;
    }

    public boolean getIsAutoApprovePostStatus() {
        return Strtobool(IsAutoApprovePostStatus);
    }

    public void setIsAutoApprovePostStatus(String isAutoApprovePostStatus) {
        IsAutoApprovePostStatus = isAutoApprovePostStatus;
    }

    public boolean getIsAutoApproveAlbume() {
        return Strtobool(IsAutoApproveAlbume);
    }

    public void setIsAutoApproveAlbume(String isAutoApproveAlbume) {
        IsAutoApproveAlbume = isAutoApproveAlbume;
    }

    public boolean getIsAutoApproveVideos() {
        return Strtobool(IsAutoApproveVideos);
    }

    public void setIsAutoApproveVideos(String isAutoApproveVideos) {
        IsAutoApproveVideos = isAutoApproveVideos;
    }

    public boolean getIsAutoApproveDocument() {
        return Strtobool(IsAutoApproveDocument);
    }

    public void setIsAutoApproveDocument(String isAutoApproveDocument) {
        IsAutoApproveDocument = isAutoApproveDocument;
    }

    public boolean getIsAutoApprovePoll() {
        return Strtobool(IsAutoApprovePoll);
    }

    public void setIsAutoApprovePoll(String isAutoApprovePoll) {
        IsAutoApprovePoll = isAutoApprovePoll;
    }

    public boolean getIsAdmin() {
        if (IsAdmin != null && IsAdmin.equalsIgnoreCase("1")) {
            return true;
        } else {
            return false;
        }
    }

    public void setIsAdmin(String isAdmin) {
        IsAdmin = isAdmin;
    }

    public boolean Strtobool(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("0")) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "DynamicWallSetting{" +
                "IsAllowPeopleToLikeThisWall='" + IsAllowPeopleToLikeThisWall + '\'' +
                ", IsAllowPeoplePostComment='" + IsAllowPeoplePostComment + '\'' +
                ", IsAllowPeopleToShareComment='" + IsAllowPeopleToShareComment + '\'' +
                ", IsAllowPeopleToLikeAndDislikeComment='" + IsAllowPeopleToLikeAndDislikeComment + '\'' +
                ", IsAllowPeopleToPostStatus='" + IsAllowPeopleToPostStatus + '\'' +
                ", IsAllowPeopleToCreatePoll='" + IsAllowPeopleToCreatePoll + '\'' +
                ", IsAllowPeopleToParticipateInPoll='" + IsAllowPeopleToParticipateInPoll + '\'' +
                ", IsAllowPeopleToInviteOtherPeople='" + IsAllowPeopleToInviteOtherPeople + '\'' +
                ", IsAllowPeopleToTagOnPost='" + IsAllowPeopleToTagOnPost + '\'' +
                ", IsAllowPeopleToUploadAlbum='" + IsAllowPeopleToUploadAlbum + '\'' +
                ", IsAllowPeopleToPutGeoLocationOnPost='" + IsAllowPeopleToPutGeoLocationOnPost + '\'' +
                ", IsAllowPeopleToPostDocument='" + IsAllowPeopleToPostDocument + '\'' +
                ", IsAllowPeopleToPostVideos='" + IsAllowPeopleToPostVideos + '\'' +
                ", WallName='" + WallName + '\'' +
                ", WallImage='" + WallImage + '\'' +
                ", WallID='" + WallID + '\'' +
                ", IsAutoApprovePost='" + IsAutoApprovePost + '\'' +
                ", IsAutoApprovePostStatus='" + IsAutoApprovePostStatus + '\'' +
                ", IsAutoApproveAlbume='" + IsAutoApproveAlbume + '\'' +
                ", IsAutoApproveVideos='" + IsAutoApproveVideos + '\'' +
                ", IsAutoApproveDocument='" + IsAutoApproveDocument + '\'' +
                ", IsAutoApprovePoll='" + IsAutoApprovePoll + '\'' +
                ", IsAdmin='" + IsAdmin + '\'' +
                '}';
    }
}
