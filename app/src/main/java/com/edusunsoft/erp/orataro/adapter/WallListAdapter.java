package com.edusunsoft.erp.orataro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.FaceDetector;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.model.WallListVo;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class WallListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<WallListVo> list = new ArrayList<>();
    private String from;
    private boolean isListVisible;
    private int[] colors2 = new int[]{Color.parseColor("#F2F2F2"), Color.parseColor("#F2F2F2")};

    public WallListAdapter(Context mContext, ArrayList<WallListVo> list, String from, boolean isListVisible) {
        this.mContext = mContext;
        this.list = list;
        this.from = from;
        this.isListVisible = isListVisible;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.menushell, parent, false);
        LinearLayout layoutparent = (LinearLayout) convertView.findViewById(R.id.layoutparent);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_menu);
        ImageView imgSrc = (ImageView) convertView.findViewById(R.id.menu_item);
        int colorPos = position % colors2.length;
        convertView.setBackgroundColor(colors2[colorPos]);

        imgSrc.setColorFilter(Color.parseColor("#27305B"));
        tvTitle.setTextColor(Color.BLACK);

        tvTitle.setText(list.get(position).getWallName());
        imgSrc.setImageResource(list.get(position).getIco());

        if (list.get(position).getDynamicWallList() != null && list.get(position).getDynamicWallList().size() > 0) {

            for (int i = 0; i < list.get(position).getDynamicWallList().size(); i++) {

                View v = inflater.inflate(R.layout.menushell, null);
                final TextView txtStanderd = (TextView) v.findViewById(R.id.tv_menu);
                ImageView imgmenu = (ImageView) v.findViewById(R.id.menu_item);
                txtStanderd.setTag("" + i);
                imgmenu.setVisibility(View.INVISIBLE);
                v.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                txtStanderd.setText(list.get(position).getDynamicWallList().get(i).getWallName());
                txtStanderd.setTextColor(mContext.getResources().getColor(R.color.black));
                txtStanderd.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        Intent intent;
                        int tempPos = Integer.valueOf((String) txtStanderd.getTag());
                        String title = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                        Log.d("walllisttitle", title);
                        new UserSharedPrefrence(mContext).setCURRENTWALLID(list.get(position).getDynamicWallList().get(tempPos).getWallID());

                        if (position == 5) {

                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            ServiceResource.SELECTED_DYNAMIC_WALL_ID = list.get(position).getDynamicWallList().get(tempPos).getWallID();
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
                            intent.putExtra("isGotoWall", true);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        } else if (position == 1) {

                            ServiceResource.FROM_SELECTED_WALL = "";
                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                            intent.putExtra("isGotoWall", false);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        } else if (position == 2) {

                            try {
                                Log.d("getWallName", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                                intent = new Intent(mContext, WallActivity.class);
                                intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                                intent.putExtra("isGotoWall", false);
                                intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                                ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                                mContext.startActivity(intent);
                                ((Activity) mContext).finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (position == 3) {

                            ServiceResource.SELECTED_DYNAMIC_WALL_ID = list.get(position).getDynamicWallList().get(tempPos).getWallID();
                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            Log.d("getWallName", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
                            intent.putExtra("isGotoWall", true);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        } else if (position == 4) {

                            ServiceResource.SELECTED_DYNAMIC_WALL_ID = list.get(position).getDynamicWallList().get(tempPos).getWallID();
                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.DIVISIONWALL);
                            intent.putExtra("isGotoWall", true);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        } else if (position == 6) {

                            ServiceResource.SELECTED_DYNAMIC_WALL_ID = list.get(position).getDynamicWallList().get(tempPos).getWallID();
                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.GROUPWALL);
                            intent.putExtra("isGotoWall", true);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        } else if (position == 7) {

                            ServiceResource.SELECTED_DYNAMIC_WALL_ID = list.get(position).getDynamicWallList().get(tempPos).getWallID();
                            ServiceResource.SELECTED_WALL_TITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            intent = new Intent(mContext, WallActivity.class);
                            intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
                            intent.putExtra("isGotoWall", true);
                            intent.putExtra("title", list.get(position).getDynamicWallList().get(tempPos).getWallName());
                            ServiceResource.WALLTITLE = list.get(position).getDynamicWallList().get(tempPos).getWallName();
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();

                        }

                    }

                });

                layoutparent.addView(v);

            }

        }

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
                    i.putExtra("position", mContext.getResources().getString(R.string.Wall));
                    mContext.startActivity(i);
                } else if (position == 1) {
                    ServiceResource.FROM_SELECTED_WALL = "";
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
                    ServiceResource.WALLTITLE = "My Wall";
                    intent.putExtra("isGotoWall", false);
                    mContext.startActivity(intent);
//                    ((Activity) mContext).finish();
                } else if (position == 2) {
                    ServiceResource.FROM_SELECTED_WALL = "";
                    Intent intent = new Intent(mContext, WallActivity.class);
                    intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
                    ServiceResource.WALLTITLE = "Institute wall";
                    intent.putExtra("isGotoWall", true);
                    mContext.startActivity(intent);
//                    ((Activity) mContext).finish();
                }

            }

        });

        return convertView;

    }

}
