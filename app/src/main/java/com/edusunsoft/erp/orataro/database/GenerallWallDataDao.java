package com.edusunsoft.erp.orataro.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface GenerallWallDataDao {

    @Insert
    void insertGenerallWall(GenerallWallData generallWallData);

    @Delete
    void deleteGenerallWall(GenerallWallData generallWallData);

    @Query("SELECT * from GeneralWall ORDER BY TempDate DESC")
    List<GenerallWallData> getAllGenerallWallData();

    @Query("SELECT * from GeneralWall WHERE WallTypeTerm = :wallTypeTerm ORDER BY TempDate DESC")
    List<GenerallWallData> getMyWallData(String wallTypeTerm);

    @Query("SELECT * from GeneralWall WHERE MemberID = :memberid ORDER BY TempDate DESC")
    List<GenerallWallData> getPersonelWallData(String memberid);

    @Query("SELECT * from GeneralWall WHERE WallTypeTerm = :wallTypeTerm and WallID = :WallID ORDER BY TempDate DESC")
    List<GenerallWallData> getDynamicWallData(String wallTypeTerm, String WallID);

    @Query("SELECT * FROM GeneralWall WHERE WallID = :WallID")
    List<GenerallWallData> getGenerallWallDataById(String WallID);

    @Query("DELETE  FROM GeneralWall")
    int deleteGeneralWallData();

    @Query("update GeneralWall set RowNo=:RowNo,PostCommentID=:PostCommentID,WallID=:WallID,MemberID=:MemberID,PostCommentTypesTerm=:PostCommentTypesTerm,PostCommentNote=:PostCommentNote,TotalLikes=:TotalLikes,TotalComments=:TotalComments,TotalDislike=:TotalDislike,DateOfPost=:DateOfPost," +
            "AssociationID=:AssociationID,AssociationType=:AssociationType,FullName=:FullName,IsDisLike=:IsDisLike," +
            "IsLike=:IsLike,ProfilePicture=:ProfilePicture,Photo=:Photo,TempDate=:TempDate,PostDate=:PostDate,PostCount=:PostCount," +
            "PostCount=:PostCount,PostUrls=:PostUrls,WallTypeTerm=:WallTypeTerm,FileType=:FileType," +
            "PostedOn=:PostedOn,FileMimeType=:FileMimeType,SendToMemberID=:SendToMemberID,PhotoCount=:PhotoCount," +
            "PhotoUrls=:PhotoUrls,PostName=:PostName,AlbumPhotoID=:AlbumPhotoID,isUserComment=:isUserComment,isUserShare=:isUserShare," +
            "isUserLike=:isUserLike,isUserDisLike=:isUserDisLike,IsAllowLikeDislike=:IsAllowLikeDislike,IsAllowPostComment=:IsAllowPostComment," +
            "PostComment=:PostComment,IsAllowSharePost=:IsAllowSharePost,IsAllowPeopleToPostCommentOnPostWall=:IsAllowPeopleToPostCommentOnPostWall," +
            "IsAllowPeopleToShareYourPost=:IsAllowPeopleToShareYourPost,IsAllowPeopleToLikeOrDislikeOnYourPost=:IsAllowPeopleToLikeOrDislikeOnYourPost," +
            "IsAllowPeopleToPostMessageOnYourWall=:IsAllowPeopleToPostMessageOnYourWall,IsAllowPeopleToLikeAndDislikeCommentWall=:IsAllowPeopleToLikeAndDislikeCommentWall," +
            "IsAllowPeopleToShareCommentWall=:IsAllowPeopleToShareCommentWall,IsAllowPeoplePostCommentWall=:IsAllowPeoplePostCommentWall" +
            " where PostCommentID=:PostCommentID")
    void updateGenerallWallData(int RowNo, String PostCommentID, String WallID, String MemberID, String PostCommentTypesTerm, String PostCommentNote,
                                String TotalLikes, String TotalComments, String TotalDislike, String DateOfPost,
                                String AssociationID, String AssociationType, String FullName, String IsDisLike, String IsLike,
                                String ProfilePicture, String Photo, String TempDate, String PostDate,
                                String PostCount, String PostUrls, String WallTypeTerm, String FileType, String PostedOn,
                                String FileMimeType, String SendToMemberID, String PhotoCount, String PhotoUrls, String PostName,
                                String AlbumPhotoID, boolean isUserComment, boolean isUserShare, boolean isUserLike
            , boolean isUserDisLike, String IsAllowLikeDislike, String IsAllowPostComment, String PostComment, String IsAllowSharePost,
                                String IsAllowPeopleToPostCommentOnPostWall, String IsAllowPeopleToShareYourPost
            , String IsAllowPeopleToLikeOrDislikeOnYourPost, String IsAllowPeopleToPostMessageOnYourWall, String IsAllowPeopleToLikeAndDislikeCommentWall, String IsAllowPeopleToShareCommentWall
            , String IsAllowPeoplePostCommentWall);


//    @Query("UPDATE GeneralWall SET room_name =:roomName WHERE room_id =:roomId")
//    void updateRoomData(String roomName, int roomId);

    @Query("delete from GeneralWall where PostCommentID=:postcommentID")
    void deleteWallItemByPostCommentID(String postcommentID);

    @Query("delete from GeneralWall where AssociationID=:assID")
    void deleteWallItemByAssID(String assID);

}
