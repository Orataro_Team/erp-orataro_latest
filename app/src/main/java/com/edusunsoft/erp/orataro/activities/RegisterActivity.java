package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.Utilities.SharedPreferenceUtil;
import com.edusunsoft.erp.orataro.adapter.CountriesListAdapter;
import com.edusunsoft.erp.orataro.model.CountryModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.edusunsoft.erp.orataro.util.Utility.CTOKENFROMSTR;

public class RegisterActivity extends Activity implements OnClickListener, ResponseWebServices, TextWatcher {

    private static int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;
    private String Message = "";
    private Button btn_continue;
    private FrameLayout fl_back;
    private Context mContext;
    private EditText evMobileNumber;
    private LinearLayout ll_edt_gr, ll_opt;
    private ImageView img_gr_mob;
    private TextView txt_title, txtDeviceId;
    private LinearLayout ll_circle_img, ll_edt_mobile;
    private boolean isShowPass = false, isShowConPass = false;
    private Bitmap myBitmap;
    private int idCount = 0;
    public static EditText edt_optnumber;
    private String regId = "";
    private boolean isLogin = false;
    private boolean isMoreChild;
    private String userNameStr = "";
    public Button btn_verify_otp;

    public String OTP_ID = "", CountryCode = "", AythenticationCode = "";

    //firebase auth object
    private FirebaseAuth mAuth;
    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;

    Spinner spn_country;
    ArrayList<CountryModel> CountryList = new ArrayList<>();
    String isCodeSentManually = "";
    TextView txt_code_manually, txt_timer;
    public int counter;
    public static ArrayList<LoginUserModel> NewLoginUserList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = RegisterActivity.this;

        //initializing objects
        mAuth = FirebaseAuth.getInstance();

        txt_title = (TextView) findViewById(R.id.txt_title);
        ll_circle_img = (LinearLayout) findViewById(R.id.ll_circle_img);
        ll_edt_mobile = (LinearLayout) findViewById(R.id.ll_edt_mobile);
        ll_opt = (LinearLayout) findViewById(R.id.ll_opt);
        txt_code_manually = (TextView) findViewById(R.id.txt_code_manually);
        txt_timer = (TextView) findViewById(R.id.txt_timer);
        evMobileNumber = (EditText) findViewById(R.id.edt_mob_gr_no);
        img_gr_mob = (ImageView) findViewById(R.id.img_gr_mob);
        btn_continue = (Button) findViewById(R.id.btn_continue);
        btn_verify_otp = (Button) findViewById(R.id.btn_verify_otp);
        txtDeviceId = findViewById(R.id.txtDeviceId);
        ll_circle_img.setVisibility(View.GONE);
        btn_continue.setText(getResources().getString(R.string.verify));
        spn_country = (Spinner) findViewById(R.id.spn_country);

        /*commented By Krishna : Get CounteCode*/

        if (Utility.isNetworkAvailable(mContext)) {
            getContryCode();
        } else {
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
        /*END*/

        spn_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryCode = CountryList.get(position).getCountrCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

        ll_circle_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                idCount++;
                if (idCount > 3) {

                    txtDeviceId.setText(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
                    txtDeviceId.setVisibility(View.VISIBLE);

                }

            }

        });

        edt_optnumber = (EditText) findViewById(R.id.edt_optnumber);

        fl_back = (FrameLayout) findViewById(R.id.fl_back);
        fl_back.setVisibility(View.GONE);
        fl_back.setOnClickListener(this);

        btn_continue.setOnClickListener(this);
        btn_verify_otp.setOnClickListener(this);
        txt_code_manually.setOnClickListener(this);
        SharedPreferenceUtil.clear();


        // Get token
        // [START retrieve_current_token]

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (!task.isSuccessful()) {

                            Log.w("getInstanceId failed", task.getException());
                            return;

                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = "FirebaseToken" + token;
                        Log.d("TAG", msg);
                        Log.d("firebasetoken", token);

                    }

                });

        // [END retrieve_current_token]


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));

        }

        // If a notification message is tapped, any data accompanying the notification
        // message is available in the intent extras. In this sample the launcher
        // intent is fired when the notification is tapped, so any accompanying data would
        // be handled here. If you want a different intent fired, set the click_action
        // field of the notification message to the desired intent. The launcher intent
        // is used when no click_action is specified.
        //

        // Handle possible data accompanying notification message.

        // [START handle_data_extras]

        if (getIntent().getExtras() != null) {

            for (String key : getIntent().getExtras().keySet()) {

                Object value = getIntent().getExtras().get(key);
                Log.d("KeyValue", "key " + key + "Value" + value);
//                Log.d(TAG, "Key: " + key + " Value: " + value);


            }

        }


    }

    public void getContryCode() {

        CTOKENFROMSTR = "true";
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, ServiceResource.COUNTRY_ID));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, ServiceResource.COUNTRY_ID));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY, ServiceResource.COUNTRY_CATEGORY));
        new AsynsTaskClass("", mContext, arrayList, true, this, false).execute(ServiceResource.LOGIN_GETPROJECTTYPE, ServiceResource.LOGIN_URL);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fl_back:
                Intent intent = new Intent(mContext, RegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                break;


            case R.id.btn_verify_otp:

                if (Utility.isNetworkAvailable(mContext)) {

                    if (edt_optnumber.getText().toString().trim().length() == 0) {

                        Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseEnterotp), "Error");

                    } else {

                        if (isCodeSentManually.equalsIgnoreCase("Yes")) {
                            verifyOtp();
                        } else {
                            verifyVerificationCode(edt_optnumber.getText().toString());
                        }

//                        verifyVerificationCode(edt_optnumber.getText().toString());
//                        verifyOtp();

                    }

                } else {

                    Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                }

                break;

            case R.id.btn_continue:

                if (ll_edt_mobile.getVisibility() == View.VISIBLE) {

                    if (Utility.isNetworkAvailable(mContext)) {
                        if (evMobileNumber.getText().toString().length() == 0) {
                            Utility.showToast(getString(R.string.Pleasemobilenumber), mContext);
                        } else {
                            registerAsyns();
                        }
                    } else {
                        Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                    }

                } else {
                }

                break;
            case R.id.txt_code_manually:
                SendOtpCodeManually();
                break;

            default:

                break;


        }

    }

    private void SendOtpCodeManually() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, evMobileNumber.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.DEVICEIDENTITY, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, "android"));
        Log.d("arraylist123", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP_MANUALLY, ServiceResource.REGISTATION_URL);

    }

    private void verifyVerificationCode(String otp) {

        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);

        //signing the user
        signInWithPhoneAuthCredential(credential);

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

//                            Utility.toast(mContext, "Authnticaino Succssfull");
                            //verification successful we will start the profile activity
                            verifyOtp();

                        } else {

                            //verification unsuccessful.. display an error message
                            String message = getResources().getString(R.string.somethingwrongmessage);

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = getResources().getString(R.string.invalidcodeentered);
                            } else if (task.getException() instanceof FirebaseException) {
                                message = getResources().getString(R.string.firebaseexception);
                            } else if (task.getException() instanceof FirebaseAuthInvalidUserException) {
                                message = getResources().getString(R.string.user_not_found);
                            }

                            Utility.ShowSnackbar(RegisterActivity.this, findViewById(R.id.containar), message);

                        }

                    }

                });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        return super.onKeyDown(keyCode, event);

    }

    public void registerAsyns() {

        CTOKENFROMSTR = "true";
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, evMobileNumber.getText().toString()));
//        arrayList.add(new PropertyVo(ServiceResource.DEVICEIDENTITY, DeviceHardWareId.getDviceHardWarId(mContext)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICEIDENTITY, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, "android"));
        Log.d("arraylist123", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP, ServiceResource.REGISTATION_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP.equalsIgnoreCase(methodName)) {

            Log.d("responselogin", result);

            JSONArray jsonObj;

            try {

                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    Message = innerObj.getString(ServiceResource.MESSAGE);
                    OTP_ID = innerObj.getString(ServiceResource.OTP_ID);
                    if (Message.contains("No Data Found")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Mobile Number Not Available For Registration")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Another Person is already Registered on this Device")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Another Person is already Registered on this Device")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("OTP Send On Your Mobile Number")) {

                        /*Commented By Krishna : 05-08-2019 - send Verification Code(OTP)  Through fireBase*/

                        try {

                            sendVerificationCode(evMobileNumber.getText().toString());
                            // commented By Krishna : 19-12-19 -  Timer to Display Send OTP Manually after 45 seconds
                            new CountDownTimer(30000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    txt_code_manually.setVisibility(View.GONE);
                                    counter++;
                                }

                                public void onFinish() {
                                    txt_code_manually.setVisibility(View.VISIBLE);
                                }

                            }.start();
                            /*END*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        /*END*/

                    } else {

                        Utility.showAlertDialog(mContext, Message, "Error");

                    }

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

        } else if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {
            parseCountryCode(result);
        } else if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_SET_OTP_MANUALLY.equalsIgnoreCase(methodName)) {
            Log.d("responselogin", result);

            JSONArray jsonObj;

            try {

                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    Message = innerObj.getString(ServiceResource.MESSAGE);
//                    OTP_ID = innerObj.getString(ServiceResource.OTP_ID);
                    if (Message.contains("No Data Found")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Mobile Number Not Available For Registration")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Another Person is already Registered on this Device")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("Another Person is already Registered on this Device")) {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    } else if (Message.contains("OTP Send On Your Mobile Number")) {
                        Utility.showToast(Message, mContext);
                        isCodeSentManually = "Yes";
                        new CountDownTimer(45000, 1000) {
                            public void onTick(long millisUntilFinished) {

                                txt_timer.setVisibility(View.VISIBLE);
                                txt_timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                                txt_code_manually.setVisibility(View.GONE);
//                                txt_timer.setText(String.valueOf(counter));
                                counter++;
                            }

                            public void onFinish() {
                                txt_code_manually.setVisibility(View.VISIBLE);
                                txt_code_manually.setText("Resend Code Manually");
                                txt_timer.setVisibility(View.GONE);
//                                txt_timer.setText("FINISH!!");
                            }
                        }.start();

                    } else {
                        Utility.showAlertDialog(mContext, Message, "Error");
                    }

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }


        } else if (ServiceResource.USER_SELECTION.equalsIgnoreCase(methodName)) {

            JSONArray jsonObj;

            try {

                Global.userdataModel = new LoginModel();
                if (result.contains("\"success\":0")) {
                    isLogin = false;
                } else {
                    if (isCodeSentManually.equalsIgnoreCase("Yes")) {
                        new UserSharedPrefrence(mContext).setOTP(edt_optnumber.getText().toString());
                    } else {
                        new UserSharedPrefrence(mContext).setOTP(OTP_ID);
                    }
                    new UserSharedPrefrence(mContext).setMOBILENUMBER(evMobileNumber.getText().toString().trim());
                    Utility.writeToFile(result, methodName, mContext);
                    new UserSharedPrefrence(mContext).setLOGIN_USERNAME(userNameStr);

                    jsonObj = new JSONArray(result);

                    Global.userdataModel.setLoginJson(result);
                    PreferenceData.setLogin(true);

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        /*store selected user data into loginmodel*/
//                        Global.userdataModel = new Gson().fromJson(innerObj.toString(), LoginModel.class);
                        Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);

                        /*END*/
                        new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);
                        Log.d("getLoginUserModel", Global.userdataModel.toString());

//                        Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
//                        Global.userdataModel.setLoginJson(result);

                        // Store whole detail of single user to sharedpreference
                        PreferenceData.saveLoginUserData(result);

                        isLogin = true;
                        isMoreChild = false;

                    }

                }

            } catch (JSONException e) {

                isLogin = false;
                e.printStackTrace();

            }

            if (isLogin) {

                changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

            } else {

                Utility.showToast(getResources().getString(R.string.InvalidMobileNumberOrPassword), mContext);

            }

        } else if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN.equalsIgnoreCase(methodName)) {

            JSONArray jsonObj;

            try {

                if (result.contains("\"success\":0")) {

                    isLogin = false;

                } else {

                    Global.userdataModel = new LoginModel();

                    jsonObj = new JSONArray(result);

                    for (int i = 0; i < jsonObj.length(); i++) {

                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        if (jsonObj.length() == 1) {
                            PreferenceData.setCTOKEN(innerObj.getString("SQLConStr"));
                        }

//                      Get New Response for Multidb within New LoginModel if single user exist

                        try {
                            LoginUserModel getUserResponseModel = new Gson().fromJson(innerObj.toString(), LoginUserModel.class);
                            UserSharedPrefrence.saveLoginData(mContext, getUserResponseModel);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        /*END*/

                    }

                    if (jsonObj.length() == 1) {

                        // if response length is one then get All detail of user by calling userselection api
                        try {
                            GetLoginResponseFromUserSelectionAPI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        PreferenceData.setMultipleUser(true);
                        if (isCodeSentManually.equalsIgnoreCase("Yes")) {
                            new UserSharedPrefrence(mContext).setOTP(edt_optnumber.getText().toString());
                        } else {
                            new UserSharedPrefrence(mContext).setOTP(OTP_ID);
                        }
                        new UserSharedPrefrence(mContext).setMOBILENUMBER(evMobileNumber.getText().toString().trim());
                        Utility.writeToFile(result, methodName, mContext);
                        new UserSharedPrefrence(mContext).setLOGIN_USERNAME(userNameStr);

                        /*store data in storage for display in switch user screen*/
                        PreferenceData.switchsaveLoginUserData(result);
                        /*END*/

                        new UserSharedPrefrence(mContext).setISMULTIPLE(true);
                        isMoreChild = true;
                        isLogin = true;

                        Global.userdataModel.setSave(true);

                        /*redirect to switch Account screen for multiple  user list*/
                        Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
                        intentHomeWork.putExtra("position", getResources().getString(R.string.SwitchAccount));
                        intentHomeWork.putExtra("isFrom", ServiceResource.FROMLOGIN);
                        startActivity(intentHomeWork);
                        finish();
                        overridePendingTransition(0, 0);
                        /*END*/

                    }
                }

            } catch (JSONException e) {

                isLogin = false;
                e.printStackTrace();

            }


        } else if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            if (isMoreChild) {

                Global.userdataModel.setSave(true);
                Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
                intentHomeWork.putExtra("position", getResources().getString(R.string.SwitchAccount));
                intentHomeWork.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(intentHomeWork);
                finish();
                overridePendingTransition(0, 0);

            } else {

                GetUserRoleRightList();

                /*commented By Krishna : 28-02-2019 Remove ERP */

//                isERP();

                /*END*/

            }

        } else if (ServiceResource.CHKINSTITUTEORUSEREXISTINERP_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("response", result);

            if (result.equalsIgnoreCase("isavailable")) {

                new UserSharedPrefrence(mContext).setIsERP(true);

            } else {

                new UserSharedPrefrence(mContext).setIsERP(false);

            }

            GetUserRoleRightList();

        } else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            try {

                Log.d("getRoleRightsResult", result);

                Utility.writeToFile(result, methodName, mContext);
                JSONArray jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).setREADWRITESETTING(result);
                Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
                ReadWriteSettingModel model;

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new ReadWriteSettingModel();
                    model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                    model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));

                    if (!Global.userdataModel.getRoleName().equalsIgnoreCase("teacher") ||
                            !Global.userdataModel.getRoleName().equalsIgnoreCase("Teacher")) {

                        model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                        model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                        model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                        model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));

                    } else {

                        model.setIsView("true");
                        model.setIsEdit("true");
                        model.setIsCreate("true");
                        model.setIsDelete("true");

                    }

                    Global.readWriteSettingList.add(model);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (isMoreChild) {

                Global.userdataModel.setSave(true);

                Intent intentHomeWord = new Intent(mContext, HomeWorkFragmentActivity.class);

                /*commented By Krishna - 23-02-2019 Remove Condition for ISERP*/

                intentHomeWord.putExtra("position", getResources().getString(R.string.SwitchAccount));

                /*END*/

                intentHomeWord.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(intentHomeWord);
                finish();
                overridePendingTransition(0, 0);

            } else {

                saveLoginLog();

            }

        } else if (ServiceResource.SAVELOGINLOG_METHODNAME.equalsIgnoreCase(methodName)) {

            if (isMoreChild) {

                Global.userdataModel.setSave(true);
//                Global.loginuserdataModel.setSave(true);
                Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
                /*commented By Krishna - 23-02-2019 Remove Condition for ISERP*/

                i.putExtra("position", getResources().getString(R.string.SwitchAccount));

                /*END*/

//                if (new UserSharedPrefrence(mContext).getIsERP()) {
//                    i.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                } else {
//                    i.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                }

                i.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            } else {
                if (isLogin) {

                    new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);
                    new UserSharedPrefrence(mContext).setISREMEMBAR(true);
                    Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
                    intentHomeWork.putExtra("position", mContext.getResources().getString(R.string.Wall));
                    startActivity(intentHomeWork);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        }

    }

    private void GetLoginResponseFromUserSelectionAPI() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, UserSharedPrefrence.loadLoginData(mContext).getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_IDENTITY, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, "android"));

        Log.d("userselectionrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_SELECTION, ServiceResource.REGISTATION_URL);

    }

    private void sendVerificationCode(String mobile) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                CountryCode + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);


    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            AythenticationCode = phoneAuthCredential.getSmsCode();
            ll_opt.setVisibility(View.VISIBLE);
            ll_edt_mobile.setVisibility(View.GONE);
            btn_continue.setVisibility(View.GONE);
            btn_verify_otp.setVisibility(View.VISIBLE);
            fl_back.setVisibility(View.VISIBLE);
            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {


            //For emulator - Firebase OTP not working in Emulator or Virtual Device, so just un comment this code and do continue OTP screen
            //-------------------START----------------------

            ll_opt.setVisibility(View.VISIBLE);
            ll_edt_mobile.setVisibility(View.GONE);
            btn_continue.setVisibility(View.GONE);
            btn_verify_otp.setVisibility(View.VISIBLE);
            fl_back.setVisibility(View.VISIBLE);

            //-------------------END----------------------


//            Utility.Longtoast(RegisterActivity.this, e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {

            if (AythenticationCode != null) {

                Utility.showToast("OTP Send On Your Mobile Number", mContext);

                ll_opt.setVisibility(View.VISIBLE);
                ll_edt_mobile.setVisibility(View.GONE);
                btn_continue.setVisibility(View.GONE);
                btn_verify_otp.setVisibility(View.VISIBLE);
                fl_back.setVisibility(View.VISIBLE);

            }

            //storing the verification id that is sent to the user
            mVerificationId = s;

        }

    };


    public void saveLoginLog() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, Global.userdataModel.getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, Global.userdataModel.getMemberID()));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SAVELOGINLOG_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void GetUserRoleRightList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, Global.userdataModel.getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, Global.userdataModel.getRoleID()));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);

    }


    public void changeGCMID(String UserId) {

        if (PreferenceData.getToken().equalsIgnoreCase(null) || PreferenceData.getToken().equalsIgnoreCase("null")

                || PreferenceData.getToken().equalsIgnoreCase("") || PreferenceData.getToken().isEmpty()) {

            try {
                Utility.RefreshFirebaseToken(mContext);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        Log.d("requestlogin", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();

    }

    private void parseCountryCode(String result) {

        JSONArray hJsonArray;

        try {

            if (result.contains("\"success\":0")) {

                // Toast.makeText(mContext,
                // getResources().getString(R.string.no_data), 0)
                // .show();

            } else {

                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {

                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    CountryModel model = new CountryModel();
                    model.setCountrCode(hJsonObject.getString(ServiceResource.COUNTRY_CODE));
                    model.setCountryName(hJsonObject
                            .getString(ServiceResource.COUNTRY_NAME));

                    CountryList.add(model);
                    Log.d("getCountryList", CountryList.toString());

                }

                if (CountryList.size() > 0) {

                    CountriesListAdapter adapter = new CountriesListAdapter(this,
                            R.layout.country_list_item, CountryList);
                    spn_country.setAdapter(adapter);

                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {


    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length() >= 6) {
            if (Utility.isNetworkAvailable(mContext)) {
                if (edt_optnumber.getText().toString().trim().length() == 0) {
                    Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseEnterotp), "Error");
                } else {
                    verifyOtp();
                }
            } else {
                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

        }

    }

    public void verifyOtp() {

        CTOKENFROMSTR = "true";
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, evMobileNumber.getText().toString()));
        if (isCodeSentManually.equalsIgnoreCase("Yes")) {
            arrayList.add(new PropertyVo(ServiceResource.OTPNumber, edt_optnumber.getText().toString()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.OTPNumber, OTP_ID));
        }

        Log.d("otprequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN, ServiceResource.REGISTATION_URL);

    }

}
