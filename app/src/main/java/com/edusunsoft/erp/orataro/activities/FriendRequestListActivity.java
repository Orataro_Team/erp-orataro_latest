package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FriendRequestAdapter;
import com.edusunsoft.erp.orataro.model.FriendRequestModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class FriendRequestListActivity extends Activity implements
        OnClickListener, ResponseWebServices {

	Context mContext;
	TextView txt_sub_name, txt_nodatafound;
	ImageView img_back, iv_add_friend;
	LinearLayout ll_search;
	EditText edtTeacherName;
	ListView lst_fb_frnd;

	FriendRequestAdapter friendRequestAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends_list_on_wall);

		mContext = FriendRequestListActivity.this;

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		txt_sub_name = (TextView) findViewById(R.id.txt_sub_name);
		txt_sub_name.setText(mContext.getResources().getString(R.string.friendreq));

		txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

		img_back = (ImageView) findViewById(R.id.img_back);
		iv_add_friend = (ImageView) findViewById(R.id.iv_add_friend);

		lst_fb_frnd = (ListView) findViewById(R.id.lst_fb_frnd);

		iv_add_friend.setVisibility(View.INVISIBLE);

		ll_search = (LinearLayout) findViewById(R.id.searchlayout);
		ll_search.setVisibility(View.VISIBLE);

		edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);

		if (Utility.isNetworkAvailable(mContext)) {
			FriendRequestList();
//			parserequestlist(Utility.readFromFile(ServiceResource.GET_FRIENDS_METHODNAME, mContext));
		} else {
//			parserequestlist(Utility.readFromFile(ServiceResource.GET_FRIENDS_METHODNAME, mContext));
			Utility.showAlertDialog(mContext,
					"Please Check Your Internet Connection","Error");
		}

		edtTeacherName.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
				String text = edtTeacherName.getText().toString()
						.toLowerCase(Locale.getDefault());
				friendRequestAdapter.filter(text);
			}
		});

		img_back.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		if (Utility.isNetworkAvailable(mContext)) {
		FriendRequestList();
		//parserequestlist(Utility.readFromFile(ServiceResource.GET_FRIENDS_METHODNAME, mContext));
		} else {
			//parserequestlist(Utility.readFromFile(ServiceResource.GET_FRIENDS_METHODNAME, mContext));
			Utility.showAlertDialog(mContext,
					"Please Check Your Internet Connection","Error");
		}

		super.onResume();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.img_back:

			// Intent intent = new Intent(mContext, FriendListActivity.class);
			// startActivity(intent);
			finish();
			break;

		default:
			break;
		}
	}

	
	
	public void FriendRequestList(){
		ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
		arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
				new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
		arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
				new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
		arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
				new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
		
		new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GET_FRIENDS_METHODNAME,
				ServiceResource.FRIENDS_URL);
	}

	@Override
	public void response(String result, String methodName) {
	if(ServiceResource.GET_FRIENDS_METHODNAME.equalsIgnoreCase(methodName)){
		Utility.writeToFile(result, methodName, mContext);
		parserequestlist(result);
	}
	}
	
	public void parserequestlist(String result){


		JSONArray jsonObj;
		try {
			Global.friendRequestModels = new ArrayList<FriendRequestModel>();

			jsonObj = new JSONArray(result);
			// JSONArray detailArrray= jsonObj.getJSONArray("");

			for (int i = 0; i < jsonObj.length(); i++) {
				JSONObject innerObj = jsonObj.getJSONObject(i);
				FriendRequestModel friendRequestModel = new FriendRequestModel();

				friendRequestModel.setFriendListID(innerObj
						.getString(ServiceResource.GET_FRIENDLISTID));

				friendRequestModel.setRequestDate(innerObj
						.getString(ServiceResource.GET_REQUESTDATE));

				friendRequestModel.setFullName(innerObj
						.getString(ServiceResource.GET_FULLNAME));
				friendRequestModel.setProfilePicture(innerObj
						.getString(ServiceResource.GET_PROFILEPICTURE));

				if (friendRequestModel.getProfilePicture() != null
						&& !friendRequestModel.getProfilePicture().equals(
								"")) {

					if (friendRequestModel.getProfilePicture().contains(
							"/img/"))
						friendRequestModel
								.setProfilePicture(ServiceResource.BASE_IMG_URL1
										+ friendRequestModel
												.getProfilePicture()
												);
					else
						friendRequestModel
								.setProfilePicture(ServiceResource.BASE_IMG_URL1

										+ friendRequestModel
												.getProfilePicture()
												);
				}

				friendRequestModel.setRequestID(innerObj
						.getString(ServiceResource.GET_REQUESTID));
				friendRequestModel.setRequestWallID(innerObj
						.getString(ServiceResource.GET_REQUESTWALLID));
				friendRequestModel.setGradeID(innerObj
						.getString(ServiceResource.FRIENDS_GRADEID));
				friendRequestModel.setGradeName(innerObj
						.getString(ServiceResource.FRIENDS_GRADENAME));
				
				friendRequestModel.setDivisionID(innerObj
						.getString(ServiceResource.FRIENDS_DIVISIONID));
				friendRequestModel.setDivisionName(innerObj
						.getString(ServiceResource.FRIENDS_DIVISIONNAME));
				
				Global.friendRequestModels.add(friendRequestModel);

			}

		} catch (JSONException e) {
			
			e.printStackTrace();
		}

		
		if (Global.friendRequestModels != null
				&& Global.friendRequestModels.size() > 0) {
			friendRequestAdapter = new FriendRequestAdapter(mContext,
					Global.friendRequestModels);
			lst_fb_frnd.setAdapter(friendRequestAdapter);
			txt_nodatafound.setVisibility(View.GONE);
		} else {
			ll_search.setVisibility(View.GONE);
			txt_nodatafound.setVisibility(View.VISIBLE);
			txt_nodatafound.setText(mContext.getResources().getString(R.string.noreqavail));
		}
		
	
	}
}
