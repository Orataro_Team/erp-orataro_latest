package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddDataByTeacherActivity;
import com.edusunsoft.erp.orataro.activities.CheckedStudentListActivity;
import com.edusunsoft.erp.orataro.activities.ViewHomWorkDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.ClassWorkListModel;
import com.edusunsoft.erp.orataro.database.ClassworkListDataDao;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GenerallWallDataDao;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ClassworkAdapter extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private List<ClassWorkListModel> homeWorkModels, copyList;
    private TextView tv_date, tv_subject, tv_sub_detail, txt_teacher_name;
    private ImageView list_image;
    private TextView txt_month;
    private TextView txt_date, txtSubname, txtEnddate;
    private LinearLayout ll_view;
    private LinearLayout ll_date, ll_date_img, ll_subname;
    private int pos = 0;
    public static boolean isFinish;
    private RefreshListner listner;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private static final int ID_CHECKED = 7;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"), Color.parseColor("#F2F2F2")};

    ClassworkListDataDao classworkListDataDao;
    GenerallWallDataDao generallWallDataDao;
    public String DELETE_ID = "";

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    public ClassworkAdapter(Context mmCOntext, List<ClassWorkListModel> homeWorkModels, RefreshListner listner) {

        this.mContext = mmCOntext;
        this.homeWorkModels = homeWorkModels;
        this.listner = listner;
        copyList = new ArrayList<ClassWorkListModel>();
        copyList.addAll(homeWorkModels);
    }

    @Override
    public int getCount() {
        return homeWorkModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.homework_listraw, parent, false);
        LinearLayout ll_finish = (LinearLayout) convertView.findViewById(R.id.ll_finish);
        CheckBox chk_finish = (CheckBox) convertView.findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) convertView.findViewById(R.id.txtisfinish);
        TextView txt_show_upload_homework = (TextView) convertView.findViewById(R.id.txt_show_upload_homework);
        txt_show_upload_homework.setVisibility(View.GONE);
        txtisfinish.setText(mContext.getResources().getString(R.string.isChecked));
        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsDelete() || Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsEdit()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.GONE);
            }
        } else {
            ll_editdelete.setVisibility(View.GONE);
        }

        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
        tv_sub_detail = (TextView) convertView.findViewById(R.id.tv_sub_detail);
        ll_subname = (LinearLayout) convertView.findViewById(R.id.ll_subname);
        txtSubname = (TextView) convertView.findViewById(R.id.txtSubname);
        txtEnddate = (TextView) convertView.findViewById(R.id.txtEnddate);
        txt_teacher_name = (TextView) convertView.findViewById(R.id.txt_teacher_name);
        if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            txt_teacher_name.setVisibility(View.VISIBLE);
        }
        TextView txtdivstd = (TextView) convertView.findViewById(R.id.txtdivstd);
        txtdivstd.setVisibility(View.VISIBLE);

        final QuickAction quickActionForEditOrDelete;
        ActionItem actionEdit, actionDelete, actionChecked;
        actionEdit = new ActionItem(ID_EDIT, "Edit", mContext.getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, "Delete", mContext.getResources().getDrawable(R.drawable.delete));
        actionChecked = new ActionItem(ID_CHECKED, "Checked", mContext.getResources().getDrawable(R.drawable.check_icon));
        quickActionForEditOrDelete = new QuickAction(mContext, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionEdit);
        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.addActionItem(actionChecked);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos,
                                    int actionId) {

                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_EDIT) {
                    int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    btnClick(editPos, 1);
                } else if (actionId == ID_DELETE) {
                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    Utility.deleteDialog(mContext, mContext.getResources().getString(R.string.Classwork), homeWorkModels.get(deletepos).getClassWorkTitle(), new Popup() {

                        @Override
                        public void deleteYes() {
                            DELETE_ID = homeWorkModels.get(deletepos).getClassWorkID();
                            deleteClassWork(homeWorkModels.get(deletepos).getClassWorkID());

                        }

                        @Override
                        public void deleteNo() {

                        }

                    });

                } else if (actionId == ID_CHECKED) {

                    /*commented By Krishna : 17-05-2019 Redirect to CheckHomework List of Student*/

                    Intent studentlistintent = new Intent(mContext, CheckedStudentListActivity.class);
                    studentlistintent.putExtra("AssociationID", homeWorkModels.get(position).getClassWorkID());

                    if (homeWorkModels.get(position).getGradeID().equalsIgnoreCase(null) || homeWorkModels.get(position).getGradeID().equalsIgnoreCase("null")) {
                        studentlistintent.putExtra("GradeID", "");
                    } else {
                        studentlistintent.putExtra("GradeID", homeWorkModels.get(position).getGradeID());
                    }
                    if (homeWorkModels.get(position).getDivisionID().equalsIgnoreCase(null) || homeWorkModels.get(position).getDivisionID().equalsIgnoreCase("null")) {
                        studentlistintent.putExtra("DivisionID", "");
                    } else {
                        studentlistintent.putExtra("DivisionID", homeWorkModels.get(position).getDivisionID());
                    }
                    studentlistintent.putExtra("AssociationType", "Classwork");
                    mContext.startActivity(studentlistintent);

                    /*END*/


                }
            }

        });

        quickActionForEditOrDelete.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickActionForEditOrDelete.show(v);
            }
        });

        txt_date.setText(homeWorkModels.get(position).getYear());
        ll_subname.setVisibility(View.VISIBLE);
        txtEnddate.setVisibility(View.GONE);
        if (Utility.isNull(homeWorkModels.get(position).getSubjectName())) {
            txtSubname.setText(homeWorkModels.get(position).getSubjectName());
        } else {
            txtSubname.setVisibility(View.GONE);
        }

        convertView.setBackgroundColor(colors[position % colors.length]);
        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);

        if (Utility.isTeacher(mContext)) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }

        if (!homeWorkModels.get(position).isCheck()) {
            chk_finish.setChecked(false);
        } else {
            chk_finish.setChecked(true);
        }

        if (homeWorkModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        if (homeWorkModels.get(position).isRead()) {
            list_image.setImageResource(R.drawable.double_tick_sky_blue);
        } else {
            list_image.setImageResource(R.drawable.tick_sky_blue);
        }

        txtdivstd.setText(homeWorkModels.get(position).getGradeName() + " " + homeWorkModels.get(position).getDivisionName());
        tv_date.setText(homeWorkModels.get(position).getDay());
        tv_subject.setText(homeWorkModels.get(position).getClassWorkTitle());
        txt_teacher_name.setText(homeWorkModels.get(position).getTechearName());

        if (Utility.isNull(homeWorkModels.get(position).getClassWorkDetails())) {

            if (homeWorkModels.get(position).getClassWorkDetails().length() > Constants.CONTINUEREADINGSIZE) {

                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = homeWorkModels.get(position).getClassWorkDetails()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR), Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {

                        if (!homeWorkModels.get(position).isRead()) {

                            if (Utility.isNetworkAvailable(mContext)) {

                                pos = position;
                                homeworkisread(homeWorkModels.get(position).getClassWorkID());

                            } else {

                                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                            }

                        } else {
                            btnClick(position, 0);
                        }
                    }
                };

                text.setSpan(clickfor, Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);

                tv_sub_detail.setMovementMethod(LinkMovementMethod
                        .getInstance());

                tv_sub_detail.setText(text, BufferType.SPANNABLE);

            } else {
                tv_sub_detail.setText(homeWorkModels.get(position).getClassWorkDetails());
            }

        }

        tv_sub_detail.setText(homeWorkModels.get(position).getClassWorkDetails());
        txt_month.setText(homeWorkModels.get(position).getMonth().toUpperCase());

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!homeWorkModels.get(position).isRead()) {

                    if (Utility.isNetworkAvailable(mContext)) {

                        pos = position;
                        homeworkisread(homeWorkModels
                                .get(position).getClassWorkID());

                    } else {

                        Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                    }

                } else {

                    btnClick(position, 0);

                }

            }

        });

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    pos = position;
                    isFinish = true;
                    classworkListIsApprove(homeWorkModels.get(position).getClassWorkID(), true);

                } else {
                    pos = position;
                    isFinish = false;
                    classworkListIsApprove(homeWorkModels.get(position).getClassWorkID(), false);

                }

            }

        });

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        homeWorkModels.clear();
        if (charText.length() == 0) {
            homeWorkModels.addAll(copyList);
        } else {
            for (ClassWorkListModel vo : copyList) {
                if (vo.getClassWorkTitle().toLowerCase(Locale.getDefault()).contains(charText) || vo.getClassWorkDetails().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getStrDateOfClasswork().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getSubjectName().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getGradeName().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getTechearName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    homeWorkModels.add(vo);
                }

            }

        }

        this.notifyDataSetChanged();

    }

    public void homeworkisread(String assosiationId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "ClassWork"));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void classworkListIsApprove(String assosiationId, boolean isApprove) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "ClassWork"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        Log.d("getRequestisapprove123", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);
    }

    public void deleteClassWork(String id) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.PRIMARYID, id));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "ClassWork"));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ASSOCIATIONDELETE_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void NotifyClassworkData(String id, boolean checked) {

        for (int i = 0; i < homeWorkModels.size(); i++) {

            if (homeWorkModels.get(i).getClassWorkID().equalsIgnoreCase(id)) {
                homeWorkModels.get(pos).setCheck(checked);
                homeWorkModels.set(pos, homeWorkModels.get(pos));
                notifyDataSetChanged();

            }
        }
    }

    public void btnClick(int position, int i) {

        if (i == 1) {

            if (Utility.isTeacher(mContext)) {
                Intent intent = new Intent(mContext, AddDataByTeacherActivity.class);
                intent.putExtra("gradeId", homeWorkModels.get(position).getGradeID());
                intent.putExtra("divisionId", homeWorkModels.get(position).getDivisionID());
                intent.putExtra("subjectId", homeWorkModels.get(position).getSubjectID());
                intent.putExtra("isFrom", ServiceResource.CLASSWORK_FLAG);
                intent.putExtra("isEdit", true);
                intent.putExtra("ClassworkModel", homeWorkModels.get(position));
                mContext.startActivity(intent);
            }

        } else {

            homeWorkModels.get(position).setRead(true);
            homeWorkModels.set(position, homeWorkModels.get(position));
            notifyDataSetChanged();
            Intent intent = new Intent(mContext, ViewHomWorkDetailsActivity.class);
            intent.putExtra("id", homeWorkModels.get(position).getClassWorkID());
            intent.putExtra("isApprove", homeWorkModels.get(position).isCheck());
            intent.putExtra("imgUrl", homeWorkModels.get(position).getImgUrl());
            intent.putExtra("gradeName", homeWorkModels.get(position).getGradeName());
            intent.putExtra("divisionName", homeWorkModels.get(position).getDivisionName());
            intent.putExtra("Subname", homeWorkModels.get(position).getSubjectName());
            intent.putExtra("Sub_details", homeWorkModels.get(position).getClassWorkDetails());
            intent.putExtra("referencelink", homeWorkModels.get(position).getReferenceLink());
            intent.putExtra("endtime", homeWorkModels.get(position).getEndTime());
            intent.putExtra("starttime", homeWorkModels.get(position).getStartTime());
            intent.putExtra("Teacher_name", homeWorkModels.get(position).getTechearName());
            intent.putExtra("Teacher_img", homeWorkModels.get(position).getTeacherImg());
            intent.putExtra("lastdate", homeWorkModels.get(position).getDateOfClassWork());
            intent.putExtra("isFrom", "Classwork");
            intent.putExtra("Title", homeWorkModels.get(position).getClassWorkTitle());
            intent.putExtra("ReadStatus", mContext.getResources().getString(R.string.isChecked));
            mContext.startActivity(intent);

        }

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            btnClick(pos, 0);
        } else if (ServiceResource.APPROVE_METHODNAME.equalsIgnoreCase(methodName)) {
            // Commented By Krishna : 18-09-2020 -12:22 - Added to Not refresh after back from detail activity.
            homeWorkModels.get(pos).setCheck(isFinish);
            homeWorkModels.set(pos, homeWorkModels.get(pos));
            notifyDataSetChanged();
            /*END*/
//            listner.refresh(methodName);
        } else if (ServiceResource.ASSOCIATIONDELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            classworkListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).classworkListDataDao();
            classworkListDataDao.deleteclassworkItemByClassworkID(DELETE_ID);
            generallWallDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).generawallDataDao();
            generallWallDataDao.deleteWallItemByAssID(DELETE_ID);
            listner.refresh(methodName);
            Utility.showToast(mContext.getResources().getString(R.string.classworkdelete), mContext);
        }
    }

}
