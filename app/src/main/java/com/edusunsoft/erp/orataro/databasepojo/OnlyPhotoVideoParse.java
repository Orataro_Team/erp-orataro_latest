package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlyPhotoVideoParse implements Parcelable {

    @SerializedName("RowNo")
    @Expose
    private Integer rowNo;
    @SerializedName("PostCommentID")
    @Expose
    private String postCommentID;
    @SerializedName("DateOfPost")
    @Expose
    private String dateOfPost;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("FileMimeType")
    @Expose
    private String fileMimeType;
    public final static Parcelable.Creator<OnlyPhotoVideoParse> CREATOR = new Creator<OnlyPhotoVideoParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OnlyPhotoVideoParse createFromParcel(Parcel in) {
            OnlyPhotoVideoParse instance = new OnlyPhotoVideoParse();
            instance.rowNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.postCommentID = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfPost = ((String) in.readValue((String.class.getClassLoader())));
            instance.photo = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileType = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileMimeType = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public OnlyPhotoVideoParse[] newArray(int size) {
            return (new OnlyPhotoVideoParse[size]);
        }

    };

    public Integer getRowNo() {
        return rowNo;
    }

    public void setRowNo(Integer rowNo) {
        this.rowNo = rowNo;
    }

    public String getPostCommentID() {
        return postCommentID;
    }

    public void setPostCommentID(String postCommentID) {
        this.postCommentID = postCommentID;
    }

    public String getDateOfPost() {
        return dateOfPost;
    }

    public void setDateOfPost(String dateOfPost) {
        this.dateOfPost = dateOfPost;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileMimeType() {
        return fileMimeType;
    }

    public void setFileMimeType(String fileMimeType) {
        this.fileMimeType = fileMimeType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(rowNo);
        dest.writeValue(postCommentID);
        dest.writeValue(dateOfPost);
        dest.writeValue(photo);
        dest.writeValue(fileType);
        dest.writeValue(fileMimeType);
    }

    public int describeContents() {
        return 0;
    }

}
