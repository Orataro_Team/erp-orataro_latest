package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollAnsSubParse implements Parcelable {

    @SerializedName("PollOptionID")
    @Expose
    private String pollOptionID;
    @SerializedName("PollID")
    @Expose
    private String pollID;
    @SerializedName("Option")
    @Expose
    private String option;
    public final static Parcelable.Creator<PollAnsSubParse> CREATOR = new Creator<PollAnsSubParse>() {
        @SuppressWarnings({
                "unchecked"
        })
        public PollAnsSubParse createFromParcel(Parcel in) {
            PollAnsSubParse instance = new PollAnsSubParse();
            instance.pollOptionID = ((String) in.readValue((String.class.getClassLoader())));
            instance.pollID = ((String) in.readValue((String.class.getClassLoader())));
            instance.option = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public PollAnsSubParse[] newArray(int size) {
            return (new PollAnsSubParse[size]);
        }

    };

    public String getPollOptionID() {
        return pollOptionID;
    }

    public void setPollOptionID(String pollOptionID) {
        this.pollOptionID = pollOptionID;
    }

    public String getPollID() {
        return pollID;
    }

    public void setPollID(String pollID) {
        this.pollID = pollID;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pollOptionID);
        dest.writeValue(pollID);
        dest.writeValue(option);
    }

    public int describeContents() {
        return 0;
    }

}
