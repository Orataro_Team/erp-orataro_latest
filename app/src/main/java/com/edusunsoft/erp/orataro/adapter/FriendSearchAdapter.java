package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FriendSearchModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

import java.util.ArrayList;

public class FriendSearchAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private ArrayList<FriendSearchModel> searchFriendModels;
    private TextView tv_frnd_name, txt_mutual_frnd, tv_reuest_send, tv_add_friend;
    private ImageView iv_add_friend;
    private CircleImageView iv_profile_pic;
    private LinearLayout ll_add_frnd, ll_frnd;
    private TextView txtGradename, txtDivision;
    private int pos;

    public FriendSearchAdapter(Context context, ArrayList<FriendSearchModel> pSearchFriendModels) {
        mContext = context;
        searchFriendModels = pSearchFriendModels;
    }

    @Override
    public int getCount() {
        return searchFriendModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.friend_listraw, parent, false);
        ll_add_frnd = (LinearLayout) convertView.findViewById(R.id.ll_add_frnd);
        ll_frnd = (LinearLayout) convertView.findViewById(R.id.ll_frnd);
        ll_frnd.setVisibility(View.GONE);
        txtGradename = (TextView) convertView.findViewById(R.id.txtGradename);
        txtDivision = (TextView) convertView.findViewById(R.id.txtDivision);
        tv_frnd_name = (TextView) convertView.findViewById(R.id.tv_frnd_name);
        tv_add_friend = (TextView) convertView.findViewById(R.id.tv_add_friend);
        tv_reuest_send = (TextView) convertView.findViewById(R.id.tv_reuest_send);
        iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);
        iv_add_friend = (ImageView) convertView.findViewById(R.id.iv_add_friend);
        if (searchFriendModels.get(position).getFullName() != null
                && !searchFriendModels.get(position).getFullName().equals("")) {
            tv_frnd_name.setText(searchFriendModels.get(position).getFullName());
        }

        CircleImageView iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);

        if (searchFriendModels.get(position).getProfilePicture() != null) {

            if (searchFriendModels.get(position).getProfilePicture() != null
                    && !searchFriendModels.get(position).getProfilePicture().equalsIgnoreCase("")) {

                try {

                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(ServiceResource.BASE_IMG_URL1 + searchFriendModels.get(position).getProfilePicture())
                            .apply(options)
                            .into(iv_profile_pic);

                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }

        if (isValid(searchFriendModels.get(position).getGradeName())) {
            txtGradename.setText(mContext.getResources().getString(R.string.Standard) + " :" + searchFriendModels.get(position).getGradeName());
        } else {
            txtGradename.setVisibility(View.INVISIBLE);
        }

        if (isValid(searchFriendModels.get(position).getDivisionName())) {
            txtDivision.setText(mContext.getResources().getString(R.string.Division) + " :" + searchFriendModels.get(position).getDivisionName());
        } else {
            txtDivision.setVisibility(View.INVISIBLE);
        }

        if (searchFriendModels.get(position).getIsFriend() != null
                && !searchFriendModels.get(position).getIsFriend().equals("")) {
            if (Integer
                    .parseInt(searchFriendModels.get(position).getIsFriend()) == 1) {
                ll_add_frnd.setVisibility(View.GONE);
                ll_frnd.setVisibility(View.VISIBLE);
            } else {
                ll_add_frnd.setVisibility(View.VISIBLE);
                ll_frnd.setVisibility(View.GONE);
            }
        }

        if (searchFriendModels.get(position).getIsRequested() != null
                && !searchFriendModels.get(position).getIsRequested().equals("")) {
            if (Integer.parseInt(searchFriendModels.get(position).getIsRequested()) == 1) {
                tv_reuest_send.setVisibility(View.VISIBLE);
                ll_add_frnd.setBackgroundResource(R.drawable.btn_bg_grey);
                ll_add_frnd.setVisibility(View.GONE);
            } else {
                ll_add_frnd.setBackgroundResource(R.drawable.btn_bg_blue);
            }
        }

        if (searchFriendModels.get(position).getIsRequested().equals("1")) {
            tv_reuest_send.setVisibility(View.VISIBLE);
            ll_add_frnd.setVisibility(View.GONE);
        }

        ll_add_frnd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                pos = position;
                SendRequestFriend();
            }
        });

        return convertView;
    }

    public boolean isValid(String str) {
        boolean isValid = true;
        if (str != null) {
            if (str.equalsIgnoreCase("") || str.equalsIgnoreCase("null")) {
                isValid = false;
            }
        } else {
            isValid = false;
        }

        return isValid;
    }

    public void SendRequestFriend() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.SEND_TOMEMBERID, Global.searchFriendModels.get(pos).getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_BYMEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_WALLID,
                new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.SEND_USERID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SEND_FRIENDS_METHODNAME,
                ServiceResource.FRIENDS_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.SEND_FRIENDS_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result.contains("[{\"message\":\"Friend Request Send SuccessFully.\",\"success\":0}]")) {
                searchFriendModels.get(pos).setIsRequested("1");
                notifyDataSetChanged();
            }
        }
    }

}
