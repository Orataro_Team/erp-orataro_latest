package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPhotoActivity;
import com.edusunsoft.erp.orataro.adapter.SaprateWallType;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.PhotoVideoFileDataDao;
import com.edusunsoft.erp.orataro.database.PhotoVideoFileModel;
import com.edusunsoft.erp.orataro.loadmoreListView.LoadMoreGrideVIew;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PhotoVideoOnlyFragment extends Fragment implements ResponseWebServices, RefreshListner, View.OnClickListener {

    private static final String ISFROM = "isFrom";
    private Context mContext;
    private int count = 1;
    private String fileType = "";
    private ArrayList<WallPostModel> wallList;
    private SaprateWallType adapter;
    private LoadMoreGrideVIew listView;
    private String from = "";
    private TextView txtNodataFound;
    private boolean isMoreData = true;
    private String file = "";
    private int refreshtype;
    LinearLayout ll_add_new;

    // variable declaration for homeworklist from ofline database
    PhotoVideoFileDataDao photoVideoFileDataDao;
    private List<PhotoVideoFileModel> PhotoVideoFileList = new ArrayList<>();
    PhotoVideoFileModel photoVideoFileModel = new PhotoVideoFileModel();
    /*END*/

    public static final PhotoVideoOnlyFragment newInstance(String isFrom) {
        PhotoVideoOnlyFragment f = new PhotoVideoOnlyFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(ISFROM, isFrom);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.comingsoon, null);
        mContext = getActivity();
        this.fileType = getArguments().getString(ISFROM);
        count = 1;

        ll_add_new = (LinearLayout) v.findViewById(R.id.ll_add_new);
        ll_add_new.setOnClickListener(this);

        if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
            ll_add_new.setVisibility(View.GONE);
        } else {
            ll_add_new.setVisibility(View.VISIBLE);
        }

        if (ServiceResource.FILE.equals(fileType)) {
            file = "FILE";
            ll_add_new.setVisibility(View.GONE);
        } else if (ServiceResource.VIDEO.equals(fileType) || ServiceResource.MP3.equalsIgnoreCase(fileType)) {
            file = "VIDEO";
            ll_add_new.setVisibility(View.GONE);
        } else {
            file = "IMAGE";
        }

        photoVideoFileDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).photoVideoFileDataDao();

        listView = (LoadMoreGrideVIew) v.findViewById(R.id.lst_fb_wall);
        txtNodataFound = (TextView) v.findViewById(R.id.txt_nodatafound);

        return v;

    }

    public void Video(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
//        arrayList.add(new PropertyVo("Count", count + ""));
        if (ServiceResource.FILE.equals(fileType)) {
            arrayList.add(new PropertyVo("PostFilterType", "FILE"));
        } else if (ServiceResource.VIDEO.equals(fileType) || ServiceResource.MP3.equalsIgnoreCase(fileType)) {
            arrayList.add(new PropertyVo("PostFilterType", "VIDEO"));
        } else {
            arrayList.add(new PropertyVo("PostFilterType", "IMAGE"));
        }

        Log.d("getVideolist", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETPOSTED_FILEIMGAEVIDEOS_METHODNAMEFORALLDATA, ServiceResource.POST_URL);

    }

    @Override
    public void onResume() {
        super.onResume();
        count = 1;

//        // Display PhotoVideoFileList from offline as well as online
        try {
            DisplayPhotoVideoFileList();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        /*END*/

//
//        if (Utility.isNetworkAvailable(getActivity())) {
//            Video(true);
//        } else {
//            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
//        }

    }

    private void DisplayPhotoVideoFileList() {
        // Commented By Krishna : 27-06-2020 - get photovideofile from offline database.
        if (Utility.isNetworkAvailable(mContext)) {

            if (fileType.equalsIgnoreCase("IMAGE")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("IMAGE");
            } else if (fileType.equalsIgnoreCase("VIDEO")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("VIDEO");
            } else if (fileType.equalsIgnoreCase("FILE")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("FILE");
            }
//            if(fileType.equalsIgnoreCase("IMAGE"))
//            PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("IMAGE");
            if (PhotoVideoFileList.isEmpty() || PhotoVideoFileList.size() == 0 || PhotoVideoFileList == null) {
                Video(true);
            } else {
                //set Adapter
                setPhotoVideoFilelistAdapter(PhotoVideoFileList);
                /*END*/
                Video(false);
            }
        } else {
            if (fileType.equalsIgnoreCase("IMAGE")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("IMAGE");
            } else if (fileType.equalsIgnoreCase("VIDEO")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("VIDEO");
            } else if (fileType.equalsIgnoreCase("FILE")) {
                PhotoVideoFileList = photoVideoFileDataDao.getPhotoVideoFileData("FILE");
            }
            if (PhotoVideoFileList != null) {
                setPhotoVideoFilelistAdapter(PhotoVideoFileList);
            }
            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

    }

    private void setPhotoVideoFilelistAdapter(List<PhotoVideoFileModel> photoVideoFileList) {

        if (photoVideoFileList != null && photoVideoFileList.size() > 0) {

            if (count == 1) {
                adapter = new SaprateWallType(mContext, photoVideoFileList, this);
                listView.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }

            txtNodataFound.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);

        } else {

            txtNodataFound.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            txtNodataFound.setText(mContext.getResources().getString(R.string.no) + " " + fileType + " " + mContext.getResources().getString(R.string.available));
            txtNodataFound.setText(mContext.getResources().getString(R.string.no) + " " + fileType + " " + mContext.getResources().getString(R.string.available));

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (methodName.equalsIgnoreCase(ServiceResource.GETPOSTED_FILEIMGAEVIDEOS_METHODNAMEFORALLDATA)) {

            onlyVideoParse(result, ServiceResource.UPDATE);

        }

    }

    public void onlyVideoParse(String result, String update) {

        if (result != null && !result.toString().equals(
                "[{\"message\":\"No Data Found\",\"success\":0}]")) {

            try {

                if (fileType.equalsIgnoreCase("IMAGE")) {
                    photoVideoFileDataDao.deletePhotoVideoFileData("IMAGE");
                } else if (fileType.equalsIgnoreCase("VIDEO")) {
                    photoVideoFileDataDao.deletePhotoVideoFileData("VIDEO");
                } else if (fileType.equalsIgnoreCase("FILE")) {
                    photoVideoFileDataDao.deletePhotoVideoFileData("FILE");
                }
                JSONArray hJsonArray = new JSONArray(result);
                for (int i = 0; i < hJsonArray.length(); i++) {

                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);

                    // parse photovideofile list data
                    ParsePhotoVideoFileList(hJsonObject);
                    /*END*/

                }

                isMoreData = true;

                if (fileType.equalsIgnoreCase("IMAGE")) {
                    if (photoVideoFileDataDao.getPhotoVideoFileData("IMAGE").size() > 0) {
                        setPhotoVideoFilelistAdapter(photoVideoFileDataDao.getPhotoVideoFileData("IMAGE"));
                    }
                } else if (fileType.equalsIgnoreCase("VIDEO")) {
                    if (photoVideoFileDataDao.getPhotoVideoFileData("VIDEO").size() > 0) {
                        setPhotoVideoFilelistAdapter(photoVideoFileDataDao.getPhotoVideoFileData("VIDEO"));
                    }
                } else if (fileType.equalsIgnoreCase("FILE")) {
                    if (photoVideoFileDataDao.getPhotoVideoFileData("FILE").size() > 0) {
                        setPhotoVideoFilelistAdapter(photoVideoFileDataDao.getPhotoVideoFileData("FILE"));
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            isMoreData = false;
        }


//        listView.setOnScrollListener(new OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//                if (isMoreData) {
//                    if (totalItemCount == visibleItemCount) {
//
//                    }
//                }
//            }
//        });

    }

    private void ParsePhotoVideoFileList(JSONObject hJsonObject) {

        try {
            photoVideoFileModel.setAlbumPhotoID(hJsonObject.getString("AlbumPhotosID"));
            photoVideoFileModel.setFileMimeType(hJsonObject.getString("FileMimeType"));
            photoVideoFileModel.setPostCommentID(hJsonObject.getString("PostCommentID"));
            photoVideoFileModel.setPostName(hJsonObject.getString("PostCommentNote"));
            photoVideoFileModel.setPhoto(ServiceResource.BASE_IMG_URL + "/DataFiles/" + hJsonObject.getString("Photo").replace("//DataFiles//", "/DataFiles/")
                    .replace("//DataFiles/", "/DataFiles/"));
            photoVideoFileModel.setDateOfPost(Utility.getDate(Long.valueOf(hJsonObject
                    .getString("DateOfPost").replace("/Date(", "").replace(")/", "")), "dd-MM-yyyy"));
            photoVideoFileModel.setFileType(hJsonObject.getString("FileType"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        photoVideoFileDataDao.insertPhotoVideoFile(photoVideoFileModel);

    }

    @Override
    public void refresh(String methodName) {

        // Display PhotoVideoFileList from offline as well as online
        try {
            DisplayPhotoVideoFileList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*END*/

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_add_new:

                Intent addphotointent = new Intent(getActivity(), AddPhotoActivity.class);
                startActivity(addphotointent);

                break;

        }

    }

}
