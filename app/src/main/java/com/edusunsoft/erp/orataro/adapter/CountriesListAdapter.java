package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.CountryModel;

import java.util.List;

public class CountriesListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<CountryModel> items;
    private final int mResource;

    public CountriesListAdapter(@NonNull Context context, @LayoutRes int resource,
                                @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {

        final View view = mInflater.inflate(mResource, parent, false);

        TextView cntryName = (TextView) view.findViewById(R.id.txtViewCountryName);

        CountryModel countrydata = items.get(position);

        cntryName.setText(countrydata.getCountrCode() + "  " + countrydata.getCountryName());

        return view;

    }

//    private final Context context;
//    private final String[] values;
//
//    public CountriesListAdapter(Context context, String[] values) {
//        super(context, R.layout.country_list_item, values);
//        this.context = context;
//        this.values = values;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        View rowView = inflater.inflate(R.layout.country_list_item, parent, false);
//        TextView textView = (TextView) rowView.findViewById(R.id.txtViewCountryName);
////        ImageView imageView = (ImageView) rowView.findViewById(R.id.imgViewFlag);
//
//        String[] g = values[position].split(",");
//        textView.setText(GetCountryZipCode(g[1]).trim());
//
//
////        String pngName = g[1].trim().toLowerCase();
////        imageView.setImageResource(context.getResources().getIdentifier("drawable/" + pngName, null, context.getPackageName()));
//        return rowView;
//
//    }
//
//    private String GetCountryZipCode(String ssid) {
//        Locale loc = new Locale("", ssid);
//
//        return loc.getDisplayName().trim();
//    }
}
