package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FriendListModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;

import java.util.ArrayList;
import java.util.Locale;

public class Friend_ListAdapter extends BaseAdapter {

	private LayoutInflater layoutInfalater;
	private Context mContext;
	private TextView tv_frnd_name, txt_mutual_frnd;
	private CircleImageView iv_profile_pic;
	private LinearLayout ll_add_frnd, ll_frnd;
	private ArrayList<FriendListModel> friendListModels, copyList;
	private TextView txtGradename,txtDivision;

	public Friend_ListAdapter(Context context, ArrayList<FriendListModel> friendListModels) {
		mContext = context;
		this.friendListModels = friendListModels;
		copyList = new ArrayList<FriendListModel>();
		copyList.addAll(friendListModels);
	}

	@Override
	public int getCount() {
		return friendListModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.friend_listraw, parent, false);
		ll_add_frnd = (LinearLayout) convertView.findViewById(R.id.ll_add_frnd);
		ll_frnd = (LinearLayout) convertView.findViewById(R.id.ll_frnd);
		txtGradename =  (TextView) convertView.findViewById(R.id.txtGradename);
	    txtDivision =  (TextView) convertView.findViewById(R.id.txtDivision);
		ll_add_frnd.setVisibility(View.GONE);
		ll_frnd.setVisibility(View.VISIBLE);
		ll_frnd.setEnabled(false);
		tv_frnd_name = (TextView) convertView.findViewById(R.id.tv_frnd_name);
		txt_mutual_frnd = (TextView) convertView.findViewById(R.id.txt_mutual_frnd);
		iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);

		if (friendListModels.get(position).getFullName() != null
				&& !friendListModels.get(position).getFullName().equals("")) {
			tv_frnd_name.setText(friendListModels.get(position).getFullName());
		}

		if(isValid(friendListModels.get(position).getGradeName())) {
			txtGradename.setText(mContext.getResources().getString(R.string.Standard) + " :" + friendListModels.get(position).getGradeName());
		}else{
			txtGradename.setVisibility(View.INVISIBLE);
		}

		if(isValid(friendListModels.get(position).getDivisionName())) {
			txtDivision.setText(mContext.getResources().getString(R.string.Division) + " :" + friendListModels.get(position).getDivisionName());
		}else{
			txtDivision.setVisibility(View.INVISIBLE);
		}

		CircleImageView iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);

		if (friendListModels.get(position).getProfilePicture() != null) {

			if (friendListModels.get(position).getProfilePicture() != null && !friendListModels.get(position).getProfilePicture().equalsIgnoreCase("")) {
				try {
					RequestOptions options = new RequestOptions()
							.centerCrop()
							.placeholder(R.drawable.ic_user_placeholder)
							.diskCacheStrategy(DiskCacheStrategy.ALL)
							.priority(Priority.HIGH)
							.dontAnimate()
							.dontTransform();

					Glide.with(mContext)
							.load(ServiceResource.BASE_IMG_URL1 + friendListModels.get(position).getProfilePicture())
							.apply(options)
							.into(iv_profile_pic);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return convertView;
	}

	public  boolean isValid(String str){
		boolean isValid = true;
		if(str != null ){
			if(str.equalsIgnoreCase("") || str.equalsIgnoreCase("null")){
				isValid = false;
			}
		}else{
			isValid = false;
		}

	return isValid ;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		friendListModels.clear();
		if (charText.length() == 0) {
			friendListModels.addAll(copyList);
		} else {
			for (FriendListModel vo : copyList) {
				if (vo.getFullName().toLowerCase(Locale.getDefault()).contains(charText)) {
					friendListModels.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}
}
