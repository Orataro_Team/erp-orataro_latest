package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FeesReciptAdapter;
import com.edusunsoft.erp.orataro.adapter.ReciptDetailAdapter;
import com.edusunsoft.erp.orataro.model.FeesReciptModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReciptDetailModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ReciptListFragment extends Fragment {

    private Context mContext;
    private ImageView img_home, img_menu;
    private TextView header_text;
    private ListView lst_recipt;
    private ArrayList<FeesReciptModel> recieptModel;
    private static final String WALLIDDIFF = "id";
    private static final String ISFROM = "isFrom";
    private static final String MODEL = "model";
    private String id, isFrom;
    private View headerview;
    private FeesReciptModel.Table reciptModel;

    public static final ReciptListFragment newInstance(String WallIddiff, String isFrom, FeesReciptModel.Table reciptModel) {

        ReciptListFragment f = new ReciptListFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(WALLIDDIFF, WallIddiff);
        bdl.putString(ISFROM, isFrom);
        bdl.putParcelable(MODEL, reciptModel);
        f.setArguments(bdl);
        return f;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_recipt_list, null);
        mContext = getActivity();
        id = getArguments().getString(WALLIDDIFF);
        isFrom = getArguments().getString(ISFROM);
        if (isFrom.equalsIgnoreCase(ServiceResource.RECIPT)) {
        } else {
            reciptModel = getArguments().getParcelable(MODEL);
        }

        headerview = v.findViewById(R.id.headerview);
        img_home = (ImageView) v.findViewById(R.id.img_home);
        img_menu = (ImageView) v.findViewById(R.id.img_menu);
        header_text = (TextView) v.findViewById(R.id.header_text);
        lst_recipt = (ListView) v.findViewById(R.id.lst_recipt);
        img_menu.setVisibility(View.INVISIBLE);
        img_home.setImageResource(R.drawable.back);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().finish();

            }

        });

        if (isFrom.equalsIgnoreCase(ServiceResource.RECIPT)) {
            header_text.setText(getActivity().getResources().getString(R.string.Receipt));
            try {
                if (HomeWorkFragmentActivity.txt_header != null) {
                    HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Receipt) + "(" + Utility.GetFirstName(mContext) + ")");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            headerview.setVisibility(View.GONE);
            header_text.setText(getActivity().getResources().getString(R.string.reciptdetail));
        }

        TextView txtfees = (TextView) v.findViewById(R.id.txtfees);
        TextView txtammount = (TextView) v.findViewById(R.id.txtammount);
        TextView txtdue = (TextView) v.findViewById(R.id.txtdue);
        TextView txtaction = (TextView) v.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) v.findViewById(R.id.imgremove);
        LinearLayout ll_remove = (LinearLayout) v.findViewById(R.id.ll_remove);
        final LinearLayout ll_bottom = (LinearLayout) v.findViewById(R.id.ll_bottom);
        final ListView lv_structure = (ListView) v.findViewById(R.id.lv_structure);

        if (isFrom.equalsIgnoreCase(ServiceResource.RECIPT)) {
            txtfees.setText("FeesName");
            txtammount.setText("Total Amount");
            txtdue.setText("Recipt Amount");
            txtaction.setText("Action");
            reciptList();

        } else {

            txtfees.setText("FeesName");
            txtammount.setText("Total Amount");
            txtdue.setText("Recipt Amount");
            txtaction.setText("Action");
            reciptDetail();

        }

        return v;
    }

    public void reciptList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo("UserID", new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID,
                new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()/*"ab86b47d-517b-4842-aae4-b6de2ec937d4"*/));

        new AsynsTaskClass(mContext, arrayList, true, new ResponseWebServices() {
            @Override
            public void response(String result, String methodName) {
                try {
                    Type type = new TypeToken<FeesReciptModel>() {
                    }.getType();
                    FeesReciptModel listReciptmodel = new Gson().fromJson(result.toString(), type);
                    FeesReciptAdapter feesreciptadapter = new FeesReciptAdapter(mContext, listReciptmodel.getTable());
                    lst_recipt.setAdapter(feesreciptadapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }).execute(ServiceResource.STUDENTFEESCOLLECTIONLIST_METHODNAME, ServiceResource.FEES_URL);
    }

    public void reciptDetail() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo("StudentID", new UserSharedPrefrence(mContext).getLoginModel().getERPMemberID()));
        arrayList.add(new PropertyVo("FeesStructureID", reciptModel.getFeesStructureID()));
        arrayList.add(new PropertyVo("FeesStructureScheduleID", reciptModel.getFeesStructureScheduleID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));

        new AsynsTaskClass(mContext, arrayList, true, new ResponseWebServices() {
            @Override
            public void response(String result, String methodName) {
                try {
                    Type type = new TypeToken<ReciptDetailModel>() {
                    }.getType();
                    ReciptDetailModel listReciptmodel = new Gson().fromJson(result.toString(), type);
                    ReciptDetailAdapter feesreciptadapter = new ReciptDetailAdapter(mContext, listReciptmodel.getTable());
                    lst_recipt.setAdapter(feesreciptadapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).execute(ServiceResource.GETFEESTRUCTUREWISESTUDENTFEECOLLECTIONDETAIL_METHODNAME, ServiceResource.FEES_URL);

    }
}
