package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddReminderActivity;
import com.edusunsoft.erp.orataro.adapter.TodoListAdapter;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ReminderFragment extends Fragment implements ResponseWebServices, RefreshListner {

    private Context mContext;
    private PullAndLoadListView homeworkList;
    private LinearLayout ll_add_new;
    private TextView txt_nodatafound;
    View headerLayout;
    private TodoListAdapter adapter;
    private Dialog mDeleteDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View convertview = inflater.inflate(R.layout.homeworklist_fragment, null);
        headerLayout = convertview.findViewById(R.id.header);
        headerLayout.setVisibility(View.GONE);
        mContext = getActivity();

        homeworkList = (PullAndLoadListView) convertview.findViewById(R.id.homeworklist);
        ll_add_new = (LinearLayout) convertview.findViewById(R.id.ll_add_new);
        txt_nodatafound = (TextView) convertview.findViewById(R.id.txt_nodatafound);
        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.REMINDERS).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.VISIBLE);
        }

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Todo) + " (" + Utility.GetFirstName(mContext) + ")");
            }
        } catch (Exception e) {

        }

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), AddReminderActivity.class);
                i.putExtra("isFrom", ServiceResource.TODO_FLAG);
                startActivity(i);

            }

        });


        if (Utility.isNetworkAvailable(mContext)) {

            TodoList(true);

        } else {

            Utility.showAlertDialog(getActivity(),
                    mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");

        }


        homeworkList.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            public void onRefresh() {
                if (Utility.isNetworkAvailable(mContext)) {
                    TodoList(true);
                } else {
                    Utility.showAlertDialog(getActivity(),
                            mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
                }
            }
        });


        return convertview;
    }


    private void TodoList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));

        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));


        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.TODO_METHODNAME,
                ServiceResource.TODO_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.TODO_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parseTodoList(result, ServiceResource.UPDATE);
        }

    }

    public class SortedDate implements Comparator<TodoListModel> {
        @Override
        public int compare(TodoListModel o1, TodoListModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }


    @Override
    public void refresh(String methodName) {
        TodoList(false);
    }

    public void parseTodoList(String result, String update) {

        JSONArray jsonObj;
        try {
            Global.todolist = new ArrayList<TodoListModel>();

            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                TodoListModel model = new TodoListModel();


                model.setMilliSecond(innerObj
                        .getString(ServiceResource.CREATEON)
                        .replace("/Date(", "").replace(")/", ""));
                String completedate = "";
                if (!innerObj
                        .getString(ServiceResource.COMPLETDATE).equalsIgnoreCase("") &&
                        !innerObj
                                .getString(ServiceResource.COMPLETDATE).equalsIgnoreCase("null")) {
                    completedate = Utility.getDate(Long.valueOf(innerObj
                            .getString(ServiceResource.COMPLETDATE)
                            .replace("/Date(", "").replace(")/", "")), "MM/dd/yyyy");
                } else {
                    completedate = "05/23/2015";
                }
                String endDate = "";
                if (!innerObj
                        .getString(ServiceResource.ENDDATE).equalsIgnoreCase("") &&
                        !innerObj
                                .getString(ServiceResource.ENDDATE).equalsIgnoreCase("null")) {
                    endDate = Utility.getDate(Long.valueOf(innerObj
                            .getString(ServiceResource.ENDDATE)
                            .replace("/Date(", "").replace(")/", "")), "MM/dd/yyyy");
                } else {
                    endDate = "05/23/2015";
                }
                String createOn = "";

                if (!innerObj
                        .getString(ServiceResource.CREATEON).equalsIgnoreCase("") &&
                        !innerObj
                                .getString(ServiceResource.CREATEON).equalsIgnoreCase("null")) {
                    createOn = Utility.getDate(Long.valueOf(innerObj
                            .getString(ServiceResource.CREATEON)
                            .replace("/Date(", "").replace(")/", "")), "MM/dd/yyyy");
                } else {
                    createOn = "05/23/2015";
                }
                String date = "";
                if (!innerObj
                        .getString(ServiceResource.CREATEON).equalsIgnoreCase("") &&
                        !innerObj
                                .getString(ServiceResource.CREATEON).equalsIgnoreCase("null")) {
                    date = Utility.getDate(Long.valueOf(innerObj
                            .getString(ServiceResource.CREATEON)
                            .replace("/Date(", "").replace(")/", "")), "EEEE/dd/MMM/yyyy");

                } else {
                    date = "05/23/2015";
                }
                String[] dateArray = date.split("/");


                if (dateArray != null && dateArray.length > 0) {

                    model.setDay(dateArray[1] + " "
                            + dateArray[0].substring(0, 3));
                    model.setMonth(dateArray[2]);


                }


                model.setEndDate(endDate);
                model.setCompletDate(completedate);

                model.setTodosID(innerObj.getString(ServiceResource.TODOSID));

                model.setTitle(innerObj.getString(ServiceResource.TITLETODO));
                model.setDetails(innerObj.getString(ServiceResource.DETAILS));
                model.setUserName(innerObj.getString(ServiceResource.USERNAME));
                model.setTypeTerm(innerObj.getString(ServiceResource.TYPETERM));

                model.setStatus(innerObj.getString(ServiceResource.STATUS));
                model.setOnCreated(innerObj.getString(ServiceResource.ONCREATED));
                model.setYear(innerObj.getString(ServiceResource.YEAR));
                model.setCreateOn(createOn);
                Global.todolist.add(model);

            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (Global.todolist != null
                && Global.todolist.size() > 0) {
            Collections.sort(Global.todolist, new SortedDate());
            Collections.reverse(Global.todolist);

            for (int i = 0; i < Global.todolist.size(); i++) {
                if (i != 0) {
                    String temp = Global.todolist.get(i - 1)
                            .getMonth();
                    if (temp.equalsIgnoreCase(Global.todolist.get(i)
                            .getMonth())) {
                        if (Global.todolist.get(i).getYear().equalsIgnoreCase(Global.todolist.get(i - 1).getYear())) {
                            Global.todolist.get(i).setVisibleMonth(false);
                        } else {
                            Global.todolist.get(i).setVisibleMonth(true);
                        }
                    } else {
                        Global.todolist.get(i).setVisibleMonth(true);
                    }
                } else {
                    Global.todolist.get(i).setVisibleMonth(true);
                }

            }
        }

        if (Global.todolist != null
                && Global.todolist.size() > 0) {

            adapter = new TodoListAdapter(mContext,
                    Global.todolist, this);
            homeworkList.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            homeworkList.setVisibility(View.VISIBLE);
        } else {
            homeworkList.setVisibility(View.INVISIBLE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText("Todo List Not Available");
        }


    }
}
