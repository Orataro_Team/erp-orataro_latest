package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;
import java.util.HashMap;

public class AtteandanceModel implements Serializable {

    String MemberID, FullName;
    HashMap<String, String> map;
    String rollNo;
    String StudentAttRegMasterID, DateOfAttendence, AttencenceType_Term;
    boolean IsWorkingDay, isWorkingStudent;

    public boolean isWorkingStudent() {
        return isWorkingStudent;
    }

    public boolean isWorkingDay() {
        return IsWorkingDay;
    }

    public void setWorkingDay(boolean workingDay) {
        IsWorkingDay = workingDay;
    }

    public void setWorkingStudent(boolean workingStudent) {
        isWorkingStudent = workingStudent;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getAttencenceType_Term() {
        return AttencenceType_Term;
    }

    public void setAttencenceType_Term(String attencenceType_Term) {
        AttencenceType_Term = attencenceType_Term;
    }

    public String getStudentAttRegMasterID() {
        return StudentAttRegMasterID;
    }

    public void setStudentAttRegMasterID(String studentAttRegMasterID) {
        StudentAttRegMasterID = studentAttRegMasterID;
    }

    public String getDateOfAttendence() {
        return DateOfAttendence;
    }

    public void setDateOfAttendence(String dateOfAttendence) {
        DateOfAttendence = dateOfAttendence;
    }

    public boolean isIsWorkingDay() {
        return IsWorkingDay;
    }

    public void setIsWorkingDay(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("true")) {
                IsWorkingDay = true;
            } else {
                IsWorkingDay = false;

            }
        } else {
            IsWorkingDay = false;
        }
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "AtteandanceModel{" +
                "MemberID='" + MemberID + '\'' +
                ", FullName='" + FullName + '\'' +
                ", map=" + map +
                ", rollNo='" + rollNo + '\'' +
                ", StudentAttRegMasterID='" + StudentAttRegMasterID + '\'' +
                ", DateOfAttendence='" + DateOfAttendence + '\'' +
                ", AttencenceType_Term='" + AttencenceType_Term + '\'' +
                ", IsWorkingDay=" + IsWorkingDay +
                '}';
    }
}
