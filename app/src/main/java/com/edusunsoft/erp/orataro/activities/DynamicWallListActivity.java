package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.CheckedStudentListAdapter;
import com.edusunsoft.erp.orataro.adapter.WallListAdapter;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.model.CheckedStudentModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DynamicWallListActivity extends AppCompatActivity {


    private Context mContext = DynamicWallListActivity.this;
    public TextView txt_no_data_found;
    ListView ls_dynamic_wall_list;
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dynamic_wall_list_activity);

        Initialization();

    }

    private void Initialization() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Select Wall");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

        ls_dynamic_wall_list = (ListView) findViewById(R.id.ls_dynamic_wall_list);

        txt_no_data_found = (TextView) findViewById(R.id.txt_no_data_found);

        WallListAdapter adapter = new WallListAdapter(mContext, Utility.dynamicwalllist, FacebookWallFragment2.from, true);
        ls_dynamic_wall_list.setAdapter(adapter);

    }

}
