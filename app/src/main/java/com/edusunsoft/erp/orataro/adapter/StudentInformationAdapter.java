package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.StudentModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

public class StudentInformationAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<StudentModel> list;
    private LayoutInflater layoutInfalater;

    public StudentInformationAdapter(Context mContext, ArrayList<StudentModel> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.userinformation, parent, false);
        ImageView iv_std = (ImageView) convertView.findViewById(R.id.iv_std);
        ImageView img_div = (ImageView) convertView.findViewById(R.id.img_div);
        TextView tv_schoolname = (TextView) convertView.findViewById(R.id.tv_schoolname);
        ImageView iv_std_icon = (ImageView) convertView.findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) convertView.findViewById(R.id.tv_std_name);
        TextView tv_std_division = (TextView) convertView.findViewById(R.id.tv_std_division);
        TextView tv_std_standard = (TextView) convertView.findViewById(R.id.tv_std_standard);
        TextView tv_fathername = (TextView) convertView.findViewById(R.id.tv_fathername);
        TextView tv_attendance = (TextView) convertView.findViewById(R.id.tv_attendance);
        TextView tv_attendance_absant = (TextView) convertView.findViewById(R.id.tv_attendance_absant);
        tv_fathername.setVisibility(View.VISIBLE);
        tv_attendance.setVisibility(View.VISIBLE);
        tv_std_name.setText("" + list.get(position).getFullname());
        tv_fathername.setText(Html.fromHtml("" + list.get(position).getFFullName()));
        tv_schoolname.setText(Html.fromHtml("" + list.get(position).getFContactNo()));
        tv_std_standard.setText(Html.fromHtml("" + list.get(position).getMFullName()));
        tv_std_division.setText(Html.fromHtml("" + list.get(position).getMContactNo()));
        tv_attendance.setText(Html.fromHtml("" + list.get(position).getPresent() + " Days"));
        tv_attendance_absant.setText("" + list.get(position).getAbsent() + "Days");

        try {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL
                            + list.get(position).getProfilePic()
                            .replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/"))
                    .apply(options)
                    .into(iv_std_icon);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

}
