package com.edusunsoft.erp.orataro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.GroupMemberInterface;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.DivisionAdapter;
import com.edusunsoft.erp.orataro.adapter.GroupMemberAdapter;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.database.GroupListModel;
import com.edusunsoft.erp.orataro.model.DivisionModel;
import com.edusunsoft.erp.orataro.model.GetProjectTypeModel;
import com.edusunsoft.erp.orataro.model.GroupMemberModel;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

/**
 * Create schoolgroups
 *
 * @author admin
 */
@SuppressLint("ResourceAsColor")
public class Create_Group_Activity extends Activity implements OnClickListener, GroupMemberInterface, ResponseWebServices {


    Context mContext;
    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;

    private String fielPath = "";
    private byte[] byteArray = null;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;


    protected boolean isFirstTimeGroupMember;
    private ArrayList<GroupMemberModel> localAddGroupMemberModels;
    private GroupMemberAdapter GroupmemberAdapter;
    private ArrayList<GroupMemberModel> selectedSkillList;
    private ArrayList<GroupMemberModel> tempGroupMemberList;
    protected ArrayList<GroupMemberModel> jobAllGroupMembers;
    private boolean isEdit = false;
    EditText edt_title, edt_subject, edt_about;
    CheckBox chb_pendingmember, chb_post, chb_pendingalbum, chb_attechment, chb_poll;
    ImageView img_profile;
    private ArrayList<GetProjectTypeModel> getProjectTypes;
    ArrayList<String> strings = new ArrayList<String>();
    Spinner spnGroupType;
    TextView txtgroupmember, txtgroupstudent;
    GroupListModel groupModel;
    LinearLayout ll_groupmember;

    String groupMemberIdStr = "";
    String groupMemberStudentStr = "";
    boolean isteacher = true;
    GroupListModel groupmodel;
    ArrayList<GroupMemberModel> groupMemberList, groupMemberStudent;
    ArrayAdapter<String> spnAdapter;
    ImageView selectall;

    private Uri fileUri;

    RadioButton rbTeacher, rbStudent;
    RadioGroup rbStudentTeacherGroup;
    boolean isTeacher = true;
    String gradeIdStr = "", divisionIdStr = "";
    boolean isStandard = true;

    String FilteredMemberList = "";
    Bitmap photoBitmap = null;
    LinearLayout ll_save;

    String Path = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_group);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = Create_Group_Activity.this;

        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        txtHeader = (TextView) findViewById(R.id.header_text);
        txtgroupstudent = (TextView) findViewById(R.id.txtgroupstudent);
        txtgroupmember = (TextView) findViewById(R.id.txtgroupteacher);
        edt_title = (EditText) findViewById(R.id.edt_title);
        edt_subject = (EditText) findViewById(R.id.edt_subject);
        edt_about = (EditText) findViewById(R.id.edt_about);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        spnGroupType = (Spinner) findViewById(R.id.spn_grouptype);
        ll_groupmember = (LinearLayout) findViewById(R.id.ll_groupmember);
        chb_pendingmember = (CheckBox) findViewById(R.id.chb_pendingmember);
        chb_post = (CheckBox) findViewById(R.id.chb_post);
        chb_pendingalbum = (CheckBox) findViewById(R.id.chb_pendingalbum);
        chb_attechment = (CheckBox) findViewById(R.id.chb_attechment);
        chb_poll = (CheckBox) findViewById(R.id.chb_poll);
        rbStudentTeacherGroup = (RadioGroup) findViewById(R.id.rbStudentTeacherGroup);

        rbTeacher = (RadioButton) findViewById(R.id.rbTeacher);
        rbStudent = (RadioButton) findViewById(R.id.rbStudent);

        try {

            if (getIntent() != null) {
                isEdit = getIntent().getBooleanExtra("isEdit", false);
                if (isEdit) {
                    txtHeader.setText(getResources().getString(R.string.EditGroup) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
                    groupModel = (GroupListModel) getIntent().getSerializableExtra("model");
                    if (Utility.isNetworkAvailable(mContext)) {
                        getgroupData();
                        parsestdgroup(Utility.readFromFile(groupModel.getGroupId(), mContext));
                    } else {
                        parsestdgroup(Utility.readFromFile(groupModel.getGroupId(), mContext));
                    }

                    if (Utility.isNetworkAvailable(mContext)) {
                        getWallMember();
                        parsewallmember(Utility.readFromFile(ServiceResource.WALLMEMBERS_METHODNAME, mContext));
                    } else {
                        parsewallmember(Utility.readFromFile(ServiceResource.WALLMEMBERS_METHODNAME, mContext));
                    }

                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        if (Utility.isNetworkAvailable(mContext)) {
            getProjectType();
            parseProjecttype(Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext));
        } else {
            parseProjecttype(Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext));
        }
        if (isEdit) {
            txtHeader.setText(getResources().getString(R.string.EditGroup) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
        } else {
            txtHeader.setText(mContext.getResources().getString(R.string.creategroup) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
        }
        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setImageResource(R.drawable.save);
        imgRightHeader.setVisibility(View.VISIBLE);

        Log.d("getuserteacher_int", String.valueOf(ServiceResource.USER_TEACHER_INT));
        Log.d("getuserteacher_int23", String.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getUserType()));

        if (ServiceResource.USER_TEACHER_INT != new UserSharedPrefrence(mContext).getLoginModel().getUserType()) {

            txtHeader.setText(getResources().getString(R.string.GroupDetail));
            imgRightHeader.setVisibility(View.INVISIBLE);
            ll_save.setVisibility(View.GONE);
            imgRightHeader.setEnabled(false);
            rbStudentTeacherGroup.setVisibility(View.GONE);
            txtgroupmember.setVisibility(View.GONE);
            edt_title.setEnabled(false);
            edt_subject.setEnabled(false);
            edt_about.setEnabled(false);
            img_profile.setEnabled(false);
            spnGroupType.setEnabled(false);
            chb_attechment.setEnabled(false);
            chb_pendingalbum.setEnabled(false);
            chb_pendingmember.setEnabled(false);
            chb_poll.setEnabled(false);
            chb_post.setEnabled(false);

        }

        if (Utility.isNetworkAvailable(mContext)) {
            getWallAllTeacher();
            parsewallmember(Utility.readFromFile(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, mContext));
        } else {
            parsewallmember(Utility.readFromFile(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, mContext));
        }

        if (Utility.isNetworkAvailable(mContext)) {
            StandardList();
            parsestsandard(Utility.readFromFile(ServiceResource.STANDERD_METHODNAME, mContext));
        } else {
            parsestsandard(Utility.readFromFile(ServiceResource.STANDERD_METHODNAME, mContext));
        }

        img_profile.setOnClickListener(this);
        txtgroupmember.setOnClickListener(this);
        imgRightHeader.setOnClickListener(this);
        imgLeftHeader.setOnClickListener(this);
        txtgroupstudent.setOnClickListener(this);
        ll_save.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (R.id.img_profile == v.getId()) {
            selectImage(1);
        }
        if (R.id.img_menu == v.getId()) {
            if (Utility.isNetworkAvailable(mContext)) {

                Validation();

            } else {
                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }
        }
        if (R.id.ll_save == v.getId()) {

            if (Utility.isNetworkAvailable(mContext)) {

                Validation();

            } else {
                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }
        }
        if (R.id.img_home == v.getId()) {
            Utility.ISLOADGROUP = false;
            finish();
        }
        if (R.id.txtgroupteacher == v.getId()) {
            isteacher = true;
            showListDialogForGroupMember(mContext, groupMemberList);
        }
        if (R.id.img_home == v.getId()) {
            finish();
        }
        if (v.getId() == R.id.txtgroupstudent) {

            isStandard = true;
            isteacher = false;
            showStanderdPopup();

        }
    }

    private void Validation() {

        if (edt_title.getText().toString().trim().equalsIgnoreCase("")) {

            SetError(edt_title, "This Field is Required");

        } else if (edt_subject.getText().toString().trim().equalsIgnoreCase("")) {

            SetError(edt_subject, "This Field is Required");

        } else {

            CreateGroup();

        }

    }

    private void SetError(EditText edt_title, String message) {

        edt_title.setFocusable(true);
        edt_title.setError(message);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADGROUP = false;
    }

    /**
     * get sppiner data from webservice
     */
    public void getProjectType() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY, "GroupType"));
        Log.d("getGroupType", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.LOGIN_GETPROJECTTYPE, ServiceResource.LOGIN_URL);

    }

    /**
     * get member list
     */
    public void getWallMember() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));

        Log.d("getMemberRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.WALLMEMBERS_METHODNAME,
                ServiceResource.GROUP_URL);

    }

    /**
     * get all teacher list
     */
    public void getWallAllTeacher() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, ServiceResource.FRIENDS_URL);
    }

    /**
     * get all student list
     */
    public void getWallAllStudentList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeIdStr));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, divisionIdStr));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETSTUDENTSLISTNAMEANDMEMBERID_METHODNAME, ServiceResource.FRIENDS_URL);
    }

    /**
     * get particulaR schoolgroups data
     */
    public void getgroupData() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GROUPID, groupModel.getGroupId()));

        Log.d("getgroupdata", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.STUGROUP_METHODNAME, ServiceResource.GROUP_URL);

    }

    /**
     * crate schoolgroups webservice
     */
    public void CreateGroup() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.GROUPID, groupmodel.getGroupId()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GROUPID, null));
        }

        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.GROUP_ABOUTGROUP_PARAM, edt_about.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.GROUP_SUBJECT_PARAM, edt_subject.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.GROUP_TITLE_PARAM, edt_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.GROUPTYPEID_PARAM, getProjectTypes.get(spnGroupType.getSelectedItemPosition()).getTermID()));
        arrayList.add(new PropertyVo(ServiceResource.ISAUTOAPPROVEPENDINGMEMBER, chb_pendingmember.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISAUTOAPPROVEPENDINGPOST, chb_post.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISAUTOAPPROVEPENDINGALBUMS, chb_pendingalbum.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISAUTOAPPROVEPENDINGATTACHMENT, chb_attechment.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISAUTOAPPROVEPENDINGPOLLS, chb_poll.isChecked()));

        groupMemberIdStr = groupMemberStudentStr + "," + groupMemberIdStr;

        List<String> list = Arrays.asList(groupMemberIdStr.split(","));

        if (list.contains(new UserSharedPrefrence(mContext).getLoginModel().getMemberID())) {

            FilteredMemberList = groupMemberIdStr.replace((new UserSharedPrefrence(mContext).getLoginModel().getMemberID()), "");

        } else {

            FilteredMemberList = groupMemberIdStr;

        }


        arrayList.add(new PropertyVo(ServiceResource.ALLGROUPMEMBERS, FilteredMemberList));

        if (Utility.BASE64_STRING != null) {

            arrayList.add(new PropertyVo(ServiceResource.FILE, Utility.BASE64_STRING));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME, new File(Utility.NewFileName).getName()));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.FILE, null));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME, null));

        }


        Log.d("creategroupRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SAVEUPDATE_GROUP_METHODNAME, ServiceResource.GROUP_URL);

    }

    /**
     * show dialog fro schoolgroups member
     *
     * @param context
     * @param AllMemberList
     */

    public void showListDialogForGroupMember(Context context, ArrayList<GroupMemberModel> AllMemberList) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customelistdialog);

        TextView dialogTitle = (TextView) dialog
                .findViewById(R.id.dialog_title);

        selectall = (ImageView) dialog
                .findViewById(R.id.selectall);
        selectall.setVisibility(View.VISIBLE);


        final EditText edtSearch = (EditText) dialog.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        ListView lv = (ListView) dialog.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialog.findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialog.findViewById(R.id.custome_Dialog_cancle_btn);
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(mContext.getResources().getString(R.string.selectgroupmember));

        if (isEdit) {

            localAddGroupMemberModels = reSetAllGroupMember(AllMemberList);

            if (localAddGroupMemberModels != null && localAddGroupMemberModels.size() > 0) {
                GroupmemberAdapter = new GroupMemberAdapter(mContext, modifyGroupMemberVo(
                        localAddGroupMemberModels, selectedSkillList), Create_Group_Activity.this);
                lv.setAdapter(GroupmemberAdapter);
                GroupmemberAdapter.notifyDataSetChanged();
            }

        } else {

            localAddGroupMemberModels = reSetAllGroupMember(AllMemberList);

            if (localAddGroupMemberModels != null && localAddGroupMemberModels.size() > 0) {
                if (isFirstTimeGroupMember) {
                    GroupmemberAdapter = new GroupMemberAdapter(mContext,
                            modifyGroupMemberVo(localAddGroupMemberModels, new ArrayList<GroupMemberModel>()), Create_Group_Activity.this);
                } else {
                    GroupmemberAdapter = new GroupMemberAdapter(mContext, modifyGroupMemberVo(localAddGroupMemberModels, tempGroupMemberList), Create_Group_Activity.this);
                }

                lv.setAdapter(GroupmemberAdapter);
                GroupmemberAdapter.notifyDataSetChanged();

            }

        }

        // select all functionality to select All Student simultaneously
        selectall.setOnClickListener(v -> {

            GroupmemberAdapter.SelectAll(localAddGroupMemberModels, selectall);

        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (GroupmemberAdapter != null) {
                    GroupmemberAdapter.filter(text);
                }
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                edtSearch.setText("");
                if (isteacher) {
                    ArrayList<GroupMemberModel> selectedGroupMemberList = new ArrayList<GroupMemberModel>();
                    selectedGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    tempGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    if (getSelectedGroupMember(jobAllGroupMembers) == null) {
                    }
                    isFirstTimeGroupMember = false;
                    String selectedGroupMemberStr = "";
                    groupMemberIdStr = "";
                    if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                        Collections.sort(selectedGroupMemberList, new CustomComparator());
                        for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                            selectedGroupMemberStr = selectedGroupMemberStr
                                    + selectedGroupMemberList.get(i).getMemberName();
                            groupMemberIdStr = groupMemberIdStr +
                                    selectedGroupMemberList.get(i).getMemberId();

                            Log.d("groupmemberIdlist", groupMemberIdStr);

                            if (selectedGroupMemberList.size() > 1) {
                                if (i != selectedGroupMemberList.size() - 1) {
                                    selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                    groupMemberIdStr = groupMemberIdStr + ",";

                                    Log.d("groupmemberIdlist123", groupMemberIdStr);

                                }
                            }
                        }
                    }
                    txtgroupmember.setText(selectedGroupMemberStr);
                } else {
                    ArrayList<GroupMemberModel> selectedGroupMemberList = new ArrayList<GroupMemberModel>();
                    selectedGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    tempGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    if (getSelectedGroupMember(jobAllGroupMembers) == null) {
                        //					selectedGroupMemberList = selectedSkillList;
                    }
                    isFirstTimeGroupMember = false;
                    String selectedGroupMemberStr = "";
                    groupMemberStudentStr = "";
                    if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                        Collections.sort(selectedGroupMemberList, new CustomComparator());
                        for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                            selectedGroupMemberStr = selectedGroupMemberStr
                                    + selectedGroupMemberList.get(i).getMemberName();
                            groupMemberStudentStr = groupMemberStudentStr +
                                    selectedGroupMemberList.get(i).getMemberId();

                            Log.d("groupMemberstr", groupMemberStudentStr);
                            if (selectedGroupMemberList.size() > 1) {

                                if (i != selectedGroupMemberList.size() - 1) {
                                    selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                    groupMemberStudentStr = groupMemberStudentStr + ",";

                                    Log.d("groupMemberstr12", groupMemberStudentStr);


                                }

                            }
                        }
                    }

                    txtgroupstudent.setText(selectedGroupMemberStr);
                }

            }

        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }

        });

        dialog.show();

    }

    /**
     * set selected false for all schoolgroups member
     * reset all schoolgroups member
     *
     * @param addGroupMemberModels
     * @return
     */

    public ArrayList<GroupMemberModel> reSetAllGroupMember(

            ArrayList<GroupMemberModel> addGroupMemberModels) {

        ArrayList<GroupMemberModel> groupMemberModels = null;

        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<GroupMemberModel>();
            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                addGroupMemberModels.get(i).setSelected(false);
                groupMemberModels.add(addGroupMemberModels.get(i));
            }
        }

        return groupMemberModels;
    }

    /**
     * modify set selected true selected schoolgroups member
     *
     * @param allGroupMember
     * @param selectedGroupMember
     * @return
     */
    public ArrayList<GroupMemberModel> modifyGroupMemberVo(
            ArrayList<GroupMemberModel> allGroupMember,
            ArrayList<GroupMemberModel> selectedGroupMember) {
        ArrayList<GroupMemberModel> addSkillModels = allGroupMember;

        if (selectedGroupMember != null && selectedGroupMember.size() > 0) {
            if (allGroupMember != null && allGroupMember.size() > 0) {
                for (int j = 0; j < selectedGroupMember.size(); j++) {
                    for (int i = 0; i < allGroupMember.size(); i++) {
                        if (selectedGroupMember.get(j).getMemberId().equals(allGroupMember.get(i).getMemberId())) {
                            allGroupMember.remove(i);
                            break;
                        }
                    }
                }
            }
        } else {
            return allGroupMember;
        }

        return addSkillModels;
    }


    /**
     * getselectedgroupmember list
     *
     * @param addGroupMemberModels
     * @return
     */

    public ArrayList<GroupMemberModel> getSelectedGroupMember(
            ArrayList<GroupMemberModel> addGroupMemberModels) {
        ArrayList<GroupMemberModel> groupMemberModels = null;
        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<GroupMemberModel>();

            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                if (addGroupMemberModels.get(i).isSelected()) {
                    groupMemberModels.add(addGroupMemberModels.get(i));
                }
            }
        }

        return groupMemberModels;

    }

    /**
     * GroupMemberModel sort from data
     *
     * @author admin
     */
    public class CustomComparator implements Comparator<GroupMemberModel> {
        @Override
        public int compare(GroupMemberModel o1, GroupMemberModel o2) {
            return o1.getMemberName().compareTo(o2.getMemberName());
        }
    }

    /**
     * get all groupmember list
     */
    @Override
    public void getAllGroupMember(int pos,
                                  ArrayList<GroupMemberModel> addGroupMemberModels) {
        jobAllGroupMembers = addGroupMemberModels;
    }
    /*
     * (non-Javadoc)
     * @see ResponseWebServices#response(java.lang.String, java.lang.String)
     */

    /**
     * get response all webservice
     */
    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.STANDERD_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parsestsandard(result);
        }

        if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {

            Log.d("getprojecttype", result);
            Utility.writeToFile(result, methodName, mContext);
            parseProjecttype(result);

        } else if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {


                } else {

                    hJsonArray = new JSONArray(result);
                    Global.StandardModels = new ArrayList<StandardModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        StandardModel model = new StandardModel();
                        model.setStandardName(hJsonObject
                                .getString(ServiceResource.GRADE));
                        model.setStandrdId(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADEID));
                        model.setDivisionId(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONID));
                        model.setDivisionName(hJsonObject
                                .getString(ServiceResource.DIVISION));
                        model.setSubjectId(hJsonObject
                                .getString(ServiceResource.STANDARD_SUBJECTID));
                        model.setSubjectName(hJsonObject
                                .getString(ServiceResource.SUBJECT));
                        Global.StandardModels.add(model);

                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }


        } else if (ServiceResource.STUGROUP_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, groupModel.getGroupId(), mContext);
            parsestdgroup(result);
        } else if (ServiceResource.WALLMEMBERS_METHODNAME.equalsIgnoreCase(methodName)
                || ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)
        ) {
            Utility.writeToFile(result, methodName, mContext);
            parsewallmember(result);
        } else if (ServiceResource.GETSTUDENTSLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                //				JSONArray hJsonArray = new JSONArray(examresult);
                //				JSONObject jsonObj = new JSONObject(examresult);
                JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
                groupMemberStudent = new ArrayList<GroupMemberModel>();
                GroupMemberModel model;
                //		 model = new  GroupMemberModel();
                //						 model.setMemberId("bvdbvd");
                //						 model.setMemberName("Pdsf");
                //						 groupMemberList.add(model);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    model = new GroupMemberModel();

                    model.setMemberId(hJsonObject
                            .getString(ServiceResource.MEMBERID));
                    //					model.setFriendID(hJsonObject
                    //							.getString(ServiceResource.FRIENDLISTID));

                    model.setMemberName(hJsonObject
                            .getString(ServiceResource.FULLNAMEPARAM));
                    //					model.setProfilePicture(ServiceResource.BASE_IMG_URL_WITHOUT+hJsonObject
                    //							.getString(ServiceResource.PROFILEPICTURE));
                    //					model.setWallID(hJsonObject
                    //							.getString(ServiceResource.WALLIDPARAM));
                    //
                    //
                    //					if(hJsonObject
                    //							.getString(ServiceResource.ISALLOWPEOPLETOSENDYOUREQUEST).equalsIgnoreCase("true")){
                    //						model.setIsAllowPeopleToSendYouRequest(true);
                    //					}else{
                    //						model.setIsAllowPeopleToSendYouRequest(false);
                    //					}
                    groupMemberStudent.add(model);

                }

            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            showListDialogForGroupMember(mContext, groupMemberStudent);


        } else if (ServiceResource.SAVEUPDATE_GROUP_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("getResponse", result);
            Utility.ISLOADGROUP = true;
            finish();

//            if (Utility.isTeacher(mContext)) {
//
//                if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {
//
//                    Intent intent = new Intent(mContext, SchoolGroupActivity.class);
//                    intent.putExtra("isFrom", ServiceResource.SCHOOLGROUP_FLAG);
//                    startActivity(intent);
//                    Create_Group_Activity.this.finish();
//
//                } else {
//
//                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
//
//                }
//
//            }

        } else if (ServiceResource.REMOVE_GROUPMEMBERS_METHODNAME.equalsIgnoreCase(methodName)) {
            getgroupData();
        }


    }

    private void SelectAll(ArrayList<GroupMemberModel> groupMemberStudent) {

        for (int i = 0; i < groupMemberStudent.size(); i++) {


            if (groupMemberStudent.get(i).isSelected() == true) {
                groupMemberStudent.get(i).setSelected(false);
                selectall.setImageDrawable(getResources().getDrawable(R.drawable.untick_icon));
            } else {
                groupMemberStudent.get(i).setSelected(true);
                selectall.setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
            }

        }

    }

    private void parseProjecttype(String result) {
        // TODO Auto-generated method stub


        JSONArray hJsonArray;
        try {


            if (result.contains("\"success\":0")) {

                // Toast.makeText(mContext,
                // getResources().getString(R.string.no_data), 0)
                // .show();

            } else {

                hJsonArray = new JSONArray(result);
                getProjectTypes = new ArrayList<GetProjectTypeModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    GetProjectTypeModel model = new GetProjectTypeModel();
                    model.setCategory(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
//                    model.setDefaultValue(hJsonObject
//                            .getString(ServiceResource.GETPROJECTTYPE_DEFAULTVALUE));
                    model.setIsDefault(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                    model.setOrderNo(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                    model.setTerm(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_TERM));
                    model.setTermID(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_TERMID));

                    getProjectTypes.add(model);

                }

            }

        } catch (JSONException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        // TODO Auto-generated method stub
        ArrayList<GetProjectTypeModel> getProjectType_models = new ArrayList<GetProjectTypeModel>();

        if (getProjectTypes != null && getProjectTypes.size() > 0) {
            for (int i = 0; i < getProjectTypes.size(); i++) {
                if (getProjectTypes.get(i).getCategory().equals("GroupType")) {
                    getProjectType_models.add(getProjectTypes.get(i));
                }
            }
        }


        if (getProjectType_models != null && getProjectType_models.size() > 0) {

            strings.clear();

            for (int i = 0; i < getProjectType_models.size(); i++) {
                strings.add(getProjectType_models.get(i).getTerm());
            }

        }

        if (strings != null && strings.size() > 0) {

            //				 Spinner spinner = (Spinner) findViewById(R.id.spinner);

            spnAdapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);

            spnAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //					Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            spnGroupType.setAdapter(spnAdapter);


        }


    }


    private void parsestdgroup(String result) {

        Log.d("parsegroupresponse", result);

        JSONArray hJsonArray, memberArray;

        try {

            JSONObject jsonObj = new JSONObject(result);
            hJsonArray = jsonObj.getJSONArray(ServiceResource.TABLE);
            memberArray = jsonObj.optJSONArray(ServiceResource.TABLE1);

            for (int i = 0; i < hJsonArray.length(); i++) {

                JSONObject innerObj = hJsonArray.getJSONObject(i);
                groupmodel = new GroupListModel();
                groupmodel.setGroupId(innerObj
                        .getString(ServiceResource.GROUP_ID));
                groupmodel.setGroupName(innerObj
                        .getString(ServiceResource.GROUP_TITLE));
                groupmodel.setGroupSubject(innerObj
                        .getString(ServiceResource.GROUP_SUBJECT));
                groupmodel.setGroupstatus(innerObj
                        .getString(ServiceResource.GROUP_ABOUTGROUP));
                groupmodel.setGroupImage(innerObj
                        .getString(ServiceResource.GROUP_IMAGE));
                groupmodel.setGroupImage(innerObj.getString(ServiceResource.GROUP_IMAGE));

                groupmodel.setGroupTypeID(innerObj
                        .getString(ServiceResource.GroupTypeID));

                groupmodel.setIsAutoApprovePendingMember(stringToboolean(innerObj
                        .getString(ServiceResource.ISAUTOAPPROVEPENDINGMEMBER)));
                groupmodel.setIsAutoApprovePendingPost(stringToboolean(innerObj
                        .getString(ServiceResource.ISAUTOAPPROVEPENDINGPOST)));
                groupmodel.setIsAutoApprovePendingPolls(stringToboolean(innerObj
                        .getString(ServiceResource.ISAUTOAPPROVEPENDINGPOLLS)));
                groupmodel.setIsAutoApprovePendingAttachment(stringToboolean(innerObj
                        .getString(ServiceResource.ISAUTOAPPROVEPENDINGATTACHMENT)));
                groupmodel.setIsAutoApprovePendingAlbums(stringToboolean(innerObj
                        .getString(ServiceResource.ISAUTOAPPROVEPENDINGALBUMS)));


            }

            if (memberArray != null && memberArray.length() > 0) {

                selectedSkillList = new ArrayList<GroupMemberModel>();

                for (int i = 0; i < memberArray.length(); i++) {
                    JSONObject innerObj = memberArray.getJSONObject(i);
                    GroupMemberModel model = new GroupMemberModel();
                    model.setGropuMemberID(innerObj
                            .getString(ServiceResource.GROPUMEMBERID));
                    model.setMemberId(innerObj
                            .getString(ServiceResource.MEMBERID));
                    model.setMemberName(innerObj
                            .getString(ServiceResource.MEMBERFULLNAME));
                    model.setMemberProfileCode(innerObj
                            .getString(ServiceResource.MEMBERPROFILECODE));
                    model.setProfilePicture(innerObj
                            .getString(ServiceResource.GROUP_PROFILEPICTURE));
                    model.setMemberRole(innerObj
                            .getString(ServiceResource.MEMBERROLE));

                    selectedSkillList.add(model);

                    Log.d("memberlist1234", selectedSkillList.toString());
                }

            } else {

                selectedSkillList = new ArrayList<GroupMemberModel>();

            }

        } catch (JSONException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        fillData();
        viewGroupMember();


    }


    private void parsestsandard(String result) {

        if (isStandard) {

            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);
                    Global.StandardModels = new ArrayList<StandardModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        StandardModel model = new StandardModel();
                        model.setStandardName(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADENAME));
                        model.setStandrdId(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADEID));
                        Global.StandardModels.add(model);

                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (Global.StandardModels != null
                    && Global.StandardModels.size() > 0) {


                if (Global.StandardModels.size() == 1) {
                } else {

                }

            } else {

            }

        } else {

            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);
                    Global.divisionModels = new ArrayList<DivisionModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {

                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        DivisionModel model = new DivisionModel();
                        model.setDivisionId(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONID));
                        model.setDivisionName(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONNAME));
                        Global.divisionModels.add(model);

                    }

                }

            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            showStanderdPopup();

        }

    }


    private void parsewallmember(String result) {

        Log.d("parsewallmember", result);

        try {
            //				JSONArray hJsonArray = new JSONArray(examresult);
            //				JSONObject jsonObj = new JSONObject(examresult);
            JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
            groupMemberList = new ArrayList<GroupMemberModel>();
            GroupMemberModel model;
            //		 model = new  GroupMemberModel();
            //						 model.setMemberId("bvdbvd");
            //						 model.setMemberName("Pdsf");
            //						 groupMemberList.add(model);

            for (int i = 0; i < hJsonArray.length(); i++) {
                JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                model = new GroupMemberModel();

                model.setMemberId(hJsonObject
                        .getString(ServiceResource.MEMBERID));
                //					model.setFriendID(hJsonObject
                //							.getString(ServiceResource.FRIENDLISTID));

                model.setMemberName(hJsonObject
                        .getString(ServiceResource.FULLNAMEPARAM));
                //					model.setProfilePicture(ServiceResource.BASE_IMG_URL_WITHOUT+hJsonObject
                //							.getString(ServiceResource.PROFILEPICTURE));
                //					model.setWallID(hJsonObject
                //							.getString(ServiceResource.WALLIDPARAM));
                //
                //
                //					if(hJsonObject
                //							.getString(ServiceResource.ISALLOWPEOPLETOSENDYOUREQUEST).equalsIgnoreCase("true")){
                //						model.setIsAllowPeopleToSendYouRequest(true);
                //					}else{
                //						model.setIsAllowPeopleToSendYouRequest(false);
                //					}
                groupMemberList.add(model);


            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Utility.getUserModelData(mContext);


                try {
                    fielPath = data.getExtras().getString("imageData_uri");
                    Path = Utility.ImageCompress(fielPath, mContext);
                    Utility.GetBASE64STRING(Path);
                } catch (Exception e) {

                    e.printStackTrace();

                }

                /*commented By Krishna : 06-02-2019 convert Filepath to Bitmap amd Display Bitmap into Image*/

                byteArray = null;
                byteArray = data.getExtras().getByteArray("imageData_byte");


                try {
                    GetBitmapFromFilePath(fielPath);
                    img_profile.setImageBitmap(photoBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == SELECT_FILE) {

                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");

                if (imgList != null && imgList.size() > 0) {

                    try {

                        fielPath = imgList.get(0).getUri().toString();
                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                        Path = Utility.ImageCompress(fielPath, mContext);

                        Utility.GetBASE64STRING(Path);

                        File file = new File(fielPath);
                        File compressedImageFile = null;

                        try {
                            compressedImageFile = new Compressor(this).compressToFile(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                        Path = Utility.ImageCompress(filePath, mContext);
                        Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                        /*END*/
                        GetBitmapFromFilePath(fielPath);
                        img_profile.setImageBitmap(photoBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

        }

    }

    private Bitmap GetBitmapFromFilePath(String fielPath) {

        photoBitmap = Utility.getBitmap(fielPath, mContext);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        if (fielPath.contains(".png") || fielPath.contains(".PNG")) {

            photoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        } else {

            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        }

        byteArray = stream.toByteArray();

        return photoBitmap;

    }


    /**
     * select image from gallary and take picture
     *
     * @param i
     */
    private void selectImage(final int i) {
        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {

                    Intent intent = new Intent(mContext, PreviewImageActivity.class);
                    intent.putExtra("FromIntent", "true");
                    intent.putExtra("RequestCode", 100);
                    startActivityForResult(intent, REQUEST_CAMERA);
                    //					Intent intent = new Intent(Create_Group_Activity.this,CameraActivity.class);
                    //					startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {
                    Intent intent = new Intent(Create_Group_Activity.this, ImageSelectionActivity.class);
                    intent.putExtra("count", 1);
                    startActivityForResult(intent, SELECT_FILE);


                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    /**
     * it convert true or false string to true or false boolean
     *
     * @param str
     * @return
     */
    public boolean stringToboolean(String str) {

        if (str.equalsIgnoreCase("true")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * filldata edit schoolgroups data
     */
    public void fillData() {

        try {

            Log.d("getgroupimage", groupmodel.getGroupImage());
            edt_title.setText(groupmodel.getGroupName());
            edt_subject.setText(groupmodel.getGroupSubject());
            edt_about.setText(groupmodel.getGroupstatus());

            chb_pendingmember.setChecked(groupmodel.isIsAutoApprovePendingMember());
            chb_pendingalbum.setChecked(groupmodel.isIsAutoApprovePendingAlbums());
            chb_attechment.setChecked(groupmodel.isIsAutoApprovePendingAttachment());
            chb_poll.setChecked(groupmodel.isIsAutoApprovePendingPolls());
            chb_post.setChecked(groupmodel.isIsAutoApprovePendingPost());

            try {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();


                Picasso.get().load(ServiceResource.BASE_IMG_URL +
                        groupModel.getGroupImage()
                                .replace("//DataFiles//", "/DataFiles/")
                                .replace("//DataFiles/", "/DataFiles/")).placeholder(R.drawable.photo).
                        into(img_profile);

//                Glide.with(mContext)
//                        .load(ServiceResource.BASE_IMG_URL + groupModel.getGroupImage())
//                        .apply(options)
//                        .into(img_profile);

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (getProjectTypes != null && getProjectTypes.size() > 0) {
                for (int i = 0; i < getProjectTypes.size(); i++) {
                    if (getProjectTypes.get(i).getTermID().equalsIgnoreCase(groupmodel.getGroupTypeID())) {
                        int spinnerPosition = spnAdapter.getPosition(getProjectTypes.get(i).getTerm());
                        spnGroupType.setSelection(spinnerPosition);
                    }
                }
            }

        } catch (Exception e) {
            Log.v("Error", e.getMessage());
        }

    }

    /**
     * call webservice for view groupmember
     */

    public void viewGroupMember() {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Log.d("viewallmember", String.valueOf(selectedSkillList.toString()));

        if (selectedSkillList != null && selectedSkillList.size() > 0) {

            ll_groupmember.removeAllViews();

            for (int i = 0; i < selectedSkillList.size(); i++) {

                View convertView = layoutInfalater.inflate(R.layout.groupmemberlayout,
                        null);
                ImageView img_profile = (ImageView) convertView.findViewById(R.id.memberprofilepic);
                TextView txtMemberName = (TextView) convertView.findViewById(R.id.txtmembername);
                TextView txtMembertype = (TextView) convertView.findViewById(R.id.txtmembertype);
                final Button btnremove = (Button) convertView.findViewById(R.id.btn_remove);
                btnremove.setTag("" + i);

                try {

                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.photo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(ServiceResource.BASE_IMG_URL1 + selectedSkillList.get(i).getProfilePicture())
                            .apply(options)
                            .into(img_profile);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                txtMemberName.setText(selectedSkillList.get(i).getMemberName());
                txtMembertype.setText(selectedSkillList.get(i).getMemberRole());
                if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
                    btnremove.setVisibility(View.INVISIBLE);
                }

                btnremove.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int pos = Integer.valueOf((String) btnremove.getTag());
                        removeMember(selectedSkillList.get(pos).getGropuMemberID());
                    }

                });

                ll_groupmember.addView(convertView);
            }

        } else {

            ll_groupmember.setVisibility(View.GONE);

        }

    }

    /**
     * webservice call for remove groupmember from id
     *
     * @param groupMemberId
     */

    public void removeMember(String groupMemberId) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.GROUPMEMBERID, groupMemberId));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.REMOVE_GROUPMEMBERS_METHODNAME, ServiceResource.GROUP_URL);
    }

    /**
     * webservice call for standard list
     */

    public void StandardList() {
        isStandard = true;
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.TYPE, ServiceResource.STANDERD_WSCALL_TYPE_GRADE));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, null));

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.STANDERD_METHODNAME, ServiceResource.STANDERD_URL);
    }

    /**
     * get grade list for standard id
     *
     * @param gradeId
     */
    public void gradeList(String gradeId) {
        isStandard = false;
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.TYPE, ServiceResource.STANDERD_WSCALL_TYPE_DIVISION));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, null));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STANDERD_METHODNAME, ServiceResource.STANDERD_URL);
    }

    /**
     * show popup for standard selection
     */
    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(mContext);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);
        TextView dialogTitle = (TextView) dialogStandard.findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);
        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard.findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard.findViewById(R.id.custome_Dialog_cancle_btn);
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(mContext.getResources().getString(R.string.selectgroupmember));
        btnCancle.setVisibility(View.GONE);
        btnDone.setVisibility(View.GONE);
        edtSearch.setVisibility(View.GONE);
        if (isStandard) {
            dialogTitle.setText(mContext.getResources().getString(R.string.selectstandard));
            if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
                StanderdAdapter adapter = new StanderdAdapter(mContext, Global.StandardModels, true);
                lv.setAdapter(adapter);
            }
        } else {

            isStandard = false;
            dialogTitle.setText(mContext.getResources().getString(R.string.selectdivision));
            if (Global.divisionModels != null && Global.divisionModels.size() > 0) {
                DivisionAdapter adapter = new DivisionAdapter(mContext, Global.divisionModels);
                lv.setAdapter(adapter);
            }

        }

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogStandard.dismiss();
                if (isStandard) {
                    gradeIdStr = Global.StandardModels.get(position).getStandrdId();
                    gradeList(Global.StandardModels.get(position).getStandrdId());
                } else {
                    divisionIdStr = Global.divisionModels.get(position).getDivisionId();
                    getWallAllStudentList();
                }

            }

        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        dialogStandard.show();

    }
}


