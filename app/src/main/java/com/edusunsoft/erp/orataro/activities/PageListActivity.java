package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.CMSPageAdapter;
import com.edusunsoft.erp.orataro.adapter.PageAdapter;
import com.edusunsoft.erp.orataro.database.BlogListDataDao;
import com.edusunsoft.erp.orataro.database.BlogListModel;
import com.edusunsoft.erp.orataro.database.CMSPageListDataDao;
import com.edusunsoft.erp.orataro.database.CmsPageListModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.edusunsoft.erp.orataro.util.Utility.RedirectToDashboard;

public class PageListActivity extends Activity implements
        OnItemClickListener, OnClickListener, ResponseWebServices {

    private PullAndLoadListView lv_page;
    Context mContext;
    TextView txt_nodatafound, header_text;
    // String[] actionlist = { "PAGES", "HOMEWORK", "SCHOOL_GROUP",
    // "MY_HAPPYGRAM", "DEVISION", "Grade", "Institute", "Subject" };
    boolean isInformation, isSchoolTiming, isTimetable, isPrayer, isPages;
    int[] iconlist = {R.drawable.pages, R.drawable.homework,
            R.drawable.schoolgroups, R.drawable.myhappygram,
            R.drawable.myhappygram, R.drawable.myhappygram,
            R.drawable.myhappygram, R.drawable.myhappygram};
    ArrayList<BlogListModel> pagesModels = new ArrayList<BlogListModel>();
    ArrayList<CmsPageListModel> cmspagesModels = new ArrayList<CmsPageListModel>();
    PageAdapter adpter;
    CMSPageAdapter cmsPageAdapter;
    private ImageView img_back;
    String from = "NAN";
    boolean isBlog;

    LinearLayout ll_search;
    EditText edtTeacherName;
    ReadWriteSettingModel readwritesettingmodel;
    boolean isRefresh = false;

    // variable declaration for homeworklist from ofline database
    BlogListDataDao blogListDataDao;
    private List<BlogListModel> BlogList = new ArrayList<>();
    BlogListModel blogListModel = new BlogListModel();
    /*END*/


    // variable declaration for homeworklist from ofline database
    CMSPageListDataDao cmsPageListDataDao;
    private List<CmsPageListModel> cmsPageList = new ArrayList<>();
    CmsPageListModel cmsPageListModel = new CmsPageListModel();
    /*END*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_list);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = this;
        lv_page = (PullAndLoadListView) findViewById(R.id.lv_page);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        header_text = (TextView) findViewById(R.id.header_text);

        blogListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).blogListDataDao();
        cmsPageListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).cmsPageListDataDao();
        //		fillPage();
        from = getIntent().getStringExtra("isFrom");

        readwritesettingmodel = Utility.ReadWriteSetting(ServiceResource.BLOG);
        ll_search = (LinearLayout) findViewById(R.id.searchlayout);
        ll_search.setVisibility(View.VISIBLE);

        try {

            if (from != null && from.equalsIgnoreCase(ServiceResource.BLOGLIST)) {
                header_text.setText(getResources().getString(R.string.Blogs) + " (" + Utility.GetFirstName(mContext) + ")");
                header_text.setPadding(0, 0, 10, 0);
                isBlog = true;

                // Display BlogList from offline as well as online
                try {
                    DisplayBlog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/
            } else {
                header_text.setText(getResources().getString(R.string.Pages) + " (" + Utility.GetFirstName(mContext) + ")");
                header_text.setPadding(0, 0, 10, 0);

                // Display BlogList from offline as well as online
                try {
                    DisplayCMSPages();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/
            }
        } catch (Exception e) {
        }

        edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);

        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        lv_page.setOnItemClickListener(this);

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                try {
                    String text = edtTeacherName.getText().toString()
                            .toLowerCase(Locale.getDefault());
                    adpter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });


        lv_page.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {
                try {
                    isRefresh = true;
                    if (from != null && from.equalsIgnoreCase(ServiceResource.BLOGLIST)) {
                        header_text.setText(getResources().getString(R.string.Blogs) + " (" + Utility.GetFirstName(mContext) + ")");
                        isBlog = true;
                        // Display BlogList from offline as well as online
                        try {
                            DisplayBlog();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*END*/
                    } else {
                        header_text.setText(getResources().getString(R.string.Pages) + " (" + Utility.GetFirstName(mContext) + ")");
                        // Display BlogList from offline as well as online
                        try {
                            DisplayCMSPages();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*END*/
                    }
                } catch (Exception e) {

                }

            }
        });

    }

    private void DisplayCMSPages() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            cmsPageList = cmsPageListDataDao.getCMSPageList();
            if (cmsPageList.isEmpty() || cmsPageList.size() == 0 || cmsPageList == null) {
                cmsPages(true);
            } else {
                //set Adapter
                setCMSPagelistAdapter(cmsPageList);
                /*END*/
                cmsPages(false);
            }
        } else {
            cmsPageList = cmsPageListDataDao.getCMSPageList();
            if (cmsPageList != null) {
                setCMSPagelistAdapter(cmsPageList);
            }
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
        /*END*/
    }

    private void setCMSPagelistAdapter(List<CmsPageListModel> cmsPageList) {
        if (cmsPageList != null && cmsPageList.size() > 0) {
            cmspagesModels = new ArrayList<CmsPageListModel>();
            for (int i = 0; i < cmsPageList.size(); i++) {
                if (cmsPageList.get(i).getPageType().equalsIgnoreCase(ServiceResource.OTHER_STRING)) {
                    cmspagesModels.add(cmsPageList.get(i));
                }
            }

            if (cmspagesModels != null && cmspagesModels.size() > 0) {
                cmsPageAdapter = new CMSPageAdapter(mContext, cmspagesModels, true, false);
                cmsPageAdapter.setcolor(true);
                lv_page.setAdapter(cmsPageAdapter);
                txt_nodatafound.setVisibility(View.GONE);
            } else {
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoPagesFound));
                ll_search.setVisibility(View.GONE);
            }

        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoPagesFound));
            ll_search.setVisibility(View.GONE);
        }

    }

    private void DisplayBlog() {

        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {

            BlogList = blogListDataDao.getBlogList();
            if (BlogList.isEmpty() || BlogList.size() == 0 || BlogList == null) {
                Blog(true);
            } else {
                //set Adapter
                setBloglistAdapter(BlogList);
                /*END*/
                Blog(false);
            }

        } else {
            BlogList = blogListDataDao.getBlogList();
            if (BlogList != null) {
                setBloglistAdapter(BlogList);
            }
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
        /*END*/
    }

    private void setBloglistAdapter(List<BlogListModel> blogList) {

        if (blogList != null && blogList.size() > 0) {
            pagesModels = new ArrayList<BlogListModel>();
            for (int i = 0; i < blogList.size(); i++) {
                pagesModels.add(blogList.get(i));
            }
            if (pagesModels != null && pagesModels.size() > 0) {
                adpter = new PageAdapter(mContext, pagesModels, true, true);
                adpter.setcolor(true);
                lv_page.setAdapter(adpter);
                txt_nodatafound.setVisibility(View.GONE);
            } else {
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoBlogAvailable));
                ll_search.setVisibility(View.GONE);
            }
        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoBlogAvailable));
            ll_search.setVisibility(View.GONE);
        }

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (isBlog) {
            try {

                if (pagesModels.size() > 0) {

                    Intent intent = new Intent(mContext, PageDetailActivity.class);
                    intent.putExtra("Page", pagesModels.get(position - 1));
                    intent.putExtra("isBlog", isBlog);
                    startActivity(intent);
                    overridePendingTransition(0, 0);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (cmspagesModels.size() > 0) {

                    Intent intent = new Intent(mContext, PageDetailActivity.class);
                    intent.putExtra("Page", cmspagesModels.get(position - 1));
                    intent.putExtra("isBlog", isBlog);
                    startActivity(intent);
                    overridePendingTransition(0, 0);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                RedirectToDashboard(this);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        RedirectToDashboard(this);

    }

    public void cmsPages(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GETCMSPAGES_METHODNAME, ServiceResource.CMSPAGE_URL);

    }

    public void Blog(boolean idViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(mContext, arrayList, idViewPopup, this).execute(ServiceResource.GETBLOGLIST_METHODNAME, ServiceResource.BLOG_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETCMSPAGES_METHODNAME.equalsIgnoreCase(methodName)) {
            getCMSPages(result);
        } else if (ServiceResource.GETBLOGLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            parseBlog(result);
        }

    }


    public void parseBlog(String result) {

        JSONArray hJsonArray;
        blogListDataDao.deleteBlogList();

        try {

            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray
                            .getJSONObject(i);

                    // parse Bloglist
                    ParseBlogList(hJsonObject);
                    /*END*/

                }

                if (blogListDataDao.getBlogList().size() > 0) {
                    setBloglistAdapter(blogListDataDao.getBlogList());
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        lv_page.onRefreshComplete();

    }

    private void ParseBlogList(JSONObject hJsonObject) {

        try {
            blogListModel.setCMSPagesID(hJsonObject.getString(ServiceResource.BLOG_BLOGSID));
            blogListModel.setPageName(hJsonObject.getString(ServiceResource.BLOG_BLOGTITLE));
            blogListModel.setPageTitle(hJsonObject.getString(ServiceResource.BLOG_BLOGTITLE));
            blogListModel.setImageURl(hJsonObject.getString(ServiceResource.BLOG_BLOGIMAGE));

            String date = Utility.getDate(
                    Long.valueOf(hJsonObject.getString(ServiceResource.BLOG_BLOGDATE)
                            .replace("/Date(", "").replace(")/", "")),
                    "dd-MM-yyyy");

            blogListModel.setDate(date);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        blogListDataDao.insertBlog(blogListModel);

    }

    public void getCMSPages(String result) {

        JSONArray hJsonArray;
        cmsPageListDataDao.deleteCMSPageList();
        try {
            if (result.contains("\"success\":0")) {
            } else {

                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray
                            .getJSONObject(i);

                    // parse CMSPageList
                    ParseCMSPageList(hJsonObject);
                    /*END*/
                }

                if (cmsPageListDataDao.getCMSPageList().size() > 0) {
                    setCMSPagelistAdapter(cmsPageListDataDao.getCMSPageList());
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        lv_page.onRefreshComplete();

    }

    private void ParseCMSPageList(JSONObject hJsonObject) {
        try {
            cmsPageListModel.setCMSPagesID(hJsonObject.getString(ServiceResource.CMSPAGES_ID));
            cmsPageListModel.setPageName(hJsonObject.getString(ServiceResource.CMSPAGES_PAGENAME));
            cmsPageListModel.setPageTitle(hJsonObject.getString(ServiceResource.CMSPAGES_PAGE_TITLE));
            cmsPageListModel.setPageType(hJsonObject.getString(ServiceResource.CMSPAGES_PAGETYPE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        cmsPageListDataDao.insertCMSPage(cmsPageListModel);
    }
}
