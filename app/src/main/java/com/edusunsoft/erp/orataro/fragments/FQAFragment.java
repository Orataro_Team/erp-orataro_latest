package com.edusunsoft.erp.orataro.fragments;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FQAListAdapter;
import com.edusunsoft.erp.orataro.model.FQAChildModel;
import com.edusunsoft.erp.orataro.model.FQAModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;

public class FQAFragment extends Fragment {

    ExpandableListView exp_fqalist;
    FQAListAdapter fqaAdapter;
    List<FQAModel> list;
    List<FQAChildModel> listChild;
    private int width;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fqafragment, null);
        exp_fqalist = (ExpandableListView) v.findViewById(R.id.exp_fqalist);


        if (HomeWorkFragmentActivity.txt_header != null) {
            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.faq) + " (" + Utility.GetFirstName(getActivity()) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 70, 0);
        }

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;

        list = new ArrayList<FQAModel>();

        FQAModel model;
        FQAChildModel modelChild;
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("After Login to Orataro you will have Wall screen on your mobile where you can read homework, classwork, circular, messages, photos etc. related updates directly at one place with scrolling it.   You can Like, Unlike and Share it.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Wall ");
        model.setImageName("1.PNG");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Press Home button on top left of screen where you will have Main menu with frequent use option which are explained in detail as per below.  ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Home Screen");
        model.setImageName("2.png");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Parents can introduce to school and other Parents for Better Communication via Orataro. Same time they can control their private information with level of privacy setting.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Profile  ");
        list.add(model);


        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("School circulars can be viewed from here in month and Date wise list on press of single circular will open in detailed view to read it. Once parents read it will be marked as read. Press back button to go back to circular list.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Circulars  ");
        list.add(model);


        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Very much Helpful for parents to involve with day to day schooling by checking daily Home Work on App with Finish Date. Press a single homework to read instruction in detail and see which teacher has sent it.  ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Home Work  ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription(":- It make schooling more transparent and updates parents with day to day teaching activity by posting daily Class Work on Orataro where it displays period wise teaching information. Press a single classwork to read instruction in detail and see which teacher has sent it.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Classwork ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription(" To communicate teacher for Childs study matter.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("PT communication ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("It will Display Exam timing.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Exam Timing ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Helps students to prepare books and homework according to daily class periods. Here normally you can see today’s time table but on scrolling forward you can see future days, weeks time table as well past days time table also.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Time Table  ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Important exam notes, Ref. study materials, Test papers, Assignments etc. will be posted here by school you can download it also for printing and future use.  ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Notice ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("It will display List of Holidays.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Holidays ");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Will Remind you about upcoming events, reminders etc. posted by school.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Calendar");
        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("To collect views, decisions via small survey of students, parents, teachers before starting activities will be activated later. ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Poll");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("About to view notifications of posts,like, dislike, comments etc. all activities happened related to you in a List view like Email. ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Notification  ");
        list.add(model);

//        listChild = new ArrayList<FQAChildModel>();
//        model = new FQAModel();
//        modelChild = new FQAChildModel();
//        modelChild.setDescription("It’s a kind of Organizer where students can set their study, Exam related reminders which parents can also view and remind them on time to complete it.");
//        listChild.add(modelChild);
//        model.setChildList(listChild);
//        model.setTitle("Reminder");
//        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Where we mentioned range of options to reach us including help desk details and social media. Top of that parents can access our other free Apps. Useful for toddlers.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        // Commented By Hardik Kanak : 06-02-2021 - About Orataro to About App
        model.setTitle("About App");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Useful to parents who need to access more than one child on this application.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Switch Account  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("<ul>    <li>        <strong>Phone Chain : </strong>        Where student can store list of emergency contacts.<strong> </strong>    </li> <p> <li>        <strong>Health profile : </strong>        Parents can update students health profile on regular basis which can be accessed by school in case of medical emergency.    </li>  </p> <p><li>        <strong>Parent Profile : </strong>        No need to Rush school for updating your contact details and other profile updates it will automatically update school records from parents mobile.        Also parent can confirm their identity for pickup and drop of their kids very much important for safety.    </li></p></ul>");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("My Profile  ");
        model.setImageName("3.png");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Parents get more information about School, Board Members, History of school, facilities offered by school etc. ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Institute Pages  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("School can be more connected to parents, students via live discuss on various topics.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Blogs  ");
        list.add(model);

//        listChild = new ArrayList<FQAChildModel>();
//        model = new FQAModel();
//        modelChild = new FQAChildModel();
//        modelChild.setDescription("Students can created division, grade and school friend network to discuss schooling topics yes parents will be aware about discussions so will not miss track. ");
//        listChild.add(modelChild);
//        model.setChildList(listChild);
//        model.setTitle("Friends ");
//        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Photo gallery to post photos on various social media platforms.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Photo  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Students can create a class schoolgroups, standard schoolgroups, and friends schoolgroups and can start discussions on topics. Such discussions help them with their studies and they can obtain answers from other members to clear doubts, thus ensuring that they have a thorough understanding of the topics ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("School Groups ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Teachers tend to reward their students of their achievements by way of happygrams which they will be able to put on the app by way of emoticons or comments of encouragement and praise.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("My Happygram  ");
        list.add(model);

//        listChild = new ArrayList<FQAChildModel>();
//        model = new FQAModel();
//        modelChild = new FQAChildModel();
//        modelChild.setDescription("Further Additional Information about school will be displayed here. ");
//        listChild.add(modelChild);
//        model.setChildList(listChild);
//        model.setTitle("Information  ");
//        list.add(model);

        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("It will display lyrics of prayers followed by school.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("School Prayer  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Common Information for whole school will be posted here like Event photos, Results, function pictures etc.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Institute Wall  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Authorized  Teacher will post information related to that Standard/Grade on this wall where students will only be able to his/her Grade/Standard wall.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Standard Wall  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Further Division teacher will only be allow to post informtion on this wall and same way students of that Division can only see this wall.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Division wall  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Subject teacher can post particular subject related information and students of that subject will only be able to see this wall.");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Subject Wall  ");
        list.add(model);
        listChild = new ArrayList<FQAChildModel>();
        model = new FQAModel();
        modelChild = new FQAChildModel();
        modelChild.setDescription("Parents can see Ongoing projects and its status of which his child is part of.  ");
        listChild.add(modelChild);
        model.setChildList(listChild);
        model.setTitle("Projects  ");
        list.add(model);

//        listChild = new ArrayList<FQAChildModel>();
//        model = new FQAModel();
//        modelChild = new FQAChildModel();
//        modelChild.setDescription("At any point of time aboutorataro can change password as per convenience. ");
//        listChild.add(modelChild);
//        model.setChildList(listChild);
//        model.setTitle("Change Password  ");
//        list.add(model);

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            exp_fqalist.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
            exp_fqalist.setIndicatorBounds(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        } else {
            exp_fqalist.setIndicatorBoundsRelative(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
            exp_fqalist.setIndicatorBoundsRelative(width - GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        }

        fqaAdapter = new FQAListAdapter(getActivity(), list);
        exp_fqalist.setAdapter(fqaAdapter);

        exp_fqalist.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                FQAListAdapter customExpandAdapter = (FQAListAdapter) exp_fqalist.getExpandableListAdapter();
                if (customExpandAdapter == null) {
                    return;
                }
                for (int i = 0; i < customExpandAdapter.getGroupCount(); i++) {
                    if (i != groupPosition) {
                        exp_fqalist.collapseGroup(i);
                    }
                }
            }

        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public int GetDipsFromPixel(float pixels) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }
}
