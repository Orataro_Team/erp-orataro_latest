package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoteParse implements Parcelable {

    @SerializedName("NotesID")
    @Expose
    private String notesID;
    @SerializedName("GradeID")
    @Expose
    private String gradeID;
    @SerializedName("DivisionID")
    @Expose
    private String divisionID;
    @SerializedName("SubjectID")
    @Expose
    private String subjectID;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("DateOfNotes")
    @Expose
    private String dateOfNotes;
    @SerializedName("NoteTitle")
    @Expose
    private String noteTitle;
    @SerializedName("NoteDetails")
    @Expose
    private String noteDetails;
    @SerializedName("ActionStartDate")
    @Expose
    private String actionStartDate;
    @SerializedName("ActionEndDate")
    @Expose
    private String actionEndDate;
    @SerializedName("DressCode")
    @Expose
    private String dressCode;
    @SerializedName("IsRead")
    @Expose
    private String isRead;
    @SerializedName("IamReady")
    @Expose
    private String iamReady;
    @SerializedName("ParentNote")
    @Expose
    private String parentNote;
    @SerializedName("Rating")
    @Expose
    private String rating;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("IsChecked")
    @Expose
    private String isChecked;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("TotalRead")
    @Expose
    private String totalRead;
    @SerializedName("TotalApprove")
    @Expose
    private String totalApprove;
    public final static Parcelable.Creator<NoteParse> CREATOR = new Creator<NoteParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NoteParse createFromParcel(Parcel in) {
            NoteParse instance = new NoteParse();
            instance.notesID = ((String) in.readValue((String.class.getClassLoader())));
            instance.gradeID = ((String) in.readValue((String.class.getClassLoader())));
            instance.divisionID = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectID = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectName = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfNotes = ((String) in.readValue((String.class.getClassLoader())));
            instance.noteTitle = ((String) in.readValue((String.class.getClassLoader())));
            instance.noteDetails = ((String) in.readValue((String.class.getClassLoader())));
            instance.actionStartDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.actionEndDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.dressCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.isRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.iamReady = ((String) in.readValue((String.class.getClassLoader())));
            instance.parentNote = ((String) in.readValue((String.class.getClassLoader())));
            instance.rating = ((String) in.readValue((Double.class.getClassLoader())));
            instance.userName = ((String) in.readValue((String.class.getClassLoader())));
            instance.isChecked = ((String) in.readValue((String.class.getClassLoader())));
            instance.photo = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileType = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.totalApprove = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public NoteParse[] newArray(int size) {
            return (new NoteParse[size]);
        }

    };

    public String getNotesID() {
        return notesID;
    }

    public void setNotesID(String notesID) {
        this.notesID = notesID;
    }

    public String getGradeID() {
        return gradeID;
    }

    public void setGradeID(String gradeID) {
        this.gradeID = gradeID;
    }

    public String getDivisionID() {
        return divisionID;
    }

    public void setDivisionID(String divisionID) {
        this.divisionID = divisionID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDateOfNotes() {
        return dateOfNotes;
    }

    public void setDateOfNotes(String dateOfNotes) {
        this.dateOfNotes = dateOfNotes;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteDetails() {
        return noteDetails;
    }

    public void setNoteDetails(String noteDetails) {
        this.noteDetails = noteDetails;
    }

    public String getActionStartDate() {
        return actionStartDate;
    }

    public void setActionStartDate(String actionStartDate) {
        this.actionStartDate = actionStartDate;
    }

    public String getActionEndDate() {
        return actionEndDate;
    }

    public void setActionEndDate(String actionEndDate) {
        this.actionEndDate = actionEndDate;
    }

    public String getDressCode() {
        return dressCode;
    }

    public void setDressCode(String dressCode) {
        this.dressCode = dressCode;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIamReady() {
        return iamReady;
    }

    public void setIamReady(String iamReady) {
        this.iamReady = iamReady;
    }

    public String getParentNote() {
        return parentNote;
    }

    public void setParentNote(String parentNote) {
        this.parentNote = parentNote;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getTotalRead() {
        return totalRead;
    }

    public void setTotalRead(String totalRead) {
        this.totalRead = totalRead;
    }

    public String getTotalApprove() {
        return totalApprove;
    }

    public void setTotalApprove(String totalApprove) {
        this.totalApprove = totalApprove;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(notesID);
        dest.writeValue(gradeID);
        dest.writeValue(divisionID);
        dest.writeValue(subjectID);
        dest.writeValue(subjectName);
        dest.writeValue(dateOfNotes);
        dest.writeValue(noteTitle);
        dest.writeValue(noteDetails);
        dest.writeValue(actionStartDate);
        dest.writeValue(actionEndDate);
        dest.writeValue(dressCode);
        dest.writeValue(isRead);
        dest.writeValue(iamReady);
        dest.writeValue(parentNote);
        dest.writeValue(rating);
        dest.writeValue(userName);
        dest.writeValue(isChecked);
        dest.writeValue(photo);
        dest.writeValue(fileType);
        dest.writeValue(totalRead);
        dest.writeValue(totalApprove);
    }

    public int describeContents() {
        return 0;
    }

}
