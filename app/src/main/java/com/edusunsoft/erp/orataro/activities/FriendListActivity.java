 package com.edusunsoft.erp.orataro.activities;

 import android.app.Activity;
 import android.content.Context;
 import android.content.Intent;
 import android.os.Bundle;
 import android.text.Editable;
 import android.text.TextWatcher;
 import android.view.View;
 import android.view.View.OnClickListener;
 import android.view.WindowManager;
 import android.widget.EditText;
 import android.widget.ImageView;
 import android.widget.LinearLayout;
 import android.widget.TextView;

 import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
 import com.edusunsoft.erp.orataro.R;
 import com.edusunsoft.erp.orataro.adapter.Friend_ListAdapter;
 import com.edusunsoft.erp.orataro.customeview.ActionItem;
 import com.edusunsoft.erp.orataro.customeview.QuickAction;
 import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
 import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
 import com.edusunsoft.erp.orataro.model.FriendListModel;
 import com.edusunsoft.erp.orataro.model.PropertyVo;
 import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
 import com.edusunsoft.erp.orataro.services.ServiceResource;
 import com.edusunsoft.erp.orataro.util.Global;
 import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
 import com.edusunsoft.erp.orataro.util.Utility;

 import org.json.JSONArray;
 import org.json.JSONException;
 import org.json.JSONObject;

 import java.util.ArrayList;
 import java.util.Locale;

 public class FriendListActivity extends Activity implements OnClickListener, ResponseWebServices {

     PullAndLoadListView lst_fb_frnd;
     ImageView img_back, iv_add_friend;

     FriendListModel friendListModel;
     ArrayList<FriendListModel> friendListModels;
     EditText edtTeacherName;
     Friend_ListAdapter friend_ListAdapter;
     LinearLayout ll_search;
     Context mContext;
     TextView txt_nodatafound;

     private static final int ID_ADD_FRIEND = 1;
     private static final int ID_REQ_FRIEND = 2;

     ActionItem actionAddfriend, actionReqFriend;
     QuickAction quickAction;

     Intent intent;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         // TODO Auto-generated method stub
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_friends_list_on_wall);
         getWindow().setSoftInputMode(
                 WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
         mContext = FriendListActivity.this;
         ll_search = (LinearLayout) findViewById(R.id.searchlayout);
         lst_fb_frnd = (PullAndLoadListView) findViewById(R.id.lst_fb_frnd);
         edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
         img_back = (ImageView) findViewById(R.id.img_back);
         iv_add_friend = (ImageView) findViewById(R.id.iv_add_friend);
         // txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
         ll_search.setVisibility(View.VISIBLE);

         txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);

         img_back.setOnClickListener(this);
         iv_add_friend.setOnClickListener(this);

         edtTeacherName.addTextChangedListener(new TextWatcher() {

             @Override
             public void afterTextChanged(Editable arg0) {
                 // TODO Auto-generated method stub

             }

             @Override
             public void beforeTextChanged(CharSequence arg0, int arg1,
                                           int arg2, int arg3) {
                 // TODO Auto-generated method stub
             }

             @Override
             public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                       int arg3) {
                 String text = edtTeacherName.getText().toString()
                         .toLowerCase(Locale.getDefault());
                 friend_ListAdapter.filter(text);
             }
         });

         actionAddfriend = new ActionItem(ID_ADD_FRIEND,mContext.getResources().getString(R.string.addfriend), mContext
                 .getResources().getDrawable(R.drawable.add_friends));
         actionReqFriend = new ActionItem(ID_REQ_FRIEND,mContext.getResources().getString(R.string.friendreq),
                 mContext.getResources().getDrawable(
                         R.drawable.fb_req_frnd_white));

         quickAction = new QuickAction(mContext, QuickAction.VERTICAL);

         quickAction.addActionItem(actionAddfriend);
         quickAction.addActionItem(actionReqFriend);

         quickAction
                 .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                     @Override
                     public void onItemClick(QuickAction source, int pos,
                                             int actionId) {
                         ActionItem actionItem = quickAction.getActionItem(pos);

                         // here we can filter which action item was clicked with
                         // pos or actionId parameter
                         if (actionId == ID_ADD_FRIEND) {
                             intent = new Intent(mContext,
                                     FreindSearchActivity.class);
                             startActivity(intent);
 //							finish();

                         } else if (actionId == ID_REQ_FRIEND) {
                             intent = new Intent(mContext,
                                     FriendRequestListActivity.class);
                             startActivity(intent);
 //							finish();
                         }
                     }
                 });

         quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
             @Override
             public void onDismiss() {
                 // Toast.makeText(context, "Dismissed",
                 // Toast.LENGTH_SHORT).show();
             }
         });


         lst_fb_frnd.setOnRefreshListener(new OnRefreshListener() {

             public void onRefresh() {
                 // Do work to refresh the list here.

                 if (Utility.isNetworkAvailable(mContext)) {
                     String result = Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext);
                     //if(examresult.equalsIgnoreCase("")) {
                         FriendsList(true);
 //					}else{
 //						FriendsList(false);
 //					}
 //					parseFriendList(Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext));
                 } else {
                     //parseFriendList(Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext));
 //					ServiceResource.FRIENDS_METHODNAME

                     Utility.showAlertDialog(mContext,
                             "Please Check Your Internet Connection","Error");
                 }
             }
         });


         if (Utility.isNetworkAvailable(mContext)) {
             String result = Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext);
             //if(examresult.equalsIgnoreCase("")) {
                 FriendsList(true);
 //			}else{
 //				FriendsList(false);
 //			}
 //			parseFriendList(Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext));
         } else {
         //	parseFriendList(Utility.readFromFile(ServiceResource.FRIENDS_METHODNAME, mContext));
 //			ServiceResource.FRIENDS_METHODNAME

             Utility.showAlertDialog(mContext,
                     "Please Check Your Internet Connection","Error");
         }
     }


     @Override
     protected void onResume() {
         // TODO Auto-generated method stub

         super.onResume();
     }




     @Override
     public void onClick(View v) {
         // TODO Auto-generated method stub

         switch (v.getId()) {
         case R.id.img_back:

             finish();
             break;

         case R.id.iv_add_friend:

             quickAction.show(v);

             break;

         default:
             break;
         }

     }



     public void FriendsList(boolean isViewPopup){
         ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
         arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                 new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
         arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                 new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
         arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                 new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
         arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

         new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.FRIENDS_METHODNAME,
                 ServiceResource.FRIENDS_URL);


     }


     @Override
     public void response(String result, String methodName) {
         if(ServiceResource.FRIENDS_METHODNAME.equalsIgnoreCase(methodName)){
             Utility.writeToFile(result, methodName, mContext);
             parseFriendList(result);
         }
     }

 public void parseFriendList(String result){

     JSONArray jsonObj;
     try {
         Global.friendListModels = new ArrayList<FriendListModel>();

         jsonObj = new JSONArray(result);
         // JSONArray detailArrray= jsonObj.getJSONArray("");

         for (int i = 0; i < jsonObj.length(); i++) {
             JSONObject innerObj = jsonObj.getJSONObject(i);
             FriendListModel friendListModel = new FriendListModel();
             friendListModel.setFullName(innerObj
                     .getString(ServiceResource.FRIENDS_FULLNAME));
             friendListModel.setProfilePicture(innerObj
                     .getString(ServiceResource.FRIENDS_PROFILEPIC));

             if (friendListModel.getProfilePicture() != null
                     && !friendListModel.getProfilePicture().equals("")) {

                 if (friendListModel.getProfilePicture().contains(
                         "/img/"))
                     friendListModel
                             .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                     + friendListModel
                                             .getProfilePicture()
                                             );
                 else
                     friendListModel
                             .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                     + friendListModel
                                             .getProfilePicture()
                             );
             }

             friendListModel.setFriendID(innerObj
                     .getString(ServiceResource.FRIENDS_FRIENDID));
             friendListModel.setFriendListID(innerObj
                     .getString(ServiceResource.FRIENDS_FRIENDLIST));
             friendListModel.setGradeID(innerObj
                     .getString(ServiceResource.FRIENDS_GRADEID));
             friendListModel.setGradeName(innerObj
                     .getString(ServiceResource.FRIENDS_GRADENAME));

             friendListModel.setDivisionID(innerObj
                     .getString(ServiceResource.FRIENDS_DIVISIONID));
             friendListModel.setDivisionName(innerObj
                     .getString(ServiceResource.FRIENDS_DIVISIONNAME));


             friendListModel.setWallID(innerObj
                     .getString(ServiceResource.FRIENDS_WALLID));
             Global.friendListModels.add(friendListModel);

         }

     } catch (JSONException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
     }

     if (Global.friendListModels != null
             && Global.friendListModels.size() > 0) {
         friend_ListAdapter = new Friend_ListAdapter(mContext,
                 Global.friendListModels);
         lst_fb_frnd.setAdapter(friend_ListAdapter);
         txt_nodatafound.setVisibility(View.GONE);
     } else {
         ll_search.setVisibility(View.GONE);
         txt_nodatafound.setVisibility(View.VISIBLE);
         txt_nodatafound.setText(mContext.getResources().getString(R.string.nofriendlist));
     }



 }
 }
