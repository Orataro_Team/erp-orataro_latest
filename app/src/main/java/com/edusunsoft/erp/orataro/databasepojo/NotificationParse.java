package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationParse implements Parcelable {

    @SerializedName("RowNo")
    @Expose
    private String rowNo;
    @SerializedName("NotificationID")
    @Expose
    private String notificationID;
    @SerializedName("AssociationID")
    @Expose
    private String associationID;
    @SerializedName("AssociationType")
    @Expose
    private String associationType;
    @SerializedName("DateofNotification")
    @Expose
    private String dateofNotification;
    @SerializedName("Isview")
    @Expose
    private String isview;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("DateofNotificationAsText")
    @Expose
    private String dateofNotificationAsText;
    @SerializedName("NotificationText")
    @Expose
    private String notificationText;
    @SerializedName("NotificationType")
    @Expose
    private String notificationType;
    @SerializedName("SendToMemberID")
    @Expose
    private String sendToMemberID;
    @SerializedName("Details")
    @Expose
    private String details;
    public final static Parcelable.Creator<NotificationParse> CREATOR = new Creator<NotificationParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NotificationParse createFromParcel(Parcel in) {
            NotificationParse instance = new NotificationParse();
            instance.rowNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.notificationID = ((String) in.readValue((String.class.getClassLoader())));
            instance.associationID = ((String) in.readValue((String.class.getClassLoader())));
            instance.associationType = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateofNotification = ((String) in.readValue((String.class.getClassLoader())));
            instance.isview = ((String) in.readValue((String.class.getClassLoader())));
            instance.fullName = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateofNotificationAsText = ((String) in.readValue((String.class.getClassLoader())));
            instance.notificationText = ((String) in.readValue((String.class.getClassLoader())));
            instance.notificationType = ((String) in.readValue((String.class.getClassLoader())));
            instance.sendToMemberID = ((String) in.readValue((String.class.getClassLoader())));
            instance.details = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public NotificationParse[] newArray(int size) {
            return (new NotificationParse[size]);
        }

    };

    public String getRowNo() {
        return rowNo;
    }

    public void setRowNo(String rowNo) {
        this.rowNo = rowNo;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getAssociationID() {
        return associationID;
    }

    public void setAssociationID(String associationID) {
        this.associationID = associationID;
    }

    public String getAssociationType() {
        return associationType;
    }

    public void setAssociationType(String associationType) {
        this.associationType = associationType;
    }

    public String getDateofNotification() {
        return dateofNotification;
    }

    public void setDateofNotification(String dateofNotification) {
        this.dateofNotification = dateofNotification;
    }

    public String getIsview() {
        return isview;
    }

    public void setIsview(String isview) {
        this.isview = isview;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateofNotificationAsText() {
        return dateofNotificationAsText;
    }

    public void setDateofNotificationAsText(String dateofNotificationAsText) {
        this.dateofNotificationAsText = dateofNotificationAsText;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getSendToMemberID() {
        return sendToMemberID;
    }

    public void setSendToMemberID(String sendToMemberID) {
        this.sendToMemberID = sendToMemberID;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(rowNo);
        dest.writeValue(notificationID);
        dest.writeValue(associationID);
        dest.writeValue(associationType);
        dest.writeValue(dateofNotification);
        dest.writeValue(isview);
        dest.writeValue(fullName);
        dest.writeValue(dateofNotificationAsText);
        dest.writeValue(notificationText);
        dest.writeValue(notificationType);
        dest.writeValue(sendToMemberID);
        dest.writeValue(details);
    }

    public int describeContents() {
        return 0;
    }

}
