package com.edusunsoft.erp.orataro.model;

import java.util.ArrayList;

public class ParentConnectModel {

    private String parentConnectSub, parentConnectSubDetail, month, year, date;
    private boolean changeMonth = false;
    private ArrayList<String> remark;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getParentConnectSub() {
        return parentConnectSub;
    }

    public void setParentConnectSub(String parentConnectSub) {
        this.parentConnectSub = parentConnectSub;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isChangeMonth() {
        return changeMonth;
    }

    public void setChangeMonth(boolean changeMonth) {
        this.changeMonth = changeMonth;
    }

    public String getParentConnectSubDetail() {
        return parentConnectSubDetail;
    }

    public void setParentConnectSubDetail(String parentConnectSubDetail) {
        this.parentConnectSubDetail = parentConnectSubDetail;
    }

    public ArrayList<String> getRemark() {
        return remark;
    }

    public void setRemark(ArrayList<String> remark) {
        this.remark = remark;
    }

}
