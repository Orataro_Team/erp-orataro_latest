package com.edusunsoft.erp.orataro.model;

public class ShareFIleModel {

    private String fileName, fileDate, fileSize;
    private boolean chnageMonth = false;

    public boolean isChnageMonth() {
        return chnageMonth;
    }

    public void setChnageMonth(boolean chnageMonth) {
        this.chnageMonth = chnageMonth;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDate() {
        return fileDate;
    }

    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

}
