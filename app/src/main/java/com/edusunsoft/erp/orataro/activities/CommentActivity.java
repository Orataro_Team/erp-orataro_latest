package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.ShareInterface;
import com.edusunsoft.erp.orataro.Interface.WallCustomeClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.MyWallAdapter;
import com.edusunsoft.erp.orataro.adapter.WallCommentAdapter;
import com.edusunsoft.erp.orataro.database.GenerallWallData;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallCommentModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView.OnEmojiconClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

/**
 * Comment view full screen
 *
 * @author admin
 */
public class CommentActivity extends Activity implements ResponseWebServices, WallCustomeClick, ShareInterface {

    LinearLayout likeLayout;
    View line;
    boolean isRemark = false;
    Context mContext;
    GenerallWallData wallPostModel;

    Integer count = 1;
    Integer tempCount = 1;
    public boolean isMoreData;
    ListView lst_comment;

    TextView txtlikecount, tv_no_comment;
    ImageView iv_like_smb;
    EmojiconEditText emojiconEditText;
    WallCommentAdapter wallCommentAdapter;
    boolean isFirst = true;
    private MyWallAdapter myWallAdapter;
    String FROM = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialogcommentlayout);

        mContext = this;

        if (getIntent() != null) {

            wallPostModel = (GenerallWallData) getIntent().getSerializableExtra(
                    "WallComment");
            FROM = getIntent().getStringExtra("from");

        }

        Global.wallCommentModels = new ArrayList<WallCommentModel>();
        likeLayout = (LinearLayout) findViewById(R.id.likeLayout);
        line = findViewById(R.id.line);

        if (getIntent() != null) {

            isRemark = getIntent().getBooleanExtra("flag", false);

        }

        if (isRemark) {

            likeLayout.setVisibility(View.GONE);
            line.setVisibility(View.GONE);

        }

        txtlikecount = (TextView) findViewById(R.id.txtlikecount);
        tv_no_comment = (TextView) findViewById(R.id.tv_no_comment);
        iv_like_smb = (ImageView) findViewById(R.id.iv_like_smb);

        lst_comment = (ListView) findViewById(R.id.lst_comment);

        // final LinearLayout addCommentLayout = (LinearLayout)
        // findViewById(R.id.layoutComment);
        emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);
        final View rootView = findViewById(R.id.root_view);
        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        final ImageView submitButton = (ImageView) findViewById(R.id.postcommentbtn);

        if (Utility.isNetworkAvailable(mContext)) {

            Global.wallCommentModels = new ArrayList<WallCommentModel>();
            commentList(1);


        } else {

            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
        }

        if (wallPostModel.getTotalLikes() != null
                && Integer.parseInt(wallPostModel.getTotalLikes()) > 0
                && !wallPostModel.getTotalLikes().equals("")) {

            if (Integer.parseInt(wallPostModel.getTotalLikes()) > 1) {

                txtlikecount.setText(wallPostModel.getTotalLikes() + getResources().getString(R.string.Likes));

            } else {

                txtlikecount.setText(wallPostModel.getTotalLikes() + getResources().getString(R.string.Like));

            }

            lst_comment.setVisibility(View.VISIBLE);
            tv_no_comment.setVisibility(View.GONE);

        } else {

            txtlikecount.setText(getResources().getString(R.string.Bethefirsttolikethis));
            tv_no_comment.setVisibility(View.VISIBLE);
            lst_comment.setVisibility(View.GONE);

        }

//        lst_comment.setOnLoadMoreListener(new OnLoadMoreListener() {
//
//            @Override
//            public void onLoadMore() {
//
//                if (isMoreData) {
//
//                    count += 10;
//                    commentList(count);
//
//                } else {
//
//                    count = 0;
//
//                }
//
//                // new LoadMoreDataTask().execute();
//
//            }
//        });

        // Give the topmost view of your activity layout hierarchy. This will be
        // used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        // Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        // If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        // If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        // On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (emojiconEditText == null || emojicon == null) {
                    return;
                }

                int start = emojiconEditText.getSelectionStart();
                int end = emojiconEditText.getSelectionEnd();
                if (start < 0) {
                    emojiconEditText.append(emojicon.getEmoji());
                } else {
                    emojiconEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }

            }

        });

        // On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // If popup is not showing => emoji keyboard is not visible, we
                // need to show it
                if (!popup.isShowing()) {
                    // toast("if");
                    // If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        // toast("if1");
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }

                    // else, open the text keyboard first and immediately after
                    // that show the emoji popup
                    else {
                        // toast("else");
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        // popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText,
                                InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }
                }

                // If popup is showing, simply dismiss it to show the undelying
                // text keyboard
                else {
                    // toast("else1");
                    popup.dismiss();
                }
            }
        });

        // On submit, add the edittext text to listview and clear the edittext
        submitButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String newText = emojiconEditText.getText().toString();
                // emojiconEditText.getText().clear();
                if (newText != null && !newText.equalsIgnoreCase("")) {

                    addComment();

                }

            }

        });
    }

    /**
     * change keyboard icon
     *
     * @param iconToBeChanged
     * @param drawableResourceId
     */
    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged,
                                         int drawableResourceId) {

        iconToBeChanged.setImageResource(drawableResourceId);

    }


    /**
     * show toast
     *
     * @param msg
     */
    public void toast(String msg) {

        Utility.toast(CommentActivity.this, "" + msg);

    }

    /**
     * AddComment Webservice
     */
    public void addComment() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_ASSOCIATIONID,
                wallPostModel.getPostCommentID()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_REFCOMMENTID, null));

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_ASSOCIATIONTYPETERM,
                wallPostModel.getAssociationType()));
        arrayList.add(new PropertyVo(ServiceResource.SENDTOMEMBERID,
                wallPostModel.getSendToMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_COMMENT, emojiconEditText
                .getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_POSTBYTYPE,
                new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDCOMMENT_METHODNAME,
                ServiceResource.POST_URL);


    }

    /**
     * getComment List from cout to count +10
     *
     * @param count
     */

    public void commentList(int count) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_POSTID,
                wallPostModel.getPostCommentID()));
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_WALLID, wallPostModel.getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_ROWNO, String.valueOf(count)));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CMT_METHODNAME,
                ServiceResource.CMT_URL);


    }

    /**
     * response of all webservices
     */

    /*
     * (non-Javadoc)
     * @see ResponseWebServices#response(java.lang.String, java.lang.String)
     */
    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.ADDCOMMENT_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                JSONArray mainJsonArray;

                try {

                    Log.d("getCommentResult", result);

                    Utility.sendPushNotification(mContext);

                    mainJsonArray = new JSONArray(result);

//
//                    wallPostModel.setTotalComments(String.valueOf(Integer
//                            .valueOf(wallPostModel.getTotalComments()) + 1));

//                    Log.d("getAllcomment", wallPostModel.getTotalComments());

                    ServiceResource.WALL_TOTALCOMMENT = "FromComment";


//                    Log.d("getTotalComment", wallPostModel.getTotalComments());
//                    Log.d("getTotalComment22", String.valueOf(Integer
//                            .valueOf(wallPostModel.getTotalComments()) + 1));


//                    WALL_TOTALCOMMENT = "FromAddComment";

//                    wallPostModel.setTotalComments(String.valueOf(Integer
//                            .valueOf(wallPostModel.getTotalComments()) + 1));


                    // JSONArray detailArrray= jsonObj.getJSONArray("");
                    Global.wallCommentModels = new ArrayList<WallCommentModel>();

                    for (int i = 0; i < mainJsonArray.length(); i++) {

                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        WallCommentModel wallCommentModel = new WallCommentModel();

                        wallCommentModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));

                        wallCommentModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallCommentModel.setCommentID(innerObj
                                .getString(ServiceResource.WALL_COMMENTID));

                        wallCommentModel.setComment(innerObj
                                .getString(ServiceResource.WALL_COMMENT));

                        wallCommentModel.setCommentOn(innerObj
                                .getString(ServiceResource.WALL_COMMENTON));

                        wallCommentModel.setIsDisLike(innerObj
                                .getString(ServiceResource.WALL_ISDISLIKE));
                        wallCommentModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));
                        wallCommentModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));
                        wallCommentModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));

                        wallCommentModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallCommentModel.setTotalReplies(innerObj
                                .getString(ServiceResource.WALL_TOTALREPLIES));
                        wallCommentModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        wallCommentModel.setPostDate(innerObj
                                .getString(ServiceResource.WALL_POSTDATE));
                        wallCommentModel.setRow_ID(innerObj
                                .getString(ServiceResource.WALL_ROW_ID));

                        Global.wallCommentModels.add(wallCommentModel);

                        Log.d("getCommentSize", String.valueOf(Global.wallCommentModels.size()));

                    }

                } catch (JSONException e) {
//                    isMoreData = false;
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

//                isMoreData = true;

            } else {

//                isMoreData = false;

            }

            emojiconEditText.setText("");
//            isMoreData = true;
//            Global.wallCommentModels = new ArrayList<WallCommentModel>();
            count = 1;
            // count += 10;
            commentList(count);
//            while (count > tempCount) {
//                if (isMoreData) {
//                    tempCount += 10;
//
//                } else {
//
//                    tempCount = 0;
//
//                }
//
//            }

//             myWallAdapter.notifyDataSetChanged();
//            lst_comment.onLoadMoreComplete();

        }

        if (ServiceResource.CMT_METHODNAME.equalsIgnoreCase(methodName)) {

            Global.wallCommentModels.clear();

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

//                isMoreData = true;

                try {

                    JSONArray mainJsonArray = new JSONArray(result);


                    for (int i = 0; i < mainJsonArray.length(); i++) {

                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        WallCommentModel wallCommentModel = new WallCommentModel();

                        wallCommentModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));

                        wallCommentModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallCommentModel.setCommentID(innerObj
                                .getString(ServiceResource.WALL_COMMENTID));

                        wallCommentModel.setComment(innerObj
                                .getString(ServiceResource.WALL_COMMENT));

                        wallCommentModel.setCommentOn(innerObj
                                .getString(ServiceResource.WALL_COMMENTON));

                        wallCommentModel.setIsDisLike(innerObj.getString(ServiceResource.WALL_COMMENTISDISLIKE));

                        wallCommentModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));

                        wallCommentModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));

                        wallCommentModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));

                        if (wallCommentModel.getProfilePicture() != null
                                && !wallCommentModel.getProfilePicture()
                                .equals("")) {

                            if (wallCommentModel.getProfilePicture().contains(
                                    "/img/"))
                                wallCommentModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallCommentModel
                                                .getProfilePicture()
                                        );
                            else

                                wallCommentModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallCommentModel
                                                .getProfilePicture());

                        }

                        wallCommentModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallCommentModel.setTotalReplies(innerObj
                                .getString(ServiceResource.WALL_TOTALREPLIES));
                        wallCommentModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));
                        wallCommentModel.setRow_ID(innerObj
                                .getString(ServiceResource.WALL_ROW_ID));

                        Global.wallCommentModels.add(wallCommentModel);


                    }

                } catch (JSONException e) {

                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

//                isMoreData = true;


            } else {

//                isMoreData = false;

            }

            if (Global.wallCommentModels != null
                    && Global.wallCommentModels.size() > 0) {

                tv_no_comment.setVisibility(View.GONE);
                lst_comment.setVisibility(View.VISIBLE);
                wallCommentAdapter = new WallCommentAdapter(mContext,
                        Global.wallCommentModels);
                lst_comment.setAdapter(wallCommentAdapter);

            } else {

                tv_no_comment.setVisibility(View.VISIBLE);
                lst_comment.setVisibility(View.GONE);

            }


            if (isFirst) {

                isFirst = false;
                showSoftKeyboard(((View) emojiconEditText));

            } else {

                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                hideSoftKeyboard();

            }

//            lst_comment.onLoadMoreComplete();

        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("getNotificatinoResult", result);
        }


    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

    }


    /**
     * Shows the soft keyboard
     */

    public void showSoftKeyboard(View view) {

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);

    }

    @Override
    public void shrePublic(WallPostModel wallPostModel, int position) {

    }

    @Override
    public void shreOnlyMe(WallPostModel wallPostModel, int position) {

    }

    @Override
    public void shreFriend(WallPostModel wallPostModel, int position) {

    }

    @Override
    public void shreSpecialFriend(WallPostModel wallPostModel, int position) {

    }

    @Override
    public void clickOnPhoto() {

    }

    @Override
    public void clickOnVideo() {

    }

    @Override
    public void clickOnFriends() {

    }

    @Override
    public void clickOnPost() {

    }

    @Override
    public void onDeletePost() {

    }

    @Override
    public void onEditPost() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (FROM.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.PROFILEWALL);
            ServiceResource.WALLTITLE = "My Wall";
            intent.putExtra("isGotoWall", false);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        } else if (FROM.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.INSTITUTEWALL);
            ServiceResource.WALLTITLE = "Institute wall";
            intent.putExtra("isGotoWall", false);
            mContext.startActivity(intent);
            ((Activity) mContext).finish();
        } else if (FROM.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (FROM.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.STANDARDWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (FROM.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.SUBJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (FROM.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.GROUPWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        } else if (FROM.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
            Intent intent = new Intent(mContext, WallActivity.class);
            intent.putExtra("isFrom", ServiceResource.PROJECTWALL);
            intent.putExtra("isGotoWall", false);
            intent.putExtra("title", ServiceResource.SELECTED_WALL_TITLE);
            mContext.startActivity(intent);
        }


    }


}
