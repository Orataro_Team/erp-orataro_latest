package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.Exam;
import com.edusunsoft.erp.orataro.model.ExamTerms;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by admin on 25-07-2017.
 */

public class ExamResultFragment extends Fragment implements ResponseWebServices {

    private Context mContext;
    private LinearLayout ll_subjectmark, ll_subname, ll_header;
    private LayoutInflater mLayoutInflater;
    private TextView txtsubname;
    private LinearLayout ll_cbscExam;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        View v = inflater.inflate(R.layout.examresult, null);
        mLayoutInflater = inflater;

        ll_subjectmark = (LinearLayout) v.findViewById(R.id.ll_subjectmark);
        ll_subname = (LinearLayout) v.findViewById(R.id.ll_subname);
        ll_header = (LinearLayout) v.findViewById(R.id.ll_header);
        txtsubname = (TextView) v.findViewById(R.id.txtsubname);
        ll_cbscExam = (LinearLayout) v.findViewById(R.id.ll_cbscExam);

        ImageView iv_std_icon = (ImageView) v.findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) v.findViewById(R.id.tv_std_name);
        TextView tv_institutename = (TextView) v.findViewById(R.id.tv_schoolname);
        TextView tv_std_standard = (TextView) v.findViewById(R.id.tv_std_standard);
        TextView tv_std_division = (TextView) v.findViewById(R.id.tv_std_division);

        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(iv_std_icon);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        tv_std_name.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME()));
        tv_institutename.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getINSTITUTENAME()));
        tv_std_standard.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_GRADENAME()));
        tv_std_division.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_DIVISIONNAME()));

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.ExamResult) + "(" + Utility.GetFirstName(mContext) + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        examResult();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void examResult() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo("StandardID", new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        arrayList.add(new PropertyVo("DivisionID", new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        arrayList.add(new PropertyVo("ClientID", new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo("InstituteID", new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo("BatchID", new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo("StudentID", new UserSharedPrefrence(mContext).getLoginModel().getERPMemberID()));
        arrayList.add(new PropertyVo("FormateType", "8020Format"));

        Log.d("zcxcdvxv", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETCBSEFINALMARKSHEETSTUDENTWISE_METHODNAME, ServiceResource.FEES_URL);

    }

    @Override
    public void response(String result, String methodName) {

        Log.d("getResult", result);

        try {

            JSONObject mainJSONObj = new JSONObject(result);
            String examType = mainJSONObj.getString("ExamType");

            if (!examType.equalsIgnoreCase("SemesterExam")) {

                JSONArray termarray = mainJSONObj.getJSONArray("Table");
                JSONArray termarrayshort = mainJSONObj.getJSONArray("Table1");
                JSONArray termarrayDetail = mainJSONObj.getJSONArray("Table2");

                ArrayList<Exam> examlist = new ArrayList<>();

                for (int i = 0; i < termarray.length(); i++) {

                    JSONObject jsonobj = termarray.getJSONObject(i);
                    Type collectionType = new TypeToken<Exam>() {
                    }.getType();
                    Exam exam = new Gson().fromJson(String.valueOf(jsonobj), collectionType);
                    examlist.add(exam);

                }

                JSONObject gettermlenght = termarrayshort.getJSONObject(0);
                HashMap<String, String> maptermlenght = new HashMap<>();
                Iterator<String> iterator = gettermlenght.keys();

                while (iterator.hasNext()) {

                    String key = iterator.next();
                    Log.i("TAG", "key:" + key + "--Value::" + gettermlenght.optString(key));
                    maptermlenght.put(key, gettermlenght.optString(key));

                }

                ArrayList<HashMap<String, String>> sublist = new ArrayList<>();
                ArrayList<ExamTerms> alldatelist = new ArrayList<>();

                for (int j = 0; j < termarrayDetail.length(); j++) {

                    JSONObject subjectwise = termarrayDetail.getJSONObject(j);
                    HashMap<String, String> subjectwisemap = new HashMap<>();
                    Iterator<String> iteratorsubwise = subjectwise.keys();
                    while (iteratorsubwise.hasNext()) {
                        String key = iteratorsubwise.next();
                        Log.i("TAG", "key:" + key + "--Value::" + subjectwise.optString(key));
                        subjectwisemap.put(key, subjectwise.optString(key));
                    }

                    ExamTerms examterms = new ExamTerms();

                    examterms.setStudentID(subjectwisemap.get("StudentID"));
                    examterms.setGRNo(subjectwisemap.get("GRNo"));
                    examterms.setStudentName(subjectwisemap.get("StudentName"));
                    examterms.setExamSeatNo(subjectwisemap.get("ExamSeatNo"));
                    examterms.setSubjectName(subjectwisemap.get("SubjectName"));
                    examterms.setPaperTypeName("PaperTypeName");
                    ArrayList<ExamTerms.Term> terms = new ArrayList<>();
                    for (int k = 0; k < examlist.size(); k++) {

                        ExamTerms.Term term = new ExamTerms.Term();
                        term.setTotalTerm(subjectwisemap.get(examlist.get(k).getExamName() + "_Total_" + examlist.get(k).getExamName()));// "Term - 1_Total_Term - 1": 100.00, "Term - 2_Total_Term - 2": 80.00,
                        term.setConvertedTerm(subjectwisemap.get(examlist.get(k).getExamName() + "_Converted_" + examlist.get(k).getExamName()));//  "Term - 1_Converted_Term - 1": 80.00, "Term - 2_Converted_Term - 2": 64.00,
                        term.setOtherExmObt(subjectwisemap.get(examlist.get(k).getExamName() + "_OtherExmObt"));// "Term - 1_OtherExmObt": 280.00, "Term - 2_OtherExmObt": 310.00,
                        term.setOtherExmCObt(subjectwisemap.get(examlist.get(k).getExamName() + "_OtherExmCObt"));// "Term - 1_OtherExmCObt": 14.30,  "Term - 2_OtherExmCObt": 15.30,
                        term.setTotalSubConverted(subjectwisemap.get(examlist.get(k).getExamName() + "_TotalSubConverted"));// "Term - 1_TotalSubConverted": 94.30,"Term - 2_TotalSubConverted": 79.30,
                        term.setWholeTotalMarks(subjectwisemap.get(examlist.get(k).getExamName() + "_WholeTotalMarks"));//  "Term - 1_WholeTotalMarks": 200.00,  "Term - 2_WholeTotalMarks": 200.00,
                        term.setTotalObtained(subjectwisemap.get(examlist.get(k).getExamName() + "_TotalObtained"));// "Term - 1_TotalObtained": 179.72,"Term - 2_TotalObtained": 171.40,
                        term.setResultTerm(subjectwisemap.get(examlist.get(k).getExamName() + "_ResultTerm"));// "Term - 1_ResultTerm": "Pass",  "Term - 2_ResultTerm": "Pass",
                        term.setClass_(subjectwisemap.get(examlist.get(k).getExamName() + "_Class"));//  "Term - 1_Class": "A+++",  "Term - 2_Class": "A+++",
                        term.setPercentage(subjectwisemap.get(examlist.get(k).getExamName() + "_Percentage"));// "Term - 1_Percentage": 89.86,"Term - 2_Percentage": 85.70,
                        term.setPercentile(subjectwisemap.get(examlist.get(k).getExamName() + "_Percentile"));//"Term - 1_Percentile": 66.67, "Term - 2_Percentile": 66.67,
                        term.setRank(subjectwisemap.get(examlist.get(k).getExamName() + "_Rank"));// "Term - 1_Rank": 1,"Term - 2_Rank": 1
                        term.setSubjectGrade(subjectwisemap.get(examlist.get(k).getExamName() + "_SubjectGrade"));
                        terms.add(term);
                    }
                    examterms.setTerms(terms);
                    alldatelist.add(examterms);
                    sublist.add(subjectwisemap);
                }

                for (int l = 0; l < alldatelist.size(); l++) {
                    View bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                    TextView txtSubname = (TextView) bottamview.findViewById(R.id.txtview);
                    txtSubname.setText(alldatelist.get(l).getSubjectName());
                    ll_subname.addView(txtSubname);
                    LinearLayout layout = new LinearLayout(mContext);
                    layout.setOrientation(LinearLayout.HORIZONTAL);
                    layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    LinearLayout layouttxt = new LinearLayout(mContext);
                    layouttxt.setOrientation(LinearLayout.VERTICAL);
                    layouttxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

                    for (int m = 0; m < maptermlenght.size(); m++) {
                        View view = mLayoutInflater.inflate(R.layout.examresultsubjectwiseshell, null);

                        View headerview = view.findViewById(R.id.ll_headerview);
                        TextView txttermheader = (TextView) view.findViewById(R.id.txttermheader);
                        TextView txttermcol = (TextView) view.findViewById(R.id.txttermcol);
                        TextView txtterm = (TextView) view.findViewById(R.id.txtterm);
                        TextView txtother = (TextView) view.findViewById(R.id.txtother);
                        TextView txttotal = (TextView) view.findViewById(R.id.txttotal);
                        TextView txtgradevalue = (TextView) view.findViewById(R.id.txtgradevalue);

                        LinearLayout ll_bottam = (LinearLayout) view.findViewById(R.id.ll_bottam);
                        LinearLayout ll_bottom_container = (LinearLayout) view.findViewById(R.id.ll_bottom_container);

                        txtterm.setText(alldatelist.get(l).getTerms().get(m).getConvertedTerm());
                        txtother.setText(alldatelist.get(l).getTerms().get(m).getOtherExmCObt());
                        txttotal.setText(alldatelist.get(l).getTerms().get(m).getTotalSubConverted());
                        txtgradevalue.setText(alldatelist.get(l).getTerms().get(m).getSubjectGrade());

                        if (l == alldatelist.size() - 1) {

                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView txtwhole = (TextView) bottamview.findViewById(R.id.txtview);
                            txtwhole.setText(alldatelist.get(l).getTerms().get(m).getWholeTotalMarks());
                            ll_bottam.addView(txtwhole);

                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView resultterm = (TextView) bottamview.findViewById(R.id.txtview);
                            resultterm.setText(alldatelist.get(l).getTerms().get(m).getResultTerm());
                            ll_bottam.addView(resultterm);


                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView txtclass = (TextView) bottamview.findViewById(R.id.txtview);
                            txtclass.setText(alldatelist.get(l).getTerms().get(m).getClass_());
                            ll_bottam.addView(txtclass);

                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView percantage = (TextView) bottamview.findViewById(R.id.txtview);
                            percantage.setText(alldatelist.get(l).getTerms().get(m).getPercentage());
                            ll_bottam.addView(percantage);

                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView percentile = (TextView) bottamview.findViewById(R.id.txtview);
                            percentile.setText(alldatelist.get(l).getTerms().get(m).getPercentile());
                            ll_bottam.addView(percentile);

                            bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView rank = (TextView) bottamview.findViewById(R.id.txtview);
                            rank.setText(alldatelist.get(l).getTerms().get(m).getRank());
                            ll_bottam.addView(rank);

                            ll_bottam.setVisibility(View.VISIBLE);
                        }
                        if (l == 0) {
                            txttermheader.setText(examlist.get(m).getExamName());
                            txttermcol.setText(examlist.get(m).getExamName());
                            headerview.setVisibility(View.VISIBLE);
                        }
                        layout.addView(view);
                    }

                    ll_subjectmark.addView(layout);
                }

                View bottamview;
                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView Wholemark = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                Wholemark.setText("Totalmark");
                ll_subname.addView(Wholemark);

                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView Result = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                Result.setText("Result");
                ll_subname.addView(bottamview);

                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView Classtxt = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                Classtxt.setText("Class");
                ll_subname.addView(bottamview);

                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView percentage = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                percentage.setText("Percentage");
                ll_subname.addView(bottamview);

                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView Percentile = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                Percentile.setText("Percentile");
                ll_subname.addView(bottamview);

                bottamview = mLayoutInflater.inflate(R.layout.examtextview, null);
                TextView Rank = (TextView) bottamview.findViewById(R.id.txtview);//new TextView(mContext);
                Rank.setText("Rank");
                ll_subname.addView(bottamview);

                Log.v("list ", sublist.size() + "");

            } else if (examType.equalsIgnoreCase("SemesterExam")) {
                JSONArray termarray = mainJSONObj.getJSONArray("Table");
                ArrayList<HashMap<String, String>> mapList = new ArrayList<>();
                LinearLayout layout = new LinearLayout(mContext);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                for (int i = 0; i < termarray.length(); i++) {
                    JSONObject gettermlenght = termarray.getJSONObject(0);
                    HashMap<String, String> maptermlenght = new HashMap<>();
                    ll_subname.removeAllViews();

                    LinearLayout layouttxt = new LinearLayout(mContext);
                    layouttxt.setOrientation(LinearLayout.VERTICAL);
                    layouttxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    Iterator<String> iterator = gettermlenght.keys();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        Log.i("TAG", "key:" + key + "--Value::" + gettermlenght.optString(key));
                        maptermlenght.put(key, gettermlenght.optString(key));

                        if (!key.toLowerCase().contains("id")) {
                            View view1 = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView txtwhole1 = (TextView) view1.findViewById(R.id.txtview);
                            txtwhole1.setText(key);
                            ll_subname.addView(txtwhole1);

                            View view = mLayoutInflater.inflate(R.layout.examtextview, null);
                            TextView txtwhole = (TextView) view.findViewById(R.id.txtview);
                            txtwhole.setText(gettermlenght.optString(key));
                            layouttxt.addView(txtwhole);
                        }
                    }
                    if (maptermlenght != null) {
                        mapList.add(maptermlenght);
                    }
                    if (layouttxt != null) {
                        layout.addView(layouttxt);
                    }
                }
                if (layout != null) {
                    ll_subjectmark.addView(layout);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TextView getTextView() {
        TextView txtview = new TextView(mContext, null, R.style.examtxtstyletext1);
        return txtview;
    }

}
