package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PtCommunicationChatAdapter;
import com.edusunsoft.erp.orataro.database.PtCommunicationModel;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.ChatModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PtCommunicationDetailActivity extends Activity implements OnClickListener, ResponseWebServices {

    ListView lvptcomunicationchat;
    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    private Context mContext;
    ArrayList<ChatModel> list;
    ImageView sendmessage;
    EditText edt_message;
    PtCommunicationChatAdapter adapter;
    PtCommunicationModel ptcommunicationModel;
    StdDivSubModel standardModel;
    String isReadIdStr = "";
    private Dialog mPoweroffDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ptcommunicationchat);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = PtCommunicationDetailActivity.this;
        ptcommunicationModel = (PtCommunicationModel) getIntent().getSerializableExtra("model");
        standardModel = (StdDivSubModel) getIntent().getSerializableExtra("standardmodel");

        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        edt_message = (EditText) findViewById(R.id.edt_message);
        sendmessage = (ImageView) findViewById(R.id.sendmessage);

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);
        txtHeader.setText(ptcommunicationModel.getCommunicationDetail());

        lvptcomunicationchat = (ListView) findViewById(R.id.lvptcomunicationchat);
        //		addChat();
        String result = Utility.readFromFile(ptcommunicationModel.getCommunicationID(), mContext);
        if (result.equalsIgnoreCase("")) {
            PtcommunicationMessageList(true);
        } else {
            PtcommunicationMessageList(false);
        }

        imgLeftheader.setOnClickListener(this);
        sendmessage.setOnClickListener(this);

    }

    private void addChat() {
        // TODO Auto-generated method stub
        list = new ArrayList<ChatModel>();
        ChatModel model;
        model = new ChatModel();
        model.setDetails("hiii");
        model.setSelf(true);
        model.setTime("1.20am");
        list.add(model);

        model = new ChatModel();
        model.setDetails("hiii");
        model.setSelf(false);
        model.setTime("1.20am");
        list.add(model);


        model = new ChatModel();
        model.setDetails("How R U?");
        model.setSelf(false);
        model.setTime("1.20am");
        list.add(model);


        model = new ChatModel();
        model.setDetails("fine What About u?");
        model.setSelf(true);
        model.setTime("1.20am");
        list.add(model);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_home) {
            finish();
        } else if (v.getId() == R.id.sendmessage) {
            if (!edt_message.getText().toString().equalsIgnoreCase("")) {
                ChatModel model;
                model = new ChatModel();
                model.setDetails(edt_message.getText().toString());

                model.setSelf(true);
                String CurrentTime = Utility.GetCurrentTime();

                model.setTime(CurrentTime);
                list.add(model);
                if (adapter != null) {

                    adapter.notifyDataSetChanged();
                    scrollMyListViewToBottom();

                }

                sendMessage();

            }

        }

    }

    private void scrollMyListViewToBottom() {
        lvptcomunicationchat.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                lvptcomunicationchat.setSelection(adapter.getCount() - 1);
            }

        });

    }

    public void sendMessage() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));
        arrayList.add(new PropertyVo(ServiceResource.PTCOMMUNICATIONDETAILSID, null));
        arrayList.add(new PropertyVo(ServiceResource.PTMESSAGE, edt_message.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PTPOSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));
        arrayList.add(new PropertyVo(ServiceResource.PTCOMMUNICATIONIDPT, ptcommunicationModel.getCommunicationID()));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.WRITECOMMENTSINPTCOMMNUNICATION_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);


    }


    public void PtcommunicationMessageList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.PTCOMMUNICATIONIDPT, ptcommunicationModel.getCommunicationID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.PTCOMMUNICATIONCHATHISTORY_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);

    }

    public void PtcommunicationMessageFlag() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.PTCOMMUNICATIONDETAILIDBYCOMMA, isReadIdStr));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.PTMEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PTCOMMUNICATIONCHATHISTORYSETVIEWFLAG, ServiceResource.PTCOMMUNICATION_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.PTCOMMUNICATIONCHATHISTORY_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, ptcommunicationModel.getCommunicationID(), mContext);
            try {
                JSONArray jsonObj = new JSONArray(result);
                list = new ArrayList<ChatModel>();
                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    ChatModel model;
                    model = new ChatModel();
                    model.setPTCommunicationDetailsID(innerObj.getString(ServiceResource.PTCOMMUNICATIONDETAILSID));
                    model.setCreatedOn(Utility.getDate(Long.valueOf(innerObj
                            .getString(ServiceResource.PTCREATEDON)
                            .replace("/Date(", "").replace(")/", "")), "dd-MM-yyyy hh:mm a"));
                    Log.v("Time", model.getCreatedOn());
                    model.setDetails(innerObj.getString(ServiceResource.PTDETAILS));
                    model.setPostUserType(innerObj.getString(ServiceResource.POSTUSERTYPE));
                    model.setTeacherAcceptance(innerObj.getString(ServiceResource.TEACHERACCEPTANCE));
                    model.setParentAcceptance(innerObj.getString(ServiceResource.PARENTACCEPTANCE));
                    model.setProfilePicture(innerObj.getString(ServiceResource.PTPROFILEPICTURE));
                    model.setMemberType(innerObj.getString(ServiceResource.PTMEMBERTYPE));
                    model.setMemberID(innerObj.getString(ServiceResource.PTMEMBERID));
                    if (innerObj.getString(ServiceResource.PTMEMBERID).equalsIgnoreCase(new UserSharedPrefrence(mContext).getLoginModel().getMemberID())) {
                        model.setSelf(true);
                    } else {
                        model.setSelf(false);
                    }
                    String[] time = model.getCreatedOn().split(" ");
                    if (time != null && time.length > 1) {
                        model.setTime(time[1] + " " + time[2]);
                    }
                    model.setIsParentRead(innerObj.getString(ServiceResource.ISPARENTREAD));
                    model.setIsTeacherRead(innerObj.getString(ServiceResource.ISTEACHERREAD));
                    list.add(model);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            isReadIdStr = "";
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    if (!list.get(i).isSelf()) {
                        if (Utility.isTeacher(mContext)) {
                            if (!list.get(i).getIsTeacherRead()) {
                                isReadIdStr = isReadIdStr + list.get(i).getPTCommunicationDetailsID() + ",";
                            }

                        } else {

                            if (!list.get(i).getIsParentRead()) {
                                isReadIdStr = isReadIdStr + list.get(i).getPTCommunicationDetailsID() + ",";

                            }

                        }

                    }

                }

            }

            if (!isReadIdStr.equalsIgnoreCase("")) {
                PtcommunicationMessageFlag();
            }

            if (list != null && list.size() > 0) {

                adapter = new PtCommunicationChatAdapter(mContext, list);
                lvptcomunicationchat.setAdapter(adapter);

            }

        } else if (ServiceResource.WRITECOMMENTSINPTCOMMNUNICATION_METHODNAME.equalsIgnoreCase(methodName)) {
            edt_message.setText("");
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                scrollMyListViewToBottom();
            } else {
                sendPushNotification(mContext);
//				PtcommunicationMessageList();
            }
        } else if (ServiceResource.PTCOMMUNICATIONCHATHISTORY_METHODNAME.equalsIgnoreCase(methodName)) {

        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {
            PtcommunicationMessageList(false);
        }
    }

    public void sendPushNotification(Context mContext) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);
    }

}
