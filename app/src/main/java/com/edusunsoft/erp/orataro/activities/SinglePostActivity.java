package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.SelectAllFromAdapterInterface;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.adapter.FetchfriendAdapter;
import com.edusunsoft.erp.orataro.adapter.WallCommentAdapter;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.DeviceHardWareId;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingDataDao;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.fragments.FacebookWallFragment2;
import com.edusunsoft.erp.orataro.loadmoreListView.LoadMoreListView;
import com.edusunsoft.erp.orataro.loadmoreListView.LoadMoreListView.OnLoadMoreListener;
import com.edusunsoft.erp.orataro.model.DynamicWallSetting;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.model.WallCommentModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView.OnEmojiconClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import github.ankushsachdeva.emojicon.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

public class SinglePostActivity extends Activity implements OnClickListener,
        SelectAllFromAdapterInterface, ResponseWebServices {
    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;

    TextView txtSubDetail, tv_like, tv_Unlike, timeBeforepost, tv_comment, tv_share;
    AdjustableImageView iv_fb, iv_profilePic;

    LinearLayout ll_like, ll_Unlike, ll_share, ll_comment;

    ImageView iv_like, iv_Unlike;

    Context mContext;
    private boolean isLikeSucess;
    boolean isCall;
    TextView txtUserName;
    private TextView txt_likes, txt_unlikes;
    private TextView txt_comments;
    boolean isSucess;
    Dialog dialog;
    EditText edtTeacherName;
    FetchfriendAdapter fetchfriendAdapter;
    ListView lst_fetch_friend;
    TextView txt_nodatafound;
    ImageView iv_select_all;
    ImageView img_save, img_back;

    WallPostModel wallPostModel;
    int pos = 0;
    boolean isFromNotification;

    private int ShareType;
    private static final int ID_PUBLIC = 1;
    private static final int ID_FRIEND = 2;
    private static final int ID_SP_FRIEND = 3;
    private static final int ID_ONLY_ME = 4;

    QuickAction quickActionForShare;
    ActionItem actionPublic, actionFriend, actionSpFriend, actionOnlyMe;
    String assosiationId, assosiationType;

    Integer count = 1;
    Integer tempCount = 1;
    public boolean isMoreData;
    LoadMoreListView lst_comment;
    EmojiconEditText emojiconEditText;
    WallCommentAdapter wallCommentAdapter;
    private TextView tv_no_comment;
    private RelativeLayout rl_comment;

    DynamicWallSettingDataDao dynamicWallSettingDataDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singlepost);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mContext = this;
        isCall = true;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null && bundle.get("notification") != null) {

            //here can get notification message
            String datas = bundle.get("notification").toString();

        }

        if (getIntent() != null) {

            // userName = getIntent().getStringExtra("username");
            // imgUrl = getIntent().getStringExtra("imgurl");
            // postContent = getIntent().getStringExtra("postcontent");
            // profilePic = getIntent().getStringExtra("profilepic");
            isFromNotification = getIntent().getBooleanExtra("isFrom", false);

            if (isFromNotification == false) {
            }

            pos = Integer.valueOf(getIntent().getIntExtra("pos", 0));
            assosiationId = getIntent().getStringExtra("aId");
            assosiationType = getIntent().getStringExtra("aType");

        }

        if (isFromNotification) {

            Global.userdataModel = new LoginModel();
            new UserSharedPrefrence(mContext).setNOTIFICATIONMESSAGE("");
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();

            if (!ServiceResource.NOTIFICATION_USERID.equalsIgnoreCase("")) {
                try {
                    Log.d("sameuserloggedin", PreferenceData.getCTOKEN());
                    userSelection(ServiceResource.NOTIFICATION_USERID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        lst_comment = (LoadMoreListView) findViewById(R.id.lst_comment);
        txtHeader = (TextView) findViewById(R.id.header_text);

        emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);
        tv_no_comment = (TextView) findViewById(R.id.tv_no_comment);

        ll_like = (LinearLayout) findViewById(R.id.ll_like);
        ll_Unlike = (LinearLayout) findViewById(R.id.ll_Unlike);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ll_comment = (LinearLayout) findViewById(R.id.ll_comment);

        tv_like = (TextView) findViewById(R.id.tv_like);
        tv_Unlike = (TextView) findViewById(R.id.tv_Unlike);

        iv_fb = (AdjustableImageView) findViewById(R.id.iv_fb);

        iv_like = (ImageView) findViewById(R.id.iv_like);
        iv_Unlike = (ImageView) findViewById(R.id.iv_Unlike);
        tv_comment = (TextView) findViewById(R.id.tv_comment);
        tv_share = (TextView) findViewById(R.id.tv_share);

        rl_comment = (RelativeLayout) findViewById(R.id.rl_comment);

        imgLeftHeader.setOnClickListener(this);
        ll_like.setOnClickListener(this);
        ll_Unlike.setOnClickListener(this);
        ll_share.setOnClickListener(this);
        ll_comment.setOnClickListener(this);


        if (Utility.isNetworkAvailable(mContext)) {
            new WallListAsync().execute(assosiationId, assosiationType);
        } else {
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

        if (PreferenceData.getIsLogin() == true) {
            try {
                txtHeader.setText(getResources().getString(R.string.Post) + " (" + Utility.GetFirstName(mContext) + ")");
            } catch (Exception e) {
            }

        }

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);

        final View rootView = findViewById(R.id.root_view);
        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        final ImageView submitButton = (ImageView) findViewById(R.id.postcommentbtn);

        if (Utility.isNetworkAvailable(mContext)) {

            if (wallPostModel != null) {

                Global.wallCommentModels = new ArrayList<WallCommentModel>();
                commentList(1);

            }

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }

        lst_comment.setOnLoadMoreListener(new OnLoadMoreListener() {

            @Override
            public void onLoadMore() {

                if (isMoreData) {

                    count += 10;
                    commentList(count);

                } else {

                    count = 0;

                }

                // new LoadMoreDataTask().execute();

            }
        });

        // Give the topmost view of your activity layout hierarchy. This will be
        // used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        // Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        // If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);

            }

        });

        // If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {

                if (popup.isShowing())

                    popup.dismiss();

            }

        });

        // On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (emojiconEditText == null || emojicon == null) {
                    return;
                }

                int start = emojiconEditText.getSelectionStart();
                int end = emojiconEditText.getSelectionEnd();

                if (start < 0) {

                    emojiconEditText.append(emojicon.getEmoji());

                } else {

                    emojiconEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());

                }

            }

        });

        // On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(0, 0, 0, KeyEvent.KEYCODE_DEL, 0,
                        0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // If popup is not showing => emoji keyboard is not visible, we
                // need to show it
                if (!popup.isShowing()) {
                    // toast("if");
                    // If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        // toast("if1");
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }

                    // else, open the text keyboard first and immediately after
                    // that show the emoji popup

                    else {

                        // toast("else");
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        // popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText,
                                InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton,
                                R.drawable.ic_action_keyboard);
                    }
                }

                // If popup is showing, simply dismiss it to show the undelying
                // text keyboard
                else {
                    // toast("else1");
                    popup.dismiss();
                }
            }
        });

        // On submit, add the edittext text to listview and clear the edittext
        submitButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String newText = emojiconEditText.getText().toString();
                // emojiconEditText.getText().clear();
                if (newText != null && !newText.equalsIgnoreCase("")) {
                    addComment();
                }

            }

        });

    }

    private void userSelection(String userID) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, userID));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_IDENTITY, Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, "android"));

        Log.d("userselectionrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_SELECTION, ServiceResource.REGISTATION_URL);

    }

    public void onCreateRefresh() {

        if (wallPostModel.getPhoto() != null && !wallPostModel.getPhoto().equalsIgnoreCase("")) {

            if (wallPostModel.getFileType().equals("VIDEO")) {

                iv_fb.setVisibility(View.VISIBLE);
                iv_fb.setImageResource(R.drawable.dummy_video);

                if (wallPostModel.getPhoto().contains(".mp3") || wallPostModel.getPhoto().contains(".MP3")) {

                    iv_fb.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200, 200);
                    iv_fb.setLayoutParams(params);

                    try {
                        iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    iv_fb.setImageResource(R.drawable.mpicon);

                }

            } else if (wallPostModel.getFileType().equalsIgnoreCase("FILE")) {

                iv_fb.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200, 200);
                iv_fb.setLayoutParams(params);
                try {
                    iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (wallPostModel.getFileMimeType().equalsIgnoreCase("TEXT") || wallPostModel.getFileMimeType().equalsIgnoreCase("text/plain")) {
                    iv_fb.setImageResource(R.drawable.txt);
                } else if (wallPostModel.getFileMimeType().equalsIgnoreCase("PDF") || wallPostModel.getFileMimeType().equalsIgnoreCase("application/pdf")) {
                    iv_fb.setVisibility(View.VISIBLE);
                    iv_fb.setImageResource(R.drawable.pdf);
                } else if (wallPostModel.getFileMimeType().equalsIgnoreCase("WORD") ||
                        wallPostModel.getFileMimeType().equalsIgnoreCase("application/msword")) {
                    iv_fb.setImageResource(R.drawable.doc);
                } else {
                    iv_fb.setImageResource(R.drawable.file);
                }

            } else if (wallPostModel.getFileType().equalsIgnoreCase("IMAGE")) {

                Log.d("getPhotoFile", wallPostModel.getPhoto());
                if (wallPostModel.getPhoto() != null) {

                    if (wallPostModel.getPhoto() != null
                            && !wallPostModel.getPhoto()
                            .equalsIgnoreCase("")) {

                        try {

                            iv_fb.setVisibility(View.VISIBLE);

                            RequestOptions options = new RequestOptions()
                                    .centerCrop()
                                    .placeholder(R.drawable.photo)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(Priority.HIGH)
                                    .dontAnimate()
                                    .dontTransform();

                            Glide.with(mContext)
                                    .load(wallPostModel.getPhoto())
                                    .into(iv_fb);


                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }

                } else {

                    iv_fb.setVisibility(View.GONE);

                }


            }


        }


        actionPublic = new ActionItem(ID_PUBLIC, mContext.getResources().getString(R.string.public1), mContext
                .getResources().getDrawable(R.drawable.fb_publics));
        actionFriend = new ActionItem(ID_FRIEND, mContext.getResources().getString(R.string.friends), mContext
                .getResources().getDrawable(R.drawable.fb_group));
        actionSpFriend = new ActionItem(ID_SP_FRIEND, mContext.getResources().getString(R.string.specialfriend),
                mContext.getResources().getDrawable(R.drawable.fb_sp_frnd));
        actionOnlyMe = new ActionItem(ID_ONLY_ME, mContext.getResources().getString(R.string.onlyme), mContext
                .getResources().getDrawable(R.drawable.fb_only_me));

        quickActionForShare = new QuickAction(mContext, QuickAction.VERTICAL);

        quickActionForShare.addActionItem(actionPublic);
        quickActionForShare.addActionItem(actionOnlyMe);
//        quickActionForShare.addActionItem(actionFriend);
//        quickActionForShare.addActionItem(actionSpFriend);

        // Set listener for action item clicked
        quickActionForShare
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        ActionItem actionItem = quickActionForShare
                                .getActionItem(pos);

                        if (actionId == ID_PUBLIC) {
                            ShareType = 1;
                            new SharePost().execute(wallPostModel);
                        }
//                        else if (actionId == ID_FRIEND) {
//                            ShareType = 2;
//                            new SharePost().execute(wallPostModel);
//                        } else if (actionId == ID_SP_FRIEND) {
//                            ShareType = 4;
//                            new ShareSpecialFriend().execute(wallPostModel);
//                        }
                        else if (actionId == ID_ONLY_ME) {
                            ShareType = 3;
                            new SharePost().execute(wallPostModel);
                        }

                    }

                });

        quickActionForShare
                .setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        // Toast.makeText(context, "Dismissed",
                        // Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void refreshdata() {

        try {

            if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToLikeDislikes().equalsIgnoreCase("true") &&
                    wallPostModel.getIsAllowPeopleToLikeAndDislikeCommentWall()
                    && wallPostModel.getIsAllowPeopleToLikeOrDislikeOnYourPost()
                    && wallPostModel.getIsAllowLikeDislike()) {

//                && !Global.DynamicWallSetting.getIsAllowPeopleToLikeAndDislikeComment()
//                        && !Global.DynamicWallSetting.getIsAllowLikeDislike()

                ll_like.setVisibility(View.VISIBLE);
                ll_Unlike.setVisibility(View.VISIBLE);

            } else {

                ll_like.setVisibility(View.GONE);
                ll_Unlike.setVisibility(View.GONE);

            }

            if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostComment().equalsIgnoreCase("true")
                    && wallPostModel.getIsAllowPeopleToPostCommentOnPostWall()
                    && wallPostModel.getIsAllowPeopleToPostMessageOnYourWall()
                    && wallPostModel.getIsAllowPostComment()) {


                ll_comment.setVisibility(View.VISIBLE);

            } else {

                ll_comment.setVisibility(View.GONE);

            }

            if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToSharePost().equalsIgnoreCase("true") &&
                    wallPostModel.getIsAllowPeopleToShareCommentWall()
                    && wallPostModel.getIsAllowPeopleToShareYourPost()
                    && wallPostModel.getIsAllowSharePost()) {


                ll_share.setVisibility(View.VISIBLE);

            } else {

                ll_share.setVisibility(View.GONE);

            }

            if (wallPostModel.getIsLike().equalsIgnoreCase("1")) {

                iv_like.setColorFilter(getResources().getColor(R.color.blue));
                tv_like.setTextColor(getResources().getColor(R.color.blue));

            } else {

                iv_like.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                tv_like.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));

            }

            if (wallPostModel.getIsDisLike().equalsIgnoreCase("1")) {

                iv_Unlike.setColorFilter(getResources().getColor(R.color.blue));
                tv_Unlike.setTextColor(getResources().getColor(R.color.blue));

            } else {

                iv_Unlike.setColorFilter(mContext.getResources().getColor(
                        R.color.grey_fb_color));
                tv_Unlike.setTextColor(mContext.getResources().getColor(
                        R.color.grp_txt_grey));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class Likeasyns extends AsyncTask<Integer, Void, Void> {

        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        String result;
        int positionwall;

        boolean islike;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            positionwall = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.LIKE_PDATAID, wallPostModel.getPostCommentID());
            map1.put(ServiceResource.LIKE_PTYPE, wallPostModel.getAssociationType());
            map1.put(ServiceResource.WALLID, wallPostModel.getWallID());
            map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map1.put(ServiceResource.SENDTOMEMBERID, wallPostModel.getSendToMemberID());
            map.put(2, map1);

            if (params[1] == 0) {
                islike = false;
            } else {
                islike = true;
            }
            result = webcall.getJSONFromSOAPWSWithBoolean(
                    ServiceResource.LIKE_METHODNAME, map,
                    ServiceResource.POST_URL, ServiceResource.LIKE_ISLIKEPARAM,
                    islike);
            JSONArray mainJsonArray;

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {
                    mainJsonArray = new JSONArray(result);
                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);

                        wallPostModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallPostModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));

                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        wallPostModel
                                .setPostCommentNote(innerObj
                                        .getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallPostModel.setTotalComments(innerObj
                                .getString(ServiceResource.WALL_TOTALCOMMENTS));
                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        if (params[1] == 0) {
                            wallPostModel.setIsLike("0");
                        } else {
                            wallPostModel.setIsLike("1");

                        }
                        isLikeSucess = true;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    isLikeSucess = false;

                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Utility.sendPushNotification(mContext);
            isCall = true;
            if (isLikeSucess) {

                if (islike) {

                } else {

                }

                refreshdata();

                if (Global.wallPostModels != null && Global.wallPostModels.size() > 0) {

                    Global.wallPostModels.set(pos, wallPostModel);

                }

            }

        }

    }

    public class DisLikeasyns extends AsyncTask<Integer, Void, Void> {

        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        String result;
        int positionwall;

        boolean isDislike;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            positionwall = params[0];
            WebserviceCall webcall = new WebserviceCall();

            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.LIKE_PDATAID,
                    wallPostModel.getPostCommentID());
            map1.put(ServiceResource.LIKE_PTYPE,
                    wallPostModel.getAssociationType());
            map1.put(ServiceResource.SHRE_POST_SHAREPOSTID,
                    wallPostModel.getSendToMemberID());
            // map1.put(ServiceResource.LIKE_ISDISLIKE,wallPostModel.getIsDisLike());
            map1.put(ServiceResource.WALLID, wallPostModel.getWallID());
            map1.put(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.USER_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

            map.put(2, map1);

            if (params[1] == 1) {

                isDislike = true;
            } else {
                isDislike = false;
            }
            result = webcall.getJSONFromSOAPWSWithBoolean(
                    ServiceResource.DISLIKE_METHODNAME, map,
                    ServiceResource.POST_URL, ServiceResource.LIKE_ISDISLIKE,
                    isDislike);

            JSONArray mainJsonArray;

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {
                    mainJsonArray = new JSONArray(result);

                    // JSONArray detailArrray= jsonObj.getJSONArray("");

                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        // wallPostModel = new WallPostModel();

                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));

                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        wallPostModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));

                        wallPostModel
                                .setPostCommentNote(innerObj
                                        .getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallPostModel.setTotalComments(innerObj
                                .getString(ServiceResource.WALL_TOTALCOMMENTS));
                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        isLikeSucess = true;

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    isLikeSucess = false;
                }

            } else {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // ll_Unlike.setEnabled(true);
            // progressDialog.dismiss();
            Utility.sendPushNotification(mContext);
            isCall = true;
            if (isLikeSucess) {
                if (isDislike) {

                } else {

                    // refreshdata();
                }
                refreshdata();
                if (Global.wallPostModels != null && Global.wallPostModels.size() > 0) {
                    Global.wallPostModels.set(pos, wallPostModel);
                }
                // wallList.set(positionwall, wallPostModel);
            }

        }

    }

    public class ShareSpecialFriend extends
            AsyncTask<WallPostModel, Void, Void> implements ICancleAsynkTask {

        //		ProgressDialog progressDialog = null;
        private String result;
        private boolean success;
        WallPostModel wallPostModel;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage("Fetching Friends...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();

//			progressDialog = new ProgressDialog(mContext);
//			progressDialog.setMessage("Fetching Friends...");
//			progressDialog.setCancelable(false);
//			progressDialog.show();

        }

        @Override
        protected Void doInBackground(WallPostModel... params) {
            wallPostModel = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());

            map1.put(ServiceResource.BEATCH_ID, null);

            map.put(2, map1);

            result = webcall.getJSONFromSOAPWS(
                    ServiceResource.FRIENDS_METHODNAME, map,
                    ServiceResource.FRIENDS_URL);

            JSONArray jsonObj;

            try {

                Global.FriendsList = new ArrayList<PersonModel>();

                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    PersonModel model = new PersonModel();
                    model.setPersonName(innerObj
                            .getString(ServiceResource.FRIENDS_FULLNAME));
                    model.setProfileImg(innerObj
                            .getString(ServiceResource.FRIENDS_PROFILEPIC));
                    if (model.getProfileImg() != null
                            && !model.getProfileImg().equals("")) {
                        model.setProfileImg(ServiceResource.BASE_IMG_URL
                                + model.getProfileImg().substring(1,
                                model.getProfileImg().length()));
                    }
                    model.setFriendId(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDID));
                    model.setFriendListID(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDLIST));
                    model.setWallID(innerObj
                            .getString(ServiceResource.FRIENDS_WALLID));
                    Global.FriendsList.add(model);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }

            dialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_fetch_friends, true);

//            dialog = new Dialog(mContext);
//
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            dialog.getWindow().setBackgroundDrawableResource(
//                    android.R.color.transparent);
//            dialog.setContentView(R.layout.dialog_fetch_friends);
//            dialog.setCancelable(true);
//            dialog.show();

            img_save = (ImageView) dialog.findViewById(R.id.img_save);
            img_back = (ImageView) dialog.findViewById(R.id.img_back);

            img_back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            img_save.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    new SharePost().execute(wallPostModel);
                }
            });

            LinearLayout searchlayout = (LinearLayout) dialog
                    .findViewById(R.id.searchlayout);
            searchlayout.setVisibility(View.VISIBLE);

            edtTeacherName = (EditText) dialog
                    .findViewById(R.id.edtsearchStudent);

            lst_fetch_friend = (ListView) dialog
                    .findViewById(R.id.lst_fetch_friend);
            TextView txt_nodatafound = (TextView) dialog
                    .findViewById(R.id.txt_nodatafound);

            if (Global.FriendsList != null && Global.FriendsList.size() > 0) {
                fetchfriendAdapter = new FetchfriendAdapter(mContext,
                        Global.FriendsList, SinglePostActivity.this);
                lst_fetch_friend.setAdapter(fetchfriendAdapter);
                txt_nodatafound.setVisibility(View.GONE);
            } else {
                searchlayout.setVisibility(View.GONE);
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoFriendsAvailable));
            }

            iv_select_all = (ImageView) dialog.findViewById(R.id.iv_select_all);
            iv_select_all.setTag("0");
            iv_select_all.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (iv_select_all.getTag().equals("0")) {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(true);
                                iv_select_all.setTag("1");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(R.color.blue));
                            fetchfriendAdapter.notifyDataSetChanged();
                        }

                    } else {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(false);
                                iv_select_all.setTag("0");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(
                                            R.color.grey_fb_color));

                            fetchfriendAdapter.notifyDataSetChanged();
                        }
                    }

                }
            });

            edtTeacherName.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {

                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                    String text = edtTeacherName.getText().toString()
                            .toLowerCase(Locale.getDefault());
                    fetchfriendAdapter.filter(text);
                }
            });
        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }

    }

    public class SharePost extends AsyncTask<WallPostModel, Void, Void> implements ICancleAsynkTask {

        private String result;
        private boolean success;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {

            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage("Sharing Post...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(WallPostModel... params) {

            try {

                WebserviceCall webcall = new WebserviceCall();
                HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

                HashMap<String, String> map1 = new HashMap<String, String>();
                map1.put(ServiceResource.WALLID, wallPostModel.getWallID());
                map1.put(ServiceResource.MEMBERID,
                        new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
                map1.put(ServiceResource.CLIENT_ID,
                        new UserSharedPrefrence(mContext).getLoginModel().getClientID());
                map1.put(ServiceResource.INSTITUTEID,
                        new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
                map1.put(ServiceResource.USER_ID,
                        new UserSharedPrefrence(mContext).getLoginModel().getUserID());
                map1.put(ServiceResource.SHRE_POST_SHAREPOSTID,
                        params[0].getSendToMemberID());
                map1.put(ServiceResource.SHRE_POST_SHAREPOSTID,
                        params[0].getPostCommentID());

                if (ShareType == 1) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Public");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 2) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Only Me");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 3) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Friend");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 4) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Special Friend");
                    String selectFriend = null;
                    for (int i = 0; i < Global.FriendsList.size(); i++) {

                        if (Global.FriendsList.get(i).isSelected()) {

                            StringBuffer sb = new StringBuffer();
                            sb.append(Global.FriendsList.get(i).getFriendId());
                            sb.append(",");

                            selectFriend = sb.toString();
                            map1.put(ServiceResource.SHRE_POST_TAGID,
                                    selectFriend);
                        }
                    }
                    selectFriend.substring(0, selectFriend.length() - 1);
                }

                map.put(2, map1);

                result = webcall.getJSONFromSOAPWS(
                        ServiceResource.SHRE_POST_METHODNAME, map,
                        ServiceResource.SHRE_POST_URL);

                success = true;

            } catch (Exception e) {
                e.printStackTrace();
                success = false;
            }

            Log.e("dsds", result);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }
            Utility.sendPushNotification(mContext);

            if (success) {

                Utility.toast(mContext, getResources().getString(R.string.ShareSuccessfull));
//                Toast.makeText(mContext, getResources().getString(R.string.ShareSuccessfull), Toast.LENGTH_SHORT).show();

            } else {

                Utility.toast(mContext, getResources().getString(R.string.Error));
//                Toast.makeText(mContext, getResources().getString(R.string.Error), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }

    }

    @Override
    public void selectedFriends(int selectedFriendCount) {
        // TODO Auto-generated method stub
        if (Global.FriendsList.size() == selectedFriendCount) {
            iv_select_all.setColorFilter(mContext.getResources().getColor(
                    R.color.blue));
            iv_select_all.setTag("1");
        } else {
            iv_select_all.setColorFilter(mContext.getResources().getColor(
                    R.color.grey_fb_color));
            iv_select_all.setTag("0");
        }

    }

    public class WallListAsync extends AsyncTask<String, Void, Void> implements ICancleAsynkTask {

        //		private ProgressDialog progressDialog;
        String result;
        JSONObject mJsonObject;
        JSONArray wallListJsonArray;
        JSONArray mainJsonArray;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        protected void onPreExecute() {
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.NOTIFICATION_ASSOCIATIONID, params[0]);
            map1.put(ServiceResource.NOTIFICATION_ASSOCIATIONTYPE,
                    "POST");
            map1.put(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CTOKEN, PreferenceData.getCTOKEN());
            map.put(2, map1);

            Log.d("getPostRequest", map.toString());


            result = webcall.getJSONFromSOAPWS(ServiceResource.GETNOTIFICATIONDETAILS_METHODNAME,
                    map, ServiceResource.NOTIFICATION_URL);

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {

                    Log.d("getreulst", result);

                    mainJsonArray = new JSONArray(result);

                    // JSONArray detailArrray= jsonObj.getJSONArray("");

                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        wallPostModel = new WallPostModel();

                        wallPostModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallPostModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallPostModel.setWallTypeTerm(innerObj.getString(ServiceResource.WALL_TYPE_TERM));

                        wallPostModel.setIsDisLike(innerObj
                                .getString(ServiceResource.WALL_ISDISLIKESCAPITAL));
                        wallPostModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));
                        wallPostModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));
                        wallPostModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));
                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));

                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        wallPostModel.setRowNo(innerObj
                                .getString(ServiceResource.WALL_ROWNO_POST));
                        wallPostModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallPostModel.setWallID(innerObj
                                .getString(ServiceResource.WALL_WALLID));
                        wallPostModel
                                .setPostCommentTypesTerm(innerObj
                                        .getString(ServiceResource.WALL_POSTCOMMENTTYPESTERM));
                        wallPostModel
                                .setPostCommentNote(innerObj
                                        .getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                        wallPostModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallPostModel.setTotalComments(innerObj
                                .getString(ServiceResource.WALL_TOTALCOMMENTS));
                        wallPostModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));
                        wallPostModel.setDateOfPost(innerObj
                                .getString(ServiceResource.WALL_DATEOFPOST));
                        wallPostModel.setAssociationID(innerObj
                                .getString(ServiceResource.WALL_ASSOCIATIONID));
                        wallPostModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));
                        wallPostModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));
                        wallPostModel.setIsDisLike(innerObj
                                .getString(ServiceResource.WALL_ISDISLIKESCAPITAL));
                        wallPostModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));
                        wallPostModel.setSendToMemberID(innerObj
                                .getString(ServiceResource.WALL_SENDTOMEMBERIDSINGLE));
                        wallPostModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));
                        wallPostModel.setPhoto(innerObj
                                .getString(ServiceResource.WALL_PHOTO));


                        wallPostModel.setPostDate(innerObj
                                .getString(ServiceResource.WALL_POSTDATE));

                        if (innerObj.getString(ServiceResource.IS_USER_LIKE).equalsIgnoreCase("1")) {
                            wallPostModel.setUserLike(true);
                        } else {
                            wallPostModel.setUserLike(false);
                        }
                        if (innerObj.getString(ServiceResource.IS_USER_DISLIKE).equalsIgnoreCase("1")) {
                            wallPostModel.setUserDisLike(true);
                        } else {
                            wallPostModel.setUserDisLike(false);
                        }
                        if (innerObj.getString(ServiceResource.IS_USER_COMMENT).equalsIgnoreCase("1")) {
                            wallPostModel.setUserComment(true);
                        } else {
                            wallPostModel.setUserComment(false);
                        }
                        if (innerObj.getString(ServiceResource.IS_USER_SHARE).equalsIgnoreCase("1")) {
                            wallPostModel.setUserShare(true);
                        } else {
                            wallPostModel.setUserShare(false);
                        }
                        wallPostModel.setFileType(innerObj
                                .getString(ServiceResource.WALL_FILETYPE));

                        wallPostModel.setFileMimeType(innerObj
                                .getString(ServiceResource.WALL_FILEMIMETYPE));

                        if (wallPostModel.getPhoto() != null
                                && !wallPostModel.getPhoto().equals(""))
                            wallPostModel.setPhoto(ServiceResource.BASE_IMG_URL
                                    + "DataFiles/"
                                    + wallPostModel.getPhoto());


                        if (wallPostModel.getProfilePicture() != null
                                && !wallPostModel.getProfilePicture()
                                .equals("")) {

                            if (wallPostModel.getProfilePicture().contains(
                                    "/img/"))
                                wallPostModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallPostModel
                                                .getProfilePicture()
                                        );
                            else

                                wallPostModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallPostModel
                                                .getProfilePicture());

                            Log.d("getwallpostmodel",
                                    wallPostModel
                                            .getProfilePicture());


                        }




                        /*commented By Krishna : 01-07-2019 - Set All Permission for Like-Dislike,Comment,share*/

                        //For Like-Dislike

                        wallPostModel.setIsAllowPeopleToLikeAndDislikeCommentWall(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKECOMMENTWALL));
                        wallPostModel.setIsAllowPeopleToLikeOrDislikeOnYourPost(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKEONYOURPOST));
                        wallPostModel.setIsAllowLikeDislike(innerObj.getString(ServiceResource.ISALLOWLIKEDISLIKE));

                        // for Comment

                        wallPostModel.setIsAllowPeopleToPostCommentOnPostWall(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTONPOSTWALL));
                        wallPostModel.setIsAllowPeopleToPostMessageOnYourWall(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOPOSTMESSAGETOWALL));
                        wallPostModel.setIsAllowPostComment(innerObj.getString(ServiceResource.ISALLOWPOSTCOMMENT));

                        // For Share

                        wallPostModel.setIsAllowPeopleToShareCommentWall(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOSHARECOMMENTWALL));
                        wallPostModel.setIsAllowPeopleToShareYourPost(innerObj
                                .getString(ServiceResource.ISALLOWPEOPLETOSHAREYOURPOST));
                        wallPostModel.setIsAllowSharePost(innerObj.getString(ServiceResource.ISALLOWSHAREPOST));

                        /*END*/

                        Global.wallPostModels.add(wallPostModel);


                    }

//                    dynamicSettingtrueall();
                    isSucess = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    isSucess = false;

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void res) {

            super.onPostExecute(res);

            if (LoaderProgress != null) {

                LoaderProgress.dismiss();

            }

            if (isSucess) {

                refreshdata();
                onCreateRefresh();
                Global.wallCommentModels = new ArrayList<WallCommentModel>();
                commentList(1);

            } else {

                finish();

            }

        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }

    }


    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged,
                                         int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    public void toast(String msg) {
        Utility.toast(SinglePostActivity.this, "" + msg);
    }


    public void addComment() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_ASSOCIATIONID,
                wallPostModel.getAssociationID()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_REFCOMMENTID, null));

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_ASSOCIATIONTYPETERM,
                wallPostModel.getAssociationType()));
        arrayList.add(new PropertyVo(ServiceResource.SENDTOMEMBERID,
                wallPostModel.getSendToMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_COMMENT, emojiconEditText
                .getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDCOMMENT_POSTBYTYPE,
                new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));


        //		examresult = webcall.getJSONFromSOAPWS(
        //				ServiceResource.ADDCOMMENT_METHODNAME, map,
        //				ServiceResource.POST_URL);
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDCOMMENT_METHODNAME,
                ServiceResource.POST_URL);
    }

    public void commentList(int count) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_POSTID,
                wallPostModel.getPostCommentID()));
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_WALLID, wallPostModel.getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        arrayList.add(new PropertyVo(ServiceResource.WALL_CMT_ROWNO, String.valueOf(count)));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CMT_METHODNAME,
                ServiceResource.CMT_URL);

    }

    public void morePicList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOTYPE, wallPostModel.getAssociationType()));


//        arrayList.add(new PropertyVo(ServiceResource.ASSOID, "14074dca-bf3b-4de9-959a-121ab0b66e90"));
        arrayList.add(new PropertyVo(ServiceResource.ASSOID, wallPostModel.getAssociationID()));


        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOSTEDPICS_METHODNAME,
                ServiceResource.POST_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.ADDCOMMENT_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {
                JSONArray mainJsonArray;

                try {

                    Utility.sendPushNotification(mContext);
                    mainJsonArray = new JSONArray(result);
                    wallPostModel.setTotalComments(String.valueOf(Integer
                            .valueOf(wallPostModel.getTotalComments()) + 1));
                    // JSONArray detailArrray= jsonObj.getJSONArray("");
                    Global.wallCommentModels = new ArrayList<WallCommentModel>();
                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        WallCommentModel wallCommentModel = new WallCommentModel();

                        wallCommentModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));

                        wallCommentModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallCommentModel.setCommentID(innerObj
                                .getString(ServiceResource.WALL_COMMENTID));

                        wallCommentModel.setComment(innerObj
                                .getString(ServiceResource.WALL_COMMENT));

                        wallCommentModel.setCommentOn(innerObj
                                .getString(ServiceResource.WALL_COMMENTON));

                        wallCommentModel.setIsDisLike(innerObj
                                .getString(ServiceResource.WALL_ISDISLIKE));

                        wallCommentModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));

                        wallCommentModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));

                        wallCommentModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));

                        wallCommentModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));

                        wallCommentModel.setTotalReplies(innerObj
                                .getString(ServiceResource.WALL_TOTALREPLIES));

                        wallCommentModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));


                        wallCommentModel.setPostDate(innerObj
                                .getString(ServiceResource.WALL_POSTDATE));

                        wallCommentModel.setRow_ID(innerObj
                                .getString(ServiceResource.WALL_ROW_ID));


                        Global.wallCommentModels.add(wallCommentModel);

                    }

                } catch (JSONException e) {

                    isMoreData = false;
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

                isMoreData = true;

            } else {

                isMoreData = false;

            }
            emojiconEditText.setText("");
            isMoreData = true;
            Global.wallCommentModels = new ArrayList<WallCommentModel>();
            count = 1;
            // count += 10;
            commentList(count);
            // while (count>tempCount) {
            // if (isMoreData) {
            // tempCount += 10;
            //
            // } else {
            // tempCount = 0;
            // }
            // }

            // myWallAdapter.notifyDataSetChanged();

            lst_comment.onLoadMoreComplete();
        }

        if (ServiceResource.CMT_METHODNAME.equalsIgnoreCase(methodName)) {
            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {

                    JSONArray mainJsonArray = new JSONArray(result);

                    // JSONArray detailArrray= jsonObj.getJSONArray("");

                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        WallCommentModel wallCommentModel = new WallCommentModel();

                        wallCommentModel.setPostCommentID(innerObj
                                .getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallCommentModel
                                .setAssociationType(innerObj
                                        .getString(ServiceResource.WALL_ASSOCIATIONTYPE));

                        wallCommentModel.setCommentID(innerObj
                                .getString(ServiceResource.WALL_COMMENTID));

                        wallCommentModel.setComment(innerObj
                                .getString(ServiceResource.WALL_COMMENT));

                        wallCommentModel.setCommentOn(innerObj
                                .getString(ServiceResource.WALL_COMMENTON));

                        wallCommentModel.setIsDisLike(innerObj
                                .getString(ServiceResource.WALL_ISDISLIKE_COMMENT));
                        wallCommentModel.setIsLike(innerObj
                                .getString(ServiceResource.WALL_ISLIKE));
                        wallCommentModel.setFullName(innerObj
                                .getString(ServiceResource.WALL_FULLNAME));
                        wallCommentModel
                                .setProfilePicture(innerObj
                                        .getString(ServiceResource.WALL_PROFILEPICTURE));

                        if (wallCommentModel.getProfilePicture() != null
                                && !wallCommentModel.getProfilePicture()
                                .equals("")) {

                            if (wallCommentModel.getProfilePicture().contains(
                                    "/img/"))
                                wallCommentModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallCommentModel
                                                .getProfilePicture()
                                        );
                            else
                                wallCommentModel
                                        .setProfilePicture(ServiceResource.BASE_IMG_URL1
                                                + wallCommentModel
                                                .getProfilePicture()
                                        );
                        }

                        wallCommentModel.setTotalLikes(innerObj
                                .getString(ServiceResource.WALL_TOTALLIKES));
                        wallCommentModel.setTotalReplies(innerObj
                                .getString(ServiceResource.WALL_TOTALREPLIES));
                        wallCommentModel.setTotalDislike(innerObj
                                .getString(ServiceResource.WALL_TOTALDISLIKE));

                        // wallCommentModel.setPostDate(innerObj
                        // .getString(ServiceResource.WALL_POSTDATE));
                        wallCommentModel.setRow_ID(innerObj
                                .getString(ServiceResource.WALL_ROW_ID));

                        Global.wallCommentModels.add(wallCommentModel);

                    }

                } catch (JSONException e) {
                    isMoreData = false;
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                isMoreData = true;

            } else {

                isMoreData = false;

            }

            if (Global.wallCommentModels != null
                    && Global.wallCommentModels.size() >= 0) {

                tv_no_comment.setVisibility(View.GONE);
                lst_comment.setVisibility(View.VISIBLE);
                wallCommentAdapter = new WallCommentAdapter(mContext,
                        Global.wallCommentModels, true, wallPostModel, pos);
                lst_comment.setAdapter(wallCommentAdapter);

                if (Global.wallCommentModels.size() == 0) {

                    //	tv_no_comment.setVisibility(View.VISIBLE);

                }

            } else {

                tv_no_comment.setVisibility(View.VISIBLE);
                lst_comment.setVisibility(View.GONE);

            }

            // myWallAdapter.notifyDataSetChanged();

            lst_comment.onLoadMoreComplete();

        } else if (ServiceResource.USER_SELECTION.equalsIgnoreCase(methodName)) {

            if (!UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("NAN") || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("")
                    || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("null") || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals(null)) {

                Global.userdataModel = new LoginModel();

                if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {

                } else {

                    JSONArray jsonObj;
                    try {
                        jsonObj = new JSONArray(result);
                        for (int i = 0; i < jsonObj.length(); i++) {

                            JSONObject innerObj = jsonObj.getJSONObject(i);

                            try {

                                /*store selected user data into loginmodel*/

                                Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                                Log.d("getResult", Global.userdataModel.toString());

                                /*END*/

                                new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);

                                /*END*/

                                // get notification count
                                try {
                                    notificationCount();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            /*END*/

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }


        } else if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONObject obj;
            JSONArray jsonObj;
            JSONArray jsonObjfriend;
            JSONArray jsonObjleave;

            try {

                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                obj = new JSONObject(result);

                jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
                jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);
                jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));

                }

                for (int i = 0; i < jsonObjfriend.length(); i++) {

                    JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));

                }

                for (int i = 0; i < jsonObjleave.length(); i++) {

                    JSONObject innerObj = jsonObjleave.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

            GetUserRoleRightList();

        } else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            try {

                Log.d("getRoleRightsResult", result);

                JSONArray jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).setREADWRITESETTING(result);
                Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
                ReadWriteSettingModel model;
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new ReadWriteSettingModel();
                    model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                    model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));

                    if (!new UserSharedPrefrence(mContext).getLoginModel().getRoleName().equalsIgnoreCase("teacher")) {
                        model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                        model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                        model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                        model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));

                    } else {
                        model.setIsView("true");
                        model.setIsEdit("true");
                        model.setIsCreate("true");
                        model.setIsDelete("true");
                    }

                    Global.readWriteSettingList.add(model);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            saveLoginLog();

        } else if (ServiceResource.SAVELOGINLOG_METHODNAME.equalsIgnoreCase(methodName)) {

            changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

        } else if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            ERPOrataroDatabase.getERPOrataroDatabase(mContext).clearAllTables();
            PreferenceData.setIsSwitched(false);

            try {
                txtHeader.setText(getResources().getString(R.string.Post) + " (" + Utility.GetFirstName(mContext) + ")");
            } catch (Exception e) {
            }

        } else if (ServiceResource.GETPOSTEDPICS_METHODNAME.equalsIgnoreCase(methodName)) {

            ArrayList<String> PhotoArraylist = new ArrayList<>();

            try {

                JSONArray array = new JSONArray(result);

                for (int i = 0; i < array.length(); i++) {

                    JSONObject obj = array.getJSONObject(i);
                    String photo = obj.getString(ServiceResource.PHOTO);
                    Log.v("Photofromapi", photo);
                    PhotoArraylist.add(photo);

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    public void notificationCount() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, Global.userdataModel.getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.NOTIFICATIONCOUNT_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

    public void GetUserRoleRightList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, new UserSharedPrefrence(mContext).getLoginModel().getRoleID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);

    }


    public void saveLoginLog() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SAVELOGINLOG_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void changeGCMID(String UserId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.img_home:

                if (isFromNotification) {


//                    Intent i = new Intent(this, HomeWorkFragmentActivity.class);
//                    i.putExtra("position", getResources().getString(R.string.Wall));
//                    startActivity(i);
                    finish();

                } else {

                    // need to revert for back issue
//                    finish();

                    Intent i = new Intent(this, HomeWorkFragmentActivity.class);
                    i.putExtra("position", getResources().getString(R.string.Wall));
                    i.putExtra("reload", true);
                    startActivity(i);
                    finish();
                }

                break;

            case R.id.ll_comment:

                Intent i = new Intent(mContext, CommentActivity.class);
                i.putExtra("WallComment", wallPostModel);
                i.putExtra("from", "");
                mContext.startActivity(i);

                break;

            case R.id.ll_like:

                if (isCall) {

                    try {

                        isCall = false;

                        if (wallPostModel.getIsLike().equalsIgnoreCase("1")) {

                            iv_like.setColorFilter(mContext.getResources().getColor(
                                    R.color.grey_fb_color));
                            tv_like.setTextColor(mContext.getResources().getColor(
                                    R.color.grp_txt_grey));
                            wallPostModel.setIsLike("0");

                            if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
                                new Likeasyns().execute(0, 0);
                            }
                            if (wallPostModel.getTotalLikes() != null
                                    && Integer.parseInt(wallPostModel.getTotalLikes()) > 0
                                    && !wallPostModel.getTotalLikes().equals("")) {

                                if (Integer.parseInt(wallPostModel.getTotalLikes()) > 1) {
                                    wallPostModel.setTotalLikes((Integer.valueOf(wallPostModel
                                            .getTotalLikes()) - 1) + "");

                                } else {
                                    wallPostModel.setTotalLikes((Integer.valueOf("0")) + "");
//

                                }


                            } else {


                            }
                        } else {

                            iv_like.setColorFilter(mContext.getResources().getColor(
                                    R.color.blue));
                            tv_like.setTextColor(mContext.getResources().getColor(
                                    R.color.blue));
                            wallPostModel.setIsLike("1");
                            if (wallPostModel.getIsDisLike().equalsIgnoreCase("1")) {
                                wallPostModel.setIsDisLike("0");
                                iv_Unlike.setColorFilter(mContext.getResources()
                                        .getColor(R.color.grey_fb_color));
                                tv_Unlike.setTextColor(mContext.getResources()
                                        .getColor(R.color.grp_txt_grey));
                                wallPostModel.setTotalDislike(""
                                        + (Integer.valueOf(wallPostModel
                                        .getTotalDislike()) - 1));

                            }
                            if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
                                new Likeasyns().execute(0, 1);
                            }
                            if (wallPostModel.getTotalLikes() != null
                                    && Integer.parseInt(wallPostModel.getTotalLikes()) > 0
                                    && !wallPostModel.getTotalLikes().equals("")) {

                                if (Integer.parseInt(wallPostModel.getTotalLikes()) > 0) {
                                    //							txt_likes.setText((Integer.valueOf(wallPostModel
                                    //									.getTotalLikes()) + 1) + " likes");
                                    wallPostModel.setTotalLikes((Integer.valueOf(wallPostModel
                                            .getTotalLikes()) + 1) + "");
                                } else {
                                    //							txt_likes.setText((Integer.valueOf(wallPostModel
                                    //									.getTotalLikes()) + 1) + " likes");
                                }

                                // iv_like.setColorFilter(getResources().getColor(R.color.blue));
                                // tv_like.setTextColor(getResources().getColor(R.color.blue));
                                //						txt_likes.setVisibility(View.VISIBLE);
                            } else {
                                //						txt_likes.setText((Integer.valueOf(wallPostModel
                                //								.getTotalLikes()) + 1) + " like");
                                wallPostModel.setTotalLikes((Integer.valueOf(wallPostModel
                                        .getTotalLikes()) + 1) + "");
                                //						txt_likes.setVisibility(View.VISIBLE);
                            }

                            if (wallPostModel.getIsDisLike().equalsIgnoreCase("1")) {

                                wallPostModel.setIsDisLike("0");
                                iv_Unlike.setColorFilter(mContext.getResources()
                                        .getColor(R.color.grey_fb_color));
                                tv_Unlike.setTextColor(mContext.getResources()
                                        .getColor(R.color.grp_txt_grey));

                            }

                        }
                        wallCommentAdapter.notifyDataSetChanged();
                        refreshdata();
                        if (Global.wallPostModels != null && Global.wallPostModels.size() > 0) {
                            Global.wallPostModels.set(pos, wallPostModel);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case R.id.ll_Unlike:
                if (isCall) {
                    isCall = false;
                    if (wallPostModel.getIsDisLike().equalsIgnoreCase("1")) {

                        iv_Unlike.setColorFilter(mContext.getResources().getColor(
                                R.color.grey_fb_color));
                        tv_Unlike.setTextColor(mContext.getResources().getColor(
                                R.color.grp_txt_grey));
                        wallPostModel.setIsDisLike("0");
                        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
                            new DisLikeasyns().execute(0, 0);
                        }
                        if (wallPostModel.getTotalDislike() != null
                                && Integer
                                .parseInt(wallPostModel.getTotalDislike()) > 0
                                && !wallPostModel.getTotalDislike().equals("")) {

                            if (Integer.parseInt(wallPostModel.getTotalDislike()) > 1) {
                                //							txt_unlikes.setText((Integer.valueOf(wallPostModel
                                //									.getTotalDislike()) - 1) + " unlikes");
                                wallPostModel.setTotalDislike((Integer.valueOf("0")) + "");
                                //							txt_unlikes.setVisibility(View.VISIBLE);
                            } else {
                                wallPostModel.setTotalDislike((Integer.valueOf(wallPostModel
                                        .getTotalDislike()) - 1) + "");
//														txt_unlikes.setVisibility(View.GONE);
                            }

                        } else {

                            // txt_unlikes.setVisibility(View.GONE);

                        }

                    } else {

                        iv_Unlike.setColorFilter(mContext.getResources().getColor(
                                R.color.blue));
                        tv_Unlike.setTextColor(mContext.getResources().getColor(
                                R.color.blue));
                        wallPostModel.setIsDisLike("1");
                        if (wallPostModel.getIsLike().equalsIgnoreCase("1")) {
                            wallPostModel.setTotalLikes(""
                                    + (Integer.valueOf(wallPostModel
                                    .getTotalLikes()) - 1));
                            wallPostModel.setIsLike("0");
                            iv_like.setColorFilter(mContext.getResources()
                                    .getColor(R.color.grey_fb_color));
                            tv_like.setTextColor(mContext.getResources().getColor(
                                    R.color.grp_txt_grey));
                        }
                        if (new UserSharedPrefrence(mContext).getLoginModel() != null) {
                            new DisLikeasyns().execute(0, 1);
                        }
                        if (wallPostModel.getTotalDislike() != null
                                && Integer
                                .parseInt(wallPostModel.getTotalDislike()) > 0
                                && !wallPostModel.getTotalDislike().equals("")) {

                            if (Integer.parseInt(wallPostModel.getTotalDislike()) > 1) {
                                //							txt_unlikes.setText((Integer.valueOf(wallPostModel
                                //									.getTotalDislike()) + 1) + " unlikes");
                                wallPostModel.setTotalDislike((Integer.valueOf(wallPostModel
                                        .getTotalDislike()) + 1) + "");
                            } else {
                                //							txt_unlikes.setText(Integer.valueOf(wallPostModel
                                //									.getTotalDislike() + 1) + " unlikes");
                                wallPostModel.setTotalDislike((Integer.valueOf(wallPostModel
                                        .getTotalDislike()) + 1) + "");
                            }

                            //						txt_unlikes.setVisibility(View.VISIBLE);
                        } else {
                            //						txt_unlikes.setVisibility(View.VISIBLE);
                            //						txt_unlikes.setText(Integer.valueOf(wallPostModel
                            //								.getTotalDislike() + 1) + " unlike");
                            wallPostModel.setTotalDislike((Integer.valueOf(wallPostModel
                                    .getTotalDislike()) + 1) + "");

                        }

                    }
                    refreshdata();
                    wallCommentAdapter.notifyDataSetChanged();
                    if (Global.wallPostModels != null && Global.wallPostModels.size() > 0) {
                        Global.wallPostModels.set(pos, wallPostModel);
                    }
                }

                break;

            case R.id.ll_share:

                quickActionForShare.show(v);

                break;

            default:
                break;

        }

    }

}
