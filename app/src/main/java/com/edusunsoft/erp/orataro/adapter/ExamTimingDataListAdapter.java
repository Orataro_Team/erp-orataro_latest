package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ExamTimingModel;

import java.util.ArrayList;
import java.util.List;

public class ExamTimingDataListAdapter extends RecyclerView.Adapter<ExamTimingDataListAdapter.MyViewHolder> {

    public List<ExamTimingModel> ExamTimingList;
    public Context mcontext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_no, txt_date, txt_subjects, txt_examtype, txttime;

        public MyViewHolder(View view) {

            super(view);

            txt_no = (TextView) view.findViewById(R.id.txt_no);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            txt_subjects = (TextView) view.findViewById(R.id.txt_subjects);
            txt_examtype = (TextView) view.findViewById(R.id.txt_examtype);
            txttime = (TextView) view.findViewById(R.id.txttime);

        }

    }


    public ExamTimingDataListAdapter(Context mContext, ArrayList<ExamTimingModel> ExamList) {
        this.mcontext = mContext;
        this.ExamTimingList = ExamList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_timing_inner_view, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ExamTimingModel examtimingModel = ExamTimingList.get(position);

        /*Fillup Detail with Validation */

        holder.txt_no.setText(String.valueOf(position + 1));

        if (examtimingModel.getDateOfExam().equalsIgnoreCase("") || examtimingModel.getDateOfExam().equalsIgnoreCase("null") ||
                examtimingModel.getDateOfExam().equalsIgnoreCase(null)) {
            holder.txt_date.setText("");
        } else {
            holder.txt_date.setText(examtimingModel.getDateOfExam());
        }

        if (examtimingModel.getSubjectName().equalsIgnoreCase("") || examtimingModel.getSubjectName().equalsIgnoreCase("null") ||
                examtimingModel.getSubjectName().equalsIgnoreCase(null)) {
            holder.txt_subjects.setText("");
        } else {
            holder.txt_subjects.setText(examtimingModel.getSubjectName());
        }

        if (examtimingModel.getPaperTypeName().equalsIgnoreCase("") || examtimingModel.getPaperTypeName().equalsIgnoreCase("null") ||
                examtimingModel.getPaperTypeName().equalsIgnoreCase(null)) {
            holder.txt_examtype.setText("");
        } else {
            holder.txt_examtype.setText(examtimingModel.getPaperTypeName());
        }

        holder.txttime.setText(examtimingModel.getStartTime() + " To " + examtimingModel.getEndTime());
        /*END*/

    }

    @Override
    public int getItemCount() {

        return ExamTimingList.size();

    }

}