package com.edusunsoft.erp.orataro.model;

import java.util.ArrayList;

public class QuestionModel {

    private String question, rightAns;
    private ArrayList<String> ansList;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRightAns() {
        return rightAns;
    }

    public void setRightAns(String rightAns) {
        this.rightAns = rightAns;
    }

    public ArrayList<String> getAnsList() {
        return ansList;
    }

    public void setAnsList(ArrayList<String> ansList) {
        this.ansList = ansList;
    }

}
