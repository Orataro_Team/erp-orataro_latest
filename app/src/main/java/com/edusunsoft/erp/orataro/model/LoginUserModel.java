
package com.edusunsoft.erp.orataro.model;


public class LoginUserModel {

    private String loginJson;
    private boolean save;
     String UserID= "";
     String UserName= "";
     String Password= "";
     String DisplayName= "";
     String MemberName= "";
     String UserType_Term = "";
     String AssociationType_Term = "";
     Boolean IsActive ;
     String ClientID = "";
     String InstituteID = "";
     String Avatar;
     String MobileNo = "";
     String OTPNumber = "";
     Object EmailID;
     String ERPMemberID = "";
     Object ERPUserID;
     Object GCMID;
     String InstituteCode = "";
     String StudentCode = "";
     String MemberID = "";
     String MobileNumber = "";
     String PrimaryName = "";
     String StandardName = "";
     String DivisionName = "";
     Object RoleName = "";
     public String SQLConStr = "null";
     String InstituteCode1 = "";
     String InstituteName = "";
     String MediumOfEducation_Term = "";
     String InstituteType_Term = "";

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public String getLoginJson() {
        return loginJson;
    }

    public void setLoginJson(String loginJson) {
        this.loginJson = loginJson;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getUserType_Term() {
        return UserType_Term;
    }

    public void setUserType_Term(String UserType_Term) {
        this.UserType_Term = UserType_Term;
    }

    public String getAssociationType_Term() {
        return AssociationType_Term;
    }

    public void setAssociationType_Term(String AssociationType_Term) {
        this.AssociationType_Term = AssociationType_Term;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String ClientID) {
        this.ClientID = ClientID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String InstituteID) {
        this.InstituteID = InstituteID;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }

    public String getOTPNumber() {
        return OTPNumber;
    }

    public void setOTPNumber(String OTPNumber) {
        this.OTPNumber = OTPNumber;
    }

    public Object getEmailID() {
        return EmailID;
    }

    public void setEmailID(Object EmailID) {
        this.EmailID = EmailID;
    }

    public String getERPMemberID() {
        return ERPMemberID;
    }

    public void setERPMemberID(String ERPMemberID) {
        this.ERPMemberID = ERPMemberID;
    }

    public Object getERPUserID() {
        return ERPUserID;
    }

    public void setERPUserID(Object ERPUserID) {
        this.ERPUserID = ERPUserID;
    }

    public Object getGCMID() {
        return GCMID;
    }

    public void setGCMID(Object GCMID) {
        this.GCMID = GCMID;
    }

    public String getInstituteCode() {
        return InstituteCode;
    }

    public void setInstituteCode(String InstituteCode) {
        this.InstituteCode = InstituteCode;
    }

    public String getStudentCode() {
        return StudentCode;
    }

    public void setStudentCode(String StudentCode) {
        this.StudentCode = StudentCode;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String MemberID) {
        this.MemberID = MemberID;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String MobileNumber) {
        this.MobileNumber = MobileNumber;
    }

    public String getPrimaryName() {
        return PrimaryName;
    }

    public void setPrimaryName(String PrimaryName) {
        this.PrimaryName = PrimaryName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String StandardName) {
        this.StandardName = StandardName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String DivisionName) {
        this.DivisionName = DivisionName;
    }

    public Object getRoleName() {
        return RoleName;
    }

    public void setRoleName(Object RoleName) {
        this.RoleName = RoleName;
    }

    public String getSQLConStr() {
        return SQLConStr;
    }

    public void setSQLConStr(String SQLConStr) {
        this.SQLConStr = SQLConStr;
    }

    public String getInstituteCode1() {
        return InstituteCode1;
    }

    public void setInstituteCode1(String InstituteCode1) {
        this.InstituteCode1 = InstituteCode1;
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    public String getMediumOfEducation_Term() {
        return MediumOfEducation_Term;
    }

    public void setMediumOfEducation_Term(String MediumOfEducation_Term) {
        this.MediumOfEducation_Term = MediumOfEducation_Term;
    }

    public String getInstituteType_Term() {
        return InstituteType_Term;
    }

    public void setInstituteType_Term(String InstituteType_Term) {
        this.InstituteType_Term = InstituteType_Term;
    }

    @Override
    public String toString() {
        return "LoginUserModel{" +
                "userID='" + UserID + '\'' +
                ", userName='" + UserName + '\'' +
                ", password='" + Password + '\'' +
                ", displayName='" + DisplayName + '\'' +
                ", memberName='" + MemberName + '\'' +
                ", UserType_Term='" + UserType_Term + '\'' +
                ", AssociationType_Term='" + AssociationType_Term + '\'' +
                ", IsActive=" + IsActive +
                ", ClientID='" + ClientID + '\'' +
                ", InstituteID='" + InstituteID + '\'' +
                ", Avatar=" + Avatar +
                ", MobileNo='" + MobileNo + '\'' +
                ", OTPNumber='" + OTPNumber + '\'' +
                ", EmailID=" + EmailID +
                ", ERPMemberID='" + ERPMemberID + '\'' +
                ", ERPUserID=" + ERPUserID +
                ", GCMID=" + GCMID +
                ", InstituteCode='" + InstituteCode + '\'' +
                ", StudentCode='" + StudentCode + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", MobileNumber='" + MobileNumber + '\'' +
                ", PrimaryName='" + PrimaryName + '\'' +
                ", StandardName='" + StandardName + '\'' +
                ", DivisionName='" + DivisionName + '\'' +
                ", RoleName=" + RoleName +
                ", SQLConStr='" + SQLConStr + '\'' +
                ", InstituteCode1='" + InstituteCode1 + '\'' +
                ", InstituteName='" + InstituteName + '\'' +
                ", MediumOfEducation_Term='" + MediumOfEducation_Term + '\'' +
                ", InstituteType_Term='" + InstituteType_Term + '\'' +
                '}';
    }
}
