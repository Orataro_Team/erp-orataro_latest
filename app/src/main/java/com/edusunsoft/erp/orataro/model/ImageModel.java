package com.edusunsoft.erp.orataro.model;

public class ImageModel {
	private String Uri;
	private boolean selected=false;
	public String getUri() {
		return Uri;
	}
	public void setUri(String uri) {
		Uri = uri;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
