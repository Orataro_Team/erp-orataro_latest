package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.CmsPageListModel;

import org.apache.commons.lang.WordUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class CMSPageAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInfalater;
    private boolean showText;
    private boolean UserGraycolor = false;
    private List<CmsPageListModel> mPagesModels = new ArrayList<>();
    private ArrayList<CmsPageListModel> copyList = new ArrayList<>();
    private int[] colors = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private int[] colors2 = new int[]{Color.parseColor("#F2F2F2"),
            Color.parseColor("#FFFFFF")};
    private boolean isBlog;

    public CMSPageAdapter(Context ctx, List<CmsPageListModel> pagesModels, boolean showText, boolean isBlog) {

        this.context = ctx;
        mPagesModels = pagesModels;
        this.showText = showText;
        this.isBlog = isBlog;
        copyList.addAll(pagesModels);

    }

    public void setcolor(boolean gray_white) {
        UserGraycolor = gray_white;
    }

    @Override
    public int getCount() {
        return mPagesModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mPagesModels.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;
        if (convertView == null) {

            viewHolder = new ViewHolderItem();
            layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfalater.inflate(R.layout.menushell, parent, false);
            viewHolder.menu_item = (ImageView) convertView.findViewById(R.id.menu_item);
            viewHolder.tv_menu = (TextView) convertView.findViewById(R.id.tv_menu);
            viewHolder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolderItem) convertView.getTag();

        }

        viewHolder.tv_menu.setText(WordUtils.capitalize(mPagesModels
                .get(position).getPageName().toLowerCase().replace("_", " ")));
        viewHolder.tv_date.setText(mPagesModels.get(position).getDate());
        viewHolder.tv_menu.setVisibility(View.GONE);
        if (this.showText) {
            viewHolder.tv_menu.setVisibility(View.VISIBLE);
        }

        if (UserGraycolor) {

            int colorPos = position % colors2.length;
            convertView.setBackgroundColor(colors2[colorPos]);

            viewHolder.menu_item.setImageResource(R.drawable.pages);

            if (isBlog) {

                viewHolder.menu_item.setImageResource(R.drawable.blogs);
                viewHolder.menu_item.setTag(R.drawable.blogs);
            } else {
                viewHolder.menu_item.setImageResource(R.drawable.pages);
                viewHolder.menu_item.setTag(R.drawable.pages);
            }
            viewHolder.menu_item.setColorFilter(Color.parseColor("#27305B"));
            viewHolder.tv_menu.setTextColor(Color.BLACK);
        } else {

            int colorPos = position % colors.length;
            convertView.setBackgroundColor(colors[colorPos]);

            if (isBlog) {
                viewHolder.menu_item.setImageResource(R.drawable.blogs);
                viewHolder.menu_item.setTag(R.drawable.blogs);
            } else {
                viewHolder.menu_item.setImageResource(R.drawable.pages);
                viewHolder.menu_item.setTag(R.drawable.pages);
            }
        }

        return convertView;
    }

    public static class ViewHolderItem {

        ImageView menu_item;
        TextView tv_menu, tv_date;

    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        mPagesModels.clear();
        if (charText.length() == 0) {
            mPagesModels.addAll(copyList);
        } else {
            for (CmsPageListModel vo : copyList) {
                if (vo.getPageName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mPagesModels.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

}
