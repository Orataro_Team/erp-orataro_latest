package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class ViewSchoolPrayerDetailsActivity extends Activity implements OnClickListener {

	private Context mContext;
	private ImageView img_back;
	private TextView txt_prayer, txt_title;
	private Intent intent;
	private String title, prayer;
	private int teacher_img;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_school_prayer_details);

		mContext = ViewSchoolPrayerDetailsActivity.this;
		title = getIntent().getStringExtra("Title");
		prayer = getIntent().getStringExtra("Prayer");
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_prayer = (TextView) findViewById(R.id.txt_prayer);
		img_back = (ImageView) findViewById(R.id.img_back);
		txt_title.setText(title);
		txt_prayer.setText(prayer);
		img_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			finish();
			break;
		default:
			break;
		}
	}

}
