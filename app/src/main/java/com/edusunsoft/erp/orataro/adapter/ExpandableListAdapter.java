package com.edusunsoft.erp.orataro.adapter;

/**
 * Created by Android Developer on 17-Jun-16.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ExamDetailActivity;
import com.edusunsoft.erp.orataro.model.ExamModel;
import com.edusunsoft.erp.orataro.util.IconTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<ExamModel>> _listDataChild;
    ArrayList<String> ExamTypeArraylist = new ArrayList<>();
    ArrayList<String> GroupExamList = new ArrayList<>();

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<ExamModel>> listChildData) {

        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ExamModel detailInfo = (ExamModel) getChild(groupPosition, childPosition);

        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);

        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.txt_subject);

        TextView txtDate = (TextView) convertView
                .findViewById(R.id.txtDate);

        TextView txt_examtype = (TextView) convertView
                .findViewById(R.id.txt_examtype);

        txtListChild.setText(detailInfo.getSubjectName());
        txtDate.setText(detailInfo.getStr_DateOfExam());
        txt_examtype.setText(detailInfo.getExamTypeName());


        ExamTypeArraylist.add(detailInfo.getExamTypeName());
        Log.d("getList", ExamTypeArraylist.toString());

        //Converting ArrayList to HashSet to remove duplicates
        HashSet<String> listToSet = new HashSet<String>(ExamTypeArraylist);


        //Creating Arraylist without duplicate values
        List<String> listWithoutDuplicates = new ArrayList<String>(listToSet);
        System.out.println("size of ArrayList without duplicates: " + listWithoutDuplicates.toString()); //should print 3 becaues of duplicates Android removed


        for (int i = 0; i < listWithoutDuplicates.size(); i++) {

            if (listWithoutDuplicates.get(i).equalsIgnoreCase(detailInfo.getExamTypeName())) {

                GroupExamList.add(detailInfo.getExamTypeName());
                Log.d("getExamList", GroupExamList.toString());

            }

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*commented By Krishna : Redirect to ExamDetail Screen*/

                Intent examdetailIntent = new Intent(_context, ExamDetailActivity.class);
                examdetailIntent.putExtra("FromString", "Exam");
                examdetailIntent.putExtra("exadetailmodel", _listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition));
                _context.startActivity(examdetailIntent);

                /*END*/

            }


        });

        return convertView;

    }


    @Override
    public int getChildrenCount(int groupPosition) {

        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();

    }

    @Override
    public Object getGroup(int groupPosition) {

        return this._listDataHeader.get(groupPosition);

    }

    @Override
    public int getGroupCount() {

        return this._listDataHeader.size();

    }

    @Override
    public long getGroupId(int groupPosition) {

        return groupPosition;

    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);

        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        IconTextView iconDown = (IconTextView) convertView.findViewById(R.id.icon_down_arrow);
        IconTextView iconUp = (IconTextView) convertView.findViewById(R.id.icon_up_arrow);

        if (isExpanded) {

            iconDown.setVisibility(View.GONE);
            iconUp.setVisibility(View.VISIBLE);

        } else {

            iconDown.setVisibility(View.VISIBLE);
            iconUp.setVisibility(View.GONE);

        }

        return convertView;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}