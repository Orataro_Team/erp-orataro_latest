/*
 * Copyright (C) 2014 Mukesh Y authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.edusunsoft.erp.orataro.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.edusunsoft.erp.orataro.BuildConfig;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.AddPostOnWallActivity;
import com.edusunsoft.erp.orataro.activities.DashBoardActivity;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.model.WallListVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static android.os.Build.VERSION_CODES.M;


public class Utility {

    public static String CTOKENFROMSTR = "";
    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();
    public static ArrayList<String> endDates = new ArrayList<String>();
    public static ArrayList<String> descriptions = new ArrayList<String>();
    public static Dialog mPoweroffDialog;
    public static Uri FileURI = null;
    public static boolean CompareDate, CompareTime, isCompareDate, isCompareDateForNextDate, isloadAttendance = false;
    public static int ResID;
    public static byte[] byteArray = null;
    public static String Base64String = null;
    public static String NewFileName = "", mCurrentPhotoPath = "";
    public static Bitmap BITMAPOFIMAGE = null;
    public static String BASE64_STRING = null;
    public static boolean ISLOADHOMEWORK = false;
    public static boolean ISLOADCLASSWORK = false;
    public static boolean ISLOADCIRCULAR = false;
    public static boolean ISLOADNOTE = false;
    public static boolean ISLOADGROUP = false;
    public static boolean ISLOADPROJECT = false;
    public static boolean ISLOADLEAVELIST = false;
    public static boolean ISLOADEVENT = false;

    // list declaration for display dynamic wall list into separate screen.
    public static ArrayList<WallListVo> dynamicwalllist = new ArrayList<WallListVo>();

    // variable to check wall header clicked or not to hide or show list of dynamic wall list title.
    public static boolean ISHEADERCLICKED = false;
    /*END*/
    public static final String FILE_PROVIDER = "com.edusunsoft.erp.orataro.fileprovider";


    public static void RedirectToDashboard(Context context) {

        Intent i = new Intent(context, DashBoardActivity.class);
        context.startActivity(i);
        ((Activity) context).finish();
//        context.finish();

    }

    public static void ShowSnackbar(Context context, View view, String Message) {

        Snackbar snackbar = Snackbar.make(view, Message, Snackbar.LENGTH_SHORT);
        snackbar.setAction(context.getResources().getString(R.string.dismiss), new OnClickListener() {
            @Override
            public void onClick(View v) {
            }

        });

        snackbar.show();

    }

    public static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    public static void Longtoast(Context mContext, String msg) {

        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

    }

    public static int GetResIDFromStringXML(Context context, String MenuName, String DefType, String PackageName, TextView txtmenuname) {

        ResID = context.getResources().getIdentifier(MenuName, DefType, PackageName);
        txtmenuname.setText(ResID);
        return ResID;

    }


    public static boolean CompareTime(Context mContext, String StartTime, String EndTime) {

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

        try {

            Date inTime = sdf.parse(StartTime);
            Date outTime = sdf.parse(EndTime);
            int dateDelta = inTime.compareTo(outTime);

            switch (dateDelta) {
                case 0:
                    //startTime and endTime not **Equal**
//                    Utility.toast(mContext, "startTime and endTime not **Equal**");
                    CompareTime = true;
                    break;
                case 1:
//                    Utility.toast(mContext, "Starttime is **Greater** then EndTime");
                    CompareTime = false;
                    //endTime is **Greater** then startTime
                    break;
                case -1:
//                    Utility.toast(mContext, "Endtime is **Greater** then StartTime");
                    //startTime is **Greater** then endTime
                    CompareTime = true;
                    break;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return CompareTime;
    }

    public static void CheckPermission(Activity mContext, String from, LinearLayout ll_photo, DynamicWallSettingModel dynamicWallSetting) {

        if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

            if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                    new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                    new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                    new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                intent.putExtra("isFrom", from);
                mContext.startActivity(intent);
                mContext.overridePendingTransition(0, 0);
            } else {
                Utility.toast(mContext, mContext.getResources().getString(R.string.youhavenotpermission));
            }

            if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {

                ll_photo.setVisibility(View.INVISIBLE);

            }

        } else {

            if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") &&
                    dynamicWallSetting.getIsAllowPeopleToPostStatus() &&
                    dynamicWallSetting.getIsAllowPostStatus()) ||
                    (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") &&
                            dynamicWallSetting.getIsAllowPeoplePostComment() &&
                            dynamicWallSetting.getIsAllowPostPhoto()) ||
                    (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") &&
                            dynamicWallSetting.getIsAllowPeopleToPostVideos() &&
                            dynamicWallSetting.getIsAllowPostVideo()) ||
                    (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) &&
                            dynamicWallSetting.getIsAllowPeopleToPostDocument() &&
                            dynamicWallSetting.getIsAllowPostFile()) {

                Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                intent.putExtra("isFrom", from);
                mContext.startActivity(intent);
                mContext.overridePendingTransition(0, 0);

            } else {

                Utility.toast(mContext, mContext.getResources().getString(R.string.youhavenotpermission));

            }

        }
    }

    public static ArrayList<String> readCalendarEvent(Context context) {
        return nameOfEvent;
    }

    public static String dateFormate(String oldDateString, String NEW_FORMAT, String OLD_FORMAT) {

        String newDateString = "";


        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);

        Date d = null;
        try {
            d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateString;
    }

    public static boolean CompareDates(DateFormat df, Date StartDate, Date EndDate) {

        if (EndDate.compareTo(StartDate) < 0) {

//            System.out.println(df.format(EndDate) + " is greater than " + df.format(StartDate));
            CompareDate = true;

        } else {

            CompareDate = false;

        }

        return CompareDate;

    }

    public static String dateFormatter(String oldDateString, String NEW_FORMAT, String OLD_FORMAT) {

        String newDateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        String dateString = sdf.format(new Date(Long.parseLong(oldDateString)));
        Date d = null;
        try {
            d = sdf.parse(dateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateString;
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static void setImageViewResource(ImageView v, int i, int tiny, Context c) {
        int sdk = Build.VERSION.SDK_INT;
        int jellyBean = Build.VERSION_CODES.KITKAT;
        if (sdk < jellyBean) {
            Drawable drawable = ContextCompat.getDrawable(c, i);
            drawable.setColorFilter(tiny, android.graphics.PorterDuff.Mode.SRC_IN);
            v.setBackgroundResource(i);
        } else {
            v.setBackground(c.getResources().getDrawable(i));
        }

    }

    public static Bitmap getBitmapFromAsset(Context mContext, String strName) {
        AssetManager assetManager = mContext.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }

    public static boolean isNull(String string) {
        if (string != null && !string.equalsIgnoreCase("") && !string.equalsIgnoreCase("null")) {
            return true;
        }
        return false;
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }

        return false;

    }

    public static boolean DateFormatterForAttendance(Context context, String DateForCompare, String DateFormat) {

        Date strDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
        try {
            strDate = sdf.parse(DateForCompare);
            if (System.currentTimeMillis() > strDate.getTime() || System.currentTimeMillis() == strDate.getTime()) {
                isCompareDate = true;
            } else {
                isCompareDate = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return isCompareDate;

    }

    public static void showAlertDialog(Context mContext, String Message, String title) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.error_dialog);

        TextView tv_alert_title = (TextView) dialog.findViewById(R.id.tv_alert_title);
        tv_alert_title.setText(title);
        TextView txtdetail = (TextView) dialog.findViewById(R.id.txtdetail);
        TextView txtno = (TextView) dialog.findViewById(R.id.txtno);

        if (Message.contains("No Data Found") || Message.contains("Data Not Found.")) {
            tv_alert_title.setVisibility(View.GONE);
            txtdetail.setText(mContext.getResources().getString(R.string.PleaseEnterValidData));
        } else {
            txtdetail.setText(Message);
        }

        txtno.setVisibility(View.GONE);
        TextView txtyes = (TextView) dialog.findViewById(R.id.txtyes);
        txtyes.setText(mContext.getResources().getString(R.string.ok));

        txtyes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }

        });

        dialog.show();

    }

    public static void showAlertDialogForTripClose(Activity mContext, String Message, String title) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.error_dialog);

        TextView tv_alert_title = (TextView) dialog.findViewById(R.id.tv_alert_title);
        tv_alert_title.setText(title);
        TextView txtdetail = (TextView) dialog.findViewById(R.id.txtdetail);
        TextView txtno = (TextView) dialog.findViewById(R.id.txtno);

        if (Message.contains("No Data Found") || Message.contains("Data Not Found.")) {
            tv_alert_title.setVisibility(View.GONE);
            txtdetail.setText(mContext.getResources().getString(R.string.PleaseEnterValidData));
        } else {
            txtdetail.setText(Message);
        }

        txtno.setVisibility(View.GONE);
        TextView txtyes = (TextView) dialog.findViewById(R.id.txtyes);
        txtyes.setText(mContext.getResources().getString(R.string.ok));

        txtyes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                mContext.finish();

            }

        });

        dialog.show();

    }

    public static void writeDataToFile(Activity activity, String data) {
        try {
            File dir = new File(Environment.getExternalStorageDirectory(), "ERPOrataroDB");
            String timeStamp;
            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            if (!dir.exists()) {
                if (dir.mkdirs()) {
                    File dbBackup = new File(dir, "ERPOrataroBackup_" + timeStamp + ".json");
                    FileOutputStream fos = new FileOutputStream(dbBackup);
                    fos.write(data.getBytes());
                    fos.close();
//                    Utility.showToast("Backup successful");
//                    shareBackup(activity, dbBackup);
                } else {
                    Utility.showToast("Filed to create backup", activity);
                }
            } else {
                File dbBackup = new File(dir, "ERPOrataroBackup_" + timeStamp + ".json");
                FileOutputStream fos = new FileOutputStream(dbBackup);
                fos.write(data.getBytes());
                fos.close();
                Utility.showToast("Backup successful", activity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    public static String getData(Activity activity, Uri uri) {
        StringBuilder text = new StringBuilder();
        try {
            InputStream inputStream = activity.getContentResolver().openInputStream(uri);
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            String mLine;
            while ((mLine = r.readLine()) != null) {
                text.append(mLine);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }


    public static long dateToMilliSeconds(String someDate, String Format) {

        SimpleDateFormat sdf = new SimpleDateFormat(Format, Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(someDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(date.getTime());
        return date.getTime();

    }

    public static String getCurrentDate(String format) {

        if (format == null || format.equalsIgnoreCase("")) {
            throw new IllegalArgumentException("format could not be null or empty");
        }
        try {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat(format, Locale.getDefault());
            return df.format(c);
        } catch (IllegalArgumentException e) {
            return "";
        }
    }

    public static long ConvertTimeSTamptoMilliseconds(String someDate, String Format) {

        SimpleDateFormat formatter = new SimpleDateFormat(Format);
        String dateString = formatter.format(new Date(Long.parseLong(someDate)));
        Log.d("convertedDate", dateString);
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println(date.getTime());
        return date.getTime();


    }

    public static void deleteDialog(Context mContext, String type, String title, final Popup popup) {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, false);

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog.findViewById(R.id.ll_submit);
        TextView tv_header = (TextView) mPoweroffDialog.findViewById(R.id.tv_header);
        TextView tv_say_something = (TextView) mPoweroffDialog.findViewById(R.id.tv_say_something);
        TextView txt_title = (TextView) mPoweroffDialog.findViewById(R.id.txt_title);
        txt_title.setVisibility(View.VISIBLE);
        tv_header.setText(mContext.getResources().getString(R.string.Delet));
        tv_say_something.setText(mContext.getResources().getString(R.string.deletemsg) + " " + type + "?");
        if (title.equalsIgnoreCase("")) {
            txt_title.setVisibility(View.GONE);
        }


        txt_title.setText(Html.fromHtml(title));
        ll_submit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.deleteYes();
                mPoweroffDialog.dismiss();
            }
        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mPoweroffDialog.dismiss();
            }

        });

    }

    public static String GetCurrentTime() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm aa");
        String strDate = mdformat.format(calendar.getTime());
        return strDate;

    }

    public static Bitmap getBitmap(String path, Context context) {

        return BitmapUtil.compressImage(path, context);

    }

    public static void sendPushNotification(Context mContext) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        new AsynsTaskClass(mContext, arrayList, false, null).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);
    }

    public static void getUserModelData(Context mContext) {

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Global.userdataModel = new LoginModel();
            Global.userdataModel = new UserSharedPrefrence(mContext).getLoginModel();
        }

    }

    public static Uri getOutputMediaFileUri(int type, Context mContext) {

        return Uri.fromFile(getOutputMediaFile(type, mContext));

    }

    public static Uri getMediaFileUri(int type, Context mContext) {

        return FileProvider.getUriForFile(mContext,
                BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(type, mContext));


    }


    private static File getOutputMediaFile(int type, Context mContext) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                ServiceResource.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                Utility.showToast(mContext.getResources().getString(R.string.Error), mContext);
                return null;

            }

        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;

        if (type == ServiceResource.MEDIA_TYPE_IMAGE) {

            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static void showToast(String message, Context context) {
        Utility.toast(context, message);
    }

    public static int pxToDp(Context context, float px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static ReadWriteSettingModel ReadWriteSetting(String result) {
        ReadWriteSettingModel model = new ReadWriteSettingModel();
        if (Global.readWriteSettingList != null && Global.readWriteSettingList.size() > 0) {
            for (int i = 0; i < Global.readWriteSettingList.size(); i++) {
                if (Global.readWriteSettingList.get(i).getRightName().equalsIgnoreCase(result)) {
                    model = Global.readWriteSettingList.get(i);
                }
            }
        }

        return model;
    }

    public static boolean isTeacher(Context mContext) {
        if (new UserSharedPrefrence(mContext).getLoginModel().getMemberType().equalsIgnoreCase(ServiceResource.USER_TEACHER_STRING)) {
            return true;
        } else {
            return false;
        }

    }

    public static String GetTimewithAmPm(Context mContext, int hourOfDay, int minute, String status) {

        status = "AM";

        if (hourOfDay > 11) {
            // If the hour is greater than or equal to 12
            // Then the current AM PM status is PM
            status = "PM";
        }

        // Initialize a new variable to hold 12 hour format hour value
        int hour_of_12_hour_format;

        if (hourOfDay > 11) {
            // If the hour is greater than or equal to 12
            // Then we subtract 12 from the hour to make it 12 hour format time
            hour_of_12_hour_format = hourOfDay;
        } else {
            hour_of_12_hour_format = hourOfDay;
        }

        return hour_of_12_hour_format + " : " + minute + " : " + status;

//        return Utility.dateFormate(hour_of_12_hour_format + " : " + minute , "HH : mm : aa", "hh : mm");

    }

    public static void GetSQLConnStr(Context mContext, ArrayList<PropertyVo> arraylist) {

        arraylist.add(new PropertyVo(ServiceResource.CTOKEN, PreferenceData.getCTOKEN()));

    }

    public static String GetFirstName(Context mContext) {
        String[] names = new UserSharedPrefrence(mContext).getLoginModel().getFullName().split(" ");
        return names[0];
    }

    public static String GetProfilePicture(String profilePicture, String userID) {

        String url = "";
        if (profilePicture.contains(".png")) {
            url = Utility.getProfilePic(userID + ".png");
        } else {
            url = Utility.getProfilePic(userID + ".jpg");
        }

        return url;

    }

    public static Bitmap compressImage2(String fielPath, Context mContext) {

        String filePath = getRealPathFromURI2(fielPath, mContext);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        //int actualHeight = options.outHeight;
        //int actualWidth = options.outWidth;

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byteArray = stream.toByteArray();
            Base64String = Base64.encodeToString(byteArray,
                    Base64.NO_WRAP);
            bmp.recycle();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new BitmapFactory().decodeFile(filename);
//        return filename;

    }

    private static String getRealPathFromURI2(String fielPath, Context mContext) {
        Uri contentUri = Uri.parse(fielPath);
        Cursor cursor = mContext.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static String ImageCompress(String fielPath, Context mContext) {

//        Log.d("getSelectedPath", fielPath);
        String filePath = getRealPath(fielPath, mContext);
//        Log.d("getSelectedPath1", filePath);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);


        //int actualHeight = options.outHeight;
        //int actualWidth = options.outWidth;

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            FileOutputStream out1 = null;
            NewFileName = getFilename();
            try {
                out1 = new FileOutputStream(NewFileName);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out1);
//                Log.d("getSelectedPath2", NewFileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;

        try {

            exif = new ExifInterface(NewFileName);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public static String getRealPath(String contentURI, Context mContext) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = mContext.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static void GetBASE64STRING(String path) {
        Log.d("getfilefrompath", path);
        BITMAPOFIMAGE = new BitmapFactory().decodeFile(path);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BITMAPOFIMAGE.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        BASE64_STRING = Base64.encodeToString(byteArray, Base64.DEFAULT);
        Log.d("getBase64str", BASE64_STRING);

    }

    public static void convertToBase64String(Context mContext, Uri uri) {

        byte[] bytes;
        String Document = "";
        String uriString = uri.toString();
        Log.d("data", "onActivityResult: uri" + uriString);
        //            myFile = new File(uriString);
        //            ret = myFile.getAbsolutePath();
        //Fpath.setText(ret);
        try {
            InputStream in = mContext.getContentResolver().openInputStream(uri);
            bytes = getBytes(in);
            Log.d("data", "onActivityResult: Base64string=" + Base64.encodeToString(bytes, Base64.DEFAULT));
            String ansValue = Base64.encodeToString(bytes, Base64.DEFAULT);
            Document = Base64.encodeToString(bytes, Base64.DEFAULT);
            Utility.BASE64_STRING = Document;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            Log.d("error", "onActivityResult: " + e.toString());
        }

    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static void GetBASE64STRINGFROMFILE(String path) {
        Log.d("filepathhhinside", path);
        String base64 = "";
        try {/*from   w w w .  ja  va  2s  .  c om*/
            File file1 = new File(path);
            Log.d("filepathhhinside", file1.getAbsolutePath());
            byte[] buffer = new byte[(int) file1.length() + 100];
            @SuppressWarnings("resource")
            int length = new FileInputStream(file1).read(buffer);
            BASE64_STRING = Base64.encodeToString(buffer, 0, length,
                    Base64.DEFAULT);
            Log.d("filebase64", base64);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String GetFilenamewithoutSpecialCharacter(String name) {
        String FileNamewithoutExtension = "";
        String extension = name.substring(name.lastIndexOf("."));

        if (name.indexOf(".") > 0) {
            FileNamewithoutExtension = name.substring(0, name.lastIndexOf("."));
        }

        String newname = FileNamewithoutExtension.replaceAll("[-\\\\[\\\\]^/,'*:.!><~@#$%+=?|\\\"\\\\\\\\()]+", "").replace(" ", "") + extension;

        Log.d("sggc", newname);
        return newname;

    }

    public long dateToMilli(String Date) {

        java.util.Date date = null;
        try {
            date = new SimpleDateFormat("yyyy/mm/dd", Locale.ENGLISH).parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long milliseconds = date.getTime();
        return milliseconds;

    }

    public static String compressImage(String imageUri) throws Exception {

        String filePath = imageUri;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        if (actualHeight > 0 && actualWidth > 0) {
            float maxHeight = 1116.0f;
            float maxWidth = 912.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;
            if (options.outHeight < maxHeight)
                return imageUri;
            if (options.outWidth < maxWidth)
                return imageUri;
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }
        }
        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_4444);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, paint);// new
        // Paint(Paint.ANTI_ALIAS_FLAG));//.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            if (filePath.contains(".png") || filePath.contains(".PNG")) {
                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else {
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public static String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Orataro/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public static String getVideoFilename(String filename) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Orataro/Video");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + filename);
        return uriSting;

    }

    public static String getDownloadFilename(String filename) {

        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Orataro/Download");

        if (!file.exists()) {
            file.mkdirs();
        }

        String uriSting = (file.getAbsolutePath() + "/"
                + filename);

        return uriSting;

    }

    public static String getProfilePic(String filename) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "Orataro/profilepic");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/"
                + filename);
        return uriSting;
    }


    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static void toast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    public static void longtoast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }


    public static void setLanguage(Context context, String lanhuageCode) {

        String languageToLoad = lanhuageCode;// "GU";

        Locale[] locales = Locale.getAvailableLocales();
        boolean found = false;
        for (int i = 0; i < locales.length; i++) {

            if (locales[i].toString().toLowerCase().equals(languageToLoad.toLowerCase())) {

                found = true;
                break;

            }

        }

        if (found) {
            Locale locale = new Locale(languageToLoad);
            if (lanhuageCode.equalsIgnoreCase("en_US")) {
                Locale.setDefault(locale);
                Configuration configuration = new Configuration();
                configuration.locale = locale;
                ((Activity) context).getBaseContext().getResources().updateConfiguration(configuration, null);
            } else if (lanhuageCode.equalsIgnoreCase("HI")) {
                String s = "कृपा";
                if (isSupported(context, s)) {

                    Locale.setDefault(locale);
                    Configuration configuration = new Configuration();
                    configuration.locale = locale;
                    ((Activity) context).getBaseContext().getResources().updateConfiguration(configuration, ((Activity) context).getBaseContext().getResources().getDisplayMetrics());
                } else {
                    Locale locale1 = new Locale("en_Us");
                    Locale.setDefault(locale1);
                    Configuration configuration = new Configuration();
                    configuration.locale = locale1;
                    ((Activity) context).getBaseContext().getResources().updateConfiguration(configuration, null);
                }
            } else {
                String s = "કૃપા";
                if (isSupported(context, s)) {
                    Locale.setDefault(locale);
                    Configuration configuration = new Configuration();
                    configuration.locale = locale;
                    ((Activity) context).getBaseContext().getResources().updateConfiguration(configuration, ((Activity) context).getBaseContext().getResources().getDisplayMetrics());
                } else {
                    Locale locale1 = new Locale("en_Us");
                    Locale.setDefault(locale1);
                    Configuration configuration = new Configuration();
                    configuration.locale = locale1;
                    ((Activity) context).getBaseContext().getResources().updateConfiguration(configuration, null);
                }
            }
        }
    }

    public static boolean isSupported(Context context, String text) {

        final int WIDTH_PX = 200;
        final int HEIGHT_PX = 80;

        int w = WIDTH_PX, h = HEIGHT_PX;
        Resources resources = context.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(w, h, conf); // this creates a MUTABLE bitmap
        Bitmap orig = bitmap.copy(conf, false);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.rgb(0, 0, 0));
        paint.setTextSize((int) (14 * scale));

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = (bitmap.getHeight() + bounds.height()) / 2;

        canvas.drawText(text, x, y, paint);
        boolean res = !orig.sameAs(bitmap);
        orig.recycle();
        bitmap.recycle();
        return res;

    }

    public static boolean isValidDate(String inDate, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }


    public static String getShareResult(String title) {

        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Orataro/shareresult");

        if (!file.exists()) {

            file.mkdirs();

        }

        String uriSting = (file.getAbsolutePath() + "/"
                + title + ".jpg");

        return uriSting;

    }

    public static String readFromFile(String FileName, Context pContext) {

        String ret = "";
        String storagePath = Environment.getExternalStorageDirectory() + "/Android/data/" + pContext.getPackageName() + "/";

        try {

            InputStream inputStream;

            if (FileName.equalsIgnoreCase(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN)) {
                inputStream = ((Activity) pContext).openFileInput(FileName);
            } else {
                inputStream = ((Activity) pContext).openFileInput(FileName + new UserSharedPrefrence(pContext).getLOGIN_USERID());
            }

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static void writeToFile(String data, String FileName, Context mContext) {

        try {

            String storagePath = Environment.getExternalStorageDirectory() + "/Android/data/" + mContext.getPackageName() + "/";
            OutputStreamWriter outputStreamWriter;
            if (FileName.equalsIgnoreCase(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN)) {
                outputStreamWriter = new OutputStreamWriter(
                        mContext.getApplicationContext().openFileOutput(FileName,
                                Context.MODE_PRIVATE));
            } else {
                outputStreamWriter = new OutputStreamWriter(
                        mContext.getApplicationContext().openFileOutput(FileName + new UserSharedPrefrence(mContext).getLOGIN_USERID(),
                                Context.MODE_PRIVATE));
            }
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static boolean isValidMObileNumber(String mobnum) {
        if (mobnum != null) {
            if (mobnum.length() > 6 && mobnum.length() < 18) {
                return true;
            }
        }
        return false;
    }

    public static String isValidStr(String strval) {
        if (strval != null && !strval.equalsIgnoreCase("") && !strval.equalsIgnoreCase("NAN")) {

        } else {
            strval = "";
        }

        return strval;
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(7);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }


    // commented By Krishna : 31-01-2019 - Method for getting FirebaseToken to Update GCMID

    public static void RefreshFirebaseToken(Context context) {

        // Get token
        // [START retrieve_current_token]s

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {

                        if (!task.isSuccessful()) {

                            return;

                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = "FirebaseToken" + token;
//                        Log.d(TAG, msg);
                        Log.d("firebasetoken", token);
                        PreferenceData.setToken(token);


                    }


                });

        // [END retrieve_current_token]
    }

    /*END*/


    /*commented By Krishna : Get File From URI if OS Version is Greater Than Marshmallow or not*/

    public static Uri GetUriFromFile(Context mcontext, String fileName) {

        if (Build.VERSION.SDK_INT > M) {

            FileURI = FileProvider.getUriForFile(mcontext,
                    BuildConfig.APPLICATION_ID + ".provider",
                    new File((Utility.getDownloadFilename(fileName))));//package.provider

            //TODO:  Permission..

        } else {

            FileURI = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));

        }


        return FileURI;
    }
    /*END*/


}
