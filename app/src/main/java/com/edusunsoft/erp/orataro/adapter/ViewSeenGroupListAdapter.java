package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.SeenByActivity;
import com.edusunsoft.erp.orataro.activities.ViewImpNotesDetailActivity;
import com.edusunsoft.erp.orataro.model.PostModel;
import com.edusunsoft.erp.orataro.util.Constants;

import java.util.ArrayList;
import java.util.Locale;

public class ViewSeenGroupListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater layoutInfalater;
	private ArrayList<PostModel> postModelList = new ArrayList<>();
	private ArrayList<PostModel> copyList = new ArrayList<>();
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };

	public ViewSeenGroupListAdapter(Context context, ArrayList<PostModel> list) {
		this.mContext = context;
		this.postModelList = list;
		copyList.addAll(list);
	}

	@Override
	public int getCount() {
		return postModelList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.impnoteslistraw, parent, false);
		LinearLayout noteLayout = (LinearLayout) convertView.findViewById(R.id.ll_date);
		LinearLayout ll_seenby = (LinearLayout) convertView.findViewById(R.id.ll_seenby);
		TextView txtSenderName = (TextView) convertView.findViewById(R.id.txt_subject);
		TextView txtPostDetail = (TextView) convertView.findViewById(R.id.txt_sub_detail);
		TextView txtSeenByCount = (TextView) convertView.findViewById(R.id.tv_seen_by);
		TextView txtTime = (TextView) convertView.findViewById(R.id.tv_time);
		TextView txtDate = (TextView) convertView.findViewById(R.id.tv_date);
		TextView txtMonth = (TextView) convertView.findViewById(R.id.txt_month);
		TextView txtYear = (TextView) convertView.findViewById(R.id.txt_year);
		LinearLayout ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
		LinearLayout ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
		int colorPos = position % colors.length;
		convertView.setBackgroundColor(colors[colorPos]);
		ll_view.setBackgroundColor(colors_list[position % colors_list.length]);
		Log.e("Position1", "" + position);
		if (postModelList.get(position).isChangeMonth()) {
			noteLayout.setVisibility(View.VISIBLE);
			txtMonth.setText(postModelList.get(position).getMonth());
		} else {
			noteLayout.setVisibility(View.GONE);
		}

		if (postModelList.get(position).getSenderName() != null) {
			txtSenderName.setText(postModelList.get(position).getSenderName());
		}

		if (postModelList.get(position).getPost() != null) {
			txtPostDetail.setText(postModelList.get(position).getPost());
		}

		if (postModelList.get(position).getPost() != null) {
			if (postModelList.get(position).getPost().length() > Constants.CONTINUEREADINGSIZE) {
				String continueReadingStr = Constants.CONTINUEREADINGSTR;
				String tempText = postModelList.get(position).getPost()
						.substring(0, Constants.CONTINUEREADINGSIZE)
						+ continueReadingStr;

				SpannableString text = new SpannableString(tempText);

				text.setSpan(
						new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR),
						Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE
								+ continueReadingStr.length(), 0);

				MyClickableSpan clickfor = new MyClickableSpan(
						tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

					@Override
					public void onClick(View widget) {

						Intent i = new Intent(mContext, ViewImpNotesDetailActivity.class);
						i.putExtra("name", postModelList.get(position).getSenderName());
						i.putExtra("post_img", postModelList.get(position).getPost_img());
						i.putExtra("post", postModelList.get(position).getPost());
						mContext.startActivity(i);
					}
				};
				text.setSpan(
						clickfor,
						Constants.CONTINUEREADINGSIZE,
						Constants.CONTINUEREADINGSIZE
								+ continueReadingStr.length(), 0);

				txtPostDetail.setMovementMethod(LinkMovementMethod.getInstance());
				txtPostDetail.setText(text, BufferType.SPANNABLE);
			} else {
				txtPostDetail.setText(postModelList.get(position).getPost());
			}
		}
		if (postModelList.get(position).getDate() != null) {
			txtDate.setText(postModelList.get(position).getDate());
		}

		if (postModelList.get(position).getTime() != null) {
			txtTime.setText(postModelList.get(position).getTime());
		}
		if (postModelList.get(position).getViewby() != null) {
			txtSeenByCount.setText(postModelList.get(position).getViewby());
		}

		txtPostDetail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(mContext, ViewImpNotesDetailActivity.class);
				i.putExtra("name", postModelList.get(position).getSenderName());
				i.putExtra("post_img", postModelList.get(position).getPost_img());
				i.putExtra("post", postModelList.get(position).getPost());
				mContext.startActivity(i);
			}
		});

		txtPostDetail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(mContext, ViewImpNotesDetailActivity.class);
				i.putExtra("name", postModelList.get(position).getSenderName());
				i.putExtra("post_img", postModelList.get(position).getPost_img());
				i.putExtra("post", postModelList.get(position).getPost());
				mContext.startActivity(i);
			}
		});
		ll_seenby.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(mContext, SeenByActivity.class);
				i.putExtra("count", postModelList.get(position).getViewby());
				mContext.startActivity(i);
			}
		});

		return convertView;

	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		postModelList.clear();
		if (charText.length() == 0) {
			postModelList.addAll(copyList);
		} else {
			for (PostModel vo : copyList) {
				if (vo.getPost().toLowerCase(Locale.getDefault()).contains(charText)) {
					postModelList.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}

}
