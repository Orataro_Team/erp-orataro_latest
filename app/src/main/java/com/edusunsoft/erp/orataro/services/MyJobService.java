package com.edusunsoft.erp.orataro.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.activities.SplashActivity;
import com.firebase.jobdispatcher.JobService;

public class MyJobService extends JobService {

    String Title = "", Message = "", Type = "";
    private static final String TAG = "MyJobService";

    @Override
    public boolean onStartJob(com.firebase.jobdispatcher.JobParameters job) {

        Log.d(TAG, "Performing long running task in scheduled job");
        // TODO(developer): add long running task here.

        Toast.makeText(getApplicationContext(), job.getExtras().getString("associationid"), Toast.LENGTH_LONG).show();
        Log.d("shceduledtask", job.getExtras().getString("associationid"));

        try {

            FireNotification(job.getExtras());

        } catch (Exception e) {

            e.printStackTrace();

        }


//        try {
//
//            JSONObject json = null;
//
//            try {
//
//                String jsonString = PreferenceData.getFirebaseMessageData();
//
//                json = new JSONObject(jsonString);
//                String title = json.getString("title");
////                String message = json.getString("body");
//
//                JSONObject jobj = new JSONObject(json.getString("body"));
//                String message = jobj.getString("body");
//
//                String notificationtype = json.getString("notificationtype");
//                String associationid = json.getString("associationid");
//                String associationtype = json.getString("associationtype");
//                String sendtomemberid = json.getString("sendtomemberid");
//
//                SendNotification(title, "My Test");
//
//
//            } catch (JSONException e) {
//
//                e.printStackTrace();
//
//            }
//
//
//        } catch (Exception e) {
//
//            e.printStackTrace();

//        }

        return false;

    }

    private void FireNotification(Bundle bundle) {

        String Title = PreferenceData.getFirebaseMessageTitle();
        String Message = PreferenceData.getFirebaseMessageMessage();

        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), getString(R.string.default_notification_channel_id))
                .setContentTitle(Title)
                .setContentText(Message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                .setColor(getResources().getColor(R.color.blue))
                .setLights(Color.RED, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.ic_launcher);

        android.app.NotificationManager notificationManager = (android.app.NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(

                    getString(R.string.default_notification_channel_id), "FCM", android.app.NotificationManager.IMPORTANCE_DEFAULT

            );

            channel.setDescription("Demo Description");
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);


        }

        assert notificationManager != null;
        notificationManager.notify(0, notificationBuilder.build());


    }

    private void SendNotification(String title, String message) {

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                getApplicationContext());

        Intent intent = new Intent(this, SplashActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification notification = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSound(defaultSoundUri)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setColor(ContextCompat.getColor(getApplicationContext(), R.color.blue))
                    .setChannelId(channelId)
                    .setContentIntent(pendingIntent)
                    .build();

            // Register the channel with the system
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            mNotificationManager.notify(0, notification);


        } else {


            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.addLine(message);
            Notification notification = null;

            notification = mBuilder.setSmallIcon(R.drawable.ic_launcher).setTicker(title).setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setStyle(inboxStyle)
                    .setSound(defaultSoundUri)
                    .setColor(getResources().getColor(R.color.blue))
                    .setContentText(message)
                    .build();

            NotificationManager notificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager2.notify(0, notification);


        }


//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(this, channelId)
//                        .setSmallIcon(R.drawable.ic_launcher)
//                        .setContentTitle(title)
//                        .setContentText(message)
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri)
//                        .setContentIntent(pendingIntent);
//
//        // Since android Oreo notification channel is needed.
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }

    @Override
    public boolean onStopJob(com.firebase.jobdispatcher.JobParameters job) {

        return false;

    }

}
