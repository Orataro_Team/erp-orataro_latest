package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollParse implements Parcelable {

    @SerializedName("PollID")
    @Expose
    private String pollID;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Details")
    @Expose
    private String details;
    @SerializedName("StartIn")
    @Expose
    private String startIn;
    @SerializedName("EndIn")
    @Expose
    private String endIn;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Participant")
    @Expose
    private String participant;
    public final static Parcelable.Creator<PollParse> CREATOR = new Creator<PollParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PollParse createFromParcel(Parcel in) {
            PollParse instance = new PollParse();
            instance.pollID = ((String) in.readValue((String.class.getClassLoader())));
            instance.startDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.endDate = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.details = ((String) in.readValue((String.class.getClassLoader())));
            instance.startIn = ((String) in.readValue((String.class.getClassLoader())));
            instance.endIn = ((String) in.readValue((String.class.getClassLoader())));
            instance.userName = ((String) in.readValue((String.class.getClassLoader())));
            instance.participant = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public PollParse[] newArray(int size) {
            return (new PollParse[size]);
        }

    };

    public String getPollID() {
        return pollID;
    }

    public void setPollID(String pollID) {
        this.pollID = pollID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStartIn() {
        return startIn;
    }

    public void setStartIn(String startIn) {
        this.startIn = startIn;
    }

    public String getEndIn() {
        return endIn;
    }

    public void setEndIn(String endIn) {
        this.endIn = endIn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getParticipant() {
        return participant;
    }

    public void setParticipant(String participant) {
        this.participant = participant;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pollID);
        dest.writeValue(startDate);
        dest.writeValue(endDate);
        dest.writeValue(title);
        dest.writeValue(details);
        dest.writeValue(startIn);
        dest.writeValue(endIn);
        dest.writeValue(userName);
        dest.writeValue(participant);
    }

    public int describeContents() {
        return 0;
    }

}