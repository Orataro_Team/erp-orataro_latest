package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.ShareInterface;
import com.edusunsoft.erp.orataro.Interface.WallCustomeClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPostOnWallActivity;
import com.edusunsoft.erp.orataro.activities.CommentActivity;
import com.edusunsoft.erp.orataro.activities.ImageListActivity;
import com.edusunsoft.erp.orataro.activities.SinglePostActivity;
import com.edusunsoft.erp.orataro.activities.VideoViewActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.CTextView;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyWallAdapter extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private static final int ID_PUBLIC = 1;
    private static final int ID_FRIEND = 2;
    private static final int ID_SP_FRIEND = 3;
    private static final int ID_ONLY_ME = 4;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private LayoutInflater layoutInfalater;
    private CircleImageView profilePicUser;
    private TextView userName, timeBeforepost;
    private LinearLayout ll_comment, ll_share;
    private LinearLayout layoutShowHide;
    private ArrayList<WallPostModel> wallList = new ArrayList<>();
    private ArrayList<WallPostModel> copyWallList = new ArrayList<>();
    private boolean hideCommentLayout = true;
    private LinearLayout showHideLayoutstatus, showHidelayoutphotovideo, ll_Photo, ll_Video, ll_Friends;
    private LinearLayout ll_like, ll_Unlike;
    private ImageView iv_like, iv_Unlike;
    private WallCustomeClick interfaceClick;
    private TextView tv_post, tv_like, tv_Unlike, tv_comment, tv_share;
    private boolean isCall;
    private TextView txt_likes, txt_unlikes;
    private TextView txt_comments;
    private boolean isLikeSucess;
    private QuickAction quickActionForShare;
    private ActionItem actionPublic, actionFriend, actionSpFriend, actionOnlyMe;
    private QuickAction quickActionForEditOrDelete;
    private ActionItem actionEdit, actionDelete;
    private ShareInterface shareInterface;
    private TextView txt_count;
    private boolean setting;
    private String from;
    private ImageView imgEditPost = null;
    private int posRemove = 0;
    private Dialog mPoweroffDialog;
    private TextView txtpostname;
    private int position;
    private int mPreviousPosition;
    private TextView editTextTitlePost, editTextDescriptionPost;
    private TextCrawler textCrawler;
    private Bitmap[] currentImageSet;
    private Bitmap currentImage;
    private int currentItem = 0;
    private int countBigImages = 0;
    private boolean noThumb;
    private String currentTitle, currentUrl, currentCannonicalUrl, currentDescription;
    public boolean isScrolling;
    private RefreshListner listner;


    public MyWallAdapter(Context context, ArrayList<WallPostModel> wallList, WallCustomeClick interfaceClick, ShareInterface pShareInterface, boolean setting, String from) {

        this.mContext = context;
        this.wallList = Global.wallPostModels;
        copyWallList.addAll(wallList);
        this.interfaceClick = interfaceClick;
        shareInterface = pShareInterface;
        this.setting = setting;
        this.from = from;
        isCall = true;

    }

    @Override
    public int getCount() {
        return wallList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int _position, View convertView, ViewGroup parent) {

        position = _position;

        try {

            layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (_position == 0) {

                convertView = layoutInfalater.inflate(R.layout.walllayout, parent, false);
                showHideLayoutstatus = (LinearLayout) convertView.findViewById(R.id.showhideLayout);
                showHidelayoutphotovideo = (LinearLayout) convertView.findViewById(R.id.showhideLayoutphotovideo);
                tv_post = (TextView) convertView.findViewById(R.id.tv_post);
                ll_Photo = (LinearLayout) convertView.findViewById(R.id.ll_Photo);
                ll_Video = (LinearLayout) convertView.findViewById(R.id.ll_Video);
                ll_Friends = (LinearLayout) convertView.findViewById(R.id.ll_Friends);
                txt_count = (TextView) convertView.findViewById(R.id.txt_count);
                if (!new UserSharedPrefrence(mContext).getLOGIN_FRIENDCOUNT().equalsIgnoreCase("0")) {
                    txt_count.setText(new UserSharedPrefrence(mContext).getLOGIN_FRIENDCOUNT());
                    txt_count.setVisibility(View.VISIBLE);
                }
                final CircleImageView iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);
                if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                    ll_Photo.setVisibility(View.INVISIBLE);
                }
                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {
                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("false") &&
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false") &&
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("false") &&
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("false")) {
                        tv_post.setVisibility(View.INVISIBLE);
                    } else {
                        tv_post.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                        if ((Global.DynamicWallSetting != null)) {
                            if (Global.DynamicWallSetting.getIsAllowPeopleToUploadAlbum() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostStatus()) {
                                tv_post.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {

                        tv_post.setVisibility(View.INVISIBLE);

                    }

                }

                showHideLayoutstatus.setVisibility(View.GONE);
                showHidelayoutphotovideo.setVisibility(View.GONE);

                String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
                if (ProfilePicture != null) {

                    try {

                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(ServiceResource.BASE_IMG_URL1
                                        + ProfilePicture
                                        .replace("//", "/")
                                        .replace("//", "/"))
                                .apply(options)
                                .into(iv_profile_pic);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                tv_post.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        interfaceClick.clickOnPost();

                    }

                });

                ll_Photo.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        interfaceClick.clickOnPhoto();

                    }

                });

                ll_Video.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        interfaceClick.clickOnVideo();

                    }

                });

            } else {

                final int pos = _position;
                convertView = layoutInfalater.inflate(R.layout.whats_on_mind_listraw, parent, false);
                mPreviousPosition = position;
                profilePicUser = (CircleImageView) convertView.findViewById(R.id.profilePicUser);
                final ImageView iv_fb = (ImageView) convertView.findViewById(R.id.iv_fb);
                iv_fb.setTag("" + _position);
                txt_likes = (TextView) convertView.findViewById(R.id.txt_likes);
                txt_unlikes = (TextView) convertView.findViewById(R.id.txt_unlikes);
                txt_comments = (TextView) convertView.findViewById(R.id.txt_comments);
                TextView txtastype = (TextView) convertView.findViewById(R.id.txtastype);
                ll_comment = (LinearLayout) convertView.findViewById(R.id.ll_comment);
                ll_comment.setTag("" + _position);
                ll_share = (LinearLayout) convertView.findViewById(R.id.ll_share);
                ll_share.setTag("" + _position);
                imgEditPost = (ImageView) convertView.findViewById(R.id.editarrow);
                LinearLayout dropPreview = (LinearLayout) convertView.findViewById(R.id.drop_preview);

                LinearLayout ll_twoormore = (LinearLayout) convertView.findViewById(R.id.ll_twoormore);
                ll_twoormore.setTag("" + _position);
                FrameLayout fl_threeormore = (FrameLayout) convertView.findViewById(R.id.fl_threeormore);
                LinearLayout ll_fourormore = (LinearLayout) convertView.findViewById(R.id.ll_fourormore);
                ImageView img_one = (ImageView) convertView.findViewById(R.id.img_one);
                ImageView img_two = (ImageView) convertView.findViewById(R.id.img_two);
                ImageView img_three = (ImageView) convertView.findViewById(R.id.img_three);
                TextView txtImageCount = (TextView) convertView.findViewById(R.id.txtImageCount);
                final LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
                ll_editdelete.setTag("" + _position);
                txtpostname = (TextView) convertView.findViewById(R.id.txtpostname);
                actionPublic = new ActionItem(ID_PUBLIC, mContext.getResources().getString(R.string.public1), mContext
                        .getResources().getDrawable(R.drawable.fb_publics));

                /*commented By Krishna  : 15-05-2019 Remove Friend Option while Share*/

//                actionFriend = new ActionItem(ID_FRIEND, mContext.getResources().getString(R.string.Friends), mContext
//                        .getResources().getDrawable(R.drawable.fb_group));
//                actionSpFriend = new ActionItem(ID_SP_FRIEND, mContext.getResources().getString(R.string.specialfriend),
//                        mContext.getResources().getDrawable(R.drawable.fb_sp_frnd));

                /*END*/
                actionOnlyMe = new ActionItem(ID_ONLY_ME, mContext.getResources().getString(R.string.onlyme), mContext
                        .getResources().getDrawable(R.drawable.fb_only_me));
                quickActionForShare = new QuickAction(mContext,
                        QuickAction.VERTICAL);
                quickActionForShare.addActionItem(actionPublic);
                quickActionForShare.addActionItem(actionOnlyMe);

                /*commented By Krishna  : 15-05-2019 Remove Friend Option while Share*/

//                quickActionForShare.addActionItem(actionFriend);
//                quickActionForShare.addActionItem(actionSpFriend);

                /*END*/

                // Set listener for action item clicked

                quickActionForShare
                        .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                            @Override
                            public void onItemClick(QuickAction source, int _pos, int actionId) {
                                ActionItem actionItem = source.getActionItem(_pos);
                                int sharePos = Integer.valueOf(((View) source.getAnchor()).getTag().toString()) - 1;
                                if (actionId == ID_PUBLIC) {
                                    shareInterface.shrePublic(wallList.get(sharePos), sharePos);
                                }
//                                else if (actionId == ID_FRIEND) {
//                                    shareInterface.shreFriend(wallList.get(sharePos), sharePos);
//                                }
//                                else if (actionId == ID_SP_FRIEND) {
//                                    shareInterface.shreSpecialFriend(wallList.get(sharePos), sharePos);
//                                }
                                else if (actionId == ID_ONLY_ME) {
                                    shareInterface.shreOnlyMe(wallList.get(sharePos), sharePos);
                                }
                            }

                        });


                ll_share.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        quickActionForShare.show(v);

                    }

                });

                actionEdit = new ActionItem(ID_EDIT, mContext.getResources().getString(R.string.Edit), mContext
                        .getResources().getDrawable(R.drawable.edit_profile));
                actionDelete = new ActionItem(ID_DELETE, mContext.getResources().getString(R.string.Delete), mContext
                        .getResources().getDrawable(R.drawable.delete));
                quickActionForEditOrDelete = new QuickAction(mContext, QuickAction.VERTICAL);
                quickActionForEditOrDelete.addActionItem(actionEdit);
                quickActionForEditOrDelete.addActionItem(actionDelete);

                quickActionForEditOrDelete
                        .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                            @Override
                            public void onItemClick(QuickAction source, int _pos, int actionId) {

                                ActionItem actionItem = source.getActionItem(_pos);

                                if (actionId == ID_EDIT) {

                                    int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString()) - 1;
                                    Intent i = new Intent(mContext, AddPostOnWallActivity.class);
                                    i.putExtra("isFrom", from);
                                    i.putExtra("isEdit", true);
                                    i.putExtra("model", wallList.get(editPos));
                                    mContext.startActivity(i);

                                } else if (actionId == ID_DELETE) {

                                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString()) - 1;
                                    posRemove = deletepos;
                                    onClickDelete(wallList.get(deletepos).getPostCommentID());

                                }


                            }


                        });

                layoutShowHide = (LinearLayout) convertView.findViewById(R.id.commentLayouthideshow);
                userName = (TextView) convertView.findViewById(R.id.nameUser);
                timeBeforepost = (TextView) convertView.findViewById(R.id.timebeforepost);
                CTextView postContent = (CTextView) convertView.findViewById(R.id.txt_sub_detail);
                postContent.setTag("" + _position);

                ll_like = (LinearLayout) convertView.findViewById(R.id.ll_like);
                ll_like.setTag("" + _position);
                final ProgressBar progressbar = (ProgressBar) convertView.findViewById(R.id.progressBar1);
                ll_Unlike = (LinearLayout) convertView.findViewById(R.id.ll_Unlike);
                ll_Unlike.setTag("" + _position);
                iv_like = (ImageView) convertView.findViewById(R.id.iv_like);
                iv_Unlike = (ImageView) convertView.findViewById(R.id.iv_Unlike);
                tv_like = (TextView) convertView.findViewById(R.id.tv_like);
                tv_like.setTag("" + _position);
                tv_Unlike = (TextView) convertView.findViewById(R.id.tv_Unlike);
                tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
                tv_share = (TextView) convertView.findViewById(R.id.tv_share);

                ll_editdelete.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        quickActionForEditOrDelete.show(v);

                    }

                });

                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {

                    imgEditPost.setVisibility(View.VISIBLE);
                    imgEditPost.setImageResource(R.drawable.back);
                    imgEditPost.setRotation(270);
                    ll_editdelete.setVisibility(View.VISIBLE);

                } else {

                    imgEditPost.setVisibility(View.GONE);
                    ll_editdelete.setVisibility(View.GONE);

                }

                txtastype.setVisibility(View.VISIBLE);
                txtastype.setText(wallList.get(pos - 1).getAssociationType());

                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToLikeDislikes().equalsIgnoreCase("true") &&
                            wallList.get(position - 1).getIsAllowPeopleToLikeAndDislikeCommentWall()
                            && wallList.get(position - 1).getIsAllowPeopleToLikeOrDislikeOnYourPost()
                            && wallList.get(position - 1).getIsAllowLikeDislike()) {

                        ll_like.setVisibility(View.VISIBLE);
                        ll_Unlike.setVisibility(View.VISIBLE);

                    } else {

                        ll_like.setVisibility(View.GONE);
                        ll_Unlike.setVisibility(View.GONE);

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostComment().equalsIgnoreCase("true") &&
                            wallList.get(position - 1).getIsAllowPeopleToPostCommentOnPostWall()
                            && wallList.get(position - 1).getIsAllowPeopleToPostMessageOnYourWall()
                            && wallList.get(position - 1).getPostComment()
                    ) {

                        ll_comment.setVisibility(View.VISIBLE);

                    } else {

                        ll_comment.setVisibility(View.GONE);

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToSharePost().equalsIgnoreCase("true") &&
                            wallList.get(position - 1).getIsAllowPeopleToShareCommentWall()
                            && wallList.get(position - 1).getIsAllowPeopleToShareYourPost()
                            && wallList.get(position - 1).getIsAllowSharePost()) {

                        ll_share.setVisibility(View.VISIBLE);

                    } else {

                        ll_share.setVisibility(View.GONE);

                    }

                } else {

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToLikeDislikes().equalsIgnoreCase("true") &&
                            wallList.get(position - 1).getIsAllowPeopleToLikeAndDislikeCommentWall()
                            && wallList.get(position - 1).getIsAllowPeopleToLikeOrDislikeOnYourPost()
                            && Global.DynamicWallSetting.getIsAllowPeopleToLikeAndDislikeComment()
                            && Global.DynamicWallSetting.getIsAllowLikeDislike()) {

                        ll_like.setVisibility(View.VISIBLE);
                        ll_Unlike.setVisibility(View.VISIBLE);

                    } else {

                        ll_like.setVisibility(View.GONE);
                        ll_Unlike.setVisibility(View.GONE);

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostComment().equalsIgnoreCase("true")
                            && wallList.get(position - 1).getIsAllowPeopleToPostCommentOnPostWall()
                            && wallList.get(position - 1).getIsAllowPeopleToPostMessageOnYourWall()
                            && Global.DynamicWallSetting.getIsAllowPeopleToPostCommentOnPost()
                            && Global.DynamicWallSetting.getIsAllowPostComment()) {

                        ll_comment.setVisibility(View.VISIBLE);

                    } else {

                        ll_comment.setVisibility(View.GONE);

                    }


                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToSharePost().equalsIgnoreCase("true") &&
                            wallList.get(position - 1).getIsAllowPeopleToShareCommentWall()
                            && wallList.get(position - 1).getIsAllowPeopleToShareYourPost()
                            && Global.DynamicWallSetting.getIsAllowPeopleToShareComment()
                            && Global.DynamicWallSetting.getIsAllowSharePost()) {

                        ll_share.setVisibility(View.VISIBLE);

                    } else {

                        ll_share.setVisibility(View.GONE);

                    }


                }


                ll_like.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int likeppos = Integer.valueOf(v.getTag().toString()) - 1;
                        isCall = false;
                        if (wallList.get(likeppos).getIsLike().equalsIgnoreCase("1")) {
                            iv_like.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                            tv_like.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));
                            new Likeasyns().execute(likeppos, 0);
                            wallList.get(likeppos).setIsLike("0");
                            if (Integer.valueOf(wallList.get(likeppos).getTotalLikes()) > 0) {
                                wallList.get(likeppos).setTotalLikes("" + (Integer.valueOf(wallList.get(likeppos).getTotalLikes()) - 1));
                            }
                        } else {
                            iv_like.setColorFilter(mContext.getResources().getColor(R.color.blue));
                            tv_like.setTextColor(mContext.getResources().getColor(R.color.blue));
                            new Likeasyns().execute(likeppos, 1);
                            wallList.get(likeppos).setIsLike("1");

                            if (wallList.get(likeppos).getIsDisLike() != null && wallList.get(likeppos).getIsDisLike().equalsIgnoreCase("1")) {
                                wallList.get(likeppos).setIsDisLike("0");
                                wallList.get(likeppos).setTotalDislike("" + (Integer.valueOf(wallList.get(likeppos).getTotalDislike()) - 1));
                                iv_Unlike.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                                tv_Unlike.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));
                            }

                            if (wallList.get(likeppos).getIsDisLike() != null && wallList.get(likeppos).getIsDisLike().equalsIgnoreCase("1")) {
                                wallList.get(likeppos).setIsDisLike("0");
                            }

                            if (Integer.valueOf(wallList.get(likeppos).getTotalLikes()) >= 0) {

                                wallList.get(likeppos).setTotalLikes("" + (Integer.valueOf(wallList.get(likeppos).getTotalLikes()) + 1));

                            }

                        }

                        notifyDataSetChanged();

                    }

                });

                ll_Unlike.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        int unlikeppos = Integer.valueOf(v.getTag().toString()) - 1;

                        isCall = false;

                        if (!wallList.get(unlikeppos).getIsDisLike().equalsIgnoreCase("1")) {
                            if (wallList.get(unlikeppos).getIsLike().equalsIgnoreCase("1")) {
                                wallList.get(unlikeppos).setIsLike("0");
                                wallList.get(unlikeppos).setTotalLikes("" + (Integer.valueOf(wallList.get(unlikeppos).getTotalLikes()) - 1));
                                iv_like.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                                tv_like.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));
                            }

                            iv_Unlike.setColorFilter(mContext.getResources().getColor(R.color.blue));
                            tv_Unlike.setTextColor(mContext.getResources().getColor(R.color.blue));
                            new DisLikeasyns().execute(unlikeppos, 1);
                            wallList.get(unlikeppos).setIsDisLike("1");

                            if (Integer.valueOf(wallList.get(unlikeppos).getTotalDislike()) >= 0) {
                                wallList.get(unlikeppos).setTotalDislike("" + (Integer.valueOf(wallList.get(unlikeppos).getTotalDislike()) + 1));
                            }

                        } else {

                            iv_Unlike.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                            tv_Unlike.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));
                            new DisLikeasyns().execute(unlikeppos, 0);
                            wallList.get(unlikeppos).setIsDisLike("0");

                            if (Integer.valueOf(wallList.get(unlikeppos).getTotalDislike()) > 0) {

                                wallList.get(unlikeppos).setTotalDislike("" + (Integer.valueOf(wallList.get(unlikeppos).getTotalDislike()) - 1));

                            }

                        }

                        notifyDataSetChanged();

                    }

                });

                if (wallList.get(position - 1).getPhoto() != null
                        && !wallList.get(position - 1).getPhoto().equals("") &&
                        wallList.get(position - 1).getPhoto().contains(".png") ||
                        wallList.get(position - 1).getPhoto().contains(".PNG") ||
                        wallList.get(position - 1).getPhoto().contains(".jpg") ||
                        wallList.get(position - 1).getPhoto().contains(".JPG") ||
                        wallList.get(position - 1).getPhoto().contains(".jpeg") ||
                        wallList.get(position - 1).getPhoto().contains(".JPEG")) {

                    iv_fb.setVisibility(View.VISIBLE);

                }

                if (wallList.get(position - 1).getPostCommentNote() != null) {

                    postContent.setVisibility(View.VISIBLE);

                } else {

                    postContent.setVisibility(View.GONE);

                }

                if (wallList.get(position - 1).getFullName() != null) {

                    userName.setText(wallList.get(position - 1).getFullName());

                }

                if (wallList.get(position - 1).getDateOfPost() != null) {

                    timeBeforepost.setText(wallList.get(position - 1).getPostCommentTypesTerm() + " " + wallList.get(position - 1).getDateOfPost());

                }

                if (wallList.get(position - 1).getTotalLikes() != null
                        && Integer.parseInt(wallList.get(position - 1).getTotalLikes()) > 0
                        && !wallList.get(position - 1).getTotalLikes().equals("")) {

                    if (Integer.parseInt(wallList.get(position - 1).getTotalLikes()) > 1) {

                        txt_likes.setText(wallList.get(position - 1).getTotalLikes() + " " + mContext.getResources().getString(R.string.Likes));

                    } else {

                        txt_likes.setText(wallList.get(position - 1).getTotalLikes() + " " + mContext.getResources().getString(R.string.Like));

                    }

                    txt_likes.setVisibility(View.VISIBLE);

                } else {

                    txt_likes.setVisibility(View.GONE);

                }

                if (wallList.get(position - 1).getIsDisLike().equalsIgnoreCase("1")) {

                    iv_Unlike.setColorFilter(mContext.getResources().getColor(R.color.blue));
                    tv_Unlike.setTextColor(mContext.getResources().getColor(R.color.blue));

                } else {

                    iv_Unlike.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                    tv_Unlike.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));

                }

                if (wallList.get(position - 1).getIsLike().equalsIgnoreCase("1")) {

                    iv_like.setColorFilter(mContext.getResources().getColor(R.color.blue));
                    tv_like.setTextColor(mContext.getResources().getColor(R.color.blue));

                } else {

                    iv_like.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
                    tv_like.setTextColor(mContext.getResources().getColor(R.color.grp_txt_grey));

                }

                if (wallList.get(position - 1).getTotalDislike() != null && Integer.parseInt(wallList.get(position - 1)
                        .getTotalDislike()) > 0
                        && !wallList.get(position - 1).getTotalDislike().equals("")) {

                    if (Integer.parseInt(wallList.get(position - 1).getTotalDislike()) > 1) {

                        txt_unlikes.setText(wallList.get(position - 1).getTotalDislike() + " " + mContext.getResources().getString(R.string.Unlikes));

                    } else {

                        txt_unlikes.setText(wallList.get(position - 1)
                                .getTotalDislike() + " " + mContext.getResources().getString(R.string.Unlike));

                    }

                    txt_unlikes.setVisibility(View.VISIBLE);

                } else {

                    txt_unlikes.setVisibility(View.GONE);

                }


                if (wallList.get(position - 1).getTotalComments() != null
                        && Integer.parseInt(wallList.get(position - 1)
                        .getTotalComments()) > 0
                        && !wallList.get(position - 1).getTotalComments().equals("")) {

                    txt_comments.setText(wallList.get(position - 1)
                            .getTotalComments() + " " + mContext.getResources().getString(R.string.Comments));

                } else {

                    txt_comments.setVisibility(View.GONE);

                }

                if (wallList.get(position - 1).getPostCommentNote() != null) {

                    if (wallList.get(position - 1).getPostCommentNote().length() > Constants.CONTINUEREADINGSIZE) {
                        String continueReadingStr = Constants.CONTINUEREADINGSTR;
                        String tempText = wallList.get(position - 1)
                                .getPostCommentNote().substring(0, Constants.CONTINUEREADINGSIZE) + continueReadingStr;
                        SpannableString text = new SpannableString(tempText);
                        text.setSpan(new ForegroundColorSpan(
                                        Constants.CONTINUEREADINGTEXTCOLOR),
                                Constants.CONTINUEREADINGSIZE,
                                Constants.CONTINUEREADINGSIZE + continueReadingStr.length(), 0);

                        MyClickableSpan clickfor = new MyClickableSpan(
                                tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                            @Override
                            public void onClick(View widget) {

                                int postSingle = Integer.valueOf(widget.getTag().toString()) - 1;
                                if (!Utility.isTeacher(mContext)) {
                                    if (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("HomeWork") || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("ClassWork")
                                            || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("Circular")
                                            || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("Note")
                                    )))) {
                                        HomeworkListIsRead(wallList.get(postSingle).getAssociationID(), wallList.get(postSingle).getAssociationType());
                                    }
                                }
                                Intent i = new Intent(mContext, SinglePostActivity.class);
                                i.putExtra("WallPost", wallList.get(postSingle));
                                i.putExtra("pos", postSingle);
                                mContext.startActivity(i);
                            }
                        };

                        text.setSpan(
                                clickfor,
                                Constants.CONTINUEREADINGSIZE,
                                Constants.CONTINUEREADINGSIZE
                                        + continueReadingStr.length(), 0);
                        postContent.setMovementMethod(LinkMovementMethod.getInstance());
                        postContent.setText(text, BufferType.SPANNABLE);

                    } else {

                        postContent.setText(wallList.get(position - 1).getPostCommentNote());

                    }

                }

                txtpostname.setText(wallList.get(position - 1).getPostedOn());

                if (wallList.get(position - 1).getPhoto() != null && !wallList.get(position - 1).getPhoto().equalsIgnoreCase("")) {

                    if (wallList.get(position - 1).getFileType().equals("VIDEO")) {

                        iv_fb.setVisibility(View.VISIBLE);
                        iv_fb.setImageResource(R.drawable.dummy_video);

                        if (wallList.get(position - 1).getPhoto().contains(".mp3") || wallList.get(position - 1).getPhoto().contains(".MP3")) {

                            iv_fb.setVisibility(View.VISIBLE);
                            LayoutParams params = new LayoutParams(200, 200);
                            iv_fb.setLayoutParams(params);

                            try {

                                iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

                            } catch (Exception e) {

                                e.printStackTrace();

                            }

                            iv_fb.setImageResource(R.drawable.mpicon);

                        }

                        progressbar.setVisibility(View.GONE);

                    } else if (wallList.get(position - 1).getFileType().equalsIgnoreCase("FILE")) {

                        iv_fb.setVisibility(View.VISIBLE);
                        LayoutParams params = new LayoutParams(200, 200);
                        iv_fb.setLayoutParams(params);
                        try {

                            iv_fb.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);

                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        if (wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("text/plain") ||
                                wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("TEXT")) {

                            iv_fb.setImageResource(R.drawable.txt);

                        } else if (wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("application/pdf") ||
                                wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("PDF")) {

                            iv_fb.setImageResource(R.drawable.pdf);

                        } else if (wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("application/msword") ||
                                wallList.get(position - 1).getFileMimeType().equalsIgnoreCase("WORD")) {

                            iv_fb.setImageResource(R.drawable.doc);

                        } else {

                            iv_fb.setImageResource(R.drawable.file);

                        }


                    } else if (wallList.get(position - 1).getFileType().equalsIgnoreCase("IMAGE")) {

                        if (Integer.valueOf(wallList.get(position - 1).getPostCount()) <= 1) {

                            if (wallList.get(position - 1).getPhoto() != null) {

                                if (wallList.get(position - 1).getPhoto() != null
                                        && !wallList.get(position - 1).getPhoto()
                                        .equalsIgnoreCase("")) {

                                    if (wallList.get(position - 1).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                                        wallList.get(position - 1).setPhoto(wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, ""));

                                    }

                                    if (wallList.get(position - 1).getPhoto().contains("DataFiles/")) {

                                        wallList.get(position - 1).setPhoto(wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", ""));

                                    }

                                    /*21 - 01 - 19 commented by krishna : Use Picasso for Display Image From Url instead of Glide */

                                    try {

                                        Picasso.get().load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                wallList.get(position - 1).getPhoto()
                                                        .replace("//DataFiles//", "/DataFiles/")
                                                        .replace("//DataFiles/", "/DataFiles/")).
                                                into(iv_fb);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }

                                    /*END*/


                                }

                            } else {

                                iv_fb.setVisibility(View.GONE);

                            }

                        } else if (Integer.valueOf(wallList.get(position - 1).getPostCount()) == 2) {

                            ll_twoormore.setVisibility(View.VISIBLE);
                            iv_fb.setVisibility(View.GONE);
                            String[] photoUrls = wallList.get(position - 1).getPostUrls().split(",");

                            if (photoUrls != null && photoUrls.length > 1) {


                                if (photoUrls[0] != null
                                        && !photoUrls[0]
                                        .equalsIgnoreCase("")) {

                                    if (photoUrls[0].contains(ServiceResource.BASE_IMG_URL)) {

                                        photoUrls[0] = wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, "");

                                    }

                                    if (photoUrls[0].contains("DataFiles/")) {

                                        photoUrls[0] = wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", "");

                                    }

                                    try {

                                        RequestOptions options = new RequestOptions()
                                                .centerCrop()
                                                .placeholder(R.drawable.photo)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .priority(Priority.HIGH)
                                                .dontAnimate()
                                                .dontTransform();

                                        Glide.with(mContext)
                                                .load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                        photoUrls[0]
                                                                .replace("//DataFiles//", "/DataFiles/")
                                                                .replace("//DataFiles/", "/DataFiles/"))

                                                .apply(options)
                                                .into(img_one);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }

                                }

                                if (photoUrls[1] != null
                                        && !photoUrls[1]
                                        .equalsIgnoreCase("")) {
                                    if (photoUrls[1].contains(ServiceResource.BASE_IMG_URL)) {
                                        photoUrls[1] = wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, "");
                                    }
                                    if (photoUrls[1].contains("DataFiles/")) {
                                        photoUrls[1] = wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", "");
                                    }

                                    try {

                                        RequestOptions options = new RequestOptions()
                                                .centerCrop()
                                                .placeholder(R.drawable.photo)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .priority(Priority.HIGH)
                                                .dontAnimate()
                                                .dontTransform();

                                        Glide.with(mContext)
                                                .load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                        photoUrls[1]
                                                                .replace("//DataFiles//", "/DataFiles/")
                                                                .replace("//DataFiles/", "/DataFiles/"))
                                                .apply(options)
                                                .into(img_two);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }

                                }

                            }

                        } else if (Integer.valueOf(wallList.get(position - 1).getPostCount()) > 2) {

                            fl_threeormore.setVisibility(View.VISIBLE);
                            ll_twoormore.setVisibility(View.VISIBLE);
                            iv_fb.setVisibility(View.GONE);
                            String[] photoUrls = wallList.get(position - 1).getPostUrls().split(",");

                            if (photoUrls != null && photoUrls.length > 2) {

                                if (photoUrls[0] != null
                                        && !photoUrls[0]
                                        .equalsIgnoreCase("")) {

                                    if (photoUrls[0].contains(ServiceResource.BASE_IMG_URL)) {

                                        photoUrls[0] = wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, "");

                                    }

                                    if (photoUrls[0].contains("DataFiles/")) {

                                        photoUrls[0] = wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", "");

                                    }

                                    try {

                                        RequestOptions options = new RequestOptions()
                                                .centerCrop()
                                                .placeholder(R.drawable.photo)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .priority(Priority.HIGH)
                                                .dontAnimate()
                                                .dontTransform();

                                        Glide.with(mContext)
                                                .load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                        photoUrls[0]
                                                                .replace("//DataFiles//", "/DataFiles/")
                                                                .replace("//DataFiles/", "/DataFiles/"))
                                                .apply(options)
                                                .into(img_one);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }

                                }

                                if (photoUrls[1] != null && !photoUrls[1].equalsIgnoreCase("")) {

                                    if (photoUrls[1].contains(ServiceResource.BASE_IMG_URL)) {
                                        photoUrls[1] = wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, "");
                                    }

                                    if (photoUrls[1].contains("DataFiles/")) {
                                        photoUrls[1] = wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", "");
                                    }

                                    try {

                                        RequestOptions options = new RequestOptions()
                                                .centerCrop()
                                                .placeholder(R.drawable.photo)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .priority(Priority.HIGH)
                                                .dontAnimate()
                                                .dontTransform();

                                        Glide.with(mContext)
                                                .load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                        photoUrls[1]
                                                                .replace("//DataFiles//", "/DataFiles/")
                                                                .replace("//DataFiles/", "/DataFiles/"))
                                                .apply(options)
                                                .into(img_two);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }

                                }

                                if (photoUrls[2] != null && !photoUrls[2].equalsIgnoreCase("")) {

                                    if (photoUrls[2].contains(ServiceResource.BASE_IMG_URL)) {

                                        photoUrls[2] = wallList.get(position - 1).getPhoto().replaceAll(ServiceResource.BASE_IMG_URL, "");
                                    }

                                    if (photoUrls[2].contains("DataFiles/")) {

                                        photoUrls[2] = wallList.get(position - 1).getPhoto().replaceAll("DataFiles/", "");

                                    }

                                    try {

                                        RequestOptions options = new RequestOptions()
                                                .centerCrop()
                                                .placeholder(R.drawable.photo)
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .priority(Priority.HIGH)
                                                .dontAnimate()
                                                .dontTransform();

                                        Glide.with(mContext)
                                                .load(ServiceResource.BASE_IMG_URL + "DataFiles/" +
                                                        photoUrls[2]
                                                                .replace("//DataFiles//", "/DataFiles/")
                                                                .replace("//DataFiles/", "/DataFiles/"))
                                                .apply(options)
                                                .into(img_three);

                                    } catch (Exception e) {

                                        e.printStackTrace();

                                    }
                                }
                            }

                            if (Integer.valueOf(wallList.get(position - 1).getPostCount()) > 3) {

                                ll_fourormore.setVisibility(View.VISIBLE);
                                txtImageCount.setText((Integer.valueOf(wallList.get(position - 1).getPostCount()) - 3) + " + More");

                            }

                        }

                    }


                }

                if (wallList.get(position - 1).getProfilePicture() != null) {

                    if (wallList.get(position - 1).getProfilePicture() != null
                            && !wallList.get(position - 1).getProfilePicture()
                            .equalsIgnoreCase("")) {
                        if (wallList.get(position - 1).getProfilePicture().contains(ServiceResource.BASE_IMG_URL1)) {
                            wallList.get(position - 1).setProfilePicture(wallList.get(position - 1).getProfilePicture().replaceAll(ServiceResource.BASE_IMG_URL1, ""));
                        }

                        try {

                            RequestOptions options = new RequestOptions()
                                    .centerCrop()
                                    .placeholder(R.drawable.photo)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(Priority.HIGH)
                                    .dontAnimate()
                                    .dontTransform();

                            Glide.with(mContext)
                                    .load(ServiceResource.BASE_IMG_URL1 +
                                            wallList.get(position - 1).getProfilePicture()
                                                    .replace("//", "/")
                                                    .replace("//", "/"))
                                    .apply(options)
                                    .into(profilePicUser);


                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                    }

                }

                postContent.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {
                            int postSingle = Integer.valueOf(v.getTag().toString()) - 1;
                            if (!Utility.isTeacher(mContext)) {

                                if (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("HomeWork") || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("ClassWork")
                                        || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("Circular")
                                        || (wallList.get(postSingle).getAssociationType().equalsIgnoreCase("Note")
                                )))) {
                                    HomeworkListIsRead(wallList.get(postSingle).getAssociationID(), wallList.get(postSingle).getAssociationType());
                                }
                            }

                            Intent i = new Intent(mContext, SinglePostActivity.class);
                            i.putExtra("aId", wallList.get(postSingle).getPostCommentID());
                            i.putExtra("aType", wallList.get(postSingle).getAssociationType());
                            i.putExtra("pos", postSingle);
                            mContext.startActivity(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });

                ll_comment.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        int commentpos = Integer.valueOf(v.getTag().toString()) - 1;
                        Intent i = new Intent(mContext, CommentActivity.class);
                        i.putExtra("WallComment", wallList.get(commentpos));
                        i.putExtra("pos", commentpos);
                        i.putExtra("from", from);
                        mContext.startActivity(i);

                    }

                });

                ll_twoormore.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {

                            int ivfbpos = Integer.valueOf(view.getTag().toString()) - 1;

                            if (wallList.get(ivfbpos).getFileType().equals("VIDEO")) {

                                Intent in = new Intent(mContext, VideoViewActivity.class);
                                in.putExtra("img", wallList.get(ivfbpos).getPhoto());
                                in.putExtra("name", wallList.get(ivfbpos).getFullName());
                                in.putExtra("FromStr", "MyWall");
                                mContext.startActivity(in);

                            } else if (wallList.get(ivfbpos).getFileType().equals("FILE")) {

                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(wallList.get(ivfbpos).getPhoto()));
                                mContext.startActivity(i);

                            } else {

                                Intent in = new Intent(mContext, ImageListActivity.class);
                                in.putExtra("model", wallList.get(ivfbpos));
                                in.putExtra("isAlredy", true);
                                mContext.startActivity(in);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }


                });

                iv_fb.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {

                            int ivfbpos = Integer.valueOf(v.getTag().toString()) - 1;

                            if (!Utility.isTeacher(mContext)) {

                                if (wallList.get(ivfbpos).getAssociationType().equalsIgnoreCase("HomeWork") || (wallList.get(ivfbpos).getAssociationType().equalsIgnoreCase("ClassWork")
                                        || (wallList.get(ivfbpos).getAssociationType().equalsIgnoreCase("Circular")
                                        || (wallList.get(ivfbpos).getAssociationType().equalsIgnoreCase("Note")
                                )))) {
                                    HomeworkListIsRead(wallList.get(ivfbpos).getAssociationID(), wallList.get(ivfbpos).getAssociationType());
                                }
                            }

                            if (wallList.get(ivfbpos).getFileType().equals("VIDEO")) {

                                Intent in = new Intent(mContext, VideoViewActivity.class);
                                in.putExtra("img", wallList.get(ivfbpos).getPhoto());
                                in.putExtra("name", wallList.get(ivfbpos).getFullName());
                                in.putExtra("FromStr", "MyWall");
                                mContext.startActivity(in);

                            } else if (wallList.get(ivfbpos).getFileType().equals("FILE")) {

                                String SELECTEDFILEURL = ServiceResource.BASE_IMG_URL + "DataFiles/" + wallList.get(ivfbpos).getPhoto();
                                Log.d("getFileurl", SELECTEDFILEURL + " " + wallList.get(ivfbpos).getPhoto());
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(SELECTEDFILEURL));
                                mContext.startActivity(i);

                            } else {

                                Intent in = new Intent(mContext, ImageListActivity.class);
                                in.putExtra("model", wallList.get(ivfbpos));
                                in.putExtra("isAlredy", false);
                                mContext.startActivity(in);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                });

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return convertView;

    }

    private void HomeworkListIsRead(String associationID, String associationType) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, associationID));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, associationType));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));

        Log.d("getapprveRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void deletePost(String postId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POSTID, postId));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.DELETEPOST_METHODNAME, ServiceResource.WALL_URL);

    }


    public void toast(String msg) {

        Utility.toast(mContext, "" + msg);

    }

    public class Likeasyns extends AsyncTask<Integer, Void, Void> {

        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        private ProgressDialog progressDialog;
        String result;
        int positionwall;
        boolean islike;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Integer... params) {

            Log.d("getPositio", String.valueOf(params[0]));
            positionwall = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.LIKE_PDATAID, wallList.get(positionwall).getPostCommentID());
            map1.put(ServiceResource.LIKE_PTYPE, wallList.get(positionwall).getAssociationType());
            map1.put(ServiceResource.SENDTOMEMBERID, wallList.get(positionwall).getMemberID());
            map1.put(ServiceResource.WALLID, wallList.get(positionwall).getWallID());
            map1.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map.put(2, map1);

            if (params[1] == 0) {

                islike = false;

            } else {

                islike = true;

            }

            Log.d("getrequestoflike", map.toString());

            result = webcall.getJSONFromSOAPWSWithBoolean(
                    ServiceResource.LIKE_METHODNAME, map,
                    ServiceResource.POST_URL, ServiceResource.LIKE_ISLIKEPARAM,
                    islike);

            JSONArray mainJsonArray;

            if (result != null && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {

                    mainJsonArray = new JSONArray(result);
                    for (int i = 0; i < mainJsonArray.length(); i++) {
                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        wallList.get(positionwall).setPostCommentID(
                                innerObj.getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallList.get(positionwall)
                                .setAssociationType(
                                        innerObj.getString(ServiceResource.WALL_ASSOCIATIONTYPE));
                        wallList.get(positionwall)
                                .setTotalLikes(
                                        innerObj.getString(ServiceResource.WALL_TOTALLIKES));
                        wallList.get(positionwall)
                                .setTotalDislike(
                                        innerObj.getString(ServiceResource.WALL_TOTALDISLIKE));
                        wallList.get(positionwall)
                                .setPostCommentNote(
                                        innerObj.getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                        wallList.get(positionwall)
                                .setTotalLikes(
                                        innerObj.getString(ServiceResource.WALL_TOTALLIKES));
                        wallList.get(positionwall)
                                .setTotalComments(
                                        innerObj.getString(ServiceResource.WALL_TOTALCOMMENTS));
                        wallList.get(positionwall)
                                .setTotalDislike(
                                        innerObj.getString(ServiceResource.WALL_TOTALDISLIKE));

                        if (params[1] == 0) {

                            wallList.get(positionwall).setIsLike("0");

                        } else {

                            wallList.get(positionwall).setIsLike("1");
                            if (wallList.get(positionwall).getIsDisLike() != null
                                    && wallList.get(positionwall)
                                    .getIsDisLike()
                                    .equalsIgnoreCase("1")) {
                                wallList.get(positionwall).setIsDisLike("0");

                            }

                        }

                        isLikeSucess = true;
//                        notifyDataSetChanged();
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                    isLikeSucess = false;

                }

            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Utility.sendPushNotification(mContext);
            if (isLikeSucess) {
                notifyDataSetChanged();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    isCall = true;

                }

            }, 2000);

        }

    }

    public class DisLikeasyns extends AsyncTask<Integer, Void, Void> {

        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        private ProgressDialog progressDialog;
        String result;
        int positionwall;
        WallPostModel wallPostModel;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext);
            LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
            LoaderProgress.setCancelable(false);
            LoaderProgress.show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            positionwall = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();
            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.LIKE_PDATAID, wallList.get(positionwall).getPostCommentID());
            map1.put(ServiceResource.LIKE_PTYPE, wallList.get(positionwall).getAssociationType());
            map1.put(ServiceResource.WALLID, wallList.get(positionwall).getWallID());
            map1.put(ServiceResource.SENDTOMEMBERID, wallList.get(positionwall).getMemberID());
            map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
            map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map.put(2, map1);
            boolean isDislike;
            if (params[1] == 1) {
                isDislike = true;
            } else {
                isDislike = false;
            }

            Log.d("dislikeresult", map.toString());

            result = webcall.getJSONFromSOAPWSWithBoolean(
                    ServiceResource.DISLIKE_METHODNAME, map,
                    ServiceResource.POST_URL, ServiceResource.LIKE_ISDISLIKE,
                    isDislike);

            JSONArray mainJsonArray;

            if (result != null
                    && !result.toString().equals(
                    "[{\"message\":\"No Data Found\",\"success\":0}]")) {

                try {

                    Log.d("dislikeresult", result);

                    mainJsonArray = new JSONArray(result);

                    for (int i = 0; i < mainJsonArray.length(); i++) {

                        JSONObject innerObj = mainJsonArray.getJSONObject(i);
                        wallList.get(positionwall)
                                .setTotalLikes(innerObj.getString(ServiceResource.WALL_TOTALLIKES));
                        wallList.get(positionwall)
                                .setTotalDislike(innerObj.getString(ServiceResource.WALL_TOTALDISLIKE));
                        wallList.get(positionwall)
                                .setPostCommentID(innerObj.getString(ServiceResource.WALL_POSTCOMMENTID));
                        wallList.get(positionwall)
                                .setPostCommentNote(innerObj.getString(ServiceResource.WALL_POSTCOMMENTNOTE));
                        wallList.get(positionwall)
                                .setTotalLikes(innerObj.getString(ServiceResource.WALL_TOTALLIKES));
                        wallList.get(positionwall)
                                .setTotalComments(innerObj.getString(ServiceResource.WALL_TOTALCOMMENTS));
                        wallList.get(positionwall)
                                .setTotalDislike(innerObj.getString(ServiceResource.WALL_TOTALDISLIKE));

                        if (params[1] == 0) {

                            wallList.get(positionwall).setIsDisLike("0");

                        } else {

                            wallList.get(positionwall).setIsLike("1");
                            if (wallList.get(positionwall).getIsLike() != null
                                    && wallList.get(positionwall).getIsLike()
                                    .equalsIgnoreCase("1")) {

                                wallList.get(positionwall).setIsLike("0");

                            }

                        }

                        isLikeSucess = true;
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                    isLikeSucess = false;

                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Utility.sendPushNotification(mContext);

            if (LoaderProgress != null)
                LoaderProgress.dismiss();

            if (isLikeSucess) {
                notifyDataSetChanged();
            }
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    isCall = true;
                }
            }, 2000);
        }
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.DELETEPOST_METHODNAME.equalsIgnoreCase(methodName)) {
            if (wallList != null && wallList.size() > 0) {
                wallList.remove(posRemove);
//                listAdapter.remove(listAdapter.getItem(id))
                notifyDataSetChanged();
            } else if (ServiceResource.WALL_METHODNAME.equalsIgnoreCase(methodName)) {
                listner.refresh(methodName);
            }

            interfaceClick.onDeletePost();
        }
    }

    public void onClickDelete(final String postId) {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, true);

//        mPoweroffDialog = new Dialog(mContext);
//        mPoweroffDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mPoweroffDialog.getWindow().setFlags(
//                WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        mPoweroffDialog.getWindow().setBackgroundDrawableResource(
//                android.R.color.transparent);
//        mPoweroffDialog.setContentView(R.layout.dialog_logout_password);
//        mPoweroffDialog.setCancelable(true);
//        mPoweroffDialog.show();

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog.findViewById(R.id.ll_submit);
        TextView tv_header = (TextView) mPoweroffDialog.findViewById(R.id.tv_header);
        TextView tv_say_something = (TextView) mPoweroffDialog.findViewById(R.id.tv_say_something);
        tv_say_something.setText(mContext.getResources().getString(R.string.Areyousureyouwanttodeletepost));
        tv_header.setText(mContext.getResources().getString(R.string.Delete));
        ll_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                deletePost(postId);
                mPoweroffDialog.dismiss();

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog.dismiss();

            }

        });

    }

    private LinkPreviewCallback callback = new LinkPreviewCallback() {
        private View mainView;
        private LinearLayout linearLayout;
        private View loading;
        private ImageView imageView;
        LinearLayout dropPreview;

        @Override
        public void onPre(View v) {

            dropPreview = (LinearLayout) v;
            currentImageSet = null;
            currentItem = 0;
            currentImage = null;
            noThumb = false;
            currentTitle = currentDescription = currentUrl = currentCannonicalUrl = "";
            layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mainView = layoutInfalater.inflate(R.layout.main_view, null);
            linearLayout = (LinearLayout) mainView.findViewById(R.id.external);
            loading = layoutInfalater.inflate(R.layout.loading, linearLayout);
            dropPreview.addView(mainView);
            dropPreview.setVisibility(View.VISIBLE);

        }

        public void setLayout(LinearLayout drop_preview) {
            this.dropPreview = drop_preview;
        }

        @Override
        public void onPos(final SourceContent sourceContent, boolean isNull) {

            linearLayout.removeAllViews();

            if (isNull || sourceContent.getFinalUrl().equals("")) {
                View failed = layoutInfalater.inflate(R.layout.failed, linearLayout);
                TextView titleTextView = (TextView) failed.findViewById(R.id.text);
                titleTextView.setText(mContext.getString(R.string.failed_preview) + "\n" + sourceContent.getFinalUrl());

                failed.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dropPreview.setVisibility(View.GONE);
                    }

                });

            } else {

                currentImageSet = new Bitmap[sourceContent.getImages().size()];
                layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View content = layoutInfalater.inflate(R.layout.preview_content, linearLayout);
                final LinearLayout infoWrap = (LinearLayout) content.findViewById(R.id.info_wrap);
                final LinearLayout titleWrap = (LinearLayout) infoWrap.findViewById(R.id.title_wrap);
                final LinearLayout thumbnailOptions = (LinearLayout) content.findViewById(R.id.thumbnail_options);
                final LinearLayout noThumbnailOptions = (LinearLayout) content.findViewById(R.id.no_thumbnail_options);
                final ImageView imageSet = (ImageView) content.findViewById(R.id.image_post_set);
                final ImageView close = (ImageView) titleWrap.findViewById(R.id.close);
                final TextView titleTextView = (TextView) titleWrap.findViewById(R.id.title);
                final TextView titleEditText = (TextView) titleWrap.findViewById(R.id.input_title);
                final TextView urlTextView = (TextView) content.findViewById(R.id.url);
                final TextView descriptionTextView = (TextView) content.findViewById(R.id.description);
                final TextView descriptionEditText = (TextView) content.findViewById(R.id.input_description);
                final TextView countTextView = (TextView) thumbnailOptions
                        .findViewById(R.id.count);
                final CheckBox noThumbCheckBox = (CheckBox) noThumbnailOptions
                        .findViewById(R.id.no_thumbnail_checkbox);
                final Button previousButton = (Button) thumbnailOptions
                        .findViewById(R.id.post_previous);
                final Button forwardButton = (Button) thumbnailOptions
                        .findViewById(R.id.post_forward);

                editTextTitlePost = titleEditText;
                editTextDescriptionPost = descriptionEditText;
                thumbnailOptions.setVisibility(View.GONE);
                close.setVisibility(View.GONE);
                titleTextView.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        titleTextView.setVisibility(View.GONE);
                        titleEditText.setText(TextCrawler.extendedTrim(titleTextView.getText()
                                .toString()));
                        titleEditText.setVisibility(View.VISIBLE);
                    }
                });

                titleEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {

                        if (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                            titleEditText.setVisibility(View.GONE);
                            currentTitle = TextCrawler.extendedTrim(titleEditText.getText().toString());
                            titleTextView.setText(currentTitle);
                            titleTextView.setVisibility(View.VISIBLE);

                        }

                        return false;

                    }

                });

                descriptionTextView.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        descriptionTextView.setVisibility(View.GONE);
                        descriptionEditText.setText(TextCrawler.extendedTrim(descriptionTextView.getText().toString()));
                        descriptionEditText.setVisibility(View.VISIBLE);
                    }
                });

                descriptionEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {

                        if (arg2.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            descriptionEditText.setVisibility(View.GONE);
                            currentDescription = TextCrawler.extendedTrim(descriptionEditText.getText().toString());
                            descriptionTextView.setText(currentDescription);
                            descriptionTextView.setVisibility(View.VISIBLE);
                        }

                        return false;
                    }
                });

                close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        dropPreview.setVisibility(View.GONE);
                    }
                });

                noThumbCheckBox
                        .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                            @Override
                            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                                noThumb = arg1;
                                if (sourceContent.getImages().size() > 1)
                                    if (noThumb)
                                        thumbnailOptions.setVisibility(View.GONE);
                                    else
                                        thumbnailOptions.setVisibility(View.GONE);

                                showHideImage(imageSet, infoWrap, !noThumb);
                            }
                        });

                previousButton.setEnabled(false);
                previousButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (currentItem > 0)
                            changeImage(previousButton, forwardButton,
                                    currentItem - 1, sourceContent,
                                    countTextView, imageSet, sourceContent
                                            .getImages().get(currentItem - 1),
                                    currentItem);
                    }
                });

                forwardButton.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (currentItem < sourceContent.getImages().size() - 1)
                            changeImage(previousButton, forwardButton,
                                    currentItem + 1, sourceContent,
                                    countTextView, imageSet, sourceContent.getImages().get(currentItem + 1), currentItem);
                    }
                });

                if (sourceContent.getImages().size() > 0) {

                    if (sourceContent.getImages().size() > 1) {
                        countTextView.setText("1 " + mContext.getString(R.string.of)
                                + " " + sourceContent.getImages().size());
                        thumbnailOptions.setVisibility(View.GONE);
                    }

                    noThumbnailOptions.setVisibility(View.GONE);

                    UrlImageViewHelper.setUrlDrawable(imageSet, sourceContent
                            .getImages().get(0), new UrlImageViewCallback() {

                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {
                            if (loadedBitmap != null) {
                                currentImage = loadedBitmap;
                                currentImageSet[0] = loadedBitmap;
                            }
                        }
                    });

                } else {
                    showHideImage(imageSet, infoWrap, false);
                }

                if (sourceContent.getTitle().equals(""))
                    sourceContent.setTitle(mContext.getString(R.string.enter_title));
                if (sourceContent.getDescription().equals(""))
                    sourceContent.setDescription(mContext.getString(R.string.enter_description));

                titleTextView.setText(sourceContent.getTitle());
                urlTextView.setText(sourceContent.getCannonicalUrl());
                descriptionTextView.setText(sourceContent.getDescription());

            }

            currentTitle = sourceContent.getTitle();
            currentDescription = sourceContent.getDescription();
            currentUrl = sourceContent.getUrl();
            currentCannonicalUrl = sourceContent.getCannonicalUrl();
        }
    };

    /**
     * Change the current image in image set
     */
    private void changeImage(Button previousButton, Button forwardButton,
                             final int index, SourceContent sourceContent,
                             TextView countTextView, ImageView imageSet, String url,
                             final int current) {

        if (currentImageSet[index] != null) {
            currentImage = currentImageSet[index];
            imageSet.setImageBitmap(currentImage);
        } else {
            UrlImageViewHelper.setUrlDrawable(imageSet, url,
                    new UrlImageViewCallback() {

                        @Override
                        public void onLoaded(ImageView imageView,
                                             Bitmap loadedBitmap, String url,
                                             boolean loadedFromCache) {
                            if (loadedBitmap != null) {
                                currentImage = loadedBitmap;
                                currentImageSet[index] = loadedBitmap;
                            }
                        }
                    });

        }

        currentItem = index;

        if (index == 0)
            previousButton.setEnabled(false);
        else
            previousButton.setEnabled(true);

        if (index == sourceContent.getImages().size() - 1)
            forwardButton.setEnabled(false);
        else
            forwardButton.setEnabled(true);

        countTextView.setText((index + 1) + " " + mContext.getString(R.string.of) + " " + sourceContent.getImages().size());

    }

    public void SetWallList(ArrayList<WallPostModel> wallPostModels) {

        wallList.clear();
        wallList.addAll(wallPostModels);
        notifyDataSetChanged();

    }

    /**
     * Show or hide the image layout according to the "No Thumbnail" ckeckbox
     */
    private void showHideImage(View image, View parent, boolean show) {

        if (show) {

            image.setVisibility(View.VISIBLE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 2f));

        } else {

            image.setVisibility(View.GONE);
            parent.setPadding(5, 5, 5, 5);
            parent.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f));

        }

    }

}

