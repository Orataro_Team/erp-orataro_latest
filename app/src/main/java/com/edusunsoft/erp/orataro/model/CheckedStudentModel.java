package com.edusunsoft.erp.orataro.model;

public class CheckedStudentModel {

    String ContactNo1, FullName, MemberID, ReadApproveID,RollNo;
    boolean IsRead, IsApprove;

    public CheckedStudentModel() {
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public String getContactNo1() {
        return ContactNo1;
    }

    public void setContactNo1(String contactNo1) {
        ContactNo1 = contactNo1;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getReadApproveID() {
        return ReadApproveID;
    }

    public void setReadApproveID(String readApproveID) {
        ReadApproveID = readApproveID;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    public boolean isApprove() {
        return IsApprove;
    }

    public void setApprove(boolean approve) {
        IsApprove = approve;
    }

    @Override
    public String toString() {
        return "CheckedStudentModel{" +
                "ContactNo1='" + ContactNo1 + '\'' +
                ", FullName='" + FullName + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", ReadApproveID='" + ReadApproveID + '\'' +
                ", IsRead=" + IsRead +
                ", IsApprove=" + IsApprove +
                '}';
    }
}
