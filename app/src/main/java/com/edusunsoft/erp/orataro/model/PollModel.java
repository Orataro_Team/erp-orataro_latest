package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PollModel implements Serializable {

    private String subName, participateCOunt, voteCount, date, year, startDate, totalVotes;
    private String PollID, EndDate, Title, Details, StartIn, EndIn, UserName, Participant;
    private String isNotify, isPresentage, isMultichoice;
    private ArrayList<PollOptionModel> optonList;

    public String getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(String totalVotes) {
        this.totalVotes = totalVotes;
    }

    public ArrayList<PollOptionModel> getOptonList() {
        return optonList;
    }

    public void setOptonList(ArrayList<PollOptionModel> optonList) {
        this.optonList = optonList;
    }

    public boolean getIsNotify() {
        return StrToBoolean(isNotify);
    }

    public void setIsNotify(String isNotify) {
        this.isNotify = isNotify;
    }

    public boolean getIsPresentage() {
        return StrToBoolean(isPresentage);
    }

    public void setIsPresentage(String isPresentage) {
        this.isPresentage = isPresentage;
    }

    public boolean getIsMultichoice() {
        return StrToBoolean(isMultichoice);
    }

    public void setIsMultichoice(String isMultichoice) {
        this.isMultichoice = isMultichoice;
    }

    public String getPollID() {
        return PollID;
    }

    public void setPollID(String pollID) {
        PollID = pollID;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getStartIn() {
        return StartIn;
    }

    public void setStartIn(String startIn) {
        StartIn = startIn;
    }

    public String getEndIn() {
        return EndIn;
    }

    public void setEndIn(String endIn) {
        EndIn = endIn;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getParticipant() {
        return Participant;
    }

    public void setParticipant(String participant) {
        Participant = participant;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    boolean changeDate = false;
    String month;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public boolean isChangeDate() {
        return changeDate;
    }

    public void setChangeDate(boolean changeDate) {
        this.changeDate = changeDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getParticipateCOunt() {
        return participateCOunt;
    }

    public void setParticipateCOunt(String participateCOunt) {
        this.participateCOunt = participateCOunt;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }

    public boolean StrToBoolean(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("true")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return "PollModel{" +
                "subName='" + subName + '\'' +
                ", participateCOunt='" + participateCOunt + '\'' +
                ", voteCount='" + voteCount + '\'' +
                ", date='" + date + '\'' +
                ", year='" + year + '\'' +
                ", startDate='" + startDate + '\'' +
                ", totalVotes='" + totalVotes + '\'' +
                ", PollID='" + PollID + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", Title='" + Title + '\'' +
                ", Details='" + Details + '\'' +
                ", StartIn='" + StartIn + '\'' +
                ", EndIn='" + EndIn + '\'' +
                ", UserName='" + UserName + '\'' +
                ", Participant='" + Participant + '\'' +
                ", isNotify='" + isNotify + '\'' +
                ", isPresentage='" + isPresentage + '\'' +
                ", isMultichoice='" + isMultichoice + '\'' +
                ", optonList=" + optonList +
                ", changeDate=" + changeDate +
                ", month='" + month + '\'' +
                '}';
    }
}
