package com.edusunsoft.erp.orataro.model;

public class CountryModel {

    String CountryName, CountrCode;

    public CountryModel() {
    }

    public CountryModel(String countryName, String countrCode) {
        CountryName = countryName;
        CountrCode = countrCode;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountrCode() {
        return CountrCode;
    }

    public void setCountrCode(String countrCode) {
        CountrCode = countrCode;
    }

    @Override
    public String toString() {
        return "CountryModel{" +
                "CountryName='" + CountryName + '\'' +
                ", CountrCode='" + CountrCode + '\'' +
                '}';
    }
}
