package com.edusunsoft.erp.orataro.services;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Utility;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

import javax.net.ssl.TrustManager;

public class SoapParser {

    public static String c_method = "";
    private static TrustManager[] trustManagers;
    private String c_url = "";
    private String namespace = "http://tempuri.org/";
    private Context context;
    private ArrayList<PropertyVo> c_args;
    private Dialog mPoweroffDialog;

    public SoapParser() {


    }

    public SoapParser(ArrayList<PropertyVo> args, String method, String url) {
        c_method = method;
        c_url = url;
        c_args = args;
    }

    public String buildData(Context context) {
        SoapObject request = new SoapObject(namespace, c_method.toString());
        for (int i = 0; i < c_args.size(); i++) {
            request.addProperty(c_args.get(i).getName(), c_args.get(i).getValue());
        }
        String res = getResponse(request, context);
        System.out.println(res);
        return res;
    }

    public String getResponse(SoapObject request, final Context context) {
        String responce = "";

        try {

            if (Utility.isNetworkAvailable(context)) {

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);
                String surl = c_url + "?op=" + c_method.toString();
                new MarshalBase64().register(envelope);
                HttpTransportSE androidHttpTransport = new HttpTransportSE(c_url, 30000);
                androidHttpTransport.call(namespace + c_method.toString(), envelope);
                SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
                responce = result.toString();
            } else {

                ((Activity) context).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        mPoweroffDialog = CustomDialog.ShowDialog(context, R.layout.dialog_logout_password, false);

//                        mPoweroffDialog = new Dialog(context);
//                        mPoweroffDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        mPoweroffDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                        mPoweroffDialog.getWindow().setBackgroundDrawableResource(
//                                android.R.color.transparent);
//                        mPoweroffDialog.setContentView(R.layout.dialog_logout_password);
//                        mPoweroffDialog.setCancelable(false);
//                        mPoweroffDialog.show();

                        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog.findViewById(R.id.ll_submit);

                        TextView tv_say_something = (TextView) mPoweroffDialog.findViewById(R.id.tv_say_something);

                        tv_say_something.setText(context.getResources().getString(R.string.PleaseCheckyourinternetconnection));

                        TextView tv_header = (TextView) mPoweroffDialog.findViewById(R.id.tv_header);

                        tv_header.setText(context.getResources().getString(R.string.NoNetwork));


                        ll_submit.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mPoweroffDialog.dismiss();
                            }
                        });

                        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
                        img_close.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mPoweroffDialog.dismiss();
                            }
                        });
                    }
                });

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responce;
    }

}
