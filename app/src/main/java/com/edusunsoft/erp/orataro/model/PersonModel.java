package com.edusunsoft.erp.orataro.model;

public class PersonModel {

    private String profileImg, personName, friendId, FriendListID, WallID;
    private boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendListID() {
        return FriendListID;
    }

    public void setFriendListID(String friendListID) {
        FriendListID = friendListID;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

}
