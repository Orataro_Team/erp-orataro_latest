package com.edusunsoft.erp.orataro.model;

public class DynamicWallModel {

	private String WallID,WallName,WallImage,AssociationType;

	public String getWallID() {
		return WallID;
	}

	public void setWallID(String wallID) {
		WallID = wallID;
	}

	public String getWallName() {
		return WallName;
	}

	public void setWallName(String wallName) {
		WallName = wallName;
	}

	public String getWallImage() {
		return WallImage;
	}

	public void setWallImage(String wallImage) {
		WallImage = wallImage;
	}

	public String getAssociationType() {
		return AssociationType;
	}

	public void setAssociationType(String associationType) {
		AssociationType = associationType;
	}

	@Override
	public String toString() {

		return "DynamicWallModel{" +
				"WallID='" + WallID + '\'' +
				", WallName='" + WallName + '\'' +
				", WallImage='" + WallImage + '\'' +
				", AssociationType='" + AssociationType + '\'' +
				'}';

	}
}
