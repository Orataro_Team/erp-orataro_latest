package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.DeleteItemNewPost;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AlbumPhotoListActivity;
import com.edusunsoft.erp.orataro.activities.UploadHomeworkActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PhotoListModel;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.edusunsoft.erp.orataro.services.ServiceResource.FROMUPLOADHOMEWORK;

public class PhotoListAdapter extends BaseAdapter implements ResponseWebServices {

    private static final int ID_DEL = 1;
    private static final int ID_PUBLIC = 2;
    private static final int ID_ONLY_ME = 3;
    private static final int ID_FRIEND = 4;
    private static final int ID_SP_FRIEND = 5;

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private ImageView btn_delete;
    public ImageView iv_profile_pic, btn_close;
    private LinearLayout ll_option;
    private LinearLayout albumDetaillayout;
    private TextView txt_title, txtDetail;
    private ArrayList<PhotoModel> photoModels;
    private DeleteItemNewPost deleteItemNewPost;
    private ActionItem actionDel, actionSpFrnd, actionPublic, actionFriend, actionOnlyMe;
    private QuickAction quickAction;
    private int flag = 0;
    public int posRemove;

    public PhotoListAdapter(Context context, ArrayList<PhotoModel> photoModels, int flag) {

        mContext = context;
        this.photoModels = photoModels;
        this.flag = flag;

    }

    public PhotoListAdapter(Context context, ArrayList<PhotoModel> photoModels, int flag, DeleteItemNewPost pDeleteItemNewPost) {
        mContext = context;
        this.photoModels = photoModels;
        this.flag = flag;
        deleteItemNewPost = pDeleteItemNewPost;
    }

    @Override
    public int getCount() {
        return photoModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.photos_listraw, parent, false);
        ll_option = (LinearLayout) convertView.findViewById(R.id.ll_option);
        iv_profile_pic = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        btn_close = (ImageView) convertView.findViewById(R.id.btn_close);
        albumDetaillayout = (LinearLayout) convertView.findViewById(R.id.albumDetaillayout);

        txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        txtDetail = (TextView) convertView.findViewById(R.id.txt_desc);

        btn_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < UploadHomeworkActivity.GetImagename.size(); i++) {
                    if (UploadHomeworkActivity.GetImagename.get(i).equalsIgnoreCase(photoModels.get(position).getPhoto().replace("http://erporataro.orataro.com/", ""))) {
                        posRemove = position;
                        photoModels.remove(posRemove);
                        notifyDataSetChanged();
                        UploadHomeworkActivity.GetImagename.remove(i);
                    }

                }

            }

        });

        btn_delete = (ImageView) convertView.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                /*commented By Krishna : 21-05-2019 Delete Particular Album*/

                Utility.deleteDialog(mContext, mContext.getResources().getString(R.string.Album), photoModels.get(position).getAlbumTitle(), new Popup() {

                    @Override
                    public void deleteYes() {

                        posRemove = position;
                        Log.d("getposition", String.valueOf(posRemove));
                        deletePhoto(photoModels.get(position).getAlbumID(), position);

                    }

                    @Override
                    public void deleteNo() {
                    }

                });

                /*END*/

            }

        });

        if (photoModels.get(position).getPhoto() != null
                && !photoModels.get(position).getPhoto().equalsIgnoreCase("")) {

            if (photoModels.get(position).getPhoto().contains(ServiceResource.BASE_IMG_URL)) {

                try {

                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.photo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(photoModels.get(position).getPhoto())
                            .apply(options)
                            .into(iv_profile_pic);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                if (photoModels.get(position).getAlbumID() != null
                        && photoModels.get(position).getAlbumID().equals("1")) {
                    iv_profile_pic.setImageResource(R.drawable.dummy_video);
                } else {

                    Bitmap bm;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(photoModels.get(position).getPhoto(), options);
                    final int REQUIRED_SIZE = 200;
                    int scale = 1;
                    while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                            && options.outHeight / scale / 2 >= REQUIRED_SIZE) scale *= 2;
                    options.inSampleSize = scale;
                    options.inJustDecodeBounds = false;
                    bm = BitmapFactory.decodeFile(photoModels.get(position).getPhoto(), options);
                    iv_profile_pic.setImageBitmap(bm);
                }
            }
        }

        txt_title.setText(photoModels.get(position).getAlbumTitle());
        txtDetail.setText(photoModels.get(position).getTotal() + " Photos");

        actionDel = new ActionItem(ID_DEL, "Delete", mContext.getResources()
                .getDrawable(R.drawable.delete));
        actionSpFrnd = new ActionItem(ID_SP_FRIEND, "Special Friend", mContext
                .getResources().getDrawable(R.drawable.fb_sp_frnd));
        actionPublic = new ActionItem(ID_PUBLIC, "Public", mContext
                .getResources().getDrawable(R.drawable.fb_publics));
        actionFriend = new ActionItem(ID_FRIEND, "Friends", mContext
                .getResources().getDrawable(R.drawable.fb_friends));
        actionOnlyMe = new ActionItem(ID_ONLY_ME, "Only Me", mContext
                .getResources().getDrawable(R.drawable.fb_only_me));

        quickAction = new QuickAction(mContext, QuickAction.VERTICAL);

        quickAction.addActionItem(actionDel);
        quickAction.addActionItem(actionPublic);
        quickAction.addActionItem(actionOnlyMe);
        quickAction.addActionItem(actionFriend);
        quickAction.addActionItem(actionSpFrnd);

        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos, int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);
                    }
                });

        ll_option.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickAction.show(v);
            }
        });

        iv_profile_pic.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (flag == 0) {

                    getAlbumPhotos(photoModels.get(position).getAlbumID());

                } else {

                    try {
                        if (photoModels.get(position).getAlbumID() != null) {
                            if (photoModels.get(position).getAlbumID().equals("0") || photoModels.get(position).getAlbumID()
                                    .equals("1")) {
                                deleteItemNewPost.DeleteItem(position);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        if (flag == 1) {
            albumDetaillayout.setVisibility(View.GONE);
            btn_close.setVisibility(View.VISIBLE);
        }

        return convertView;
    }


    public void getAlbumPhotos(String albumId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_ID, albumId));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETALLPHOTOSBYALBUM_METHODNAME,
                ServiceResource.ALBUM_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETALLPHOTOSBYALBUM_METHODNAME.equalsIgnoreCase(methodName)) {
            ArrayList<PhotoListModel> photoListModels;

            JSONArray jsonObj = null;
            photoListModels = new ArrayList<PhotoListModel>();
            try {
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    PhotoListModel model = new PhotoListModel();
                    model.setAlbumPhotosID("AlbumPhotosID");
                    model.setPhoto(innerObj.getString(ServiceResource.ALBUM_PHOTO));

                    if (model.getPhoto() != null && !model.getPhoto().equals("")) {
                        model.setPhoto(ServiceResource.BASE_IMG_URL + "/DataFiles/" + model.getPhoto());
                    }

                    photoListModels.add(model);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (photoListModels != null && photoListModels.size() > 0) {

                FROMUPLOADHOMEWORK = "false";
                Intent album_photo_list = new Intent(mContext, AlbumPhotoListActivity.class);
                album_photo_list.putParcelableArrayListExtra("photoModels", photoListModels);
                mContext.startActivity(album_photo_list);

            }

        } else if (ServiceResource.DELETEPHOTOVIDEO_METHODNAME.equalsIgnoreCase(methodName)) {

            photoModels.remove(posRemove);
            notifyDataSetChanged();
            Utility.showToast(mContext.getResources().getString(R.string.albumdelete), mContext);

        }


    }

    private void deletePhoto(String photoalbumID, int position) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ASSTYPE, "Album"));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.PhotoDelete_ID, photoalbumID));

        Log.d("getRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.DELETEPHOTOVIDEO_METHODNAME, ServiceResource.POST_URL);

    }


}
