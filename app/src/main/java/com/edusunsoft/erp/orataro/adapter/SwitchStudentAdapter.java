package com.edusunsoft.erp.orataro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SwitchStudentAdapter extends BaseAdapter implements ResponseWebServices {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private CircleImageView iv_std_icon;
    private TextView tv_std_name, tv_std_gendar, tv_std_division, tv_std_standard;
    private List<LoginUserModel> switchStudentModels = new ArrayList<>();
    private static int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;
    private Utility utility;
    private int count;
    private LinearLayout ll_std, ll_division, ll_batches;
    private TextView txt_usertype;
    private ImageView iv_std;
    private TextView tv_schoolname;
    private String regId;
    private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;


    public SwitchStudentAdapter(Context context, List<LoginUserModel> switchStudentModels) {

        this.mContext = context;
        this.switchStudentModels = switchStudentModels;

    }

    @Override
    public int getCount() {
        return switchStudentModels.size();
    }

    @Override
    public Object getItem(int position) {

        return null;

    }

    @Override
    public long getItemId(int position) {

        return 0;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.switch_student_listraw, parent, false);
        utility = new Utility();
        ImageView iv_right = (ImageView) convertView.findViewById(R.id.iv_right);
        ll_std = (LinearLayout) convertView.findViewById(R.id.ll_std);
        ll_division = (LinearLayout) convertView.findViewById(R.id.ll_division);
        ll_batches = (LinearLayout) convertView.findViewById(R.id.ll_batches);
        iv_std = (ImageView) convertView.findViewById(R.id.iv_std);
        txt_usertype = (TextView) convertView.findViewById(R.id.tv_usertype);
        tv_schoolname = (TextView) convertView.findViewById(R.id.tv_schoolname);
        iv_std_icon = (CircleImageView) convertView.findViewById(R.id.iv_std_icon);
        tv_std_name = (TextView) convertView.findViewById(R.id.tv_std_name);
        tv_std_gendar = (TextView) convertView.findViewById(R.id.tv_std_gendar);
        tv_std_division = (TextView) convertView.findViewById(R.id.tv_std_division);
        tv_std_standard = (TextView) convertView.findViewById(R.id.tv_std_standard);

        try {

            // set selected user
            if(new UserSharedPrefrence(mContext).getLoginModel() != null){
                if(new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(switchStudentModels.get(position).getUserID())){
                    iv_right.setVisibility(View.VISIBLE);
                }
            }

            Log.d("getsqlconnsStr", switchStudentModels.get(position).getSQLConStr());

            tv_std_name.setText(switchStudentModels.get(position).getMemberName());
            if (Utility.isNull(switchStudentModels.get(position).getInstituteName())) {
                tv_schoolname.setText(switchStudentModels.get(position).getInstituteName());
            }
            if (Utility.isNull(switchStudentModels.get(position).getStandardName())) {
                tv_std_standard.setText(switchStudentModels.get(position).getStandardName());
            }
            if (Utility.isNull(switchStudentModels.get(position).getDivisionName())) {
                tv_std_division.setText(switchStudentModels.get(position).getDivisionName());
            }

            if (Utility.isNull(switchStudentModels.get(position).getUserType_Term())) {
                txt_usertype.setText(switchStudentModels.get(position).getUserType_Term());
            }

            if (switchStudentModels.get(position).getUserType_Term().equalsIgnoreCase("Student") ||
                    switchStudentModels.get(position).getUserType_Term().equalsIgnoreCase("Parent")) {
            } else {
                iv_std.setVisibility(View.GONE);
                tv_std_standard.setVisibility(View.GONE);
                tv_std_division.setVisibility(View.GONE);
                ll_std.setVisibility(View.GONE);
                ll_division.setVisibility(View.GONE);
                ll_batches.setVisibility(View.GONE);
            }

            Log.d("getavatara", switchStudentModels.get(position).toString());


            if (Utility.isNull(switchStudentModels.get(position).getAvatar())) {

                try {

                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.ic_user_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(ServiceResource.BASE_IMG_URL1 + switchStudentModels.get(position).getAvatar().replace("//", "/"))
                            .apply(options)
                            .into(iv_std_icon);

                    Log.d("getprofilepicurl", ServiceResource.BASE_IMG_URL1 + switchStudentModels.get(position).getAvatar().replace("//", "/"));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("getSQLSTR", switchStudentModels.get(position).getSQLConStr());

        convertView.setBackgroundColor(colors[position % colors.length]);

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                PreferenceData.setLogin(true);
                PreferenceData.setCTOKEN(switchStudentModels.get(position).getSQLConStr());
                UserSharedPrefrence.saveLoginData(mContext, switchStudentModels.get(position));
                try {
                    userSelection(switchStudentModels.get(position).getUserID(), switchStudentModels.get(position).getSQLConStr());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

        return convertView;

    }

    private void userSelection(String userID, String sqlConStr) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, userID));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_IDENTITY, Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID)));
        arrayList.add(new PropertyVo(ServiceResource.DEVICE_TYPE, "android"));

        Log.d("userselectionrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.USER_SELECTION, ServiceResource.REGISTATION_URL);

    }

    public void changeGCMID(String UserId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            ERPOrataroDatabase.getERPOrataroDatabase(mContext).clearAllTables();
            PreferenceData.setIsSwitched(false);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }

//            notificationCount();

            Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
            intentHomeWork.putExtra("position", mContext.getResources().getString(R.string.Wall));
            mContext.startActivity(intentHomeWork);
            ((Activity) mContext).finish();
            ((Activity) mContext).overridePendingTransition(0, 0);

        } else if (ServiceResource.USER_SELECTION.equalsIgnoreCase(methodName)) {

            if (!UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("NAN") || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("")
                    || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals("null") || UserSharedPrefrence.loadLoginData(mContext).getDisplayName().equals(null)) {

                Global.userdataModel = new LoginModel();

                if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {

                } else {

                    JSONArray jsonObj;

                    try {


                        jsonObj = new JSONArray(result);
                        for (int i = 0; i < jsonObj.length(); i++) {

                            JSONObject innerObj = jsonObj.getJSONObject(i);

                            try {

                                /*store selected user data into loginmodel*/
                                Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
//                                Global.userdataModel = gson.fromJson(innerObj.toString(), LoginModel.class);
                                Log.d("getResult", Global.userdataModel.toString());

                                /*END*/

                                new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);

                                /*END*/

                                // get notification count
                                try {
                                    notificationCount();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            /*END*/

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

        } else if (ServiceResource.CHKINSTITUTEORUSEREXISTINERP_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result.equalsIgnoreCase("isavailable")) {
                new UserSharedPrefrence(mContext).setIsERP(true);
            } else {
                new UserSharedPrefrence(mContext).setIsERP(false);
            }

            GetUserRoleRightList();

        } else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {

            try {

                Log.d("getRoleRightsResult", result);

                JSONArray jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).setREADWRITESETTING(result);
                Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
                ReadWriteSettingModel model;
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new ReadWriteSettingModel();
                    model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                    model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));

                    if (!new UserSharedPrefrence(mContext).getLoginModel().getRoleName().equalsIgnoreCase("teacher")) {
                        model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                        model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                        model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                        model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));

                    } else {
                        model.setIsView("true");
                        model.setIsEdit("true");
                        model.setIsCreate("true");
                        model.setIsDelete("true");
                    }

                    Global.readWriteSettingList.add(model);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            saveLoginLog();

        } else if (ServiceResource.SAVELOGINLOG_METHODNAME.equalsIgnoreCase(methodName)) {

            changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

        } else if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONObject obj;
            JSONArray jsonObj;
            JSONArray jsonObjfriend;
            JSONArray jsonObjleave;

            try {

                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                obj = new JSONObject(result);

                jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
                jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);
                jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));

                }

                for (int i = 0; i < jsonObjfriend.length(); i++) {

                    JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));

                }

                for (int i = 0; i < jsonObjleave.length(); i++) {

                    JSONObject innerObj = jsonObjleave.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            GetUserRoleRightList();

        }

    }

    public void saveLoginLog() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SAVELOGINLOG_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void GetUserRoleRightList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, new UserSharedPrefrence(mContext).getLoginModel().getRoleID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void notificationCount() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, Global.userdataModel.getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.NOTIFICATIONCOUNT_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

}
