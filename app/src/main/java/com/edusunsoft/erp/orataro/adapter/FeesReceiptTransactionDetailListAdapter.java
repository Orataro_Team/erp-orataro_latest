package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ExamDetailActivity;
import com.edusunsoft.erp.orataro.model.FeeTransactionDetailModel;

import java.util.ArrayList;

/**
 * Created by admin on 12-06-2017.
 */

public class FeesReceiptTransactionDetailListAdapter extends RecyclerView.Adapter<FeesReceiptTransactionDetailListAdapter.MyViewHolder> {

    public ArrayList<FeeTransactionDetailModel> transactiondetaillist;
    public Context mcontext;
    public int selectedPosition;

    int getSelectedPosition;

    public FeesReceiptTransactionDetailListAdapter(Context mContext, ArrayList<FeeTransactionDetailModel> feesTransactionDetailList) {

        this.mcontext = mContext;
        this.transactiondetaillist = feesTransactionDetailList;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtstatus, txt_rect_no, txt_date, txt_amount;
        ImageView img_fee_receipt_detail;

        public MyViewHolder(View view) {

            super(view);
            getSelectedPosition = getLayoutPosition();

            txtstatus = (TextView) view.findViewById(R.id.txtstatus);
            txt_rect_no = (TextView) view.findViewById(R.id.txt_rect_no);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            txt_amount = (TextView) view.findViewById(R.id.txt_amount);
            img_fee_receipt_detail = (ImageView) view.findViewById(R.id.img_fee_receipt_detail);


            img_fee_receipt_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selectedPosition = getLayoutPosition();
                    Intent examdetailIntent = new Intent(mcontext, ExamDetailActivity.class);
                    examdetailIntent.putExtra("FromString", "FeeReceipt");
                    examdetailIntent.putExtra("feetransactiondetailmodel", transactiondetaillist.get(selectedPosition));
                    mcontext.startActivity(examdetailIntent);

                }

            });


        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fee_receipt_transaction_detail_list_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        FeeTransactionDetailModel feereceiptdetailModel = transactiondetaillist.get(position);

        if (feereceiptdetailModel.getTransactionStatus().equalsIgnoreCase("ERROR")) {
            holder.txtstatus.setTextColor(mcontext.getResources().getColor(R.color.red));
            holder.txt_rect_no.setTextColor(mcontext.getResources().getColor(R.color.red));
            holder.txt_date.setTextColor(mcontext.getResources().getColor(R.color.red));
            holder.txt_amount.setTextColor(mcontext.getResources().getColor(R.color.red));
            holder.img_fee_receipt_detail.setColorFilter(ContextCompat.getColor(mcontext, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        if (feereceiptdetailModel.getReceiptNo().equalsIgnoreCase(null) || feereceiptdetailModel.getReceiptNo().equalsIgnoreCase("null")) {
            holder.txt_rect_no.setText("");
        } else {
            holder.txt_rect_no.setText(feereceiptdetailModel.getReceiptNo());
        }
        holder.txtstatus.setText(feereceiptdetailModel.getTransactionStatus());
        holder.txt_date.setText(feereceiptdetailModel.getStrTransactionDate());
        holder.txt_amount.setText(String.valueOf(feereceiptdetailModel.getAmount()));

        /*END*/
    }

    @Override
    public int getItemCount() {

        return transactiondetaillist.size();

    }

}
//public class FeesReceiptTransactionDetailListAdapter extends BaseAdapter {
//
//    private Context mContext;
//    private ArrayList<FeeTransactionDetailModel> transactiondetaillist;
//
//    public FeesReceiptTransactionDetailListAdapter(Context mContext, ArrayList<FeeTransactionDetailModel> feesTransactionDetailList) {
//
//        this.mContext = mContext;
//        this.transactiondetaillist = feesTransactionDetailList;
//    }
//
//
//    @Override
//    public int getCount() {
//        return transactiondetaillist.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(final int i, View view, ViewGroup viewGroup) {
//
//        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = layoutInfalater.inflate(R.layout.fee_receipt_transaction_detail_list_row, viewGroup, false);
//
//        TextView txtstatus = (TextView) v.findViewById(R.id.txtstatus);
//        TextView txt_rect_no = (TextView) v.findViewById(R.id.txt_rect_no);
//        TextView txt_date = (TextView) v.findViewById(R.id.txt_date);
//        TextView txt_amount = (TextView) v.findViewById(R.id.txt_amount);
//        txtstatus.setText(transactiondetaillist.get(i).getTransactionStatus());
//        txt_rect_no.setText(transactiondetaillist.get(i).getReceiptNo());
//        txt_date.setText(transactiondetaillist.get(i).getStrTransactionDate());
//        txt_amount.setText(String.valueOf(transactiondetaillist.get(i).getAmount()));
//
////        txtfees.setText(transactiondetaillist.get(i).getDisplayLabel());
////        txtammount.setText(transactiondetaillist.get(i).getAmountToBePaid() + "");
////        txtdue.setText(transactiondetaillist.get(i).getPaidAmounts() + "");
////        txtaction.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Intent intent = new Intent(mContext, FragmentViewActivity.class);
////                intent.putExtra("model", transactiondetaillist.get(i));
////                intent.putExtra("isFrom", ServiceResource.RECIPTDETAIL);
////                mContext.startActivity(intent);
////            }
////
////        });
//
//        return v;
//    }
