package com.edusunsoft.erp.orataro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class uploadHomeworkResModel implements Serializable {

    @SerializedName("data")
    @Expose
    private ArrayList<Data> data = null;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable {
        @SerializedName("StudentReplayID")
        @Expose
        private String StudentReplayID;
        @SerializedName("MemberID")
        @Expose
        private String MemberID;
        @SerializedName("ImageURL")
        @Expose
        private String ImageURL;
        @SerializedName("TextContaine")
        @Expose
        private String TextContaine;
        @SerializedName("IsApprove")
        @Expose
        private boolean IsApprove;
        @SerializedName("Note")
        @Expose
        private String Note;
        @SerializedName("DisplayName")
        @Expose
        private String DisplayName;
        @SerializedName("CreatedOn")
        @Expose
        private String CreatedOn;
        @SerializedName("FullName")
        @Expose
        private String FullName;
        @SerializedName("RollNo")
        @Expose
        private String RollNo;
        @SerializedName("FileType")
        @Expose
        private String FileType;


        public String getFileType() {
            return FileType;
        }

        public void setFileType(String fileType) {
            FileType = fileType;
        }

        public String getStudentReplayID() {
            return StudentReplayID;
        }

        public void setStudentReplayID(String studentReplayID) {
            StudentReplayID = studentReplayID;
        }

        public String getMemberID() {
            return MemberID;
        }

        public void setMemberID(String memberID) {
            MemberID = memberID;
        }

        public String getImageURL() {
            return ImageURL;
        }

        public void setImageURL(String imageURL) {
            ImageURL = imageURL;
        }

        public String getTextContaine() {
            return TextContaine;
        }

        public void setTextContaine(String textContaine) {
            TextContaine = textContaine;
        }

        public boolean getIsApprove() {
            return IsApprove;
        }

        public void setIsApprove(boolean isApprove) {
            IsApprove = isApprove;
        }

        public String getNote() {
            return Note;
        }

        public void setNote(String note) {
            Note = note;
        }

        public String getDisplayName() {
            return DisplayName;
        }

        public void setDisplayName(String displayName) {
            DisplayName = displayName;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String createdOn) {
            CreatedOn = createdOn;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String fullName) {
            FullName = fullName;
        }

        public String getRollNo() {
            return RollNo;
        }

        public void setRollNo(String rollNo) {
            RollNo = rollNo;
        }


        @Override
        public String toString() {
            return "Data{" +
                    "StudentReplayID='" + StudentReplayID + '\'' +
                    ", MemberID='" + MemberID + '\'' +
                    ", ImageURL='" + ImageURL + '\'' +
                    ", TextContaine='" + TextContaine + '\'' +
                    ", IsApprove=" + IsApprove +
                    ", Note='" + Note + '\'' +
                    ", DisplayName='" + DisplayName + '\'' +
                    ", CreatedOn='" + CreatedOn + '\'' +
                    ", FullName='" + FullName + '\'' +
                    ", RollNo='" + RollNo + '\'' +
                    ", FileType='" + FileType + '\'' +
                    '}';
        }

    }

}
