package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Holiday")
public class HolidaysModel {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "Year")
    @SerializedName("Year")
    public String Year;

    @ColumnInfo(name = "NoOfDay")
    @SerializedName("NoOfDay")
    public int NoOfDay;

    @ColumnInfo(name = "HolidayDetails")
    @SerializedName("HolidayDetails")
    public String HolidayDetails;

    @ColumnInfo(name = "HolidayTitle")
    @SerializedName("HolidayTitle")
    public String HolidayTitle;

    @ColumnInfo(name = "EndDate")
    @SerializedName("EndDate")
    public String EndDate;

    @ColumnInfo(name = "StartDate")
    @SerializedName("StartDate")
    public String StartDate;

    @ColumnInfo(name = "HolidayID")
    @SerializedName("HolidayID")
    public String HolidayID;

    @ColumnInfo(name = "Hmonth")
    @SerializedName("Hmonth")
    public String Hmonth;

    @ColumnInfo(name = "Hday")
    @SerializedName("Hday")
    public String Hday;

    public int getNoOfDay() {
        return NoOfDay;
    }

    public void setNoOfDay(int noOfDay) {
        NoOfDay = noOfDay;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getHday() {
        return Hday;
    }

    public void setHday(String hday) {
        Hday = hday;
    }

    public String getHmonth() {
        return Hmonth;
    }

    public void setHmonth(String hmonth) {
        Hmonth = hmonth;
    }

    public String getHolidayID() {
        return HolidayID;
    }

    public void setHolidayID(String holidayID) {
        HolidayID = holidayID;
    }

    public String getDateOfHoliday() {
        return StartDate;
    }

    public void setDateOfHoliday(String dateOfHoliday) {
        StartDate = dateOfHoliday;
    }

    public String getEndDateOfHoliday() {
        return EndDate;
    }

    public void setEndDateOfHoliday(String dateOfHoliday) {
        EndDate = dateOfHoliday;
    }

    public String getHolidayTitle() {
        return HolidayTitle;
    }

    public void setHolidayTitle(String holidayTitle) {
        HolidayTitle = holidayTitle;
    }

    public String getHolidayDetails() {
        return HolidayDetails;
    }

    public void setHolidayDetails(String holidayDetails) {
        HolidayDetails = holidayDetails;
    }

    @Override
    public String toString() {
        return "HolidaysModel{" +
                "id=" + id +
                ", Year='" + Year + '\'' +
                ", NoOfDay=" + NoOfDay +
                ", HolidayDetails='" + HolidayDetails + '\'' +
                ", HolidayTitle='" + HolidayTitle + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", StartDate='" + StartDate + '\'' +
                ", HolidayID='" + HolidayID + '\'' +
                ", Hmonth='" + Hmonth + '\'' +
                ", Hday='" + Hday + '\'' +
                '}';
    }
}
