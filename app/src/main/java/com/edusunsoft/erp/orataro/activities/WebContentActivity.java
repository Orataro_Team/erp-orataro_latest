package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edusunsoft.erp.orataro.R;


public class WebContentActivity extends Activity {
    private static final String TAG = "WebContentActivity";
    SharedPreferences sp;
    static Context mContext;
    public static final String KEY_ATOM2REQUEST = "Atom2Request";
    String Atom2Request;
    Intent intent;
    boolean loadingFinished = true;
    boolean redirect = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webviewrecharge);
        //  Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        mContext = this;
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            Atom2Request = extras.getString(KEY_ATOM2REQUEST);
        Log.d("ATOM2Request webview", Atom2Request);
        WebView webView = (WebView) findViewById(R.id.wv_activity);
      //  String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";

        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
    //   webView.getSettings().setDomStorageEnabled(true);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        //webView.getSettings().setUserAgentString(newUA);
        webView.loadUrl(Atom2Request);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
//            if(urlNewString.contains("CCFundTransfer")){
//                urlNewString = urlNewString.replace("CCFundTransfer","NBFundTransfer");
//
//            }
//            if(urlNewString.contains("DBFundTransfer")){
//                urlNewString = urlNewString.replace("DBFundTransfer","NBFundTransfer");
//            }
            view.loadUrl(urlNewString);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            //SHOW LOADING IF IT ISNT ALREADY VISIBLE
            Log.w(TAG, "Loading");
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                //HIDE LOADING IT HAS FINISHED
                Log.w(TAG, "Finish Loading");
            } else {
                redirect = false;
            }

        }
    }
    public class WebAppInterface {
        Context mContext;
        WebAppInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void onResponse(String reponseText) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("Result", reponseText);
            setResult(RESULT_OK, returnIntent);
            finish();

        }
    }

}