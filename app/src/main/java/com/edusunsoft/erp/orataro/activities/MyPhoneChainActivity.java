package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.UpdateListner;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.MyPhoneChainAdapter;
import com.edusunsoft.erp.orataro.model.MyPhoneChainModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyPhoneChainActivity extends Activity implements OnClickListener, ResponseWebServices, UpdateListner {

    public String Message = "";
    Context mContext;
    private LinearLayout ll_add_new;
    Dialog mAddNewDialog;
    MyPhoneChainAdapter mPhoneChainAdapter;
    ListView lst_my_ph_chain;
    ArrayList<MyPhoneChainModel> myPhoneChainModels;
    MyPhoneChainModel phoneChainModel;
    private ImageView img_back, imgRightHeader;
    EditText edtTeacherName;
    TextView header_text;
    TextView txt_noContact;
    String spnTypeStr, spnPriortyStr, contactNameStr, contactDetailStr, contactNumberStr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_phone_chain);

        mContext = this;
        header_text = (TextView) findViewById(R.id.header_text);
        try {
            header_text.setText(mContext.getResources().getString(R.string.myphonechain) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        img_back = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);
        lst_my_ph_chain = (ListView) findViewById(R.id.lst_my_ph_chain);
        txt_noContact = (TextView) findViewById(R.id.txt_nocontact);
        img_back.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);
        img_back.setOnClickListener(this);
        if (Utility.isNetworkAvailable(mContext)) {
            myphonechain();
        } else {
            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
        }


        ll_add_new.setVisibility(View.VISIBLE);

        ll_add_new.setOnClickListener(this);

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                mPhoneChainAdapter.filter(text);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_add_new:

                mAddNewDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_add_phone_chain,true);

//                mAddNewDialog = new Dialog(mContext);
//                mAddNewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                mAddNewDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                mAddNewDialog.getWindow().setBackgroundDrawableResource(
//                        android.R.color.transparent);
//                mAddNewDialog.setContentView(R.layout.dialog_add_phone_chain);
//                mAddNewDialog.setCancelable(true);
//                mAddNewDialog.show();

                final EditText edt_name = (EditText) mAddNewDialog.findViewById(R.id.edt_name);
                final EditText edt_relation = (EditText) mAddNewDialog.findViewById(R.id.edt_relation);
                final EditText edt_phone = (EditText) mAddNewDialog.findViewById(R.id.edt_phone_no);
                LinearLayout ll_add_appriciation = (LinearLayout) mAddNewDialog.findViewById(R.id.ll_add_appriciation);
                ImageView img_close = (ImageView) mAddNewDialog.findViewById(R.id.img_close);
                Spinner spn_type = (Spinner) mAddNewDialog.findViewById(R.id.spn_type);
                Spinner spn_priorty = (Spinner) mAddNewDialog.findViewById(R.id.spn_priority);

                List<String> type = new ArrayList<String>();
                type.add(mContext.getResources().getString(R.string.PleaseSelectContactType));
                type.add("Mobile");
                type.add("Home");
                type.add("Office");

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, type);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spn_type.setAdapter(adapter);
                spn_type.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        if (position == 0) {
                            spnTypeStr = "";
                        } else {
                            spnTypeStr = parent.getItemAtPosition(position).toString();
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                List<String> priorty = new ArrayList<String>();
                priorty.add(mContext.getResources().getString(R.string.PleaseSelectpriority));
                priorty.add("Important");
                priorty.add("Normal");
                priorty.add("Not Important");
                ArrayAdapter<String> adapterPriorty = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, priorty);
                adapterPriorty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spn_priorty.setAdapter(adapterPriorty);
                spn_priorty.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            spnPriortyStr = "";
                        } else {
                            spnPriortyStr = parent.getItemAtPosition(position).toString();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                img_close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mAddNewDialog.dismiss();
                    }
                });

                ll_add_appriciation.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        contactNameStr = edt_name.getText().toString();
                        contactDetailStr = edt_relation.getText().toString();
                        contactNumberStr = edt_phone.getText().toString();
                        if (contactNameStr == null || contactNameStr.equalsIgnoreCase("")) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseEnterName), mContext);
                        } else if (contactDetailStr == null || contactDetailStr.equalsIgnoreCase("")) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseEnterRelation), mContext);
                        } else if (contactNumberStr == null || contactNumberStr.equalsIgnoreCase("")) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseEnterRelation), mContext);
                        } else if (contactNumberStr.length() < 9) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseEnterValidNum), mContext);
                        } else if (spnTypeStr == null || spnTypeStr.equalsIgnoreCase("")) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseSelectContactType), mContext);
                        } else if (spnPriortyStr == null || spnPriortyStr.equalsIgnoreCase("")) {
                            Utility.showToast(mContext.getResources().getString(R.string.PleaseSelectPriority), mContext);
                        } else {
                            if (Utility.isNetworkAvailable(mContext)) {
                                addPhone();
                            } else {
                                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
                            }
                        }
                    }
                });
                break;
            case R.id.img_home:
                finish();
                break;
            default:
                break;
        }

    }

    public void myphonechain() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PHONEBOOK_METHODNAME, ServiceResource.PHONEBOOK_URL);
    }

    public void addPhone() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTNAME, contactNameStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTDETAIL, contactDetailStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTNO, contactNumberStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTTYPETERM, spnTypeStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_PRIORITYLEVEL, spnPriortyStr));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PHONEBOOK_ADD_METHODNAME, ServiceResource.PHONEBOOK_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.PHONEBOOK_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.phonebookModels = new ArrayList<MyPhoneChainModel>();
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    MyPhoneChainModel phonechainmodel = new MyPhoneChainModel();
                    phonechainmodel.setParent_name(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNAME));
                    phonechainmodel.setParent_number(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNO));
                    phonechainmodel.setParent_relation(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTDETAIL));
                    phonechainmodel.setPhoneBookID(innerObj.getString(ServiceResource.PHONEBOOK_PHONEBOOKID));
                    phonechainmodel.setPriorityLevelTerm(innerObj.getString(ServiceResource.PHONEBOOK_PRIORITYLEVELTERM));
                    phonechainmodel.setContactTypeTerm(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTTYPETERM));
                    Global.phonebookModels.add(phonechainmodel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (Global.phonebookModels != null && Global.phonebookModels.size() > 0) {
                mPhoneChainAdapter = new MyPhoneChainAdapter(mContext, Global.phonebookModels, this);
                lst_my_ph_chain.setAdapter(mPhoneChainAdapter);
                txt_noContact.setVisibility(View.INVISIBLE);
                lst_my_ph_chain.setVisibility(View.VISIBLE);
            } else {
                txt_noContact.setVisibility(View.VISIBLE);
                lst_my_ph_chain.setVisibility(View.INVISIBLE);
            }
        }
        if (ServiceResource.PHONEBOOK_ADD_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            boolean isValid;
            try {
                Global.phonebookModels = new ArrayList<MyPhoneChainModel>();
                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    Message = innerObj.getString(ServiceResource.MESSAGE);
                    if (Message.equalsIgnoreCase("Created")) {
                        isValid = true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mAddNewDialog.dismiss();
            Utility.showToast(Message, mContext);
            myphonechain();
        }
    }

    @Override
    public void Update() {
        myphonechain();

    }

}




