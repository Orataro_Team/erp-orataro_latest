package com.edusunsoft.erp.orataro.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {GenerallWallData.class, DynamicWallSettingModel.class, DynamicWallListModel.class, DashboardMenuModel.class, HomeWorkListModel.class, ClassWorkListModel.class, StdDivSubModel.class, CircularListModel.class, NoticeListModel.class, PTCommMemberModel.class, PtCommunicationModel.class, TimeTableModel.class, HolidaysModel.class, CmsPageListModel.class, BlogListModel.class, GroupListModel.class, ProjectListModel.class, PhotoVideoFileModel.class}, version = 4, exportSchema = false)
public abstract class ERPOrataroDatabase extends RoomDatabase {
    private static ERPOrataroDatabase INSTANCE;

    public abstract GenerallWallDataDao generawallDataDao();

    public abstract DynamicWallSettingDataDao dynamicWallSettingDataDao();

    public abstract DynamicWallListDataDao dynamicWallListDataDao();

    public abstract DashboardMenuDataDao dashboardMenuDataDao();

    public abstract HomeworkListDataDao homeworkListDataDao();

    public abstract ClassworkListDataDao classworkListDataDao();

    public abstract CircularListDataDao circularListDataDao();

    public abstract NoticeListDataDao noticeListDataDao();

    public abstract StudentMemberListDataDao studentMemberListDataDao();

    public abstract PTCommunicationListDataDao ptCommunicationListDataDao();

    public abstract TimeTableDataDao timeTableDataDao();

    public abstract HolidayDataDao holidayDataDao();

    public abstract CMSPageListDataDao cmsPageListDataDao();

    public abstract BlogListDataDao blogListDataDao();

    public abstract GroupListDataDao groupListDataDao();

    public abstract ProjectListDataDao projectListDataDao();

    public abstract PhotoVideoFileDataDao photoVideoFileDataDao();

    public abstract StdDivSubDataDao stdDivSubDataDao();

    public static ERPOrataroDatabase getERPOrataroDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ERPOrataroDatabase.class, "ERPOrataroDatabase")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;

    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
