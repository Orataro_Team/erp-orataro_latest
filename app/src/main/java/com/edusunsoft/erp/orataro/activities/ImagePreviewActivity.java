package com.edusunsoft.erp.orataro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ImagePreviewActivity extends Activity {
    private static final int PIC_CROP = 3000;
    public static int TAKE_IMAGE = 111;
    Uri mCapturedImageURI;
    private String fromintent;
    private Bitmap bitmap;
    private ImageView imgPreview;
    private ImageView imgUse;
    private ImageView imgCropImage;

    private ImageView imgCancel;
    private String capturedImageFilePath = null;
    private ImageView imgRotateImage;

    private int img_rotation = 0;
    private Bitmap bitmap_rotated = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);


        imgPreview = (ImageView) findViewById(R.id.imgPreview);

        imgUse = (ImageView) findViewById(R.id.imgUse);

        imgCancel = (ImageView) findViewById(R.id.imgCancel);

        imgRotateImage = (ImageView) findViewById(R.id.imgRotateImage);
        imgCropImage = (ImageView) findViewById(R.id.imgCropImage);
        fromintent = getIntent().getStringExtra("FromIntent");


        if (fromintent != null && fromintent.equals("true")) {
            try {
                //			    String fileName = "temp.jpg";
                //			    ContentValues values = new ContentValues();
                //			    values.put(MediaStore.Images.Media.TITLE, fileName);
                mCapturedImageURI = Utility.getOutputMediaFileUri(1, ImagePreviewActivity.this);
                Intent intent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mCapturedImageURI);
                fromintent = null;
                startActivityForResult(intent, TAKE_IMAGE);

            } catch (Exception e) {

                Log.e("", "", e);

            }
        }


        imgUse.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (capturedImageFilePath != null) {
//					if(bitmap_rotated!=null)
//					{
//						Intent intent =new Intent();
//						byte[] byteArray =null;
//						ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//						FileOutputStream out = null;
//						try {
//							out = new FileOutputStream(capturedImageFilePath);
//							bitmap_rotated.compress(Bitmap.CompressFormat.JPEG, 100, out);
//						} catch (FileNotFoundException e) {
//							e.printStackTrace();
//						}
//						bitmap_rotated.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//						byteArray = bytes.toByteArray();//convertImageToByteArra(fileUri.getPath());
//						intent.putExtra("imageData_byte",byteArray ); //send image byte to  main activity
//						intent.putExtra("imageData_uri",capturedImageFilePath);//send image Uri to  main activity
//						//						intent.putExtra("imageData_bitmap",new BitmapUtil(CameraActivity.this).compressImage(uriTarget.toString()));//send image Bitmap to  main activity
//						setResult(RESULT_OK, intent);
//						//						bitmap.recycle();
//						finish();
//					}
//					else
//					{
                    Intent intent = new Intent();
                    //						intent.putExtra("imageData_uri", capturedImageFilePath);
                    //						setResult(RESULT_OK, intent);
                    //						bitmap.recycle();
                    //						finish();

                    byte[] byteArray = null;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    byteArray = bytes.toByteArray();//convertImageToByteArra(fileUri.getPath());
                    intent.putExtra("imageData_byte", byteArray); //send image byte to  main activity
                    intent.putExtra("imageData_uri", capturedImageFilePath);//send image Uri to  main activity
                    //						intent.putExtra("imageData_bitmap",new BitmapUtil(CameraActivity.this).compressImage(uriTarget.toString()));//send image Bitmap to  main activity
                    setResult(RESULT_OK, intent);
                    //						bitmap.recycle();
                    finish();


//					}


                }
            }
        });


        imgRotateImage.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (capturedImageFilePath != null) {


                    Matrix matrix = new Matrix();

                    img_rotation += 90;

                    if (img_rotation > 360) {
                        img_rotation = 90;
                    }

                    matrix.postRotate(img_rotation);

                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);

                    bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);


                    imgPreview.setImageBitmap(bitmap);

                    byte[] byteArray = null;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(capturedImageFilePath);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
//					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                }
            }
        });


        imgCropImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                performCrop(Uri.fromFile(new File(capturedImageFilePath)));

            }

        });

        imgCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });


    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub

        //		capturedImageFilePath=null;
        super.onResume();
    }


    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if ((requestCode == TAKE_IMAGE) && (resultCode == RESULT_OK)) {
            //		        mode = MODE_VIEWER;
            //		        String[] projection = { MediaStore.Images.Media.DATA };
            //		        Cursor cursor = managedQuery(mCapturedImageURI, projection, null,
            //		                null, null);
            //		        int column_index_data = cursor
            //		                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            //		        cursor.moveToFirst();

            //THIS IS WHAT YOU WANT!

            //		        bitmap = BitmapFactory.decodeFile(capturedImageFilePath);
            //
            capturedImageFilePath = null;

            try {

                capturedImageFilePath = mCapturedImageURI.getPath();// cursor.getString(column_index_data);
                Log.e(getResources().getString(R.string.app_name), "Path : " + capturedImageFilePath);
                capturedImageFilePath = Utility.compressImage(capturedImageFilePath);
                bitmap = BitmapFactory.decodeFile(capturedImageFilePath);
                //			        Log.e(getResources().getString(R.string.app_name), "Size : " + (bitmap.getByteCount()/1024));
                Log.e(getResources().getString(R.string.app_name), "W X H : " + bitmap.getWidth() + " X " + bitmap.getHeight());

                if (bitmap != null) {

                    imgPreview.setImageBitmap(bitmap);

                }

            } catch (Exception e) {
                // TODO: handle exception
                //				mCapturedImageURI = Utility.getOutputMediaFileUri(1,PreviewImageActivity.this);
                //				Intent intent1 = new Intent(
                //						MediaStore.ACTION_IMAGE_CAPTURE);
                //				intent.putExtra(MediaStore.EXTRA_OUTPUT,
                //						mCapturedImageURI);
                //				fromintent=null;
                //				startActivityForResult(intent1, TAKE_IMAGE);
                Utility.getUserModelData(ImagePreviewActivity.this);
                Utility.toast(ImagePreviewActivity.this, e.getMessage().toString() + "Sorry There is some error with Camera.\nPlease Try Again.");
//                Toast.makeText(ImagePreviewActivity.this, e.getMessage().toString()+"Sorry There is some error with Camera.\nPlease Try Again.", 1).show();
                finish();
            }


        } else if ((requestCode == PIC_CROP)
                && (resultCode == RESULT_OK)) {
            if (requestCode == PIC_CROP) {
                if (intent != null) {
                    try {
                        // et the returned data
                        Bundle extras = intent.getExtras();
                        // get the cropped bitmap
                        bitmap = extras.getParcelable("data");


                    } catch (Exception e) {
                        Uri selectedImage = intent.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};

                        try {
                            bitmap = BitmapFactory.decodeStream(getContentResolver()
                                    .openInputStream(selectedImage));
                        } catch (FileNotFoundException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                    if (bitmap != null) {
                        imgPreview.setImageBitmap(bitmap);
                    }
                    byte[] byteArray = null;
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(capturedImageFilePath);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

//			            imgView.setImageBitmap(selectedBitmap);
                }
            }
        } else {
            finish();
        }

    }


    private void performCrop(Uri picUri) {

        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 128);
            cropIntent.putExtra("outputY", 128);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Utility.toast(this, errorMessage);
//            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//            toast.show();
        }
    }

}
