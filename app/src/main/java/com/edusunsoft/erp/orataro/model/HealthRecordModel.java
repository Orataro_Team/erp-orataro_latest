package com.edusunsoft.erp.orataro.model;

public class HealthRecordModel {

    private String MemberHeathID, ClientID, InstituteID, MemberID, BloodGroupTerm, Height, Weight, AllergiesIfAny, SpecificDiseasSuffered;
    private String OperationUndergone, AnyOtheDiseasesForWhichTheChildIsOnRegular, Medicatoin, ColourFearIfAny, DateOfLastUpdate;
    private String FamilyDoctorName, ContactNo, PatientID, CreateOn, CreateBy, UpdateOn, UpdateBy;

    public String getMemberHeathID() {
        return MemberHeathID;
    }

    public void setMemberHeathID(String memberHeathID) {
        MemberHeathID = memberHeathID;
    }

    public String getClientID() {
        return ClientID;
    }

    public void setClientID(String clientID) {
        ClientID = clientID;
    }

    public String getInstituteID() {
        return InstituteID;
    }

    public void setInstituteID(String instituteID) {
        InstituteID = instituteID;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getBloodGroupTerm() {
        return BloodGroupTerm;
    }

    public void setBloodGroupTerm(String bloodGroupTerm) {
        BloodGroupTerm = bloodGroupTerm;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getAllergiesIfAny() {
        return AllergiesIfAny;
    }

    public void setAllergiesIfAny(String allergiesIfAny) {
        AllergiesIfAny = allergiesIfAny;
    }

    public String getSpecificDiseasSuffered() {
        return SpecificDiseasSuffered;
    }

    public void setSpecificDiseasSuffered(String specificDiseasSuffered) {
        SpecificDiseasSuffered = specificDiseasSuffered;
    }

    public String getOperationUndergone() {
        return OperationUndergone;
    }

    public void setOperationUndergone(String operationUndergone) {
        OperationUndergone = operationUndergone;
    }

    public String getAnyOtheDiseasesForWhichTheChildIsOnRegular() {
        return AnyOtheDiseasesForWhichTheChildIsOnRegular;
    }

    public void setAnyOtheDiseasesForWhichTheChildIsOnRegular(
            String anyOtheDiseasesForWhichTheChildIsOnRegular) {
        AnyOtheDiseasesForWhichTheChildIsOnRegular = anyOtheDiseasesForWhichTheChildIsOnRegular;
    }

    public String getMedicatoin() {
        return Medicatoin;
    }

    public void setMedicatoin(String medicatoin) {
        Medicatoin = medicatoin;
    }

    public String getColourFearIfAny() {
        return ColourFearIfAny;
    }

    public void setColourFearIfAny(String colourFearIfAny) {
        ColourFearIfAny = colourFearIfAny;
    }

    public String getDateOfLastUpdate() {
        return DateOfLastUpdate;
    }

    public void setDateOfLastUpdate(String dateOfLastUpdate) {
        DateOfLastUpdate = dateOfLastUpdate;
    }

    public String getFamilyDoctorName() {
        return FamilyDoctorName;
    }

    public void setFamilyDoctorName(String familyDoctorName) {
        FamilyDoctorName = familyDoctorName;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getPatientID() {
        return PatientID;
    }

    public void setPatientID(String patientID) {
        PatientID = patientID;
    }

    public String getCreateOn() {
        return CreateOn;
    }

    public void setCreateOn(String createOn) {
        CreateOn = createOn;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String createBy) {
        CreateBy = createBy;
    }

    public String getUpdateOn() {
        return UpdateOn;
    }

    public void setUpdateOn(String updateOn) {
        UpdateOn = updateOn;
    }

    public String getUpdateBy() {
        return UpdateBy;
    }

    public void setUpdateBy(String updateBy) {
        UpdateBy = updateBy;
    }

}
