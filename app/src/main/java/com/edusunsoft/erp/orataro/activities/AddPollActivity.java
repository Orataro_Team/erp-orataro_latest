package com.edusunsoft.erp.orataro.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.PollOptionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddPollActivity extends AppCompatActivity implements OnClickListener, ResponseWebServices {

    private Context mContext;
    private LinearLayout btnOptions;
    private QuickAction quickAction;
    private ImageView img_back, iv_save;
    private Calendar c, c1;
    private int myear, myear1;
    private int month, month1;
    private int day, day1;
    static final int DATE_DIALOG_ID = 111;
    static final int TO_DATE_DIALOG_ID = 121;
    private EditText edt_poll_name, edt_introduction_text;
    private TextView tv_end_date, tv_start_date;
    private String start_date, end_date;
    private LinearLayout lladdedittext, llanswerlist;
    private String answerListStr = "";
    private ImageView imgRemove;
    private LinearLayout ll_chknotify, llchkpresantage, llchkmultichoice;
    private CheckBox chknotify, chkpresantage, chkmultichoice;
    private boolean isEdit;
    private PollModel model;
    private ArrayList<PollOptionModel> optionList;
    private TextView header_text;
    LinearLayout ll_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_poll);
        mContext = AddPollActivity.this;

        isEdit = getIntent().getBooleanExtra("isEdit", false);
        model = (PollModel) getIntent().getSerializableExtra("model");
        edt_poll_name = (EditText) findViewById(R.id.edt_poll_name);
        edt_introduction_text = (EditText) findViewById(R.id.edt_introduction_text);
        header_text = (TextView) findViewById(R.id.header_text);
        ll_chknotify = (LinearLayout) findViewById(R.id.ll_chknotify);
        llchkpresantage = (LinearLayout) findViewById(R.id.llchkpresantage);
        llchkmultichoice = (LinearLayout) findViewById(R.id.llchkmultichoice);
        chknotify = (CheckBox) findViewById(R.id.chknotify);
        chkpresantage = (CheckBox) findViewById(R.id.chkpresantage);
        chkmultichoice = (CheckBox) findViewById(R.id.chkmultichoice);
        tv_start_date = (TextView) findViewById(R.id.tv_start_date);
        tv_end_date = (TextView) findViewById(R.id.tv_end_date);
        lladdedittext = (LinearLayout) findViewById(R.id.lladdedittext);
        llanswerlist = (LinearLayout) findViewById(R.id.llanswerlist);
        imgRemove = (ImageView) findViewById(R.id.imgRemove);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);

        try {
            if (isEdit) {
                header_text.setText(getResources().getString(R.string.editpoll) + " (" + Utility.GetFirstName(mContext) + ")");
                editPoll();
            } else {
                header_text.setText(getResources().getString(R.string.AddPoll) + " (" + Utility.GetFirstName(mContext) + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        c1 = Calendar.getInstance();
        myear1 = c.get(Calendar.YEAR);
        month1 = c.get(Calendar.MONTH);
        day1 = c.get(Calendar.DAY_OF_MONTH);

        tv_start_date.setText((day < 10 ? "0" + day : day)
                + "-"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "-"
                + myear);

        tv_end_date.setText(((day1 + 1) < 10 ? "0" + day1 : day1)
                + "-"
                + ((month1 + 1) < 10 ? "0" + (month1 + 1)
                : (month1 + 1))
                + "-" + myear1);

        img_back = (ImageView) findViewById(R.id.img_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);

        img_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_save.setOnClickListener(this);

        tv_start_date.setOnClickListener(this);
        tv_end_date.setOnClickListener(this);
        lladdedittext.setOnClickListener(this);

        ll_chknotify.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chknotify.isChecked()) {
                    chknotify.setChecked(false);
                } else {
                    chknotify.setChecked(true);
                }
            }
        });

        llchkpresantage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chkpresantage.isChecked()) {
                    chkpresantage.setChecked(false);
                } else {
                    chkpresantage.setChecked(true);
                }
            }
        });

        llchkmultichoice.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chkmultichoice.isChecked()) {
                    chkmultichoice.setChecked(false);
                } else {
                    chkmultichoice.setChecked(true);
                }
            }
        });


        if (!isEdit) {
            addEditTextLayout(false);
        }


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog datePickerDialog;

        switch (id) {
            case DATE_DIALOG_ID:
                datePickerDialog = new DatePickerDialog(this, datePickerListener,
                        myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 2678400000l);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                //datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                return datePickerDialog;

            case TO_DATE_DIALOG_ID:
                // set date picker as current date
                datePickerDialog = new DatePickerDialog(this, datePickerListener1,
                        myear1, month1, day1);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            tv_start_date.setText((day < 10 ? "0" + day : day)
                    + "-"
                    + ((month + 1) < 10 ? "0" + (month + 1)
                    : (month + 1))
                    + "-"
                    + myear);

        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener1 = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            myear1 = selectedYear;
            month1 = selectedMonth;
            day1 = selectedDay;

            tv_end_date.setText((day1 < 10 ? "0" + day1 : day1)
                    + "-"
                    + ((month1 + 1) < 10 ? "0" + (month1 + 1)
                    : (month1 + 1))
                    + "-" + myear1);

        }
    };

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;

            case R.id.iv_save:
                break;

            case R.id.ll_save:

                if (!edt_poll_name.getText().toString().equalsIgnoreCase("")) {

                    AddPoll(true);

                }
                
                break;

            case R.id.tv_start_date:

                showDialog(DATE_DIALOG_ID);
                tv_start_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-"
                        + myear);

                break;

            case R.id.tv_end_date:

                showDialog(TO_DATE_DIALOG_ID);
                tv_end_date.setText((day1 < 10 ? "0" + day1 : day1)
                        + "-"
                        + ((month1 + 1) < 10 ? "0" + (month1 + 1)
                        : (month1 + 1))
                        + "-" + myear1);

                break;

            case R.id.lladdedittext:

                addEditTextLayout(true);
                break;

            default:
                break;
        }
    }

    private void addEditTextLayout(boolean isRemove) {
        LayoutInflater layoutInfalater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInfalater.inflate(R.layout.edittext, null);

        ImageView imgRemove = (ImageView) v.findViewById(R.id.imgRemove);
        if (isRemove) {
            imgRemove.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    llanswerlist.removeView((View) v.getParent());
                }
            });
        } else {
            imgRemove.setVisibility(View.INVISIBLE);
        }

        llanswerlist.addView(v);

    }

    public static boolean isDateAfter(String startDate, String endDate) {
        try {
            String myFormatString = "MM/dd/yyyy"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            if (date1.after(startingDate))
                return true;
            else
                return false;
        } catch (Exception e) {

            return false;
        }
    }

    public void AddPoll(boolean isCallWS) {

        int count = llanswerlist.getChildCount();

        for (int i = 0; i < count; i++) {
            View v = null;
            v = llanswerlist.getChildAt(i);
            if (v instanceof LinearLayout) {
                int childCount = ((LinearLayout) v).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View ChildView = ((LinearLayout) v).getChildAt(j);
                    if (ChildView instanceof EditText) {
                        EditText edtAnswer = (EditText) ChildView;
                        if (!edtAnswer.getText().toString().equalsIgnoreCase("")) {
                            answerListStr = answerListStr + edtAnswer.getText().toString() + "#";

                        } else {

                            edtAnswer.setError(getResources().getString(R.string.errorpollanswer));
                            isCallWS = false;
                        }
                    }
                }
            }


        }
        if (isCallWS) {

            AddPollWebService();

        }

        Log.v("answer", answerListStr);

    }

    public void AddPollWebService() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.POLLOPTIONBYHASE, answerListStr));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID, model.getPollID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.DETAILS, edt_introduction_text.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.TILTE, edt_poll_name.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ISMULTICHOICE, chkmultichoice.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISNOTIFYALL, chknotify.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.ISPERCENTEGE, chkpresantage.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.STARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SAVEUPDATEPOLLS_METHODNAME, ServiceResource.POLL_URL);
    }

    public void editPoll() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.POLL_POLLID, model.getPollID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOLLFOREDIT_METHODNAME, ServiceResource.POLL_URL);
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETPOLLFOREDIT_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            JSONObject jobj;
            JSONArray OptionArray;
            try {
                jobj = new JSONObject(result);
                jsonObj = jobj.getJSONArray(ServiceResource.POLL);
                OptionArray = jobj.getJSONArray(ServiceResource.POLLOPTION);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);

                    String date = Utility.getDate(
                            Long.valueOf(innerObj
                                    .getString(ServiceResource.POLL_STARTDATE)
                                    .replace("/Date(", "").replace(")/", "")),
                            "EEEE/dd/MMM/yyyy");
                    String[] dateArray = date.split("/");
                    if (dateArray != null && dateArray.length > 0) {
                        Log.v("date",
                                (dateArray[0] + "" + dateArray[1] + "" + dateArray[2]));
                        model.setDate(dateArray[1] + " "
                                + dateArray[0].substring(0, 3));
                        model.setMonth(dateArray[2]);
                        model.setYear(dateArray[3]);
                    }

                    model.setStartDate(innerObj
                            .getString(ServiceResource.POLL_STARTDATE)
                            .replace("/Date(", "").replace(")/", ""));
                    model.setPollID(innerObj
                            .getString(ServiceResource.POLL_POLLID));
                    model.setEndDate(innerObj
                            .getString(ServiceResource.POLL_ENDDATE)
                            .replace("/Date(", "").replace(")/", ""));
                    model.setTitle(innerObj
                            .getString(ServiceResource.POLL_TITLE));
                    model.setDetails(innerObj
                            .getString(ServiceResource.POLL_DETAIL));
                    model.setIsNotify(innerObj
                            .getString(ServiceResource.ISNOTIFYALL));
                    model.setIsPresentage(innerObj
                            .getString(ServiceResource.ISPERCENTEGE));
                    model.setIsMultichoice(innerObj
                            .getString(ServiceResource.ISMULTICHOICE));
                }

                optionList = new ArrayList<PollOptionModel>();
                for (int i = 0; i < OptionArray.length(); i++) {
                    JSONObject innerObj = OptionArray.getJSONObject(i);
                    PollOptionModel optionModel = new PollOptionModel();
                    optionModel.setPollOptionID(innerObj.getString(ServiceResource.POLLOPTIONID));
                    optionModel.setOption(innerObj.getString(ServiceResource.OPTION));
                    optionList.add(optionModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            filldata();

        } else if (ServiceResource.SAVEUPDATEPOLLS_METHODNAME.equalsIgnoreCase(methodName)) {
            sendPushNotification();
        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            finish();

        }

    }

    private void filldata() {

        edt_poll_name.setText(model.getTitle());
        edt_introduction_text.setText(model.getDetails());
        tv_start_date.setText(Utility.getDate(Long.valueOf(model.getStartDate()), "dd-MM-yyyy"));
        tv_end_date.setText(Utility.getDate(Long.valueOf(model.getEndDate()), "dd-MM-yyyy"));
        chknotify.setChecked(model.getIsNotify());

        chkpresantage.setChecked(model.getIsPresentage());
        chkmultichoice.setChecked(model.getIsMultichoice());

        if (optionList != null && optionList.size() > 0) {

            for (int i = 0; i < optionList.size(); i++) {

                LayoutInflater layoutInfalater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View v = layoutInfalater.inflate(R.layout.edittext, null);

                EditText edtOptionans = (EditText) v.findViewById(R.id.edtOptionans);
                edtOptionans.setText(optionList.get(i).getOption());
                ImageView imgRemove = (ImageView) v.findViewById(R.id.imgRemove);

                if (i != 0) {

                    imgRemove.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            llanswerlist.removeView((View) v.getParent());

                        }

                    });

                } else {

                    imgRemove.setVisibility(View.INVISIBLE);

                }

                llanswerlist.addView(v);

            }

        } else {

            addEditTextLayout(false);

        }
    }

    public void sendPushNotification() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME, ServiceResource.NOTIFICATION_URL);

    }

}
