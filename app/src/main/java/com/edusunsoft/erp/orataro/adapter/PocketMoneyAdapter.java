package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.PocketMoneyModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.List;

/**
 * Created by admin on 16-05-2017.
 */

public class PocketMoneyAdapter extends RecyclerView.Adapter<PocketMoneyAdapter.MyViewHolder> {

    public List<PocketMoneyModel> pocketmoneyList;
    public Context mcontext;
    public int selectedPosition;

    int getSelectedPosition;
    String Narration = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_date, tv_deposit, tv_withdraw, tv_balance;
        LinearLayout lyl_container;
        ImageView img_pocket_money_detail;

        public MyViewHolder(View view) {

            super(view);


            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_deposit = (TextView) view.findViewById(R.id.tv_deposit);
            tv_withdraw = (TextView) view.findViewById(R.id.tv_withdraw);
            tv_balance = (TextView) view.findViewById(R.id.tv_balance);

            lyl_container = (LinearLayout) view.findViewById(R.id.lyl_container);

            img_pocket_money_detail = (ImageView) view.findViewById(R.id.img_pocket_money_detail);
            img_pocket_money_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSelectedPosition = getLayoutPosition();
                    if (pocketmoneyList.get(getSelectedPosition).getNarration().equalsIgnoreCase(null)
                            || pocketmoneyList.get(getSelectedPosition).getNarration().equalsIgnoreCase("null")) {
                        Narration = "";
                    } else {
                        Narration = pocketmoneyList.get(getSelectedPosition).getNarration();
                    }
                    Utility.showAlertDialog(mcontext, Narration, "Transaction Detail");
                }

            });

        }

    }


    public PocketMoneyAdapter(Context mContext, List<PocketMoneyModel> pocketmoneyList) {

        this.mcontext = mContext;
        this.pocketmoneyList = pocketmoneyList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pocket_money_list_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PocketMoneyModel pocketMoneyModel = pocketmoneyList.get(position);

        Log.d("getdate", pocketmoneyList.get(position).toString());

        holder.tv_date.setText(pocketMoneyModel.getDateOfTransaction());
        holder.tv_deposit.setText(String.valueOf(pocketMoneyModel.getDeposit()));
        if (pocketMoneyModel.getDeposit() == 0.00) {
            holder.tv_deposit.setText("--");
        } else {
            holder.tv_deposit.setText(String.valueOf(pocketMoneyModel.getDeposit()));
        }
        if (pocketMoneyModel.getWithdraw() == 0.00) {
            holder.tv_withdraw.setText("--");
        } else {
            holder.tv_withdraw.setText(String.valueOf(pocketMoneyModel.getWithdraw()));
        }

        holder.tv_balance.setText(String.valueOf(pocketMoneyModel.getTotalBalance()));

        if ((position % 2 == 0)) {

            holder.lyl_container.setBackgroundColor(mcontext.getResources().getColor(R.color.white));

        } else {

            holder.lyl_container.setBackgroundColor(mcontext.getResources().getColor(R.color.gray_color));

        }


        /*END*/

    }

    @Override
    public int getItemCount() {

        return pocketmoneyList.size();

    }

}
