package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PtCommunicationListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.PTCommMemberModel;
import com.edusunsoft.erp.orataro.database.PTCommunicationListDataDao;
import com.edusunsoft.erp.orataro.database.PtCommunicationModel;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PtCOmmunicationActivity extends Activity implements ResponseWebServices, OnClickListener {

    ArrayList<PtCommunicationModel> ptCommunicationModels;
    PullAndLoadListView lvPtCommunication;
    Context mContext;
    StdDivSubModel standardModel;
    PTCommMemberModel memberModel;
    LinearLayout ll_add_new;

    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader, txt_nodatafound;
    PtCommunicationListAdapter adapter;
    EditText edt_title;
    private Dialog mPoweroffDialog;

    // variable declaration for studentmember list from ofline database
    PTCommunicationListDataDao ptCommunicationListDataDao;
    private List<PtCommunicationModel> PTcommunicationList = new ArrayList<>();
    PtCommunicationModel ptCommunicationModel = new PtCommunicationModel();
    /*END*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ptcommunication);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = PtCOmmunicationActivity.this;

        ptCommunicationListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).ptCommunicationListDataDao();

        standardModel = (StdDivSubModel) getIntent().getSerializableExtra("modelStandard");
        memberModel = (PTCommMemberModel) getIntent().getSerializableExtra("model");

        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
        edt_title = (EditText) findViewById(R.id.edt_title);

        lvPtCommunication = (PullAndLoadListView) findViewById(R.id.lvPtcoomunication);
        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);
        ll_add_new.setOnClickListener(this);

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.PTCOMMUNICATOIN).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
            }
        } else {
            ll_add_new.setVisibility(View.VISIBLE);
        }

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);
        imgLeftheader.setOnClickListener(this);
        txtHeader.setText(getResources().getString(R.string.PTCOMMUNICATION));

        edt_title.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                String text = edt_title.getText().toString()
                        .toLowerCase(Locale.getDefault());
                if (adapter != null) {
                    adapter.filter(text);
                }
            }

        });


        lvPtCommunication.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            public void onRefresh() {

                // Get PTCommunication List from offline database - Commented By Krishna : 06-07-2020
                GetPTCommunicationList();
                /*END*/

//                if (Utility.isNetworkAvailable(mContext)) {
//
//                    if (Utility.isTeacher(mContext)) {
//
//                        PtcommunicationList(true);
//
//                    } else {
//
//                        String result = Utility.readFromFile(ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME + new UserSharedPrefrence(mContext).getLoginModel().getGradeID()
//                                + new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()
//                                + memberModel.getSubjectID() + memberModel.getMemberID(), mContext);
//                        if (result.equalsIgnoreCase("")) {
//                            PtcommunicationList(true);
//                        } else {
//                            PtcommunicationList(false);
//                        }
//                        parseptcommunicationlist(result);
//                    }
//
//                } else {
//                    if (Utility.isTeacher(mContext)) {
//                        parseptcommunicationlist(Utility.readFromFile(ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME + standardModel.getStandrdId() + standardModel.getDivisionId() + standardModel.getSubjectId() + memberModel.getMemberID(), mContext));
//                    } else {
//                        parseptcommunicationlist(Utility.readFromFile(ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME + new UserSharedPrefrence(mContext).getLoginModel().getGradeID()
//                                + new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()
//                                + memberModel.getSubjectID() + memberModel.getMemberID(), mContext));
//                    }
//
//                }

            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();

        // Get PTCommunication List from offline database - Commented By Krishna : 06-07-2020
        GetPTCommunicationList();
        /*END*/
//
//        if (Utility.isNetworkAvailable(mContext)) {
//
//            if (Utility.isTeacher(mContext)) {
//                PtcommunicationList(true);
//            } else {
//
//                String result = Utility.readFromFile(ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME + new UserSharedPrefrence(mContext).getLoginModel().getGradeID()
//                        + new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()
//                        + memberModel.getSubjectID() + memberModel.getMemberID(), mContext);
//                if (result.equalsIgnoreCase("")) {
//                    PtcommunicationList(true);
//                } else {
//                    PtcommunicationList(false);
//                }
//                parseptcommunicationlist(result);
//
//            }
//        } else {
//            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
//        }

    }

    private void GetPTCommunicationList() {

        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            if (Utility.isTeacher(mContext)) {
                PTcommunicationList = ptCommunicationListDataDao.getPTComunicationList(memberModel.MemberID);
            } else {
                PTcommunicationList = ptCommunicationListDataDao.getPTComunicationListForStudent(memberModel.MemberID);
            }

            Log.d("getptcommulist", PTcommunicationList.toString());
            if (PTcommunicationList.isEmpty() || PTcommunicationList.size() == 0 || PTcommunicationList == null) {
                PtcommunicationList(true);
            } else {
                //set Adapter
                setPTCommunicationlistAdapter(PTcommunicationList);
                /*END*/
                PtcommunicationList(false);
            }
        } else {
            if (Utility.isTeacher(mContext)) {
                PTcommunicationList = ptCommunicationListDataDao.getPTComunicationList(memberModel.MemberID);
            } else {
                PTcommunicationList = ptCommunicationListDataDao.getPTComunicationListForStudent(memberModel.MemberID);
            }
            if (PTcommunicationList != null) {
                setPTCommunicationlistAdapter(PTcommunicationList);
            }
        }
    }

    private void setPTCommunicationlistAdapter(List<PtCommunicationModel> pTcommunicationList) {
        Log.d("getlist1111", pTcommunicationList.toString());
        if (pTcommunicationList != null && pTcommunicationList.size() > 0) {
            Log.d("getlist11", pTcommunicationList.toString());
            adapter = new PtCommunicationListAdapter(mContext, pTcommunicationList, standardModel);
            lvPtCommunication.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            lvPtCommunication.setVisibility(View.VISIBLE);
        } else {
            txt_nodatafound.setVisibility(View.VISIBLE);
            lvPtCommunication.setVisibility(View.GONE);
        }


    }

    public void createPtCommunication(String str) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.PTMEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.STUDENTMEMBERID, memberModel.getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID_PT, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

            arrayList.add(new PropertyVo(ServiceResource.DivisionID, standardModel.getDivisionId()));
            arrayList.add(new PropertyVo(ServiceResource.GradeID, standardModel.getStandrdId()));
            arrayList.add(new PropertyVo(ServiceResource.PTSUJBECTID, standardModel.getSubjectId()));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.STUDENTMEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID_PT, memberModel.getMemberID()));

            arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.PTSUJBECTID, memberModel.getSubjectID()));

        }

        arrayList.add(new PropertyVo(ServiceResource.PTTITLE, str));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        Log.d("getptRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CREATENEWPTCOMMNUNICATION_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);

    }

    public void PtcommunicationList(boolean isViewPopUp) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.MEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
            arrayList.add(new PropertyVo(ServiceResource.GradeID, standardModel.getStandrdId()));
            arrayList.add(new PropertyVo(ServiceResource.DivisionID, standardModel.getDivisionId()));
            arrayList.add(new PropertyVo(ServiceResource.SUBJECTID1, standardModel.getSubjectId()));
            arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID_PT, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.STUDENTMEMBERID, memberModel.getMemberID()));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.MEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
            arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            arrayList.add(new PropertyVo(ServiceResource.SUBJECTID1, memberModel.getSubjectID()));
            arrayList.add(new PropertyVo(ServiceResource.TEACHERMEMBERID_PT, memberModel.getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.STUDENTMEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        }

        Log.d("ptrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopUp, this).execute(ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME, ServiceResource.PTCOMMUNICATION_URL);

    }


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETPTCOMMUNICATIONLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            parseptcommunicationlist(result);
        } else if (ServiceResource.CREATENEWPTCOMMNUNICATION_METHODNAME.equalsIgnoreCase(methodName)) {
            Log.d("getResponse1234", result);
            // Get PTCommunication List from offline database - Commented By Krishna : 06-07-2020
            GetPTCommunicationList();
            /*END*/
//            PtcommunicationList(true);
            sendPushNotification();
        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {
        }

    }

    private void parseptcommunicationlist(String result) {

        Log.d("getstudentResult", result);

        JSONArray jsonObj;
        if (Utility.isTeacher(mContext)) {
            ptCommunicationListDataDao.deletePTCommunicationByMemberID(memberModel.MemberID);
        } else {
            ptCommunicationListDataDao.deletePTCommunicationByTeacherID(memberModel.MemberID);
        }

        try {

            jsonObj = new JSONArray(result);
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);

                // parse homework list data
                ParsePTCommunicationList(innerObj);
                /*END*/
            }

            if (Utility.isTeacher(mContext)) {
                if (ptCommunicationListDataDao.getPTComunicationList(memberModel.MemberID).size() > 0) {
                    Log.d("getlis123", ptCommunicationListDataDao.getPTComunicationList(memberModel.MemberID).toString());
                    //set Adapter
                    setPTCommunicationlistAdapter(ptCommunicationListDataDao.getPTComunicationList(memberModel.MemberID));
                    /*END*/
                } else {
                    txt_nodatafound.setVisibility(View.VISIBLE);
                    lvPtCommunication.setVisibility(View.GONE);
                }

            } else {
                if (ptCommunicationListDataDao.getPTComunicationListForStudent(memberModel.MemberID).size() > 0) {
                    //set Adapter
                    setPTCommunicationlistAdapter(ptCommunicationListDataDao.getPTComunicationListForStudent(memberModel.MemberID));
                    /*END*/
                } else {
                    txt_nodatafound.setVisibility(View.VISIBLE);
                    lvPtCommunication.setVisibility(View.GONE);
                }
            }

//

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void ParsePTCommunicationList(JSONObject innerObj) {

        try {
            ptCommunicationModel.setCommunicationID(innerObj.getString(ServiceResource.PTCOMMUNICATIONID));
            ptCommunicationModel.setCommunicationDetail(innerObj.getString(ServiceResource.PTCOMMUNICATIONDETAIL));
            ptCommunicationModel.setUserName(innerObj.getString(ServiceResource.PTUSERNAME));
            ptCommunicationModel.setUnReadCount(innerObj.getString(ServiceResource.PTUNREADCOUNT));
            ptCommunicationModel.setSeqNo(innerObj.getString(ServiceResource.PTSEQNO));
            ptCommunicationModel.setMemberID(innerObj.getString("MemberID"));
            ptCommunicationModel.setTeacherID(innerObj.getString("TeacherID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ptCommunicationListDataDao.insertPTCommunicationDataList(ptCommunicationModel);

        Log.d("getmemberidhere", memberModel.getMemberID());
        Log.d("getptlisttt", ptCommunicationListDataDao.getPTComunicationList(memberModel.getMemberID()).toString());

    }

    public void sendPushNotification() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.ll_add_new) {

            showPopup();
//            if (Utility.isTeacher(mContext)) {
//                if (Utility.ReadWriteSetting(ServiceResource.PTCOMMUNICATOIN).getIsCreate()) {
//                    if (Utility.isNetworkAvailable(mContext)) {
//                        showPopup();
//                    } else {
//                        Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection));
//                    }
//                } else {
//                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
//                }
//            } else {
//                showPopup();
//            }
        } else if (v.getId() == R.id.img_home) {
            finish();
        }
    }

    public void showPopup() {

        Constants.ForDialogStyle = "Logout";
        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, true);

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog
                .findViewById(R.id.ll_submit);
        TextView tv_header = (TextView) mPoweroffDialog.findViewById(R.id.tv_header);
        tv_header.setText(getResources().getString(R.string.AddPtCommunication));
        TextView tv_say_something = (TextView) mPoweroffDialog.findViewById(R.id.tv_say_something);
        tv_say_something.setVisibility(View.GONE);
        final EditText edt_title = (EditText) mPoweroffDialog.findViewById(R.id.edt_title);

        View view_line = mPoweroffDialog.findViewById(R.id.view_line);
        view_line.setVisibility(View.VISIBLE);
        edt_title.setVisibility(View.VISIBLE);
        ll_submit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (!edt_title.getText().toString().equalsIgnoreCase("")) {
                    mPoweroffDialog.dismiss();
                    createPtCommunication(edt_title.getText().toString());
                } else {
                    Utility.toast(mContext, "Please Enter Title");
                }

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog
                .findViewById(R.id.img_close);
        img_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mPoweroffDialog.dismiss();

            }

        });

    }

}
