package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class WallPostModel implements Serializable {

    private String RowNo;
    private String PostCommentID;
    private String WallID;
    private String MemberID;
    private String PostCommentTypesTerm;
    private String PostCommentNote;
    private String TotalLikes;
    private String TotalComments;
    private String TotalDislike;
    private String DateOfPost;
    private String AssociationID;
    private String AssociationType;
    private String FullName;
    private String IsDisLike;
    private String IsLike;
    private String ProfilePicture;
    private String Photo;
    private String TempDate;
    private String PostDate;
    private String PostCount;
    private String PostUrls;
    private String WallTypeTerm;
    private String FileType, PostedOn, FileMimeType;
    private String SendToMemberID;
    private String PhotoCount;
    private String PhotoUrls;
    private String PostName;
    private String AlbumPhotoID;
    private boolean isUserComment = true, isUserShare = true;
    private boolean isUserLike = true, isUserDisLike = true;
    private String IsAllowLikeDislike, IsAllowPostComment, PostComment, IsAllowSharePost, IsAllowPeopleToPostCommentOnPostWall, IsAllowPeopleToShareYourPost, IsAllowPeopleToLikeOrDislikeOnYourPost, IsAllowPeopleToPostMessageOnYourWall, IsAllowPeopleToLikeAndDislikeCommentWall, IsAllowPeopleToShareCommentWall, IsAllowPeoplePostCommentWall;

    public boolean getIsAllowLikeDislike() {
        return Strtobool(IsAllowLikeDislike);
    }

    public void setIsAllowLikeDislike(String isAllowLikeDislike) {
        IsAllowLikeDislike = isAllowLikeDislike;
    }

    public boolean getIsAllowPostComment() {
        return Strtobool(IsAllowPostComment);

    }

    public void setIsAllowPostComment(String isAllowPostComment) {
        IsAllowPostComment = isAllowPostComment;
    }

    public boolean getPostComment() {
        return Strtobool(PostComment);
    }

    public void setPostComment(String postComment) {
        PostComment = postComment;
    }

    public boolean getIsAllowSharePost() {
        return Strtobool(IsAllowSharePost);
    }

    public void setIsAllowSharePost(String isAllowSharePost) {
        IsAllowSharePost = isAllowSharePost;
    }

    public boolean getIsAllowPeopleToPostCommentOnPostWall() {
        return Strtobool(IsAllowPeopleToPostCommentOnPostWall);
    }

    public void setIsAllowPeopleToPostCommentOnPostWall(String isAllowPeopleToPostCommentOnPostWall) {
        IsAllowPeopleToPostCommentOnPostWall = isAllowPeopleToPostCommentOnPostWall;
    }

    public String getAlbumPhotoID() {
        return AlbumPhotoID;
    }

    public void setAlbumPhotoID(String albumPhotoID) {
        AlbumPhotoID = albumPhotoID;
    }

    public String getPostName() {
        return PostName;
    }

    public void setPostName(String postName) {
        PostName = postName;
    }

    public String getTempDate() {
        return TempDate;
    }

    public void setTempDate(String tempDate) {
        TempDate = tempDate;
    }

    public String getFileMimeType() {
        return FileMimeType;
    }

    public void setFileMimeType(String fileMimeType) {
        FileMimeType = fileMimeType;
    }

    public String getPostedOn() {
        return PostedOn;
    }

    public void setPostedOn(String postedOn) {
        PostedOn = postedOn;
    }

    public boolean isUserComment() {
        return isUserComment;
    }

    public void setUserComment(boolean isUserComment) {
        this.isUserComment = isUserComment;
    }

    public boolean isUserShare() {
        return isUserShare;
    }

    public void setUserShare(boolean isUserShare) {
        this.isUserShare = isUserShare;
    }

    public boolean isUserLike() {
        return isUserLike;
    }

    public void setUserLike(boolean isUserLike) {
        this.isUserLike = isUserLike;
    }

    public boolean isUserDisLike() {
        return isUserDisLike;
    }

    public void setUserDisLike(boolean isUserDisLike) {
        this.isUserDisLike = isUserDisLike;
    }

    public String getSendToMemberID() {
        return SendToMemberID;
    }

    public void setSendToMemberID(String sendToMemberID) {
        SendToMemberID = sendToMemberID;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getRowNo() {
        return RowNo;
    }

    public void setRowNo(String rowNo) {
        RowNo = rowNo;
    }

    public String getPostCommentID() {
        return PostCommentID;
    }

    public void setPostCommentID(String postCommentID) {
        PostCommentID = postCommentID;
    }

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getPostCommentTypesTerm() {
        return PostCommentTypesTerm;
    }

    public void setPostCommentTypesTerm(String postCommentTypesTerm) {
        PostCommentTypesTerm = postCommentTypesTerm;
    }

    public String getPostCommentNote() {
        return PostCommentNote;
    }

    public void setPostCommentNote(String postCommentNote) {
        PostCommentNote = postCommentNote;
    }

    public String getTotalLikes() {
        return TotalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        TotalLikes = totalLikes;
    }

    public String getTotalComments() {
        return TotalComments;
    }

    public void setTotalComments(String totalComments) {
        TotalComments = totalComments;
    }

    public String getTotalDislike() {
        return TotalDislike;
    }

    public void setTotalDislike(String totalDislike) {
        TotalDislike = totalDislike;
    }

    public String getDateOfPost() {
        return DateOfPost;
    }

    public void setDateOfPost(String dateOfPost) {
        DateOfPost = dateOfPost;
    }

    public String getAssociationID() {
        return AssociationID;
    }

    public void setAssociationID(String associationID) {
        AssociationID = associationID;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getIsDisLike() {
        return IsDisLike;
    }

    public void setIsDisLike(String isDisLike) {
        IsDisLike = isDisLike;
    }

    public String getIsLike() {
        return IsLike;
    }

    public void setIsLike(String isLike) {
        IsLike = isLike;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        ProfilePicture = profilePicture;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getPostDate() {
        return PostDate;
    }

    public void setPostDate(String postDate) {
        PostDate = postDate;
    }

    public String getPhotoCount() {
        return PhotoCount;
    }

    public void setPhotoCount(String photoCount) {
        PhotoCount = photoCount;
    }

    public String getPhotoUrls() {
        return PhotoUrls;
    }

    public void setPhotoUrls(String photoUrls) {
        PhotoUrls = photoUrls;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getPostCount() {
        return PostCount;
    }

    public void setPostCount(String postCount) {
        PostCount = postCount;
    }

    public String getPostUrls() {
        return PostUrls;
    }

    public void setPostUrls(String postUrls) {
        PostUrls = postUrls;
    }

    public String getWallTypeTerm() {
        return WallTypeTerm;
    }

    public void setWallTypeTerm(String wallTypeTerm) {
        WallTypeTerm = wallTypeTerm;
    }

    public boolean getIsAllowPeopleToShareYourPost() {
        return Strtobool(IsAllowPeopleToShareYourPost);
    }

    public void setIsAllowPeopleToShareYourPost(String isAllowPeopleToShareYourPost) {
        IsAllowPeopleToShareYourPost = isAllowPeopleToShareYourPost;
    }

    public boolean getIsAllowPeopleToLikeOrDislikeOnYourPost() {
        return Strtobool(IsAllowPeopleToLikeOrDislikeOnYourPost);
    }

    public void setIsAllowPeopleToLikeOrDislikeOnYourPost(String isAllowPeopleToLikeOrDislikeOnYourPost) {
        IsAllowPeopleToLikeOrDislikeOnYourPost = isAllowPeopleToLikeOrDislikeOnYourPost;
    }

    public boolean getIsAllowPeopleToPostMessageOnYourWall() {
        return Strtobool(IsAllowPeopleToPostMessageOnYourWall);
    }

    public void setIsAllowPeopleToPostMessageOnYourWall(String isAllowPeopleToPostMessageOnYourWall) {
        IsAllowPeopleToPostMessageOnYourWall = isAllowPeopleToPostMessageOnYourWall;
    }

    public boolean getIsAllowPeopleToLikeAndDislikeCommentWall() {
        return Strtobool(IsAllowPeopleToLikeAndDislikeCommentWall);
    }

    public void setIsAllowPeopleToLikeAndDislikeCommentWall(String isAllowPeopleToLikeAndDislikeCommentWall) {
        IsAllowPeopleToLikeAndDislikeCommentWall = isAllowPeopleToLikeAndDislikeCommentWall;
    }

    public boolean getIsAllowPeopleToShareCommentWall() {
        return Strtobool(IsAllowPeopleToShareCommentWall);
    }

    public void setIsAllowPeopleToShareCommentWall(String isAllowPeopleToShareCommentWall) {
        IsAllowPeopleToShareCommentWall = isAllowPeopleToShareCommentWall;
    }

    public String getIsAllowPeoplePostCommentWall() {
        return IsAllowPeoplePostCommentWall;
    }

    public void setIsAllowPeoplePostCommentWall(String isAllowPeoplePostCommentWall) {
        IsAllowPeoplePostCommentWall = isAllowPeoplePostCommentWall;
    }

    public boolean Strtobool(String str) {
        if (str != null && !str.equalsIgnoreCase("")) {
            if (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("0")) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    @Override
    public String toString() {
        return "WallPostModel{" +
                "RowNo='" + RowNo + '\'' +
                ", PostCommentID='" + PostCommentID + '\'' +
                ", WallID='" + WallID + '\'' +
                ", MemberID='" + MemberID + '\'' +
                ", PostCommentTypesTerm='" + PostCommentTypesTerm + '\'' +
                ", PostCommentNote='" + PostCommentNote + '\'' +
                ", TotalLikes='" + TotalLikes + '\'' +
                ", TotalComments='" + TotalComments + '\'' +
                ", TotalDislike='" + TotalDislike + '\'' +
                ", DateOfPost='" + DateOfPost + '\'' +
                ", AssociationID='" + AssociationID + '\'' +
                ", AssociationType='" + AssociationType + '\'' +
                ", FullName='" + FullName + '\'' +
                ", IsDisLike='" + IsDisLike + '\'' +
                ", IsLike='" + IsLike + '\'' +
                ", ProfilePicture='" + ProfilePicture + '\'' +
                ", Photo='" + Photo + '\'' +
                ", TempDate='" + TempDate + '\'' +
                ", PostDate='" + PostDate + '\'' +
                ", PostCount='" + PostCount + '\'' +
                ", PostUrls='" + PostUrls + '\'' +
                ", WallTypeTerm='" + WallTypeTerm + '\'' +
                ", FileType='" + FileType + '\'' +
                ", PostedOn='" + PostedOn + '\'' +
                ", FileMimeType='" + FileMimeType + '\'' +
                ", SendToMemberID='" + SendToMemberID + '\'' +
                ", PhotoCount='" + PhotoCount + '\'' +
                ", PhotoUrls='" + PhotoUrls + '\'' +
                ", isUserComment=" + isUserComment +
                ", isUserShare=" + isUserShare +
                ", isUserLike=" + isUserLike +
                ", isUserDisLike=" + isUserDisLike +
                ", IsAllowPeopleToShareYourPost='" + IsAllowPeopleToShareYourPost + '\'' +
                ", IsAllowPeopleToLikeOrDislikeOnYourPost='" + IsAllowPeopleToLikeOrDislikeOnYourPost + '\'' +
                ", IsAllowPeopleToPostMessageOnYourWall='" + IsAllowPeopleToPostMessageOnYourWall + '\'' +
                ", IsAllowPeopleToLikeAndDislikeCommentWall='" + IsAllowPeopleToLikeAndDislikeCommentWall + '\'' +
                ", IsAllowPeopleToShareCommentWall='" + IsAllowPeopleToShareCommentWall + '\'' +
                ", IsAllowPeoplePostCommentWall='" + IsAllowPeoplePostCommentWall + '\'' +
                '}';
    }
}
