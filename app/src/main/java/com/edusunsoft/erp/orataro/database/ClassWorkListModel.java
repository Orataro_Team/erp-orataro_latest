package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "ClassworkList")
public class ClassWorkListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "ClassWorkID")
    @SerializedName("ClassWorkID")
    public String ClassWorkID;

    @ColumnInfo(name = "DateOfClassWork")
    @SerializedName("DateOfClassWork")
    public String DateOfClassWork;

    @ColumnInfo(name = "PostID")
    @SerializedName("PostID")
    public String PostID;

    @ColumnInfo(name = "GradeID")
    @SerializedName("GradeID")
    public String GradeID;

    @ColumnInfo(name = "DivisionID")
    @SerializedName("DivisionID")
    public String DivisionID;

    @ColumnInfo(name = "SubjectID")
    @SerializedName("SubjectID")
    public String SubjectID;

    @ColumnInfo(name = "TeacherID")
    @SerializedName("TeacherID")
    public String TeacherID;

    @ColumnInfo(name = "ClassWorkTitle")
    @SerializedName("ClassWorkTitle")
    public String ClassWorkTitle;

    @ColumnInfo(name = "ClassWorkDetails")
    @SerializedName("ClassWorkDetails")
    public String ClassWorkDetails;

    @ColumnInfo(name = "strDateOfClasswork")
    @SerializedName("strDateOfClasswork")
    public String strDateOfClasswork;

    @ColumnInfo(name = "ReferenceLink")
    @SerializedName("ReferenceLink")
    public String ReferenceLink;

    @ColumnInfo(name = "ExpectingDateOfCompletion")
    @SerializedName("ExpectingDateOfCompletion")
    public String ExpectingDateOfCompletion;

    @ColumnInfo(name = "StartTime")
    @SerializedName("StartTime")
    public String StartTime;

    @ColumnInfo(name = "EndTime")
    @SerializedName("EndTime")
    public String EndTime;

    @ColumnInfo(name = "UserName")
    @SerializedName("UserName")
    public String UserName;

    @ColumnInfo(name = "SubjectName")
    @SerializedName("SubjectName")
    public String SubjectName;

    @ColumnInfo(name = "techearName")
    @SerializedName("techearName")
    public String techearName;

    @ColumnInfo(name = "teacherImg")
    @SerializedName("teacherImg")
    public String teacherImg;

    @ColumnInfo(name = "dateInMillisecond")
    @SerializedName("dateInMillisecond")
    public String dateInMillisecond;

    @ColumnInfo(name = "IsRead")
    @SerializedName("IsRead")
    public boolean IsRead;

    @ColumnInfo(name = "isCheck")
    @SerializedName("isCheck")
    public boolean isCheck;

    @ColumnInfo(name = "imgUrl")
    @SerializedName("imgUrl")
    public String imgUrl;

    @ColumnInfo(name = "GradeName")
    @SerializedName("GradeName")
    public String GradeName;

    @ColumnInfo(name = "DivisionName")
    @SerializedName("DivisionName")
    public String DivisionName;

    @ColumnInfo(name = "visibleMonth")
    @SerializedName("visibleMonth")
    public boolean visibleMonth;

    @ColumnInfo(name = "month")
    @SerializedName("month")
    public String month;

    @ColumnInfo(name = "day")
    @SerializedName("day")
    public String day;

    @ColumnInfo(name = "year")
    @SerializedName("year")
    public String year;


    public ClassWorkListModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassWorkID() {
        return ClassWorkID;
    }

    public void setClassWorkID(String classWorkID) {
        ClassWorkID = classWorkID;
    }

    public String getDateOfClassWork() {
        return DateOfClassWork;
    }

    public void setDateOfClassWork(String dateOfClassWork) {
        DateOfClassWork = dateOfClassWork;
    }

    public String getPostID() {
        return PostID;
    }

    public void setPostID(String postID) {
        PostID = postID;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getTeacherID() {
        return TeacherID;
    }

    public void setTeacherID(String teacherID) {
        TeacherID = teacherID;
    }

    public String getClassWorkTitle() {
        return ClassWorkTitle;
    }

    public void setClassWorkTitle(String classWorkTitle) {
        ClassWorkTitle = classWorkTitle;
    }

    public String getClassWorkDetails() {
        return ClassWorkDetails;
    }

    public void setClassWorkDetails(String classWorkDetails) {
        ClassWorkDetails = classWorkDetails;
    }

    public String getStrDateOfClasswork() {
        return strDateOfClasswork;
    }

    public void setStrDateOfClasswork(String strDateOfClasswork) {
        this.strDateOfClasswork = strDateOfClasswork;
    }

    public String getReferenceLink() {
        return ReferenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        ReferenceLink = referenceLink;
    }

    public String getExpectingDateOfCompletion() {
        return ExpectingDateOfCompletion;
    }

    public void setExpectingDateOfCompletion(String expectingDateOfCompletion) {
        ExpectingDateOfCompletion = expectingDateOfCompletion;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getTechearName() {
        return techearName;
    }

    public void setTechearName(String techearName) {
        this.techearName = techearName;
    }

    public String getTeacherImg() {
        return teacherImg;
    }

    public void setTeacherImg(String teacherImg) {
        this.teacherImg = teacherImg;
    }

    public String getDateInMillisecond() {
        return dateInMillisecond;
    }

    public void setDateInMillisecond(String dateInMillisecond) {
        this.dateInMillisecond = dateInMillisecond;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getGradeName() {
        return GradeName;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    public String getDivisionName() {
        return DivisionName;
    }

    public void setDivisionName(String divisionName) {
        DivisionName = divisionName;
    }

    public boolean isVisibleMonth() {
        return visibleMonth;
    }

    public void setVisibleMonth(boolean visibleMonth) {
        this.visibleMonth = visibleMonth;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "ClassWorkListModel{" +
                "id=" + id +
                ", ClassWorkID='" + ClassWorkID + '\'' +
                ", DateOfClassWork='" + DateOfClassWork + '\'' +
                ", PostID='" + PostID + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", SubjectID='" + SubjectID + '\'' +
                ", TeacherID='" + TeacherID + '\'' +
                ", ClassWorkTitle='" + ClassWorkTitle + '\'' +
                ", ClassWorkDetails='" + ClassWorkDetails + '\'' +
                ", strDateOfClasswork='" + strDateOfClasswork + '\'' +
                ", ReferenceLink='" + ReferenceLink + '\'' +
                ", ExpectingDateOfCompletion='" + ExpectingDateOfCompletion + '\'' +
                ", StartTime='" + StartTime + '\'' +
                ", EndTime='" + EndTime + '\'' +
                ", UserName='" + UserName + '\'' +
                ", SubjectName='" + SubjectName + '\'' +
                ", techearName='" + techearName + '\'' +
                ", teacherImg='" + teacherImg + '\'' +
                ", dateInMillisecond='" + dateInMillisecond + '\'' +
                ", IsRead=" + IsRead +
                ", isCheck=" + isCheck +
                ", imgUrl='" + imgUrl + '\'' +
                ", GradeName='" + GradeName + '\'' +
                ", DivisionName='" + DivisionName + '\'' +
                ", visibleMonth=" + visibleMonth +
                ", month='" + month + '\'' +
                ", day='" + day + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
