package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;

public class AddPtCommunicationActivity extends Activity implements OnClickListener {

	private ImageView imgLeftheader,imgRightHeader;
	private TextView txtHeader;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_pt_communication);
		imgLeftheader = (ImageView) findViewById(R.id.img_home);
		imgRightHeader = (ImageView) findViewById(R.id.img_menu);
		txtHeader = (TextView) findViewById(R.id.header_text);
		mContext = AddPtCommunicationActivity.this;
		imgLeftheader.setImageResource(R.drawable.back);
		imgRightHeader.setImageResource(R.drawable.tick_mark);

		try{ 
			txtHeader.setText(getResources().getString(R.string.AddPtCommunication)+" ("+ Utility.GetFirstName(mContext) +")");
		}catch(Exception e){
			e.printStackTrace();
		}

		imgLeftheader.setOnClickListener(this);
		imgRightHeader.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_home:
			finish();
			break;
		case R.id.img_menu:
			finish();
			break;
		default:
			break;
		}
	}


}
