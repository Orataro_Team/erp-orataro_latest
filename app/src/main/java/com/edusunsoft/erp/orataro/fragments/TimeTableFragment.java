package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.TimeTableListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.TimeTableDataDao;
import com.edusunsoft.erp.orataro.database.TimeTableModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.edusunsoft.erp.orataro.util.timeComperator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimeTableFragment extends Fragment implements OnClickListener, ResponseWebServices {

    private TextView txtDay, txt_nodatafound;
    private ListView lvTimetable;
    private LinearLayout ll_main;
    private ImageView imgNextDay, imgPreviousDay;
    private String[] day = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
            "Saturday"};
    private int pos = 0;
    private Context mContext;
    public TimeTableListAdapter timeTableListAdapter;
    public ArrayList<TimeTableModel> timeTableModels;
    public ArrayList<TimeTableModel> timeTableModels_day;


    // variable declaration for homeworklist from ofline database
    TimeTableDataDao timeTableDataDao;
    private List<TimeTableModel> TimeTableList = new ArrayList<>();
    TimeTableModel timeTableModel = new TimeTableModel();
    public String ISDataExist = "";
    /*END*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        View v = inflater.inflate(R.layout.activity_time_table, null);
        imgNextDay = (ImageView) v.findViewById(R.id.imgNextDay);
        imgPreviousDay = (ImageView) v.findViewById(R.id.imgPreviousDay);
        txtDay = (TextView) v.findViewById(R.id.txtDay);
        lvTimetable = (ListView) v.findViewById(R.id.lvTimetable);

        timeTableDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).timeTableDataDao();

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.TimeTable) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);
        ll_main = (LinearLayout) v.findViewById(R.id.ll_main);
        txtDay.setText(day[pos]);

        imgNextDay.setOnClickListener(this);
        imgPreviousDay.setOnClickListener(this);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Get Timetable for Teacher and Student
        GetTimeTable();
        /*END*/
    }

    private void GetTimeTable() {

        if (Utility.isNetworkAvailable(mContext)) {

            if (Utility.isTeacher(mContext)) {
                TimeTableList = timeTableDataDao.getTimeTableList("Teacher");
                if (TimeTableList.isEmpty() || TimeTableList.size() == 0 || TimeTableList == null) {
                    getTimeTableTeacher(true);
                } else {
                    //set Adapter
                    setTimeTablelistAdapter(TimeTableList);
                    /*END*/
                    getTimeTableTeacher(false);
                }
            } else {
                TimeTableList = timeTableDataDao.getTimeTableList("Student");
                if (TimeTableList.isEmpty() || TimeTableList.size() == 0 || TimeTableList == null) {
                    getTimeTable(true);
                } else {
                    //set Adapter
                    setTimeTablelistAdapter(TimeTableList);
                    /*END*/
                    getTimeTable(false);
                }
            }
        } else {
            if (Utility.isTeacher(mContext)) {
                TimeTableList = timeTableDataDao.getTimeTableList("Teacher");
            } else {
                TimeTableList = timeTableDataDao.getTimeTableList("Student");
            }
            if (TimeTableList != null) {
                setTimeTablelistAdapter(TimeTableList);
            }
            Utility.showAlertDialog(getActivity(), mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

    }

    private void setTimeTablelistAdapter(List<TimeTableModel> timeTableList) {

        timeTableModels_day = new ArrayList<>();
        if (timeTableList != null && timeTableList.size() > 0) {

            for (int i = 0; i < timeTableList.size(); i++) {
                if (timeTableList.get(i).getDayofweek()
                        .equalsIgnoreCase(day[pos])) {
                    timeTableModels_day.add(timeTableList.get(i));
                }
            }
        }

        Collections.sort(timeTableModels_day, new timeComperator());
        if (timeTableModels_day != null
                && timeTableModels_day.size() > 0) {
            timeTableListAdapter = new TimeTableListAdapter(
                    mContext, timeTableModels_day);
            lvTimetable.setAdapter(timeTableListAdapter);
            lvTimetable.setVisibility(View.VISIBLE);
            timeTableListAdapter.notifyDataSetChanged();
        } else {
            lvTimetable.setVisibility(View.GONE);
            txt_nodatafound.setText(getResources().getString(R.string.NoTimeTableListAvailableFor) + day[pos]);
            txt_nodatafound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home:

                break;
            case R.id.imgNextDay:
                if (pos < 5) {
                    pos = pos + 1;
                } else {
                    pos = 0;
                }

                txtDay.setText(day[pos]);

                if (Utility.isTeacher(mContext)) {
                    TimeTableList = timeTableDataDao.getTimeTableList("Teacher");
                } else {
                    TimeTableList = timeTableDataDao.getTimeTableList("Student");
                }
                if (TimeTableList != null) {
                    setTimeTablelistAdapter(TimeTableList);
                }


//                timeTableModels_day = new ArrayList<TimeTableModel>();
//                if (timeTableModels != null && timeTableModels.size() > 0) {
//                    for (int i = 0; i < timeTableModels.size(); i++) {
//                        if (timeTableModels.get(i).getDayofweek().equalsIgnoreCase(day[pos])) {
//                            timeTableModels_day.add(timeTableModels.get(i));
//                        }
//                    }
//                }
//
//                Collections.sort(timeTableModels_day, new timeComperator());
//                if (timeTableModels_day != null && timeTableModels_day.size() > 0) {
//                    timeTableListAdapter = new TimeTableListAdapter(mContext, timeTableModels_day);
//                    lvTimetable.setAdapter(timeTableListAdapter);
//                    lvTimetable.setVisibility(View.VISIBLE);
//                    timeTableListAdapter.notifyDataSetChanged();
//                    txt_nodatafound.setVisibility(View.GONE);
//                } else {
//                    lvTimetable.setVisibility(View.GONE);
//                    txt_nodatafound.setText(getResources().getString(R.string.NoTimeTableListAvailableFor) + day[pos]);
//                    txt_nodatafound.setVisibility(View.VISIBLE);
//                }
                break;
            case R.id.imgPreviousDay:
                if (pos > 0) {
                    pos = pos - 1;
                } else {
                    pos = 5;
                }

                txtDay.setText(day[pos]);

                if (Utility.isTeacher(mContext)) {
                    TimeTableList = timeTableDataDao.getTimeTableList("Teacher");
                } else {
                    TimeTableList = timeTableDataDao.getTimeTableList("Student");
                }
                if (TimeTableList != null) {
                    setTimeTablelistAdapter(TimeTableList);
                }

//                timeTableModels_day = new ArrayList<TimeTableModel>();
//                if (timeTableModels != null && timeTableModels.size() > 0) {
//                    for (int i = 0; i < timeTableModels.size(); i++) {
//                        if (timeTableModels.get(i).getDayofweek().equalsIgnoreCase(day[pos])) {
//                            timeTableModels_day.add(timeTableModels.get(i));
//                        }
//                    }
//                }
//
//                Collections.sort(timeTableModels_day, new timeComperator());
//
//                if (timeTableModels_day != null && timeTableModels_day.size() > 0) {
//                    timeTableListAdapter = new TimeTableListAdapter(mContext, timeTableModels_day);
//                    lvTimetable.setAdapter(timeTableListAdapter);
//                    lvTimetable.setVisibility(View.VISIBLE);
//                    timeTableListAdapter.notifyDataSetChanged();
//                } else {
//                    lvTimetable.setVisibility(View.GONE);
//                    txt_nodatafound.setText(getResources().getString(R.string.NoTimeTableListAvailableFor) + day[pos]);
//                    txt_nodatafound.setVisibility(View.VISIBLE);
//                }
                break;
            default:
                break;
        }
    }

    public void getTimeTableTeacher(boolean isViewPopup) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.TIMETABLETEACHER_METHODNAME, ServiceResource.TIMETABLE_URL);
    }

    public void getTimeTable(boolean isViewPopup) {
        int error = 0;
        try {
            ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
            arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
            arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
            if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
                arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
                arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            } else {
                arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
                arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
            }

            new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.TIMETABLE_METHODNAME, ServiceResource.TIMETABLE_URL);

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.TIMETABLE_METHODNAME.equalsIgnoreCase(methodName)) {
            parseTimetable(result, methodName);
        } else if (ServiceResource.TIMETABLETEACHER_METHODNAME.equalsIgnoreCase(methodName)) {
            parseTimetableForTeacher(result, methodName);
        }
    }

    private void parseTimetableForTeacher(String result, String methodName) {
        JSONArray jsonObj;
        timeTableDataDao.deleteTimeTable("Teacher");
        try {
            jsonObj = new JSONArray(result);
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                // parse Timetable list
                ParseTimeTableListForTeacher(innerObj, methodName);
                /*END*/
            }

            if (timeTableDataDao.getTimeTableList("Teacher").size() > 0) {
                //set Adapter
                setTimeTablelistAdapter(timeTableDataDao.getTimeTableList("Teacher"));
                /*END*/
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ParseTimeTableListForTeacher(JSONObject innerObj, String methodName) {

        try {

            timeTableModel.setDayofweek(innerObj.getString(ServiceResource.DAYOFWEEK));
            timeTableModel.setEndTime(innerObj.getString(ServiceResource.ENDTIME));
            timeTableModel.setStartTime(innerObj.getString(ServiceResource.STARTTIME));
            timeTableModel.setSubjectID(innerObj.getString(ServiceResource.SUBJECTID));
            timeTableModel.setSubjectName(innerObj.getString(ServiceResource.SUBJECTNAME));
            timeTableModel.setForTeacherORStudent("Teacher");
            String divisionname = innerObj.getString(ServiceResource.DIVISIONNAMETIMETABLE);
            String gradeName = innerObj.getString(ServiceResource.GRADENAMETIMETABLE);
            String subjectname = innerObj.getString(ServiceResource.SUBJECTNAME);
            timeTableModel.setSubjectName(gradeName + "/" + divisionname + "/" + subjectname);
            long secounds = 0;
            int hour = Integer.valueOf(timeTableModel.getEndTime().split(" ")[0].split(":")[0]);
            int min = Integer.valueOf(timeTableModel.getEndTime().split(" ")[0].split(":")[1]);
            if (timeTableModel.getEndTime().contains("PM")) {
                secounds = (12 + 60 * 60) + (hour * 60 * 60)
                        + (min * 60);
            } else {
                secounds = (hour * 60 * 60) + (min * 60);
            }

            timeTableModel.setTotalTime(secounds);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        timeTableDataDao.insertTimeTableList(timeTableModel);

    }

    public void parseTimetable(String result, String methodName) {
        JSONArray jsonObj;
        timeTableDataDao.deleteTimeTable("Student");
        try {
            jsonObj = new JSONArray(result);
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                // parse Timetable list
                ParseTimeTableList(innerObj, methodName);
                /*END*/
            }

            if (timeTableDataDao.getTimeTableList("Student").size() > 0) {
                //set Adapter
                setTimeTablelistAdapter(timeTableDataDao.getTimeTableList("Student"));
                /*END*/
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void ParseTimeTableList(JSONObject innerObj, String methodName) {

        try {

            timeTableModel.setDayofweek(innerObj.getString(ServiceResource.DAYOFWEEK));
            timeTableModel.setEndTime(innerObj.getString(ServiceResource.ENDTIME));
            timeTableModel.setStartTime(innerObj.getString(ServiceResource.STARTTIME));
            timeTableModel.setSubjectID(innerObj.getString(ServiceResource.SUBJECTID));
            timeTableModel.setSubjectName(innerObj.getString(ServiceResource.SUBJECTNAME));
            timeTableModel.setForTeacherORStudent("Student");
            timeTableModel.setSubjectName(innerObj.getString(ServiceResource.SUBJECTNAME));
            if (timeTableModel.getSubjectName() != null && !timeTableModel.getSubjectName().equals("null")) {
                timeTableModel.setSubjectName(innerObj.getString(ServiceResource.SUBJECTNAME));
            } else {
                timeTableModel.setSubjectName("Break");
            }

            long secounds = 0;
            int hour = Integer.valueOf(timeTableModel.getEndTime().split(" ")[0].split(":")[0]);
            int min = Integer.valueOf(timeTableModel.getEndTime().split(" ")[0].split(":")[1]);
            if (timeTableModel.getEndTime().contains("PM")) {
                secounds = (12 + 60 * 60) + (hour * 60 * 60)
                        + (min * 60);
            } else {
                secounds = (hour * 60 * 60) + (min * 60);
            }

            timeTableModel.setTotalTime(secounds);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        timeTableDataDao.insertTimeTableList(timeTableModel);

    }

}
