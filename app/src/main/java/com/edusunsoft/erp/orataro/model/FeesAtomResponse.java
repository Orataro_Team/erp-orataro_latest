package com.edusunsoft.erp.orataro.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesAtomResponse implements Parcelable {

    @SerializedName("mmp_txn")
    @Expose
    private String mmpTxn;
    @SerializedName("mer_txn")
    @Expose
    private String merTxn;
    @SerializedName("amt")
    @Expose
    private String amt;
    @SerializedName("prod")
    @Expose
    private String prod;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("bank_txn")
    @Expose
    private String bankTxn;
    @SerializedName("f_code")
    @Expose
    private String fCode;
    @SerializedName("clientcode")
    @Expose
    private String clientcode;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("merchant_id")
    @Expose
    private String merchantId;
    @SerializedName("udf9")
    @Expose
    private String udf9;
    @SerializedName("discriminator")
    @Expose
    private String discriminator;
    @SerializedName("surcharge")
    @Expose
    private String surcharge;
    @SerializedName("CardNumber")
    @Expose
    private String cardNumber;
    @SerializedName("udf1")
    @Expose
    private String udf1;
    @SerializedName("udf2")
    @Expose
    private String udf2;
    @SerializedName("udf3")
    @Expose
    private String udf3;
    @SerializedName("udf4")
    @Expose
    private String udf4;
    @SerializedName("udf5")
    @Expose
    private String udf5;
    @SerializedName("udf6")
    @Expose
    private String udf6;

    public final static Parcelable.Creator<FeesAtomResponse> CREATOR = new Creator<FeesAtomResponse>() {
        public FeesAtomResponse createFromParcel(Parcel in) {
            FeesAtomResponse instance = new FeesAtomResponse();
            instance.mmpTxn = ((String) in.readValue((String.class.getClassLoader())));
            instance.merTxn = ((String) in.readValue((String.class.getClassLoader())));
            instance.amt = ((String) in.readValue((String.class.getClassLoader())));
            instance.prod = ((String) in.readValue((String.class.getClassLoader())));
            instance.date = ((String) in.readValue((String.class.getClassLoader())));
            instance.bankTxn = ((String) in.readValue((String.class.getClassLoader())));
            instance.fCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.clientcode = ((String) in.readValue((String.class.getClassLoader())));
            instance.bankName = ((String) in.readValue((String.class.getClassLoader())));
            instance.merchantId = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf9 = ((String) in.readValue((String.class.getClassLoader())));
            instance.discriminator = ((String) in.readValue((String.class.getClassLoader())));
            instance.surcharge = ((String) in.readValue((String.class.getClassLoader())));
            instance.cardNumber = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf4 = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf5 = ((String) in.readValue((String.class.getClassLoader())));
            instance.udf6 = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public FeesAtomResponse[] newArray(int size) {
            return (new FeesAtomResponse[size]);
        }

    };

    public String getMmpTxn() {
        return mmpTxn;
    }

    public void setMmpTxn(String mmpTxn) {
        this.mmpTxn = mmpTxn;
    }

    public String getMerTxn() {
        return merTxn;
    }

    public void setMerTxn(String merTxn) {
        this.merTxn = merTxn;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBankTxn() {
        return bankTxn;
    }

    public void setBankTxn(String bankTxn) {
        this.bankTxn = bankTxn;
    }

    public String getFCode() {
        return fCode;
    }

    public void setFCode(String fCode) {
        this.fCode = fCode;
    }

    public String getClientcode() {
        return clientcode;
    }

    public void setClientcode(String clientcode) {
        this.clientcode = clientcode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getUdf9() {
        return udf9;
    }

    public void setUdf9(String udf9) {
        this.udf9 = udf9;
    }

    public String getDiscriminator() {
        return discriminator;
    }

    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    public String getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String surcharge) {
        this.surcharge = surcharge;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getUdf6() {
        return udf6;
    }

    public void setUdf6(String udf6) {
        this.udf6 = udf6;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mmpTxn);
        dest.writeValue(merTxn);
        dest.writeValue(amt);
        dest.writeValue(prod);
        dest.writeValue(date);
        dest.writeValue(bankTxn);
        dest.writeValue(fCode);
        dest.writeValue(clientcode);
        dest.writeValue(bankName);
        dest.writeValue(merchantId);
        dest.writeValue(udf9);
        dest.writeValue(discriminator);
        dest.writeValue(surcharge);
        dest.writeValue(cardNumber);
        dest.writeValue(udf1);
        dest.writeValue(udf2);
        dest.writeValue(udf3);
        dest.writeValue(udf4);
        dest.writeValue(udf5);
        dest.writeValue(udf6);
    }

    public int describeContents() {
        return 0;
    }

}