package com.edusunsoft.erp.orataro.activities;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.HomeworkPhotoAdapter;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;


public class ViewUploadedHWImageActivityActivity extends AppCompatActivity implements View.OnClickListener, ResponseWebServices {

    String StudentrplyID = "", Note = "", FILETYPE = "", FILEURL = "";
    Context mContext;

    private GridView grv_photos;
    private ImageView img_back, img_menu;
    private HomeworkPhotoAdapter homeworkphoto_ListAdapter;
    private TextView header_text, txt_approve_note;
    LinearLayout lyl_approve, lyl_approve_note;
    CheckBox chk_approve;
    EditText et_approve_note;
    boolean isApprove;
    ImageView img_file;
    File file;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photo_list);
        Initialization();
    }

    private void Initialization() {

        mContext = ViewUploadedHWImageActivityActivity.this;

        img_file = (ImageView) findViewById(R.id.img_file);
        chk_approve = (CheckBox) findViewById(R.id.chk_approve);
        et_approve_note = (EditText) findViewById(R.id.et_approve_note);
        grv_photos = (GridView) findViewById(R.id.grv_photos);
        txt_approve_note = (TextView) findViewById(R.id.txt_approve_note);
        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText("Homework Images");
        img_back = (ImageView) findViewById(R.id.img_back);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        img_menu.setVisibility(View.VISIBLE);
        img_menu.setImageResource(R.drawable.save);
        img_menu.setOnClickListener(this);

        lyl_approve = (LinearLayout) findViewById(R.id.lyl_approve);
        lyl_approve_note = (LinearLayout) findViewById(R.id.lyl_approve_note);

        StudentrplyID = getIntent().getStringExtra("StudentReplayID");
        Note = getIntent().getStringExtra("Note");
        FILETYPE = getIntent().getStringExtra("FileType");
        if (FILETYPE.equalsIgnoreCase("FILE")) {
            grv_photos.setVisibility(View.GONE);
            FILEURL = getIntent().getStringExtra("FileURL");
            if (FILETYPE.equalsIgnoreCase("FILE")) {
                img_file.setVisibility(View.VISIBLE);
                grv_photos.setVisibility(View.GONE);
                FILEURL = getIntent().getStringExtra("FileURL");
                if (FILEURL != null && !FILEURL.contains("null")) {

                    if (FILEURL.contains(".TEXT") || FILEURL.contains(".txt")) {
                        img_file.setImageResource(R.drawable.txt);
                    } else if (FILEURL.contains(".PDF") || FILEURL.contains(".pdf")) {
                        img_file.setImageResource(R.drawable.pdf);
                    } else if (FILEURL.contains(".doc") || FILEURL.contains(".doc") || FILEURL.contains(".docs")) {
                        img_file.setImageResource(R.drawable.doc);
                    } else if (FILEURL.contains(".xlsx") || FILEURL.contains(".xls") || FILEURL.contains(".xlsm")) {
                        img_file.setImageResource(R.drawable.excel_file);
                    }

                }

                img_file.setVisibility(View.VISIBLE);

            } else {
                img_file.setVisibility(View.GONE);
            }
        }

        img_file.setOnClickListener(this);

        isApprove = getIntent().getBooleanExtra("isApproved", false);

        if (Utility.isTeacher(mContext)) {
            et_approve_note.setText(Note);
            if (isApprove) {
                chk_approve.setChecked(true);
            } else {
                chk_approve.setChecked(false);
            }
            lyl_approve.setVisibility(View.VISIBLE);
        } else {
            txt_approve_note.setText(Note);
            txt_approve_note.setVisibility(View.VISIBLE);
            et_approve_note.setVisibility(View.GONE);
            img_menu.setVisibility(View.GONE);
            lyl_approve.setVisibility(View.VISIBLE);
            lyl_approve_note.setVisibility(View.GONE);
        }

        homeworkphoto_ListAdapter = new HomeworkPhotoAdapter(mContext, ServiceResource.GetHwUploadedImageList);
        grv_photos.setAdapter(homeworkphoto_ListAdapter);
        img_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                this.finish();
                break;
            case R.id.img_menu:
                try {
                    SubmitNoteofHomework(StudentrplyID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_file:
                if (Utility.isNetworkAvailable(mContext)) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(FILEURL));
                    mContext.startActivity(i);
                } else {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                }
                saveDownload(FILEURL);
                break;
        }
    }

    private void saveDownload(String url) {
        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//        DownloadManager.Request request = new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//        long id = mgr.enqueue(request);
//        Utility.showToast(mContext.getResources().getString(R.string.downloadstarting), mContext);

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);

    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

    private void SubmitNoteofHomework(String studentrplyID) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.STUDENT_REPLAY_ID,
                studentrplyID));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM,
                chk_approve.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.Note,
                et_approve_note.getText().toString()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SAVE_TEACHER_COMMENT_STUDNET_UPLODED_HOMEWORK,
                ServiceResource.UPLOADHOMEWORK_URL);

    }

    @Override
    public void response(String result, String methodName) {
        Log.d("getResponse", result);
        this.finish();
    }

}
