package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface HomeworkListDataDao {

    @Insert
    void insertHomeworkList(HomeWorkListModel homeWorkListModel);

    @Query("SELECT * from HomeWorkList")
    List<HomeWorkListModel> getHomeworkListList();

    @Query("DELETE  FROM HomeWorkList")
    int deleteHomeworkListItem();

    @Query("delete from HomeWorkList where AssignmentID=:assignementid")
    void deleteHomeworkItemByAssignmentID(String assignementid);


    @Query("UPDATE HomeWorkList SET IsRead= :isread WHERE AssignmentID = :homeworassgnmntid")
    int updateIsReadyByAssignmentID(String homeworassgnmntid, boolean isread);

//    @Query("UPDATE HomeWorkList. SET room_name =:roomName WHERE room_id =:roomId")
//    void updateRoomData(String roomName, int roomId);

}
