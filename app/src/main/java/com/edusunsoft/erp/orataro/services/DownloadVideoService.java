package com.edusunsoft.erp.orataro.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.edusunsoft.erp.orataro.NotificationId;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadVideoService extends IntentService {

	private int result = Activity.RESULT_CANCELED;
	public static final String URL = "urlpath";
	public static final String FILENAME = "filename";
	public static final String FILEPATH = "filepath";
	public static final String RESULT = "examresult";
	public static final String NOTIFICATION = "service receiver";

	private NotificationManager mNotifyManager;
	private Notification.Builder mBuilder;
	int id = 1;
	boolean isFinish = false; 
	
	

	public DownloadVideoService() {
		super("DownloadVideoService");

	}
	//@Override
	//public int onStartCommand(Intent intent, int flags, int startId) {
	//
	//	String urlPath = intent.getStringExtra(URL);
	//	mNotifyManager = (NotificationUtil) getSystemService(Context.NOTIFICATION_SERVICE);
	//	mBuilder = new NotificationCompat.Builder(this);
	//	mBuilder.setContentTitle("Download")
	//	.setContentText("Download in progress")
	//	.setSmallIcon(R.drawable.orataro);
	//
	//	// TODO Auto-generated method stub
	//	//			new ProgressBack().execute();
	//	//			new DownloadFileFromURL().execute(urlPath);
	//
	//	mBuilder.setProgress(100, 0, false);
	//	mNotifyManager.notify(id, mBuilder.build());
	//
	//	int count;
	//	try {
	//		URL url = new URL(urlPath);
	//		URLConnection conection = url.openConnection();
	//		conection.connect();
	//		// this will be useful so that you can show a tipical 0-100% progress bar
	//		int lenghtOfFile = conection.getContentLength();
	//
	//		// download the file
	//		InputStream input = new BufferedInputStream(url.openStream(), 8192);
	//
	//		// Output stream
	//		OutputStream output = new FileOutputStream("/sdcard/demo.mp4");
	//
	//		byte data[] = new byte[1024];
	//
	//		long total = 0;
	//
	//		while ((count = input.read(data)) != -1) {
	//			total += count;
	//			// publishing the progress....
	//			// After this onProgressUpdate will be called
	//			publishProgress(""+(int)((total*100)/lenghtOfFile));
	//
	//			// writing data to file
	//			output.write(data, 0, count);
	//		}
	//
	//		// flushing output
	//		output.flush();
	//
	//		// closing streams
	//		output.close();
	//		input.close();
	//
	//	} catch (Exception e) {
	//		Log.e("Error: ", e.getMessage());
	//	}
	//	mBuilder.setContentText("Download complete");
	//	// Removes the progress bar
	//	mBuilder.setProgress(0, 0, false);
	//	mNotifyManager.notify(id, mBuilder.build());
	//	// TODO Auto-generated method stub
	//	return android.app.Service.START_STICKY;
	//}
	@Override
	protected void onHandleIntent(Intent intent) {
		id = NotificationId.getID();
		String urlPath = intent.getStringExtra(URL);
		mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder = new Notification.Builder(this);
		mBuilder.setContentTitle("Download")
		.setContentText("Download in progress")
		.setSmallIcon(R.drawable.notification_icon);

		// TODO Auto-generated method stub
		//			new ProgressBack().execute();
		//			new DownloadFileFromURL().execute(urlPath);

		mBuilder.setProgress(100, 0, false);
		mNotifyManager.notify(id, mBuilder.build());

		int count;
		try {
			String fileName  = System.currentTimeMillis()+".mp4";
			String[] fNamearray= urlPath.split("/");
			if(fNamearray != null && fNamearray.length>0){
				fileName = fNamearray[fNamearray.length-1];

			}
			java.net.URL url = new URL(urlPath);
			URLConnection conection = url.openConnection();
			conection.connect();
			// this will be useful so that you can show a tipical 0-100% progress bar
			int lenghtOfFile = conection.getContentLength();

			// download the file
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			// Output stream
			OutputStream output = new FileOutputStream(Utility.getVideoFilename(fileName));

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				// publishing the progress....
				// After this onProgressUpdate will be called
				publishProgress(""+(int)((total*100)/lenghtOfFile));
				if((int)((total*100)/lenghtOfFile) == 100){
					isFinish =true;
				}

				// writing data to file
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();

		} catch (Exception e) {
			Log.e("Error: ", e.getMessage());
		}
		mBuilder.setContentText("Download complete");
		// Removes the progress bar
		mBuilder.setProgress(0, 0, false);
		mNotifyManager.notify(id, mBuilder.build());

		
		
	  

	}
	private void publishProgress(String string) {
		// TODO Auto-generated method stub
		mBuilder.setContentTitle("Download")
		.setContentText("Download in progress "+ Integer.parseInt(string)+" %")
		.setSmallIcon(R.drawable.notification_icon);
		mBuilder.setProgress(100, Integer.parseInt(string), false);
		mNotifyManager.notify(id, mBuilder.build());
	}
	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		//        private ProgressDialog pDialog;

		/**
		 * Before starting background thread
		 * Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//            pDialog = new ProgressDialog(VideoViewActivity.this);
			//            pDialog.setMessage("Downloading file. Please wait...");
			//            pDialog.setIndeterminate(false);
			//            pDialog.setMax(100);
			//            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			//            pDialog.setCancelable(true);
			//            pDialog.show();
			mBuilder.setProgress(100, 0, false);
			mNotifyManager.notify(id, mBuilder.build());
			//            showDialog(progress_bar_type);
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				java.net.URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// this will be useful so that you can show a tipical 0-100% progress bar
				int lenghtOfFile = conection.getContentLength();

				// download the file
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream
				OutputStream output = new FileOutputStream("/sdcard/demo.mp4");

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress(""+(int)((total*100)/lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			mBuilder.setContentTitle("Download")
			.setContentText("Download in progress "+ Integer.parseInt(progress[0])+" %")
			.setSmallIcon(R.drawable.notification_icon);
			mBuilder.setProgress(100, Integer.parseInt(progress[0]), false);
			mNotifyManager.notify(id, mBuilder.build());
			//            pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task
		 * Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			//            dismissDialog(progress_bar_type);
			mBuilder.setContentText("Download complete");
			// Removes the progress bar
			mBuilder.setProgress(0, 0, false);
			mNotifyManager.notify(id, mBuilder.build());
			//        	pDialog.dismiss();
			// Displaying downloaded image into image view
			// Reading image path from sdcard
			String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
		}

	}
public void threadrun(boolean isRun){
	if(isRun){
	new Handler().postDelayed(new Runnable() {

		@Override
		public void run() {
			

		}
	}, 3000);
	}
}

}
