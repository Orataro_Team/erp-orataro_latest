package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.GroupMemberInterface;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.DivisionAdapter;
import com.edusunsoft.erp.orataro.adapter.GroupMemberAdapter;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.database.ProjectListModel;
import com.edusunsoft.erp.orataro.model.DivisionModel;
import com.edusunsoft.erp.orataro.model.GroupMemberModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * create project
 *
 * @author admin
 */
public class CreateProjectActivity extends Activity implements OnClickListener, ResponseWebServices, GroupMemberInterface {

    Context mContext;
    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;
    private TextView txtgroupmember, txtgroupstudent;
    TextView txtStartDate, txtEndDate;
    EditText edtProjectTitle, edtdefination;
    private Calendar c;
    private int myear;
    private int month;
    private int day;
    private int mHour;
    private int mMinute;
    private String todayDate;

    private int myearEnd;
    private int monthEnd;
    private int dayEnd;
    private int mHourEnd;
    private int mMinuteEnd;
    private String todayDateEnd;
    ImageView selectall;

    static final int DATE_DIALOG_ID = 111;
    static final int ENDDATE_DIALOG_ID = 112;
    boolean isEdit = false;
    ProjectListModel projectModel;

    LinearLayout ll_groupmember;

    protected boolean isFirstTimeGroupMember;
    private GroupMemberAdapter GroupmemberAdapter;
    protected ArrayList<GroupMemberModel> jobAllGroupMembers;
    private ArrayList<GroupMemberModel> localAddGroupMemberModels;
    private ArrayList<GroupMemberModel> tempGroupMemberList;
    ArrayList<GroupMemberModel> groupMemberList, groupMemberStudent;
    boolean isteacher = true;
    RadioButton rbTeacher, rbStudent;
    RadioGroup rbStudentTeacherGroup;
    boolean isTeacher = true;
    String gradeIdStr = "", divisionIdStr = "";
    boolean isStandard = true;
    private ArrayList<GroupMemberModel> selectedSkillList;
    private String groupMemberIdStr = "", groupMemberStudentStr = "";
    String FilteredMemberList = "";
    LinearLayout ll_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_project);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = CreateProjectActivity.this;

        txtgroupstudent = (TextView) findViewById(R.id.txtgroupstudent);
        rbStudentTeacherGroup = (RadioGroup) findViewById(R.id.rbStudentTeacherGroup);
        rbTeacher = (RadioButton) findViewById(R.id.rbTeacher);
        rbStudent = (RadioButton) findViewById(R.id.rbStudent);

        txtgroupmember = (TextView) findViewById(R.id.txtgroupmember);
        edtProjectTitle = (EditText) findViewById(R.id.edt_title);
        txtStartDate = (TextView) findViewById(R.id.txtstartdate);
        txtEndDate = (TextView) findViewById(R.id.txtenddate);
        edtdefination = (EditText) findViewById(R.id.edt_defination);

        ll_groupmember = (LinearLayout) findViewById(R.id.ll_groupmember);

        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        txtHeader = (TextView) findViewById(R.id.header_text);

        if (getIntent() != null) {

            isEdit = getIntent().getBooleanExtra("isEdit", false);

            if (isEdit) {

                projectModel = (ProjectListModel) getIntent().getSerializableExtra("model");

                if (Utility.isNetworkAvailable(mContext)) {

                    projectData();
                    paarseprojectbyid(Utility.readFromFile(projectModel.getProjectID(), mContext));

                } else {

                    paarseprojectbyid(Utility.readFromFile(projectModel.getProjectID(), mContext));

                }

            }

        }

        try {
            if (isEdit) {
                txtHeader.setText(getResources().getString(R.string.editproject) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
            } else {
                txtHeader.setText(getResources().getString(R.string.createproject) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.isNetworkAvailable(mContext)) {
            getWallAllTeacher();
            parseteachenamemember(Utility.readFromFile(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, mContext));
        } else {
            parseteachenamemember(Utility.readFromFile(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, mContext));
        }

        if (Utility.isNetworkAvailable(mContext)) {
            StandardList();
            parsestdmethodname(Utility.readFromFile(ServiceResource.STANDERD_METHODNAME, mContext));
        } else {
            parsestdmethodname(Utility.readFromFile(ServiceResource.STANDERD_METHODNAME, mContext));
        }

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setImageResource(R.drawable.save);
        imgRightHeader.setVisibility(View.VISIBLE);

        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        todayDate = (day < 10 ? "0" + day : day)
                + "/"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "/" + myear;

        myearEnd = c.get(Calendar.YEAR);
        monthEnd = c.get(Calendar.MONTH);
        dayEnd = c.get(Calendar.DAY_OF_MONTH);
        mHourEnd = c.get(Calendar.HOUR_OF_DAY);
        mMinuteEnd = c.get(Calendar.MINUTE);

        todayDateEnd = (dayEnd < 10 ? "0" + dayEnd : dayEnd)
                + "/"
                + ((monthEnd + 1) < 10 ? "0" + (monthEnd + 1)
                : (monthEnd + 1))
                + "/" + myearEnd;

        imgRightHeader.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        imgLeftHeader.setOnClickListener(this);
        txtgroupmember.setOnClickListener(this);
        txtStartDate.setOnClickListener(this);
        txtEndDate.setOnClickListener(this);
        txtgroupstudent.setOnClickListener(this);

        if (ServiceResource.USER_TEACHER_INT != new UserSharedPrefrence(mContext).getLoginModel().getUserType()) {
            txtHeader.setText(getResources().getString(R.string.ProjectDetail));
            imgRightHeader.setVisibility(View.INVISIBLE);
            ll_save.setVisibility(View.GONE);
            txtgroupmember.setVisibility(View.GONE);
            rbStudentTeacherGroup.setVisibility(View.GONE);
            edtProjectTitle.setEnabled(false);
            edtdefination.setEnabled(false);
            txtStartDate.setEnabled(false);
            txtEndDate.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (R.id.img_menu == v.getId()) {
            if (Utility.isNetworkAvailable(mContext)) {

                if (!Utility.isNull(edtProjectTitle.getText().toString())) {
                    edtProjectTitle.setError(mContext.getResources().getString(R.string.enterprojecttitle));
                } else if (!Utility.isNull(txtStartDate.getText().toString())) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.enterstartdate));
                } else if (!Utility.isNull(txtEndDate.getText().toString())) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.enterenddate));
                } else if (!Utility.isNull(edtdefination.getText().toString())) {
                    edtdefination.setError(mContext.getResources().getString(R.string.enterprojectdefinition));
                } else {
                    addProject();
                }

            } else {
                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }
        }
        if (R.id.ll_save == v.getId()) {

            if (Utility.isNetworkAvailable(mContext)) {

                if (!Utility.isNull(edtProjectTitle.getText().toString())) {
                    edtProjectTitle.setError(mContext.getResources().getString(R.string.enterprojecttitle));
                } else if (!Utility.isNull(txtStartDate.getText().toString())) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.enterstartdate));
                } else if (!Utility.isNull(txtEndDate.getText().toString())) {
                    Utility.toast(mContext, mContext.getResources().getString(R.string.enterenddate));
                } else if (!Utility.isNull(edtdefination.getText().toString())) {
                    edtdefination.setError(mContext.getResources().getString(R.string.enterprojectdefinition));
                } else {
                    addProject();
                }

            } else {
                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }
        }
        if (R.id.img_home == v.getId()) {
            Utility.ISLOADPROJECT = false;
            finish();
        }
        if (R.id.txtstartdate == v.getId()) {
            showDialog(DATE_DIALOG_ID);
        }
        if (R.id.txtenddate == v.getId()) {
            showDialog(ENDDATE_DIALOG_ID);
        }
        if (R.id.txtgroupmember == v.getId()) {
            isteacher = true;
            showListDialogForGroupMember(mContext, groupMemberList);
        }
        if (R.id.txtgroupstudent == v.getId()) {
            isteacher = false;
            showStanderdPopup();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADPROJECT = false;
    }

    /***
     * get WAll All Teacher List
     */
    public void getWallAllTeacher() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME,
                ServiceResource.FRIENDS_URL);
    }

    /**
     * get Wall All Student List
     */

    public void getWallAllStudentList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeIdStr));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, divisionIdStr));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETSTUDENTSLISTNAMEANDMEMBERID_METHODNAME, ServiceResource.FRIENDS_URL);
    }

    /**
     * webservice call for standard list
     */
    public void StandardList() {
        isStandard = true;
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.TYPE, ServiceResource.STANDERD_WSCALL_TYPE_GRADE));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, null));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STANDERD_METHODNAME, ServiceResource.STANDERD_URL);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog datePickerDialog;

        switch (id) {
            case DATE_DIALOG_ID:
                datePickerDialog = new DatePickerDialog(this, datePickerListener, myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;
            case ENDDATE_DIALOG_ID:
                datePickerDialog = new DatePickerDialog(this, datePickerListenerEnd, myearEnd, monthEnd, dayEnd);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;
        }
        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            txtStartDate.setText((day < 10 ? "0" + day : day)
                    + "-"
                    + ((month + 1) < 10 ? "0" + (month + 1)
                    : (month + 1))
                    + "-" + myear);
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListenerEnd = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myearEnd = selectedYear;
            monthEnd = selectedMonth;
            dayEnd = selectedDay;

            txtEndDate.setText((dayEnd < 10 ? "0" + dayEnd : dayEnd)
                    + "-"
                    + ((monthEnd + 1) < 10 ? "0" + (monthEnd + 1)
                    : (monthEnd + 1))
                    + "-" + myearEnd);
        }
    };

    /**
     * project data from project id
     */
    public void projectData() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PROJECT_PROJECTID, projectModel.getProjectID()));
        Log.d("isEditrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PRJECTDATABYID_METHODNAME, ServiceResource.PROJECT_URL);
    }

    /**
     * get response for all Webservice
     */
    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.PRJECTDATABYID_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, projectModel.getProjectID(), mContext);
            paarseprojectbyid(result);
        } else if (ServiceResource.STANDERD_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parsestdmethodname(result);
        } else if (ServiceResource.WALLMEMBERS_METHODNAME.equalsIgnoreCase(methodName)
                || ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)
        ) {
            Utility.writeToFile(result, methodName, mContext);
            parseteachenamemember(result);
        } else if (ServiceResource.GETSTUDENTSLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)) {
            try {

                JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
                groupMemberStudent = new ArrayList<GroupMemberModel>();
                GroupMemberModel model;

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    model = new GroupMemberModel();
                    model.setMemberId(hJsonObject.getString(ServiceResource.MEMBERID));
                    model.setMemberName(hJsonObject.getString(ServiceResource.FULLNAMEPARAM));
                    groupMemberStudent.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            showListDialogForGroupMember(mContext, groupMemberStudent);
        } else if (ServiceResource.ADDPROJECT_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.ISLOADPROJECT = true;
            finish();
        } else if (ServiceResource.REMOVE_GROUPMEMBERS_METHODNAME.equalsIgnoreCase(methodName)) {
            projectData();
        }
    }

    private void parsestdmethodname(String result) {

        if (isStandard) {
            JSONArray hJsonArray;
            try {
                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);
                    Global.StandardModels = new ArrayList<StandardModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        StandardModel model = new StandardModel();
                        model.setStandardName(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADENAME));
                        model.setStandrdId(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADEID));
                        Global.StandardModels.add(model);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            JSONArray hJsonArray;
            try {

                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);
                    Global.divisionModels = new ArrayList<DivisionModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        DivisionModel model = new DivisionModel();
                        model.setDivisionId(hJsonObject.getString(ServiceResource.STANDARD_DIVISIONID));
                        model.setDivisionName(hJsonObject.getString(ServiceResource.STANDARD_DIVISIONNAME));
                        Global.divisionModels.add(model);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            showStanderdPopup();

        }

    }

    private void parseteachenamemember(String result) {

        try {

            JSONArray hJsonArray = new JSONArray(result);
            groupMemberList = new ArrayList<GroupMemberModel>();
            GroupMemberModel model;

            for (int i = 0; i < hJsonArray.length(); i++) {
                JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                model = new GroupMemberModel();
                model.setMemberId(hJsonObject.getString(ServiceResource.MEMBERID));
                model.setMemberName(hJsonObject.getString(ServiceResource.FULLNAMEPARAM));
                groupMemberList.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void paarseprojectbyid(String result) {

        JSONArray hJsonArray, memberArray;

        try {

            JSONObject jsonObj = new JSONObject(result);
            hJsonArray = jsonObj.getJSONArray(ServiceResource.TABLE);
            memberArray = jsonObj.optJSONArray(ServiceResource.TABLE1);

            Log.d("getProjectListarray", memberArray.toString());

            for (int i = 0; i < hJsonArray.length(); i++) {

                JSONObject innerObj = hJsonArray.getJSONObject(i);
                projectModel.setProjectID(innerObj.getString(ServiceResource.PROJECT_PROJECTID));
                projectModel.setProjectTitle(innerObj.getString(ServiceResource.PROJECT_TITLE));
                projectModel.setProjectDefination(innerObj.getString(ServiceResource.PROJECT_DEFINATION));
                projectModel.setProjectScope(innerObj.getString(ServiceResource.PROJECT_PROJECTSCOPE));
                String startdate = Utility.getDate(
                        Long.valueOf(innerObj.getString(ServiceResource.PROJECT_STARTDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "dd-MM-yyyy");
                projectModel.setStartDate(startdate);
                String enddate = Utility.getDate(
                        Long.valueOf(innerObj.getString(ServiceResource.PROJECT_ENDDATE)
                                .replace("/Date(", "").replace(")/", "")),
                        "dd-MM-yyyy");

                projectModel.setEndDate(enddate);
                projectModel.setGroupID(innerObj.getString(ServiceResource.PROJECT_GROUPID));


            }

            if (memberArray != null && memberArray.length() > 0) {

                selectedSkillList = new ArrayList<GroupMemberModel>();

                for (int i = 0; i < memberArray.length(); i++) {

                    JSONObject innerObj = memberArray.getJSONObject(i);
                    GroupMemberModel model = new GroupMemberModel();
                    model.setGropuMemberID(innerObj.getString(ServiceResource.GROPUMEMBERID));
                    model.setMemberId(innerObj.getString(ServiceResource.MEMBERID));
                    model.setMemberName(innerObj.getString(ServiceResource.MEMBERFULLNAME));
                    model.setMemberProfileCode(innerObj.getString(ServiceResource.MEMBERPROFILECODE));
                    model.setProfilePicture(innerObj.getString(ServiceResource.GROUP_PROFILEPICTURE));
                    model.setMemberRole(innerObj.getString(ServiceResource.MEMBERROLE));
                    selectedSkillList.add(model);

                    viewGroupMember(selectedSkillList);


                }

            } else {

                selectedSkillList = new ArrayList<GroupMemberModel>();

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        fillData();

//        viewGroupMember(selectedSkillList);

    }

    /**
     * sort array list from date
     *
     * @author admin
     */

    public class CustomComparator implements Comparator<GroupMemberModel> {
        @Override
        public int compare(GroupMemberModel o1, GroupMemberModel o2) {
            return o1.getMemberName().compareTo(o2.getMemberName());
        }
    }

    /**
     * fill data on edittetext when it in edit mode
     */
    public void fillData() {
        try {
            edtProjectTitle.setText(projectModel.getProjectTitle());
            edtdefination.setText(projectModel.getProjectDefination());
            txtStartDate.setText(projectModel.getStartDate());
            txtEndDate.setText(projectModel.getEndDate());
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
    }

    /**
     * show dialog for groupmembers
     *
     * @param context
     * @param AllMemberList
     */

    public void showListDialogForGroupMember(Context context, final ArrayList<GroupMemberModel> AllMemberList) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customelistdialog);

        TextView dialogTitle = (TextView) dialog
                .findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialog.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        final ListView lv = (ListView) dialog.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialog
                .findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialog
                .findViewById(R.id.custome_Dialog_cancle_btn);

        selectall = (ImageView) dialog
                .findViewById(R.id.selectall);
        selectall.setVisibility(View.VISIBLE);

//        CheckBox chkSelectAll = (CheckBox) dialog.findViewById(R.id.selectall);
//        chkSelectAll.setVisibility(View.VISIBLE);
        if (isteacher) {
            edtSearch.setHint(mContext.getResources().getString(R.string.enterprojectTeacher));
            dialogTitle.setText(mContext.getResources().getString(R.string.ProjectTeacher));
        } else {
            edtSearch.setHint(mContext.getResources().getString(R.string.enterprojectmember));
            dialogTitle.setText(mContext.getResources().getString(R.string.ProjectStudents));
        }

        if (isEdit) {
            localAddGroupMemberModels = reSetAllGroupMember(AllMemberList);
            if (localAddGroupMemberModels != null && localAddGroupMemberModels.size() > 0) {
                GroupmemberAdapter = new GroupMemberAdapter(mContext, modifyGroupMemberVo(
                        localAddGroupMemberModels, selectedSkillList),
                        CreateProjectActivity.this);
                lv.setAdapter(GroupmemberAdapter);
                GroupmemberAdapter.notifyDataSetChanged();
            }
        } else {
            localAddGroupMemberModels = reSetAllGroupMember(AllMemberList);
            if (localAddGroupMemberModels != null && localAddGroupMemberModels.size() > 0) {
                if (isFirstTimeGroupMember) {
                    GroupmemberAdapter = new GroupMemberAdapter(mContext,
                            modifyGroupMemberVo(localAddGroupMemberModels,
                                    new ArrayList<GroupMemberModel>()),
                            CreateProjectActivity.this);
                } else {
                    GroupmemberAdapter = new GroupMemberAdapter(mContext, modifyGroupMemberVo(
                            localAddGroupMemberModels, tempGroupMemberList),
                            CreateProjectActivity.this);
                }
                lv.setAdapter(GroupmemberAdapter);
                GroupmemberAdapter.notifyDataSetChanged();
            }
        }
        // select all functionality to select All Student simultaneously
        selectall.setOnClickListener(v -> {

            GroupmemberAdapter.SelectAll(localAddGroupMemberModels, selectall);

        });

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                if (GroupmemberAdapter != null) {
                    GroupmemberAdapter.filter(text);
                }
            }
        });


        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                edtSearch.setText("");
                if (isteacher) {
                    ArrayList<GroupMemberModel> selectedGroupMemberList = new ArrayList<GroupMemberModel>();
                    selectedGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    tempGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    isFirstTimeGroupMember = false;
                    String selectedGroupMemberStr = "";
                    groupMemberIdStr = "";
                    if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                        Collections.sort(selectedGroupMemberList, new CustomComparator());
                        for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                            selectedGroupMemberStr = selectedGroupMemberStr
                                    + selectedGroupMemberList.get(i).getMemberName();
                            groupMemberIdStr = groupMemberIdStr +
                                    selectedGroupMemberList.get(i).getMemberId();

                            if (selectedGroupMemberList.size() > 1) {
                                if (i != selectedGroupMemberList.size() - 1) {
                                    selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                    groupMemberIdStr = groupMemberIdStr + ",";
                                }
                            }
                        }
                    }
                    txtgroupmember.setText(selectedGroupMemberStr);
                } else {
                    ArrayList<GroupMemberModel> selectedGroupMemberList = new ArrayList<GroupMemberModel>();
                    selectedGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    tempGroupMemberList = getSelectedGroupMember(jobAllGroupMembers);
                    isFirstTimeGroupMember = false;
                    String selectedGroupMemberStr = "";
                    groupMemberStudentStr = "";
                    if (selectedGroupMemberList != null && selectedGroupMemberList.size() > 0) {
                        Collections.sort(selectedGroupMemberList, new CustomComparator());
                        for (int i = 0; i < selectedGroupMemberList.size(); i++) {
                            selectedGroupMemberStr = selectedGroupMemberStr
                                    + selectedGroupMemberList.get(i).getMemberName();
                            groupMemberStudentStr = groupMemberStudentStr +
                                    selectedGroupMemberList.get(i).getMemberId();
                            if (selectedGroupMemberList.size() > 1) {
                                if (i != selectedGroupMemberList.size() - 1) {
                                    selectedGroupMemberStr = selectedGroupMemberStr + ",";
                                    groupMemberStudentStr = groupMemberStudentStr + ",";
                                }
                            }
                        }
                    }
                    txtgroupstudent.setText(selectedGroupMemberStr);
                }
            }
        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    /**
     * set selected false for all groupmember
     *
     * @param addGroupMemberModels
     * @return
     */
    public ArrayList<GroupMemberModel> reSetAllGroupMember(
            ArrayList<GroupMemberModel> addGroupMemberModels) {

        ArrayList<GroupMemberModel> groupMemberModels = null;

        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<GroupMemberModel>();
            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                addGroupMemberModels.get(i).setSelected(false);
                groupMemberModels.add(addGroupMemberModels.get(i));
            }
        }

        return groupMemberModels;
    }


    /**
     * set selected false selected schoolgroups member
     */
    public ArrayList<GroupMemberModel> modifyGroupMemberVo(
            ArrayList<GroupMemberModel> allGroupMember,
            ArrayList<GroupMemberModel> selectedGroupMember) {
        ArrayList<GroupMemberModel> addSkillModels = allGroupMember;

        if (selectedGroupMember != null && selectedGroupMember.size() > 0) {
            if (allGroupMember != null && allGroupMember.size() > 0) {
                for (int j = 0; j < selectedGroupMember.size(); j++) {
                    for (int i = 0; i < allGroupMember.size(); i++) {
                        if (selectedGroupMember.get(j).getMemberId().equals(allGroupMember.get(i).getMemberId())) {
                            allGroupMember.remove(i);
                            break;
                        }
                    }
                }
            }
        } else {
            return allGroupMember;
        }

        return addSkillModels;
    }

    /**
     * get selected schoolgroups member
     *
     * @param addGroupMemberModels
     * @return
     */
    public ArrayList<GroupMemberModel> getSelectedGroupMember(
            ArrayList<GroupMemberModel> addGroupMemberModels) {
        ArrayList<GroupMemberModel> groupMemberModels = null;
        if (addGroupMemberModels != null && addGroupMemberModels.size() > 0) {
            groupMemberModels = new ArrayList<GroupMemberModel>();
            for (int i = 0; i < addGroupMemberModels.size(); i++) {
                if (addGroupMemberModels.get(i).isSelected()) {
                    groupMemberModels.add(addGroupMemberModels.get(i));
                }
            }
        }

        return groupMemberModels;

    }

    /**
     * get all schoolgroups member modifyed
     */
    /*
     * (non-Javadoc)
     * @see GroupMemberInterface#getAllGroupMember(int, java.util.ArrayList)
     */
    @Override
    public void getAllGroupMember(int pos, ArrayList<GroupMemberModel> addGroupMemberModels) {
        jobAllGroupMembers = addGroupMemberModels;
    }

    /**
     * get student list webservice
     */
    public void StudentList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STUDENTLIST_METHODNAME, ServiceResource.PROJECT_URL);
    }

    /**
     * add project webservice
     */
    public void addProject() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.PROJECT_PROJECTID, projectModel.getProjectID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.PROJECT_PROJECTID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_PROJECTSTARTDATE, Utility.dateFormate(txtStartDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_PROJECTENDDATE, Utility.dateFormate(txtEndDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_PROJECTTITLE, edtProjectTitle.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_PROJECTDEFINETION, edtdefination.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_PROJECTSCOPE, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        Log.d("getMemberlist123", groupMemberStudentStr);
        Log.d("getMemberlist123456", groupMemberIdStr);

        groupMemberIdStr = groupMemberStudentStr + "," + groupMemberIdStr;

        Log.d("getMemberlist123456789", groupMemberIdStr);

        List<String> list = Arrays.asList(groupMemberIdStr.split(","));

        Log.d("listtttt", list.toString());

        if (list.contains(new UserSharedPrefrence(mContext).getLoginModel().getMemberID())) {

            FilteredMemberList = groupMemberIdStr.replace((new UserSharedPrefrence(mContext).getLoginModel().getMemberID()), "");

        } else {

            FilteredMemberList = groupMemberIdStr;

        }

        arrayList.add(new PropertyVo(ServiceResource.ADDPROJECT_GROUPMEMBERS, FilteredMemberList));

        Log.d("projectrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDPROJECT_METHODNAME, ServiceResource.PROJECT_URL);

    }

    /**
     * schoolgroups member view
     */

    public void viewGroupMember(ArrayList<GroupMemberModel> selectedProjectList) {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        if (!selectedProjectList.isEmpty()) {

        ll_groupmember.setVisibility(View.VISIBLE);
        ll_groupmember.removeAllViews();

        for (int i = 0; i < selectedProjectList.size(); i++) {

            View convertView = layoutInfalater.inflate(R.layout.groupmemberlayout,
                    null);
            ImageView img_profile = (ImageView) convertView.findViewById(R.id.memberprofilepic);
            TextView txtMemberName = (TextView) convertView.findViewById(R.id.txtmembername);
            TextView txtMembertype = (TextView) convertView.findViewById(R.id.txtmembertype);
            final Button btnremove = (Button) convertView.findViewById(R.id.btn_remove);
            btnremove.setTag("" + i);

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1 + selectedProjectList.get(i).getProfilePicture())
                        .apply(options)
                        .into(img_profile);

            } catch (Exception e) {

                e.printStackTrace();

            }

            txtMemberName.setText(selectedProjectList.get(i).getMemberName());
            txtMembertype.setText(selectedProjectList.get(i).getMemberRole());

            btnremove.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    int pos = Integer.valueOf((String) btnremove.getTag());
                    removeMember(selectedProjectList.get(pos).getGropuMemberID());

                }

            });

            ll_groupmember.addView(convertView);

        }

//        }

//        else {
//
//            ll_groupmember.setVisibility(View.GONE);
//
//        }

    }

    /**
     * remove project member from project id
     *
     * @param groupMemberId
     */

    public void removeMember(String groupMemberId) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.GROUPMEMBERID, groupMemberId));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.REMOVE_GROUPMEMBERS_METHODNAME, ServiceResource.GROUP_URL);
    }


    /**
     * getgradelist from gradeid
     *
     * @param gradeId
     */
    public void gradeList(String gradeId) {
        isStandard = false;
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.GRADEID, gradeId));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
        arrayList.add(new PropertyVo(ServiceResource.TEACHERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.TYPE,
                ServiceResource.STANDERD_WSCALL_TYPE_DIVISION));
        arrayList.add(new PropertyVo(ServiceResource.SUBJECTID, null));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STANDERD_METHODNAME, ServiceResource.STANDERD_URL);
    }

    /**
     * standar popup with multiple selection
     */


    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(mContext);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);

        TextView dialogTitle = (TextView) dialogStandard
                .findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard
                .findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard
                .findViewById(R.id.custome_Dialog_cancle_btn);
        //		lv.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        btnCancle.setVisibility(View.GONE);
        btnDone.setVisibility(View.GONE);

        if (isStandard) {
            dialogTitle.setText(mContext.getResources().getString(R.string.selectstandard));

            if (Global.StandardModels != null
                    && Global.StandardModels.size() > 0) {

                StanderdAdapter adapter = new StanderdAdapter(mContext,
                        Global.StandardModels, true);
                lv.setAdapter(adapter);
            }

        } else {
            dialogTitle.setText(mContext.getResources().getString(R.string.selectdivision));
            if (Global.divisionModels != null
                    && Global.divisionModels.size() > 0) {
                DivisionAdapter adapter = new DivisionAdapter(mContext,
                        Global.divisionModels);
                lv.setAdapter(adapter);
            } else {

            }


        }
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                dialogStandard.dismiss();
                if (isStandard) {
                    gradeIdStr = Global.StandardModels.get(position).getStandrdId();
                    gradeList(Global.StandardModels.get(position).getStandrdId());
                } else {
                    isStandard = true;
                    divisionIdStr = Global.divisionModels.get(position).getDivisionId();
                    getWallAllStudentList();
                }
            }
        });

        edtSearch.setVisibility(View.GONE);

        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
            }
        });

        dialogStandard.show();

    }

    public ArrayList<GroupMemberModel> listAllTrueFalse(ArrayList<GroupMemberModel> allSkillVo, boolean istrue) {
        for (int i = 0; i < allSkillVo.size(); i++) {
            allSkillVo.get(i).setSelected(istrue);
        }
        return allSkillVo;
    }
}
