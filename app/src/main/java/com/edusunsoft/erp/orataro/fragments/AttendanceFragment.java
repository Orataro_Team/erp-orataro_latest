package com.edusunsoft.erp.orataro.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.AttendaceTeacherAdapter;
import com.edusunsoft.erp.orataro.adapter.AttendanceStudentAdapter;
import com.edusunsoft.erp.orataro.customeview.NumberPicker;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.AttendanceModelABPS;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class AttendanceFragment extends Fragment implements ResponseWebServices, OnItemSelectedListener, OnClickListener {

    ListView lvattendance;
    ImageView imgPrevious, imgNext, imgPreviousday, imgnextday;
    TextView txtmonth, txtday;
    String[] monthArray = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY",
            "JUNE", "JULY", "AUGUST", "SEPTMBER", "OCTOBER", "NOVEMBER",
            "DECEMBER"};


    private Calendar c;
    private int myear;
    private int month;
    private int day;
    int[] daycount;
    Spinner spnstandard;
    ArrayList<String> standardArray;
    Context mContext;
    private Dialog numPickerDialog;
    LinearLayout ll_day;
    String startTime = "";
    String EndTime = "";
    int minVal, maxValue;
    private String[] _month;
    private NumberPicker np_days;
    private NumberPicker np_month;
    private NumberPicker np_year;
    String gradeIdStr = "", divisionIdStr = "";
    String monthstrvalue = "", yearStrval = "", dateStrval = "";
    AttendaceTeacherAdapter adapter;
    String currenttxtdate = "";
    Button btn_save;
    String days[] = {"Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday"};

    boolean isWorkingDay, isCompareDate, isCompareDateForNextDate;

    int totalStudents, preasentStudents, abcentStudent, onLeaveStudent;
    CheckBox chk_isWorking;
    String studentDataStr = "";
    LinearLayout ll_working;
    TextView txtdaystudent;
    Button btn_changeView;
    boolean isQuickView;
    View quickview;
    EditText edt_absent, edtleave, edt_seakleave;
    Button ganratereport;
    String edtabsantstr = "";
    String edtLeaveStr = "";
    String edtleavestr = "";
    String edtseakleavestr = "";
    String edtSeaklleavestr = "";
    ArrayList<String> RollNolist = new ArrayList<>();
    private int stYear, stDay, stMonth;
    public File file;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.attendancefragment, null);
        mContext = getActivity();

        daycount = new int[12];
        c = Calendar.getInstance();

        ll_day = (LinearLayout) v.findViewById(R.id.ll_day);
        imgPrevious = (ImageView) v.findViewById(R.id.imgPrevious);
        imgNext = (ImageView) v.findViewById(R.id.imgNext);
        txtmonth = (TextView) v.findViewById(R.id.txtmonth);
        txtday = (TextView) v.findViewById(R.id.txtday);
        imgPreviousday = (ImageView) v.findViewById(R.id.imgPreviousday);
        imgnextday = (ImageView) v.findViewById(R.id.imgnextday);
        lvattendance = (ListView) v.findViewById(R.id.lvattendance);
        spnstandard = (Spinner) v.findViewById(R.id.spnstandard);
        btn_save = (Button) v.findViewById(R.id.btn_save);
        chk_isWorking = (CheckBox) v.findViewById(R.id.chb_working);
        ll_working = (LinearLayout) v.findViewById(R.id.ll_working);
        txtdaystudent = (TextView) v.findViewById(R.id.txtdaystudent);
        btn_changeView = (Button) v.findViewById(R.id.btn_changeView);
        quickview = v.findViewById(R.id.quickview);
        edt_absent = (EditText) v.findViewById(R.id.edt_absent);
        edtleave = (EditText) v.findViewById(R.id.edtleave);
        edt_seakleave = (EditText) v.findViewById(R.id.edt_seakleave);
        ganratereport = (Button) v.findViewById(R.id.ganratereport);

        btn_changeView.setVisibility(View.VISIBLE);
        lvattendance.setVisibility(View.VISIBLE);
        quickview.setVisibility(View.GONE);

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Attendance) + "(" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        btn_save.setOnClickListener(this);
        btn_changeView.setOnClickListener(this);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String formattedDate = df.format(c.getTime());


        if (Utility.dateToMilliSeconds(formattedDate, "dd-MM-yyyy") > Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", ""))
                && Utility.dateToMilliSeconds(formattedDate, "dd-MM-yyyy") < Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", ""))
                || Utility.dateToMilliSeconds(formattedDate, "dd-MM-yyyy") == Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", ""))) {

            txtday.setText(Utility.getDate(System.currentTimeMillis(), "dd-MMM-yyyy"));

            txtdaystudent.setText(Utility.getDate(System.currentTimeMillis(), "MMM-yyyy"));

            String[] tempdate = txtday.getText().toString().split("-");

            monthstrvalue = getMonth(tempdate[1]) + "";
            yearStrval = tempdate[2] + "";

            //yyyy-MM-dd
            String tempMonth = (getMonthNumber(tempdate[1]) + 1) < 10 ?
                    "0" + (getMonthNumber(tempdate[1]) + 1) : (getMonthNumber(tempdate[1]) + 1) + "";
            String tempday = Integer.valueOf(tempdate[0]) < 10 ? "0" + Integer.valueOf(tempdate[0]) : Integer.valueOf(tempdate[0]) + "";
            dateStrval = tempdate[2] + "-" + tempMonth + "-" + tempday;


        } else {

            Utility.toast(mContext, "Current date is not between semester date");
            txtday.setEnabled(false);

        }

        if (Utility.isTeacher(mContext)) {

            StandardGradeList();
            txtday.setVisibility(View.VISIBLE);
            txtdaystudent.setVisibility(View.GONE);

        } else {

            spnstandard.setVisibility(View.GONE);
            btn_save.setVisibility(View.GONE);
            ll_working.setVisibility(View.GONE);
            txtday.setVisibility(View.GONE);
            txtdaystudent.setVisibility(View.VISIBLE);
            AttendanceListStudent();

        }

        txtday.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                /*Commented By Krishna : 26-07-2019 -  Date Selection for Attendance with Validation*/

                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    isCompareDate = Utility.CompareDates(df, df.parse(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")), df.parse(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (isCompareDate) {

                    Utility.toast(mContext, getResources().getString(R.string.attendancedatevalidation));

                } else {

                    Calendar c = Calendar.getInstance();
                    stYear = c.get(Calendar.YEAR);
                    stMonth = c.get(Calendar.MONTH);
                    stDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dpd = new DatePickerDialog(mContext,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                    String formattedDate = Utility.dateFormate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year, "dd-MMM-yyyy", "dd-MM-yyyy");

                                    monthstrvalue = String.valueOf(monthOfYear + 1);
                                    yearStrval = String.valueOf(year);

                                    isCompareDateForNextDate = Utility.DateFormatterForAttendance(mContext, formattedDate, "dd-MMM-yyyy");

                                    if (!isCompareDateForNextDate) {
                                        Utility.showAlertDialog(mContext, getResources().getString(R.string.pleaseselectpreviousday), "Error");
                                        String CurrentDate = Utility.dateFormate(stDay + "-" + (stMonth + 1) + "-" + stYear, "dd-MMM-yyyy", "dd-MM-yyyy");
                                        txtday.setText(CurrentDate);
                                        btn_save.setClickable(false);
                                        btn_save.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                                        ganratereport.setClickable(false);
                                        ganratereport.setBackgroundColor(getResources().getColor(R.color.gray_color_code));

                                    } else {

                                        txtday.setText(formattedDate);
                                        btn_save.setClickable(true);
                                        btn_save.setBackgroundColor(getResources().getColor(R.color.blue));
                                        ganratereport.setClickable(true);
                                        ganratereport.setBackgroundColor(getResources().getColor(R.color.blue));

                                        dateStrval = Utility.dateFormate(txtday.getText().toString(), "yyyy-MM-dd", "dd-MMM-yyyy");

                                        if (gradeIdStr.equalsIgnoreCase("") && divisionIdStr.equalsIgnoreCase("")) {

                                            if (Utility.isTeacher(mContext)) {
                                                StandardGradeList();
                                            } else {
                                                AttendanceListStudent();
                                            }

                                        } else {

                                            if (Utility.isTeacher(mContext)) {
                                                AttendanceListTeacher();
                                            } else {
                                                AttendanceListStudent();
                                            }

                                        }

                                    }

                                }

                            }, stYear, stMonth, stDay);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                        dpd.getDatePicker().setMinDate((Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", ""))));
                        dpd.getDatePicker().setMaxDate((Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", ""))));

                    }

                    dpd.show();

                }

                /*END*/

            }


        });

        txtdaystudent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                /*Commented By Krishna : 26-07-2019 -  Date Selection for Attendance with Validation*/

                String BatchStartDate = Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                isCompareDate = Utility.DateFormatterForAttendance(mContext, BatchStartDate, "dd-MM-yyyy");

                if (!isCompareDate) {

                    Utility.toast(mContext, getResources().getString(R.string.attendancedatevalidation));

                } else {

                    Calendar c = Calendar.getInstance();
                    stYear = c.get(Calendar.YEAR);
                    stMonth = c.get(Calendar.MONTH);
                    stDay = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dpd = new DatePickerDialog(mContext,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                    String formattedDate = Utility.dateFormate(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year, "dd-MMM-yyyy", "dd-MM-yyyy");

                                    monthstrvalue = String.valueOf(monthOfYear + 1);
                                    yearStrval = String.valueOf(year);

                                    isCompareDateForNextDate = Utility.DateFormatterForAttendance(mContext, formattedDate, "dd-MMM-yyyy");

                                    if (!isCompareDateForNextDate) {
                                        Utility.showAlertDialog(mContext, getResources().getString(R.string.pleaseselectpreviousday), "Error");
                                        String CurrentDate = Utility.dateFormate(stDay + "-" + (stMonth + 1) + "-" + stYear, "dd-MMM-yyyy", "dd-MM-yyyy");
                                        txtday.setText(CurrentDate);
                                        txtdaystudent.setText(CurrentDate);
                                        btn_save.setClickable(false);
                                        btn_save.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                                        ganratereport.setClickable(false);
                                        ganratereport.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                                    } else {
                                        txtday.setText(formattedDate);
                                        txtdaystudent.setText(formattedDate);
                                        btn_save.setClickable(true);
                                        btn_save.setBackgroundColor(getResources().getColor(R.color.blue));
                                        ganratereport.setClickable(true);
                                        ganratereport.setBackgroundColor(getResources().getColor(R.color.blue));

                                        dateStrval = Utility.dateFormate(txtday.getText().toString(), "yyyy-MM-dd", "dd-MMM-yyyy");

                                        if (gradeIdStr.equalsIgnoreCase("") && divisionIdStr.equalsIgnoreCase("")) {

                                            if (Utility.isTeacher(mContext)) {
                                                StandardGradeList();
                                            } else {
                                                AttendanceListStudent();
                                            }

                                        } else {

                                            if (Utility.isTeacher(mContext)) {
                                                AttendanceListTeacher();
                                            } else {
                                                AttendanceListStudent();
                                            }

                                        }

                                    }

                                }

                            }, stYear, stMonth, stDay);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                        dpd.getDatePicker().setMinDate((Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", ""))));
                        dpd.getDatePicker().setMaxDate((Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", ""))));

                    }

                    dpd.show();

                }

                /*END*/

            }

        });

        ganratereport.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                GanrateReport();

            }

        });

        spnstandard.setOnItemSelectedListener(this);
        return v;

    }

    public int getMonth(String month) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("MMM").parse(month));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int monthInt = cal.get(Calendar.MONTH) + 1;

        return monthInt;

    }

    public int getMonthNumber(String month) {
        Date date = null;
        try {
            date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int _month = cal.get(Calendar.MONTH);


        return _month;

    }

    public void StandardGradeList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Utility.getUserModelData(mContext);

        }

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
//      arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));


//      new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GRADE_DIVISION_FOR_CLASSTEACHER_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);


    }

    public void AttendanceListTeacher() {

        edtabsantstr = "";
        edtLeaveStr = "";
        edtSeaklleavestr = "";

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Utility.getUserModelData(mContext);

        }

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, gradeIdStr));
        arrayList.add(new PropertyVo(ServiceResource.YEAR, yearStrval));
        arrayList.add(new PropertyVo(ServiceResource.MONTH, monthstrvalue));

        arrayList.add(new PropertyVo(ServiceResource.DivisionID, divisionIdStr));

        Log.d("attendancerequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.ATTENDANCELISTFORTEACHER_METHODNAME, ServiceResource.ATTENDANCE_URL);

    }

    public void saveAttendance() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Utility.getUserModelData(mContext);

        }

        arrayList.add(new PropertyVo(ServiceResource.STANDARDID, gradeIdStr));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, divisionIdStr));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ATTENDENCEBY, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.DATEOFATTENDENCE, currenttxtdate));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        String dayStrval = Utility.dateFormate(currenttxtdate, "EEEE", "MM-dd-yyyy");

        arrayList.add(new PropertyVo(ServiceResource.DAYOFATTENDENCE_TERM, dayStrval));
        arrayList.add(new PropertyVo(ServiceResource.TOTALSTUDENTS, totalStudents));
        arrayList.add(new PropertyVo(ServiceResource.PRESENTSTUDENTS, preasentStudents));
        arrayList.add(new PropertyVo(ServiceResource.ABSENTSTUDENTS, abcentStudent));
        arrayList.add(new PropertyVo(ServiceResource.ONLEAVESTUDENTS, onLeaveStudent));
        arrayList.add(new PropertyVo(ServiceResource.ISWORKINGDAY, chk_isWorking.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTDATA, studentDataStr));
        arrayList.add(new PropertyVo(ServiceResource.OLDSTUDATTEREGIMASTERID, editIdFromDate()));

        Log.d("presentstudentstr23", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.SAVEATTENDANCE_METHODNAME, ServiceResource.ATTENDANCE_URL);

    }

    public void GanrateReport() {

        /*commented By Krishna : 23-05-2019 - Display Choice of Report Generattion for Attendance*/

        final CharSequence[] items = {mContext.getResources().getString(R.string.AllType),
                mContext.getResources().getString(R.string.Onlyworkingday),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Generate Report !");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(mContext.getResources().getString(R.string.AllType))) {

                    isWorkingDay = false;
                    GenerateReportRequest();

                } else if (items[item].equals(mContext.getResources().getString(R.string.Onlyworkingday))) {

                    isWorkingDay = true;
                    GenerateReportRequest();

                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

        /*END*/

    }

    private void GenerateReportRequest() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Utility.getUserModelData(mContext);
        }
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        arrayList.add(new PropertyVo(ServiceResource.BATCHSTART, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.BATCHEND, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));

        arrayList.add(new PropertyVo(ServiceResource.GradeID, gradeIdStr));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, divisionIdStr));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));
        arrayList.add(new PropertyVo(ServiceResource.YEAR, yearStrval));
        arrayList.add(new PropertyVo(ServiceResource.MONTH, monthstrvalue));
        if (spnstandard.getSelectedItem().equals(null) || spnstandard.getSelectedItem().equals("")) {
            arrayList.add(new PropertyVo(ServiceResource.STDDVINAME, ""));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.STDDVINAME, spnstandard.getSelectedItem().toString()));
        }
//        if (standardArray.size() > 0) {
//            arrayList.add(new PropertyVo(ServiceResource.STDDVINAME, spnstandard.getSelectedItem().toString()));
//        }

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTENAME, new UserSharedPrefrence(mContext).getLoginModel().getInstituteName()));
        arrayList.add(new PropertyVo(ServiceResource.MemberID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ISONLYWORKINGDAY, isWorkingDay));

        Log.d("attendanceRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ATTENDANCEREPORT_METHODNAME, ServiceResource.ATTENDANCE_URL);

    }

    public void AttendanceListStudent() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {

            Utility.getUserModelData(mContext);

        }

        arrayList.add(new PropertyVo(ServiceResource.YEARSTARTDATE, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.YEARENDDATE, Utility.getDate(Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd().replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));

        arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));

        Log.d("studentrequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ATTENDANCELISTFORSTUDENT_METHODNAME, ServiceResource.ATTENDANCE_URL);

    }

    public String editIdFromDate() {

        String editId = "";

        for (int i = 0; i < Global.editAttendance.size(); i++) {

            String tempdate = Utility.getDate(Long.valueOf(Global.editAttendance.get(i).getDateOfAttendence()), "yyyy-MM-dd");
            Log.d("getDate", tempdate);

            if (tempdate.equalsIgnoreCase(dateStrval)) {

                editId = Global.editAttendance.get(i).getStudentAttRegMasterID();
                break;

            }

        }

        return editId;

    }

    public boolean editworkingFromDate() {
        boolean editIsWorking = false;
        if (Global.editAttendance != null && Global.editAttendance.size() > 0) {
            for (int i = 0; i < Global.editAttendance.size(); i++) {

                String tempdate = Utility.getDate(Long.valueOf(Global.editAttendance.get(i).getDateOfAttendence()), "yyyy-MM-dd");
                if (tempdate.equalsIgnoreCase(dateStrval)) {
                    editIsWorking = Global.editAttendance.get(i).isIsWorkingDay();
                    break;
                }
            }
        }

        return editIsWorking;

    }


    @Override
    public void response(String result, String methodName) {

//        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {
        if (ServiceResource.GRADE_DIVISION_FOR_CLASSTEACHER_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {


                } else {

                    hJsonArray = new JSONArray(result);
                    Global.StandardModels = new ArrayList<StandardModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {

                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        StandardModel model = new StandardModel();
//                        model.setStandardName(hJsonObject
//                                .getString(ServiceResource.GRADE));
                        model.setStandrdId(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADEID));
                        model.setDivisionId(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONID));
//                        model.setDivisionName(hJsonObject
//                                .getString(ServiceResource.DIVISION));
                        model.setDivisionName(hJsonObject
                                .getString(ServiceResource.STANDARD_GRADEDIVISION_NAME));

                        Global.StandardModels.add(model);
                        Log.d("standardrarraylist", Global.StandardModels.toString());


                    }

                }

            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            standardArray = new ArrayList<String>();

            if (Global.StandardModels != null && Global.StandardModels.size() > 0) {

                btn_save.setClickable(true);
                btn_save.setBackgroundColor(getResources().getColor(R.color.blue));
                ganratereport.setClickable(true);
                ganratereport.setBackgroundColor(getResources().getColor(R.color.blue));

                gradeIdStr = Global.StandardModels.get(0).getStandrdId();
                divisionIdStr = Global.StandardModels.get(0).getDivisionId();

                for (int i = 0; i < Global.StandardModels.size(); i++) {

                    standardArray.add(Global.StandardModels.get(i).getDivisionName());

                }

                if (standardArray != null && standardArray.size() > 0) {

                    btn_save.setClickable(true);
                    btn_save.setBackgroundColor(getResources().getColor(R.color.blue));
                    ganratereport.setClickable(true);
                    ganratereport.setBackgroundColor(getResources().getColor(R.color.blue));

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                            getActivity(), android.R.layout.simple_spinner_item,
                            standardArray);

                    // Drop down layout style - list view with radio button

                    dataAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    // attaching data adapter to spinner
                    spnstandard.setAdapter(dataAdapter);

                } else {

                    btn_save.setClickable(false);
                    btn_save.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                    ganratereport.setClickable(false);
                    ganratereport.setBackgroundColor(getResources().getColor(R.color.gray_color_code));

                }

                AttendanceListTeacher();

            } else {

                btn_save.setClickable(false);
                btn_save.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                ganratereport.setClickable(false);
                ganratereport.setBackgroundColor(getResources().getColor(R.color.gray_color_code));

            }

        } else if (ServiceResource.ATTENDANCEREPORT_METHODNAME.equalsIgnoreCase(methodName)) {

            saveDownload(ServiceResource.BASE_IMG_URL + result);

        } else if (ServiceResource.ATTENDANCELISTFORTEACHER_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("getstudentList", result);

            Global.attendancemodels = new ArrayList<AtteandanceModel>();
            Global.editAttendance = new ArrayList<AtteandanceModel>();

            JSONArray hJsonArray;
            JSONObject jObj;
            JSONArray attendaceeditarray;

            try {

                if (result.contains("\"success\":0")) {


                } else {

                    Global.attendancemodels.clear();
                    Global.editAttendance.clear();

                    jObj = new JSONObject(result);

                    hJsonArray = jObj.getJSONArray(ServiceResource.TABLE);
                    attendaceeditarray = jObj.getJSONArray(ServiceResource.TABLE1);

                    for (int i = 0; i < hJsonArray.length(); i++) {

                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        AtteandanceModel model = new AtteandanceModel();
                        model.setMemberID(hJsonObject
                                .getString(ServiceResource.MEMBERID));
                        model.setFullName(hJsonObject
                                .getString(ServiceResource.FULLNAME));
                        model.setRollNo(hJsonObject
                                .getString(ServiceResource.ROLLNO));
                        Iterator<String> iter = hJsonObject.keys();

                        HashMap<String, String> map = new HashMap<String, String>();

                        while (iter.hasNext()) {

                            String key = iter.next();

                            if (Utility.isValidDate(key, "yyyy-MM-dd")) {

                                try {

                                    Object value = hJsonObject.get(key);
                                    map.put(key, value.toString());

                                } catch (JSONException e) {
                                    // Something went wrong!
                                }

                            }

                        }

                        model.setMap(map);
                        Global.attendancemodels.add(model);

                    }

                    for (int i = 0; i < attendaceeditarray.length(); i++) {

                        JSONObject hJsonObject = attendaceeditarray.getJSONObject(i);
                        AtteandanceModel model = new AtteandanceModel();
                        model.setStudentAttRegMasterID(hJsonObject
                                .getString(ServiceResource.STUDENTATTREGMASTERID));
                        model.setDateOfAttendence(hJsonObject
                                .getString(ServiceResource.DATEOFATTENDENCE).replace("/Date(", "").replace(")/", ""));
                        model.setIsWorkingDay(hJsonObject
                                .getString(ServiceResource.ISWORKINGDAY));

                        Global.editAttendance.add(model);

                    }

                }

            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }


            if (Global.attendancemodels != null && Global.attendancemodels.size() > 0) {

                btn_save.setClickable(true);
                btn_save.setBackgroundColor(getResources().getColor(R.color.blue));
                ganratereport.setClickable(true);
                ganratereport.setBackgroundColor(getResources().getColor(R.color.blue));

                adapter = new AttendaceTeacherAdapter(
                        getActivity(), Global.attendancemodels, dateStrval);
                lvattendance.setAdapter(adapter);

                edtabsantstr = "";
                edtLeaveStr = "";
                edtSeaklleavestr = "";

                for (int l = 0; l < Global.attendancemodels.size(); l++) {
                    AttendanceModelABPS abps = new AttendanceModelABPS(Global.attendancemodels.get(l).getMap().get(dateStrval));

                    if (abps.isAbsant()) {

                        edtabsantstr = edtabsantstr + Global.attendancemodels.get(l).getRollNo();
                        if (l != Global.attendancemodels.size() - 1) {
                            edtabsantstr = edtabsantstr + ",";
                        }

                    } else if (abps.isLeave()) {
                        edtLeaveStr = edtLeaveStr + Global.attendancemodels.get(l).getRollNo();
                        if (l != Global.attendancemodels.size() - 1) {
                            edtLeaveStr = edtLeaveStr + ",";
                        }
                    } else if (abps.isSikleave()) {
                        edtSeaklleavestr = edtSeaklleavestr + Global.attendancemodels.get(l).getRollNo();
                        if (l != Global.attendancemodels.size() - 1) {
                            edtSeaklleavestr = edtSeaklleavestr + ",";
                        }
                    }

                }


                if (edtabsantstr.equalsIgnoreCase("null") || edtabsantstr.equalsIgnoreCase(null)
                        || edtabsantstr.equalsIgnoreCase("")) {

                    edt_absent.setHint("absent");
                    edt_absent.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));

                    edt_absent.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            edt_absent.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));

                        }

                        @Override
                        public void afterTextChanged(Editable s) {


                        }

                    });


                } else {

                    edt_absent.setText(edtabsantstr);
                    edt_absent.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));

                }

                if (edtLeaveStr.equalsIgnoreCase("null") || edtLeaveStr.equalsIgnoreCase(null)
                        || edtLeaveStr.equalsIgnoreCase("")) {

                    edtleave.setHint("leave");
                    edtleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));


                    edtleave.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            edtleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }

                    });

                } else {
                    edtleave.setText(edtLeaveStr);
                    edtleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));
                }

                if (edtSeaklleavestr.equalsIgnoreCase("null") || edtSeaklleavestr.equalsIgnoreCase(null)
                        || edtSeaklleavestr.equalsIgnoreCase("")) {
                    edt_seakleave.setHint("Seak leave");
                    edt_seakleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));
                    edt_seakleave.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            edt_seakleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }

                    });


                } else {
                    edt_seakleave.setText(edtSeaklleavestr);
                    edt_seakleave.setTextColor(ContextCompat.getColor(mContext, R.color.dark_black));
                }

            } else {

                lvattendance.setAdapter(null);
                btn_save.setClickable(false);
                btn_save.setBackgroundColor(getResources().getColor(R.color.gray_color_code));
                ganratereport.setClickable(false);
                ganratereport.setBackgroundColor(getResources().getColor(R.color.gray_color_code));

            }

            chk_isWorking.setChecked(editworkingFromDate());

        } else if (ServiceResource.ATTENDANCELISTFORSTUDENT_METHODNAME.equalsIgnoreCase(methodName)) {

            Global.attendancemodels = new ArrayList<AtteandanceModel>();
            JSONArray hJsonArray;
            JSONObject jObj;
            JSONArray attendaceeditarray;

            try {

                Global.attendancemodels.clear();

                if (result.contains("\"success\":0")) {


                } else {

                    // jObj = new JSONObject(examresult);

                    hJsonArray = new JSONArray(result);
                    // jObj.getJSONArray(ServiceResource.TABLE);

                    for (int i = 0; i < hJsonArray.length(); i++) {

                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        AtteandanceModel model = new AtteandanceModel();

                        model.setStudentAttRegMasterID(hJsonObject
                                .getString(ServiceResource.STUDENTATTENDENCEID));
                        model.setDateOfAttendence(hJsonObject
                                .getString(ServiceResource.DATEOFATTENDENCE));
                        model.setAttencenceType_Term(hJsonObject
                                .getString(ServiceResource.ATTENCENCETYPE_TERM));

                        Global.attendancemodels.add(model);


                    }


                }


            } catch (JSONException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            if (Global.attendancemodels != null && Global.attendancemodels.size() > 0) {

                AttendanceStudentAdapter std = new AttendanceStudentAdapter(
                        getActivity(), Global.attendancemodels, dateStrval);
                lvattendance.setAdapter(std);

            } else {

                lvattendance.setAdapter(null);

            }


        } else if (ServiceResource.SAVEATTENDANCE_METHODNAME.equalsIgnoreCase(methodName)) {

            Log.d("attendanceResponse", result);

            // 23 - 01 - 2019 commented by Krishna : Redirect to Wall Screen if Attendance saved Successfully

            try {

                if (result.contains("\"success\":0")) {

//                    Global.attendancemodels.clear();
                    Utility.toast(mContext, "Attendance Saved Successfully");
                    AttendanceListTeacher();

//                    edt_absent.setText("");
//                    edtleave.setText("");
//                    edt_seakleave.setText("");

                } else {

                }

            } catch (Exception e) {

                e.printStackTrace();

            }

            /*END*/

//            sendPushNotification();

        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            AttendanceListTeacher();

        }


    }

    public void sendPushNotification() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // TODO Auto-generated method stub
        gradeIdStr = Global.StandardModels.get(position).getStandrdId();
        divisionIdStr = Global.StandardModels.get(position).getDivisionId();
        AttendanceListTeacher();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_save) {


            onLeaveStudent = 0;
            preasentStudents = 0;
            abcentStudent = 0;

            if (!isQuickView) {

                if (adapter != null) {

                    ArrayList<AtteandanceModel> templist = adapter.getModifyAttendanceList();
                    Log.d("getTotalStudnet", String.valueOf(templist.size()));
                    totalStudents = templist.size();
                    currenttxtdate = Utility.dateFormate(txtday.getText().toString(), "MM-dd-yyyy", "dd-MMM-yyyy");
                    studentDataStr = "";

                    for (int i = 0; i < templist.size(); i++) {

                        studentDataStr = studentDataStr + templist.get(i).getMemberID() + ",";
                        if (templist.get(i).getMap().get(dateStrval).equalsIgnoreCase(ServiceResource.PRESENT) || templist.get(i).getMap().get(dateStrval).equalsIgnoreCase("null") || templist.get(i).getMap().get(dateStrval).equalsIgnoreCase(null)) {

                            preasentStudents++;
                            Log.d("getPresentStudent", String.valueOf(preasentStudents));
                            studentDataStr = studentDataStr + ServiceResource.PRESENT + "#";

                        } else if (templist.get(i).getMap().get(dateStrval).equalsIgnoreCase(ServiceResource.ABSENT)) {

                            abcentStudent++;
                            Log.d("getbasentStudents", String.valueOf(abcentStudent));
                            studentDataStr = studentDataStr + ServiceResource.ABSENT + "#";

                        } else if (templist.get(i).getMap().get(dateStrval).equalsIgnoreCase(ServiceResource.LEAVE)) {

                            onLeaveStudent++;
                            Log.d("getLeaveStudnet", String.valueOf(onLeaveStudent));
                            studentDataStr = studentDataStr + ServiceResource.LEAVE + "#";

                        } else if (templist.get(i).getMap().get(dateStrval).equalsIgnoreCase(ServiceResource.SICKLEAVE)) {

                            onLeaveStudent++;
                            Log.d("getSeakStudnet", String.valueOf(onLeaveStudent));
                            studentDataStr = studentDataStr + ServiceResource.SICKLEAVE + "#";

                        }

                    }

                    // open dialog for confirmation to save attendance - 25-06-2020 by krishna
                    OpenDialogforAttendanceConfirmation(preasentStudents, abcentStudent, onLeaveStudent);

                }

            } else {

                String[] edtleaveArray = {}, edtabsantArray = {}, edtSeakleaveArray = {};

                if (adapter != null) {

                    ArrayList<AtteandanceModel> templist = adapter.getModifyAttendanceList();
                    totalStudents = templist.size();

                    currenttxtdate = Utility.dateFormate(txtday.getText().toString(), "MM-dd-yyyy", "dd-MMM-yyyy");
                    studentDataStr = "";

                    for (int i = 0; i < templist.size(); i++) {

                        studentDataStr = studentDataStr + templist.get(i).getMemberID() + ",";
                        edtleavestr = edtleave.getText().toString();
                        edtabsantstr = edt_absent.getText().toString();
                        edtseakleavestr = edt_seakleave.getText().toString();

                        edtleaveArray = edtleavestr.split(",");
                        edtabsantArray = edtabsantstr.split(",");
                        edtSeakleaveArray = edtseakleavestr.split(",");

                        boolean isAdded = false;

                        if (edtabsantArray != null && edtabsantArray.length > 0) {

                            if (!isAdded) {

                                for (int l = 0; l < edtabsantArray.length; l++) {

                                    if (edtabsantArray[l].equalsIgnoreCase(templist.get(i).getRollNo())) {

                                        abcentStudent++;
                                        studentDataStr = studentDataStr + ServiceResource.ABSENT + "#";
                                        isAdded = true;
                                        RollNolist.add(templist.get(i).getRollNo());


                                    }

                                }

                            }

                        }

                        if (edtleaveArray != null && edtleaveArray.length > 0) {

                            if (!isAdded) {

                                for (int l = 0; l < edtleaveArray.length; l++) {

                                    if (edtleaveArray[l].equalsIgnoreCase(templist.get(i).getRollNo())) {

                                        onLeaveStudent++;
                                        studentDataStr = studentDataStr + ServiceResource.LEAVE + "#";
                                        isAdded = true;
                                        RollNolist.add(templist.get(i).getRollNo());

                                    }

                                }

                            }

                        }

                        if (edtSeakleaveArray != null && edtSeakleaveArray.length > 0) {

                            if (!isAdded) {

                                for (int l = 0; l < edtSeakleaveArray.length; l++) {

                                    if (edtSeakleaveArray[l].equalsIgnoreCase(templist.get(i).getRollNo())) {

                                        onLeaveStudent++;
                                        studentDataStr = studentDataStr + ServiceResource.SICKLEAVE + "#";
                                        isAdded = true;
                                        RollNolist.add(templist.get(i).getRollNo());

                                    }
                                }
                            }
                        }

                        if (!isAdded) {

                            preasentStudents++;
                            studentDataStr = studentDataStr + ServiceResource.PRESENT + "#";

                        }

                    }


                }

                Log.v("attendancestudentstr", studentDataStr);
                Log.v("absentstudent", String.valueOf(abcentStudent));
                Log.v("presentstudent", String.valueOf(preasentStudents));
                Log.v("leavestudent", String.valueOf(onLeaveStudent));


                saveAttendance();


            }

        }

        if (v.getId() == R.id.btn_changeView) {

            if (isQuickView) {

                isQuickView = false;
                btn_changeView.setText(getResources().getString(R.string.quick));
                lvattendance.setVisibility(View.VISIBLE);
                quickview.setVisibility(View.GONE);

            } else {

                try {

                    if (Global.attendancemodels.size() > 0) {

                        for (int i = 0; i < Global.attendancemodels.size(); i++) {

                            if (Global.attendancemodels.get(i).getRollNo().equalsIgnoreCase("")) {

                                Utility.toast(getActivity(), "Roll Number Not Found");
                                isQuickView = false;
                                btn_changeView.setText(getResources().getString(R.string.quick));
                                lvattendance.setVisibility(View.VISIBLE);
                                quickview.setVisibility(View.GONE);
//                            btn_changeView.setClickable(false);

                                break;

                            } else {

                                edtabsantstr = "";
                                edtLeaveStr = "";
                                edtSeaklleavestr = "";
                                isQuickView = true;
                                btn_changeView.setClickable(true);
                                btn_changeView.setText(getResources().getString(R.string.normal));
                                lvattendance.setVisibility(View.GONE);
                                quickview.setVisibility(View.VISIBLE);

                            }

                        }

                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }


            }

        }

    }

    private void OpenDialogforAttendanceConfirmation(int preasentStudents, int abcentStudent, int onLeaveStudent) {

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.error_dialog);
        TextView tv_alert_title = (TextView) dialog.findViewById(R.id.tv_alert_title);
        TextView txtdetail = (TextView) dialog.findViewById(R.id.txtdetail);
        TextView txtno = (TextView) dialog.findViewById(R.id.txtno);
        tv_alert_title.setText(getResources().getString(R.string.confirmationattendancemessage));
        txtdetail.setText("Present student is " + String.valueOf(preasentStudents) + " Absent student is " + String.valueOf(abcentStudent) + " and  on Leave student is " + String.valueOf(onLeaveStudent));
        TextView txtyes = (TextView) dialog.findViewById(R.id.txtyes);
        txtyes.setText(getResources().getString(R.string.save));
        txtno.setText(getResources().getString(R.string.Cancel));
        txtno.setVisibility(View.VISIBLE);
        txtyes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAttendance();
                dialog.dismiss();
            }
        });
        txtno.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void saveDownload(String url) {

        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");

        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);

        for (int i = 0; i < fileList.size(); i++) {

            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {

                isDownloading = true;

            }

        }

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setMimeType("application/pdf")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);



//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager)
//                mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//
//
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//
//        DownloadManager.Request request =
//                new DownloadManager.Request(source);
//        request.setTitle(getResources().getString(R.string.app_name) + fileName);
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager
//                .Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//        request.setMimeType("application/pdf");
//        long downloadId = mgr.enqueue(request);
//        Utility.toast(mContext, getResources().getString(R.string.downloadstarting));

    }

    public ArrayList<File> getfile(File dir) {

        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();

        if (listFile != null && listFile.length > 0) {

            for (int i = 0; i < listFile.length; i++) {

                fileList.add(listFile[i]);

            }

        }

        return fileList;
    }

    public class SortedDate implements Comparator<StandardModel> {

        @Override
        public int compare(StandardModel o1, StandardModel o2) {

            return o1.getStandardName().compareTo(o2.getStandardName());

        }

    }

}
