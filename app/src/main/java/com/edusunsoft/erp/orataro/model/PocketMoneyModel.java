package com.edusunsoft.erp.orataro.model;

public class PocketMoneyModel {

    Double Amount,Deposit,Withdraw,TotalBalance;
    String HstTransactionID, StudentID, DateOfTransaction, TransactionType_Term, Narration, AssociationType,SeqNo;

    public PocketMoneyModel() {
    }

    public Double getTotalBalance() {
        return TotalBalance;
    }

    public void setTotalBalance(Double totalBalance) {
        TotalBalance = totalBalance;
    }

    public Double getDeposit() {
        return Deposit;
    }

    public void setDeposit(Double deposit) {
        Deposit = deposit;
    }

    public Double getWithdraw() {
        return Withdraw;
    }

    public void setWithdraw(Double withdraw) {
        Withdraw = withdraw;
    }

    public String getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(String seqNo) {
        SeqNo = seqNo;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public String getHstTransactionID() {
        return HstTransactionID;
    }

    public void setHstTransactionID(String hstTransactionID) {
        HstTransactionID = hstTransactionID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getDateOfTransaction() {
        return DateOfTransaction;
    }

    public void setDateOfTransaction(String dateOfTransaction) {
        DateOfTransaction = dateOfTransaction;
    }

    public String getTransactionType_Term() {
        return TransactionType_Term;
    }

    public void setTransactionType_Term(String transactionType_Term) {
        TransactionType_Term = transactionType_Term;
    }

    public String getNarration() {
        return Narration;
    }

    public void setNarration(String narration) {
        Narration = narration;
    }

    public String getAssociationType() {
        return AssociationType;
    }

    public void setAssociationType(String associationType) {
        AssociationType = associationType;
    }

    @Override
    public String toString() {
        return "PocketMoneyModel{" +
                "Amount=" + Amount +
                ", HstTransactionID='" + HstTransactionID + '\'' +
                ", StudentID='" + StudentID + '\'' +
                ", DateOfTransaction='" + DateOfTransaction + '\'' +
                ", TransactionType_Term='" + TransactionType_Term + '\'' +
                ", Narration='" + Narration + '\'' +
                ", AssociationType='" + AssociationType + '\'' +
                '}';
    }
}
