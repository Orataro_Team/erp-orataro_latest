package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ImageSelectionActivity;
import com.edusunsoft.erp.orataro.activities.PreviewImageActivity;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.ParentsProfileModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FatherProfileFragment extends Fragment implements OnClickListener,
        OnItemSelectedListener, ResponseWebServices {

    private View view;
    Context mContext;
    ImageView img_profile, iv_idproof;
    EditText edt_fathername, edt_phone_no, edt_occuption, edt_designation,
            edt_address, edt_officenumber;
    CheckBox chb_remember;
    LinearLayout ll_save;
    TextView txt_birthday, txt_anivesarydate, txt_dateofexp, txt_dateofissue;
    EditText edt_firstname, edt_lastname, edt_study, edt_email, edt_occdetail,
            edt_homeaddress, edt_idno, edt_idtype;

    static final int DATE_DIALOG_BIRTHDAY = 111;
    static final int DATE_DIALOG_ANYVESARY = 112;
    static final int DATE_DIALOG_DATEOFIISUE = 113;
    static final int DATE_DIALOG_DATEOFEXPIRY = 114;

    int bmYear, bmDay, bmMonth;
    int amYear, amDay, amMonth;
    int imYear, imDay, imMonth;
    int emDay, emYear, emMonth;

    ParentsProfileModel parentProfileModel;
    int type;
    Boolean isNull;
    public String typeStr = "Mr.";
    LinearLayout ll_spn;
    Spinner spn_gender;

    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    protected int REQUEST_CAMERAID = 3;
    protected int SELECT_FILEID = 4;
    private String fielPath = "";
    private byte[] byteArray;
    private String fielPathId = "";
    private byte[] byteArrayID;
    private Uri fileUri, fileUriId;
    private static final String TYPE = "type";

    /**
     * set value in argument
     *
     * @param type
     * @return
     */
    public static final FatherProfileFragment newInstance(int type) {
        FatherProfileFragment f = new FatherProfileFragment();
        Bundle bdl = new Bundle(2);
        bdl.putInt(TYPE, type);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.father_profile_fragment, container, false);
        mContext = getActivity();
        ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        type = getArguments().getInt(TYPE);
        parentProfileModel = new ParentsProfileModel();

        ll_save = (LinearLayout) view.findViewById(R.id.ll_save);
        ll_spn = (LinearLayout) view.findViewById(R.id.ll_gender);
        spn_gender = (Spinner) view.findViewById(R.id.spn_gender);
        img_profile = (ImageView) view.findViewById(R.id.img_profile);
        iv_idproof = (ImageView) view.findViewById(R.id.iv_idproof);
        edt_fathername = (EditText) view.findViewById(R.id.edt_fathername);
        edt_phone_no = (EditText) view.findViewById(R.id.edt_phone_no);
        edt_occuption = (EditText) view.findViewById(R.id.edt_occuption);
        edt_designation = (EditText) view.findViewById(R.id.edt_designation);
        edt_address = (EditText) view.findViewById(R.id.edt_address);
        edt_officenumber = (EditText) view.findViewById(R.id.edt_officenumber);
        chb_remember = (CheckBox) view.findViewById(R.id.chb_remember);

        txt_birthday = (TextView) view.findViewById(R.id.txt_birthday);
        txt_anivesarydate = (TextView) view
                .findViewById(R.id.txt_anivesarydate);
        txt_dateofexp = (TextView) view.findViewById(R.id.txt_dateofexp);
        txt_dateofissue = (TextView) view.findViewById(R.id.txt_dateofissue);

        edt_firstname = (EditText) view.findViewById(R.id.edt_firstname);
        edt_lastname = (EditText) view.findViewById(R.id.edt_lastname);
        edt_study = (EditText) view.findViewById(R.id.edt_study);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_occdetail = (EditText) view.findViewById(R.id.edt_occdetail);
        edt_homeaddress = (EditText) view.findViewById(R.id.edt_homeaddress);
        edt_idno = (EditText) view.findViewById(R.id.edt_idno);
        edt_idtype = (EditText) view.findViewById(R.id.edt_idtype);

        List<String> categories = new ArrayList<String>();

        categories.add("Mr.");
        categories.add("Mrs.");
        categories.add("Miss");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spn_gender.setAdapter(adapter);
        spn_gender.setOnItemSelectedListener(this);

        if (type == ServiceResource.FATHER_PROFILE_INT) {
            edt_fathername.setHint("FatherName");
            ll_spn.setVisibility(View.GONE);
            if (Global.fatherProfile != null && Global.fatherProfile.size() > 0) {
                parentProfileModel = Global.fatherProfile.get(0);
            }

        } else if (type == ServiceResource.MOTHER_PROFILE_INT) {
            ll_spn.setVisibility(View.GONE);
            edt_fathername.setHint("MotherName");
            if (Global.motherProfile != null && Global.motherProfile.size() > 0) {
                parentProfileModel = Global.motherProfile.get(0);
            }
        } else { //if(type == ServiceResource.GARDIAN_PROFILE_INT){
            edt_fathername.setHint("GuardianName");
            if (Global.gardianProfile != null
                    && Global.gardianProfile.size() > 0) {
                parentProfileModel = Global.gardianProfile.get(0);
            }
        }

        Calendar c = Calendar.getInstance();
        bmYear = c.get(Calendar.YEAR);
        bmMonth = c.get(Calendar.MONTH);
        bmDay = c.get(Calendar.DAY_OF_MONTH);

        amYear = c.get(Calendar.YEAR);
        amMonth = c.get(Calendar.MONTH);
        amDay = c.get(Calendar.DAY_OF_MONTH);

        imYear = c.get(Calendar.YEAR);
        imMonth = c.get(Calendar.MONTH);
        imDay = c.get(Calendar.DAY_OF_MONTH);

        emYear = c.get(Calendar.YEAR);
        emMonth = c.get(Calendar.MONTH);
        emDay = c.get(Calendar.DAY_OF_MONTH);

        fill_data();

        ll_save.setOnClickListener(this);
        txt_birthday.setOnClickListener(this);
        txt_anivesarydate.setOnClickListener(this);
        txt_dateofexp.setOnClickListener(this);
        txt_dateofissue.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        iv_idproof.setOnClickListener(this);

        return view;

    }

    /**
     * fill data in form
     */
    private void fill_data() {

        if (Utility.isNull(parentProfileModel.getFullName())) {
            edt_fathername.setText(parentProfileModel.getFullName());
        }
        if (Utility.isNull(parentProfileModel.getFirstName())) {
            edt_firstname.setText(parentProfileModel.getFirstName());
        }
        if (Utility.isNull(parentProfileModel.getLastName())) {
            edt_lastname.setText(parentProfileModel.getLastName());
        }
        if (Utility.isNull(parentProfileModel.getTxt_birthday())) {
            txt_birthday.setText(Utility.dateFormate(parentProfileModel.getTxt_birthday(), "dd-MM-yyyy", "MM-dd-yyyy"));
        }
        if (Utility.isNull(parentProfileModel.getTxt_anywarsarydate())) {
            txt_anivesarydate.setText(Utility.dateFormate(parentProfileModel
                    .getTxt_anywarsarydate(), "dd-MM-yyyy", "MM-dd-yyyy"));
        }
        if (Utility.isNull(parentProfileModel.getStudy())) {
            edt_study.setText(parentProfileModel.getStudy());
        }
        if (Utility.isNull(parentProfileModel.getOocupationDetail())) {
            edt_occdetail.setText(parentProfileModel.getOocupationDetail());
        }
        if (Utility.isNull(parentProfileModel.getEmail())) {
            edt_email.setText(parentProfileModel.getEmail());
        }
        if (Utility.isNull(parentProfileModel.getHomeAddress())) {
            edt_homeaddress.setText(parentProfileModel.getHomeAddress());
        }
        if (Utility.isNull(parentProfileModel.getIdNo())) {
            edt_idno.setText(parentProfileModel.getIdNo());
        }
        if (Utility.isNull(parentProfileModel.getIdType())) {
            edt_idtype.setText(parentProfileModel.getIdType());
        }
        if (Utility.isNull(parentProfileModel.getDateofissue())) {
            txt_dateofissue.setText(Utility.dateFormate(parentProfileModel.getDateofissue(), "dd-MM-yyyy", "MM-dd-yyyy"));
        }
        if (Utility.isNull(parentProfileModel.getDateofexpiry())) {
            txt_dateofexp.setText(Utility.dateFormate(parentProfileModel.getDateofexpiry(), "dd-MM-yyyy", "MM-dd-yyyy"));
        }
        if (Utility.isNull(parentProfileModel.getMobileNo())) {
            edt_phone_no.setText(parentProfileModel.getMobileNo());
        }
        if (Utility.isNull(parentProfileModel.getDesignationOccupationTerm())) {
            edt_designation.setText(parentProfileModel
                    .getDesignationOccupationTerm());
        }
        if (Utility.isNull(parentProfileModel.getOccupationTerm())) {
            edt_occuption.setText(parentProfileModel.getOccupationTerm());
        }
        if (Utility.isNull(parentProfileModel.getOfficeAddress())) {
            edt_address.setText(parentProfileModel.getOfficeAddress());
        }
        if (Utility.isNull(parentProfileModel.getContactNo())) {
            edt_officenumber.setText(parentProfileModel.getContactNo());
        }
        if (Utility.isNull(parentProfileModel.getContactNo())) {
            edt_officenumber.setText(parentProfileModel.getContactNo());
        }
        if (Utility.isNull(parentProfileModel.getTypeGender())) {
            if (parentProfileModel.getTypeGender().contains("Mr.")) {
                spn_gender.setSelection(0);
                typeStr = "Mr.";
            } else if (parentProfileModel.getTypeGender().contains("Mrs.")) {
                spn_gender.setSelection(1);
                typeStr = "Mrs.";
            } else {
                spn_gender.setSelection(2);
                typeStr = "Miss";
            }
        }

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL + parentProfileModel.getAvtar())
                    .apply(options)
                    .into(img_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            Glide.with(mContext)
                    .load(ServiceResource.BASE_IMG_URL + parentProfileModel.getIdproofpath())
                    .apply(options)
                    .into(iv_idproof);
        } catch (Exception e) {
            e.printStackTrace();
        }

        chb_remember.setChecked(parentProfileModel.isIsPickup());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_save:

                if (Utility.isNetworkAvailable(mContext)) {
                    if (!Utility.isNull(txt_birthday.getText().toString())) {
                        txt_birthday.setError(getResources().getString(R.string.Pleaseselectbirthdaydate));
                        Utility.toast(mContext, getResources().getString(R.string.Pleaseselectbirthdaydate));
                    } else if (!Utility.isNull(txt_anivesarydate.getText().toString())) {
                        txt_anivesarydate.setError(getResources().getString(R.string.Pleaseselectaniversarydate));
                        Utility.toast(mContext, getResources().getString(R.string.Pleaseselectaniversarydate));
                    }
                    if (!Utility.isNull(txt_dateofexp.getText().toString())) {
                        txt_dateofexp.setError(getResources().getString(R.string.PleaseselectExpiryDate));
                        Utility.toast(mContext, getResources().getString(R.string.PleaseselectExpiryDate));
                    } else if (!Utility.isNull(txt_dateofissue.getText().toString())) {
                        txt_dateofissue.setError(getResources().getString(R.string.PleaseselectDateOfIssue));
                        Utility.toast(mContext, getResources().getString(R.string.PleaseselectDateOfIssue));
                    } else {
                        AddParentProfile();
                    }
                } else {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
                }
                break;
            case R.id.img_profile:
                selectImage(2);
                break;
            case R.id.iv_idproof:
//                selectImage(1);
                break;
            case R.id.txt_birthday:
                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        txt_birthday.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }

                }, bmYear, bmMonth, bmDay);
                dpd.show();
                break;

            case R.id.txt_anivesarydate:
                DatePickerDialog adpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                txt_anivesarydate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, amYear, amMonth, amDay);
                adpd.show();

                break;

            case R.id.txt_dateofexp:
                DatePickerDialog edpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                txt_dateofexp.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, emYear, emMonth, emDay);
                edpd.show();

                break;

            case R.id.txt_dateofissue:
                DatePickerDialog idpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // Display Selected date in textbox
                                txt_dateofissue.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }

                        }, imYear, imMonth, imDay);
                idpd.show();

                break;

            default:
                break;
        }
    }

    /**
     * showtoast
     *
     * @param msg
     */
    private void toast(String msg) {
        Utility.toast(mContext, msg);
//        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public void findEdittext(View v) {

        for (int i = 0; i < ((LinearLayout) v).getChildCount(); i++) {
            View view = ((LinearLayout) v).getChildAt(i);

            if (view instanceof EditText) {
                if (!Utility.isNull(((EditText) view).getText().toString())) {
                    ((EditText) view).setError("Please Enter " + ((EditText) view).getHint());
                    isNull = true;
                }
            }
            if (view instanceof TextView) {
                if (!Utility.isNull(((TextView) view).getText().toString())) {
                    ((TextView) view).setError("Please Enter " + ((TextView) view).getHint());
                    isNull = true;
                }
            } else if (view instanceof LinearLayout) {
                findEdittext(view);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        typeStr = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * select image from camera and gallry
     *
     * @param i
     */
    private void selectImage(final int i) {
        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (2 == i) {
                        intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    } else {
                        intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERAID);
                    }
                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {
                    Intent intent; //= new Intent(
                    if (i == 2) {
                        intent = new Intent(mContext, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILE);
                    } else {
                        intent = new Intent(mContext, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILEID);
                    }
                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = null;
                fielPath = data.getExtras().getString("imageData_uri");
                byteArray = data.getExtras().getByteArray("imageData_byte");
                img_profile.setImageBitmap(new BitmapFactory().decodeByteArray(byteArray, 0, byteArray.length));
            } else if (requestCode == SELECT_FILE) {
                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                if (imgList != null && imgList.size() > 0) {
                    Bitmap thePic;
                    fielPath = imgList.get(0).getUri().toString();
                    thePic = Utility.getBitmap(fielPath, mContext);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (fielPath.contains(".png") || fielPath.contains(".PNG")) {
                        thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    } else {
                        thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    byteArray = stream.toByteArray();
                    img_profile.setImageBitmap(thePic);
                }
            } else if (requestCode == REQUEST_CAMERAID) {
                Bitmap thumbnail = null;
                fielPathId = data.getExtras().getString("imageData_uri");
                byteArrayID = data.getExtras().getByteArray("imageData_byte");

                if(byteArrayID.length > 0){

                    iv_idproof.setImageBitmap(new BitmapFactory().decodeByteArray(byteArrayID, 0, byteArrayID.length));

                }

            } else if (requestCode == SELECT_FILEID) {
                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                if (imgList != null && imgList.size() > 0) {
                    Uri selectedImageUri = imgList.get(0).getUri();
                    Bitmap bm;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imgList.get(0).getUri().toString(), options);
                    fielPathId = imgList.get(0).getUri().toString();
                    Bitmap thePic;
                    thePic = Utility.getBitmap(imgList.get(0).getUri().toString(), mContext);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (fielPath.contains(".png") || fielPath.contains(".PNG")) {
                        thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    } else {
                        thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    byteArrayID = stream.toByteArray();
                    iv_idproof.setImageBitmap(thePic);
                }
            }
        }
    }

    /**
     * webservice call For parent profile
     */
    public void ParentProfile() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        //	examresult = webcall.getJSONFromSOAPWS(
        //			ServiceResource.PARENTPROFILE_METHODNAME, map,
        //			ServiceResource.PARENTPROFILE_URL);

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PARENTPROFILE_METHODNAME,
                ServiceResource.PARENTPROFILE_URL);
    }


    /**
     * webservice call for addparent profile
     */
    public void AddParentProfile() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (parentProfileModel.getParentProfileID() != null && !parentProfileModel.getParentProfileID().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, parentProfileModel.getParentProfileID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        if (type == ServiceResource.FATHER_PROFILE_INT) {
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_INITIALTERM, "Mr."));
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_RELATION, "Father"));
        } else if (type == ServiceResource.MOTHER_PROFILE_INT) {
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_INITIALTERM, "Mrs."));
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_RELATION, "Mother"));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_INITIALTERM, typeStr));
            arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_RELATION, "Guardian"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_FIRSTNAME, edt_firstname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_LASTNAME, edt_lastname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_FULLNAMEADD, edt_fathername.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_DOB, Utility.dateFormate(txt_birthday.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_DOA, Utility.dateFormate(txt_anivesarydate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_HIGHESTUDY, edt_study.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_OCCUPATION, edt_occuption.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_DESIGNATION, edt_designation.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_OCCUPATIONDETAIL, edt_occdetail.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_CONTACTNOADD, edt_officenumber.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_MOBILENOADD, edt_phone_no.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_EMAIL, edt_email.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_HOMEADDRESS, edt_homeaddress.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_OFFICENUMBER, edt_officenumber.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_IDNO, edt_idno.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_IDTYPE, edt_idtype.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFIL_DATEOFISSUE, Utility.dateFormate(txt_dateofissue.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_DATEOFEXPIRY, Utility.dateFormate(txt_dateofexp.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_ISPICK, chb_remember.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_IDPROOFATTACH, new File(fielPathId).getName()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_PARENTPHOTO, new File(fielPath).getName()));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_PARENTFILE, byteArray));
        arrayList.add(new PropertyVo(ServiceResource.PARENTPROFILE_IDPROOFFILE, byteArrayID));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.PARENTPROFILE_ADD_METHODNAME, ServiceResource.PARENTPROFILE_URL);
    }

    /**
     * get response for all webservice
     */
    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.PARENTPROFILE_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.fatherProfile = new ArrayList<ParentsProfileModel>();
                Global.motherProfile = new ArrayList<ParentsProfileModel>();
                Global.gardianProfile = new ArrayList<ParentsProfileModel>();

                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    ParentsProfileModel model = new ParentsProfileModel();
                    model.setParentProfileID(innerObj.getString(ServiceResource.PARENTPROFILE_PARENTPROFILEID));
                    model.setFullName(innerObj.getString(ServiceResource.PARENTPROFILE_FULLNAME));
                    model.setRelationTypeTerm(innerObj.getString(ServiceResource.PARENTPROFILE_RELATIONTYPETERM));
                    model.setContactNo(innerObj.getString(ServiceResource.PARENTPROFILE_CONTACTNO));
                    model.setMobileNo(innerObj.getString(ServiceResource.PARENTPROFILE_MOBILENO));
                    model.setOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_OCCUPATIONTERM));
                    model.setDesignationOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_DESIGNATIONOCCUPATIONTERM));
                    model.setOfficeAddress(innerObj.getString(ServiceResource.PARENTPROFILE_OFFICEADDRESS));
                    model.setIsPickup(innerObj.getString(ServiceResource.PARENTPROFILE_ISPICKUP));
                    model.setFirstName(innerObj.getString(ServiceResource.PARENTPROFILE_FIRSTNAME_PARSING));
                    model.setLastName(innerObj.getString(ServiceResource.PARENTPROFILE_LASTNAME_PARSING));
                    String birthdaystr = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING).equalsIgnoreCase("null")) {
                        birthdaystr = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    String anyversary = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY).equalsIgnoreCase("null")) {
                        anyversary = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    model.setTxt_birthday(birthdaystr);
                    model.setTxt_anywarsarydate(anyversary);
                    model.setStudy(innerObj.getString(ServiceResource.PARENTPROFILE_HIGHESTSTUDY));
                    model.setEmail(innerObj.getString(ServiceResource.PARENTPROFILE_EMAILID));
                    model.setOocupationDetail(innerObj.getString(ServiceResource.PARENTPROFILE_OCCDETAIL));
                    model.setHomeAddress(innerObj.getString(ServiceResource.PARENTPROFILE_HOMEADDRESSpARSING));
                    model.setIdNo(innerObj.getString(ServiceResource.PARENTPROFILE_IDNOPARSING));
                    model.setIdType(innerObj.getString(ServiceResource.PARENTPROFILE_TYPETERM));
                    String dateofissue = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE).equalsIgnoreCase("null")) {
                        dateofissue = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    String dateofexp = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP).equalsIgnoreCase("null")) {
                        dateofexp = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    model.setDateofissue(dateofissue);
                    model.setDateofexpiry(dateofexp);
                    model.setAvtar(innerObj.getString(ServiceResource.PARENTPROFILE_AVTAR));
                    model.setIdproofpath(innerObj.getString(ServiceResource.PARENTPROFILE_IDPROOFATTACHED));

                    if (innerObj.getString(
                            ServiceResource.PARENTPROFILE_RELATIONTYPETERM).contains("ather")) {
                        Global.fatherProfile.add(model);
                    } else if (innerObj.getString(
                            ServiceResource.PARENTPROFILE_RELATIONTYPETERM).contains("other")) {
                        Global.motherProfile.add(model);
                    } else {
                        Global.gardianProfile.add(model);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (type == ServiceResource.FATHER_PROFILE_INT) {
                edt_fathername.setHint("FatherName");
                ll_spn.setVisibility(View.GONE);
                if (Global.fatherProfile != null && Global.fatherProfile.size() > 0) {
                    parentProfileModel = Global.fatherProfile.get(0);
                }

            } else if (type == ServiceResource.MOTHER_PROFILE_INT) {
                ll_spn.setVisibility(View.GONE);
                edt_fathername.setHint("MotherName");
                if (Global.motherProfile != null && Global.motherProfile.size() > 0) {
                    parentProfileModel = Global.motherProfile.get(0);
                }
            } else {
                edt_fathername.setHint("GuardianName");
                if (Global.gardianProfile != null
                        && Global.gardianProfile.size() > 0) {
                    parentProfileModel = Global.gardianProfile.get(0);
                }
            }
            fill_data();
        }
        if (ServiceResource.PARENTPROFILE_ADD_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.fatherProfile = new ArrayList<ParentsProfileModel>();
                Global.motherProfile = new ArrayList<ParentsProfileModel>();
                Global.gardianProfile = new ArrayList<ParentsProfileModel>();
                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    ParentsProfileModel model = new ParentsProfileModel();
                    model.setParentProfileID(innerObj.getString(ServiceResource.PARENTPROFILE_PARENTPROFILEID));
                    model.setFullName(innerObj.getString(ServiceResource.PARENTPROFILE_FULLNAME));
                    model.setRelationTypeTerm(innerObj.getString(ServiceResource.PARENTPROFILE_RELATIONTYPETERM));
                    model.setContactNo(innerObj.getString(ServiceResource.PARENTPROFILE_CONTACTNO));
                    model.setMobileNo(innerObj.getString(ServiceResource.PARENTPROFILE_MOBILENO));
                    model.setOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_OCCUPATIONTERM));
                    model.setDesignationOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_DESIGNATIONOCCUPATIONTERM));
                    model.setOfficeAddress(innerObj.getString(ServiceResource.PARENTPROFILE_OFFICEADDRESS));
                    model.setIsPickup(innerObj.getString(ServiceResource.PARENTPROFILE_ISPICKUP));
                    model.setAvtar(innerObj.getString(ServiceResource.PARENTPROFILE_AVTAR));
                    model.setIdproofpath(innerObj.getString(ServiceResource.PARENTPROFILE_IDPROOFATTACHED));
                    if (innerObj.getString(
                            ServiceResource.PARENTPROFILE_RELATIONTYPETERM).contains("ather")) {
                        Global.fatherProfile.add(model);
                    } else if (innerObj.getString(
                            ServiceResource.PARENTPROFILE_RELATIONTYPETERM)
                            .contains("other")) {
                        Global.motherProfile.add(model);
                    } else {
                        Global.gardianProfile.add(model);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ParentProfile();
        }
    }
}
