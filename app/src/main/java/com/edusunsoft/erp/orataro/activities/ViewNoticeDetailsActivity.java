package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.NoticeListModel;
import com.edusunsoft.erp.orataro.fragments.HomeWorkListFragment;
import com.edusunsoft.erp.orataro.fragments.NotesFragment;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.io.File;
import java.util.ArrayList;

public class ViewNoticeDetailsActivity extends Activity implements OnClickListener, ResponseWebServices {

    Context mContext;
    ImageView img_back, img_profile;
    TextView txt_title, txt_note, txt_techerName, txt_note_details,
            txt_parent_note, txt_dress_code, txt_strat_date, txt_end_date, txt_ref_link;
    LinearLayout ll_dress_code, ll_strat_date, ll_end_date, ll_parent_note, ll_reference_link;
    Dialog mAddRemarkDialog;

    NoticeListModel notesModel;
    private AdjustableImageView iv_cr_details;
    private RelativeLayout rl_view;
    boolean isFile = false;
    public File file;
    CheckBox chk_finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ViewNoticeDetailsActivity.this;
        notesModel = (NoticeListModel) getIntent().getSerializableExtra("NotesModel");
        LinearLayout ll_finish = (LinearLayout) findViewById(R.id.ll_finish);
        chk_finish = (CheckBox) findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) findViewById(R.id.txtisfinish);

        txtisfinish.setText(getResources().getString(R.string.Checked));

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }

        img_back = (ImageView) findViewById(R.id.img_back);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_note = (TextView) findViewById(R.id.txt_note);
        txt_note_details = (TextView) findViewById(R.id.txt_note_details);
        txt_dress_code = (TextView) findViewById(R.id.txt_dress_code);
        txt_strat_date = (TextView) findViewById(R.id.txt_strat_date);
        txt_end_date = (TextView) findViewById(R.id.txt_end_date);
        txt_ref_link = (TextView) findViewById(R.id.txt_ref_link);
        txt_techerName = (TextView) findViewById(R.id.txt_techerName);
        ll_dress_code = (LinearLayout) findViewById(R.id.ll_dress_code);
        ll_strat_date = (LinearLayout) findViewById(R.id.ll_strat_date);
        ll_end_date = (LinearLayout) findViewById(R.id.ll_end_date);
        ll_reference_link = (LinearLayout) findViewById(R.id.ll_reference_link);
        iv_cr_details = (AdjustableImageView) findViewById(R.id.iv_cr_details);
        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        iv_cr_details.setVisibility(View.VISIBLE);

        if (notesModel.getNoteTitle() != null
                && !notesModel.getNoteTitle().equals("null")) {
            txt_title.setText(notesModel.getNoteTitle());
        }

        if (notesModel.isCheck()) {
            chk_finish.setChecked(true);
        } else {
            chk_finish.setChecked(false);
        }

        if (notesModel.getReferenceLink() != null && !notesModel.getReferenceLink().equals("null")) {
            txt_ref_link.setText(notesModel.getReferenceLink());
        } else {
            txt_ref_link.setVisibility(View.GONE);
        }

        if (notesModel.getNoteTitle() != null && !notesModel.getNoteTitle().equals("null")) {
            txt_note.setText(notesModel.getNoteTitle());
        } else {
            txt_note.setVisibility(View.GONE);
        }

        if (notesModel.getNoteDetails() != null
                && !notesModel.getNoteDetails().equals("null")) {
            txt_note_details.setText(notesModel.getNoteDetails());
        } else {
            txt_note_details.setVisibility(View.GONE);
        }

        if (notesModel.getDressCode() != null && !notesModel.getDressCode().equals("null")) {
            txt_dress_code.setText(notesModel.getDressCode());
        } else {
            ll_dress_code.setVisibility(View.GONE);
        }

        if (notesModel.getActionStartDate() != null && !notesModel.getActionStartDate().equals("null")) {
            txt_strat_date.setText(notesModel.getActionStartDate());
        } else {
            ll_strat_date.setVisibility(View.GONE);
        }

        if (notesModel.getActionEndDate() != null && !notesModel.getActionEndDate().equals("null")) {
            txt_end_date.setText(notesModel.getActionEndDate());
        } else {
            ll_end_date.setVisibility(View.GONE);
        }

        Log.d("getprofilepicture", notesModel.getProfilePicture());

        if (notesModel.getProfilePicture() != null && !notesModel.getProfilePicture().equals("null")) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_user_placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(notesModel.getProfilePicture())
                        .apply(options)
                        .into(img_profile);

                Log.d("getnoteimageprofile", ServiceResource.BASE_IMG_URL1 + notesModel.getProfilePicture());

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            img_profile.setImageResource(R.drawable.photo);

        }

        if (notesModel.getImgUrl() != null && !notesModel.getImgUrl().contains("null")) {

            Log.d("geturl", notesModel.getImgUrl());
            if (notesModel.getImgUrl().contains(".TEXT") || notesModel.getImgUrl().contains(".txt")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.txt);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (notesModel.getImgUrl().contains(".PDF") || notesModel.getImgUrl().contains(".pdf")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.pdf);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (notesModel.getImgUrl().contains(".doc") || notesModel.getImgUrl().contains(".doc")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.doc);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (notesModel.getImgUrl().contains(".docs") || notesModel.getImgUrl().contains(".docs")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.file);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (notesModel.getImgUrl().contains(".xlsx") ||
                    notesModel.getImgUrl().contains("EXCEL") || notesModel.getImgUrl().contains(".xls") || notesModel.getImgUrl().contains(".xlsm")) {
                isFile = true;
                iv_cr_details.setImageResource(R.drawable.excel_file);
                LayoutParams params = new LayoutParams(200, 200);
                iv_cr_details.setLayoutParams(params);
                iv_cr_details.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                try {
                    RequestOptions options = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.photo)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();

                    Glide.with(mContext)
                            .load(notesModel.getImgUrl().replace("//DataFiles//",
                                    "/DataFiles/"))
                            .apply(options)
                            .into(iv_cr_details);

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

            iv_cr_details.setVisibility(View.VISIBLE);

        } else {

            iv_cr_details.setVisibility(View.GONE);

        }

        if (notesModel.getUserName() != null
                && !notesModel.getUserName().contains("null")) {
            txt_techerName.setText(notesModel.getUserName());
        } else {
            txt_techerName.setVisibility(View.GONE);
        }

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                noteListIsApprove(notesModel.getNotesID(), isChecked);

            }
        });


        img_back.setOnClickListener(this);
        iv_cr_details.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                Utility.ISLOADNOTE = false;
                finish();
                break;
            case R.id.iv_cr_details:
                if (!isFile) {
                    Intent i = new Intent(mContext, ZoomImageAcitivity.class);
                    i.putExtra("img", notesModel.getImgUrl());
                    i.putExtra("name", notesModel.getNoteTitle());
                    startActivity(i);
                } else {
                    saveDownload(notesModel.getImgUrl().replace("//DataFiles//", "/DataFiles/"));
                }

                break;
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADNOTE = false;
    }

    public void noteListIsApprove(String assosiationId, boolean isApprove) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Note"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);
    }

    @Override
    public void response(String result, String methodName) {
        NotesFragment.noticeListAdapter.NotifyNotesSingleItem(notesModel.getNotesID(), chk_finish.isChecked());
    }

    private void saveDownload(String url) {
        boolean isDownloading = false;
        String fileName = System.currentTimeMillis() + ".pdf";
        String[] fNamearray = url.split("/");
        if (fNamearray != null && fNamearray.length > 0) {
            fileName = fNamearray[fNamearray.length - 1];
        }

        File f = new File(Utility.getDownloadFilename(""));
        ArrayList<File> fileList = getfile(f);
        for (int i = 0; i < fileList.size(); i++) {
            if (fileList.get(i).getName().equalsIgnoreCase(fileName)) {
                isDownloading = true;
            }
        }

//        file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName);
//        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
//        Uri source = Uri.parse(url);
//        Uri destination = Uri.fromFile(new File(Utility.getDownloadFilename(fileName)));
//        DownloadManager.Request request = new DownloadManager.Request(source);
//        request.setTitle("Orataro " + fileName);
//        request.setDescription("Downloding...");
//        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS, fileName);
//        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//        if (file.exists())
//            file.delete();
//        request.allowScanningByMediaScanner();
//        long id = mgr.enqueue(request);
//        Utility.showToast(getResources().getString(R.string.downloadstarting), mContext);

        DownloadManager mgr = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri source = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(source);

        long id = mgr.enqueue(request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setTitle("Orataro " + fileName)
                .setDescription("Downloading")
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED));
        Utility.Longtoast(mContext, getResources().getString(R.string.downloadstarting) + "\n" + Utility.getDownloadFilename(fileName) + fileName);
    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        ArrayList<File> fileList = new ArrayList<File>();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                fileList.add(listFile[i]);
            }
        }
        return fileList;
    }

}
