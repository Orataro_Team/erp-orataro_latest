package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ImageSelectionActivity;
import com.edusunsoft.erp.orataro.activities.PreviewImageActivity;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StudentProfileModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StudentProfileFragment extends Fragment implements OnItemSelectedListener, OnClickListener, ResponseWebServices {
    CircleImageView img_profile;
    EditText edt_firstname;
    EditText edt_middlename;
    EditText edt_lastname;
    EditText edt_fullname;
    EditText edt_displayname;
    Spinner spn_gender;
    TextView txt_birthday;
    TextView txt_anivesarydate;
    EditText edt_email;
    EditText edt_address1;
    EditText edt_address2;
    EditText edt_cityname;
    EditText edt_state;
    EditText edt_country;
    EditText edt_zipcode;
    Context mContext;
    int bmYear, bmDay, bmMonth;
    int amYear, amDay, amMonth;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private String fielPath = "";
    private byte[] byteArray;
    LinearLayout ll_save;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.studentprofilefragment, null);
        mContext = getActivity();
        ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ll_save = (LinearLayout) v.findViewById(R.id.ll_save);
        img_profile = (CircleImageView) v.findViewById(R.id.img_profile);
        edt_firstname = (EditText) v.findViewById(R.id.edt_firstname);
        edt_middlename = (EditText) v.findViewById(R.id.edt_middlename);
        edt_lastname = (EditText) v.findViewById(R.id.edt_lastname);
        edt_fullname = (EditText) v.findViewById(R.id.edt_fullname);
        edt_displayname = (EditText) v.findViewById(R.id.edt_displayname);
        spn_gender = (Spinner) v.findViewById(R.id.spn_gender);
        txt_birthday = (TextView) v.findViewById(R.id.txt_birthday);
        txt_anivesarydate = (TextView) v.findViewById(R.id.txt_anivesarydate);
        edt_email = (EditText) v.findViewById(R.id.edt_email);
        edt_address1 = (EditText) v.findViewById(R.id.edt_address1);
        edt_address2 = (EditText) v.findViewById(R.id.edt_address2);
        edt_cityname = (EditText) v.findViewById(R.id.edt_cityname);
        edt_state = (EditText) v.findViewById(R.id.edt_state);
        edt_country = (EditText) v.findViewById(R.id.edt_country);
        edt_zipcode = (EditText) v.findViewById(R.id.edt_zipcode);

        getStudentProfile();

        Calendar c = Calendar.getInstance();
        bmYear = c.get(Calendar.YEAR);
        bmMonth = c.get(Calendar.MONTH);
        bmDay = c.get(Calendar.DAY_OF_MONTH);

        amYear = c.get(Calendar.YEAR);
        amMonth = c.get(Calendar.MONTH);
        amDay = c.get(Calendar.DAY_OF_MONTH);

        List<String> categories = new ArrayList<String>();

        categories.add("Mr.");
        categories.add("Mrs.");
        categories.add("Miss");
        categories.add("Dr.");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spn_gender.setAdapter(adapter);
        spn_gender.setOnItemSelectedListener(this);


        txt_birthday.setOnClickListener(this);
        txt_anivesarydate.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        ll_save.setOnClickListener(this);

        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(),new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(img_profile);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_birthday:

                DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                                bmDay = dayOfMonth;
                                bmMonth = (monthOfYear + 1);
                                bmYear = year;
                                txt_birthday.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, bmYear, bmMonth, bmDay);
                dpd.getDatePicker().setMaxDate(new Date().getTime());
                dpd.show();

                break;

            case R.id.ll_save:
                updateProfile();
                break;

            case R.id.txt_anivesarydate:

                DatePickerDialog adpd = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                amDay = dayOfMonth;
                                amMonth = (monthOfYear + 1);
                                amYear = year;
                                txt_anivesarydate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, amYear, amMonth, amDay);

                adpd.getDatePicker().setMaxDate(new Date().getTime());
                adpd.show();

                break;

            case R.id.img_profile:
                selectImage(2);
                break;

        }

    }

    private void selectImage(final int i) {
        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.ChoosefromLibrary),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (2 == i) {
                        intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    } else {
                        intent = new Intent(mContext, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                } else if (items[item].equals(mContext.getResources().getString(R.string.ChoosefromLibrary))) {
                    Intent intent;
                    if (i == 2) {
                        intent = new Intent(mContext, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILE);
                    } else {
                        intent = new Intent(mContext, ImageSelectionActivity.class);
                        intent.putExtra("count", 1);
                        startActivityForResult(intent, SELECT_FILE);
                    }
                } else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = null;
                fielPath = data.getExtras().getString("imageData_uri");
                byteArray = data.getExtras().getByteArray("imageData_byte");
                img_profile.setImageBitmap(new BitmapFactory().decodeByteArray(byteArray, 0, byteArray.length));
                UploadProfilePic();
            } else if (requestCode == SELECT_FILE) {
                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                if (imgList != null && imgList.size() > 0) {
                    Bitmap thePic;
                    fielPath = imgList.get(0).getUri().toString();
                    thePic = Utility.getBitmap(imgList.get(0).getUri().toString(), mContext);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (fielPath.contains(".png") || fielPath.contains(".PNG")) {
                        thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    } else {
                        thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    byteArray = stream.toByteArray();
                    img_profile.setImageBitmap(thePic);
                    UploadProfilePic();
                }

            } else if (requestCode == SELECT_FILE) {

                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                if (imgList != null && imgList.size() > 0) {
                    Bitmap thePic;
                    fielPath = imgList.get(0).getUri().toString();
                    thePic = Utility.getBitmap(imgList.get(0).getUri().toString(), mContext);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (fielPath.contains(".png") || fielPath.contains(".PNG")) {
                        thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    } else {
                        thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    byteArray = stream.toByteArray();
                    img_profile.setImageBitmap(thePic);
                    UploadProfilePic();
                }
            }
        }
    }

    public void getStudentProfile() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETSTUDENTPROFILEDATA_METHODNAME, ServiceResource.PROFILE_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (methodName.equalsIgnoreCase(ServiceResource.GETSTUDENTPROFILEDATA_METHODNAME)) {
            JSONArray jsonObj;
            try {
                jsonObj = new JSONArray(result);
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    Global.StudentProfileModel = new StudentProfileModel();
                    Global.StudentProfileModel.setFirstName(innerObj.getString(ServiceResource.PROFILE_FIRSTNAME));
                    Global.StudentProfileModel.setLastName(innerObj.getString(ServiceResource.PROFILE_LASTNAME));
                    Global.StudentProfileModel.setMiddleName(innerObj.getString(ServiceResource.PROFILE_MIDDLENAME));
                    Global.StudentProfileModel.setFullName(innerObj.getString(ServiceResource.PROFILE_FULLNAME));
                    Global.StudentProfileModel.setInitial(innerObj.getString(ServiceResource.PROFILE_INITIAL));
                    Global.StudentProfileModel.setDOB(innerObj.getString(ServiceResource.PROFILE_DOB));
                    Global.StudentProfileModel.setDateOfAnniversary(innerObj.getString(ServiceResource.PROFILE_DATEOFANNIVERSARY));
                    Global.StudentProfileModel.setEmailID(innerObj.getString(ServiceResource.PROFILE_EMAILID));
                    Global.StudentProfileModel.setAddress1(innerObj.getString(ServiceResource.PROFILE_ADDRESS1));
                    Global.StudentProfileModel.setAddress2(innerObj.getString(ServiceResource.PROFILE_ADDRESS2));
                    Global.StudentProfileModel.setCity(innerObj.getString(ServiceResource.PROFILE_CITY));
                    Global.StudentProfileModel.setState(innerObj.getString(ServiceResource.PROFILE_STATE));
                    Global.StudentProfileModel.setCountry(innerObj.getString(ServiceResource.PROFILE_COUNTRY));
                    Global.StudentProfileModel.setZipCode(innerObj.getString(ServiceResource.PROFILE_ZIPCODE));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            filldata();

        } else if (methodName.equalsIgnoreCase(ServiceResource.UPDATESTUDENTPROFILEDATA_METHODNAME)) {

            try {

                Utility.toast(mContext, result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            getStudentProfile();

        } else if (ServiceResource.CHNAGEPROFILEPHOTOS_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                JSONArray jArray = new JSONArray(result);
                JSONObject jobj = jArray.getJSONObject(0);
                String urlArray = jobj.getString(ServiceResource.MESSAGE);

                String[] url = urlArray.split(",");
                if (url != null && url.length > 0) {
                    new UserSharedPrefrence(mContext).setLOGIN_PROFILEPIC(url[0]);
                    new UserSharedPrefrence(mContext).getLoginModel().setProfilePicture(url[0]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void filldata() {

        try {

            if (Utility.isNull(Global.StudentProfileModel.getFirstName())) {
                edt_firstname.setText(Global.StudentProfileModel.getFirstName());
            } else {
                edt_firstname.setHint("Firstname");
            }
            if (Utility.isNull(Global.StudentProfileModel.getLastName())) {
                edt_lastname.setText(Global.StudentProfileModel.getLastName());
            } else {
                edt_lastname.setHint("Lastname");
            }
            if (Utility.isNull(Global.StudentProfileModel.getMiddleName())) {
                edt_middlename.setText(Global.StudentProfileModel.getMiddleName());
            } else {
                edt_middlename.setHint("Middlename");
            }
            if (Utility.isNull(Global.StudentProfileModel.getAddress1())) {
                edt_address1.setText(Global.StudentProfileModel.getAddress1());
            } else {
                edt_address1.setHint("Address1");
            }
            if (Utility.isNull(Global.StudentProfileModel.getAddress2())) {
                edt_address2.setText(Global.StudentProfileModel.getAddress2());
            } else {
                edt_address2.setHint("Address2");
            }
            if (Utility.isNull(Global.StudentProfileModel.getEmailID())) {
                edt_email.setText(Global.StudentProfileModel.getEmailID());
            } else {
                edt_email.setHint("Email");
            }
            if (Utility.isNull(Global.StudentProfileModel.getCity())) {
                edt_cityname.setText(Global.StudentProfileModel.getCity());
            } else {
                edt_cityname.setHint("City");
            }
            if (Utility.isNull(Global.StudentProfileModel.getState())) {
                edt_state.setText(Global.StudentProfileModel.getState());
            } else {
                edt_state.setHint("State");
            }
            if (Utility.isNull(Global.StudentProfileModel.getCountry())) {
                edt_country.setText(Global.StudentProfileModel.getCountry());
            } else {
                edt_country.setHint("Country");
            }
            if (Utility.isNull(Global.StudentProfileModel.getZipCode())) {
                edt_zipcode.setText(Global.StudentProfileModel.getZipCode());
            } else {
                edt_zipcode.setHint("Zipcode");
            }


            edt_fullname.setText(Global.StudentProfileModel.getFullName());
            edt_displayname.setText(new UserSharedPrefrence(mContext).getLoginModel().getDisplayName());
            txt_birthday.setText(Utility.getDate(Long.valueOf(Global.StudentProfileModel.getDOB()
                    .replace("/Date(", "").replace(")/", "")), "dd-MM-yyyy"));
            txt_anivesarydate.setText(Utility.getDate(Long.valueOf(Global.StudentProfileModel.getDateOfAnniversary()
                    .replace("/Date(", "").replace(")/", "")), "dd-MM-yyyy"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateProfile() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_FIRSTNAME, edt_firstname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_MIDDLENAME, edt_middlename.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_LASTNAME, edt_lastname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_FULLNAME, edt_firstname.getText().toString() + " " + edt_lastname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_INITIAL, spn_gender.getSelectedItem().toString()));
        if (!txt_birthday.getText().toString().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.PROFILE_DOB, Utility.dateFormate(txt_birthday.getText().toString(), "MM-dd-yyyy", "dd-MM-yyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.PROFILE_DOB, null));
        }
        if (!txt_anivesarydate.getText().toString().equalsIgnoreCase("")) {
            arrayList.add(new PropertyVo(ServiceResource.PROFILE_DATEOFANNIVERSARY, Utility.dateFormate(txt_anivesarydate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.PROFILE_DATEOFANNIVERSARY, null));
        }

        arrayList.add(new PropertyVo(ServiceResource.PROFILE_DISPLAYNAME, edt_displayname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_ADDRESS1, edt_address1.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_ADDRESS2, edt_address2.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_CITY, edt_cityname.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_STATE, edt_state.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_COUNTRY, edt_country.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.PROFILE_ZIPCODE, edt_zipcode.getText().toString()));

        Log.d("getRequest11", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.UPDATESTUDENTPROFILEDATA_METHODNAME,
                ServiceResource.PROFILE_URL);

    }

    private void UploadProfilePic() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.NAMEPHOTO, fielPath.trim()));
        if (byteArray != null) arrayList.add(new PropertyVo(ServiceResource.PHOTOFILE, byteArray));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHNAGEPROFILEPHOTOS_METHODNAME, ServiceResource.PARENTPROFILE_URL);

    }

}