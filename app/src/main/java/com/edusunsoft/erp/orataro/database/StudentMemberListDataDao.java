package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StudentMemberListDataDao {

    @Insert
    void insertStudentMemberList(PTCommMemberModel PTCommMemberModel);

    @Query("SELECT * from PTCommMemberModel WHERE ForMember = :forMember")
    List<PTCommMemberModel> getStudentMemberList(String forMember);
    
    @Query("delete from PTCommMemberModel where ForMember =:ForMember")
    void deletememberlist(String ForMember);

}
