package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.util.CustomDialog;

import java.util.ArrayList;

public class HappyGramStudentAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<MyHappyGramModel> studentList;
    private LayoutInflater layoutInfalater;
    public Dialog mPoweroffDialog;

    private int[] colors = new int[]{Color.parseColor("#F2F2F2"), Color.parseColor("#F2F2F2")};

    private String[] emojiesClassName = {"emotion1", "emotion2", "emotion3", "emotion4", "emotion5",
            "emotion6", "emotion7", "emotion8", "emotion9", "emotion10",
            "emotion11", "emotion12", "emotion13", "emotion14", "emotion15",
            "emotion16", "emotion17", "emotion18", "emotion19", "emotion20",
            "emotion21", "emotion22", "emotion23", "emotion24", "emotion25"};

    private int[] emojies = {R.drawable.emotion_01, R.drawable.emotion_02, R.drawable.emotion_03
            , R.drawable.emotion_04, R.drawable.emotion_05, R.drawable.emotion_06
            , R.drawable.emotion_07, R.drawable.emotion_08, R.drawable.emotion_09
            , R.drawable.emotion_10, R.drawable.emotion_11, R.drawable.emotion_12
            , R.drawable.emotion_13, R.drawable.emotion_14, R.drawable.emotion_15
            , R.drawable.emotion_16, R.drawable.emotion_17, R.drawable.emotion_18
            , R.drawable.emotion_19, R.drawable.emotion_20, R.drawable.emotion_21
            , R.drawable.emotion_22, R.drawable.emotion_23, R.drawable.emotion_24
            , R.drawable.emotion_25
    };

    public HappyGramStudentAdapter(Context mContext, ArrayList<MyHappyGramModel> studentList) {
        this.mContext = mContext;
        this.studentList = studentList;
    }

    @Override
    public int getCount() {

        return studentList.size();

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.studentlistraw, null);
        final TextView txtGrno = (TextView) convertView.findViewById(R.id.txtgrno);
        final TextView txtStudentName = (TextView) convertView.findViewById(R.id.txtstudentname);
        final CheckBox chkStudent = (CheckBox) convertView.findViewById(R.id.chkstudent);
        final LinearLayout ll_bottam = (LinearLayout) convertView.findViewById(R.id.ll_bottom);
        final ImageView img_smile = (ImageView) convertView.findViewById(R.id.img_smile);
        final ImageView img_emojies = (ImageView) convertView.findViewById(R.id.img_emojies);
        final AutoCompleteTextView edt_appretion = (AutoCompleteTextView) convertView.findViewById(R.id.edt_appretion);
        final EditText edt_note = (EditText) convertView.findViewById(R.id.edt_note);
        final ImageView imgcross = (ImageView) convertView.findViewById(R.id.imgcross);
        chkStudent.setVisibility(View.VISIBLE);
        txtGrno.setText(studentList.get(position).getRegistationNo());
        txtStudentName.setText(studentList.get(position).getFullName());
        edt_appretion.setText(studentList.get(position).getHg_apprication());
        edt_note.setText(studentList.get(position).getHg_note());
        if (studentList.get(position).getEmotion() != null && !studentList.get(position).getEmotion().equalsIgnoreCase("") && !studentList.get(position).getEmotion().equalsIgnoreCase("null")) {

            img_emojies.setImageResource(strngtointemojies(studentList.get(position).getEmotion()));
            img_emojies.setVisibility(View.VISIBLE);
            imgcross.setVisibility(View.VISIBLE);

        }

        ll_bottam.setVisibility(View.VISIBLE);
        edt_appretion.setVisibility(View.VISIBLE);

        if (studentList.get(position).isChecked()) {

            chkStudent.setChecked(true);

        } else {

            chkStudent.setChecked(false);

        }

        chkStudent.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    studentList.get(position).setChecked(true);

                } else {

                    studentList.get(position).setChecked(false);

                }


            }


        });

        edt_appretion.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {

                    studentList.get(position).setHg_apprication(s.toString());

                }

            }

        });

        edt_note.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s != null && s.length() != 0) {
                    studentList.get(position).setHg_note(s.toString());
                }
            }
        });

        img_smile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_emojies, true);

//                final Dialog mPoweroffDialog = new Dialog(mContext);
//                mPoweroffDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                mPoweroffDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                mPoweroffDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//                mPoweroffDialog.setContentView(R.layout.dialog_emojies);
//                mPoweroffDialog.setCancelable(true);
//                mPoweroffDialog.show();

                GridView grd_emojies = (GridView) mPoweroffDialog.findViewById(R.id.grd_emojies);
                EmojiesAdapter emojieAdapter = new EmojiesAdapter(mContext);
                grd_emojies.setAdapter(emojieAdapter);

                grd_emojies.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                        img_emojies.setVisibility(View.VISIBLE);
                        imgcross.setVisibility(View.VISIBLE);
                        img_emojies.setImageResource(emojies[pos]);
                        studentList.get(position).setEmotion(emojiesClassName[pos]);
                        mPoweroffDialog.dismiss();
                    }
                });
            }
        });

        img_emojies.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                img_emojies.setVisibility(View.INVISIBLE);
                imgcross.setVisibility(View.INVISIBLE);
                studentList.get(position).setEmotion("");
            }
        });

        return convertView;

    }

    public ArrayList<MyHappyGramModel> getSelectedStudentList() {

        ArrayList<MyHappyGramModel> seletedStudentList = new ArrayList<MyHappyGramModel>();

        if (studentList != null && studentList.size() > 0) {
            for (int i = 0; i < studentList.size(); i++) {
                if (studentList.get(i).isChecked()) {
                    seletedStudentList.add(studentList.get(i));
                    Log.d("getAddedCheckedList", seletedStudentList.toString());
                }
            }

            return seletedStudentList;

        } else {

            return null;

        }

    }

    public int strngtointemojies(String str) {
        if (str != null && !str.equalsIgnoreCase("") && !str.equalsIgnoreCase("null")) {
            int pos = 0;
            try {
                pos = Integer.valueOf(str.substring(str.length() - 2, str.length()));
            } catch (NumberFormatException e) {
                pos = Integer.valueOf(str.substring(str.length() - 1, str.length()));
            }
            return emojies[pos - 1];
        } else {
            return emojies[0];
        }
    }
}
