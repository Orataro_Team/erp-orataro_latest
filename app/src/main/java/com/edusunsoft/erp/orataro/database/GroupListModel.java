package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "GroupList")
public class GroupListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "IsAutoApprovePendingMember")
    @SerializedName("IsAutoApprovePendingMember")
    public boolean IsAutoApprovePendingMember;

    @ColumnInfo(name = "IsAutoApprovePendingPost")
    @SerializedName("IsAutoApprovePendingPost")
    public boolean IsAutoApprovePendingPost;

    @ColumnInfo(name = "IsAutoApprovePendingAlbums")
    @SerializedName("IsAutoApprovePendingAlbums")
    public boolean IsAutoApprovePendingAlbums;

    @ColumnInfo(name = "IsAutoApprovePendingAttachment")
    @SerializedName("IsAutoApprovePendingAttachment")
    public boolean IsAutoApprovePendingAttachment;

    @ColumnInfo(name = "IsAutoApprovePendingPolls")
    @SerializedName("IsAutoApprovePendingPolls")
    public boolean IsAutoApprovePendingPolls;

    @ColumnInfo(name = "groupName")
    @SerializedName("groupName")
    public String groupName;

    @ColumnInfo(name = "GroupTypeID")
    @SerializedName("GroupTypeID")
    public String GroupTypeID;

    @ColumnInfo(name = "WallID")
    @SerializedName("WallID")
    public String WallID;

    @ColumnInfo(name = "TotalMemeber")
    @SerializedName("TotalMemeber")
    public String TotalMemeber;

    @ColumnInfo(name = "GroupSubject")
    @SerializedName("GroupSubject")
    public String GroupSubject;

    @ColumnInfo(name = "UserName")
    @SerializedName("UserName")
    public String UserName;

    @ColumnInfo(name = "groupMember")
    @SerializedName("groupMember")
    public String groupMember;

    @ColumnInfo(name = "groupCreate")
    @SerializedName("groupCreate")
    public String groupCreate;

    @ColumnInfo(name = "groupId")
    @SerializedName("groupId")
    public String groupId;

    @ColumnInfo(name = "view")
    @SerializedName("view")
    public String view;

    @ColumnInfo(name = "share")
    @SerializedName("share")
    public String share;

    @ColumnInfo(name = "disscusion")
    @SerializedName("disscusion")
    public String disscusion;

    @ColumnInfo(name = "teacherCount")
    @SerializedName("teacherCount")
    public String teacherCount;

    @ColumnInfo(name = "StudentCount")
    @SerializedName("StudentCount")
    public String StudentCount;

    @ColumnInfo(name = "groupImage")
    @SerializedName("groupImage")
    public String groupImage;

    @ColumnInfo(name = "groupstatus")
    @SerializedName("groupstatus")
    public String groupstatus;

    public String getWallID() {
        return WallID;
    }

    public void setWallID(String wallID) {
        WallID = wallID;
    }

    public String getGroupTypeID() {
        return GroupTypeID;
    }

    public void setGroupTypeID(String groupTypeID) {
        GroupTypeID = groupTypeID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public boolean isIsAutoApprovePendingMember() {
        return IsAutoApprovePendingMember;
    }

    public void setIsAutoApprovePendingMember(boolean isAutoApprovePendingMember) {
        IsAutoApprovePendingMember = isAutoApprovePendingMember;
    }

    public boolean isIsAutoApprovePendingPost() {
        return IsAutoApprovePendingPost;
    }

    public void setIsAutoApprovePendingPost(boolean isAutoApprovePendingPost) {
        IsAutoApprovePendingPost = isAutoApprovePendingPost;
    }

    public boolean isIsAutoApprovePendingAlbums() {
        return IsAutoApprovePendingAlbums;
    }

    public void setIsAutoApprovePendingAlbums(boolean isAutoApprovePendingAlbums) {
        IsAutoApprovePendingAlbums = isAutoApprovePendingAlbums;
    }

    public boolean isIsAutoApprovePendingAttachment() {
        return IsAutoApprovePendingAttachment;
    }

    public void setIsAutoApprovePendingAttachment(
            boolean isAutoApprovePendingAttachment) {
        IsAutoApprovePendingAttachment = isAutoApprovePendingAttachment;
    }

    public boolean isIsAutoApprovePendingPolls() {
        return IsAutoApprovePendingPolls;
    }

    public void setIsAutoApprovePendingPolls(boolean isAutoApprovePendingPolls) {
        IsAutoApprovePendingPolls = isAutoApprovePendingPolls;
    }

    public String getGroupSubject() {
        return GroupSubject;
    }

    public void setGroupSubject(String groupSubject) {
        GroupSubject = groupSubject;
    }

    public String getTotalMemeber() {
        return TotalMemeber;
    }

    public void setTotalMemeber(String totalMemeber) {
        TotalMemeber = totalMemeber;
    }

    public String getGroupCreate() {
        return groupCreate;
    }

    public void setGroupCreate(String groupCreate) {
        this.groupCreate = groupCreate;
    }

    public String getGroupMember() {
        return groupMember;
    }

    public void setGroupMember(String groupMember) {
        this.groupMember = groupMember;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupstatus() {
        return groupstatus;
    }

    public void setGroupstatus(String groupstatus) {
        this.groupstatus = groupstatus;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getStudentCount() {
        return StudentCount;
    }

    public void setStudentCount(String studentCount) {
        StudentCount = studentCount;
    }

    public String getTeacherCount() {
        return teacherCount;
    }

    public void setTeacherCount(String teacherCount) {
        this.teacherCount = teacherCount;
    }

    public String getDisscusion() {
        return disscusion;
    }

    public void setDisscusion(String disscusion) {
        this.disscusion = disscusion;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    @Override
    public String toString() {
        return "GroupListModel{" +
                "id=" + id +
                ", IsAutoApprovePendingMember=" + IsAutoApprovePendingMember +
                ", IsAutoApprovePendingPost=" + IsAutoApprovePendingPost +
                ", IsAutoApprovePendingAlbums=" + IsAutoApprovePendingAlbums +
                ", IsAutoApprovePendingAttachment=" + IsAutoApprovePendingAttachment +
                ", IsAutoApprovePendingPolls=" + IsAutoApprovePendingPolls +
                ", groupName='" + groupName + '\'' +
                ", GroupTypeID='" + GroupTypeID + '\'' +
                ", WallID='" + WallID + '\'' +
                ", TotalMemeber='" + TotalMemeber + '\'' +
                ", GroupSubject='" + GroupSubject + '\'' +
                ", UserName='" + UserName + '\'' +
                ", groupMember='" + groupMember + '\'' +
                ", groupCreate='" + groupCreate + '\'' +
                ", groupId='" + groupId + '\'' +
                ", view='" + view + '\'' +
                ", share='" + share + '\'' +
                ", disscusion='" + disscusion + '\'' +
                ", teacherCount='" + teacherCount + '\'' +
                ", StudentCount='" + StudentCount + '\'' +
                ", groupImage='" + groupImage + '\'' +
                ", groupstatus='" + groupstatus + '\'' +
                '}';
    }
}
