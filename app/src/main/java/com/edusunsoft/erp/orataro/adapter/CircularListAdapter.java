package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddDataByTeacherActivity;
import com.edusunsoft.erp.orataro.activities.CheckedStudentListActivity;
import com.edusunsoft.erp.orataro.activities.ViewCircularDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.CircularListDataDao;
import com.edusunsoft.erp.orataro.database.CircularListModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GenerallWallDataDao;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CircularListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context context;
    private TextView homeworkListSubject;
    private ImageView list_image, iv_cr_detail_chart;
    private TextView txt_month;
    private TextView txt_date;
    private TextView tv_cr_date, txt_subject, txt_sub_detail, txt_teacher_name;
    private LinearLayout ll_view;
    private LinearLayout ll_date, ll_date_img;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private static final int ID_CHECKED = 7;
    private RefreshListner listner;
    private List<CircularListModel> circularModels, copyList;
    private int pos = 0;
    public static boolean isFinish;
    // color code
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    public String DELETE_ID = "";
    CircularListDataDao circularListDataDao;
    GenerallWallDataDao generallWallDataDao;

    public CircularListAdapter(Context context, List<CircularListModel> circularModels, RefreshListner listner) {
        this.context = context;
        this.circularModels = circularModels;
        this.listner = listner;
        copyList = new ArrayList<CircularListModel>();
        copyList.addAll(circularModels);
    }

    @Override
    public int getCount() {
        return circularModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.circular_listraw, parent, false);

        LinearLayout ll_finish = (LinearLayout) convertView.findViewById(R.id.ll_finish);
        CheckBox chk_finish = (CheckBox) convertView.findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) convertView.findViewById(R.id.txtisfinish);
        txtisfinish.setText(context.getResources().getString(R.string.isAccept));
        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }

        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);

        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsEdit() || Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsDelete()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.GONE);
            }
        } else {
            ll_editdelete.setVisibility(View.GONE);
        }

        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        iv_cr_detail_chart = (ImageView) convertView.findViewById(R.id.iv_cr_detail_chart);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        tv_cr_date = (TextView) convertView.findViewById(R.id.tv_cr_date);
        txt_subject = (TextView) convertView.findViewById(R.id.txt_subject);
        txt_sub_detail = (TextView) convertView.findViewById(R.id.txt_sub_detail);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        txt_teacher_name = (TextView) convertView.findViewById(R.id.txt_teacher_name);
        if (new UserSharedPrefrence(context).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            txt_teacher_name.setVisibility(View.VISIBLE);
        }

        final QuickAction quickActionForEditOrDelete;
        ActionItem actionEdit, actionDelete, actionChecked;
        actionEdit = new ActionItem(ID_EDIT, "Edit", context.getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, "Delete", context.getResources().getDrawable(R.drawable.delete));
        actionChecked = new ActionItem(ID_CHECKED, "Checked", context.getResources().getDrawable(R.drawable.check_icon));
        quickActionForEditOrDelete = new QuickAction(context, QuickAction.VERTICAL);

        if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsCreate()) {
            quickActionForEditOrDelete.addActionItem(actionEdit);
        }

        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.addActionItem(actionChecked);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos, int actionId) {

                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_EDIT) {
                    int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsEdit()) {
                        btnClick(editPos, 1, txt_subject);
                    } else {
                        Utility.toast(context, ServiceResource.TOASTPERMISSIONMSG);
                    }
                } else if (actionId == ID_DELETE) {
                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    Utility.deleteDialog(context, context.getResources().getString(R.string.Circular),
                            circularModels.get(deletepos).getCr_subject(), new Popup() {

                                @Override
                                public void deleteYes() {
                                    if (Utility.ReadWriteSetting(ServiceResource.CIRCULAR).getIsDelete()) {
                                        Log.d("deleteid", circularModels.get(deletepos).getCircularID());
                                        DELETE_ID = circularModels.get(deletepos).getCircularID();
                                        deleteCircular(circularModels.get(deletepos).getCircularID());
                                    } else {
                                        Utility.toast(context, ServiceResource.TOASTPERMISSIONMSG);
                                    }

                                }

                                @Override
                                public void deleteNo() {

                                }
                            });

                } else if (actionId == ID_CHECKED) {

                    /*commented By Krishna : 17-05-2019 Redirect to CheckHomework List of Student*/

                    Intent studentlistintent = new Intent(context, CheckedStudentListActivity.class);
                    studentlistintent.putExtra("FROMSSTR", "Circular");
                    studentlistintent.putExtra("AssociationID", circularModels.get(position).getCircularID());

                    if (circularModels.get(position).getGradeID().equalsIgnoreCase(null) || circularModels.get(position).getGradeID().equalsIgnoreCase("null")) {
                        studentlistintent.putExtra("GradeID", "");
                    } else {
                        studentlistintent.putExtra("GradeID", circularModels.get(position).getGradeID());
                    }
                    if (circularModels.get(position).getDivisionID().equalsIgnoreCase(null) || circularModels.get(position).getDivisionID().equalsIgnoreCase("null")) {
                        studentlistintent.putExtra("DivisionID", "");
                    } else {
                        studentlistintent.putExtra("DivisionID", circularModels.get(position).getDivisionID());
                    }
                    studentlistintent.putExtra("AssociationType", "Circular");
                    context.startActivity(studentlistintent);

                    /*END*/


                }


            }


        });

        quickActionForEditOrDelete.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickActionForEditOrDelete.show(v);
            }

        });

        convertView.setBackgroundColor(colors[position % colors.length]);

        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);

        if (circularModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        ll_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        if (circularModels.get(position).isRead()) {
            list_image.setImageResource(R.drawable.double_tick_sky_blue);
        } else {
            list_image.setImageResource(R.drawable.tick_sky_blue);
        }

        txt_date.setText(circularModels.get(position).getYear());
        tv_cr_date.setText(circularModels.get(position).getCr_date());
        txt_subject.setText(circularModels.get(position).getCr_subject());
        txt_sub_detail.setText(circularModels.get(position).getCr_detail_txt());
        txt_teacher_name.setText(circularModels.get(position).getTeacher_name());
        if (circularModels.get(position).getCr_detail_txt() != null) {
            if (circularModels.get(position).getCr_detail_txt().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = circularModels.get(position).getCr_detail_txt().substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(tempText.substring(Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {
                        if (!circularModels.get(position).isRead()) {
                            if (Utility.isNetworkAvailable(context)) {
                                pos = position;
                                HomeworkListIsRead(circularModels
                                        .get(position).getCircularID());
                            } else {
                                Utility.showAlertDialog(context,
                                        context.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                            }
                        } else {
                            btnClick(position, 0, txt_subject);
                        }
                    }
                };

                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                txt_sub_detail.setMovementMethod(LinkMovementMethod.getInstance());
                txt_sub_detail.setText(text, BufferType.SPANNABLE);
            } else {
                txt_sub_detail.setText(circularModels.get(position).getCr_detail_txt());
            }
        }

        iv_cr_detail_chart.setImageResource(circularModels.get(position).getCr_detail());
        txt_month.setText(circularModels.get(position).getCr_month().toUpperCase());

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!circularModels.get(position).isRead()) {
                    if (Utility.isNetworkAvailable(context)) {
                        pos = position;
                        HomeworkListIsRead(circularModels.get(position).getCircularID());
                    } else {
                        Utility.showAlertDialog(context, context.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                    }
                } else {
                    btnClick(position, 0, v);
                }
            }
        });

        if (circularModels.get(position).isAccept()) {
            chk_finish.setChecked(true);
        } else {
            chk_finish.setChecked(false);
        }

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pos = position;
                isFinish = true;
                circularListIsApprove(circularModels.get(position).getCircularID(), isChecked);
            }
        });

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        circularModels.clear();
        if (charText.length() == 0) {
            circularModels.addAll(copyList);
        } else {
            for (CircularListModel vo : copyList) {
                if (vo.getCr_subject().toLowerCase(Locale.getDefault()).contains(charText) || vo.getCr_detail_txt().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getStr_dateofcircular().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getCr_subject().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getSubjectID().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getTeacher_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    circularModels.add(vo);
                }

            }

        }
        this.notifyDataSetChanged();
    }

    public void NotifyCircularItemData(String id, boolean checked) {

        for (int i = 0; i < circularModels.size(); i++) {

            if (circularModels.get(i).getCircularID().equalsIgnoreCase(id)) {
                circularModels.get(pos).setAccept(checked);
                circularModels.set(pos, circularModels.get(pos));
                notifyDataSetChanged();
            }
        }
    }

    public void HomeworkListIsRead(String assosiationId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));

        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Circular"));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void circularListIsApprove(String assosiationId, boolean isApprove) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Circular"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME, ServiceResource.CHECK_URL);
    }


    public void deleteCircular(String id) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.PRIMARYID, id));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "Circular"));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.ASSOCIATIONDELETE_METHODNAME, ServiceResource.CHECK_URL);

    }

    public void btnClick(int position, int i, View v) {

        if (i == 1) {

            if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {

                Intent intent = new Intent(context, AddDataByTeacherActivity.class);
                intent.putExtra("gradeId", circularModels.get(position).getGradeID());
                intent.putExtra("divisionId", circularModels.get(position).getDivisionID());
                intent.putExtra("subjectId", circularModels.get(position).getSubjectID());
                intent.putExtra("isFrom", ServiceResource.CIRCULAR_FLAG);
                intent.putExtra("isEdit", true);
                intent.putExtra("CircularModel", circularModels.get(position));
                context.startActivity(intent);

            }

        } else {

            circularModels.get(position).setRead(true);
            circularModels.set(position, circularModels.get(position));
            notifyDataSetChanged();
            Intent intent = new Intent(context, ViewCircularDetailsActivity.class);
            intent.putExtra("Crname", circularModels.get(position).getCr_subject());
            intent.putExtra("Crdetails", circularModels.get(position).getImgUrl());
            intent.putExtra("Crdetails_txt", circularModels.get(position).getCr_detail_txt());
            intent.putExtra("CrTeacher_name", circularModels.get(position).getTeacher_name());
            intent.putExtra("CrTeacher_img", circularModels.get(position).getTeacher_img());
            intent.putExtra("isApporve", circularModels.get(position).isAccept());
            intent.putExtra("id", circularModels.get(position).getCircularID());
            intent.putExtra("referencelink", circularModels.get(position).getReferenceLink());
            context.startActivity(intent);

        }

    }


    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            btnClick(pos, 0, txt_subject);
        } else if (ServiceResource.APPROVE_METHODNAME.equalsIgnoreCase(methodName)) {
            // Commented By Krishna : 18-09-2020 -12:22 - Added to Not refresh after back from detail activity.
            circularModels.get(pos).setAccept(isFinish);
            circularModels.set(pos, circularModels.get(pos));
            notifyDataSetChanged();
            /*END*/
//            listner.refresh(methodName);
        } else if (ServiceResource.ASSOCIATIONDELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            circularListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(context).circularListDataDao();
            circularListDataDao.deletecircularItemByCircularID(DELETE_ID);
            generallWallDataDao = ERPOrataroDatabase.getERPOrataroDatabase(context).generawallDataDao();
            generallWallDataDao.deleteWallItemByAssID(DELETE_ID);
            listner.refresh(methodName);
            Utility.showToast(context.getResources().getString(R.string.circulardelete), context);
        }

    }

}
