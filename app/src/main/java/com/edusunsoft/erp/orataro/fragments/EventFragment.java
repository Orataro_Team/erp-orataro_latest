package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ICheckboxSelected;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.CreateEventActivity;
import com.edusunsoft.erp.orataro.adapter.DateListAdapter;
import com.edusunsoft.erp.orataro.adapter.EventsListAdapter;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView;
import com.edusunsoft.erp.orataro.model.CalendarEventModel;
import com.edusunsoft.erp.orataro.model.CalenderEventModel;
import com.edusunsoft.erp.orataro.model.GetProjectTypeModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimeZone;

public class EventFragment extends Fragment implements AdapterView.OnItemClickListener, ICheckboxSelected, ResponseWebServices, RefreshListner, View.OnClickListener {

    public static String GRADEID = "gradeid";
    public static String DIVISIONID = "divisionid";
    public static String SUBJECTID = "subjectid";
    public static String FROM = "isFrom";


    Context mContext;
    ImageView img_back;
    PullAndLoadListView lst_events;
    LinearLayout ll_add_new;
    EventsListAdapter eventsListAdapter;
    DateListAdapter date_list_Adapter;
    ListView lst_date;
    Dialog mDialog;

    ArrayList<CalendarEventModel> calendarEventModels;
    CalendarEventModel calendarEventModel;
    String month = "JAN";
    int year;
    Button btn_syncevent;
    LinearLayout ll_menu;

    QuickAction quickActionForEditOrDelete;
    ActionItem actionEdit, actionType;
    private ArrayList<GetProjectTypeModel> getProjectTypes;
    private static final int ID_SAVE = 5;
    private static final int ID_TYPE = 7;
    private static final int ID_DELETE = 6;

    private View v;
    private ImageView imgLeftHeader, imgRightHeader;
    private TextView txtHeader;

    public static final EventFragment newInstance(String gradeId, String divisionId, String subjectId, String from) {

        EventFragment circularFragment = new EventFragment();
        Bundle bdl = new Bundle();
        bdl.putString(GRADEID, gradeId);
        bdl.putString(DIVISIONID, divisionId);
        bdl.putString(SUBJECTID, subjectId);
        bdl.putString(FROM, from);circularFragment.setArguments(bdl);

        return circularFragment;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.activity_events_details, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Initialization(v);

        return v;

    }

    private void Initialization(View view) {

        mContext = getActivity();
        year = Calendar.getInstance().get(Calendar.YEAR);

        try {
            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Events) + "(" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
            HomeWorkFragmentActivity.btn_menu.setImageResource(R.drawable.back);
            HomeWorkFragmentActivity.btn_menu.setRotation(-90);
            HomeWorkFragmentActivity.btn_menu.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        lst_events = (PullAndLoadListView) view.findViewById(R.id.lst_events);
        btn_syncevent = (Button) view.findViewById(R.id.btn_syncevent);
//        ll_menu = (LinearLayout) view.findViewById(R.id.ll_menu);
        ll_add_new = (LinearLayout) view.findViewById(R.id.ll_add_new);
        ll_add_new.setOnClickListener(this);

        quickActionForEditOrDelete = new QuickAction(mContext,
                QuickAction.VERTICAL);

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.EVENT).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.GONE);
        }

        Utility.ISLOADEVENT = false;

        lst_events.setOnItemClickListener(this);
        btn_syncevent.setOnClickListener(this);
//        img_back.setOnClickListener(this);
//        ll_menu.setOnClickListener(this);

        getProjectType();

        if (Utility.isNetworkAvailable(mContext)) {
            calenderEvent(String.valueOf(year), true);
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

        lst_events.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            public void onRefresh() {

                if (Utility.isNetworkAvailable(mContext)) {

                    calenderEvent(String.valueOf(year), true);

                } else {

                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                }

            }

        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void refresh(String methodName) {

        if (Utility.isNetworkAvailable(mContext)) {

            calenderEvent(String.valueOf(year), true);

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

        }
    }

    @Override
    public void onResume() {

        super.onResume();

        /*commented By Krishna : 24-05-2019 - Get List of Event*/

        if (Utility.isNetworkAvailable(mContext)) {
            if (Utility.ISLOADEVENT) {
                calenderEvent(String.valueOf(year), true);
            }
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

        /*END*/
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                getActivity().finish();
                break;
            case R.id.img_menu:
                quickActionForEditOrDelete.show(HomeWorkFragmentActivity.btn_menu);
                break;
            case R.id.btn_syncevent:
                //
                break;
            case R.id.ll_add_new:

                /*commented By Krishna : 24-05-2019 - Redirect to Create Event Activity*/

                Intent eventIntent = new Intent(mContext, CreateEventActivity.class);
                startActivity(eventIntent);

                /*END*/

                break;

            default:
                break;

        }
    }


    public void calenderEvent(String year, boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_YEAR, year));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CALENDERDATA_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));

        if (Utility.isTeacher(mContext)) {
            arrayList.add(new PropertyVo(ServiceResource.GradeID, null));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            Log.d("calenderRequest", arrayList.toString());
        }

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CALENDERDATA_METHODNAME, ServiceResource.CALENDERDATA_URL);

    }

    public void getProjectType() {

        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY,
                "eventType"));

        Log.d("getProjecttype", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.LOGIN_GETPROJECTTYPE,
                ServiceResource.LOGIN_URL);

    }

    public final void addEventCalender() {
        if (Build.VERSION.SDK_INT >= 14) {
            for (int i = 0; i < calendarEventModels.size(); i++) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                Calendar calendar = Calendar.getInstance();
                try {
                    calendar.setTime(simpleDateFormat.parse(Utility.getDate(Long.valueOf(calendarEventModels.get(i).getStartDate()
                                    .replace("/Date(", "").replace(")/", "")),
                            "MM/dd/yyyy")));
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void saveImageMenu() {
//        actionEdit = new ActionItem(ID_SAVE, "Sync Calendar", mContext.getResources().getDrawable(R.drawable.save31));
        actionType = new ActionItem(ID_TYPE, "Filters", mContext.getResources().getDrawable(R.drawable.add));
//        quickActionForEditOrDelete.addActionItem(actionEdit);
        String[] arratType = new String[getProjectTypes.size() + 1];
        for (int i = 0; i < getProjectTypes.size(); i++) {
            arratType[i] = getProjectTypes.get(i).getTerm();
            if (i == (getProjectTypes.size() - 1)) {
                arratType[i + 1] = "Holiday";
            }
        }

        quickActionForEditOrDelete.addActionItemChild(actionType, arratType, this::selectedcheckbox);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos,
                                    int actionId) {
                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_SAVE) {
                    addEventCalender();
                }
            }
        });

    }

    @Override
    public void selectedcheckbox(String type, boolean isSelected, boolean isAllFalse) {

        quickActionForEditOrDelete.dismiss();
        if (eventsListAdapter != null) {
            eventsListAdapter.fileter(type, isSelected, isAllFalse);
        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {

            Log.d("response", result);

            JSONArray hJsonArray;

            try {

                if (result.contains("\"success\":0")) {

                } else {

                    hJsonArray = new JSONArray(result);
                    getProjectTypes = new ArrayList<GetProjectTypeModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        GetProjectTypeModel model = new GetProjectTypeModel();
                        model.setCategory(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
                        model.setIsDefault(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                        model.setOrderNo(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                        model.setTerm(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_TERM));
                        model.setTermID(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_TERMID));
                        getProjectTypes.add(model);
                        Log.d("getProjectType", getProjectTypes.toString());

                    }
                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

            ArrayList<GetProjectTypeModel> getProjectType_models = new ArrayList<GetProjectTypeModel>();

            if (getProjectTypes != null && getProjectTypes.size() > 0) {

                for (int i = 0; i < getProjectTypes.size(); i++) {

                    if (getProjectTypes.get(i).getCategory().equalsIgnoreCase("EventType")) {
                        getProjectType_models.add(getProjectTypes.get(i));
                    }
                }
            }

            saveImageMenu();

        } else if (ServiceResource.CALENDERDATA_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.writeToFile(result, methodName, mContext);
            parsecalendar(result);

        }
    }

    public void parsecalendar(String result) {

        Log.d("calenderResult", result);

        JSONArray jsonObj;

        try {

            Global.calenderdEventList = new ArrayList<CalendarEventModel>();

            jsonObj = new JSONArray(result);
            Global.calenderdDataList = new ArrayList<CalenderEventModel>();

            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject innerObj = jsonObj.getJSONObject(i);
                CalendarEventModel calendarEventModel = new CalendarEventModel();
                Log.v("long date", innerObj.getString(ServiceResource.CALENDERDATA_START) + "");
                String date1 = (Utility.getDate(Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.CALENDERDATA_START), "yyyy/MM/dd"),
                        "EEEE/dd/MMM/yyyy"));

                String[] arraydate = date1.split("[/]");

                if (arraydate != null && arraydate.length > 0) {
                    Log.v("date", arraydate[0] + "" + arraydate[1] + "" + arraydate[2]);
                    calendarEventModel.setCl_date(arraydate[1] + " " + arraydate[0].substring(0, 3));
                    calendarEventModel.setMonth(arraydate[2]);
                    calendarEventModel.setYear(arraydate[3]);
                }
                calendarEventModel.setStartDate(String.valueOf(Utility.dateToMilliSeconds(innerObj.getString(ServiceResource.CALENDERDATA_START), "yyyy/MM/dd"))
                        .replace("/Date(", "").replace(")/", ""));

                calendarEventModel.setType(innerObj.getString(ServiceResource.CALENDERDATA_TYPE));
                calendarEventModel.setCl_subject(innerObj.getString(ServiceResource.CALENDERDATA_TITLE));
                calendarEventModel.setReferenceLink(innerObj.getString(ServiceResource.CALENDERDATA_REFERENCELINK));
                calendarEventModel
                        .setCl_detail_txt(innerObj.getString(ServiceResource.CALENDERDATA_ACTIVITYDETAIL));
                calendarEventModel.setActivityID(innerObj.getString(ServiceResource.CALENDERDATA_ACTIVITYID));
                calendarEventModel.setCreateBy(innerObj.getString(ServiceResource.CALENDERDATA_CREATEDBY));
                Global.calenderdEventList.add(calendarEventModel);

                if (Global.calenderdEventList != null && Global.calenderdEventList.size() > 0) {

                    Collections.sort(Global.calenderdEventList, new EventFragment.SortedDate());
                    Collections.reverse(Global.calenderdEventList);

                }

                for (int j = 0; j < Global.calenderdEventList.size(); j++) {
                    if (j != 0) {
                        Log.v("Month", Global.calenderdEventList.get(j).getMonth());
                        String temp = Global.calenderdEventList.get(j - 1).getMonth();
                        if (temp.equalsIgnoreCase(Global.calenderdEventList.get(j).getMonth())) {
                            Global.calenderdEventList.get(j).setVisibleMonth(false);
                        } else {
                            Global.calenderdEventList.get(j).setVisibleMonth(true);
                        }
                    } else {
                        Global.calenderdEventList.get(j).setVisibleMonth(true);
                    }


                }
                //				adapter = new CalendarAdapter(getActivity(), month,Global.calenderdDataList,CalendarFragment.this);


                setAdapter();
                //new EventList().execute();


            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void setAdapter() {

        if (new UserSharedPrefrence(mContext).getCALENDAREVENT().equalsIgnoreCase("0")) {
            new UserSharedPrefrence(mContext).setCALENDAREVENT(Global.calenderdEventList.size() + "");
        } else {
            if (Integer.valueOf(new UserSharedPrefrence(mContext).getCALENDAREVENT()) < Global.calenderdEventList.size()) {
                new UserSharedPrefrence(mContext).setCALENDAREVENT(Global.calenderdEventList.size() + "");
            }
        }

        eventsListAdapter = new EventsListAdapter(mContext, Global.calenderdEventList, this);
        lst_events.setAdapter(eventsListAdapter);
//
//        int k = 0;
//        for (int i = 0; i < Global.calenderdEventList.size(); i++) {
//
//            if (month.length() >= 3) {
//
//                if (Global.calenderdEventList.get(i).getMonth().equalsIgnoreCase(month.substring(0, 3))) {
//                    k = i;
//                    break;
//
//                }
//            }
//        }

//        final int pos = k;
//        lst_events.post(new Runnable() {
//            @Override
//            public void run() {
//                //call smooth scroll
//                if (lst_events.getCount() > pos + 7) {
//                    //				lst_events.getFirstVisiblePosition();
//                    lst_events.smoothScrollToPositionFromTop(pos, 0, 0);
//                    //					lst_events.smo
//
//                } else {
//                    lst_events.smoothScrollToPositionFromTop(pos, 0, 0);
//                }
//            }
//        });
    }


    public class SortedDate implements Comparator<CalendarEventModel> {
        @Override
        public int compare(CalendarEventModel o1, CalendarEventModel o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    }

}
