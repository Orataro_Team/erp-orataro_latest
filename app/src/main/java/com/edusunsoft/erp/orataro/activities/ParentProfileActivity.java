package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.ParentProfileFragment;
import com.edusunsoft.erp.orataro.model.ParentsProfileModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParentProfileActivity extends FragmentActivity implements OnClickListener, ResponseWebServices {

    private ImageView btn_back;
    private Context mContext = ParentProfileActivity.this;
    public static int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_profile);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        btn_back = (ImageView) findViewById(R.id.img_back);
        btn_back.setOnClickListener(this);

        if (Utility.isNetworkAvailable(mContext)) {

            ParentProfile();

        } else {

            Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back:

                finish();

                break;

            default:

                break;
        }
    }

    public void ParentProfile() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));

        new AsynsTaskClass(mContext, arrayList, true, this).execute
                (ServiceResource.PARENTPROFILE_METHODNAME, ServiceResource.PARENTPROFILE_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.PARENTPROFILE_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.fatherProfile = new ArrayList<ParentsProfileModel>();
                Global.motherProfile = new ArrayList<ParentsProfileModel>();
                Global.gardianProfile = new ArrayList<ParentsProfileModel>();
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    ParentsProfileModel model = new ParentsProfileModel();

                    model.setParentProfileID(innerObj.getString(ServiceResource.PARENTPROFILE_PARENTPROFILEID));
                    model.setFullName(innerObj.getString(ServiceResource.PARENTPROFILE_FULLNAME));
                    model.setRelationTypeTerm(innerObj.getString(ServiceResource.PARENTPROFILE_RELATIONTYPETERM));
                    model.setContactNo(innerObj.getString(ServiceResource.PARENTPROFILE_CONTACTNO));
                    model.setMobileNo(innerObj.getString(ServiceResource.PARENTPROFILE_MOBILENO));
                    model.setOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_OCCUPATIONTERM));
                    model.setDesignationOccupationTerm(innerObj.getString(ServiceResource.PARENTPROFILE_DESIGNATIONOCCUPATIONTERM));
                    model.setOfficeAddress(innerObj.getString(ServiceResource.PARENTPROFILE_OFFICEADDRESS));
                    model.setIsPickup(innerObj.getString(ServiceResource.PARENTPROFILE_ISPICKUP));
                    model.setFirstName(innerObj.getString(ServiceResource.PARENTPROFILE_FIRSTNAME_PARSING));
                    model.setLastName(innerObj.getString(ServiceResource.PARENTPROFILE_LASTNAME_PARSING));
                    String birthdaystr = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING).equalsIgnoreCase("null")) {
                        birthdaystr = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DOB_PARSING)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    String anyversary = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY).equalsIgnoreCase("null")) {
                        anyversary = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFANEVERSARY)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    model.setTxt_birthday(birthdaystr);
                    model.setTxt_anywarsarydate(anyversary);
                    model.setStudy(innerObj.getString(ServiceResource.PARENTPROFILE_HIGHESTSTUDY));
                    model.setEmail(innerObj.getString(ServiceResource.PARENTPROFILE_EMAILID));
                    model.setOocupationDetail(innerObj.getString(ServiceResource.PARENTPROFILE_OCCDETAIL));
                    model.setHomeAddress(innerObj.getString(ServiceResource.PARENTPROFILE_HOMEADDRESSpARSING));
                    model.setIdNo(innerObj.getString(ServiceResource.PARENTPROFILE_IDNOPARSING));
                    model.setIdType(innerObj.getString(ServiceResource.PARENTPROFILE_TYPETERM));
                    String dateofissue = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE).equalsIgnoreCase("null")) {
                        dateofissue = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFISSUE)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    String dateofexp = "";
                    if (!innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP).equalsIgnoreCase("") && !innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP).equalsIgnoreCase("null")) {
                        dateofexp = Utility.getDate(Long.valueOf(innerObj.getString(ServiceResource.PARENTPROFILE_DATEOFXEP)
                                .replace("/Date(", "").replace(")/", "")), "MM-dd-yyyy");
                    }
                    model.setDateofissue(dateofissue);
                    model.setDateofexpiry(dateofexp);
                    model.setAvtar(innerObj
                            .getString(ServiceResource.PARENTPROFILE_AVTAR));
                    model.setIdproofpath(innerObj
                            .getString(ServiceResource.PARENTPROFILE_IDPROOFATTACHED));
                    if (innerObj.getString(ServiceResource.PARENTPROFILE_RELATIONTYPETERM).contains("ather")) {
                        Global.fatherProfile.add(model);
                    } else if (innerObj.getString(ServiceResource.PARENTPROFILE_RELATIONTYPETERM).contains("other")) {
                        Global.motherProfile.add(model);
                    } else {
                        Global.gardianProfile.add(model);
                    }

                }


            } catch (JSONException e) {

                e.printStackTrace();

            }

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.groupViewContainer, new ParentProfileFragment());
            ft.commit();

        }
    }

}
