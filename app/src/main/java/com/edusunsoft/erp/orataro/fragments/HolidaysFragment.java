package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.HolidaysListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.HolidayDataDao;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class HolidaysFragment extends Fragment implements ResponseWebServices {

    private Context mContext;
    private ListView lst_holidays;
    private HolidaysModel holidaysModel;
    private ArrayList<HolidaysModel> holidaysModels = new ArrayList<>();
    private EditText edtTeacherName;
    private HolidaysListAdapter holidaysListAdapter;
    private Global mGlobal;
    private TextView txt_nodatafound;
    private boolean isSucess;

    // variable declaration for homeworklist from ofline database
    HolidayDataDao holidayDataDao;
    private List<HolidaysModel> HolidayList = new ArrayList<>();
    HolidaysModel HolidayModel = new HolidaysModel();
    /*END*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.holidays_fragment, container, false);
        mContext = getActivity();

        try {
            if (HomeWorkFragmentActivity.txt_header != null) {
                HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.Holiday) + " (" + Utility.GetFirstName(mContext) + ")");
                HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holidayDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).holidayDataDao();

        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);
        edtTeacherName = (EditText) v.findViewById(R.id.edtsearchStudent);
        lst_holidays = (ListView) v.findViewById(R.id.lst_holidays);
        mGlobal = new Global();

//
//        if (Utility.isNetworkAvailable(mContext)) {
//            holiday(true);
//        } else {
//            Utility.showAlertDialog(mContext, "Please make sure that you have an active Internet connection", "Error");
//        }

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                try {
                    String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                    holidaysListAdapter.filter(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

        return v;

    }

    @Override
    public void onResume() {
        super.onResume();
        // Get Holiday list from offline database
        GetHoliday();
        /*END*/
    }

    private void GetHoliday() {
        if (Utility.isNetworkAvailable(mContext)) {
            HolidayList = holidayDataDao.getHolidayList();
            if (HolidayList.isEmpty() || HolidayList.size() == 0 || HolidayList == null) {
                holiday(true);
            } else {
                //set Adapter
                setHolidaylistAdapter(HolidayList);
                /*END*/
                holiday(false);
            }
        } else {
            HolidayList = holidayDataDao.getHolidayList();
            if (HolidayList != null) {
                setHolidaylistAdapter(HolidayList);
            }
            Utility.showAlertDialog(mContext, "Please make sure that you have an active Internet connection", "Error");
        }
    }

    private void setHolidaylistAdapter(List<HolidaysModel> holidayList) {
        if (holidayList != null && holidayList.size() > 0) {

            lst_holidays.setVisibility(View.VISIBLE);
            holidaysListAdapter = new HolidaysListAdapter(mContext, holidayList);
            lst_holidays.setAdapter(holidaysListAdapter);
            txt_nodatafound.setVisibility(View.GONE);

        } else {

            lst_holidays.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoHolidayListFound));

        }

    }

    public void holiday(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_USERID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_MEMBERTYPE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        arrayList.add(new PropertyVo(ServiceResource.BEACHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));

        Log.d("holidaylist", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).
                execute(ServiceResource.HOLIDAY_METHODNAME, ServiceResource.HOLIDAY_URL);

    }


    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.HOLIDAY_METHODNAME.equalsIgnoreCase(methodName)) {
            parseHolday(result);
        }

    }

    public void parseHolday(String result) {

        Log.d("holidayresult", result);
        JSONArray hJsonArray;
        holidayDataDao.deleteHoliday();

        try {

            Global.holidaysModel = new HolidaysModel();

            if (result.contains("\"success\":0")) {

            } else {

                hJsonArray = new JSONArray(result);
                Global.holidaysModels = new ArrayList<HolidaysModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {

                    //TODO need to parse new response.
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);

                    // parse homework list data
                    ParseHolidayList(hJsonObject);
                    /*END*/
                }

                isSucess = true;

            }

        } catch (JSONException e) {

            e.printStackTrace();
            isSucess = false;

        }

        if (isSucess) {

            if (holidayDataDao.getHolidayList().size() > 0) {
                //set Adapter
                setHolidaylistAdapter(holidayDataDao.getHolidayList());
                /*END*/
            }


        }


    }

    private void ParseHolidayList(JSONObject hJsonObject) {
        try {
            HolidayModel.setHday(hJsonObject.getString(ServiceResource.HOLIDAY_DAY));
            HolidayModel.setDateOfHoliday(hJsonObject.getString(ServiceResource.HOLIDAY_DATE));
            HolidayModel.setEndDateOfHoliday(hJsonObject.getString(ServiceResource.HOLIDAY_END_DATE));
            HolidayModel.setHolidayTitle(hJsonObject.getString(ServiceResource.HOLIDAY_TITLE));
            HolidayModel.setNoOfDay(hJsonObject.getInt(ServiceResource.HOLIDAY_DAYCOUNT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holidayDataDao.insertHoliday(HolidayModel);

    }

    public class SortedDate implements Comparator<HolidaysModel> {

        @Override
        public int compare(HolidaysModel o1, HolidaysModel o2) {

            return o1.getDateOfHoliday().compareTo(o2.getDateOfHoliday());

        }

    }


}
