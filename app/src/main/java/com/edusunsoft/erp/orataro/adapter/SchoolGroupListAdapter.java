package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.WallActivity;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.Create_Group_Activity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GroupListDataDao;
import com.edusunsoft.erp.orataro.database.GroupListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.RoundedImageView;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SchoolGroupListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private List<GroupListModel> groupList = new ArrayList<>();
    private ArrayList<GroupListModel> copyList = new ArrayList<>();
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private QuickAction quickActionForEditOrDelete;
    private ActionItem actionEdit, actionDelete;
    private RefreshListner listner;

    GroupListDataDao groupListDataDao;
    public String DELETE_ID = "";

    public SchoolGroupListAdapter(Context context, List<GroupListModel> groupList, RefreshListner listner) {
        this.mContext = context;
        this.groupList = groupList;
        copyList.addAll(groupList);
        this.listner = listner;
    }

    @Override
    public int getCount() {
        return groupList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderItem viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolderItem();
            layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfalater.inflate(R.layout.group_list_item, parent, false);

            viewHolder.ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
            viewHolder.txt_group_name = (TextView) convertView.findViewById(R.id.txt_groupname);
            viewHolder.group_icon = (RoundedImageView) convertView.findViewById(R.id.img_group_icon);
            viewHolder.txt_group_status = (TextView) convertView.findViewById(R.id.txt_grp_status);
            viewHolder.txt_no_of_student = (TextView) convertView.findViewById(R.id.txt_No_of_student);
            viewHolder.txt_no_of_teacher = (TextView) convertView.findViewById(R.id.txt_No_of_teacher);
            viewHolder.txt_no_of_discussion = (TextView) convertView.findViewById(R.id.txt_No_of_discussion);
            viewHolder.txt_no_of_share = (TextView) convertView.findViewById(R.id.txt_No_of_share);
            viewHolder.txt_no_of_view = (TextView) convertView.findViewById(R.id.txt_No_of_view);
            viewHolder.ll_editDelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
            viewHolder.ll_editDelete.setTag("" + position);
            viewHolder.imgEditDelete = (ImageView) convertView.findViewById(R.id.imgEditDelete);
            viewHolder.img_wall = (TextView) convertView.findViewById(R.id.img_wall);
            Log.e("Position1", "" + position);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
            viewHolder.ll_editDelete.setTag("" + position);
        }

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsView()) {
                viewHolder.ll_editDelete.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ll_editDelete.setVisibility(View.INVISIBLE);
                Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
            }
        }

        actionEdit = new ActionItem(ID_EDIT, mContext.getResources().getString(R.string.Edit), mContext
                .getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, mContext.getResources().getString(R.string.Delete), mContext
                .getResources().getDrawable(R.drawable.delete));
        quickActionForEditOrDelete = new QuickAction(mContext, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionEdit);
        quickActionForEditOrDelete.addActionItem(actionDelete);

        quickActionForEditOrDelete
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int _pos,
                                            int actionId) {
                        ActionItem actionItem = quickActionForEditOrDelete
                                .getActionItem(_pos);

                        if (actionId == ID_EDIT) {
                            int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                        } else if (actionId == ID_DELETE) {
                            if (Utility.isTeacher(mContext)) {
                                if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsDelete()) {
                                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                                    if (Utility.isNetworkAvailable(mContext)) {
                                        DELETE_ID = groupList.get(deletepos).getGroupId();
                                        RemoveGroup(groupList.get(deletepos).getGroupId());
                                    } else {
                                        Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                                    }
                                } else {
                                    Utility.toast(mContext, ServiceResource.TOASTPERMISSIONMSG);
                                }
                            }
                        }
                    }

                });

        viewHolder.ll_editDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Utility.deleteDialog(mContext, " Group", "", new Popup() {

                    @Override
                    public void deleteYes() {
                        RemoveGroup(groupList.get(position).getGroupId());
                    }

                    @Override
                    public void deleteNo() {

                    }
                });

            }
        });

        viewHolder.img_wall.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ServiceResource.FROM_SELECTED_WALL = "FromGroupList";
                ServiceResource.SELECTED_WALL_TITLE = groupList.get(position).getGroupName();
                ServiceResource.SELECTED_DYNAMIC_WALL_ID = groupList.get(position).getWallID();
                new UserSharedPrefrence(mContext).setCURRENTWALLID(groupList.get(position).getWallID());
                Intent i = new Intent(mContext, WallActivity.class);
//                i.putExtra("isGotoWall", true);
                i.putExtra("isFrom", ServiceResource.GROUPWALL);
                i.putExtra("title", groupList.get(position).getGroupName());
                ServiceResource.WALLTITLE = groupList.get(position).getGroupName();
                mContext.startActivity(i);

            }

        });

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            viewHolder.ll_editDelete.setVisibility(View.GONE);
        } else {
            if (Utility.ReadWriteSetting(ServiceResource.GROUP).getIsEdit() ||
                    Utility.ReadWriteSetting(ServiceResource.GROUP).getIsDelete()) {
                viewHolder.ll_editDelete.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ll_editDelete.setVisibility(View.GONE);
            }
        }

        if (Utility.isNull(groupList.get(position).getGroupName())) {
            viewHolder.txt_group_name.setText(groupList.get(position).getGroupName());
        }
        if (Utility.isNull(groupList.get(position).getGroupstatus())) {
            viewHolder.txt_group_status.setText(groupList.get(position).getGroupstatus());
        }

        if (Utility.isNull(groupList.get(position).getStudentCount())) {
            viewHolder.txt_no_of_student.setText(groupList.get(position).getStudentCount());
        }

        if (Utility.isNull(groupList.get(position).getTeacherCount())) {
            viewHolder.txt_no_of_teacher.setText(groupList.get(position).getTeacherCount());
        }
        if (Utility.isNull(groupList.get(position).getView())) {
            viewHolder.txt_no_of_view.setText(groupList.get(position).getView());
        }

        if (Utility.isNull(groupList.get(position).getShare())) {
            viewHolder.txt_no_of_share.setText(groupList.get(position).getShare());
        }

        if (Utility.isNull(groupList.get(position).getDisscusion())) {
            viewHolder.txt_no_of_discussion.setText(groupList.get(position).getDisscusion());
        }

        try {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.photo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();


            Picasso.get().load(ServiceResource.BASE_IMG_URL +
                    groupList.get(position).getGroupImage()
                            .replace("//DataFiles//", "/DataFiles/")
                            .replace("//DataFiles/", "/DataFiles/")).placeholder(R.drawable.photo).
                    into(viewHolder.group_icon);

        } catch (Exception e) {

            e.printStackTrace();

        }

        convertView.setBackgroundColor(colors[position % colors.length]);

        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, Create_Group_Activity.class);
                i.putExtra("isEdit", true);
                i.putExtra("model", groupList.get(position));
                mContext.startActivity(i);

            }

        });

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        groupList.clear();
        if (charText.length() == 0) {
            groupList.addAll(copyList);
        } else {
            for (GroupListModel vo : copyList) {
                if (vo.getGroupName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    groupList.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public static class ViewHolderItem {
        RoundedImageView group_icon;
        TextView txt_group_name;
        TextView txt_group_status;
        TextView txt_no_of_student;
        TextView txt_no_of_teacher;
        TextView txt_no_of_discussion;
        TextView txt_no_of_share;
        TextView txt_no_of_view, img_wall;
        LinearLayout ll_view, ll_editDelete;
        ImageView imgEditDelete;
    }

    public void RemoveGroup(String groupId) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.GROUPID, groupId));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.REMOVE_GROUP_METHODNAME, ServiceResource.GROUP_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.REMOVE_GROUP_METHODNAME.equalsIgnoreCase(methodName)) {
            groupListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(mContext).groupListDataDao();
            groupListDataDao.deleteGroupByGroupID(DELETE_ID);
            listner.refresh(methodName);
        }
    }
}
