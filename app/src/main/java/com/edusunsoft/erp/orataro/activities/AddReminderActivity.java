package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.NotesListAdapter;
import com.edusunsoft.erp.orataro.adapter.StanderdAdapter;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.model.GetProjectType_model;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.NotesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Locale;

//Add note
//Add Todo (reminder)

public class AddReminderActivity extends Activity implements OnClickListener, ResponseWebServices {

    ListView noticeListView;
    private NotesListAdapter noticeListAdapter;

    private LinearLayout ll_add_new;
    Dialog view;

    LinearLayout ll_comming_soon;
    FrameLayout fl_main;

    Context mContext;

    TextView txt_nodatafound;

    ArrayList<NotesModel> noticeModels;
    NotesModel noticeModel;

    EditText edt_subject_name, edt_notes_title, edt_note_details;
    Spinner edt_dress_code;
    Spinner spn_status;
    TextView tv_start_date, tv_end_date;

    int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1,
            R.drawable.teacher_2};

    int pos;

    String subject_name, notes_title, note_details, dress_code;

    private int date;
    private boolean isEdit;

    NotesModel model;
    Calendar c;
    private int myear;

    private int month;
    private int day, mHour, mMinute;
    private String todayDate;
    private String gradeId;
    private String divisionId;
    private String subjectId;
    private String subjectName;
    private String isFrom;
    boolean isTodo, isTodoEdit;
    static final int DATE_DIALOG_ID = 111;
    private static final int REQUEST_PATH = 112;
    private int fileType = 0;
    TextView header_text;
    TodoListModel todoModel;
    ArrayList<String> strings = new ArrayList<String>();
    LinearLayout ll_imgattechment;
    private ImageView iv_attachment;

    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    public ArrayList<GetProjectType_model> getProjectTypes;
    private byte[] byteArray;
    private String fielPath;
    private Uri fileUri;
    TextView txtGradeDivision;
    String gradeDivisionIdStr = "";

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);
        mContext = this;
        bundle = savedInstanceState;

        if (getIntent() != null) {

            gradeId = getIntent().getStringExtra("gradeId");
            divisionId = getIntent().getStringExtra("divisionId");
            subjectId = getIntent().getStringExtra("subjectId");
            subjectName = getIntent().getStringExtra("subjectName");
            isFrom = getIntent().getStringExtra("isFrom");

            isEdit = getIntent().getBooleanExtra("isEdit", false);
            if (!isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG) && isEdit == true) {
                model = (NotesModel) getIntent().getSerializableExtra("model");
            }
        }
        if (getIntent() == null) {
            if (savedInstanceState != null) {
                fileUri = Uri.parse(savedInstanceState.getString("file_uri"));
                Utility.getUserModelData(mContext);
                gradeId = savedInstanceState.getString("gradeId");
                divisionId = savedInstanceState.getString("divisionId");
                subjectId = savedInstanceState.getString("subjectId");
                subjectName = savedInstanceState.getString("subjectName");
                isFrom = savedInstanceState.getString("isFrom");

                isEdit = savedInstanceState.getBoolean("isEdit", false);
                if (!isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG) && isEdit == true) {
                    model = (NotesModel) savedInstanceState.getSerializable("model");
                }
            }
        }

        header_text = (TextView) findViewById(R.id.header_text);

        ll_imgattechment = (LinearLayout) findViewById(R.id.ll_imgattechment);
        iv_attachment = (ImageView) findViewById(R.id.iv_attachment);
        edt_subject_name = (EditText) findViewById(R.id.edt_subject_name);

        edt_notes_title = (EditText) findViewById(R.id.edt_notes_title);

        edt_note_details = (EditText) findViewById(R.id.edt_note_details);

        txtGradeDivision = (TextView) findViewById(R.id.txtGradeDivision);

        edt_dress_code = (Spinner) findViewById(R.id.edt_dress_code);
        spn_status = (Spinner) findViewById(R.id.spn_status);
        tv_start_date = (TextView) findViewById(R.id.tv_start_date);

        tv_end_date = (TextView) findViewById(R.id.tv_end_date);

        ImageView img_back = (ImageView) findViewById(R.id.img_back);

        ImageView iv_save = (ImageView) findViewById(R.id.iv_save);

        img_back.setOnClickListener(this);


        iv_save.setOnClickListener(this);

        if (isFrom.equalsIgnoreCase(ServiceResource.TODO_FLAG)) {
            ll_imgattechment.setVisibility(View.GONE);
            isTodo = true;
            isTodoEdit = getIntent().getBooleanExtra("isEdit", false);
            if (isTodoEdit) {
                todoModel = (TodoListModel) getIntent().getSerializableExtra("model");
                fiiTodoData(todoModel);
            }
            header_text.setText(mContext.getResources().getString(R.string.Todo));
        } else {


            ll_imgattechment.setVisibility(View.VISIBLE);
            txtGradeDivision.setVisibility(View.VISIBLE);
            if (Utility.isNetworkAvailable(mContext)) {
                getStandarDivisionList();
                //	parsestddivisionlist(examresult,"");
                parsestddivisionlist(Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext));
            } else {
                //parsestddivisionlist(examresult,"");
                parsestddivisionlist(Utility.readFromFile(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, mContext));
            }
            edt_subject_name.setVisibility(View.GONE);
        }


        c = Calendar.getInstance();
        myear = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        todayDate = (day < 10 ? "0" + day : day)
                + "-"
                + ((month + 1) < 10 ? "0" + (month + 1)
                : (month + 1))
                + "-" + myear;

        if (isTodo) {
            edt_subject_name.setVisibility(View.GONE);
        } else {
            edt_subject_name.setText(subjectName);
            edt_subject_name.setEnabled(false);
        }
        tv_start_date.setOnClickListener(this);
        tv_end_date.setOnClickListener(this);
        iv_attachment.setOnClickListener(this);
        txtGradeDivision.setOnClickListener(this);

        if (isTodo) {
            ArrayList<String> strings = new ArrayList<String>();
            strings.add("Important");
            strings.add("Normal");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //					Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            edt_dress_code.setAdapter(adapter);
            if (isTodoEdit) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (todoModel.getTypeTerm().trim().equals(adapter.getItem(i).toString())) {
                        edt_dress_code.setSelection(i);
                        break;
                    }
                }
            }
            ArrayList<String> strings1 = new ArrayList<String>();
            strings1.add("Cancelled");
            strings1.add("Completed");
            strings1.add("In-Progress");
            strings1.add("Insufficient");
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings1);

            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //					Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            spn_status.setAdapter(adapter1);
            if (isTodoEdit) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (todoModel.getStatus().trim().equals(adapter.getItem(i).toString())) {
                        edt_dress_code.setSelection(i);
                        break;
                    }
                }
            }
        } else {
            spn_status.setVisibility(View.GONE);
            if (Utility.isNetworkAvailable(mContext)) {
                getProjectType();
                parseprojecttype(Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext));
            } else {
                parseprojecttype(Utility.readFromFile(ServiceResource.LOGIN_GETPROJECTTYPE, mContext));
            }
            if (isEdit) {
                fillNote();
            }
        }
    }

    ;

    // set data of patcilar notes in edit text and spiner when it in edit mode
    /*
     *
     * */
    private void fillNote() {

        edt_notes_title.setText(model.getNoteTitle());
        edt_note_details.setText(model.getNoteDetails());
        for (int i = 0; i < strings.size(); i++) {
            if (strings.get(i).equalsIgnoreCase(model.getDressCode())) {
                edt_dress_code.setSelection(i);
            }
        }
        tv_start_date.setText(model.getActionStartDate());
        tv_end_date.setText(model.getActionEndDate());
        edt_subject_name.setText(model.getSubjectName());
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog datePickerDialog;

        switch (id) {
            case DATE_DIALOG_ID:

                datePickerDialog = new DatePickerDialog(this, datePickerListener,
                        myear, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
                return datePickerDialog;
        }
        return null;

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            view.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
            myear = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (date == 1) {
                tv_end_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            } else if (date == 2) {
                tv_start_date.setText((day < 10 ? "0" + day : day)
                        + "-"
                        + ((month + 1) < 10 ? "0" + (month + 1)
                        : (month + 1))
                        + "-" + myear);
            }

            date = 0;
        }
    };
    private String curFileName;
    private String FileMineType;


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.img_back:
                finish();
                break;
            case R.id.iv_save:
                if (isTodo) {
                    if (!Utility.isNull(tv_start_date.getText().toString())) {
                        tv_start_date.setError(mContext.getResources().getString(R.string.enterstartdate));
                    } else if (!Utility.isNull(tv_end_date.getText().toString())) {
                        tv_end_date.setError(mContext.getResources().getString(R.string.enterenddate));
                    } else {
                        addTodo();
                    }
                } else {
                    if (!Utility.isNull(tv_start_date.getText().toString())) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.enterstartdate));
                    } else if (!Utility.isNull(tv_end_date.getText().toString())) {
                        Utility.toast(mContext, mContext.getResources().getString(R.string.enterenddate));
                    } else {
                        addNote();
                    }
                }
                break;
            case R.id.tv_end_date:
                date = 1;
                showDialog(DATE_DIALOG_ID);
                break;
            case R.id.tv_start_date:
                date = 2;
                showDialog(DATE_DIALOG_ID);
                break;

            case R.id.iv_attachment:
                selectImage(1);
                break;
            case R.id.txtGradeDivision:
                showStanderdPopup();
                break;
            default:
                break;
        }
    }

    // set data of patcilar todo(reminder) in edit text and spiner when it in edit mode
    public void fiiTodoData(TodoListModel model) {
        if (Utility.isNull(model.getTitle())) {
            edt_notes_title.setText(model.getTitle());
        }
        if (Utility.isNull(model.getDetails())) {
            edt_note_details.setText(model.getDetails());
        }
        if (Utility.isNull(model.getStatus())) {
            //spn_status.setpo
        }

        if (Utility.isNull(model.getTypeTerm())) {

        }

        if (Utility.isNull(model.getEndDate())) {
            tv_end_date.setText(model.getEndDate());
        }
        if (Utility.isNull(model.getCreateOn())) {
            tv_start_date.setText(model.getCreateOn());
        }
    }

    // class FOr sort data in Arraylist
    public class SortedDate implements Comparator<NotesModel> {
        @Override
        public int compare(NotesModel o1, NotesModel o2) {
            return o1.getMilliSecond().compareTo(o2.getMilliSecond());
        }
    }

    // add note webservice
    public void addNote() {
        Utility.getUserModelData(mContext);
        ContentValues values = new ContentValues();

        if (isEdit) {
            values.put(ServiceResource.NOTES_EDITID, model.getNotesID());
        } else {
            values.put(ServiceResource.NOTES_EDITID, "");
        }


        values.put(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
        values.put(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID());
        values.put(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID());
        values.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        values.put(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        values.put(ServiceResource.BEATCH_ID, "");
        values.put(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr);
        values.put(ServiceResource.NOTES_SUBJECTID, "");
        if (isEdit) {
            //			values.put(ServiceResource.GRADEID, model.getGradeID());
            //			values.put(ServiceResource.DIVISIONID,
            //					model.getDivisionID());
            //			values.put(ServiceResource.NOTES_SUBJECTID,model.getSubjectID());
        } else {
            //			values.put(ServiceResource.GRADEID, gradeId);
            //			values.put(ServiceResource.DIVISIONID,
            //					divisionId);
            //			//				values.put(ServiceResource.SUBJECTID,subjectId);
            //			//				values.put(ServiceResource.ISAPPROVE,
            //			//						String.valueOf(Global.notesModels.get(pos).isIsRead());
            //			//				values.put(ServiceResource.ASSOCIATIONTYPE, "Note");
            //			values.put(ServiceResource.NOTES_SUBJECTID,subjectId);
        }
        if (byteArray != null) {
            if (fileType == 0) {
                values.put(ServiceResource.FILE,
                        byteArray);
                values.put(ServiceResource.FILENAME,
                        new File(fielPath).getName());
                values.put(ServiceResource.FILETYPE,
                        "IMAGE");
                values.put(ServiceResource.FILEMINETYPE,
                        FileMineType);
            } else if (fileType == 2) {
                values.put(ServiceResource.FILE,
                        byteArray);
                values.put(ServiceResource.FILENAME,
                        new File(fielPath).getName());
                values.put(ServiceResource.FILETYPE,
                        "FILE");
                values.put(ServiceResource.FILEMINETYPE,
                        FileMineType);
            }
        } else {
            values.put(ServiceResource.FILE,
                    "");
            values.put(ServiceResource.FILENAME,
                    "");
            values.put(ServiceResource.FILETYPE,
                    "");
            values.put(ServiceResource.FILEMINETYPE,
                    "");
        }


        String DressCode = "";


        try {
            DressCode = edt_dress_code.getSelectedItem().toString();
        } catch (Exception e) {
            // TODO: handle exception
        }


        values.put(ServiceResource.NOTES_NOTETITLE, edt_notes_title.getText().toString());
        values.put(ServiceResource.NOTES_RATING, "0");
        values.put(ServiceResource.NOTES_DRESSCODE, DressCode);
        values.put(ServiceResource.NOTES_NOTEDETAILS, edt_note_details.getText().toString().replace("'", ""));
        values.put(ServiceResource.NOTES_ACTIONSTARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {

            values.put(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy"));
        } else {
            values.put(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy"));
        }
        values.put(ServiceResource.NOTES_INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId());


        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_EDITID, model.getNotesID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_EDITID, null));
        }


        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.GRADEDIVISOINID, gradeDivisionIdStr));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_SUBJECTID, null));
        if (isEdit) {
            //			arrayList.add(new Property_vo(ServiceResource.GRADEID, model.getGradeID()));
            //			arrayList.add(new Property_vo(ServiceResource.DIVISIONID,
            //					model.getDivisionID()));
            //			arrayList.add(new Property_vo(ServiceResource.NOTES_SUBJECTID,model.getSubjectID()));
        } else {
            //			arrayList.add(new Property_vo(ServiceResource.GRADEID, gradeId));
            //			arrayList.add(new Property_vo(ServiceResource.DIVISIONID,
            //					divisionId));
            //			//				arrayList.add(new Property_vo(ServiceResource.SUBJECTID,subjectId);
            //			//				arrayList.add(new Property_vo(ServiceResource.ISAPPROVE,
            //			//						String.valueOf(Global.notesModels.get(pos).isIsRead()));
            //			//				arrayList.add(new Property_vo(ServiceResource.ASSOCIATIONTYPE, "Note");
            //			arrayList.add(new Property_vo(ServiceResource.NOTES_SUBJECTID,subjectId));
        }
        if (byteArray != null) {
            if (fileType == 0) {
                arrayList.add(new PropertyVo(ServiceResource.FILE,
                        byteArray));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME,
                        new File(fielPath).getName()));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                        "IMAGE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE,
                        FileMineType));
            } else if (fileType == 2) {
                arrayList.add(new PropertyVo(ServiceResource.FILE,
                        byteArray));
                arrayList.add(new PropertyVo(ServiceResource.FILENAME,
                        new File(fielPath).getName()));
                arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                        "FILE"));
                arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE,
                        FileMineType));
            }
        } else {
            arrayList.add(new PropertyVo(ServiceResource.FILE,
                    null));
            arrayList.add(new PropertyVo(ServiceResource.FILENAME,
                    null));
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    null));
            arrayList.add(new PropertyVo(ServiceResource.FILEMINETYPE,
                    null));
        }


        try {
            DressCode = edt_dress_code.getSelectedItem().toString();
        } catch (Exception e) {
            // TODO: handle exception
        }


        arrayList.add(new PropertyVo(ServiceResource.NOTES_NOTETITLE, edt_notes_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_RATING, "0"));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_DRESSCODE, DressCode));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_NOTEDETAILS, edt_note_details.getText().toString().replace("'", "")));
        arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONSTARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        if (tv_end_date.getText().toString().equalsIgnoreCase("")) {

            arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(todayDate, "MM-dd-yyyy", "dd-MM-yyyy")));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.NOTES_ACTIONENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        }
        arrayList.add(new PropertyVo(ServiceResource.NOTES_INSTITUTIONWALLID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteWallId()));
        arrayList.add(new PropertyVo("Timing", "2:00 pm"));
        arrayList.add(new PropertyVo("EndTime", "3:00 pm"));


        if (Utility.isNetworkAvailable(mContext)) {
            new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CREATENOTESWITHMULTY_METHODNAME,
                    ServiceResource.CHECK_URL);
        } else {
//			DataBaseHelper dbHelper = DataBaseHelper.newInstance(mContext);
//			dbHelper.insert(DataBaseHelper.TABLE_CREATENOTESWITHMULTY, values);
//			finish();

        }


    }

    //Add Todo(reminder webservice)
    private void addTodo() {
        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (isTodoEdit) {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, todoModel.getTodosID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.EDITID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.WALLID, new UserSharedPrefrence(mContext).getLoginModel().getWallID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TITLE, edt_notes_title.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_DETAILS,
                edt_note_details.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_TYPETERM, edt_dress_code.getSelectedItem().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STATUS, spn_status.getSelectedItem().toString()));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_STARTDATE, Utility.dateFormate(tv_start_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_ENDDATE, Utility.dateFormate(tv_end_date.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ADDTODO_COMPLETDATE, ""));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.ADDTODO_METHODNAME,
                ServiceResource.TODO_URL);
    }

    // get Spinner List from webservice which get from category DressCode,Rating
    public void getProjectType() {
        //		fsdfsd
        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY,
                "DressCode,Rating"));

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.LOGIN_GETPROJECTTYPE,
                ServiceResource.LOGIN_URL);
    }

    public void response(String result, String methodName) {
        if (ServiceResource.LOGIN_GETPROJECTTYPE.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parseprojecttype(result);
        }
        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {
            Utility.writeToFile(result, methodName, mContext);
            parsestddivisionlist(result);
        }
        if (ServiceResource.ADDTODO_METHODNAME.equalsIgnoreCase(methodName)) {
            Intent i = new Intent(AddReminderActivity.this,
                    HomeWorkFragmentActivity.class);
            i.putExtra("position", mContext.getResources().getString(R.string.Todo));
            startActivity(i);
            finish();
        }
        if (ServiceResource.CREATENOTESWITHMULTY_METHODNAME.equalsIgnoreCase(methodName)) {

            Utility.sendPushNotification(mContext);
            JSONArray hJsonArray;
            try {


                if (result.contains("\"success\":0")) {

                    // Toast.makeText(mContext,
                    // getResources().getString(R.string.no_data), 0)
                    // .show();

                } else {

                    hJsonArray = new JSONArray(result);
                    getProjectTypes = new ArrayList<GetProjectType_model>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        GetProjectType_model model = new GetProjectType_model();
                        model.setCategory(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
                        model.setDefaultValue(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_DEFAULTVALUE));
                        model.setIsDefault(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                        model.setOrderNo(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                        model.setTerm(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_TERM));
                        model.setTermID(hJsonObject
                                .getString(ServiceResource.GETPROJECTTYPE_TERMID));

                        getProjectTypes.add(model);

                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            finish();


        } else if (ServiceResource.CREATENOTES_METHODNAME.equalsIgnoreCase(methodName)) {
            //			fdsfdf
        }
    }

    private void parseprojecttype(String result) {
        // TODO Auto-generated method stub


        JSONArray hJsonArray;
        try {


            if (result.contains("\"success\":0")) {

                // Toast.makeText(mContext,
                // getResources().getString(R.string.no_data), 0)
                // .show();

            } else {

                hJsonArray = new JSONArray(result);
                getProjectTypes = new ArrayList<GetProjectType_model>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    GetProjectType_model model = new GetProjectType_model();
                    model.setCategory(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_CATEGORY));
                    model.setDefaultValue(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_DEFAULTVALUE));
                    model.setIsDefault(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_ISDEFAULT));
                    model.setOrderNo(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_ORDERNO));
                    model.setTerm(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_TERM));
                    model.setTermID(hJsonObject
                            .getString(ServiceResource.GETPROJECTTYPE_TERMID));

                    getProjectTypes.add(model);


                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        // TODO Auto-generated method stub
        ArrayList<GetProjectType_model> getProjectType_models = new ArrayList<GetProjectType_model>();

        if (getProjectTypes != null && getProjectTypes.size() > 0) {
            for (int i = 0; i < getProjectTypes.size(); i++) {
                if (getProjectTypes.get(i).getCategory().equals("DressCode")) {
                    getProjectType_models.add(getProjectTypes.get(i));
                }
            }
        }


        if (getProjectType_models != null && getProjectType_models.size() > 0) {

            for (int i = 0; i < getProjectType_models.size(); i++) {
                strings.add(getProjectType_models.get(i).getTerm());
            }

        }

        if (strings != null && strings.size() > 0) {
            //				 Spinner spinner = (Spinner) findViewById(R.id.spinner);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item, strings);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //					Spinner sItems = (Spinner) findViewById(R.id.spinner1);
            edt_dress_code.setAdapter(adapter);


        }


    }

    private void parsestddivisionlist(String result) {
        // TODO Auto-generated method stub

        JSONArray hJsonArray;
        try {

            Global.holidaysModel = new HolidaysModel();
            if (result.contains("\"success\":0")) {


            } else {

                hJsonArray = new JSONArray(result);
                Global.StandardModels = new ArrayList<StandardModel>();

                for (int i = 0; i < hJsonArray.length(); i++) {
                    boolean isAlreadyInsert = false;
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StandardModel model = new StandardModel();
                    model.setStandardName(hJsonObject
                            .getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject
                            .getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject
                            .getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject
                            .getString(ServiceResource.DIVISION));
                    model.setSubjectId(hJsonObject
                            .getString(ServiceResource.STANDARD_SUBJECTID));
                    model.setSubjectName(hJsonObject
                            .getString(ServiceResource.SUBJECT));
                    if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
                        for (int j = 0; j < Global.StandardModels.size(); j++) {
                            if (Global.StandardModels.get(j).getDivisionId().equalsIgnoreCase(model.getDivisionId())
                                    && Global.StandardModels.get(j).getStandrdId().equalsIgnoreCase(model.getStandrdId())) {
                                isAlreadyInsert = true;
                            }
                        }
                    }

                    if (!isAlreadyInsert) {

                        Global.StandardModels.add(model);

                    }


                }
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }


    }

    ///select image from gallary and capture image from camera
    private void selectImage(final int i) {
        final CharSequence[] items = {mContext.getResources().getString(R.string.takephoto),
                mContext.getResources().getString(R.string.fromlib),
                mContext.getResources().getString(R.string.File),
                mContext.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mContext.getResources().getString(R.string.takephoto))) {
                    //					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //
                    //					fileUri = Utility.getOutputMediaFileUri(ServiceResource.MEDIA_TYPE_IMAGE,mContext);
                    //
                    //					intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    //
                    //					// start the image capture Intent
                    //					startActivityForResult(intent, REQUEST_CAMERA);
//					Intent intent  = new Intent(mContext, Preview_image.class);
//					intent.putExtra("FromIntent","true");
//					intent.putExtra("RequestCode", 100);
//					startActivityForResult(intent, REQUEST_CAMERA);

                    //					Intent intent = new Intent(AddReminderActivity.this,CameraActivity.class);
                    //					startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals(mContext.getResources().getString(R.string.fromlib))) {

                    Intent intent = new Intent(AddReminderActivity.this, ImageSelectionActivity.class);
                    intent.putExtra("count", 1);
                    startActivityForResult(intent, SELECT_FILE);

                    //					Intent intent = new Intent(
                    //							Intent.ACTION_PICK,
                    //							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //					intent.setType("image/*");
                    //					if(i == 2){
                    //						startActivityForResult(
                    //								Intent.createChooser(intent, "Select File"),
                    //								SELECT_FILE);
                    //					}else{
                    //						startActivityForResult(
                    //								Intent.createChooser(intent, "Select File"),
                    //								SELECT_FILE);
                    //					}
                }
//				else if (items[item].equals(mContext.getResources().getString(R.string.File))) {
//					Intent intent1 = new Intent(mContext, FileChooser.class);
//					startActivityForResult(intent1, REQUEST_PATH);
//
//				}
                else if (items[item].equals(mContext.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {

                Bitmap thumbnail = null;
                Utility.getUserModelData(mContext);
                //				if(data.getExtras().get("data")==null)
                //				{
                //					try {
                //						thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                //					} catch (FileNotFoundException e) {
                //						// TODO Auto-generated catch block
                //						e.printStackTrace();
                //					} catch (IOException e) {
                //						// TODO Auto-generated catch block
                //						e.printStackTrace();
                //					}
                //				}
                //				else
                //				{
                //					thumbnail = Utility.getBitmap(fileUri.getPath(), mContext);//(Bitmap) data.getExtras().get("data");
                ////				}
                ////				Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                //					fielPath=fileUri.getPath();
                //				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //				thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                //                byteArray = null;
                //				byteArray = bytes.toByteArray();
                //				iv_attachment.setImageBitmap(thumbnail);


                try {
                    fielPath = data.getExtras().getString("imageData_uri");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                byteArray = null;
                byteArray = data.getExtras().getByteArray("imageData_byte");
                iv_attachment.setImageBitmap(new BitmapFactory().decodeByteArray(data.getExtras().getByteArray("imageData_byte"), 0, data.getExtras().getByteArray("imageData_byte").length));


                //				File destination = new File(
                //						Environment.getExternalStorageDirectory(),
                //						System.currentTimeMillis() + ".jpg");
                //
                //				FileOutputStream fo;
                //
                //				fielPath=destination.getPath();
                //
                //				try {
                //					destination.createNewFile();
                //					fo = new FileOutputStream(destination);
                //					fo.write(bytes.toByteArray());
                //					fo.close();
                //				} catch (FileNotFoundException e) {
                //					e.printStackTrace();
                //				} catch (IOException e) {
                //					e.printStackTrace();
                //				}
                //
                //
                //
                //				Bitmap thePic;
                //				BitmapFactory.Options options = new BitmapFactory.Options();
                //				options.inJustDecodeBounds = true;
                //				BitmapFactory.decodeFile(fielPath, options);
                //				// fielPath=selectedImagePath;
                //
                //								final int REQUIRED_SIZE = 200;
                //								int scale = 1;
                //								while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                //										&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
                //									scale *= 2;
                //								options.inSampleSize = scale;
                //								options.inJustDecodeBounds = false;
                //				thePic = BitmapFactory.decodeFile(fielPath, options);
                //				// Bitmap thePic = (Bitmap) extras.get("data");
                //				ByteArrayOutputStream stream = new ByteArrayOutputStream();
                //				thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);

                // ivImage.setImageBitmap(thumbnail);

            } else if (requestCode == SELECT_FILE) {
                ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");
                //				Toast.makeText(mContext, ""+imgList.size(), 1).show();
                if (imgList != null && imgList.size() > 0) {


                    Uri selectedImageUri = imgList.get(0).getUri();


                    //				Uri selectedImageUri = data.getData();
//					String[] projection = { MediaColumns.DATA };
//					CursorLoader cursorLoader = new CursorLoader(mContext,
//							selectedImageUri, projection, null, null, null);
//					Cursor cursor = cursorLoader.loadInBackground();
//					int column_index = cursor
//							.getColumnIndexOrThrow(MediaColumns.DATA);
//					cursor.moveToFirst();
//
//					String selectedImagePath = cursor.getString(column_index);
                    fielPath = imgList.get(0).getUri().toString();
                    Bitmap thePic;
                    thePic = Utility.getBitmap(fielPath, mContext);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (fielPath.contains(".png") || fielPath.contains(".PNG")) {
                        thePic.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    } else {
                        thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
//					thePic.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byteArray = stream.toByteArray();
                    iv_attachment.setImageBitmap(thePic);
                }
            } else if (requestCode == REQUEST_PATH) {
                curFileName = data.getStringExtra("GetFileName");
                String path = data.getStringExtra("GetPath");
                fileType = 2;
                iv_attachment.setColorFilter(Color.parseColor("#27305B"), android.graphics.PorterDuff.Mode.SRC_IN);
                if (curFileName.contains("pdf")) {
                    FileMineType = "PDF";
                    iv_attachment.setImageResource(R.drawable.pdf);
                } else if (curFileName.contains("PDF")) {
                    FileMineType = "PDF";
                    iv_attachment.setImageResource(R.drawable.pdf);
                } else if (curFileName.contains("txt")) {
                    FileMineType = "TEXT";
                    iv_attachment.setImageResource(R.drawable.txt);
                } else if (curFileName.contains("TXT")) {
                    FileMineType = "TEXT";
                    iv_attachment.setImageResource(R.drawable.txt);
                } else if (curFileName.contains("doc")) {
                    FileMineType = "WORD";
                    iv_attachment.setImageResource(R.drawable.doc);
                } else {
                    FileMineType = "DOCUMENT";
                    iv_attachment.setImageResource(R.drawable.doc);
                }
                byteArray = convertVideoToByteArra(path + "/" + curFileName);
                fielPath = path + "/" + curFileName;
            }
        }

    }


    public byte[] convertVideoToByteArra(String filePath) {

        File file = new File(filePath);

        byte[] b = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            b = new byte[(int) file.length()];
            fileInputStream.read(b);
            for (int i = 0; i < b.length; i++) {
                System.out.print((char) b[i]);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found.");
            e.printStackTrace();
        } catch (IOException e1) {
            System.out.println("Error Reading The File.");
            e1.printStackTrace();
        }

        return b;

    }

    //get  standard and division list from webservice
    public void getStandarDivisionList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        new AsynsTaskClass(mContext, arrayList, false, AddReminderActivity.this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);


    }

    // show standard  popup which have multiple selection and make string of division id and standard id  with standardId, divisionId # format
    public void showStanderdPopup() {

        final Dialog dialogStandard = new Dialog(mContext);
        dialogStandard.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogStandard.setContentView(R.layout.customelistdialog);
        final StanderdAdapter adapter = new StanderdAdapter(mContext,
                Global.StandardModels, true, true);
        TextView dialogTitle = (TextView) dialogStandard
                .findViewById(R.id.dialog_title);
        final EditText edtSearch = (EditText) dialogStandard.findViewById(R.id.searchCity);
        edtSearch.setVisibility(View.VISIBLE);

        ListView lv = (ListView) dialogStandard.findViewById(R.id.custome_Dialog_List);
        Button btnDone = (Button) dialogStandard
                .findViewById(R.id.custome_Dialog_DOne_btn);
        Button btnCancle = (Button) dialogStandard
                .findViewById(R.id.custome_Dialog_cancle_btn);
        //		lv.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        edtSearch.setHint(mContext.getResources().getString(R.string.entergroupmember));
        dialogTitle.setText(mContext.getResources().getString(R.string.selectgroupmember));

        //		btnCancle.setVisibility(View.GONE);
        //		btnDone.setVisibility(View.GONE);
        edtSearch.setVisibility(View.GONE);


        dialogTitle.setText(mContext.getResources().getString(R.string.selectstandard));
        if (Global.StandardModels != null
                && Global.StandardModels.size() > 0) {


            lv.setAdapter(adapter);

        }


        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                String text = edtSearch.getText().toString()
                        .toLowerCase(Locale.getDefault());

            }
        });


        btnDone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogStandard.dismiss();
                adapter.getSelectedDivision();
                String txtTempStr = "";
                gradeDivisionIdStr = "";
                for (int i = 0; i < adapter.getSelectedDivision().size(); i++) {

                    if (i == adapter.getSelectedDivision().size() - 1) {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ".";
                    } else {
                        txtTempStr = txtTempStr + adapter.getSelectedDivision().get(i).getStandardName() + "-"
                                + adapter.getSelectedDivision().get(i).getDivisionName() + ",";
                    }
                    String tempStr = adapter.getSelectedDivision().get(i).getStandrdId() + ","
                            + adapter.getSelectedDivision().get(i).getDivisionId() + "#";

                    gradeDivisionIdStr = gradeDivisionIdStr + tempStr;


                }
                txtGradeDivision.setText(txtTempStr);
            }
        });
        btnCancle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialogStandard.dismiss();
            }
        });

        dialogStandard.show();


    }

}
