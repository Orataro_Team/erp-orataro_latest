package com.edusunsoft.erp.orataro.model;

import java.util.ArrayList;

public class WallModel {

    private String personName, timeBeforePost, postContent, postImg, profilePicImg;
    private boolean comment;
    private ArrayList<CommentModel> commentList = new ArrayList<>();
    private boolean availPostContent, avilablepostImg;

    public boolean isComment() {
        return comment;
    }

    public void setComment(boolean comment) {
        this.comment = comment;
    }

    public ArrayList<CommentModel> getCommentList() {
        return commentList;
    }

    public void setCommentList(ArrayList<CommentModel> commentList) {
        this.commentList = commentList;
    }

    public String getProfilePicImg() {
        return profilePicImg;
    }

    public void setProfilePicImg(String profilePicImg) {
        this.profilePicImg = profilePicImg;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getTimeBeforePost() {
        return timeBeforePost;
    }

    public void setTimeBeforePost(String timeBeforePost) {
        this.timeBeforePost = timeBeforePost;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostImg() {
        return postImg;
    }

    public void setPostImg(String postImg) {
        this.postImg = postImg;
    }

    public boolean isAvailPostContent() {
        return availPostContent;
    }

    public void setAvailPostContent(boolean availPostContent) {
        this.availPostContent = availPostContent;
    }

    public boolean isAvilablepostImg() {
        return avilablepostImg;
    }

    public void setAvilablepostImg(boolean avilablepostImg) {
        this.avilablepostImg = avilablepostImg;
    }

}
