package com.edusunsoft.erp.orataro.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReciptDetailModel implements Parcelable {

    public final static Parcelable.Creator<ReciptDetailModel> CREATOR = new Creator<ReciptDetailModel>() {

        public ReciptDetailModel createFromParcel(Parcel in) {
            ReciptDetailModel instance = new ReciptDetailModel();
            in.readList(instance.table, (Table.class.getClassLoader()));
            return instance;
        }

        public ReciptDetailModel[] newArray(int size) {
            return (new ReciptDetailModel[size]);
        }

    };

    @SerializedName("Table")
    @Expose
    private ArrayList<Table> table = null;

    public ArrayList<Table> getTable() {
        return table;
    }

    public void setTable(ArrayList<Table> table) {
        this.table = table;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(table);
    }

    public int describeContents() {
        return 0;
    }

    public class Table implements Parcelable {

        @SerializedName("StudentFeesCollectionID")
        @Expose
        private String studentFeesCollectionID;
        @SerializedName("StudentID")
        @Expose
        private String studentID;
        @SerializedName("StandardID")
        @Expose
        private String standardID;
        @SerializedName("FeesStructureID")
        @Expose
        private String feesStructureID;
        @SerializedName("DateOfFees")
        @Expose
        private String dateOfFees;
        @SerializedName("AmountToBePaid")
        @Expose
        private Double amountToBePaid;
        @SerializedName("TotalDue")
        @Expose
        private Double totalDue;
        @SerializedName("StudentFullName")
        @Expose
        private String studentFullName;
        @SerializedName("DisplayLabel")
        @Expose
        private String displayLabel;
        @SerializedName("StructuteName")
        @Expose
        private String structuteName;
        @SerializedName("PaidAmounts")
        @Expose
        private Double paidAmounts;
        @SerializedName("ReceiptPath")
        @Expose
        private String receiptPath;
        @SerializedName("BatchID")
        @Expose
        private String batchID;
        @SerializedName("ClientID")
        @Expose
        private String clientID;
        @SerializedName("InstituteID")
        @Expose
        private String instituteID;
        public final Parcelable.Creator<Table> CREATOR = new Creator<Table>() {

            public Table createFromParcel(Parcel in) {
                Table instance = new Table();
                instance.studentFeesCollectionID = ((String) in.readValue((String.class.getClassLoader())));
                instance.studentID = ((String) in.readValue((String.class.getClassLoader())));
                instance.standardID = ((String) in.readValue((String.class.getClassLoader())));
                instance.feesStructureID = ((String) in.readValue((String.class.getClassLoader())));
                instance.dateOfFees = ((String) in.readValue((String.class.getClassLoader())));
                instance.amountToBePaid = ((Double) in.readValue((Double.class.getClassLoader())));
                instance.totalDue = ((Double) in.readValue((Double.class.getClassLoader())));
                instance.studentFullName = ((String) in.readValue((String.class.getClassLoader())));
                instance.displayLabel = ((String) in.readValue((String.class.getClassLoader())));
                instance.structuteName = ((String) in.readValue((String.class.getClassLoader())));
                instance.paidAmounts = ((Double) in.readValue((Double.class.getClassLoader())));
                instance.receiptPath = ((String) in.readValue((String.class.getClassLoader())));
                instance.batchID = ((String) in.readValue((String.class.getClassLoader())));
                instance.clientID = ((String) in.readValue((String.class.getClassLoader())));
                instance.instituteID = ((String) in.readValue((String.class.getClassLoader())));
                return instance;
            }

            public Table[] newArray(int size) {
                return (new Table[size]);
            }

        };

        public String getStudentFeesCollectionID() {
            return studentFeesCollectionID;
        }

        public void setStudentFeesCollectionID(String studentFeesCollectionID) {
            this.studentFeesCollectionID = studentFeesCollectionID;
        }

        public String getStudentID() {
            return studentID;
        }

        public void setStudentID(String studentID) {
            this.studentID = studentID;
        }

        public String getStandardID() {
            return standardID;
        }

        public void setStandardID(String standardID) {
            this.standardID = standardID;
        }

        public String getFeesStructureID() {
            return feesStructureID;
        }

        public void setFeesStructureID(String feesStructureID) {
            this.feesStructureID = feesStructureID;
        }

        public String getDateOfFees() {
            return dateOfFees;
        }

        public void setDateOfFees(String dateOfFees) {
            this.dateOfFees = dateOfFees;
        }

        public Double getAmountToBePaid() {
            return amountToBePaid;
        }

        public void setAmountToBePaid(Double amountToBePaid) {
            this.amountToBePaid = amountToBePaid;
        }

        public Double getTotalDue() {
            return totalDue;
        }

        public void setTotalDue(Double totalDue) {
            this.totalDue = totalDue;
        }

        public String getStudentFullName() {
            return studentFullName;
        }

        public void setStudentFullName(String studentFullName) {
            this.studentFullName = studentFullName;
        }

        public String getDisplayLabel() {
            return displayLabel;
        }

        public void setDisplayLabel(String displayLabel) {
            this.displayLabel = displayLabel;
        }

        public String getStructuteName() {
            return structuteName;
        }

        public void setStructuteName(String structuteName) {
            this.structuteName = structuteName;
        }

        public Double getPaidAmounts() {
            return paidAmounts;
        }

        public void setPaidAmounts(Double paidAmounts) {
            this.paidAmounts = paidAmounts;
        }

        public String getReceiptPath() {
            return receiptPath;
        }

        public void setReceiptPath(String receiptPath) {
            this.receiptPath = receiptPath;
        }

        public String getBatchID() {
            return batchID;
        }

        public void setBatchID(String batchID) {
            this.batchID = batchID;
        }

        public String getClientID() {
            return clientID;
        }

        public void setClientID(String clientID) {
            this.clientID = clientID;
        }

        public String getInstituteID() {
            return instituteID;
        }

        public void setInstituteID(String instituteID) {
            this.instituteID = instituteID;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(studentFeesCollectionID);
            dest.writeValue(studentID);
            dest.writeValue(standardID);
            dest.writeValue(feesStructureID);
            dest.writeValue(dateOfFees);
            dest.writeValue(amountToBePaid);
            dest.writeValue(totalDue);
            dest.writeValue(studentFullName);
            dest.writeValue(displayLabel);
            dest.writeValue(structuteName);
            dest.writeValue(paidAmounts);
            dest.writeValue(receiptPath);
            dest.writeValue(batchID);
            dest.writeValue(clientID);
            dest.writeValue(instituteID);
        }

        public int describeContents() {
            return 0;
        }

    }
}