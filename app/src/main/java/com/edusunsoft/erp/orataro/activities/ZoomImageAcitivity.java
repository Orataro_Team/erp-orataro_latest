package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ViewPagerAdapter;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener.Anchor;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class ZoomImageAcitivity extends Activity implements OnClickListener, ResponseWebServices {

    ImageView img_back, iv_fb, iv_save;
    Context mContext;
    TextView txtHeader;
    LinearLayout ll_fb;
    WallPostModel wallPostModel;
    ArrayList<String> photoList;
    QuickAction quickActionForEditOrDelete;
    ActionItem actionEdit;
    private static final int ID_SAVE = 5;
    private static final int ID_DELETE = 6;

    LinearLayout ll_save;
    ArrayList<String> listPhoto;
    ViewPager vpPager;
    ViewPagerAdapter pagerAdapter;
    public String img;
    public Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_img);

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ZoomImageAcitivity.this;
        wallPostModel = new WallPostModel();

        vpPager = (ViewPager) findViewById(R.id.vpPager);
        img_back = (ImageView) findViewById(R.id.img_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        iv_save.setVisibility(View.VISIBLE);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);

        saveImageMenu();

        ll_fb = (LinearLayout) findViewById(R.id.ll_fb);
        iv_fb = (ImageView) findViewById(R.id.iv_fb_zoom);
        txtHeader = (TextView) findViewById(R.id.header_text);
        if (getIntent() != null) {
            img = getIntent().getStringExtra("img");
            Log.d("getImage", img);
            String name = getIntent().getStringExtra("name");
            listPhoto = getIntent().getStringArrayListExtra("imgList");

            try {
                txtHeader.setText(getResources().getString(R.string.PostImages) + " (" + new UserSharedPrefrence(mContext).getLoginModel().getFirstName() + ")");
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (listPhoto != null && listPhoto.size() > 0) {

                pagerAdapter = new ViewPagerAdapter(mContext, listPhoto);
                vpPager.setAdapter(pagerAdapter);
                vpPager.setVisibility(View.VISIBLE);
                ll_fb.setVisibility(View.GONE);

            } else {

                vpPager.setVisibility(View.GONE);
                ll_fb.setVisibility(View.VISIBLE);

            }

            if (img != null && !img.equalsIgnoreCase("")) {

                if (img.contains(ServiceResource.BASE_IMG_URL)) {
                    try {
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.photo)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mContext)
                                .load(img)
                                .apply(options)
                                .into(iv_fb);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Bitmap bm;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(img, options);

                    final int REQUIRED_SIZE = 200;
                    int scale = 1;
                    while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                            && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                        scale *= 2;
                    options.inSampleSize = scale;
                    options.inJustDecodeBounds = false;
                    bm = BitmapFactory.decodeFile(img, options);

                    iv_fb.setImageBitmap(bm);

                }

            }

        }

        iv_fb.setScaleType(ImageView.ScaleType.MATRIX);
        ll_fb.setOnTouchListener(new PanAndZoomListener(ll_fb, iv_fb, Anchor.TOPLEFT));
        img_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
    }

    private void saveImageMenu() {

        actionEdit = new ActionItem(ID_SAVE, "Save", mContext.getResources().getDrawable(R.drawable.save31));
        quickActionForEditOrDelete = new QuickAction(mContext, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionEdit);

        quickActionForEditOrDelete
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int _pos, int actionId) {
                        ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);
                        if (actionId == ID_SAVE) {
                            new GetImageFromUrl().execute(img);
//                            saveImageFromImageView(bitmap);
                        }
                    }
                });

        quickActionForEditOrDelete
                .setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {

                    }

                });

    }

    public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                FileOutputStream fos;
                File file = new File(Utility.getFilename());
                Log.d("getFilePath", file.getAbsolutePath());
                fos = new FileOutputStream(file);
                bitmap.compress(CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
                Utility.toast(mContext, getResources().getString(R.string.ImageSave) + file.getAbsolutePath());

            } catch (FileNotFoundException e) {

                Log.e("GREC", e.getMessage(), e);

            } catch (IOException e) {

                Log.e("GREC", e.getMessage(), e);

            }

        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.ll_save:
                quickActionForEditOrDelete.show(ll_save);
                break;
            default:
                break;
        }
    }


    public void saveImageFromImageView(Bitmap bitmap) {


//        Bitmap bitmap = ((BitmapDrawable) iv_fb.getDrawable()).getBitmap();
//
//        File file = new File(Utility.getFilename());
//        try {
//            file.createNewFile();
//            FileOutputStream ostream = new FileOutputStream(file);
//            bitmap.compress(CompressFormat.JPEG, 100, ostream);
//            ostream.close();
//
//            ContentValues values = new ContentValues();
//            //
//            values.put(Images.Media.DATE_TAKEN,
//                    System.currentTimeMillis());
//            values.put(Images.Media.MIME_TYPE, "image/jpeg");
//            values.put(MediaStore.MediaColumns.DATA, file.toString());
//
//            ((Activity) mContext).getContentResolver().insert(
//                    Images.Media.EXTERNAL_CONTENT_URI, values);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Log.d("getFilePAth", file.getAbsolutePath());
//        Utility.toast(mContext, getResources().getString(R.string.ImageSave) + file.getAbsolutePath());
////        Toast.makeText(mContext, getResources().getString(R.string.ImageSave) + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
    }

    public void morePicList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOTYPE, "ALBUM"));

        arrayList.add(new PropertyVo(ServiceResource.ASSOID, "14074dca-bf3b-4de9-959a-121ab0b66e90"));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETPOSTEDPICS_METHODNAME,
                ServiceResource.POST_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.GETPOSTEDPICS_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                photoList = new ArrayList<String>();
                JSONArray array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    String photo = obj.getString(ServiceResource.PHOTO);
                    photoList.add(photo);
                    Log.v("Photo", photo);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

}
