
package com.edusunsoft.erp.orataro.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExamTerms implements Parcelable {

    public final static Parcelable.Creator<ExamTerms> CREATOR = new Creator<ExamTerms>() {

        public ExamTerms createFromParcel(Parcel in) {
            ExamTerms instance = new ExamTerms();
            instance.studentID = ((String) in.readValue((String.class.getClassLoader())));
            instance.gRNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.examSeatNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.studentName = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectName = ((String) in.readValue((String.class.getClassLoader())));
            instance.paperTypeName = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.terms, (Term.class.getClassLoader()));
            return instance;
        }

        public ExamTerms[] newArray(int size) {
            return (new ExamTerms[size]);
        }

    };

    @SerializedName("StudentID")
    @Expose
    private String studentID;
    @SerializedName("GRNo")
    @Expose
    private String gRNo;
    @SerializedName("ExamSeatNo")
    @Expose
    private String examSeatNo;
    @SerializedName("StudentName")
    @Expose
    private String studentName;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("PaperTypeName")
    @Expose
    private String paperTypeName;
    @SerializedName("terms")
    @Expose
    private List<Term> terms = null;

    public static String isNull(String s) {
        if (s != null && !s.equalsIgnoreCase("") && !s.equalsIgnoreCase("null")) {
            return s;
        } else {
            return "-";
        }

    }

    public String getStudentID() {
        return isNull(studentID);
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getGRNo() {
        return isNull(gRNo);
    }

    public void setGRNo(String gRNo) {
        this.gRNo = gRNo;
    }

    public String getExamSeatNo() {
        return isNull(examSeatNo);
    }

    public void setExamSeatNo(String examSeatNo) {
        this.examSeatNo = examSeatNo;
    }

    public String getStudentName() {
        return isNull(studentName);
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSubjectName() {
        return isNull(subjectName);
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getPaperTypeName() {
        return isNull(paperTypeName);
    }

    public void setPaperTypeName(String paperTypeName) {
        this.paperTypeName = paperTypeName;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(studentID);
        dest.writeValue(gRNo);
        dest.writeValue(examSeatNo);
        dest.writeValue(studentName);
        dest.writeValue(subjectName);
        dest.writeValue(paperTypeName);
        dest.writeList(terms);
    }

    public int describeContents() {
        return 0;
    }

    public static class Term implements Parcelable {

        public final static Parcelable.Creator<Term> CREATOR = new Creator<Term>() {

            public Term createFromParcel(Parcel in) {
                Term instance = new Term();
                instance.totalTerm = ((String) in.readValue((String.class.getClassLoader())));
                instance.convertedTerm = ((String) in.readValue((String.class.getClassLoader())));
                instance.otherExmObt = ((String) in.readValue((String.class.getClassLoader())));
                instance.otherExmCObt = ((String) in.readValue((String.class.getClassLoader())));
                instance.totalSubConverted = ((String) in.readValue((String.class.getClassLoader())));
                instance.wholeTotalMarks = ((String) in.readValue((String.class.getClassLoader())));
                instance.totalObtained = ((String) in.readValue((String.class.getClassLoader())));
                instance.resultTerm = ((String) in.readValue((String.class.getClassLoader())));
                instance._class = ((String) in.readValue((String.class.getClassLoader())));
                instance.percentage = ((String) in.readValue((String.class.getClassLoader())));
                instance.percentile = ((String) in.readValue((String.class.getClassLoader())));
                instance.rank = ((String) in.readValue((String.class.getClassLoader())));
                instance.SubjectGrade = ((String) in.readValue((String.class.getClassLoader())));
                return instance;
            }

            public Term[] newArray(int size) {
                return (new Term[size]);
            }

        };
        @SerializedName("Total_Term")
        @Expose
        private String totalTerm;
        @SerializedName("Converted_Term")
        @Expose
        private String convertedTerm;
        @SerializedName("OtherExmObt")
        @Expose
        private String otherExmObt;
        @SerializedName("OtherExmCObt")
        @Expose
        private String otherExmCObt;
        @SerializedName("TotalSubConverted")
        @Expose
        private String totalSubConverted;
        @SerializedName("WholeTotalMarks")
        @Expose
        private String wholeTotalMarks;
        @SerializedName("TotalObtained")
        @Expose
        private String totalObtained;
        @SerializedName("ResultTerm")
        @Expose
        private String resultTerm;
        @SerializedName("Class")
        @Expose
        private String _class;
        @SerializedName("Percentage")
        @Expose
        private String percentage;
        @SerializedName("Percentile")
        @Expose
        private String percentile;
        @SerializedName("Rank")
        @Expose
        private String rank;
        @SerializedName("SubjectGrade")
        @Expose
        private String SubjectGrade;

        public String getSubjectGrade() {
            return isNull(SubjectGrade);
        }

        public void setSubjectGrade(String subjectGrade) {
            SubjectGrade = subjectGrade;
        }

        public String getTotalTerm() {
            return isNull(totalTerm);
        }

        public void setTotalTerm(String totalTerm) {
            this.totalTerm = totalTerm;
        }

        public String getConvertedTerm() {
            return isNull(convertedTerm);
        }

        public void setConvertedTerm(String convertedTerm) {
            this.convertedTerm = convertedTerm;
        }

        public String getOtherExmObt() {
            return isNull(otherExmObt);
        }

        public void setOtherExmObt(String otherExmObt) {
            this.otherExmObt = otherExmObt;
        }

        public String getOtherExmCObt() {
            return isNull(otherExmCObt);
        }

        public void setOtherExmCObt(String otherExmCObt) {
            this.otherExmCObt = otherExmCObt;
        }

        public String getTotalSubConverted() {
            return isNull(totalSubConverted);
        }

        public void setTotalSubConverted(String totalSubConverted) {
            this.totalSubConverted = totalSubConverted;
        }

        public String getWholeTotalMarks() {
            return wholeTotalMarks;
        }

        public void setWholeTotalMarks(String wholeTotalMarks) {
            this.wholeTotalMarks = wholeTotalMarks;
        }

        public String getTotalObtained() {
            return isNull(totalObtained);
        }

        public void setTotalObtained(String totalObtained) {
            this.totalObtained = totalObtained;
        }

        public String getResultTerm() {
            return isNull(resultTerm);
        }

        public void setResultTerm(String resultTerm) {
            this.resultTerm = resultTerm;
        }

        public String getClass_() {
            return isNull(_class);
        }

        public void setClass_(String _class) {
            this._class = _class;
        }

        public String getPercentage() {
            return isNull(percentage);
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String getPercentile() {
            return isNull(percentile);
        }

        public void setPercentile(String percentile) {
            this.percentile = percentile;
        }

        public String getRank() {
            return isNull(rank);
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(totalTerm);
            dest.writeValue(convertedTerm);
            dest.writeValue(otherExmObt);
            dest.writeValue(otherExmCObt);
            dest.writeValue(totalSubConverted);
            dest.writeValue(wholeTotalMarks);
            dest.writeValue(totalObtained);
            dest.writeValue(resultTerm);
            dest.writeValue(_class);
            dest.writeValue(percentage);
            dest.writeValue(percentile);
            dest.writeValue(rank);
            dest.writeValue(SubjectGrade);
        }

        public int describeContents() {
            return 0;
        }

    }
}
