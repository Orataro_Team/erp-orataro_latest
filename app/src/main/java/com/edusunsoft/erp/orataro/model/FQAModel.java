package com.edusunsoft.erp.orataro.model;

import java.util.List;


public class FQAModel {

	private List<FQAChildModel> childList;
	private String title;
	private String imageName="";

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<FQAChildModel> getChildList() {
		return childList;
	}

	public void setChildList(List<FQAChildModel> childList) {
		this.childList = childList;
	}

}
