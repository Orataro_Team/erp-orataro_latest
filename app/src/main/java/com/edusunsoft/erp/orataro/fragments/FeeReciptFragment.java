package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.FeeTransactionDetailActivity;
import com.edusunsoft.erp.orataro.adapter.FeesListAdapter;
import com.edusunsoft.erp.orataro.model.FeeTransactionDetailModel;
import com.edusunsoft.erp.orataro.model.FeesModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class FeeReciptFragment extends Fragment implements ResponseWebServices, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Context mContext;
    TextView txt_nodatafound;

    ListView lvfees;
    LinearLayout ll_remove;

    FeesModel feesReceiptModel;
    ArrayList<FeesModel> FeesReceiptList = new ArrayList<>();

    FeeTransactionDetailModel feestransactiondetailModel;
    public static ArrayList<FeeTransactionDetailModel> FeesTransactionDetailList = new ArrayList<>();

    // variable declaration for fee receipt detail
    private LinearLayoutManager mLayoutManager;
    LinearLayout lyl_transaction_detail;
    TextView txt_nodatafound_transactiion, txt_title, txt_show_transaction_detail, txt_total_amount;
    RecyclerView lvfees_transaction_detail;
    Double TotalAmount = 0.0;


    public FeeReciptFragment() {
    }


    public static FeeReciptFragment newInstance(String param1, String param2) {

        FeeReciptFragment fragment = new FeeReciptFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View convertview = inflater.inflate(R.layout.fragment_fees, container, false);
        mContext = getActivity();
        ImageView iv_std_icon = (ImageView) convertview.findViewById(R.id.iv_std_icon);
        TextView tv_std_name = (TextView) convertview.findViewById(R.id.tv_std_name);
        TextView tv_institutename = (TextView) convertview.findViewById(R.id.tv_schoolname);
        TextView tv_std_standard = (TextView) convertview.findViewById(R.id.tv_std_standard);
        TextView tv_std_division = (TextView) convertview.findViewById(R.id.tv_std_division);
        ll_remove = (LinearLayout) convertview.findViewById(R.id.ll_remove);
        txt_nodatafound = (TextView) convertview.findViewById(R.id.txt_nodatafound);
        lvfees = (ListView) convertview.findViewById(R.id.lvfees);
        txt_title = (TextView) convertview.findViewById(R.id.txt_title);
        txt_show_transaction_detail = (TextView) convertview.findViewById(R.id.txt_show_transaction_detail);
        txt_total_amount = (TextView) convertview.findViewById(R.id.txt_total_amount);
        txt_title.setVisibility(View.VISIBLE);
        txt_show_transaction_detail.setVisibility(View.VISIBLE);
        lyl_transaction_detail = (LinearLayout) convertview.findViewById(R.id.lyl_transaction_detail);
        txt_nodatafound_transactiion = (TextView) convertview.findViewById(R.id.txt_nodatafound_transactiion);
        lvfees_transaction_detail = (RecyclerView) convertview.findViewById(R.id.lvfees_transaction_detail);
        mLayoutManager = new LinearLayoutManager(getActivity());
        // use a linear layout manager
        lvfees_transaction_detail.setLayoutManager(mLayoutManager);

        if (HomeWorkFragmentActivity.txt_header != null) {
            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.feesreceipt) + " (" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
        }

        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
        if (ProfilePicture != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + ProfilePicture
                                .replace("//", "/")
                                .replace("//", "/"))
                        .apply(options)
                        .into(iv_std_icon);

            } catch (Exception e) {
                e.printStackTrace();
            }

            tv_std_name.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_FULLNAME()));
            tv_institutename.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getINSTITUTENAME()));
            tv_std_standard.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_GRADENAME()));
            tv_std_division.setText(Utility.isValidStr(new UserSharedPrefrence(mContext).getLOGIN_DIVISIONNAME()));

        }

        txt_show_transaction_detail.setOnClickListener(this);

        return convertview;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            feesinfo();
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }
    }

    public void feesinfo() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.STUDENTID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        Log.d("RequestFeee", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETSTUDENTFEEINFOSTRUCTURE_METHODNAME, ServiceResource.FEES_URL1);

    }

    @Override
    public void response(String result, String methodName) {

        Log.d("getFeeResponse", result);
        if (ServiceResource.GETSTUDENTFEEINFOSTRUCTURE_METHODNAME.equalsIgnoreCase(methodName)) {

            try {

                TotalAmount = 0.0;
                FeesReceiptList.clear();
                JSONObject jobj = new JSONObject(result + "}");
                JSONArray mainJsonArray2 = jobj.getJSONArray(ServiceResource.TABLE);
                for (int i = 0; i < mainJsonArray2.length(); i++) {

                    JSONObject childobj = mainJsonArray2.getJSONObject(i);

                    feesReceiptModel = new FeesModel();
                    feesReceiptModel.setAmount(childobj.getString("Amount"));
                    feesReceiptModel.setUnpaid_amount(childobj.getDouble("UnpaidAmount"));
                    feesReceiptModel.setTotalPaidAmount(childobj.getDouble("TotalPaidAmount"));
                    feesReceiptModel.setFeesStructuteID(childobj.getString("FeesStructuteID"));
                    feesReceiptModel.setStudentID(childobj.getString("StudentID"));
                    feesReceiptModel.setDisplayName(childobj.getString("DisplayName"));
                    feesReceiptModel.setBatchName(childobj.getString("BatchName"));
                    feesReceiptModel.setStudentFeesCollectionID(childobj.getString("StudentFeesCollectionID"));
                    feesReceiptModel.setClientID(childobj.getString("ClientID"));
                    feesReceiptModel.setInstituteID(childobj.getString("InstituteID"));
                    feesReceiptModel.setBatchID(childobj.getString("BatchID"));

                    if (feesReceiptModel.getTotalPaidAmount() > 0) {

                        txt_total_amount.setVisibility(View.VISIBLE);
                        TotalAmount = TotalAmount + feesReceiptModel.getUnpaid_amount();
                        txt_total_amount.setText(String.valueOf(TotalAmount));
                        FeesReceiptList.add(feesReceiptModel);

                    }

                }

                FeesListAdapter listAdapter = new FeesListAdapter(mContext, FeesReceiptList, "FromFeeReceipt");
                lvfees.setAdapter(listAdapter);

                FeesTransactionDetailList.clear();
                JSONObject jobj1 = new JSONObject(result + "}");
                JSONArray mainJsonArray3 = jobj1.getJSONArray(ServiceResource.TABLE3);
                for (int j = 0; j < mainJsonArray3.length(); j++) {

                    JSONObject transactiondetailobj = mainJsonArray3.getJSONObject(j);

                    feestransactiondetailModel = new FeeTransactionDetailModel();
                    feestransactiondetailModel.setAmount(transactiondetailobj.getDouble("TransactionAmount"));
                    feestransactiondetailModel.setPaymentTransactionDetailsID(transactiondetailobj.getString("PaymentTransactionDetailsID"));
                    feestransactiondetailModel.setTransactionFrom(transactiondetailobj.getString("TransactionFrom"));
                    feestransactiondetailModel.setTransactionDate(transactiondetailobj.getString("TransactionDate"));
                    feestransactiondetailModel.setTransactionStatus(transactiondetailobj.getString("TransactionStatus"));
                    feestransactiondetailModel.setPaymentGetWayTranID(transactiondetailobj.getString("PaymentGetWayTranID"));
                    feestransactiondetailModel.setBookID(transactiondetailobj.getString("BookID"));
                    feestransactiondetailModel.setStructureDisplayName(transactiondetailobj.getString("StructureDisplayName"));
                    feestransactiondetailModel.setUserName(transactiondetailobj.getString("UserName"));
                    feestransactiondetailModel.setReceiptNo(transactiondetailobj.getString("ReceiptNo"));
                    feestransactiondetailModel.setStrTransactionDate(transactiondetailobj.getString("strTransactionDate"));

                    FeesTransactionDetailList.add(feestransactiondetailModel);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (ServiceResource.STUDENTFEESCOLLECTIONLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                JSONObject jobj = new JSONObject(result + "}");
                JSONArray mainJsonArray = jobj.getJSONArray(ServiceResource.TABLE);
                Type type = new TypeToken<ArrayList<FeesModel>>() {
                }.getType();
                ArrayList<FeesModel> listFees = new Gson().fromJson(mainJsonArray.toString(), type);
                FeesListAdapter listAdapter = new FeesListAdapter(mContext, listFees, "");
                lvfees.setAdapter(listAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txt_show_transaction_detail:

                Intent mintent = new Intent(getActivity(), FeeTransactionDetailActivity.class);
                mContext.startActivity(mintent);

                break;

        }

    }

}
