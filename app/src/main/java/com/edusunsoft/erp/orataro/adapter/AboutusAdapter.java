package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

import java.util.ArrayList;

public class AboutusAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> list;
    private ArrayList<Integer> iconList;
    private LayoutInflater layoutInfalater;
    private int[] colors2 = new int[]{Color.parseColor("#F2F2F2"),
            Color.parseColor("#FFFFFF")};

    public AboutusAdapter(Context mContext) {
        this.mContext = mContext;
        list = new ArrayList<String>();
        list.add("Share App");
        list.add("Rate App");
        list.add("Our Other Free Apps");
        list.add("Facebook");
        list.add("Twitter");
        list.add("LinkedIn");
//		list.add("Google+");
        list.add("About Us");
        //list.add("Contact Us");
        list.add("Help Desk");

        iconList = new ArrayList<Integer>();
        iconList.add(R.drawable.share);
        iconList.add(R.drawable.star);
        iconList.add(R.drawable.more23);
        iconList.add(R.drawable.facebook);
        iconList.add(R.drawable.twitter);
        iconList.add(R.drawable.linkdin);
//		iconList.add(R.drawable.googleplus);
        iconList.add(R.drawable.aboutorataro);
        iconList.add(R.drawable.customersupport);

    }

    @Override
    public int getCount() {

        return list.size();
        
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.menushell, parent, false);

        ImageView menu_item = (ImageView) convertView.findViewById(R.id.menu_item);
        TextView tv_menu = (TextView) convertView.findViewById(R.id.tv_menu);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.img_arrow);
        TextView txt_count = (TextView) convertView.findViewById(R.id.txt_count);

        menu_item.setVisibility(View.VISIBLE);
        arrow.setVisibility(View.GONE);
        txt_count.setVisibility(View.GONE);
        tv_menu.setText(list.get(position));
        tv_menu.setTextColor(Color.BLACK);

        menu_item.setColorFilter(Color.parseColor("#27305B"));
        menu_item.setImageResource(iconList.get(position));
        int colorPos = position % colors2.length;
        convertView.setBackgroundColor(colors2[colorPos]);
        return convertView;
    }

}
