package com.edusunsoft.erp.orataro.FragmentActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.fragments.PageDetailFragment;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

public class Information_prayerActivity extends FragmentActivity {


    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;
    String isFrom;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_prayeractivity);
        mContext = Information_prayerActivity.this;
        imgLeftHeader = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);


        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.GONE);
        if (getIntent() != null) {
            isFrom = getIntent().getStringExtra("isFrom");
        }
        try {

            Log.d("getPageIDd", new UserSharedPrefrence(mContext).getLoginModel().getSchoolTiming());

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (isFrom.equalsIgnoreCase(ServiceResource.INFORMATION_FLAG)) {
                txtHeader.setText(getResources().getString(R.string.Information) + " (" + Utility.GetFirstName(mContext) + ")");
                ft.replace(R.id.containar_information, PageDetailFragment.newInstance(ServiceResource.INFORMATION_STRING, new UserSharedPrefrence(mContext).getLoginModel().getInformation()));
            } else {
                txtHeader.setText(getResources().getString(R.string.SchoolPrayer) + " (" + Utility.GetFirstName(mContext) + ")");
                ft.replace(R.id.containar_information, PageDetailFragment.newInstance(ServiceResource.PRAYER_STRING, new UserSharedPrefrence(mContext).getLoginModel().getPrayer()));
            }
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgLeftHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });

    }

}
