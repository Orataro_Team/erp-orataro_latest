package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PTCommunicationListDataDao {

    @Insert
    void insertPTCommunicationDataList(PtCommunicationModel ptCommunicationModel);

    @Query("SELECT * from PTCommunicationList")
    List<PtCommunicationModel> getPTComunicationList();

    @Query("SELECT * from PTCommunicationList WHERE MemberID = :memberid")
    List<PtCommunicationModel> getPTComunicationList(String memberid);

    @Query("SELECT * from PTCommunicationList WHERE TeacherID = :teacherid")
    List<PtCommunicationModel> getPTComunicationListForStudent(String teacherid);

    @Query("DELETE  FROM PTCommunicationList where CommunicationID =:CommunicationID")
    int deletePTCommunication(String CommunicationID);

    @Query("DELETE  FROM PTCommunicationList where MemberID =:memberid")
    int deletePTCommunicationByMemberID(String memberid);

    @Query("DELETE  FROM PTCommunicationList where TeacherID =:teacherid")
    int deletePTCommunicationByTeacherID(String teacherid);

    @Query("DELETE  FROM PTCommunicationList")
    int deletePTCommunicationList();


}
