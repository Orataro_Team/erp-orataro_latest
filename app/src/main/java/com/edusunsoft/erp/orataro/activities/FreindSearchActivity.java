package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FriendSearchAdapter;
import com.edusunsoft.erp.orataro.model.FriendSearchModel;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FreindSearchActivity extends Activity implements OnClickListener, ResponseWebServices {

	// EditText edtHeader;
	ListView lst_find_friend;
	ImageView img_back, iv_search, img_save;
	ArrayList<PersonModel> personList;
	int count = 1;
	EditText edtTeacherName;
	boolean isFriend;
	private Context mContext = FreindSearchActivity.this;
	LinearLayout searchlayout;
	TextView txt_nodatafound;
	String friend_name;
	FriendSearchAdapter searchFriendAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_friends);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);

		txt_nodatafound = (TextView) findViewById(R.id.txt_nodatafound);
		// edtHeader = (EditText) findViewById(R.id.edtHeader);
		lst_find_friend = (ListView) findViewById(R.id.lst_find_friend);
		searchlayout = (LinearLayout) findViewById(R.id.searchlayout);
		searchlayout.setVisibility(View.VISIBLE);

		iv_search = (ImageView) findViewById(R.id.iv_search);
		img_save = (ImageView) findViewById(R.id.img_save);
		img_back = (ImageView) findViewById(R.id.img_back);

		iv_search.setOnClickListener(this);
		img_save.setOnClickListener(this);
		img_back.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_search:

			friend_name = edtTeacherName.getText().toString();

			try {
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
						0);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (Utility.isNetworkAvailable(mContext)) {
			 SearchFriendList();
			} else {
				Utility.showAlertDialog(mContext,
						"Please Check Your Internet Connection","Error");
			}
			break;

		case R.id.img_save:

			finish();

			break;

		case R.id.img_back:

//			Intent intent = new Intent(mContext, FriendListActivity.class);
//			startActivity(intent);
            finish();
			break;

		default:
			break;
		}
	}



	public void SearchFriendList(){
		ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
		arrayList.add(new PropertyVo(ServiceResource.SEARCH_SEARCHNAME, friend_name));
		arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
				new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
		arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
				new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
		arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
				new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
		new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SEARCH_FRIENDS_METHODNAME,
				ServiceResource.FRIENDS_URL);
	}
/**
 * getResponse for all Webservices
 */
	@Override
	public void response(String result, String methodName) {
	if(ServiceResource.SEARCH_FRIENDS_METHODNAME.equalsIgnoreCase(methodName)){
		JSONArray jsonObj;
		try {
			Global.searchFriendModels = new ArrayList<FriendSearchModel>();

			jsonObj = new JSONArray(result);
			// JSONArray detailArrray= jsonObj.getJSONArray("");

			for (int i = 0; i < jsonObj.length(); i++) {
				JSONObject innerObj = jsonObj.getJSONObject(i);
				FriendSearchModel searchFriendModel = new FriendSearchModel();
				searchFriendModel.setMemberID(innerObj
						.getString(ServiceResource.SEARCH_MEMBERID));
				searchFriendModel.setFullName(innerObj
						.getString(ServiceResource.SEARCH_FULLNAME));
				searchFriendModel.setWallID(innerObj
						.getString(ServiceResource.SEARCH_WALLID));
				searchFriendModel.setRoleName(innerObj
						.getString(ServiceResource.SEARCH_ROLENAME));
				searchFriendModel.setProfilePicture(innerObj
						.getString(ServiceResource.SEARCH_PROFILEPICTURE));

				if (searchFriendModel.getProfilePicture() != null
						&& !searchFriendModel.getProfilePicture()
								.equals("")) {

					if (searchFriendModel.getProfilePicture().contains(
							"/img/"))
						searchFriendModel
								.setProfilePicture(ServiceResource.BASE_IMG_URL1
										+ searchFriendModel
												.getProfilePicture()
												);
					else
						searchFriendModel
								.setProfilePicture(ServiceResource.BASE_IMG_URL1

										+ searchFriendModel
												.getProfilePicture()
												);
				}

				searchFriendModel.setIsFriend(innerObj
						.getString(ServiceResource.SEARCH_ISFRIEND));
				searchFriendModel.setIsRequested(innerObj
						.getString(ServiceResource.SEARCH_ISREQUESTED));
				searchFriendModel.setGradeID(innerObj
						.getString(ServiceResource.FRIENDS_GRADEID));
				searchFriendModel.setGradeName(innerObj
						.getString(ServiceResource.FRIENDS_GRADENAME));
				
				searchFriendModel.setDivisionID(innerObj
						.getString(ServiceResource.FRIENDS_DIVISIONID));
				searchFriendModel.setDivisionName(innerObj
						.getString(ServiceResource.FRIENDS_DIVISIONNAME));
				Global.searchFriendModels.add(searchFriendModel);

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (Global.searchFriendModels != null
				&& Global.searchFriendModels.size() > 0) {
			txt_nodatafound.setVisibility(View.GONE);
			searchFriendAdapter = new FriendSearchAdapter(mContext,
					Global.searchFriendModels);
			lst_find_friend.setAdapter(searchFriendAdapter);

		} else {
			searchlayout.setVisibility(View.VISIBLE);
			lst_find_friend.setVisibility(View.GONE);
			txt_nodatafound.setVisibility(View.VISIBLE);
			txt_nodatafound.setText(mContext.getResources().getString(R.string.nosearchdata));
		}
		
		
	}
		
	}
	
}
