package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.HealthRecordModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class HealthRecordActivity extends Activity implements OnClickListener, ResponseWebServices {

    Context mContext;
    private ImageView img_back, imgRightHeader;
    LinearLayout ll_save;
    EditText edt_one, edt_two, edt_three, edt_four, edt_five, edt_height, edt_weight, edt_bloodgroup, edt_familydoctor, edt_contactno, edt_case;
    TextView txtHeader;
    public String message;
    public JSONArray jsonObj;
    public boolean isValid;
    HealthRecordModel healthModel;
    private TextView txtstudentName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.health_record_fragment);

        mContext = this;

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        txtstudentName = (TextView) findViewById(R.id.txtstudentName);
        edt_one = (EditText) findViewById(R.id.edt_one);
        edt_two = (EditText) findViewById(R.id.edt_two);
        edt_three = (EditText) findViewById(R.id.edt_three);
        edt_four = (EditText) findViewById(R.id.edt_four);
        edt_five = (EditText) findViewById(R.id.edt_five);

        edt_height = (EditText) findViewById(R.id.edt_height);
        edt_weight = (EditText) findViewById(R.id.edt_weight);
        edt_bloodgroup = (EditText) findViewById(R.id.edt_bloodgroup);
        edt_familydoctor = (EditText) findViewById(R.id.edt_familydocotr);
        edt_contactno = (EditText) findViewById(R.id.edt_contactno);
        edt_case = (EditText) findViewById(R.id.edt_case);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);


        healthRecord();

        img_back = (ImageView) findViewById(R.id.img_home);
        imgRightHeader = (ImageView) findViewById(R.id.img_menu);
        img_back.setImageResource(R.drawable.back);
        txtHeader = (TextView) findViewById(R.id.header_text);
        img_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        imgRightHeader.setOnClickListener(this);
        txtstudentName.setText(Html.fromHtml(getResources().getString(R.string.fillthefollw) + " <b>" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + "</b>."));
        imgRightHeader.setVisibility(View.INVISIBLE);
        txtHeader.setText(getResources().getString(R.string.HealthRecord));

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent intent;
        switch (v.getId()) {

            case R.id.img_home:

                finish();

                break;
            case R.id.ll_save:

                if (Utility.isNetworkAvailable(mContext)) {
                    if (!Utility.isNull(edt_height.getText().toString())) {
                        toast(getResources().getString(R.string.PleaseAddHeight));
                    } else if (!Utility.isNull(edt_weight.getText().toString())) {
                        toast(getResources().getString(R.string.PleaseAddWeight));
                    } else {
                        addHealthRecord();
                    }
                } else {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                }

                break;

            default:
                break;
        }
    }


    private void toast(String text) {
        // TODO Auto-generated method stub
        Utility.toast(mContext, text);
//		Toast.makeText(mContext, text, 1).show();
    }


    private void fillData() {

        if (healthModel != null) {
            if (Utility.isNull(healthModel.getBloodGroupTerm())) {
                edt_bloodgroup.setText(healthModel.getBloodGroupTerm());
            }
            if (Utility.isNull(healthModel.getHeight())) {
                edt_height.setText(healthModel.getHeight());
            }
            if (Utility.isNull(healthModel.getWeight())) {
                edt_weight.setText(healthModel.getWeight());
            }
            if (Utility.isNull(healthModel.getFamilyDoctorName())) {
                edt_familydoctor.setText(healthModel.getFamilyDoctorName());
            }
            if (Utility.isNull(healthModel.getContactNo())) {
                edt_contactno.setText(healthModel.getContactNo());
            }
            if (Utility.isNull(healthModel.getPatientID())) {
                edt_case.setText(healthModel.getPatientID());
            }
            if (Utility.isNull(healthModel.getAllergiesIfAny())) {
                edt_one.setText(healthModel.getAllergiesIfAny());
            }
            if (Utility.isNull(healthModel.getSpecificDiseasSuffered())) {
                edt_two.setText(healthModel.getSpecificDiseasSuffered());
            }
            if (Utility.isNull(healthModel.getOperationUndergone())) {
                edt_three.setText(healthModel.getOperationUndergone());
            }
            if (Utility.isNull(healthModel.getAnyOtheDiseasesForWhichTheChildIsOnRegular())) {
                edt_four.setText(healthModel.getAnyOtheDiseasesForWhichTheChildIsOnRegular());
            }
            if (Utility.isNull(healthModel.getColourFearIfAny())) {
                edt_five.setText(healthModel.getColourFearIfAny());
            }
        }

    }

    public void healthRecord() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.HOLIDAY_MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.HOLIDAY_CLIENTID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.HOLIDAY_INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.HOLIDAY_BEACHID, null));


//	examresult = webcall.getJSONFromSOAPWS(
//			ServiceResource.HEALTH_RECORD_METHODNAME, map,
//			ServiceResource.HEALTH_RECORD_URL);

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.HEALTH_RECORD_METHODNAME,
                ServiceResource.HEALTH_RECORD_URL);

    }

    public void addHealthRecord() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (healthModel != null && healthModel.getMemberHeathID() != null && healthModel.getMemberHeathID().length() > 0) {
            arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_ID, healthModel.getMemberHeathID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_ID, null));
        }
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_BLOODGROUP, edt_bloodgroup.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_HEIGHT, edt_height.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_WEIGHT, edt_weight.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_ALLERGIES, edt_one.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_SPECIFICDISEASES, edt_two.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_OPERATIONUNDERGROUND, edt_three.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_ANYOTHERDISEASE, edt_three.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_MEDICATION, edt_four.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_COLOUTFEAR, edt_five.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_FALILYDOCTORNAME, edt_familydoctor.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_CONTACTNO, edt_contactno.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_PATIENTID, edt_case.getText().toString()));
        Calendar c = Calendar.getInstance();
        arrayList.add(new PropertyVo(ServiceResource.HEALTH_RECORD_DATEOFLASTUPDATE, Utility.getDate(c.getTimeInMillis(), "MM/dd/yyyy")));

        Log.d("Healthrecordreq", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.HEALTH_RECORD_ADD_METHODNAME,
                ServiceResource.HEALTH_RECORD_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.HEALTH_RECORD_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                jsonObj = new JSONArray(result);
                healthModel = new HealthRecordModel();
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    healthModel.setMemberHeathID(innerObj.getString(ServiceResource.HEALTH_RECORD_MEMBERHEALTHID));
                    healthModel.setBloodGroupTerm(innerObj.getString(ServiceResource.HEALTH_RECORD_BLOODGROOUPTERM));
                    healthModel.setHeight(innerObj.getString(ServiceResource.HEALTH_RECORD_HEIGHT_PARSING));
                    healthModel.setWeight(innerObj.getString(ServiceResource.HEALTH_RECORD_WEIGHT_PARSING));
                    healthModel.setAllergiesIfAny(innerObj.getString(ServiceResource.HEALTH_RECORD_ALLERGIESIFANY));
                    healthModel.setSpecificDiseasSuffered(innerObj.getString(ServiceResource.HEALTH_RECORD_SPECIFICDISEASSUFFERED));
                    healthModel.setOperationUndergone(innerObj.getString(ServiceResource.HEALTH_RECORD_OPERATIONUNDERGONE));
                    healthModel.setAnyOtheDiseasesForWhichTheChildIsOnRegular(innerObj.getString(ServiceResource.HEALTH_RECORD_ANYOTHEDISEASESFORWHICHTHECHILDISONREGULAR));
                    healthModel.setMedicatoin(innerObj.getString(ServiceResource.HEALTH_RECORD_MEDICATOIN));
                    healthModel.setColourFearIfAny(innerObj.getString(ServiceResource.HEALTH_RECORD_COLOURFEARIFANY));
                    healthModel.setDateOfLastUpdate(innerObj.getString(ServiceResource.HEALTH_RECORD_DATEOFLASTUPDATE_PARSING));
                    healthModel.setFamilyDoctorName(innerObj.getString(ServiceResource.HEALTH_RECORD_FAMILYDOCTORNAME));
                    healthModel.setContactNo(innerObj.getString(ServiceResource.HEALTH_RECORD_CONTACTNO_PARSING));
                    healthModel.setPatientID(innerObj.getString(ServiceResource.HEALTH_RECORD_PATIENTID_PARSING));
                    healthModel.setUpdateOn(innerObj.getString(ServiceResource.HEALTH_RECORD_UPDATEON));
                    healthModel.setUpdateBy(innerObj.getString(ServiceResource.HEALTH_RECORD_UPDATEBY));
                    healthModel.setCreateOn(innerObj.getString(ServiceResource.HEALTH_RECORD_CREATEON));
                    healthModel.setCreateBy(innerObj.getString(ServiceResource.HEALTH_RECORD_CREATEBY));
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            fillData();

        }

        if (ServiceResource.HEALTH_RECORD_ADD_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    message = innerObj.getString(ServiceResource.MESSAGE);
                    if (message.contains("SuccessFully")) {
                        isValid = true;

                    }
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (isValid) {
                toast(message);
            }

        }
    }


}
