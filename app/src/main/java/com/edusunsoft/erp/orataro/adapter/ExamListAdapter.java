package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.Exam;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.List;

/**
 * Created by admin on 16-05-2017.
 */

public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.MyViewHolder> {

    public List<Exam> ExamList;
    public Context mcontext;
    public int selectedPosition;
    String EXAMBASEURL = "";
    String ExamIDs = "", BASEURL = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView DateofExam, Subject, ExamType, txt_exam_name;

        public MyViewHolder(View view) {

            super(view);

            DateofExam = (TextView) view.findViewById(R.id.txt_date);
            Subject = (TextView) view.findViewById(R.id.txt_subject);
            ExamType = (TextView) view.findViewById(R.id.txt_examtype);
            txt_exam_name = (TextView) view.findViewById(R.id.txt_exam_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selectedPosition = getLayoutPosition();

                    BASEURL = ServiceResource.BASE_IMG_URL1 + ExamList.get(selectedPosition).getClientID() + "/" + ExamList.get(selectedPosition).getInstituteID() + "/MarkSheets/" + ExamList.get(selectedPosition).getID() + ".pdf";
                    Log.d("getPDFURL", BASEURL);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.addCategory(Intent.CATEGORY_BROWSABLE);
                    i.setData(Uri.parse(BASEURL));
                    mcontext.startActivity(i);
//
//                    if (ServiceResource.CURRENTEXAM == 1) {
//
//                        EXAMBASEURL = ServiceResource.TERMEXAMRESULTURL;
//
//                    } else if (ServiceResource.CURRENTEXAM == 2) {
//
//                        EXAMBASEURL = ServiceResource.SEMESTEREXAMRESULTURL;
//
//                    } else if (ServiceResource.CURRENTEXAM == 3) {
//
//                        EXAMBASEURL = ServiceResource.CBSEEXAMRESULTURL;
//
//                    }
//
//                    if (ServiceResource.CURRENTEXAM == 3) {
//
//                        BASEURL = EXAMBASEURL + "?isFinalResult=false" + "&standardID=" + ExamList.get(selectedPosition).getStandardID() + "&divisionID=" + ExamList.get(selectedPosition).getDivisionID()
//                                + "&examID=" + ExamList.get(selectedPosition).getExamMasterID() + "&ExamName=" + ExamList.get(selectedPosition).getOnlyExamName() + "&ClientID=" + new UserSharedPrefrence(mcontext).getLoginModel().getClientID()
//                                + "&InstituteID=" + new UserSharedPrefrence(mcontext).getLoginModel().getInstituteID() + "&TermYearID=" + ExamList.get(selectedPosition).getBatchID() + "&ShiftID=" + ExamList.get(selectedPosition).getShiftID()
//                                + "&StudentID=" + new UserSharedPrefrence(mcontext).getLoginModel().getMemberID() + "&ExamIDs=";
//
//                    } else {
//
//                        BASEURL = EXAMBASEURL + "?isFinalResult=false" + "&standardID=" + ExamList.get(selectedPosition).getStandardID() + "&divisionID=" + ExamList.get(selectedPosition).getDivisionID()
//                                + "&examID=" + ExamList.get(selectedPosition).getExamMasterID() + "&ExamName=" + ExamList.get(selectedPosition).getOnlyExamName() + "&ClientID=" + new UserSharedPrefrence(mcontext).getLoginModel().getClientID()
//                                + "&InstituteID=" + new UserSharedPrefrence(mcontext).getLoginModel().getInstituteID() + "&TermYearID=" + ExamList.get(selectedPosition).getBatchID() + "&ShiftID=" + ExamList.get(selectedPosition).getShiftID()
//                                + "&StudentID=" + new UserSharedPrefrence(mcontext).getLoginModel().getMemberID();
//
//                    }


                }


            });


        }


    }


    public ExamListAdapter(Context mContext, List<Exam> ExamList) {
        this.mcontext = mContext;
        this.ExamList = ExamList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weekly_exam_list_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Exam examModel = ExamList.get(position);

        /*Fillup Detail with Validation */

        if (examModel.getExamName().equalsIgnoreCase("") || examModel.getExamName().equalsIgnoreCase("null") ||
                examModel.getExamName().equalsIgnoreCase(null)) {

            holder.txt_exam_name.setText("Exam Name");

        } else {

            holder.txt_exam_name.setText(examModel.getExamName() + " (" + "Std:" + examModel.getStandard() + " - " + "Div:" + examModel.getDivision() + " - " + "Year:" + examModel.getYear() + ")");

        }


        /*END*/

    }

    @Override
    public int getItemCount() {

        return ExamList.size();

    }

}
