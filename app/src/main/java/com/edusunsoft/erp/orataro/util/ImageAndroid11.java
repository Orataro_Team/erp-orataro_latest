package com.edusunsoft.erp.orataro.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageAndroid11 {

    //For android 11
    public static final int REQUEST_PICK_IMAGE_ANDROID_11 = 3331;
    public static final int REQUEST_IMAGE_CAPTURE_ANDROID_11 = 3332;
    public static final int REQUEST_PICK_FILE_ANDROID_11 = 3333;
    public static final int REQUEST_VIDEO_CAPTURE_ANDROID_11 = 3334;
    public static final int REQUEST_PICK_VIDEO_ANDROID_11 = 3335;


    //image URI to PATH string
    public static String getPathFromURI(Context context, Uri selectedImage) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath());
            folder.mkdirs();
            File file = new File(folder, "image_tmp.jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();

            int maxBufferSize = 1024 * 1024;

            try {
                InputStream inputStream = context.getContentResolver().openInputStream(selectedImage);
                Log.e("InputStream Size","Size " + inputStream);
                int  bytesAvailable = inputStream.available();
                int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                final byte[] buffers = new byte[bufferSize];

                FileOutputStream outputStream = new FileOutputStream(file);
                int read = 0;
                while ((read = inputStream.read(buffers)) != -1) {
                    outputStream.write(buffers, 0, read);
                }
                inputStream.close();
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    //image BITMAP to PATH string
    public static String getPathFromBitmap(Context context,Bitmap bmp) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath());
            folder.mkdirs();
            File file = new File(folder, "image_tmp.jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();

            try {

                FileOutputStream outputStream = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //image URI to File PATH string
    public static String getPathFromFileURL(Context context,Uri uri) {
        Uri returnUri  = uri;
        String  newDirName  = "orataro_tmp";

        Cursor returnCursor = context.getContentResolver().query(returnUri, new String[]{
                OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE
        }, null, null, null);


        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));

        if (name != null){
            String[] namearray = name.split("\\.");
            Log.e("PATH", "name1:: "+namearray[0]);
            Log.e("PATH", "name2:: "+namearray[1]);
            namearray[0] = "sample";
            name = namearray[0] + "."+namearray[1];
            Log.e("PATH", "final name:: "+name);
        }



        File output;
        if (!newDirName.equals("")) {
            File dir = new File(context.getFilesDir() + "/" + newDirName);
            if (!dir.exists()) {
                dir.mkdir();
            }
            output = new File(context.getFilesDir() + "/" + newDirName + "/" + name);
        } else {
            output = new File(context.getFilesDir() + "/" + name);
        }
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(output);
            int read = 0;
            int bufferSize = 1024;
            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

            inputStream.close();
            outputStream.close();

        } catch (Exception e) {

            Log.e("Exception", e.getMessage());
        }
        return output.getPath();
    }

}
