package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

public class StanderdAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater layoutInfalater;
    private TextView txtDivision, txtSubject, txtcount;
    private ArrayList<StandardModel> standardModels = new ArrayList<>();
    private TextView txtStandard;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    private boolean isShowCheckbox = false;
    private boolean isDivisionShow = false;
    private String isFrom = "";

    public StanderdAdapter(Context context, ArrayList<StandardModel> standardModels, String isFrom) {

        this.mContext = context;
        this.standardModels = standardModels;
        this.isFrom = isFrom;
    }

    public StanderdAdapter(Context context, ArrayList<StandardModel> standardModels, boolean isCheck) {
        this.mContext = context;
        this.standardModels = standardModels;
        isShowCheckbox = isCheck;
    }

    public StanderdAdapter(Context context, ArrayList<StandardModel> standardModels, boolean isCheck, boolean isDivisionShow) {
        this.mContext = context;
        this.standardModels = standardModels;
        isShowCheckbox = isCheck;
        this.isDivisionShow = isDivisionShow;
    }

    @Override
    public int getCount() {
        return standardModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.divisiongrade, parent, false);
        CheckBox chkdivision = (CheckBox) convertView.findViewById(R.id.chkdivision);
        txtStandard = (TextView) convertView.findViewById(R.id.txtStanderd);
        txtDivision = (TextView) convertView.findViewById(R.id.txtDivision);
        txtSubject = (TextView) convertView.findViewById(R.id.txtSubject);
        txtcount = (TextView) convertView.findViewById(R.id.txtcount);
        convertView.setBackgroundColor(colors[position % colors.length]);
        txtStandard.setText(standardModels.get(position).getStandardName());
        if (isShowCheckbox) {
            txtDivision.setVisibility(View.GONE);
            txtSubject.setVisibility(View.GONE);
        } else {
            txtDivision.setText(standardModels.get(position).getDivisionName());
            txtSubject.setText(standardModels.get(position).getSubjectName());
        }

        if (isDivisionShow) {
            txtDivision.setText(standardModels.get(position).getDivisionName());
            txtDivision.setVisibility(View.VISIBLE);
            chkdivision.setVisibility(View.VISIBLE);
        }
        if (standardModels.get(position).isChecked()) {
            chkdivision.setChecked(true);
        } else {
            chkdivision.setChecked(false);
        }

        chkdivision.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                standardModels.get(position).setChecked(isChecked);

            }


        });

        if (isFrom.equalsIgnoreCase(ServiceResource.LEAVE_FLAG)) {

            Log.d("getLeaveCount", standardModels.get(position).getCountleave());
            txtcount.setText(standardModels.get(position).getCountleave());
            txtcount.setVisibility(View.VISIBLE);

        } else {

            txtcount.setVisibility(View.GONE);

        }

        return convertView;

    }

    public ArrayList<StandardModel> getSelectedDivision() {
        ArrayList<StandardModel> selectedDivision = new ArrayList<StandardModel>();
        if (standardModels != null && standardModels.size() > 0) {
            for (int i = 0; i < standardModels.size(); i++) {
                if (standardModels.get(i).isChecked()) {
                    selectedDivision.add(standardModels.get(i));
                }
            }
        }
        return selectedDivision;
    }

}