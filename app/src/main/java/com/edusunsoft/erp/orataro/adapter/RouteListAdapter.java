package com.edusunsoft.erp.orataro.adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.OnRouteItemClickListener;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.databinding.RouteListRowBinding;
import com.edusunsoft.erp.orataro.model.RouteListResModel;

import java.util.ArrayList;

public class RouteListAdapter extends RecyclerView.Adapter<RouteListAdapter.RouteListAdapterHolder> {
    private ArrayList<RouteListResModel.Data> dataArrayList;
    private OnRouteItemClickListener onRouteItemClickListener;

    public RouteListAdapter(ArrayList<RouteListResModel.Data> dataArrayList, OnRouteItemClickListener onRouteItemClickListener) {
        this.dataArrayList = dataArrayList;
        this.onRouteItemClickListener = onRouteItemClickListener;
    }


    @NonNull
    @Override
    public RouteListAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new RouteListAdapterHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.route_list_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RouteListAdapterHolder routeListAdapterHolder, int position) {
        routeListAdapterHolder.bindView(dataArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataArrayList != null ? dataArrayList.size() : 0;
    }

    class RouteListAdapterHolder extends RecyclerView.ViewHolder {
        RouteListRowBinding routeListRowBinding;

        RouteListAdapterHolder(@NonNull RouteListRowBinding routeListRowBinding) {
            super(routeListRowBinding.getRoot());
            this.routeListRowBinding = routeListRowBinding;
        }

        void bindView(RouteListResModel.Data data) {
            routeListRowBinding.txtRouteType.setText(data.getIsForPickup() == 0 ? "Drop route" : "Pickup route");
            routeListRowBinding.txtRouteFrom.setText(data.getStartPoint());
            routeListRowBinding.txtDestination.setText(data.getEndPoint());
            routeListRowBinding.txtRouteName.setText(data.getRouteName());
            routeListRowBinding.txtRouteTime.setText(String.format("%s to %s", data.getStartTime(), data.getEndTime()));
            routeListRowBinding.txtDriverName.setText(data.getDriverName());
            routeListRowBinding.txtDriverMobileNo.setText(data.getDriverMobileNo());
            routeListRowBinding.txtDriverVehicleRegNo.setText(data.getRTORegNo());
            routeListRowBinding.txtBusName.setText(data.getBusName());

            try {
                routeListRowBinding.txtAtttendance.setText(data.getAttendanceStatusTerm());
            } catch (Exception e) {
                e.printStackTrace();
            }

            routeListRowBinding.cardView.setOnClickListener(v -> {
                if (onRouteItemClickListener != null) {
                    onRouteItemClickListener.onItemClick(data);
                }

            });

        }

    }

}
