package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ViewSchoolPrayerDetailsActivity;
import com.edusunsoft.erp.orataro.model.SchoolPrayerModel;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class SchoolPrayerListAdapter extends BaseAdapter {
	
	private LayoutInflater layoutInfalater;
	private Context context;
	private TextView txt_title, txt_prayer;
	private ArrayList<SchoolPrayerModel> schoolPrayerModels = new ArrayList<>();
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	public SchoolPrayerListAdapter(Context context, ArrayList<SchoolPrayerModel> schoolPrayerModels) {
		this.context = context;
		this.schoolPrayerModels = schoolPrayerModels;
	}

	@Override
	public int getCount() {
		return schoolPrayerModels.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.school_prayer_listraw, parent, false);
		convertView.setBackgroundColor(colors[position % colors.length]);
		txt_title = (TextView) convertView.findViewById(R.id.txt_title);
		txt_prayer = (TextView) convertView.findViewById(R.id.txt_prayer);
		txt_title.setText(schoolPrayerModels.get(position).getTitle());
		txt_prayer.setText(schoolPrayerModels.get(position).getPrayer());
		if(Utility.isNull(schoolPrayerModels.get(position).getPrayer())){
			if(schoolPrayerModels.get(position).getPrayer().length()> Constants.CONTINUEREADINGSIZE){
				String continueReadingStr = Constants.CONTINUEREADINGSTR;
				String tempText =schoolPrayerModels.get(position).getPrayer().substring(0, Constants.CONTINUEREADINGSIZE )+continueReadingStr;
				SpannableString text = new SpannableString(tempText);
				text.setSpan(new ForegroundColorSpan(Constants.CONTINUEREADINGTEXTCOLOR), Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZE+continueReadingStr.length(), 0);
				MyClickableSpan clickfor = new MyClickableSpan(tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

					@Override
					public void onClick(View widget) {
						Intent intent = new Intent(context, ViewSchoolPrayerDetailsActivity.class);
						intent.putExtra("Title", schoolPrayerModels.get(position).getTitle());
						intent.putExtra("Prayer", schoolPrayerModels.get(position).getPrayer());
						context.startActivity(intent);
					}
				};

				text.setSpan(clickfor, Constants.CONTINUEREADINGSIZE,  Constants.CONTINUEREADINGSIZE+continueReadingStr.length(), 0);
				txt_prayer.setMovementMethod(LinkMovementMethod.getInstance());
				txt_prayer.setText(text, BufferType.SPANNABLE);
			}else{
				txt_prayer.setText(schoolPrayerModels.get(position).getPrayer());	
			}

		}

		txt_prayer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ViewSchoolPrayerDetailsActivity.class);
				intent.putExtra("Title", schoolPrayerModels.get(position).getTitle());
				intent.putExtra("Prayer", schoolPrayerModels.get(position).getPrayer());
				context.startActivity(intent);
			}
		});

		return convertView;
	}

}
