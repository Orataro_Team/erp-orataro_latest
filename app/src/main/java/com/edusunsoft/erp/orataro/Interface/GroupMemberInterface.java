package com.edusunsoft.erp.orataro.Interface;

import com.edusunsoft.erp.orataro.model.GroupMemberModel;

import java.util.ArrayList;


public interface GroupMemberInterface {
	public void getAllGroupMember(int pos, ArrayList<GroupMemberModel> addGroupMemberModels);
}
