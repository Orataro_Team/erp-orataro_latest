package com.edusunsoft.erp.orataro.customeview;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;

import java.util.UUID;

public class DeviceHardWareId {

	public static String getDviceHardWarId(Context mContext) {

		String android_id = Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);
		final TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
		final String tmDevice, tmSerial, androidId;
	    tmDevice = "" + tm.getDeviceId();
	    tmSerial = "" + tm.getSimSerialNumber();
	    androidId = "" + Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);

	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
	    String deviceId = deviceUuid.toString();
	  
	    if(deviceId.equalsIgnoreCase("")){
	    	if(new UUIDSharedPrefrance(mContext).getUUID().equalsIgnoreCase("")){
	    	UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
	    	deviceId = uid.randomUUID().toString();
	    	new UUIDSharedPrefrance(mContext).setUUID(deviceId);
	    	}else{
	    		deviceId =new UUIDSharedPrefrance(mContext).getUUID();
	    	}
	    }
		return deviceId;
	}

}
