package com.edusunsoft.erp.orataro.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;
import com.edusunsoft.erp.orataro.customeview.NumberPicker;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SettingFragment extends Fragment implements ResponseWebServices, View.OnClickListener {
    private CheckBox chksoundnotification, chkvibrationnotification, chkledonnotification;
    private Context mContext;
    private Dialog numPickerDialog;
    private String days[] = {"Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday"};
    private String[] selectedMonthArray;
    private Calendar c;
    private int myear;
    private int month;
    private int day;
    private int[] daycount;
    private String[] monthArray = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY",
            "JUNE", "JULY", "AUGUST", "SEPTMBER", "OCTOBER", "NOVEMBER",
            "DECEMBER"};
    private String[] _month;
    private NumberPicker np_days;
    private NumberPicker np_month;
    private NumberPicker np_year;
    private String monthstrvalue = "", yearStrval = "", dateStrval = "";
    private Button btnClearCache;

    private Dialog mPoweroffDialog;
    ImageView img_logout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.settingfragment, null);
        mContext = getActivity();

        try {

            HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.notificationsettings) + " (" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);

        } catch (Exception e) {

            e.printStackTrace();

        }

        img_logout = (ImageView) v.findViewById(R.id.img_logout);
        img_logout.setOnClickListener(this);

        chksoundnotification = (CheckBox) v.findViewById(R.id.soundnotification);
        chkvibrationnotification = (CheckBox) v.findViewById(R.id.vibrationnotification);
        chkledonnotification = (CheckBox) v.findViewById(R.id.ledonnotification);
        btnClearCache = (Button) v.findViewById(R.id.btnClearCache);
        chkledonnotification.setChecked(new UserSharedPrefrence(getActivity()).getIsLedNotification());
        chksoundnotification.setChecked(new UserSharedPrefrence(getActivity()).getIsSoundNotification());
        chkvibrationnotification.setChecked(new UserSharedPrefrence(getActivity()).getIsVibrationNotification());

        chksoundnotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                new UserSharedPrefrence(getActivity()).setIsSoundNotification(isChecked);

            }

        });

        chkvibrationnotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                new UserSharedPrefrence(getActivity()).setIsVibrationNotification(isChecked);

            }

        });

        chkledonnotification.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                new UserSharedPrefrence(getActivity()).setIsLedNotification(isChecked);

            }

        });

        btnClearCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                settingClearCatchDialog();


            }


        });

        return v;

    }

    public void settingClearCatchDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);//
        dialog.setContentView(R.layout.dialogsetting);

        final TextView tv_start_date = (TextView) dialog.findViewById(R.id.tv_start_date);
        final TextView tv_end_date = (TextView) dialog.findViewById(R.id.tv_end_date);
        Button btndelete = (Button) dialog.findViewById(R.id.btndelete);
        ImageView imgcross = (ImageView) dialog.findViewById(R.id.imgcross);
        final CheckBox chk_circular = (CheckBox) dialog.findViewById(R.id.chk_circular);
        final CheckBox chk_classwork = (CheckBox) dialog.findViewById(R.id.chk_classwork);
        final CheckBox chk_homework = (CheckBox) dialog.findViewById(R.id.chk_homework);
        final CheckBox chk_notes = (CheckBox) dialog.findViewById(R.id.chk_notes);
        final CheckBox chk_todos = (CheckBox) dialog.findViewById(R.id.chk_todos);
        final CheckBox chk_notification = (CheckBox) dialog.findViewById(R.id.chk_notification);
        final CheckBox chk_poll = (CheckBox) dialog.findViewById(R.id.chk_poll);
        final CheckBox chk_photoVideo = (CheckBox) dialog.findViewById(R.id.chk_photoVideo);
        final CheckBox chk_wall = (CheckBox) dialog.findViewById(R.id.chk_wall);
        final CheckBox chk_profile = (CheckBox) dialog.findViewById(R.id.chk_profile);
        final CheckBox chk_otherwall = (CheckBox) dialog.findViewById(R.id.chk_otherwall);

        tv_start_date.setText(Utility.getDate(System.currentTimeMillis(), "dd-MMM-yyyy"));
        tv_end_date.setText(Utility.getDate(System.currentTimeMillis(), "dd-MMM-yyyy"));

        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthYearPicker("01-01-1970",
                        Utility.getDate(System.currentTimeMillis(), "MM-dd-yyyy"), tv_start_date);
            }
        });


        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMonthYearPicker("01-01-1970",
                        Utility.getDate(System.currentTimeMillis(), "MM-dd-yyyy"), tv_end_date);
            }
        });

        imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public int getMonth(String month) {

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("MMM").parse(month));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int monthInt = cal.get(Calendar.MONTH) + 1;

        return monthInt;

    }

    public void showMonthYearPicker(String sdate, String enddate, final TextView txtday) {

        //				sdate = "02-15-2015";
        //				enddate = "02-20-2016";
        String[] sMonth = sdate.split("-");
        String[] eMonth = enddate.split("-");

        final int startMonth = Integer.valueOf(sMonth[0]);
        final int startYear = Integer.valueOf(sMonth[2]);
        final int startDate = Integer.valueOf(sMonth[1]);
        final int endMonth = Integer.valueOf(eMonth[0]);
        final int endYear = Integer.valueOf(eMonth[2]);
        final int endDate = Integer.valueOf(eMonth[1]);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());


        final Dialog d = new Dialog(mContext, R.style.SampleTheme_Light);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        d.setContentView(R.layout.dialoge_number_picker);
        TextView tv_search_job = (TextView) d.findViewById(R.id.tv_search_job);
        tv_search_job.setText("Select Date");
        Button btn_ok = (Button) d.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) d.findViewById(R.id.btn_cancel);
        np_year = (NumberPicker) d.findViewById(R.id.np_year);
        np_month = (NumberPicker) d.findViewById(R.id.np_month);
        np_days = (NumberPicker) d.findViewById(R.id.np_days);


//		if(!Utility.isTeacher(mContext)){
        //np_days.setVisibility(View.GONE);
//		}
        np_year.setMinValue(startYear); // min value 0
        np_year.setMaxValue(endYear); // max value 100
        // np.setWrapSelectorWheel(true);
        np_year.setFocusable(true);
        np_year.setFocusableInTouchMode(true);

        _month = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        np_month.setFocusable(true);
        np_month.setFocusableInTouchMode(true);

        np_year.setValue(Integer.valueOf(txtday.getText().toString().split("-")[2]));


        setMonthPickerFromYear(startYear, startMonth, endYear, endMonth,
                np_year.getValue());
        //  np_month.setValue(1);
        int tempMonth = 0;
        if (Integer.valueOf(txtday.getText().toString().split("-")[2]) == startYear) {
            np_month.setValue((getMonthNumber(txtday.getText().toString().split("-")[1]) + 1) - (startMonth) + 1);
        } else {
            np_month.setValue((getMonthNumber(txtday.getText().toString().split("-")[1]) + 1));
        }
        setMonthPickerFromYear(startYear, startMonth, endYear, endMonth,
                np_year.getValue());
        //		np_month.setValue(1);
        setDatePickerFromYearAndMonth(startYear, startMonth, startDate,
                endYear, endMonth, endDate, np_year.getValue(),
                getMonthNumber(np_month.getDisplayedValues()[np_month
                        .getValue() - 1]));

        np_days.setValue(Integer.valueOf(txtday.getText().toString().split("-")[0]));
        //		np_days.setValue(1);

        //		if(Utility.dateToMilliSeconds(formattedDate, "dd-MM-yyyy")> Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchStart())
        //				&& Utility.dateToMilliSeconds(formattedDate, "dd-MM-yyyy")< Long.valueOf(new UserSharedPrefrence(mContext).getLoginModel().getBatchEnd())){
        //
        //			np_days.setValue(c.get(Calendar.DAY_OF_MONTH));
        //			np_month.setValue((c.get(Calendar.MONTH)-(startMonth))+1);//c.get(Calendar.MONTH)-startMonth);
        //			np_year.setValue(c.get(Calendar.YEAR));
        //			//			np_month.getDisplayedValues()[np_month.getValue()]
        //		}

        np_year.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal,
                                      int newVal) {

                setMonthPickerFromYear(startYear, startMonth, endYear,
                        endMonth, newVal);

                //				np_month.setValue(1);
                setDatePickerFromYearAndMonth(startYear, startMonth, startDate,
                        endYear, endMonth, endDate, np_year.getValue(),
                        getMonthNumber(np_month.getDisplayedValues()[np_month
                                .getValue() - 1]));
                //				np_days.setValue(1);

            }
        });

        np_month.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal,
                                      int newVal) {
                setDatePickerFromYearAndMonth(startYear, startMonth, startDate,
                        endYear, endMonth, endDate, np_year.getValue(),
                        getMonthNumber(np_month.getDisplayedValues()[np_month
                                .getValue() - 1]));
                //				np_days.setValue(1);

            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                String selecteddate = np_days.getValue() + "-" + np_month.getDisplayedValues()[np_month
                        .getValue() - 1] + "-" + np_year.getValue();

                if (c.getTimeInMillis() < Utility.dateToMilliSeconds(selecteddate, "dd-MMM-yyyy")) {
                    Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.pleaseselectpreviousday),"Error");
                } else {
                    txtday.setText(np_days.getValue() + "-" + np_month.getDisplayedValues()[np_month.getValue() - 1] + "-" + np_year.getValue());
                    monthstrvalue = getMonth(np_month.getDisplayedValues()[np_month.getValue() - 1]) + "";
                    yearStrval = np_year.getValue() + "";
                    String tempMonth = (getMonthNumber(np_month.getDisplayedValues()[np_month.getValue() - 1]) + 1) < 10 ?
                            "0" + (getMonthNumber(np_month.getDisplayedValues()[np_month.getValue() - 1]) + 1) : (getMonthNumber(np_month.getDisplayedValues()[np_month.getValue() - 1]) + 1) + "";
                    String tempday = np_days.getValue() < 10 ? "0" + np_days.getValue() : np_days.getValue() + "";
                    dateStrval = np_year.getValue() + "-" + tempMonth + "-" + tempday;
                }
                d.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });

        d.show();

    }

    public int getNumberOfDays(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        int numDays = calendar.getActualMaximum(Calendar.DATE);
        return numDays;
    }

    public int getMonthNumber(String month) {
        Date date = null;
        try {
            date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int _month = cal.get(Calendar.MONTH);
        return _month;
    }

    public void setMonthPickerFromYear(int pStartYear, int pStartMonth,
                                       int pEndYear, int pEndMonth, int currentYear) {
        final int startMonth = pStartMonth;
        final int startYear = pStartYear;
        final int endMonth = pEndMonth;
        final int endYear = pEndYear;

        if (currentYear == endYear) {
            np_month.setMinValue(1);
            np_month.setMaxValue(endMonth);

            String[] new_month = new String[endMonth];
            for (int i = 0; i < endMonth; i++) {
                new_month[i] = _month[i];
            }
            np_month.setDisplayedValues(new_month);
            np_month.initializeSelectorWheelIndices();

        } else {

            Log.e("Months", "" + (12 - startMonth));
            np_month.setMinValue(1);
            np_month.setMaxValue(12 - (startMonth - 1));
            String[] new_month = new String[12 - (startMonth - 1)];
            for (int i = 0; i < 12 - (startMonth - 1); i++) {
                new_month[i] = _month[(i + startMonth) - 1];
            }
            np_month.setDisplayedValues(new_month);
            np_month.initializeSelectorWheelIndices();

        }

    }

    public void setDatePickerFromYearAndMonth(int pStartYear, int pStartMonth,
                                              int pStartDate, int pEndYear, int pEndMonth, int pEnddate,
                                              int currentYear, int currentMonth) {
        final int startMonth = pStartMonth;
        final int startYear = pStartYear;
        final int startDate = pStartDate;
        final int endMonth = pEndMonth;
        final int endYear = pEndYear;
        final int endDate = pEnddate;

        int NumberOfDays = getNumberOfDays(Integer.valueOf(np_year.getValue()),
                getMonthNumber(np_month.getDisplayedValues()[np_month
                        .getValue() - 1]));

        if (currentYear == endYear && (currentMonth + 1) == endMonth) {
            np_days.setMinValue(1);
            np_days.setMaxValue(endDate);

            int[] new_month = new int[endDate];
            for (int i = 0; i < endDate; i++) {
                new_month[i] = i + 1;
            }
            np_days.initializeSelectorWheelIndices();

        } else if (currentYear == startYear && (currentMonth + 1) == startMonth) {

            Log.e("Months", "" + (12 - startMonth));
            np_days.setMinValue(startDate);
            np_days.setMaxValue(NumberOfDays);
            np_days.initializeSelectorWheelIndices();

        } else {
            np_days.setMinValue(1);
            np_days.setMaxValue(NumberOfDays);

            int[] new_month = new int[NumberOfDays];
            for (int i = 0; i < NumberOfDays; i++) {
                new_month[i] = i + 1;
            }
            np_days.initializeSelectorWheelIndices();
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_logout:

                try {

                    Logout();

                } catch (Exception e) {

                    e.printStackTrace();

                }

                break;
        }
    }

    private void Logout() {

        Constants.ForDialogStyle = "Logout";

        mPoweroffDialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_logout_password, true);

        LinearLayout ll_submit = (LinearLayout) mPoweroffDialog
                .findViewById(R.id.ll_submit);

        ll_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /*Commented By Krishna : 08-05-2019 Update Firebase Token For Notification if Single User*/

                mPoweroffDialog.dismiss();
                try {
                    changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/

            }

        });

        ImageView img_close = (ImageView) mPoweroffDialog.findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mPoweroffDialog.dismiss();

            }

        });

    }

    public void changeGCMID(String UserId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, ""));
        Log.d("requestlogin", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    @Override
    public void response(String result, String methodName) {


        if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            ERPOrataroDatabase.getERPOrataroDatabase(mContext).clearAllTables();
            new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
            new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
            Intent intent = new Intent(mContext, RegisterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            new UserSharedPrefrence(mContext).clearPrefrence();
            SharedPreferences preferences = mContext.getSharedPreferences("LOGIN_USER_DATA", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
            startActivity(intent);
            getActivity().finish();
            getActivity().overridePendingTransition(0, 0);

        }

    }

}
