package com.edusunsoft.erp.orataro.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NoticeListDataDao {

    @Insert
    void insertNoticeList(NoticeListModel noticeListModel);

    @Query("SELECT * from NoticeList")
    List<NoticeListModel> getNoticeList();

    @Query("DELETE  FROM NoticeList")
    int deleteNoticeListItem();

    @Query("delete from NoticeList where NotesID=:notesid")
    void deleteNotesItemByNoticeID(String notesid);

    @Query("UPDATE NoticeList SET IsRead= :isread WHERE NotesID = :noticeid")
    int updateIsReadyByNoticeID(String noticeid, boolean isread);

}
