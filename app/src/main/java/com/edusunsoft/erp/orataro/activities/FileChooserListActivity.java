package com.edusunsoft.erp.orataro.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.FileArrayAdapter;
import com.edusunsoft.erp.orataro.model.Item;

import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileChooserListActivity extends ListActivity {

	private File currentDir;
	private FileArrayAdapter adapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		currentDir = new File("/storage/");
		fill(currentDir); 
	}
	
	/**
	 * get txt doc pdf format file filter and explorer for that
	 * @param f
	 */
	private void fill(File f)
	{
		File[]dirs = f.listFiles();
		this.setTitle("Current Dir: "+f.getName());
		List<Item> dir = new ArrayList<Item>();
		List<Item> fls = new ArrayList<Item>();
		try{
			for(File ff: dirs)
			{ 
				Date lastModDate = new Date(ff.lastModified());
				DateFormat formater = DateFormat.getDateTimeInstance();
				String date_modify = formater.format(lastModDate);
				if(ff.isDirectory()){


					File[] fbuf = ff.listFiles();
					int buf = 0;
					if(fbuf != null){ 
						buf = fbuf.length;
					} 
					else buf = 0; 
					String num_item = String.valueOf(buf);
					if(buf == 0) num_item = num_item + " item";
					else num_item = num_item + " items";

					dir.add(new Item(ff.getName(),num_item,date_modify,ff.getAbsolutePath(),"directory_icon"));

				}
				else
				{
					String[] fileextension=ff.getName().split(".");
					String extension = "";

					int i = ff.getName().lastIndexOf('.');
					if (i > 0) {
					extension = ff.getName().substring(i+1);
					}
					

//						Log.v("extension", fileextension[fileextension.length-1]);
						if(extension.equalsIgnoreCase("txt") ||
								extension.equalsIgnoreCase("docx") ||
								extension.equalsIgnoreCase("doc") ||
								extension.equalsIgnoreCase("pdf") ||
								extension.equalsIgnoreCase("mp3")){
							fls.add(new Item(ff.getName(),ff.length() + " Byte", date_modify, ff.getAbsolutePath(),"file_icon"));

						}

					


					//					if(ff.getName().contains("txt") || ff.getName().contains("TXT") ||
					//							ff.getName().contains("docx") || ff.getName().toLowerCase().contains("doc")   ||  ff.getName().contains("DOC")   ||
					//							ff.getName().contains("PDF")  || ff.getName().contains("pdf") ){
					//						fls.add(new Item(ff.getName(),ff.length() + " Byte", date_modify, ff.getAbsolutePath(),"file_icon"));
					//					}
				}
			}
		}catch(Exception e)
		{    
			Log.v("error", e.getMessage());
		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if(!f.getName().equalsIgnoreCase("storage"))
			dir.add(0,new Item("..","Parent Directory","",f.getParent(),"directory_up"));
		adapter = new FileArrayAdapter(FileChooserListActivity.this, R.layout.file_view,dir);
		this.setListAdapter(adapter); 
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Item o = adapter.getItem(position);
		if(o.getImage().equalsIgnoreCase("directory_icon")||o.getImage().equalsIgnoreCase("directory_up")){

			currentDir = new File(o.getPath());
			if("/storage/emulated".equalsIgnoreCase(o.getPath())){
				if(o.getImage().equalsIgnoreCase("directory_up")){
					currentDir = new File("/storage/");
				}else{
				currentDir = new File("/storage/emulated/0/");
				}
			}
			fill(currentDir);
		}
		else
		{
			onFileClick(o);
		}
	}
/**
 * when click on file	
 * @param
 */
private void onFileClick(Item o)
	{
		//Toast.makeText(this, "Folder Clicked: "+ currentDir, Toast.LENGTH_SHORT).show();
		Intent intent = new Intent();
		intent.putExtra("GetPath",currentDir.toString());
		intent.putExtra("GetFileName",o.getName());
		setResult(RESULT_OK, intent);
		finish();
	}
}
