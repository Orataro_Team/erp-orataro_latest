package com.edusunsoft.erp.orataro.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.databinding.ActivityRouteMapBinding;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.RouteListResModel;
import com.edusunsoft.erp.orataro.model.RouteStudentListModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.BitmapUtil;
import com.edusunsoft.erp.orataro.util.MapUtil;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.Nullable;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.edusunsoft.erp.orataro.util.MapUtil.decodePoly;

public class RouteMapActivity extends FragmentActivity implements OnMapReadyCallback, ResponseWebServices {

    private static final String TAG = "RouteMapActivity";
    private GoogleMap mMap;
    private RouteListResModel.Data data;
    private static String EXTRA_ROUTE = "EXTRA_ROUTE";
    private ActivityRouteMapBinding activityRouteMapBinding;
    private DatabaseReference databaseReference;
    private Marker busLocationMarker;
    private List<LatLng> pathLatLngs;
    private boolean isFakeEventStarted = false;

    Double Firebaselatitude = 0.0, Firebaselongitude = 0.0;
    String TripStatus = "", TripClosedMessage = "";

    private ChildEventListener onChildChange = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Log.d(TAG, "onDataChange: " + dataSnapshot.getValue().toString());
            try {

                if (dataSnapshot.getKey().equalsIgnoreCase("status")) {
                    TripStatus = dataSnapshot.getValue().toString();
                } else if (dataSnapshot.getKey().equalsIgnoreCase("latitude")) {
                    Firebaselatitude = Double.valueOf(dataSnapshot.getValue().toString());
                } else if (dataSnapshot.getKey().equalsIgnoreCase("longitude")) {
                    Firebaselongitude = Double.valueOf(dataSnapshot.getValue().toString());
                }

                if (TripStatus.equalsIgnoreCase("TripClose")) {
                    if (data.getIsForPickup() == 0) {
                        TripClosedMessage = "Your Drop Trip Closed Successfully";
                    } else {
                        TripClosedMessage = "Your Pickup Trip Closed Successfully";
                    }
                    Utility.showAlertDialogForTripClose(RouteMapActivity.this, TripClosedMessage, "Trip Closed");
                } else {
                    moveBusMarker(new LatLng(Firebaselatitude, Firebaselongitude));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private ValueEventListener onChildValueChange = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            Log.d(TAG, "onDataChange: " + dataSnapshot.getValue().toString());
            try {

                if (dataSnapshot.getKey().equalsIgnoreCase("status")) {
                    TripStatus = dataSnapshot.getValue().toString();
                } else if (dataSnapshot.getKey().equalsIgnoreCase("latitude")) {
                    if (Firebaselatitude != null) {
                        Firebaselatitude = 0.0;
                        Firebaselatitude = Double.valueOf(dataSnapshot.getValue().toString());
                    }

                } else if (dataSnapshot.getKey().equalsIgnoreCase("longitude")) {
                    Firebaselongitude = Double.valueOf(dataSnapshot.getValue().toString());
                }
                if (TripStatus.equalsIgnoreCase("TripClose")) {
                    if (data.getIsForPickup() == 0) {
                        TripClosedMessage = "Your Drop Trip Closed Successfully";
                    } else {
                        TripClosedMessage = "Your Pickup Trip Closed Successfully";
                    }
                    Utility.showAlertDialogForTripClose(RouteMapActivity.this, TripClosedMessage, "Trip Closed");
                } else {
                    moveBusMarker(new LatLng(Firebaselatitude, Firebaselongitude));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }

    };

    private GoogleMap.CancelableCallback callback = new GoogleMap.CancelableCallback() {
        @Override
        public void onFinish() {
//            if (!isFakeEventStarted) {
//                fakeMoveBus();
//            }
        }

        @Override
        public void onCancel() {

        }
    };

    private void moveBusMarker(LatLng latLng) {
        if (busLocationMarker == null) {
            MarkerOptions busMarker = new MarkerOptions();
            busMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_map_icon_blue));
            busMarker.title(data.getBusName());
            busMarker.flat(true);
            busMarker.anchor(0.5f, 0.5f);
            busMarker.position(latLng);
            busLocationMarker = mMap.addMarker(busMarker);
        } else {
            //busLocationMarker.setPosition(latLng);
            MapUtil.animateMarker(busLocationMarker, latLng);
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void fakeMoveBus() {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                isFakeEventStarted = true;
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                for (LatLng latLng : pathLatLngs) {
                    if (RouteMapActivity.this.isDestroyed()) {
                        cancel(true);
                    }
                    try {
                        Thread.sleep(250);
                        runOnUiThread(() -> moveBusMarker(latLng));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                isFakeEventStarted = false;
                cancel(true);
                finish();
            }
        }.execute();
    }

    public static Intent getInstance(Activity activity, RouteListResModel.Data data) {
        Intent i = new Intent(activity, RouteMapActivity.class);
        i.putExtra(EXTRA_ROUTE, data);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRouteMapBinding = DataBindingUtil.setContentView(this, R.layout.activity_route_map);

        if (getIntent().getExtras() != null) {
            data = (RouteListResModel.Data) getIntent().getExtras().getSerializable(EXTRA_ROUTE);
            Log.d(TAG, "onCreate: data: " + data);
        } else {
            finish();
        }

        getStudentWiseBusRouteInformation();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        try {
            activityRouteMapBinding.mapToolbar.headerText.setText("Route Map");
            activityRouteMapBinding.mapToolbar.imgHome.setImageResource(R.drawable.back);
            activityRouteMapBinding.mapToolbar.imgHome.setOnClickListener(v -> finish());

            activityRouteMapBinding.fbCenter.setOnClickListener(v -> zoomToPolyline());
            activityRouteMapBinding.fbBack.setOnClickListener(v -> finish());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (data.getBusTripLogID() == null) {
            showPath();
        } else {
            getDataFromFirebase();
        }
    }

    private void getStudentWiseBusRouteInformation() {
        ArrayList<PropertyVo> arrayList = new ArrayList<>();

        arrayList.add(new PropertyVo(ServiceResource.BusRouteID,
                data.getBusRouteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(this).getLoginModel().getClientID()));

        Log.d("routeListRequest", arrayList.toString());

        new AsynsTaskClass(this, arrayList, true, this).execute(ServiceResource.GetStudentWiseBusRouteInformation, ServiceResource.ROUTELIST_URL);
    }

    private void getDataFromFirebase() {
        showPath();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference busIdRef = databaseReference.child(data.getBusTripLogID());
        busIdRef.addListenerForSingleValueEvent(onSingleEventListener(busIdRef));
    }

    private ValueEventListener onSingleEventListener(DatabaseReference busIdRef) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, dataSnapshot.getValue().toString());
                try {

                    TripStatus = dataSnapshot.child("status").getValue(String.class);
                    String latitude = dataSnapshot.child("latitude").getValue(String.class);
                    String longitude = dataSnapshot.child("longitude").getValue(String.class);
                    Firebaselatitude = Double.parseDouble(latitude);
                    Firebaselongitude = Double.parseDouble(longitude);

                    if (TripStatus.equalsIgnoreCase("TripClose")) {
                        if (data.getIsForPickup() == 0) {
                            TripClosedMessage = "Your Drop Trip Closed Successfully";
                        } else {
                            TripClosedMessage = "Your Pickup Trip Closed Successfully";
                        }
                        Utility.showAlertDialogForTripClose(RouteMapActivity.this, TripClosedMessage, "Trip Closed");
                    } else {
                        moveBusMarker(new LatLng(Firebaselatitude, Firebaselongitude));
                    }

                    busIdRef.addChildEventListener(onChildChange);

                    Log.d("getDataall", latitude + " " + longitude + " " + TripStatus);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }

    private void showPath() {

        pathLatLngs = decodePoly(data.getPolyLine());
        PolylineOptions polyLineOptions = new PolylineOptions();
        polyLineOptions.jointType(JointType.ROUND);
        polyLineOptions.startCap(new RoundCap());
        polyLineOptions.endCap(new RoundCap());
        polyLineOptions.addAll(pathLatLngs);
        polyLineOptions.width(8);
        polyLineOptions.color(getResources().getColor(R.color.blue));
        zoomToPolyline();

        mMap.addPolyline(polyLineOptions);

    }

    public void zoomToPolyline() {
        if (mMap == null || pathLatLngs == null || pathLatLngs.isEmpty())
            return;

        LatLngBounds.Builder builder = LatLngBounds.builder();

        for (LatLng latLng : pathLatLngs) {
            builder.include(latLng);
        }

        LatLngBounds bounds = builder.build();

        try {
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.55);
            int padding = (int) (width * 0.08);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            mMap.animateCamera(cu, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void response(String result, String methodName) {
        Log.d(TAG, "response: " + result);
        JSONObject resultJsonObject = new JSONObject();
        try {
            resultJsonObject.put("data", new JSONArray(result));
            RouteStudentListModel routeStudentListModel = new Gson().fromJson(resultJsonObject.toString(), RouteStudentListModel.class);
            int i = 0;
            for (RouteStudentListModel.DataItem dataItem : routeStudentListModel.getData()) {
                i = i + 1;
                MarkerOptions pickupDropPoint = new MarkerOptions();
                if (i == 1) {
                    pickupDropPoint.title(dataItem.getPointName()).snippet("Pickup Point").icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.writeTextOnDrawable(this, R.drawable.marker_light_green, String.valueOf(i))));
                } else if (dataItem.getPointName().equals(data.getPointName())) {
                    pickupDropPoint.title(dataItem.getPointName()).snippet("Your Point").icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.writeTextOnDrawable(this, R.drawable.marker_home, "")));
                } else if (i > 1 && i < routeStudentListModel.getData().size()) {
                    pickupDropPoint.title(dataItem.getPointName()).snippet("Pickup Point").icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.writeTextOnDrawable(this, R.drawable.marker_grey, String.valueOf(i))));
                } else {
                    pickupDropPoint.title(dataItem.getPointName()).snippet("Drop Point").icon(BitmapDescriptorFactory.fromBitmap(BitmapUtil.writeTextOnDrawable(this, R.drawable.marker_red, String.valueOf(i))));
                }

                pickupDropPoint.position(new LatLng(dataItem.getLatitude(), dataItem.getLongitude()));
                mMap.addMarker(pickupDropPoint);

            }

        } catch (JSONException e) {
            // TODO: 2/1/20 Show error dialog for retry
            e.printStackTrace();
        }

    }

}
