package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.StudentModel;

import java.util.ArrayList;
import java.util.Locale;

public class StudentListGroupAdapter extends BaseAdapter {
	
	private Context mContext;
	private LayoutInflater layoutInfalater;
	private TextView tv_class, tv_student_name;
	private LinearLayout ll_view;
	private ArrayList<StudentModel> list = new ArrayList<>();
	private ArrayList<StudentModel> copyList = new ArrayList<>();
	
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	public StudentListGroupAdapter(Context mContext, ArrayList<StudentModel> list) {
		this.mContext = mContext;
		this.list = list;
		copyList.addAll(list);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.students_list_items, parent, false);
		ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
		convertView.setBackgroundColor(colors[position % colors.length]);
		tv_class = (TextView) convertView.findViewById(R.id.tv_class);
		tv_student_name = (TextView) convertView.findViewById(R.id.tv_student_name);
		if(list.get(position).getStudentId()!= null){
			tv_class.setText(list.get(position).getStudentId());
		}
		if(list.get(position).getStudentName() != null){
			tv_student_name.setText(list.get(position).getStudentName());
		}

		return convertView;

	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		list.clear();
		if (charText.length() == 0) {
			list.addAll(copyList);
		} else {
			for (StudentModel vo : copyList) {
				if (vo.getStudentName().toLowerCase(Locale.getDefault()).contains(charText)) {
					list.add(vo);
				}
			}
		}

		this.notifyDataSetChanged();
	}
	
}
