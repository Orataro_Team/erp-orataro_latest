package com.edusunsoft.erp.orataro.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "DashboardMenu")
public class DashboardMenuModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "MenuName")
    @SerializedName("MenuName")
    public String MenuName;

    @ColumnInfo(name = "MenuIconName")
    @SerializedName("MenuIconName")
    public String MenuIconName;

    @ColumnInfo(name = "DefaultValue")
    @SerializedName("DefaultValue")
    public String DefaultValue;

    @ColumnInfo(name = "IsView")
    @SerializedName("IsView")
    public boolean IsView;

    @ColumnInfo(name = "isStatic")
    @SerializedName("isStatic")
    public boolean isStatic;

    public DashboardMenuModel() {
    }

    public DashboardMenuModel(String menuName, String menuIconName, boolean isView, boolean isStatic) {
        this.MenuName = menuName;
        this.MenuIconName = menuIconName;
        this.IsView = isView;
        this.isStatic = isStatic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public String getMenuIconName() {
        return MenuIconName;
    }

    public void setMenuIconName(String menuIconName) {
        MenuIconName = menuIconName;
    }

    public String getDefaultValue() {
        return DefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        DefaultValue = defaultValue;
    }

    public boolean isView() {
        return IsView;
    }

    public void setView(boolean view) {
        IsView = view;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    @Override
    public String toString() {
        return "DashboardMenuPermissionModel{" +
                "id=" + id +
                ", MenuName='" + MenuName + '\'' +
                ", MenuIconName='" + MenuIconName + '\'' +
                ", DefaultValue='" + DefaultValue + '\'' +
                ", IsView=" + IsView +
                ", isStatic=" + isStatic +
                '}';
    }
}
