package com.edusunsoft.erp.orataro.util;

import com.edusunsoft.erp.orataro.database.BlogListModel;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingModel;
import com.edusunsoft.erp.orataro.database.GroupListModel;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.database.HomeWorkListModel;
import com.edusunsoft.erp.orataro.database.ProjectListModel;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.CalendarEventModel;
import com.edusunsoft.erp.orataro.model.CalenderEventModel;
import com.edusunsoft.erp.orataro.model.CircularModel;
import com.edusunsoft.erp.orataro.model.ClassWorkModel;
import com.edusunsoft.erp.orataro.model.DivisionModel;
import com.edusunsoft.erp.orataro.model.DynamicWallModel;
import com.edusunsoft.erp.orataro.model.ExamTimingModel;
import com.edusunsoft.erp.orataro.model.FriendListModel;
import com.edusunsoft.erp.orataro.model.FriendRequestModel;
import com.edusunsoft.erp.orataro.model.FriendSearchModel;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LeaveListModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.edusunsoft.erp.orataro.model.MemberModel;
import com.edusunsoft.erp.orataro.model.MenuPermissionModel;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.model.MyPhoneChainModel;
import com.edusunsoft.erp.orataro.model.NotesModel;
import com.edusunsoft.erp.orataro.model.NotificationListModel;
import com.edusunsoft.erp.orataro.model.ParentsProfileModel;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PollModel;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.model.SubjectModel;
import com.edusunsoft.erp.orataro.model.TodoListModel;
import com.edusunsoft.erp.orataro.model.UserModel;
import com.edusunsoft.erp.orataro.model.WallCommentModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;

import java.util.ArrayList;

public class Global {

    public static LoginModel userdataModel = null;
    public static LoginUserModel loginuserModel = null;
    public static UserModel loginuserdataModel = null;
    public static HolidaysModel holidaysModel = null;

    public static ArrayList<LoginModel> userdataList = null;
    public static ArrayList<LoginUserModel> loginuserList = new ArrayList<>();
    public static ArrayList<LoginModel> LoginUserdataList = null;
    public static ArrayList<CalenderEventModel> calenderdDataList = null;
    public static ArrayList<CalendarEventModel> calenderdEventList = null;

    public static ArrayList<HolidaysModel> holidaysModels = null;
    public static ArrayList<ProjectListModel> projectsModels = null;
    public static ArrayList<PollModel> pollModels = null;
    public static ArrayList<ClassWorkModel> classWorkModels = null;
    public static ArrayList<CircularModel> circularkModels = null;
    public static ArrayList<HomeWorkModel> homeworkModels = null;
    public static ArrayList<HomeWorkListModel> homeworklistModels = null;
    public static ArrayList<NotesModel> notesModels = null;
    public static ArrayList<PhotoModel> AlbumModels = null;
    public static ArrayList<GroupListModel> groupModels = null;
    public static ArrayList<StandardModel> StandardModels = null;
    // chnage for offline database
//    public static ArrayList<StdDivSubModel> StandardModels = null;
    public static ArrayList<DivisionModel> divisionModels = null;
    public static ArrayList<SubjectModel> subjectModels = null;
    public static ArrayList<MyHappyGramModel> myHappyGramModels = null;
    public static ArrayList<PersonModel> FriendsList = null;
    public static MyHappyGramModel myHappyGramModel = null;
    public static ArrayList<BlogListModel> cmsPageListModels = null;
    public static ArrayList<MyPhoneChainModel> phonebookModels = null;
    public static ArrayList<ParentsProfileModel> fatherProfile = null;
    public static ArrayList<ParentsProfileModel> motherProfile = null;
    public static ArrayList<ParentsProfileModel> gardianProfile = null;
    public static ArrayList<com.edusunsoft.erp.orataro.model.PostCommentModel> PostCommentModel = null;
    public static ArrayList<WallPostModel> wallPostModels = new ArrayList<>();
    public static ArrayList<WallCommentModel> wallCommentModels = null;
    public static ArrayList<DynamicWallModel> DynamicWallModels = null;
    public static ArrayList<FriendListModel> friendListModels = null;
    public static ArrayList<FriendRequestModel> friendRequestModels = null;
    public static ArrayList<FriendSearchModel> searchFriendModels = null;
    public static ArrayList<NotificationListModel> notificationListModels = null;
    public static ArrayList<TodoListModel> todolist = null;
    public static ArrayList<ProjectListModel> projectlist = null;
    public static ArrayList<ReadWriteSettingModel> readWriteSettingList = null;
    public static ArrayList<MyHappyGramModel> myHappygramViewStudentList = null;
    // public static com.edusunsoft.erp.orataro.model.DynamicWallSetting DynamicWallSetting = null;
    public static DynamicWallSettingModel DynamicWallSetting = null;
    public static com.edusunsoft.erp.orataro.model.StudentProfileModel StudentProfileModel = null;
    public static ArrayList<MemberModel> ptTeacherlistmodels = null;
    public static ArrayList<AtteandanceModel> attendancemodels = null;
    public static ArrayList<AtteandanceModel> editAttendance = null;
    public static ArrayList<LeaveListModel> leaveListModels = null;

    public static ArrayList<MenuPermissionModel> menuPermissionList = new ArrayList<>();

    public static ArrayList<ExamTimingModel> examtimingList = new ArrayList<>();

}
