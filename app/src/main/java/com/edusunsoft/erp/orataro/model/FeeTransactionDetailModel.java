package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class FeeTransactionDetailModel implements Serializable {

    String PaymentTransactionDetailsID = "", TransactionFrom = "", TransactionDate = "", TransactionStatus = "", PaymentGetWayTranID = "", BookID = "", StructureDisplayName = "", UserName = "", ReceiptNo = "",strTransactionDate = "";

    double Amount = 0.0;

    public String getStrTransactionDate() {
        return strTransactionDate;
    }

    public void setStrTransactionDate(String strTransactionDate) {
        this.strTransactionDate = strTransactionDate;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getPaymentTransactionDetailsID() {
        return PaymentTransactionDetailsID;
    }

    public void setPaymentTransactionDetailsID(String paymentTransactionDetailsID) {
        PaymentTransactionDetailsID = paymentTransactionDetailsID;
    }

    public String getTransactionFrom() {
        return TransactionFrom;
    }

    public void setTransactionFrom(String transactionFrom) {
        TransactionFrom = transactionFrom;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return TransactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        TransactionStatus = transactionStatus;
    }

    public String getPaymentGetWayTranID() {
        return PaymentGetWayTranID;
    }

    public void setPaymentGetWayTranID(String paymentGetWayTranID) {
        PaymentGetWayTranID = paymentGetWayTranID;
    }

    public String getBookID() {
        return BookID;
    }

    public void setBookID(String bookID) {
        BookID = bookID;
    }

    public String getStructureDisplayName() {
        return StructureDisplayName;
    }

    public void setStructureDisplayName(String structureDisplayName) {
        StructureDisplayName = structureDisplayName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getReceiptNo() {
        return ReceiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        ReceiptNo = receiptNo;
    }

    @Override
    public String toString() {
        return "FeeTransactionDetailModel{" +
                "PaymentTransactionDetailsID='" + PaymentTransactionDetailsID + '\'' +
                ", TransactionFrom='" + TransactionFrom + '\'' +
                ", TransactionDate='" + TransactionDate + '\'' +
                ", TransactionStatus='" + TransactionStatus + '\'' +
                ", PaymentGetWayTranID='" + PaymentGetWayTranID + '\'' +
                ", BookID='" + BookID + '\'' +
                ", StructureDisplayName='" + StructureDisplayName + '\'' +
                ", UserName='" + UserName + '\'' +
                ", ReceiptNo='" + ReceiptNo + '\'' +
                ", strTransactionDate='" + strTransactionDate + '\'' +
                ", Amount=" + Amount +
                '}';
    }
}
