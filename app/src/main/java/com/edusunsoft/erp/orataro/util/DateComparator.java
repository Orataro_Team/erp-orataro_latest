package com.edusunsoft.erp.orataro.util;

import com.edusunsoft.erp.orataro.model.CalenderEventModel;

import java.util.Comparator;

public class DateComparator implements Comparator<CalenderEventModel> {

		@Override
		public int compare(CalenderEventModel lhs, CalenderEventModel rhs) {
			return (new Utility().dateToMilli(lhs.getStart()) < new Utility().dateToMilli(rhs.getStart())) ? -1: (new Utility().dateToMilli(lhs.getStart()) > new Utility().dateToMilli(rhs.getStart())) ? 1:0 ;
		}
    }
	
	

