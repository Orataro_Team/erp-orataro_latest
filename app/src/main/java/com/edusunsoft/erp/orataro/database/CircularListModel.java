package com.edusunsoft.erp.orataro.database;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "CircularList")
public class CircularListModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    public int id;

    @ColumnInfo(name = "cr_date")
    @SerializedName("cr_date")
    public String cr_date;

    @ColumnInfo(name = "imgUrl")
    @SerializedName("imgUrl")
    public String imgUrl;

    @ColumnInfo(name = "cr_subject")
    @SerializedName("cr_subject")
    public String cr_subject;

    @ColumnInfo(name = "cr_detail")
    @SerializedName("cr_detail")
    public int cr_detail;

    @ColumnInfo(name = "year")
    @SerializedName("year")
    public String year;

    @ColumnInfo(name = "milliSecond")
    @SerializedName("milliSecond")
    public String milliSecond;

    @ColumnInfo(name = "SubjectID")
    @SerializedName("SubjectID")
    public String SubjectID;

    @ColumnInfo(name = "DivisionID")
    @SerializedName("DivisionID")
    public String DivisionID;

    @ColumnInfo(name = "GradeID")
    @SerializedName("GradeID")
    public String GradeID;

    @ColumnInfo(name = "CircularID")
    @SerializedName("CircularID")
    public String CircularID;

    @ColumnInfo(name = "typeTerm")
    @SerializedName("typeTerm")
    public String typeTerm;

    @ColumnInfo(name = "isAccept")
    @SerializedName("isAccept")
    public boolean isAccept;

    @ColumnInfo(name = "cr_detail_txt")
    @SerializedName("cr_detail_txt")
    public String cr_detail_txt;

    @ColumnInfo(name = "teacher_name")
    @SerializedName("teacher_name")
    public String teacher_name;

    @ColumnInfo(name = "teacher_img")
    @SerializedName("teacher_img")
    public String teacher_img;

    @ColumnInfo(name = "isVisibleMonth")
    @SerializedName("isVisibleMonth")
    public boolean isVisibleMonth;

    @ColumnInfo(name = "cr_month")
    @SerializedName("cr_month")
    public String cr_month;

    @ColumnInfo(name = "ReferenceLink")
    @SerializedName("ReferenceLink")
    public String ReferenceLink;

    @ColumnInfo(name = "str_dateofcircular")
    @SerializedName("str_dateofcircular")
    public String str_dateofcircular;

    @ColumnInfo(name = "IsRead")
    @SerializedName("IsRead")
    public boolean IsRead;


    public CircularListModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCr_date() {
        return cr_date;
    }

    public void setCr_date(String cr_date) {
        this.cr_date = cr_date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCr_subject() {
        return cr_subject;
    }

    public void setCr_subject(String cr_subject) {
        this.cr_subject = cr_subject;
    }

    public int getCr_detail() {
        return cr_detail;
    }

    public void setCr_detail(int cr_detail) {
        this.cr_detail = cr_detail;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMilliSecond() {
        return milliSecond;
    }

    public void setMilliSecond(String milliSecond) {
        this.milliSecond = milliSecond;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getDivisionID() {
        return DivisionID;
    }

    public void setDivisionID(String divisionID) {
        DivisionID = divisionID;
    }

    public String getGradeID() {
        return GradeID;
    }

    public void setGradeID(String gradeID) {
        GradeID = gradeID;
    }

    public String getCircularID() {
        return CircularID;
    }

    public void setCircularID(String circularID) {
        CircularID = circularID;
    }

    public String getTypeTerm() {
        return typeTerm;
    }

    public void setTypeTerm(String typeTerm) {
        this.typeTerm = typeTerm;
    }

    public boolean isAccept() {
        return isAccept;
    }

    public void setAccept(boolean accept) {
        isAccept = accept;
    }

    public String getCr_detail_txt() {
        return cr_detail_txt;
    }

    public void setCr_detail_txt(String cr_detail_txt) {
        this.cr_detail_txt = cr_detail_txt;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_img() {
        return teacher_img;
    }

    public void setTeacher_img(String teacher_img) {
        this.teacher_img = teacher_img;
    }

    public boolean isVisibleMonth() {
        return isVisibleMonth;
    }

    public void setVisibleMonth(boolean visibleMonth) {
        isVisibleMonth = visibleMonth;
    }

    public String getCr_month() {
        return cr_month;
    }

    public void setCr_month(String cr_month) {
        this.cr_month = cr_month;
    }

    public String getReferenceLink() {
        return ReferenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        ReferenceLink = referenceLink;
    }

    public String getStr_dateofcircular() {
        return str_dateofcircular;
    }

    public void setStr_dateofcircular(String str_dateofcircular) {
        this.str_dateofcircular = str_dateofcircular;
    }

    public boolean isRead() {
        return IsRead;
    }

    public void setRead(boolean read) {
        IsRead = read;
    }

    @Override
    public String toString() {
        return "CircularListModel{" +
                "id=" + id +
                ", cr_date='" + cr_date + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", cr_subject='" + cr_subject + '\'' +
                ", cr_detail='" + cr_detail + '\'' +
                ", year='" + year + '\'' +
                ", milliSecond='" + milliSecond + '\'' +
                ", SubjectID='" + SubjectID + '\'' +
                ", DivisionID='" + DivisionID + '\'' +
                ", GradeID='" + GradeID + '\'' +
                ", CircularID='" + CircularID + '\'' +
                ", typeTerm='" + typeTerm + '\'' +
                ", isAccept=" + isAccept +
                ", cr_detail_txt='" + cr_detail_txt + '\'' +
                ", teacher_name='" + teacher_name + '\'' +
                ", teacher_img='" + teacher_img + '\'' +
                ", isVisibleMonth=" + isVisibleMonth +
                ", cr_month='" + cr_month + '\'' +
                ", ReferenceLink='" + ReferenceLink + '\'' +
                ", str_dateofcircular='" + str_dateofcircular + '\'' +
                ", IsRead=" + IsRead +
                '}';
    }
}
