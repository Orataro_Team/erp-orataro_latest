

package com.edusunsoft.erp.orataro.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeesPaymentType implements Parcelable {

    @SerializedName("AcctID")
    @Expose
    private String acctID;
    @SerializedName("AcctName")
    @Expose
    private String acctName;
    @SerializedName("AcctCode")
    @Expose
    private String acctCode;
    @SerializedName("CurrentBalance")
    @Expose
    private String currentBalance;
    @SerializedName("IsMOPAccount")
    @Expose
    private String isMOPAccount;
    @SerializedName("IsPaidOut")
    @Expose
    private String isPaidOut;
    @SerializedName("Charges")
    @Expose
    private String charges;

    public final static Parcelable.Creator<FeesPaymentType> CREATOR = new Creator<FeesPaymentType>() {
        public FeesPaymentType createFromParcel(Parcel in) {
            FeesPaymentType instance = new FeesPaymentType();
            instance.acctID = ((String) in.readValue((String.class.getClassLoader())));
            instance.acctName = ((String) in.readValue((String.class.getClassLoader())));
            instance.acctCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.currentBalance = ((String) in.readValue((String.class.getClassLoader())));
            instance.isMOPAccount = ((String) in.readValue((String.class.getClassLoader())));
            instance.isPaidOut = ((String) in.readValue((String.class.getClassLoader())));
            instance.charges = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public FeesPaymentType[] newArray(int size) {
            return (new FeesPaymentType[size]);
        }

    };

    public String getAcctID() {
        return acctID;
    }

    public void setAcctID(String acctID) {
        this.acctID = acctID;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctCode() {
        return acctCode;
    }

    public void setAcctCode(String acctCode) {
        this.acctCode = acctCode;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getIsMOPAccount() {
        return isMOPAccount;
    }

    public void setIsMOPAccount(String isMOPAccount) {
        this.isMOPAccount = isMOPAccount;
    }

    public String getIsPaidOut() {
        return isPaidOut;
    }

    public void setIsPaidOut(String isPaidOut) {
        this.isPaidOut = isPaidOut;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(acctID);
        dest.writeValue(acctName);
        dest.writeValue(acctCode);
        dest.writeValue(currentBalance);
        dest.writeValue(isMOPAccount);
        dest.writeValue(isPaidOut);
        dest.writeValue(charges);
    }

    public int describeContents() {
        return 0;
    }

}