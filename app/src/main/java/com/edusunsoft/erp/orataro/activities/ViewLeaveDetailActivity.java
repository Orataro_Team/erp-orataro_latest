package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.GroupMemberModel;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LeaveListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class ViewLeaveDetailActivity extends Activity implements OnClickListener, ResponseWebServices, OnItemSelectedListener {


    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader;
    private Context mContext;

    TextView txtname, txtdetails, txtapplicantby, txtApprovedOn, txtleavedate, txtstartdate, txtenddate, txtteachername;
    Button btnSave;
    CheckBox chkpreapplicant;
    Spinner spnStatus;
    private LeaveListModel model;
    private ArrayList<GroupMemberModel> groupMemberList;
    private int spnPosition;
    ArrayList<String> statusList;
    StdDivSubModel stdandardModel;
    EditText edtNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_details);
        mContext = ViewLeaveDetailActivity.this;
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);

        model = (LeaveListModel) getIntent().getSerializableExtra("model");
        stdandardModel = (StdDivSubModel) getIntent().getSerializableExtra("stdmodel");

        txtname = (TextView) findViewById(R.id.txtname);
        txtdetails = (TextView) findViewById(R.id.txtdetails);
        txtapplicantby = (TextView) findViewById(R.id.txtapplicantby);
        txtApprovedOn = (TextView) findViewById(R.id.txtApprovedOn);
        txtstartdate = (TextView) findViewById(R.id.txtstartdate);
        txtenddate = (TextView) findViewById(R.id.txtenddate);
        txtteachername = (TextView) findViewById(R.id.txtteachername);
        btnSave = (Button) findViewById(R.id.btn_save);
        chkpreapplicant = (CheckBox) findViewById(R.id.chkpreapplication);
        spnStatus = (Spinner) findViewById(R.id.spn_status);
        txtleavedate = (TextView) findViewById(R.id.txtleavedate);
        edtNote = (EditText) findViewById(R.id.edtnote);


        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);

        try {
            txtHeader.setText(getResources().getString(R.string.leave) + "(" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
        }

        imgLeftheader.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        txtdetails.setText(model.getReasonForLeave());


        try {

            String sDate = model.getStartDate().replace("/Date(", "").replace(")/", "");
            txtstartdate.setText(Utility.getDate(Long.valueOf(sDate), "dd-MM-yyyy"));

        } catch (Exception e) {

            txtstartdate.setText("");

        }

        try {
            String eDate = model.getEndDate().replace("/Date(", "").replace(")/", "");
            txtenddate.setText(Utility.getDate(Long.valueOf(eDate), "dd-MM-yyyy"));
        } catch (Exception e) {
            txtenddate.setText("");
        }
        chkpreapplicant.setChecked(model.getIsPerApplication());
        txtapplicantby.setText(model.getApplicationBY());
        edtNote.setText(model.getTeacherComment());
        try {
            String approveondate = model.getApprovedOn().replace("/Date(", "").replace(")/", "");
            txtApprovedOn.setText(Utility.getDate(Long.valueOf(approveondate), "dd-MM-yyyy"));
        } catch (Exception e) {
            txtApprovedOn.setText("");
        }
        txtname.setText(model.getStudentName());
        try {
            String tempdate = model.getDateOfApplication().replace("/Date(", "").replace(")/", "");
            txtleavedate.setText(Utility.getDate(Long.valueOf(tempdate), "dd-MM-yyyy"));
        } catch (Exception e) {
            txtleavedate.setText("");
        }

        Calendar c = Calendar.getInstance();

        Log.d("gettartDate", model.getStartDate());
        Log.d("gettartDate2", String.valueOf(c.getTimeInMillis()));

        /*Commented By Krishna : 29-04-2019 Remove Condition for Leave Approval*/

//        if (Long.valueOf(model.getEndDate().replace("/Date(", "").replace(")/", "")) < c.getTimeInMillis()) {
//
//            btnSave.setVisibility(View.GONE);
//
//        }


        /*END*/


        getWallAllTeacher();
        spnStatus.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_home) {
            Utility.ISLOADLEAVELIST = false;
            finish();
        } else if (v.getId() == R.id.btn_save) {
            ChangeLeaveStatus();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADLEAVELIST = false;
        finish();
    }

    public void getWallAllTeacher() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME,
                ServiceResource.FRIENDS_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)
        ) {

            try {

                //				JSONArray hJsonArray = new JSONArray(examresult);
                //				JSONObject jsonObj = new JSONObject(examresult);
                JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
                groupMemberList = new ArrayList<GroupMemberModel>();
                GroupMemberModel model;


                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    model = new GroupMemberModel();

                    model.setMemberId(hJsonObject
                            .getString(ServiceResource.MEMBERID));
                    //					model.setFriendID(hJsonObject
                    //							.getString(ServiceResource.FRIENDLISTID));

                    model.setMemberName(hJsonObject
                            .getString(ServiceResource.FULLNAMEPARAM));

                    groupMemberList.add(model);

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
            getProjectType();

            teacherNameFromId(model.getApplicationToTeacherID());
            txtteachername.setText(groupMemberList.get(spnPosition).getMemberName());

            statusList = new ArrayList<String>();
            statusList.add("Approved");
            statusList.add("Reject");
            statusList.add("Pending");


            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item,
                    statusList);

            // Drop down layout style - list view with radio button
            dataAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //			// attaching data adapter to spinner
            spnStatus.setAdapter(dataAdapter);
            //			mySpinner.getSelectedItem().toString();

            if (model.getApplicationStatus_Term().equalsIgnoreCase("Approved")) {

                btnSave.setVisibility(View.GONE);

            } else if (model.getApplicationStatus_Term().equalsIgnoreCase("Reject")) {

                btnSave.setVisibility(View.GONE);
            }


            for (int i = 0; i < statusList.size(); i++) {

                if (model.getApplicationStatus_Term().equalsIgnoreCase(statusList.get(i))) {

                    spnPosition = i;

                }

            }

            spnStatus.setSelection(spnPosition);


        } else if (ServiceResource.CHANGELEAVESTATUS_METHODNAME.equalsIgnoreCase(methodName)) {
            sendPushNotification(mContext);
        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {
//			finish();type name = new type(arguments);
            notificationCount();

        } else if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONObject obj;
            JSONArray jsonObj;
            JSONArray jsonObjfriend;
            JSONArray jsonObjleave;

            try {

                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                obj = new JSONObject(result);
                jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
                jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);
                jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);


                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));

                }

                for (int i = 0; i < jsonObjfriend.length(); i++) {
                    JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));

                }
                for (int i = 0; i < jsonObjleave.length(); i++) {
                    JSONObject innerObj = jsonObjleave.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Intent i = new Intent();
            setResult(1, i);
            finish();
        }


    }


    public void sendPushNotification(Context mContext) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);
    }

    public void getProjectType() {
        //		fsdfsd
        Utility.getUserModelData(mContext);
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.CATEGORY,
                ""));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.LOGIN_GETPROJECTTYPE,
                ServiceResource.LOGIN_URL);
    }

    public String teacherNameFromId(String id) {
        String teacherName = "";
        if (groupMemberList != null && groupMemberList.size() > 0) {
            for (int i = 0; i < groupMemberList.size(); i++) {
                if (groupMemberList.get(i).getMemberId().equalsIgnoreCase(id)) {
                    teacherName = groupMemberList.get(i).getMemberName();
                    spnPosition = i;
                }
            }
        }
        return teacherName;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }


    public void ChangeLeaveStatus() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.StudentLeaveNoteID, model.getSchoolLeaveNoteID()));

        arrayList.add(new PropertyVo(ServiceResource.status,
                spnStatus.getSelectedItem().toString()));
        arrayList.add(new PropertyVo(ServiceResource.GradeID,
                stdandardModel.getStandrdId()));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID,
                stdandardModel.getDivisionId()));
        arrayList.add(new PropertyVo(ServiceResource.Note,
                edtNote.getText().toString()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.CHANGELEAVESTATUS_METHODNAME,
                ServiceResource.LEAVE_URL);
    }

    public void notificationCount() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));


        //		examresult = webcall.getJSONFromSOAPWS(
        //				ServiceResource.NOTIFICATIONCOUNT_METHODNAME, map,
        //				ServiceResource.NOTIFICATION_URL);

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.NOTIFICATIONCOUNT_METHODNAME,
                ServiceResource.NOTIFICATION_URL);

    }
}
