package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.customeview.DeviceHardWareId;
import com.edusunsoft.erp.orataro.model.HomeWorkModel;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.ReadWriteSettingModel;
import com.edusunsoft.erp.orataro.parse.LoginParse;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * verify opt register
 * verify opt forgotpassword ,new pasword after otp forgot password
 *
 * @author admin
 */
public class ContinueForRegisterActivity extends Activity implements OnClickListener, ResponseWebServices, TextWatcher {

    private FrameLayout fl_back;
    private Context mContext;
    public static EditText edt_optnumber;
    private TextView txt_title;
    private static ContinueForRegisterActivity inst;
    private String userNameStr = "";
    private String regId = "";
    private boolean isLogin = false;
    private boolean isMoreChild;
    private Button btn_continue;
    private String mobileNumber = "";

    public static ContinueForRegisterActivity instance() {

        return inst;

    }

    @Override
    public void onStart() {

        super.onStart();
        inst = this;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_continue);
        mContext = ContinueForRegisterActivity.this;

        btn_continue = findViewById(R.id.btn_continue);
        txt_title = (TextView) findViewById(R.id.txt_title);
        edt_optnumber = (EditText) findViewById(R.id.edt_optnumber);
        edt_optnumber.addTextChangedListener(this);

        fl_back = (FrameLayout) findViewById(R.id.fl_back);

        fl_back.setOnClickListener(this);
        btn_continue.setOnClickListener(this);

        if (getIntent().getExtras() != null) {

            mobileNumber = getIntent().getExtras().getString(ServiceResource.EXTRA_MOBILE);

        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fl_back:

                Intent registerIntent = new Intent(mContext, RegisterActivity.class);
                registerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                registerIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(registerIntent);
                finish();
                overridePendingTransition(0, 0);

                break;

            case R.id.btn_continue:

                if (Utility.isNetworkAvailable(mContext)) {

                    if (edt_optnumber.getText().toString().trim().length() == 0) {

                        Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseEnterotp), "Error");
                    }

                    if (mobileNumber != null && mobileNumber.length() == 0) {

                        Utility.showAlertDialog(mContext, getResources().getString(R.string.Pleasemobilenumber), "Error");

                    } else {

                        verifyOtp();

                    }

                } else {

                    Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                }

                break;

            default:

                break;
        }

    }


    public void updateList(final String smsMessage) {
        edt_optnumber.setText(smsMessage);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void verifyOtp() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MOBILENUMBER, mobileNumber));
        arrayList.add(new PropertyVo(ServiceResource.OTPNumber, edt_optnumber.getText().toString()));
        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN, ServiceResource.REGISTATION_URL);
    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.USER_REGISTRATION_AND_LOGIN_BY_OTP_LOGIN.equalsIgnoreCase(methodName)) {

            JSONArray jsonObj;

            try {

                Global.userdataModel = new LoginModel();

                if (result.contains("\"success\":0")) {

                    isLogin = false;

                } else {

                    PreferenceData.setLogin(true);

                    Utility.writeToFile(result, methodName, mContext);
                    new UserSharedPrefrence(mContext).setLOGIN_USERNAME(userNameStr);

                    jsonObj = new JSONArray(result);

                    Global.userdataModel.setLoginJson(result);

                    PreferenceData.saveLoginUserData(result);

                    Log.d("getData", PreferenceData.getLoginUserData().toString());
                    Log.d("result123", result);

                    if (jsonObj.length() == 1) {

                        for (int i = 0; i < jsonObj.length(); i++) {

                            JSONObject innerObj = jsonObj.getJSONObject(i);
                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            Global.userdataModel.setLoginJson(result);

                            PreferenceData.saveLoginUserData(result);
                            Log.d("getData", PreferenceData.getLoginUserData().toString());
                            Log.d("result123", result);

                            isLogin = true;
                            isMoreChild = false;

                        }

                    } else {

                        int count = 0;
                        int posJson = 0;

                        for (int i = 0; i < jsonObj.length(); i++) {

                            Log.d("jsonarraysize", String.valueOf(jsonObj.length()));

                            JSONObject innerObj = jsonObj.getJSONObject(i);
                            if (!(ServiceResource.GURUKULINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                                if (ServiceResource.SINGLE_DEVICE_FLAG) {
                                    if (!(ServiceResource.STATICINSTITUTEID.indexOf(innerObj.getString(ServiceResource.LOGIN_INSTITUTEID)) >= 0)) {
                                        if (innerObj.getString(ServiceResource.DEVICEIDENTITY).equalsIgnoreCase(DeviceHardWareId.getDviceHardWarId(mContext))) {
                                            count++;
                                            posJson = i;
                                        }

                                    } else {

                                        count++;
                                        posJson = i;
                                    }

                                } else {

                                    count++;
                                    posJson = i;

                                }

                            }

                        }

                        if (count == 1) {

                            JSONObject innerObj = jsonObj.getJSONObject(posJson);
                            Global.userdataModel = new LoginModel();
                            Global.userdataModel = LoginParse.PaserLoginModelFromJSONObject(innerObj, result);
                            Global.userdataModel.setLoginJson(result);

                            PreferenceData.saveLoginUserData(result);
                            Log.d("getData", PreferenceData.getLoginUserData().toString());
                            Log.d("getData123", result);

                            isLogin = true;
                            isMoreChild = false;

                        } else if (count > 1) {

                            new UserSharedPrefrence(mContext).setISMULTIPLE(true);
                            isMoreChild = true;
                            isLogin = true;


                        }

                    }

                }

            } catch (JSONException e) {

                isLogin = false;
                e.printStackTrace();

            }


            if (isLogin == true) {

                Log.d("islogin", "true");
                changeGCMID(new UserSharedPrefrence(mContext).getLoginModel().getMobileNumber());

            } else {

                Log.d("islogin", "false");
                Utility.showToast(getResources().getString(R.string.InvalidMobileNumberOrPassword), mContext);

            }

        } else if (ServiceResource.CHANGEGCMID_METHODNAME.equalsIgnoreCase(methodName)) {

            if (isMoreChild) {

                Global.userdataModel.setSave(true);
//                Global.loginuserdataModel.setSave(true);

                Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
                /*commented By Krishna - 23-02-2019 Remove Condition for ISERP*/

                intentHomeWork.putExtra("position", getResources().getString(R.string.SwitchAccount));


                /*END*/

//                if (new UserSharedPrefrence(mContext).getIsERP()) {
//                    intentHomeWork.putExtra("position", getResources().getString(R.string.SwitchAccount));
//                } else {
//                    intentHomeWork.putExtra("position", getResources().getString(R.string.SwitchAccount));
//                }
                intentHomeWork.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(intentHomeWork);
                finish();
                overridePendingTransition(0, 0);

            } else {

                GetUserRoleRightList();

//                isERP();

            }

        }

//        else if (ServiceResource.CHKINSTITUTEORUSEREXISTINERP_METHODNAME.equalsIgnoreCase(methodName)) {
//
//            if (examresult.equalsIgnoreCase("isavailable")) {
//                new UserSharedPrefrence(mContext).setIsERP(true);
//            } else {
//                new UserSharedPrefrence(mContext).setIsERP(false);
//            }
//
//            GetUserRoleRightList();
//
//        }
//
        else if (ServiceResource.GETUSERROLERIGHTLIST_METHODNAME.equalsIgnoreCase(methodName)) {
            try {
                Utility.writeToFile(result, methodName, mContext);
                JSONArray jsonObj = new JSONArray(result);
                new UserSharedPrefrence(mContext).setREADWRITESETTING(result);
                Global.readWriteSettingList = new ArrayList<ReadWriteSettingModel>();
                ReadWriteSettingModel model;
                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    model = new ReadWriteSettingModel();
                    model.setRightID(innerObj.getString(ServiceResource.RIGHTID));
                    model.setRightName(innerObj.getString(ServiceResource.RIGHTNAME));
                    if (!Global.userdataModel.getRoleName().equalsIgnoreCase("teacher")) {
                        model.setIsView(innerObj.getString(ServiceResource.ISVIEWSETTING));
                        model.setIsEdit(innerObj.getString(ServiceResource.ISEDIT));
                        model.setIsCreate(innerObj.getString(ServiceResource.ISCREATE));
                        model.setIsDelete(innerObj.getString(ServiceResource.ISDELETE));
                    } else {
                        model.setIsView("true");
                        model.setIsEdit("true");
                        model.setIsCreate("true");
                        model.setIsDelete("true");
                    }
                    Global.readWriteSettingList.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (isMoreChild) {

                Global.userdataModel.setSave(true);
//                Global.loginuserdataModel.setSave(true);

                Intent intentHomeWord = new Intent(mContext, HomeWorkFragmentActivity.class);
                /*commented By Krishna - 23-02-2019 Remove Condition for ISERP*/

                intentHomeWord.putExtra("position", getResources().getString(R.string.SwitchAccount));


                /*END*/

//                if (new UserSharedPrefrence(mContext).getIsERP()) {
//                    intentHomeWord.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                } else {
//                    intentHomeWord.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                }
                intentHomeWord.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(intentHomeWord);
                finish();
                overridePendingTransition(0, 0);
            } else {
                saveLoginLog();
            }

        } else if (ServiceResource.NOTIFICATIONCOUNT_METHODNAME.equalsIgnoreCase(methodName)) {

            JSONObject obj;
            JSONArray jsonObj;
            JSONArray jsonObjfriend;

            try {

                Global.homeworkModels = new ArrayList<HomeWorkModel>();
                obj = new JSONObject(result);
                jsonObj = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE);
                jsonObjfriend = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE1);

                JSONArray jsonObjleave = obj.getJSONArray(ServiceResource.NOTIFICATION_TABLE2);

                for (int i = 0; i < jsonObj.length(); i++) {

                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_NOTIFICATIONCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_NOTIFICATIONCOUNT));

                }

                for (int i = 0; i < jsonObjfriend.length(); i++) {

                    JSONObject innerObj = jsonObjfriend.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLOGIN_FRIENDCOUNT(innerObj.getString(ServiceResource.NOTIFICATION_FRIENDCOUNT));

                }

                for (int i = 0; i < jsonObjleave.length(); i++) {
                    JSONObject innerObj = jsonObjleave.getJSONObject(i);
                    new UserSharedPrefrence(mContext).setLEAVECOUNT(innerObj.getString(ServiceResource.NOTIFICATION_LEAVEAPPLICATION));
                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

        } else if (ServiceResource.SAVELOGINLOG_METHODNAME.equalsIgnoreCase(methodName)) {

            if (isMoreChild) {

                Global.userdataModel.setSave(true);
//                Global.loginuserdataModel.setSave(true);
                Intent i = new Intent(mContext, HomeWorkFragmentActivity.class);
                /*commented By Krishna - 23-02-2019 Remove Condition for ISERP*/

                i.putExtra("position", getResources().getString(R.string.SwitchAccount));


                /*END*/

//                if (new UserSharedPrefrence(mContext).getIsERP()) {
//                    i.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                } else {
//                    i.putExtra("position", mContext.getResources().getString(R.string.SwitchAccount));
//                }
                i.putExtra("isFrom", ServiceResource.FROMLOGIN);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            } else {
                if (isLogin) {
                    new UserSharedPrefrence(mContext).setLoginModel(Global.userdataModel);
                    new UserSharedPrefrence(mContext).setISREMEMBAR(true);
                    Intent intentHomeWork = new Intent(mContext, HomeWorkFragmentActivity.class);
                    intentHomeWork.putExtra("position", mContext.getResources().getString(R.string.Wall));
                    startActivity(intentHomeWork);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void isERP() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, Global.userdataModel.getERPMemberID()));

        if (Global.userdataModel.getMemberType().contains(ServiceResource.USER_STUDENT_STRING)) {
            arrayList.add(new PropertyVo(ServiceResource.USERTYPE, "student"));
        } else if (Global.userdataModel.getMemberType().contains(ServiceResource.USER_PARENTS_STRING)) {
            arrayList.add(new PropertyVo(ServiceResource.USERTYPE, "parent"));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.USERTYPE, "teacher"));
        }

        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.CHKINSTITUTEORUSEREXISTINERP_METHODNAME, ServiceResource.FEES_URL);
    }

    public void changeGCMID(String UserId) {

        Log.d("insidegcmtoken", "sdzfc");
        if (PreferenceData.getToken().equalsIgnoreCase(null) || PreferenceData.getToken().equalsIgnoreCase("null")

                || PreferenceData.getToken().equalsIgnoreCase("") || PreferenceData.getToken().isEmpty()) {

            try {

                Utility.RefreshFirebaseToken(mContext);

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USERNAME, UserId));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_GCMID, PreferenceData.getToken()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.CHANGEGCMID_METHODNAME, ServiceResource.LOGIN_URL);

    }

    public void saveLoginLog() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, Global.userdataModel.getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, Global.userdataModel.getMemberID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.SAVELOGINLOG_METHODNAME, ServiceResource.LOGIN_URL);
    }

    public void GetUserRoleRightList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, Global.userdataModel.getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, Global.userdataModel.getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, Global.userdataModel.getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.LOGIN_ROLLID, Global.userdataModel.getRoleID()));
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETUSERROLERIGHTLIST_METHODNAME, ServiceResource.LOGIN_URL);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {


    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length() >= 6) {

            if (Utility.isNetworkAvailable(mContext)) {
                if (edt_optnumber.getText().toString().trim().length() == 0) {
                    Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseEnterotp), "Error");
                }
                if (mobileNumber != null && mobileNumber.length() == 0) {
                    Utility.showAlertDialog(mContext, getResources().getString(R.string.Pleasemobilenumber), "Error");

                } else {

                    verifyOtp();

                }

            } else {

                Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            }
        }

    }
}
