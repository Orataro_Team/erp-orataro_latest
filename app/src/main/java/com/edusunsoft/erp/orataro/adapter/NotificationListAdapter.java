package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.edusunsoft.erp.orataro.FragmentActivity.ListSelectionActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.ListLeaveActivity;
import com.edusunsoft.erp.orataro.activities.SinglePostActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.CTextView;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.model.NotificationListModel;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class NotificationListAdapter extends BaseAdapter {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private CTextView txt_notification;
    private ImageView img_settings;
    CircleImageView iv_profile_pic;
    private ArrayList<NotificationListModel> notificationListModels = new ArrayList<>();
    private ArrayList<NotificationListModel> copyList = new ArrayList<>();
    private static final int ID_DELETE = 2;
    private static final int ID_EDIT = 1;
    private ActionItem actionDelete, actionEdit;
    private QuickAction quickAction;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};

    public NotificationListAdapter(Context context, ArrayList<NotificationListModel> notificationListModels) {
        mContext = context;
        this.notificationListModels = notificationListModels;
        copyList.addAll(notificationListModels);
    }

    @Override
    public int getCount() {
        return notificationListModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.notification_listraw, parent, false);
        txt_notification = (CTextView) convertView.findViewById(R.id.txt_notification);
        iv_profile_pic = (CircleImageView) convertView.findViewById(R.id.iv_profile_pic);
        convertView.setBackgroundColor(colors[position % colors.length]);
        iv_profile_pic.setColorFilter(mContext.getResources().getColor(R.color.blue), PorterDuff.Mode.SRC_ATOP);
        txt_notification.setText(notificationListModels.get(position).getFullName() + " " + notificationListModels.get(position).getNotificationText() + " " + notificationListModels.get(position).getDetail());
        if (notificationListModels.get(position).getNotificationType().equalsIgnoreCase("like")) {
            iv_profile_pic.setImageResource(R.drawable.fb_like);
        } else if (notificationListModels.get(position).getNotificationType().equalsIgnoreCase("dislike")) {
            iv_profile_pic.setImageResource(R.drawable.fb_like);
            iv_profile_pic.setRotation(-180f);
        } else if (notificationListModels.get(position).getNotificationType().equalsIgnoreCase("Share")) {
            iv_profile_pic.setImageResource(R.drawable.fb_share);
        } else if (notificationListModels.get(position).getNotificationType().equalsIgnoreCase("Comment")) {
            iv_profile_pic.setImageResource(R.drawable.fb_comments);
        }

        actionDelete = new ActionItem(ID_DELETE, "Delete", mContext.getResources().getDrawable(R.drawable.delete));
        actionEdit = new ActionItem(ID_EDIT, "Edit", mContext.getResources().getDrawable(R.drawable.edit_profile));
        quickAction = new QuickAction(mContext, QuickAction.VERTICAL);
        quickAction.addActionItem(actionEdit);
        quickAction.addActionItem(actionDelete);

        quickAction
                .setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos,
                                            int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);

                        if (actionId == ID_EDIT) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Edit));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.Edit), Toast.LENGTH_SHORT).show();
                        } else if (actionId == ID_DELETE) {
                            Utility.toast(mContext, mContext.getResources().getString(R.string.Delete));
//							Toast.makeText(mContext,  mContext.getResources().getString(R.string.Delete), Toast.LENGTH_SHORT).show();
                        }

                    }

                });

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String ASSOCIATIONTYPE = notificationListModels.get(position).getAssociationType();
                Log.d("getassociationtype", ASSOCIATIONTYPE);

                if (ASSOCIATIONTYPE.equalsIgnoreCase("Post")) {

                    Intent nIntent = new Intent(mContext, SinglePostActivity.class);
                    nIntent.putExtra("isFrom", true);
                    nIntent.putExtra("aId", notificationListModels.get(position).getAssociationID());
                    nIntent.putExtra("aType", notificationListModels.get(position).getAssociationType());
                    mContext.startActivity(nIntent);

                } else if (ASSOCIATIONTYPE.equalsIgnoreCase("LeaveApplication")) {

                    if (Utility.isTeacher(mContext)) {

                        Intent intent = new Intent(mContext, ListSelectionActivity.class);
                        intent.putExtra("isFrom", ServiceResource.LEAVE_FLAG);
                        intent.putExtra("iswithoutsubject", true);
                        mContext.startActivity(intent);

                    } else {

                        Intent intent = new Intent(mContext, ListLeaveActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(intent);

                    }

                }

            }


        });

        return convertView;
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        notificationListModels.clear();
        if (charText.length() == 0) {
            notificationListModels.addAll(copyList);

        } else {

            for (NotificationListModel vo : copyList) {

                if (vo.getProfile_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    notificationListModels.add(vo);
                }
            }
        }

        this.notifyDataSetChanged();

    }

}
