package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddDataByTeacherActivity;
import com.edusunsoft.erp.orataro.activities.CheckedStudentListActivity;
import com.edusunsoft.erp.orataro.activities.ShowUploadedHomeworkActivity;
import com.edusunsoft.erp.orataro.activities.UploadHomeworkActivity;
import com.edusunsoft.erp.orataro.activities.ViewHomWorkDetailsActivity;
import com.edusunsoft.erp.orataro.customeview.ActionItem;
import com.edusunsoft.erp.orataro.customeview.QuickAction;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GenerallWallDataDao;
import com.edusunsoft.erp.orataro.database.HomeWorkListModel;
import com.edusunsoft.erp.orataro.database.HomeworkListDataDao;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeWorkListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context context;
    public static List<HomeWorkListModel> homeWorkModels, copyList;
    public HomeWorkListModel homeWorkListModel;
    private TextView tv_date, tv_subject, tv_sub_detail, txt_teacher_name, txt_upload_homework, txt_show_upload_homework;
    private ImageView list_image;
    private TextView txt_month;
    private TextView txt_date;
    private LinearLayout ll_view;
    private LinearLayout ll_date, ll_date_img, ll_subname;
    private TextView txtSubname, txtEnddate;
    public static int pos;
    public static boolean isFinish;
    private RefreshListner refresh;
    private static final int ID_EDIT = 5;
    private static final int ID_DELETE = 6;
    private static final int ID_CHECKED = 7;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};

    HomeworkListDataDao homeworkListDataDao;
    GenerallWallDataDao generallWallDataDao;
    public String DELETE_ID = "";
    public boolean isFinishChecked = false;

    public HomeWorkListAdapter(Context context, List<HomeWorkListModel> homeWorkModels, RefreshListner listner) {

        this.context = context;
        this.homeWorkModels = homeWorkModels;
        this.refresh = listner;
        copyList = new ArrayList<HomeWorkListModel>();
        copyList.addAll(homeWorkModels);

    }

    @Override
    public int getCount() {
        return homeWorkModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.homework_listraw, parent, false);

        LinearLayout ll_finish = (LinearLayout) convertView.findViewById(R.id.ll_finish);
        CheckBox chk_finish = (CheckBox) convertView.findViewById(R.id.chb_finish);
        TextView txtisfinish = (TextView) convertView.findViewById(R.id.txtisfinish);
        txtisfinish.setText(context.getResources().getString(R.string.Finished));

        homeworkListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(context).homeworkListDataDao();

        LinearLayout ll_editdelete = (LinearLayout) convertView.findViewById(R.id.ll_editdelete);
        ll_editdelete.setTag("" + position);
        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            if (Utility.ReadWriteSetting(ServiceResource.HOMEWORKSETTING).getIsDelete() || Utility.ReadWriteSetting(ServiceResource.HOMEWORKSETTING).getIsEdit()) {
                ll_editdelete.setVisibility(View.VISIBLE);
            } else {
                ll_editdelete.setVisibility(View.GONE);
            }
        } else {
            ll_editdelete.setVisibility(View.GONE);
        }

        if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {
            ll_finish.setVisibility(View.GONE);
        } else {
            ll_finish.setVisibility(View.VISIBLE);
        }

        if (homeWorkModels.get(position).isFinish()) {

            chk_finish.setChecked(true);

        } else {

            chk_finish.setChecked(false);

        }

        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
        tv_sub_detail = (TextView) convertView.findViewById(R.id.tv_sub_detail);
        ll_subname = (LinearLayout) convertView.findViewById(R.id.ll_subname);
        txtSubname = (TextView) convertView.findViewById(R.id.txtSubname);
        txtEnddate = (TextView) convertView.findViewById(R.id.txtEnddate);
        txt_teacher_name = (TextView) convertView.findViewById(R.id.txt_teacher_name);
        txt_upload_homework = (TextView) convertView.findViewById(R.id.txt_upload_homework);
        txt_show_upload_homework = (TextView) convertView.findViewById(R.id.txt_show_upload_homework);

        if (!Utility.isTeacher(context)) {
            txt_upload_homework.setVisibility(View.VISIBLE);
        }

        txt_upload_homework.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent studentlistintent = new Intent(context, UploadHomeworkActivity.class);
                studentlistintent.putExtra("AssociationID", homeWorkModels.get(position).getAssignmentID());
                context.startActivity(studentlistintent);
            }
        });

        txt_show_upload_homework.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent studentlistintent = new Intent(context, ShowUploadedHomeworkActivity.class);
                studentlistintent.putExtra("AssociationID", homeWorkModels.get(position).getAssignmentID());
                context.startActivity(studentlistintent);
            }
        });

        if (new UserSharedPrefrence(context).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            txt_teacher_name.setVisibility(View.VISIBLE);
        }
        TextView txtdivstd = (TextView) convertView.findViewById(R.id.txtdivstd);
        txtdivstd.setVisibility(View.VISIBLE);
        convertView.setBackgroundColor(colors[position % colors.length]);
        ll_view.setBackgroundColor(colors_list[position % colors_list.length]);
        ll_subname.setVisibility(View.VISIBLE);
        final QuickAction quickActionForEditOrDelete;
        ActionItem actionEdit, actionDelete, actionChecked;
        actionEdit = new ActionItem(ID_EDIT, "Edit", context.getResources().getDrawable(R.drawable.edit_profile));
        actionDelete = new ActionItem(ID_DELETE, "Delete", context.getResources().getDrawable(R.drawable.delete));
        actionChecked = new ActionItem(ID_CHECKED, "Checked", context.getResources().getDrawable(R.drawable.check_icon));
        quickActionForEditOrDelete = new QuickAction(context, QuickAction.VERTICAL);
        quickActionForEditOrDelete.addActionItem(actionEdit);
        quickActionForEditOrDelete.addActionItem(actionDelete);
        quickActionForEditOrDelete.addActionItem(actionChecked);
        quickActionForEditOrDelete.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int _pos, int actionId) {

                ActionItem actionItem = quickActionForEditOrDelete.getActionItem(_pos);

                if (actionId == ID_EDIT) {
                    int editPos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    btnClick(editPos, 1);
                } else if (actionId == ID_DELETE) {
                    final int deletepos = Integer.valueOf(((View) source.getAnchor()).getTag().toString());
                    Utility.deleteDialog(context, context.getResources().getString(R.string.Homework), homeWorkModels.get(deletepos).getTitle(), new Popup() {
                        @Override
                        public void deleteYes() {
                            DELETE_ID = homeWorkModels.get(deletepos).getAssignmentID();
                            deleteHomework(homeWorkModels.get(deletepos).getAssignmentID());
                        }

                        @Override
                        public void deleteNo() {
                        }

                    });

                } else if (actionId == ID_CHECKED) {

                    /*commented By Krishna : 17-05-2019 Redirect to CheckHomework List of Student*/

                    Intent studentlistintent = new Intent(context, CheckedStudentListActivity.class);
                    studentlistintent.putExtra("FROMSSTR", "Homework");
                    studentlistintent.putExtra("AssociationID", homeWorkModels.get(position).getAssignmentID());
                    studentlistintent.putExtra("GradeID", homeWorkModels.get(position).getGradeID());
                    studentlistintent.putExtra("DivisionID", homeWorkModels.get(position).getDivisionID());
                    studentlistintent.putExtra("AssociationType", "Homework");
                    context.startActivity(studentlistintent);

                    /*END*/

                }


            }


        });

        quickActionForEditOrDelete.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });

        ll_editdelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                quickActionForEditOrDelete.show(v);
            }
        });


        if (homeWorkModels.get(position).isFinish()) {
            chk_finish.setChecked(true);
        } else {
            chk_finish.setChecked(false);
        }

        if (homeWorkModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        ll_date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });

        txt_date.setText(homeWorkModels.get(position).getYear());
        if (homeWorkModels.get(position).isIsRead()) {
            list_image.setImageResource(R.drawable.double_tick_sky_blue);
        } else {
            list_image.setImageResource(R.drawable.tick_sky_blue);
        }


        txtdivstd.setText(homeWorkModels.get(position).getGradeName() + " " + homeWorkModels.get(position).getDivisionName());
        tv_date.setText(homeWorkModels.get(position).getDay());
        tv_subject.setText(homeWorkModels.get(position).getTitle());
        txt_teacher_name.setText(homeWorkModels.get(position).getTecherName());
        if (new UserSharedPrefrence(context).getLoginModel().getUserType() != ServiceResource.USER_TEACHER_INT) {
            txt_teacher_name.setVisibility(View.VISIBLE);
        }
        if (Utility.isNull(homeWorkModels.get(position).getHomeWorksDetails())) {
            if (homeWorkModels.get(position).getHomeWorksDetails().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = homeWorkModels.get(position)
                        .getHomeWorksDetails()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {

                        if (!homeWorkModels.get(position).isIsRead()) {

                            if (Utility.isNetworkAvailable(context)) {

                                pos = position;
                                HomeworkListIsRead(homeWorkModels.get(position).getAssignmentID());

                            } else {

                                Utility.showAlertDialog(context, context.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

                            }

                        } else {

                            btnClick(position, 0);

                        }

                    }

                };

                text.setSpan(
                        clickfor, Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                tv_sub_detail.setMovementMethod(LinkMovementMethod.getInstance());
                tv_sub_detail.setText(text, BufferType.SPANNABLE);
            } else {
                tv_sub_detail.setText(homeWorkModels.get(position).getHomeWorksDetails());
            }

        }

        txtSubname.setText(homeWorkModels.get(position).getSubjectName());
        txtEnddate.setText(context.getResources().getString(R.string.EndDate) + ":" + Utility.dateFormate(homeWorkModels.get(position).getDateOfFinish(), "dd/MM/yyyy", "MM/dd/yyyy"));
        tv_sub_detail.setText(homeWorkModels.get(position).getHomeWorksDetails());
        txt_month.setText(homeWorkModels.get(position).getMonth().toUpperCase());

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!homeWorkModels.get(position).isIsRead()) {
                    if (Utility.isNetworkAvailable(context)) {
                        pos = position;
                        HomeworkListIsRead(homeWorkModels.get(position).getAssignmentID());
                    } else {
                        Utility.showAlertDialog(context, context.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
                    }
                } else {
                    btnClick(position, 0);
                }

            }

        });

        chk_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isFinishChecked = true;
                if (isChecked) {
                    pos = position;
                    isFinish = true;
                    HomeworkListIsApprove(homeWorkModels.get(position).getAssignmentID(), true);
                } else {
                    pos = position;
                    isFinish = false;
                    HomeworkListIsApprove(homeWorkModels.get(position).getAssignmentID(), false);
                }

            }

        });

        return convertView;
    }


    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        homeWorkModels.clear();
        if (charText.length() == 0) {
            homeWorkModels.addAll(copyList);
        } else {
            for (HomeWorkListModel vo : copyList) {
                if (vo.getTitle().toLowerCase(Locale.getDefault()).contains(charText) ||
                        vo.getHomeWorksDetails().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getStrDateOfHomeWork().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getSubjectName().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getDivisionName().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getDateOfFinish().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getTitle().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getTecherName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    homeWorkModels.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public void HomeworkListIsRead(String assosiationId) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "HomeWork"));
        arrayList.add(new PropertyVo(ServiceResource.ISREAD, true));

        Log.d("getapprveRequest", arrayList.toString());
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.CHECK_METHODNAME, ServiceResource.CHECK_URL);
    }

    public void HomeworkListIsApprove(String assosiationId, boolean isApprove) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID, assosiationId));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "HomeWork"));
        arrayList.add(new PropertyVo(ServiceResource.ISAPPROVEPARAM, isApprove));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.APPROVE_METHODNAME,
                ServiceResource.CHECK_URL);

    }

    public void deleteHomework(String id) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.PRIMARYID, id));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE, "HomeWork"));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.ASSOCIATIONDELETE_METHODNAME, ServiceResource.CHECK_URL);
    }


    public void UpdateData(String id, boolean checked) {

        for (int i = 0; i < homeWorkModels.size(); i++) {

            if (homeWorkModels.get(i).getAssignmentID().equalsIgnoreCase(id)) {
                homeWorkModels.get(pos).setFinish(checked);
                homeWorkModels.set(pos, homeWorkModels.get(pos));
                notifyDataSetChanged();

            }
        }
    }


    public void btnClick(int position, int i) {

        if (i == 1) {

            if (new UserSharedPrefrence(context).getLoginModel().getUserType() == ServiceResource.USER_TEACHER_INT) {

                Intent intent = new Intent(context, AddDataByTeacherActivity.class);
                intent.putExtra("gradeId", homeWorkModels.get(position).getGradeID());
                intent.putExtra("divisionId", homeWorkModels.get(position).getDivisionID());
                intent.putExtra("subjectId", homeWorkModels.get(position).getSubjectID());
                intent.putExtra("isFrom", ServiceResource.HOMEWORK_FLAG);
                intent.putExtra("subjectName", homeWorkModels.get(position).getSubjectName());
                intent.putExtra("isEdit", true);
                Log.d("homrwork", homeWorkModels.get(position).toString());
                intent.putExtra("HomeworkModel", homeWorkModels.get(position));
                context.startActivity(intent);

            }

        } else {

            homeWorkModels.get(position).setIsRead(true);
            homeWorkModels.set(position, homeWorkModels.get(position));
            notifyDataSetChanged();
            Intent intent = new Intent(context, ViewHomWorkDetailsActivity.class);
            Log.d("getHomeworkimgurl", homeWorkModels.get(position).getImgUrl());
            intent.putExtra("Title", homeWorkModels.get(position).getTitle());
            intent.putExtra("lastdate", homeWorkModels.get(position).getDateOfFinish());
            intent.putExtra("imgUrl", homeWorkModels.get(position).getImgUrl());
            intent.putExtra("gradeName", homeWorkModels.get(position).getGradeName());
            intent.putExtra("divisionName", homeWorkModels.get(position).getDivisionName());
            intent.putExtra("Subname", homeWorkModels.get(position).getSubjectName());
            intent.putExtra("Sub_details", homeWorkModels.get(position).getHomeWorksDetails());
            intent.putExtra("Teacher_name", homeWorkModels.get(position).getTecherName());
            intent.putExtra("Teacher_img", homeWorkModels.get(position).getTeacherProfilePicture());
            intent.putExtra("referencelink", homeWorkModels.get(position).getReferenceLink());
            intent.putExtra("id", homeWorkModels.get(position).getAssignmentID());
            intent.putExtra("isApprove", homeWorkModels.get(position).isFinish());
            intent.putExtra("ReadStatus", "Finished");
            intent.putExtra("isFrom", "Homework");
            context.startActivity(intent);

        }

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.CHECK_METHODNAME.equalsIgnoreCase(methodName)) {
            btnClick(pos, 0);
        } else if (ServiceResource.APPROVE_METHODNAME.equalsIgnoreCase(methodName)) {
            // Commented By Krishna : 18-09-2020 -12:22 - Added to Not refresh after back from detail activity.
            homeWorkModels.get(pos).setFinish(isFinish);
            homeWorkModels.set(pos, homeWorkModels.get(pos));
            notifyDataSetChanged();
            /*END*/
//            refresh.refresh(methodName);
        } else if (ServiceResource.ASSOCIATIONDELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            homeworkListDataDao.deleteHomeworkItemByAssignmentID(DELETE_ID);
            generallWallDataDao = ERPOrataroDatabase.getERPOrataroDatabase(context).generawallDataDao();
            generallWallDataDao.deleteWallItemByAssID(DELETE_ID);
            refresh.refresh(methodName);
            Utility.showToast(context.getResources().getString(R.string.homeworkdelete), context);
        }

    }

}
