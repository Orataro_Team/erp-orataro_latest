package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.Utilities.PreferenceData;
import com.edusunsoft.erp.orataro.adapter.SwitchStudentAdapter;
import com.edusunsoft.erp.orataro.model.LoginModel;
import com.edusunsoft.erp.orataro.model.LoginUserModel;
import com.edusunsoft.erp.orataro.model.SwitchStudentModel;
import com.edusunsoft.erp.orataro.model.UserModel;

import java.util.ArrayList;

public class SwitchStudentFragment extends Fragment {

    private Context mContext;
    private ListView switchListView;
    private LinearLayout ll_add_new;
    private ArrayList<SwitchStudentModel> switchStudentModels;
    private SwitchStudentModel switchStudentModel;
    private SwitchStudentAdapter switchStudentAdapter;
    private FrameLayout fl_main;
    private String result = "";
    private LoginModel model;
    private LoginUserModel usermodel;
    private boolean isSave = false;

    private ArrayList<UserModel> LoginUserList;
    private ArrayList<LoginUserModel> NewLoginUserList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.switch_list_fragment, container, false);
        mContext = getActivity();

        if (HomeWorkFragmentActivity.txt_header != null) {

            HomeWorkFragmentActivity.txt_header.setText(getActivity().getResources().getString(R.string.SwitchAccount));
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 60, 0);

        }

        fl_main = (FrameLayout) view.findViewById(R.id.fl_main);
        fl_main.setVisibility(View.VISIBLE);

        switchListView = (ListView) view.findViewById(R.id.homeworklist);
        ll_add_new = (LinearLayout) view.findViewById(R.id.ll_add_new);
        ll_add_new.setVisibility(View.GONE);

        try {
            AddUserData();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;

    }

    public void AddUserData() {

        try {

            NewLoginUserList = PreferenceData.getSwitchLoginUserData();

            if (NewLoginUserList.size() > 0) {
                switchStudentAdapter = new SwitchStudentAdapter(mContext, NewLoginUserList);
                switchListView.setAdapter(switchStudentAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}