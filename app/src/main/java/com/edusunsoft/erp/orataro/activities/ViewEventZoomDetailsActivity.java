package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener;
import com.edusunsoft.erp.orataro.customeview.PanAndZoomListener.Anchor;
import com.edusunsoft.erp.orataro.util.AdjustableImageView;
import com.edusunsoft.erp.orataro.util.Utility;

public class ViewEventZoomDetailsActivity extends Activity implements OnClickListener {

    private Context mContext;
    private AdjustableImageView iv_ev_details;
    private ImageView img_back;
    private TextView txt_ev_name, txt_ev_details, txt_referencelink;
    private String ev_name, ev_details_txt, ReferenceLink;
    private int ev_details;
    private RelativeLayout rl_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_zoom_details);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ViewEventZoomDetailsActivity.this;

        ev_name = getIntent().getStringExtra("Evname");
        ev_details = getIntent().getIntExtra("Evdetails", 0);
        ev_details_txt = getIntent().getStringExtra("Evdetails_txt");
        ReferenceLink = getIntent().getStringExtra("referencelink");

        img_back = (ImageView) findViewById(R.id.img_back);
        iv_ev_details = (AdjustableImageView) findViewById(R.id.iv_ev_details);

        iv_ev_details.setScaleType(ImageView.ScaleType.MATRIX);
        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        rl_view.setOnTouchListener(new PanAndZoomListener(rl_view,
                iv_ev_details, Anchor.TOPLEFT));

        txt_ev_name = (TextView) findViewById(R.id.txtname);
        txt_ev_details = (TextView) findViewById(R.id.txt_ev_details);
        txt_referencelink = (TextView) findViewById(R.id.txt_referencelink);

        txt_ev_name.setText(ev_name);
        iv_ev_details.setImageResource(ev_details);
        txt_ev_details.setText(ev_details_txt);
        if (ReferenceLink.equalsIgnoreCase("") || ReferenceLink.equalsIgnoreCase("null") || ReferenceLink.equalsIgnoreCase(null)) {
            txt_referencelink.setVisibility(View.GONE);
        } else {
            txt_referencelink.setText(ReferenceLink);
        }

        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                Utility.ISLOADEVENT = false;
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADEVENT = false;
        finish();
    }
}
