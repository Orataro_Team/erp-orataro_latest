package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class ExtraActivityListAdapter extends BaseAdapter {

	private LayoutInflater layoutInfalater;
	private Context context;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };

	public ExtraActivityListAdapter(Context ctx) {
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderItem viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolderItem();
			layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInfalater.inflate(R.layout.assignment_listraw, parent, false);

			viewHolder.list_image = (ImageView) convertView.findViewById(R.id.list_image);
			viewHolder.txt_month = (TextView) convertView.findViewById(R.id.txt_month);
			viewHolder.txt_date = (TextView) convertView.findViewById(R.id.txt_date);
			viewHolder.ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderItem) convertView.getTag();
		}

		int colorPos = position % colors.length;
		convertView.setBackgroundColor(colors[colorPos]);
		int colorPos1 = position % colors_list.length;
		viewHolder.list_image.setBackgroundColor(colors_list[colorPos1]);

		if (position == 0) {
			viewHolder.ll_date.setVisibility(View.VISIBLE);
		} else if (position == 6) {
			viewHolder.ll_date.setVisibility(View.VISIBLE);
		} else {
			viewHolder.ll_date.setVisibility(View.GONE);
		}

		viewHolder.ll_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		return convertView;
	}

	public static class ViewHolderItem {
		TextView homeworkListSubject;
		ImageView list_image;
		TextView txt_month;
		TextView txt_date;
		LinearLayout ll_date;
	}

}
