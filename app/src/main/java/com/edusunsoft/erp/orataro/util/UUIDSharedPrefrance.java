package com.edusunsoft.erp.orataro.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UUIDSharedPrefrance {

    private SharedPreferences pref;
    private Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private String PREF_NAME = "UUIDStore";
    public String UUID = "uuid";
    public String isFirstTime = "isfirsttime";
    public String Lang = "lang";
    private String MOBILE_NUMNBER = "mobilenumber";
    private String PASSWORD = "password";
    public String isMenuPermissionCall = "isMenuPermissionCall";

    public UUIDSharedPrefrance(Context pContext) {
        _context = pContext;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }

    public String getMOBILE_NUMNBER() {
        return pref.getString(MOBILE_NUMNBER, "");
    }

    public void setMOBILE_NUMNBER(String mOBILE_NUMNBER) {
        editor = pref.edit();
        editor.putString(MOBILE_NUMNBER, mOBILE_NUMNBER);
        editor.commit();
    }

    public String getPASSWORD() {
        return pref.getString(PASSWORD, "");
    }

    public void setPASSWORD(String pASSWORD) {
        editor = pref.edit();
        editor.putString(PASSWORD, pASSWORD);
        editor.commit();
    }

    public String getLang() {
        return pref.getString(Lang, "en_US");
    }

    public void setLang(String lang) {
        editor = pref.edit();
        editor.putString(Lang, lang);
        editor.commit();
    }

    public boolean getIsFirstTime() {

        return pref.getBoolean(isFirstTime, false);

    }

    public void setIsFirstTime(boolean isfirsttime) {
        editor = pref.edit();
        editor.putBoolean(isFirstTime, isfirsttime);
        editor.commit();
    }


    public boolean getIsMenuPermissionCall() {

        return pref.getBoolean(isMenuPermissionCall, false);

    }

    public void setIsMenuPermissionCall(boolean isMenuPermissionCall) {
        editor = pref.edit();
        editor.putBoolean("isMenuPermissionCall", isMenuPermissionCall);
        editor.commit();
    }


    public String getUUID() {
        return pref.getString(UUID, "");
    }

    public void setUUID(String uUID) {
        editor = pref.edit();
        editor.putString(UUID, uUID);
        editor.commit();
    }

}
