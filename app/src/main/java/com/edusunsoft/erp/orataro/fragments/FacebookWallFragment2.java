package com.edusunsoft.erp.orataro.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.PhotosListActivity;
import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.SelectAllFromAdapterInterface;
import com.edusunsoft.erp.orataro.Interface.ShareInterface;
import com.edusunsoft.erp.orataro.Interface.WallCustomeClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddPostOnWallActivity;
import com.edusunsoft.erp.orataro.activities.FriendListActivity;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;
import com.edusunsoft.erp.orataro.activities.VideoListActivity;
import com.edusunsoft.erp.orataro.activities.WallMemberActivity;
import com.edusunsoft.erp.orataro.adapter.FetchfriendAdapter;
import com.edusunsoft.erp.orataro.adapter.MyWallAdapter;
import com.edusunsoft.erp.orataro.adapter.MyWallAdapter2;
import com.edusunsoft.erp.orataro.adapter.WallListAdapter;
import com.edusunsoft.erp.orataro.database.CircularListDataDao;
import com.edusunsoft.erp.orataro.database.ClassworkListDataDao;
import com.edusunsoft.erp.orataro.database.DynamicWallListDataDao;
import com.edusunsoft.erp.orataro.database.DynamicWallListModel;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingDataDao;
import com.edusunsoft.erp.orataro.database.DynamicWallSettingModel;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.GenerallWallData;
import com.edusunsoft.erp.orataro.database.GenerallWallDataDao;
import com.edusunsoft.erp.orataro.database.GroupListDataDao;
import com.edusunsoft.erp.orataro.database.HomeworkListDataDao;
import com.edusunsoft.erp.orataro.database.NoticeListDataDao;
import com.edusunsoft.erp.orataro.database.PTCommunicationListDataDao;
import com.edusunsoft.erp.orataro.database.ProjectListDataDao;
import com.edusunsoft.erp.orataro.model.PersonModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.WallListVo;
import com.edusunsoft.erp.orataro.model.WallModel;
import com.edusunsoft.erp.orataro.model.WallPostModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.CircleImageView;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.edusunsoft.erp.orataro.util.Utility.writeDataToFile;

/**
 * All Wall
 *
 * @author admin
 */

public class FacebookWallFragment2 extends Fragment implements OnClickListener,
        WallCustomeClick, ShareInterface, SelectAllFromAdapterInterface, AnimationListener, ResponseWebServices {

    private static final String WALLIDDIFF = "WallIddiff";
    private static final String ISFROM = "isFrom";
    private static final String RELOAD = "reload";
    static View v;
    private static Context mContext;
    private ListView lst_fb_wall;
    public View footerView;
    public Button footer_1;
    private LinearLayout ll_add_new;
    private MyWallAdapter myWallAdapter;
    private MyWallAdapter2 myWallAdapter2;
    private ArrayList<WallModel> wallList;
    private TextView tv_post;
    private LinearLayout showHideLayoutstatus, showHidelayoutphotovideo, ll_Photo, ll_Video, ll_Friends;
    public boolean isMoreData = false;
    private int ShareType;
    private Dialog dialog;
    private EditText edtTeacherName;
    private FetchfriendAdapter fetchfriendAdapter;
    private ListView lst_fetch_friend;
    private TextView txt_nodatafound;
    private ImageView iv_select_all;
    private ImageView img_save, img_back;
    private String wallId;
    public static String from;
    private LinearLayout addpostlayout;
    private CircleImageView iv_profile_pic;
    protected int refreshtype;
    private TextView txt_count;
    private boolean reload = false;
    private boolean isOffline = false;
    public static boolean headerClick = false;
    public static boolean isShowMenu = false;
    public static ListView lvbounce;
    private static ArrayList<DynamicWallListModel> subjectlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> standardlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> divisionlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> grouplist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<DynamicWallListModel> projectlist = new ArrayList<DynamicWallListModel>();
    private static ArrayList<WallListVo> list = new ArrayList<WallListVo>();
    private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;
    private boolean isReloadRequire = false;
    private Runnable mRunnable;
    WallPostModel wallPostModel;
    ImageView img_testing;
    // Flag for current page
    int current_page = 1;

    // database Repo declaration for generallwalldata
    private GenerallWallDataDao generallWallDataDao;
    private List<GenerallWallData> generalWallDataList = new ArrayList<>();
    private String data;
    private static final int REQUEST_STORAGE_FOR_WRITE = 1001;
    private static final int REQUEST_STORAGE_FOR_READ = 1002;
    private static final int PICK_FILE_RESULT_CODE = 1111;

    // variable declaration for generallwall data
    GenerallWallDataDao generalwallDataDao;
    HomeworkListDataDao homeworklistdatadao;
    ClassworkListDataDao classworkListDataDao;
    CircularListDataDao circularListDataDao;
    NoticeListDataDao noteListDataDao;
    PTCommunicationListDataDao ptCommunicationListDataDao;
    GroupListDataDao groupListDataDao;
    ProjectListDataDao projectListDataDao;
    private ArrayList<String> PostCommentIdListOffline = new ArrayList<>();
    private Integer count = 1;
    public String ISFROMWALLDATA = "";
    GenerallWallData generallWallData = new GenerallWallData();
    /*END*/

    // variable declaration for dynamicwallsetting
    DynamicWallSettingDataDao dynamicWallSettingDataDao;
    /*END*/

    // variable declaration for dynamicwallsetting
    DynamicWallListDataDao dynamicWallListDataDao;
    private List<DynamicWallListModel> dynamicWallList = new ArrayList<>();
    /*END*/

    SwipeRefreshLayout mSwipeRefreshLayout;

    public static final FacebookWallFragment2 newInstance(String WallIddiff, String isFrom) {

        FacebookWallFragment2 f = new FacebookWallFragment2();
        Bundle bdl = new Bundle(2);
        bdl.putString(WALLIDDIFF, WallIddiff);
        bdl.putString(ISFROM, isFrom);
        f.setArguments(bdl);
        return f;

    }

    public static final FacebookWallFragment2 newInstance(String WallIddiff, String isFrom, boolean reload) {

        FacebookWallFragment2 f = new FacebookWallFragment2();
        Bundle bdl = new Bundle(2);
        bdl.putString(WALLIDDIFF, WallIddiff);
        bdl.putString(ISFROM, isFrom);
        bdl.putBoolean(RELOAD, reload);
        f.setArguments(bdl);
        return f;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getActivity();

        if (new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase(null)
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("null")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("")
                || new UserSharedPrefrence(mContext).getLoginModel().getUserID().equalsIgnoreCase("NAN")) {

            new UUIDSharedPrefrance(mContext).setMOBILE_NUMNBER(new UserSharedPrefrence(mContext).getLOGIN_MOBILENUMBER());
            new UUIDSharedPrefrance(mContext).setPASSWORD(new UserSharedPrefrence(mContext).getLOGIN_PASSWORD());
            Intent intent = new Intent(mContext, RegisterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            new UserSharedPrefrence(mContext).clearPrefrence();
            startActivity(intent);
            getActivity().finish();

        }

        v = inflater.inflate(R.layout.facebook_wall_fragment, container, false);
        this.wallId = getArguments().getString(WALLIDDIFF);

        this.from = getArguments().getString(ISFROM);
        this.reload = getArguments().getBoolean(RELOAD, false);
        new UserSharedPrefrence(mContext).getLoginModel().setCurrentWallId(wallId);

        Global.wallPostModels = new ArrayList<WallPostModel>();

        generalwallDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).generawallDataDao();
        homeworklistdatadao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).homeworkListDataDao();
        classworkListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).classworkListDataDao();
        circularListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).circularListDataDao();
        noteListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).noticeListDataDao();
        ptCommunicationListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).ptCommunicationListDataDao();
        groupListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).groupListDataDao();
        projectListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).projectListDataDao();
        dynamicWallListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).dynamicWallListDataDao();
        dynamicWallSettingDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).dynamicWallSettingDataDao();

        img_testing = (ImageView) v.findViewById(R.id.img_testing);

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.blue);
        lst_fb_wall = (ListView) v.findViewById(R.id.lst_fb_wall);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    DisplayWallData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        //code to set adapter to populate list
        footerView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_view, null, false);
        footer_1 = (Button) footerView.findViewById(R.id.footer_1);
        footer_1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMoreData == true) {
                    count += 10;
                    try {
                        GetAllWallData(count, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showToast("No More Data", getActivity());
                }

            }

        });

        lst_fb_wall.addFooterView(footerView);

        tv_post = (TextView) v.findViewById(R.id.tv_post);
        showHideLayoutstatus = (LinearLayout) v.findViewById(R.id.showhideLayout);
        showHidelayoutphotovideo = (LinearLayout) v.findViewById(R.id.showhideLayoutphotovideo);
        addpostlayout = (LinearLayout) v.findViewById(R.id.addpostlayout);
        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);
        txt_count = (TextView) v.findViewById(R.id.txt_count);

        ll_Photo = (LinearLayout) v.findViewById(R.id.ll_Photo);
        ll_Video = (LinearLayout) v.findViewById(R.id.ll_Video);
        ll_Friends = (LinearLayout) v.findViewById(R.id.ll_Friends);

        lvbounce = (ListView) v.findViewById(R.id.lvbounce);
        showHideLayoutstatus.setVisibility(View.GONE);
        showHidelayoutphotovideo.setVisibility(View.GONE);
//        addpostlayout.setVisibility(View.GONE);
        tv_post.setOnClickListener(this);
        ll_Photo.setOnClickListener(this);
        ll_Video.setOnClickListener(this);
        wallList = new ArrayList<WallModel>();
        iv_profile_pic = (CircleImageView) v.findViewById(R.id.iv_profile_pic);

        // Code Comment by hardik_kanak 25-01-2021
//        String ProfilePicture = Utility.GetProfilePicture(new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture(), new UserSharedPrefrence(mContext).getLoginModel().getUserID());
//        if (ProfilePicture != null) {
//
//            try {
//
//                RequestOptions options = new RequestOptions()
//                        .centerCrop()
//                        .placeholder(R.drawable.photo)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .priority(Priority.HIGH)
//                        .dontAnimate()
//                        .dontTransform();
//
//                Glide.with(mContext)
//                        .load(ServiceResource.BASE_IMG_URL1
//                                + ProfilePicture
//                                .replace("//", "/")
//                                .replace("//", "/"))
//                        .apply(options)
//                        .into(iv_profile_pic);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }



        if (new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture() != null) {

            try {

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                        .dontAnimate()
                        .dontTransform();

                Glide.with(mContext)
                        .load(ServiceResource.BASE_IMG_URL1
                                + new UserSharedPrefrence(mContext).getLoginModel().getProfilePicture())
                        .apply(options)
                        .into(iv_profile_pic);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        headerClick = true;
        isShowMenu = false;
        lvbounce.setVisibility(View.GONE);

        // need to revert for back issue
//        isShowMenu = false;
//        lvbounce.setVisibility(View.GONE);

        try {

            if (HomeWorkFragmentActivity.txt_header != null) {
                if (from.equalsIgnoreCase(ServiceResource.Wall)) {

                    try {
                        HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Wall) + " \n" + "(" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
//                        HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Wall) + " (" + Utility.GetFirstName(mContext) + ")");
                        new UserSharedPrefrence(mContext).setCURRENTWALLID(new UserSharedPrefrence(mContext).getLOGIN_WALLID());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Standard) + " \n" + "(" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
//                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Standard) + " (" + Utility.GetFirstName(mContext) + ")");
                }

                if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                    Log.d("frominstwall", "");
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Institute) + " \n" + "(" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
//                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Institute) + " (" + Utility.GetFirstName(mContext) + ")");
                    new UserSharedPrefrence(mContext).setCURRENTWALLID(new UserSharedPrefrence(mContext).getLOGIN_INSTITUTIONWALLID());
                }

                if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Division) + " \n" + "(" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
//                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Division) + " (" + Utility.GetFirstName(mContext) + ")");
                }

                if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Subjects) + " \n" + "(" + new UserSharedPrefrence(mContext).getLoginModel().getFullName() + ")");
//                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Subjects) + " (" + Utility.GetFirstName(mContext) + ")");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        // Call api to get deketed wall post : Commented By Krishna : 31-05-2020
        GetDeletedWallPostData(false);

        try {
            DisplayWallData();
            // need to revert for back issue
//            menuChangeWall(v, true);
            /*END*/
        } catch (Exception e) {
            e.printStackTrace();
        }

//        lst_fb_wall.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
//
//            public void onRefresh() {
//
//                DisplayWallData();
//                refreshtype = 1;
//
//            }
//
//        });

//        lst_fb_wall.setOnLoadMoreListener(new PullAndLoadListView.OnLoadMoreListener() {
//
//            @Override
//            public void onLoadMore() {
//                if (isMoreData) {
//                    count += 10;
//                    try {
//                        GetAllWallData(count, true);
//                        refreshtype = 0;
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
////                    try {
////                        GetAllWallData(count, false);
////                        refreshtype = 0;
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
//                } else {
//                    count = 0;
//                }
//
//            }
//
//        });

        lst_fb_wall.setSmoothScrollbarEnabled(true);

        return v;

    }

    private void GetDeletedWallPostData(boolean isLoader) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        Log.d("deletedatarequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isLoader, this, isLoader, false).execute(ServiceResource.GETDELETEPOSTDATA, ServiceResource.LOGIN_URL);

    }


    @Override
    public void onResume() {

        super.onResume();

//        try {
//            DisplayWallData();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }


    public static void showMenuHeader() {

        // need to revert for back issue
//        if (Utility.ISHEADERCLICKED == false) {
//            Utility.ISHEADERCLICKED = true;
//            menuChangeWall(v, true);
//        } else {
//            lvbounce.setVisibility(View.GONE);
//            Utility.ISHEADERCLICKED = false;
//        }

        if (headerClick) {

            if (!isShowMenu) {

                isShowMenu = true;
                menuChangeWall(v, true);

            } else {

                isShowMenu = false;
                menuChangeWall(v, false);
                lvbounce.setVisibility(View.GONE);

            }

        }

    }

    private void DisplayWallData() {

        if (Utility.isNetworkAvailable(getActivity())) {

            DynamicMenuList();

            if (from.equalsIgnoreCase(ServiceResource.Wall)) {
                generalWallDataList = generalwallDataDao.getAllGenerallWallData();
                Log.d("getGenerallWallData", generalWallDataList.toString());
            } else if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
                generalWallDataList = generalwallDataDao.getPersonelWallData(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
                Log.d("getmyprofiledata", generalWallDataList.toString());
            } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Institute");
                Log.d("getinstittedata", generalWallDataList.toString());
            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                generalWallDataList = generalwallDataDao.getDynamicWallData("Grade", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                generalWallDataList = generalwallDataDao.getDynamicWallData("Division", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
            } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                generalWallDataList = generalwallDataDao.getDynamicWallData("Subject", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                generalWallDataList = generalwallDataDao.getDynamicWallData("Group", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                generalWallDataList = generalwallDataDao.getDynamicWallData("Project", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
            }

            // Datewise sorting TODO

            if (generalWallDataList.size() == 0) {

                try {
                    GetAllWallData(1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
//
//                btnLoadMore = new Button(getActivity());
//                btnLoadMore.setText("Load More");
//
//                // Adding button to listview at footer
//                lst_fb_wall.addFooterView(btnLoadMore);

                // set Adapter to listview
                SetAdapter();
                /*END*/

                try {
                    GetAllWallData(1, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } else {

            subjectlist.clear();
            standardlist.clear();
            divisionlist.clear();
            grouplist.clear();
            projectlist.clear();
            dynamicWallList = dynamicWallListDataDao.getDynamicWallList();
            if (dynamicWallList.size() > 0) {
                for (int i = 0; i < dynamicWallList.size(); i++) {
                    if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Subject")) {
                        subjectlist.add(dynamicWallList.get(i));
                    } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Grade")) {
                        standardlist.add(dynamicWallList.get(i));
                    } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Division")) {
                        divisionlist.add(dynamicWallList.get(i));
                    } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Group")) {
                        grouplist.add(dynamicWallList.get(i));
                    } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("project")) {
                        projectlist.add(dynamicWallList.get(i));
                    }
                }
            }

            // check for this db is exist or not TODO

            if (from.equalsIgnoreCase(ServiceResource.Wall)) {
                generalWallDataList = generalwallDataDao.getAllGenerallWallData();
            } else if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
                generalWallDataList = generalwallDataDao.getPersonelWallData(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Institute");
            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Grade");
            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Division");
            } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Subject");
            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Group");
            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                generalWallDataList = generalwallDataDao.getMyWallData("Project");
            }
            //            count = generalWallDataList.size();
            // set Adapter to listview
            SetAdapter();
            /*END*/

            if (generalWallDataList.size() == 0) {
                Utility.showToast("No internet Connected", mContext);
            }

        }
        /*END*/
    }

    private void SetAdapter() {

        if (from.equalsIgnoreCase(ServiceResource.Wall)) {
            generalWallDataList = generalwallDataDao.getAllGenerallWallData();
        } else if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {
            generalWallDataList = generalwallDataDao.getPersonelWallData(new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
        } else if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
            generalWallDataList = generalwallDataDao.getMyWallData("Institute");
        } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
            generalWallDataList = generalwallDataDao.getDynamicWallData("Grade", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
        } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
            generalWallDataList = generalwallDataDao.getDynamicWallData("Division", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
        } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
            generalWallDataList = generalwallDataDao.getDynamicWallData("Subject", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
        } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
            generalWallDataList = generalwallDataDao.getDynamicWallData("Group", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
        } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
            generalWallDataList = generalwallDataDao.getDynamicWallData("Project", ServiceResource.SELECTED_DYNAMIC_WALL_ID);
        }

        if (generalWallDataList.size() < 10) {
            lst_fb_wall.removeFooterView(footerView);
        }

        if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {
            //Creating a button - Load More
            if (generalWallDataList.size() > 0 && generalWallDataList != null && !generalWallDataList.isEmpty()) {

                myWallAdapter2 = new MyWallAdapter2(getActivity(), generalWallDataList, FacebookWallFragment2.this, FacebookWallFragment2.this, false, from);
                lst_fb_wall.setAdapter(myWallAdapter2);

//                if (btnLoadMore != null) {
//                    // Adding button to listview at footer
//                    lst_fb_wall.addFooterView(btnLoadMore);
//                    btnLoadMore.setOnClickListener(new View.OnClickListener() {
//
//                       @Override
//                        public void onClick(View arg0) {
//                            if (isMoreData == true) {
//                                count += 10;
//                                try {
//                                    GetAllWallData(count, true);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                Utility.showToast("No More Data", getActivity());
//                            }
//
//                        }
//
//                    });

//                }

            } else {

                lst_fb_wall.removeFooterView(footerView);
                showHideLayoutstatus.setVisibility(View.GONE);
                showHideLayoutstatus.setVisibility(View.GONE);
                showHidelayoutphotovideo.setVisibility(View.GONE);
                txt_nodatafound.setVisibility(View.VISIBLE);

                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {
                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {
                        addpostlayout.setVisibility(View.VISIBLE);
                        tv_post.setVisibility(View.VISIBLE);
                    } else {
                        addpostlayout.setVisibility(View.INVISIBLE);
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                        ll_Photo.setVisibility(View.INVISIBLE);
                    }

                } else {

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                        if ((Global.DynamicWallSetting != null)) {

                            if (Global.DynamicWallSetting.getIsAllowPeopleToUploadAlbum() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostStatus()) {
                                addpostlayout.setVisibility(View.VISIBLE);
                            } else {
                                addpostlayout.setVisibility(View.VISIBLE);
                                tv_post.setVisibility(View.INVISIBLE);
                            }

                        }

                    } else {
                        addpostlayout.setVisibility(View.VISIBLE);
                        tv_post.setVisibility(View.INVISIBLE);
                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                        ll_Photo.setVisibility(View.INVISIBLE);
                    }

                }

                try {
                    txt_nodatafound.setText(mContext.getResources().getString(R.string.NoWallDataAvailable));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
//            if (refreshtype == 0) {
//                lst_fb_wall.onLoadMoreComplete();
//            } else if (refreshtype == 1) {
//                lst_fb_wall.onRefreshComplete();
//            }
//            refreshtype = -1;
        } else {

            {

                if (generalWallDataList.size() > 0 && Global.DynamicWallSetting != null) {
                    myWallAdapter2 = new MyWallAdapter2(getActivity(), generalWallDataList, FacebookWallFragment2.this, FacebookWallFragment2.this, false, from);
                    lst_fb_wall.setAdapter(myWallAdapter2);
                } else {
                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                        if ((Global.DynamicWallSetting != null)) {

                            if (Global.DynamicWallSetting.getIsAllowPeopleToUploadAlbum() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() ||
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostStatus()) {
                                addpostlayout.setVisibility(View.VISIBLE);
                            } else {
                                addpostlayout.setVisibility(View.VISIBLE);
                                tv_post.setVisibility(View.INVISIBLE);
                            }

                        }

                    } else {

                        addpostlayout.setVisibility(View.VISIBLE);
                        tv_post.setVisibility(View.INVISIBLE);

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                        ll_Photo.setVisibility(View.INVISIBLE);
                    }
                    try {
                        txt_nodatafound.setText(mContext.getResources().getString(R.string.NoWallDataAvailable));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
//                lst_fb_wall.removeFooterView(btnLoadMore);

            }
        }

//        if (refreshtype == 0) {
//            lst_fb_wall.onLoadMoreComplete();
//        } else if (refreshtype == 1) {
//            lst_fb_wall.onRefreshComplete();
//        }
//        refreshtype = -1;

    }

    private void GetAllWallData(int rowno, boolean isLoader) {

        if (isLoader) {
            Log.d("isloader", "true");
        } else {
            Log.d("isloader", "false");
        }

        if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {

//            isMoreData = true;

            current_page += 1;

            ArrayList<PropertyVo> arrayList = new ArrayList<>();
            arrayList.add(new PropertyVo(ServiceResource.WALLID,
                    wallId));
            arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.WALL_ROWNOSMALL, String.valueOf(rowno)));

            Log.d("mywallrequest", arrayList.toString());

            new AsynsTaskClass(mContext, arrayList, isLoader, this).execute(ServiceResource.GETMYWALLDATA_METHODNAME, ServiceResource.WALL_URL);

        } else if (from.equalsIgnoreCase(ServiceResource.Wall)) {

//            isMoreData = true;

            current_page += 1;

            ArrayList<PropertyVo> arrayList = new ArrayList<>();
            arrayList.add(new PropertyVo(ServiceResource.WALLID,
                    wallId));
            arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.WALL_ROWNOSMALL, String.valueOf(rowno)));

            Log.d("generalwallrequest", arrayList.toString());

            new AsynsTaskClass(mContext, arrayList, isLoader, this).execute(ServiceResource.WALL_METHODNAME, ServiceResource.WALL_URL);

        } else {

            current_page += 1;

//            isMoreData = true;

            ArrayList<PropertyVo> arrayList = new ArrayList<>();
            arrayList.add(new PropertyVo(ServiceResource.WALLID,
                    wallId));
            arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
            arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
            arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
            arrayList.add(new PropertyVo(ServiceResource.WALL_ROWNOSMALL, String.valueOf(rowno)));

            Log.d("dynamicwallrequest", arrayList.toString());

            new AsynsTaskClass(mContext, arrayList, isLoader, this).execute(ServiceResource.GETDYNAMICWALLDATA_METHODNAME, ServiceResource.WALL_URL);

//            if (generalWallDataList.size() == 1) {
//                new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETDYNAMICWALLDATA_METHODNAME, ServiceResource.WALL_URL);
//            } else {
//                new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETDYNAMICWALLDATA_METHODNAME, ServiceResource.WALL_URL);
//            }

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_post:

                /*commented By Krishna : 28-06-2019 Check Permission for Post Status on wall*/

                if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") ||
                            new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) {

                        Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                        intent.putExtra("isFrom", from);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                        ((Activity) mContext).finish();

                    } else {

                        Utility.toast(mContext, "You Dont have Permission");

                    }

                    if (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("false")) {
                        ll_Photo.setVisibility(View.INVISIBLE);
                    }

                } else {

//                    if (Global.DynamicWallSetting != null) {
                    if ((new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostStatus().equalsIgnoreCase("true") &&
                            Global.DynamicWallSetting.getIsAllowPeopleToPostStatus() &&
                            Global.DynamicWallSetting.getIsAllowPostStatus()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostPhoto().equalsIgnoreCase("true") &&
                                    Global.DynamicWallSetting.getIsAllowPeoplePostComment() &&
                                    Global.DynamicWallSetting.getIsAllowPostPhoto()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostVideo().equalsIgnoreCase("true") &&
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostVideos() &&
                                    Global.DynamicWallSetting.getIsAllowPostVideo()) ||
                            (new UserSharedPrefrence(mContext).getLoginModel().isIsAllowUserToPostFiles().equalsIgnoreCase("true")) &&
                                    Global.DynamicWallSetting.getIsAllowPeopleToPostDocument() &&
                                    Global.DynamicWallSetting.getIsAllowPostFile()) {

                        Intent intent = new Intent(mContext, AddPostOnWallActivity.class);
                        intent.putExtra("isFrom", from);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        ((Activity) mContext).finish();
                        getActivity().overridePendingTransition(0, 0);

                    } else {
                        Utility.toast(mContext, "You Dont have Permission");
                    }

//                    }
//                else{
//                        Utility.toast(mContext, "You Dont have Permission");
//                    }

                }

                /*END*/


                break;

            case R.id.ll_Photo:

                Intent intent = new Intent(mContext, PhotosListActivity.class);
                intent.putExtra("isFrom", from);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            case R.id.ll_Video:

                intent = new Intent(mContext, VideoListActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            case R.id.ll_Friends:

                intent = new Intent(mContext, FriendListActivity.class);
                intent.putExtra("friend", true);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);

                break;

            default:
                break;

        }

    }

    @Override
    public void clickOnPhoto() {
        Intent intent = new Intent(mContext, PhotosListActivity.class);
        intent.putExtra("isFrom", from);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnVideo() {
        Intent intent = new Intent(mContext, VideoListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnFriends() {
        Intent intent = new Intent(mContext, FriendListActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

    @Override
    public void clickOnPost() {

        /*commented By Krishna : 28-06-2019 Check Permission for Post Status on wall*/

        try {
            Utility.CheckPermission(getActivity(), from, ll_Photo, Global.DynamicWallSetting);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDeletePost() {

    }

    @Override
    public void onEditPost() {

    }

    /**
     * share public webservice call reuslt
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shrePublic(WallPostModel, int)
     */
    @Override
    public void shrePublic(WallPostModel wallPostModel, int position) {

        ShareType = 1;
        new SharePost().execute(wallPostModel);

    }

    /**
     * share onlyMe webservice call examresult
     */

    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreOnlyMe(WallPostModel wallPostModel, int position) {

        ShareType = 2;
        new SharePost().execute(wallPostModel);

    }

    /**
     * share public webservice call examresult
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreFriend(WallPostModel wallPostModel, int position) {

        ShareType = 3;
        new SharePost().execute(wallPostModel);
    }

    /**
     * share special friend webservice call examresult
     */
    /*
     * (non-Javadoc)
     * @see ShareInterface#shreOnlyMe(WallPostModel, int)
     */
    @Override
    public void shreSpecialFriend(WallPostModel wallPostModel, int position) {

        ShareType = 4;
        new ShareSpecialFriend().execute(wallPostModel);

    }

    /**
     * webservice call for special friends
     */
    public class ShareSpecialFriend extends
            AsyncTask<WallPostModel, Void, Void> {

        //		ProgressDialog progressDialog = null;
        private String result;
        private boolean success;
        WallPostModel wallPostModel;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext);
            LoaderProgress.setMessage("Fetching Friends...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
        }

        @Override
        protected Void doInBackground(WallPostModel... params) {

            // TODO Auto-generated method stub
            wallPostModel = params[0];
            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();
            map1.put(ServiceResource.MEMBERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map1.put(ServiceResource.BEATCH_ID, null);

            map.put(2, map1);

            result = webcall.getJSONFromSOAPWS(
                    ServiceResource.FRIENDS_METHODNAME, map,
                    ServiceResource.FRIENDS_URL);

            JSONArray jsonObj;

            try {

                Global.FriendsList = new ArrayList<PersonModel>();
                jsonObj = new JSONArray(result);
                // JSONArray detailArrray= jsonObj.getJSONArray("");

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    PersonModel model = new PersonModel();
                    model.setPersonName(innerObj
                            .getString(ServiceResource.FRIENDS_FULLNAME));
                    model.setProfileImg(innerObj
                            .getString(ServiceResource.FRIENDS_PROFILEPIC));
                    if (model.getProfileImg() != null
                            && !model.getProfileImg().equals("")) {
                        model.setProfileImg(ServiceResource.BASE_IMG_URL
                                + model.getProfileImg().substring(1,
                                model.getProfileImg().length()));
                    }
                    model.setFriendId(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDID));
                    model.setFriendListID(innerObj
                            .getString(ServiceResource.FRIENDS_FRIENDLIST));
                    model.setWallID(innerObj
                            .getString(ServiceResource.FRIENDS_WALLID));
                    Global.FriendsList.add(model);

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }

            dialog = CustomDialog.ShowDialog(mContext, R.layout.dialog_fetch_friends, true);
//            dialog = new Dialog(mContext);
//
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            dialog.getWindow().setBackgroundDrawableResource(
//                    android.R.color.transparent);
//            dialog.setContentView(R.layout.dialog_fetch_friends);
//            dialog.setCancelable(true);
//            dialog.show();

            img_save = (ImageView) dialog.findViewById(R.id.img_save);
            img_back = (ImageView) dialog.findViewById(R.id.img_back);

            img_back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }

            });

            img_save.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    // TODO Auto-generated method stub
                    dialog.dismiss();
                    new SharePost().execute(wallPostModel);

                }

            });

            LinearLayout searchlayout = (LinearLayout) dialog
                    .findViewById(R.id.searchlayout);
            searchlayout.setVisibility(View.VISIBLE);

            edtTeacherName = (EditText) dialog
                    .findViewById(R.id.edtsearchStudent);

            lst_fetch_friend = (ListView) dialog
                    .findViewById(R.id.lst_fetch_friend);

            TextView txt_nodatafound = (TextView) dialog
                    .findViewById(R.id.txt_nodatafound);

            if (Global.FriendsList != null && Global.FriendsList.size() > 0) {
                fetchfriendAdapter = new FetchfriendAdapter(mContext,
                        Global.FriendsList, FacebookWallFragment2.this);
                lst_fetch_friend.setAdapter(fetchfriendAdapter);
            } else {
                searchlayout.setVisibility(View.GONE);
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoFriendsAvailable));
            }

            iv_select_all = (ImageView) dialog.findViewById(R.id.iv_select_all);
            iv_select_all.setTag("0");
            iv_select_all.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (iv_select_all.getTag().equals("0")) {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(true);
                                iv_select_all.setTag("1");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(R.color.blue));
                            fetchfriendAdapter.notifyDataSetChanged();
                        }

                    } else {
                        if (Global.FriendsList != null
                                && Global.FriendsList.size() > 0) {
                            for (int i = 0; i < Global.FriendsList.size(); i++) {
                                Global.FriendsList.get(i).setSelected(false);
                                iv_select_all.setTag("0");
                            }
                            iv_select_all.setColorFilter(mContext
                                    .getResources().getColor(
                                            R.color.grey_fb_color));

                            fetchfriendAdapter.notifyDataSetChanged();
                        }
                    }

                }
            });

            edtTeacherName.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {

                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {


                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

                    if (fetchfriendAdapter != null) {

                        String text = edtTeacherName.getText().toString()
                                .toLowerCase(Locale.getDefault());
                        fetchfriendAdapter.filter(text);

                    }

                }

            });

        }

    }

    /**
     * webservice call for share post
     *
     * @author admin
     */
    public class SharePost extends AsyncTask<WallPostModel, Void, Void> implements ICancleAsynkTask {

        //		ProgressDialog progressDialog = null;
        private String result;
        private boolean success;


        @Override
        protected void onPreExecute() {
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage("Sharing Post...");
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(WallPostModel... params) {
            try {

                WebserviceCall webcall = new WebserviceCall();
                HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();
                HashMap<String, String> map1 = new HashMap<String, String>();
                map1.put(ServiceResource.WALLID, wallId);
                map1.put(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
                map1.put(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID());
                map1.put(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
                map1.put(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID());
                map1.put(ServiceResource.SENDTOMEMBERID, params[0].getSendToMemberID());
                map1.put(ServiceResource.SHRE_POST_SHAREPOSTID, params[0].getPostCommentID());
                if (ShareType == 1) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Public");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 2) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Only Me");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 3) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Friend");
                    map1.put(ServiceResource.SHRE_POST_TAGID, null);
                } else if (ShareType == 4) {
                    map1.put(ServiceResource.SHRE_POST_SHARETYPE, "Special Friend");
                    String selectFriend = "";
                    for (int i = 0; i < Global.FriendsList.size(); i++) {
                        if (Global.FriendsList.get(i).isSelected()) {
                            selectFriend += Global.FriendsList.get(i).getFriendId() + ",";
                            map1.put(ServiceResource.SHRE_POST_TAGID, selectFriend);
                        }
                    }
                    selectFriend.substring(0, selectFriend.length() - 1);

                }

                map.put(2, map1);

                result = webcall.getJSONFromSOAPWS(ServiceResource.SHRE_POST_METHODNAME, map, ServiceResource.SHRE_POST_URL);
                success = true;
            } catch (Exception e) {
                success = false;
                e.printStackTrace();
            }
            if (ServiceResource.isShowLog) {
                Log.e("dsds", result);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }

            Utility.sendPushNotification(mContext);

            if (success) {
                Utility.toast(mContext, getResources().getString(R.string.ShareSuccessfull));
//                Toast.makeText(mContext, getResources().getString(R.string.ShareSuccessfull), Toast.LENGTH_SHORT).show();
            } else {
                Utility.toast(mContext, getResources().getString(R.string.Error));
//                Toast.makeText(mContext, getResources().getString(R.string.Error), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

        @Override
        public void onCancleTask() {
            cancel(true);
        }

    }

    @Override
    public void selectedFriends(int selectedFriendCount) {
        if (Global.FriendsList.size() == selectedFriendCount) {
            iv_select_all.setColorFilter(mContext.getResources().getColor(R.color.blue));
            iv_select_all.setTag("1");
        } else {
            iv_select_all.setColorFilter(mContext.getResources().getColor(R.color.grey_fb_color));
            iv_select_all.setTag("0");
        }
    }


    /**
     * @param tv
     * @param isVisible
     */

    public static void menuChangeWall(View tv, boolean isVisible) {

        // need to revert for back issue
//        Animation animBounce;
//        list.clear();
//        WallListVo vo;
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.Wall));
//        vo.setIco(R.drawable.wall);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.mywall));
//        vo.setIco(R.drawable.mywall);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.Institutewall));
//        vo.setIco(R.drawable.dash_institute);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.Standardwall));
//        vo.setIco(R.drawable.dash_grade);
//        vo.setDynamicWallList(standardlist);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.Divisionwall));
//        vo.setIco(R.drawable.dash_division);
//        vo.setDynamicWallList(divisionlist);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.Subjectswall));
//        vo.setIco(R.drawable.dash_subject);
//        vo.setDynamicWallList(subjectlist);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.groupwall));
//        vo.setIco(R.drawable.schoolgroups);
//        vo.setDynamicWallList(grouplist);
//        list.add(vo);
//
//        vo = new WallListVo();
//        vo.setWallName(mContext.getResources().getString(R.string.projectwall));
//        vo.setIco(R.drawable.project);
//        vo.setDynamicWallList(projectlist);
//        list.add(vo);
//
//        Utility.dynamicwalllist = list;
//
////        lvbounce.setVisibility(View.VISIBLE);
////        WallListAdapter adapter = new WallListAdapter(mContext, list, from, true);
////        lvbounce.setAdapter(adapter);
////        animBounce = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
////        lvbounce.startAnimation(animBounce);

        Animation animBounce;
        if (isVisible) {
            list.clear();
            WallListVo vo;
            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Wall));
            vo.setIco(R.drawable.wall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.mywall));
            vo.setIco(R.drawable.mywall);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Institutewall));
            vo.setIco(R.drawable.dash_institute);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Standardwall));
            vo.setIco(R.drawable.dash_grade);
            vo.setDynamicWallList(standardlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Divisionwall));
            vo.setIco(R.drawable.dash_division);
            vo.setDynamicWallList(divisionlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.Subjectswall));
            vo.setIco(R.drawable.dash_subject);
            vo.setDynamicWallList(subjectlist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.groupwall));
            vo.setIco(R.drawable.schoolgroups);
            vo.setDynamicWallList(grouplist);
            list.add(vo);

            vo = new WallListVo();
            vo.setWallName(mContext.getResources().getString(R.string.projectwall));
            vo.setIco(R.drawable.project);
            vo.setDynamicWallList(projectlist);
            list.add(vo);

            lvbounce.setVisibility(View.VISIBLE);
            WallListAdapter adapter = new WallListAdapter(mContext, list, from, true);
            lvbounce.setAdapter(adapter);
            animBounce = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
            lvbounce.startAnimation(animBounce);

        } else {

            lvbounce.endViewTransition(tv);
            lvbounce.clearAnimation();
            lvbounce.setVisibility(View.GONE);

        }

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /**
     * dynamic menu webservice
     */

    public void DynamicMenuList() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));

        }

        arrayList.add(new PropertyVo(ServiceResource.GETUSERDYNAMICMENUDATA_ROLENAME, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        Log.d("getDynamicMenuDatareq", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, false, this).execute(ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME, ServiceResource.LOGIN_URL);

    }

    /**
     * get response for all webservice
     */

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.GETUSERDYNAMICMENUDATA_METHODNAME.equalsIgnoreCase(methodName)) {
            parseDaynamicList(result);
        } else if (ServiceResource.GETDELETEPOSTDATA.equalsIgnoreCase(methodName)) {
            Log.d("getdeltedresult", result);
            JSONArray DeletedPostArray, DeletedHomeworkArray, DeletedClassworkArray, DeletedCircularArray, DeletedEventArray,
                    DeletedHolidayArray, DeletedNotesArray, DeletedTODOSArray, DeletedPTCommunicationArray, DeletedProjectArray, DeletedGroupArray;
            JSONObject jobj = null;
            try {
                jobj = new JSONObject(result);
                DeletedPostArray = jobj.getJSONArray(ServiceResource.POST);
                DeletedHomeworkArray = jobj.getJSONArray(ServiceResource.HOMEWORK);
                DeletedClassworkArray = jobj.getJSONArray(ServiceResource.DELETEDCLASSWORK);
                DeletedCircularArray = jobj.getJSONArray(ServiceResource.CIRCULARS);
                DeletedEventArray = jobj.getJSONArray(ServiceResource.EVENTS);
                DeletedHolidayArray = jobj.getJSONArray(ServiceResource.HOLIDAYS);
                DeletedNotesArray = jobj.getJSONArray(ServiceResource.NOTES);
                DeletedTODOSArray = jobj.getJSONArray(ServiceResource.TODOS);
                DeletedPTCommunicationArray = jobj.getJSONArray(ServiceResource.PTCOMMUNICATION);
                DeletedProjectArray = jobj.getJSONArray(ServiceResource.PROJECT);
                DeletedGroupArray = jobj.getJSONArray(ServiceResource.GROUP);

                if (DeletedPostArray.length() > 0) {
                    for (int i = 0; i < DeletedPostArray.length(); i++) {
                        JSONObject Obj = DeletedPostArray.getJSONObject(i);
                        generalwallDataDao.deleteWallItemByPostCommentID(Obj.getString(ServiceResource.WALL_POSTCOMMENTID));
                    }
                }

                if (DeletedHomeworkArray.length() > 0) {
                    for (int i = 0; i < DeletedHomeworkArray.length(); i++) {
                        JSONObject Obj = DeletedHomeworkArray.getJSONObject(i);
                        homeworklistdatadao.deleteHomeworkItemByAssignmentID(Obj.getString(ServiceResource.ASSIGNMENTID));
                    }
                }

                if (DeletedClassworkArray.length() > 0) {
                    for (int i = 0; i < DeletedClassworkArray.length(); i++) {
                        JSONObject Obj = DeletedClassworkArray.getJSONObject(i);
                        classworkListDataDao.deleteclassworkItemByClassworkID(Obj.getString(ServiceResource.CLASSWORKID));
                    }
                }

                if (DeletedCircularArray.length() > 0) {
                    for (int i = 0; i < DeletedCircularArray.length(); i++) {
                        JSONObject Obj = DeletedCircularArray.getJSONObject(i);
                        circularListDataDao.deletecircularItemByCircularID(Obj.getString(ServiceResource.CIRCULARID));
                    }
                }

                if (DeletedNotesArray.length() > 0) {
                    for (int i = 0; i < DeletedNotesArray.length(); i++) {
                        JSONObject Obj = DeletedNotesArray.getJSONObject(i);
                        noteListDataDao.deleteNotesItemByNoticeID(Obj.getString(ServiceResource.NOTICEID));
                    }
                }

                if (DeletedPTCommunicationArray.length() > 0) {
                    for (int i = 0; i < DeletedPTCommunicationArray.length(); i++) {
                        JSONObject Obj = DeletedPTCommunicationArray.getJSONObject(i);
                        ptCommunicationListDataDao.deletePTCommunication(Obj.getString(ServiceResource.COMMUNICATIONID));
                    }
                }

                if (DeletedGroupArray.length() > 0) {
                    for (int i = 0; i < DeletedGroupArray.length(); i++) {
                        JSONObject Obj = DeletedGroupArray.getJSONObject(i);
                        groupListDataDao.deleteGroupByGroupID(Obj.getString(ServiceResource.DELETEGROUPID));
                    }
                }

                if (DeletedProjectArray.length() > 0) {
                    for (int i = 0; i < DeletedProjectArray.length(); i++) {
                        JSONObject Obj = DeletedProjectArray.getJSONObject(i);
                        projectListDataDao.deleteProjectByProjectID(Obj.getString(ServiceResource.PROJECTID));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (ServiceResource.WALL_METHODNAME.equals(methodName)) {

            Log.d("getgeneralwalldata", result);

            if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
                lst_fb_wall.removeFooterView(footerView);
                Utility.showToast("No More Data Available", getActivity());
            }

            isMoreData = true;

            PostCommentIdListOffline.clear();
            for (int i = 0; i < generalwallDataDao.getAllGenerallWallData().size(); i++) {
                PostCommentIdListOffline.add(generalwallDataDao.getAllGenerallWallData().get(i).getPostCommentID());
            }

            JSONArray jsonObj;
            JSONObject resultJson = new JSONObject();
            try {
                resultJson.put("data", new JSONArray(result));
                try {
                    jsonObj = new JSONArray(result);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject Obj = jsonObj.getJSONObject(i);
                        // method to store wallpost data from jsonarray
                        GetWallDataResponse(Obj);
                        /*END*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // get listview current position - used to maintain scroll position
                int currentPosition = lst_fb_wall.getFirstVisiblePosition();
                // set Adapter to listview
                SetAdapter();
                // Setting new scroll position
                lst_fb_wall.setSelectionFromTop(currentPosition + 1, 0);
                /*END*/
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (ServiceResource.GETMYWALLDATA_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
                lst_fb_wall.removeFooterView(footerView);
                Utility.showToast("No More Data Available", getActivity());
            }

            isMoreData = true;
            Log.d("mywallresponse", result);

            PostCommentIdListOffline.clear();
            for (int i = 0; i < generalwallDataDao.getPersonelWallData(new UserSharedPrefrence(mContext).getLoginModel().getMemberID()).size(); i++) {
                PostCommentIdListOffline.add(generalwallDataDao.getPersonelWallData(new UserSharedPrefrence(mContext).getLoginModel().getMemberID()).get(i).getPostCommentID());
            }

            JSONArray jsonObj;
            JSONObject resultJson = new JSONObject();
            try {
                resultJson.put("data", new JSONArray(result));
                try {
                    jsonObj = new JSONArray(result);
                    for (int i = 0; i < jsonObj.length(); i++) {
                        JSONObject Obj = jsonObj.getJSONObject(i);
                        // method to store wallpost data from jsonarray
                        GetWallDataResponse(Obj);
                        /*END*/
                    }

                    // get listview current position - used to maintain scroll position
                    int currentPosition = lst_fb_wall.getFirstVisiblePosition();
                    // set Adapter to listview
                    SetAdapter();
                    // Setting new scroll position
                    lst_fb_wall.setSelectionFromTop(currentPosition + 1, 0);
//                    // set Adapter to listview
//                    SetAdapter();
//                    /*END*/

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (ServiceResource.GETDYNAMICWALLDATA_METHODNAME.equalsIgnoreCase(methodName)) {

            if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
                lst_fb_wall.removeFooterView(footerView);
                Utility.showToast("No More Data Available", getActivity());
            }

            isMoreData = true;

            PostCommentIdListOffline.clear();
            if (from.equalsIgnoreCase(ServiceResource.INSTITUTEWALL)) {
                for (int i = 0; i < generalwallDataDao.getMyWallData("Institute").size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Institute").get(i).getPostCommentID());
                }
            } else if (from.equalsIgnoreCase(ServiceResource.STANDARDWALL)) {
                for (int i = 0; i < generalwallDataDao.getDynamicWallData("Grade", ServiceResource.SELECTED_DYNAMIC_WALL_ID).size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Grade").get(i).getPostCommentID());
                }
            } else if (from.equalsIgnoreCase(ServiceResource.DIVISIONWALL)) {
                for (int i = 0; i < generalwallDataDao.getDynamicWallData("Division", ServiceResource.SELECTED_DYNAMIC_WALL_ID).size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Division").get(i).getPostCommentID());
                }
            } else if (from.equalsIgnoreCase(ServiceResource.SUBJECTWALL)) {
                for (int i = 0; i < generalwallDataDao.getDynamicWallData("Subject", ServiceResource.SELECTED_DYNAMIC_WALL_ID).size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Subject").get(i).getPostCommentID());
                }
            } else if (from.equalsIgnoreCase(ServiceResource.GROUPWALL)) {
                for (int i = 0; i < generalwallDataDao.getDynamicWallData("Group", ServiceResource.SELECTED_DYNAMIC_WALL_ID).size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Group").get(i).getPostCommentID());
                }
            } else if (from.equalsIgnoreCase(ServiceResource.PROJECTWALL)) {
                for (int i = 0; i < generalwallDataDao.getDynamicWallData("Project", ServiceResource.SELECTED_DYNAMIC_WALL_ID).size(); i++) {
                    PostCommentIdListOffline.add(generalwallDataDao.getMyWallData("Project").get(i).getPostCommentID());
                }
            }

            try {

                JSONArray mainJsonArray;
                JSONObject jobj = new JSONObject(result);
                mainJsonArray = jobj.getJSONArray(ServiceResource.WALL_POSTDATA);

                for (int i = 0; i < mainJsonArray.length(); i++) {

                    JSONObject Obj = mainJsonArray.getJSONObject(i);
                    // method to store wallpost data from jsonarray
                    GetWallDataResponse(Obj);
                    /*END*/

                }

                /* get All Dynamic wall settings*/
                onlySetting(result);
                /*END*/

                // get listview current position - used to maintain scroll position
                int currentPosition = lst_fb_wall.getFirstVisiblePosition();
                // set Adapter to listview
                SetAdapter();
                // Setting new scroll position
                lst_fb_wall.setSelectionFromTop(currentPosition + 1, 0);
//               // set Adapter to listview
//                SetAdapter();
//                /*END*/


            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    private void GetWallDataResponse(JSONObject Obj) {

        try {
            generallWallData.setRowNo(Obj.getString(ServiceResource.WALL_ROW_ID));
            generallWallData.setTempDate(Obj.getString(ServiceResource.WALL_TEMPDATE).replace("/Date(", "").replace(")/", ""));
            generallWallData.setPostCommentID(Obj.getString(ServiceResource.WALL_POSTCOMMENTID));
            generallWallData.setWallID(Obj.getString(ServiceResource.WALLID));
            generallWallData.setPostCommentTypesTerm(Obj.getString(ServiceResource.WALL_POSTCOMMENTTYPESTERM));
            generallWallData.setPostCommentNote(Obj.getString(ServiceResource.WALL_POSTCOMMENTNOTE));
            generallWallData.setTotalLikes(Obj.getString(ServiceResource.WALL_TOTALLIKES));
            generallWallData.setTotalComments(Obj.getString(ServiceResource.WALL_TOTALCOMMENTS));
            generallWallData.setTotalDislike(Obj.getString(ServiceResource.WALL_TOTALDISLIKE));
            generallWallData.setMemberID(Obj.getString(ServiceResource.WALL_CMT_MEMBERID));
            generallWallData.setDateOfPost(Obj.getString(ServiceResource.WALL_DATEOFPOST));
            generallWallData.setAssociationID(Obj.getString(ServiceResource.WALL_ASSOCIATIONID));
            generallWallData.setAssociationType(Obj.getString(ServiceResource.WALL_ASSOCIATIONTYPE));
            generallWallData.setFullName(Obj.getString(ServiceResource.WALL_FULLNAME));
            generallWallData.setIsDisLike(Obj.getString(ServiceResource.WALL_ISDISLIKE));
            generallWallData.setIsLike(Obj.getString(ServiceResource.WALL_ISLIKE));
            generallWallData.setProfilePicture(Obj.getString(ServiceResource.WALL_PROFILEPICTURE));
            generallWallData.setPhoto(Obj.getString(ServiceResource.WALL_PHOTO));
            generallWallData.setPhotoCount(Obj.getString(ServiceResource.WALL_PHOTOCOUNT));
            generallWallData.setPhotoUrls(Obj.getString(ServiceResource.WALL_PHOTOURLS));
            generallWallData.setPostCount(Obj.getString(ServiceResource.WALL_POSTCOUNT));
            generallWallData.setPostUrls(Obj.getString(ServiceResource.WALL_POSTURLS));
            generallWallData.setFileType(Obj.getString(ServiceResource.WALL_FILETYPE));
            generallWallData.setFileMimeType(Obj.getString(ServiceResource.WALL_FILEMIMETYPE));
            generallWallData.setWallTypeTerm(Obj.getString(ServiceResource.WALL_TYPE_TERM));
            generallWallData.setPostedOn(Obj.getString(ServiceResource.WALL_POSTEDON));
            generallWallData.setIsAllowPeopleToShareYourPost(Obj.getString(ServiceResource.ISALLOWPEOPLETOSHAREYOURPOST));
            generallWallData.setIsAllowPeopleToLikeOrDislikeOnYourPost(Obj.getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKEONYOURPOST));
            generallWallData.setIsAllowPeopleToPostMessageOnYourWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTMESSAGETOWALL));
            generallWallData.setIsAllowPeopleToLikeAndDislikeCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOLIKEORDISLIKECOMMENTWALL));
            generallWallData.setIsAllowPeopleToShareCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOSHARECOMMENTWALL));
            generallWallData.setIsAllowPeoplePostCommentWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTWALL));

            if (from.equalsIgnoreCase(ServiceResource.Wall) || from.equalsIgnoreCase(ServiceResource.PROFILEWALL)) {

                generallWallData.setIsAllowLikeDislike(Obj.getString(ServiceResource.ISALLOWLIKEDISLIKE));
                generallWallData.setIsAllowPostComment(Obj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                generallWallData.setPostComment(Obj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                generallWallData.setIsAllowSharePost(Obj.getString(ServiceResource.ISALLOWSHAREPOST));

            }

            generallWallData.setIsAllowPeopleToPostCommentOnPostWall(Obj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTONPOSTWALL));
            //TODO Create RoomDatabase (SQLite) object for access database of Room


            if (PostCommentIdListOffline.contains(generallWallData.getPostCommentID())) {

                Log.d("getUpdate", "update");

                generalwallDataDao.updateGenerallWallData(count, generallWallData.getPostCommentID(), generallWallData.getWallID(), generallWallData.getMemberID(),
                        generallWallData.getPostCommentTypesTerm(), generallWallData.getPostCommentNote(), generallWallData.getTotalLikes(), generallWallData.getTotalComments(),
                        generallWallData.getTotalDislike(),
                        generallWallData.getDateOfPost(), generallWallData.getAssociationID(), generallWallData.getAssociationType(), generallWallData.getFullName(),
                        generallWallData.getIsDisLike(), generallWallData.getIsLike(), generallWallData.getProfilePicture(), generallWallData.getPhoto(),
                        generallWallData.getTempDate(), generallWallData.getPostDate(), generallWallData.getPostCount(), generallWallData.getPostUrls(),
                        generallWallData.getWallTypeTerm(), generallWallData.getFileType(), generallWallData.getPostedOn(), generallWallData.getFileMimeType(),
                        generallWallData.getSendToMemberID(), generallWallData.getPhotoCount(), generallWallData.getPhotoUrls(), generallWallData.getPostName(),
                        generallWallData.getAlbumPhotoID(), generallWallData.isUserComment(), generallWallData.isUserShare(), generallWallData.isUserLike(),
                        generallWallData.isUserDisLike(), generallWallData.getIsAllowLikeDislike(), generallWallData.getIsAllowPostComment(),
                        generallWallData.getPostComment(), generallWallData.getIsAllowSharePost(), generallWallData.getIsAllowPeopleToPostCommentOnPostWall(),
                        generallWallData.getIsAllowPeopleToShareYourPost(), generallWallData.getIsAllowPeopleToLikeOrDislikeOnYourPost(),
                        generallWallData.getIsAllowPeopleToPostMessageOnYourWall(), generallWallData.getIsAllowPeopleToLikeAndDislikeCommentWall(),
                        generallWallData.getIsAllowPeopleToShareCommentWall(),
                        generallWallData.getIsAllowPeoplePostCommentWall());
                // Update
            } else {
                Log.d("getUpdate", "insert");
                generalwallDataDao.insertGenerallWall(generallWallData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void writeToFile(String data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED) {
                writeDataToFile(getActivity(), data);
            } else {
                this.data = data;
                requestStoragePermission(false);
            }
        } else {
            writeDataToFile(getActivity(), data);
        }

    }

    private void requestStoragePermission(boolean isForRead) {
        if (!isForRead) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_FOR_WRITE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_FOR_READ);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_FOR_WRITE) {
            if (grantResults[0] == PERMISSION_GRANTED) {
                writeToFile(data);
            }
        }
        if (requestCode == REQUEST_STORAGE_FOR_READ) {
            if (grantResults[0] == PERMISSION_GRANTED) {
                openFileChooser();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void openFileChooser() {
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            chooseFile.setType("application/json");
        } else {
            chooseFile.setType("application/octet-stream");
        }
        chooseFile.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICK_FILE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_FILE_RESULT_CODE && resultCode == RESULT_OK) {
            Uri uri;
            if (data != null) {
                uri = data.getData();
                if (uri != null) {
                    String src = Utility.getData(getActivity(), uri);
                    Log.d("TAG", src);
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                    builderSingle.setIcon(R.drawable.ic_launcher);
                    builderSingle.setTitle(getResources().getString(R.string.app_name));
                    builderSingle.setMessage("You will lost your previous all data when perform this operation?");

                    builderSingle.setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());

                    builderSingle.setPositiveButton("Restore", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
//                        restoreData(src);
                    });
                    builderSingle.show();
                } else {
                    Log.d("TAG", "No path found");
//                    Utility.showError("Error while restoring data");
                }
            } else {
                Log.d("TAG", "No data found");
//                Utility.showError("Error while restoring data");
            }
        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * show hide menu click on header
     */

    public class SortedDate implements Comparator<WallPostModel> {
        @Override
        public int compare(WallPostModel o1, WallPostModel o2) {
            return o1.getTempDate().compareTo(o2.getTempDate());
        }
    }

    public void parseDaynamicList(String result) {

        DynamicWallListModel model = new DynamicWallListModel();
        ArrayList<DynamicWallListModel> templist = new ArrayList<DynamicWallListModel>();

        try {

            if (ServiceResource.isShowLog) {

            }

            dynamicWallListDataDao.deleteDynamicWallList();

            JSONObject jsonobj = new JSONObject(result);
            JSONArray table = jsonobj.getJSONArray(ServiceResource.GETUSERDYNAMICMENUDATA_TABLE);


            for (int i = 0; i < table.length(); i++) {

                JSONObject obj = table.getJSONObject(i);
                model.setWallID(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLID));
                model.setWallName(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLNAME));
                model.setWallImage(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_WALLIMAGE));
                model.setAssociationType(obj.getString(ServiceResource.GETUSERDYNAMICMENUDATA_ASSOCIATIONTYPE));
                templist.add(model);

                /*insert dynamicwallmenulist into database.*/
                dynamicWallListDataDao.insertDynamicWallList(model);
                /*END*/

            }

            subjectlist.clear();
            standardlist.clear();
            divisionlist.clear();
            grouplist.clear();
            projectlist.clear();


        } catch (Exception e) {
            e.printStackTrace();
        }

        dynamicWallList = dynamicWallListDataDao.getDynamicWallList();
        if (!dynamicWallList.isEmpty()) {
            for (int i = 0; i < dynamicWallList.size(); i++) {
                if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Subject")) {
                    subjectlist.add(dynamicWallList.get(i));
                } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Grade")) {
                    standardlist.add(dynamicWallList.get(i));
                } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Division")) {
                    divisionlist.add(dynamicWallList.get(i));
                } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("Group")) {
                    grouplist.add(dynamicWallList.get(i));
                } else if (dynamicWallList.get(i).AssociationType.equalsIgnoreCase("project")) {
                    projectlist.add(dynamicWallList.get(i));
                }
            }

        }

    }

    @Override
    public void onStop() {

        super.onStop();

        if (HomeWorkFragmentActivity.img_wallarrow != null) {

            HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.GONE);

        }

    }

    @Override
    public void onStart() {

        super.onStart();

        if (HomeWorkFragmentActivity.img_wallarrow != null) {

            HomeWorkFragmentActivity.img_wallarrow.setVisibility(View.GONE);

            HomeWorkFragmentActivity.img_wallarrow.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getActivity(), WallMemberActivity.class);
                    mContext.startActivity(i);

                }

            });

        }

    }

    public void onlySetting(String result) {

        try {

            if (from.equalsIgnoreCase(ServiceResource.PROFILEWALL) || from.equalsIgnoreCase(ServiceResource.Wall)) {

            } else {

                JSONObject jobj = new JSONObject(result);
                JSONArray jsonArray = jobj.getJSONArray(ServiceResource.WALL_ROLEDATA);
                JSONArray jsonArrayAdminsetting = jobj.getJSONArray(ServiceResource.WALLROLEDATA);
                Log.d("getWallRoleData123", jsonArrayAdminsetting.toString());
                Log.d("getWallRoleData12345", jsonArray.toString());

                Global.DynamicWallSetting = new DynamicWallSettingModel();

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject innerObj = jsonArray.getJSONObject(i);
                    Global.DynamicWallSetting.setIsAllowPeopleToLikeThisWall(innerObj.getString(ServiceResource.ISALLOWPEOPLETOLIKETHISWALL));
                    Global.DynamicWallSetting.setIsAllowPeoplePostComment(innerObj.getString(ServiceResource.ISALLOWPEOPLEPOSTCOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToShareComment(innerObj.getString(ServiceResource.ISALLOWPEOPLETOSHARECOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToLikeAndDislikeComment(innerObj.getString(ServiceResource.ISALLOWPEOPLETOLIKEANDDISLIKECOMMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostStatus(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAllowPeopleToCreatePoll(innerObj.getString(ServiceResource.ISALLOWPEOPLETOCREATEPOLL));
                    Global.DynamicWallSetting.setIsAllowPeopleToParticipateInPoll(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPARTICIPATEINPOLL));
                    Global.DynamicWallSetting.setIsAllowPeopleToInviteOtherPeople(innerObj.getString(ServiceResource.ISALLOWPEOPLETOINVITEOTHERPEOPLE));
                    Global.DynamicWallSetting.setIsAllowPeopleToTagOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOTAGONPOST));
                    Global.DynamicWallSetting.setIsAllowPeopleToUploadAlbum(innerObj.getString(ServiceResource.ISALLOWPEOPLETOUPLOADALBUM));
                    Global.DynamicWallSetting.setIsAllowPeopleToPutGeoLocationOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPUTGEOLOCATIONONPOST));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostDocument(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTDOCUMENT));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostVideos(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTVIDEOS));
                    Global.DynamicWallSetting.setWallName(innerObj.getString(ServiceResource.WALLNAME));
                    Global.DynamicWallSetting.setWallImage(innerObj.getString(ServiceResource.WALLIMAGE));
                    Global.DynamicWallSetting.setWallID(innerObj.getString(ServiceResource.WALLID));
                    Global.DynamicWallSetting.setIsAutoApprovePost(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOST));
                    Global.DynamicWallSetting.setIsAutoApprovePostStatus(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAutoApproveAlbume(innerObj.getString(ServiceResource.ISAUTOAPPROVEALBUME));
                    Global.DynamicWallSetting.setIsAutoApproveVideos(innerObj.getString(ServiceResource.ISAUTOAPPROVEVIDEOS));
                    Global.DynamicWallSetting.setIsAutoApproveDocument(innerObj.getString(ServiceResource.ISAUTOAPPROVEDOCUMENT));
                    Global.DynamicWallSetting.setIsAutoApprovePoll(innerObj.getString(ServiceResource.ISAUTOAPPROVEPOLL));
                    Global.DynamicWallSetting.setIsAdmin(innerObj.getString(ServiceResource.ISADMIN));
                    Global.DynamicWallSetting.setIsAllowPeopleToPostCommentOnPost(innerObj.getString(ServiceResource.ISALLOWPEOPLETOPOSTCOMMENTONPOST));

                }

                for (int j = 0; j < jsonArrayAdminsetting.length(); j++) {

                    JSONObject innerObj = jsonArrayAdminsetting.getJSONObject(j);
                    Global.DynamicWallSetting.setIsAllowLikeDislike(innerObj.getString(ServiceResource.ISALLOWLIKEDISLIKE));
                    Global.DynamicWallSetting.setIsAllowPostAlbum(innerObj.getString(ServiceResource.ISALLOWPOSTALBUM));
                    Global.DynamicWallSetting.setIsAllowPostComment(innerObj.getString(ServiceResource.ISALLOWPOSTCOMMENT));
                    Global.DynamicWallSetting.setIsAllowPostFile(innerObj.getString(ServiceResource.ISALLOWPOSTFILE));
                    Global.DynamicWallSetting.setIsAllowPostPhoto(innerObj.getString(ServiceResource.ISALLOWPOSTPHOTO));
                    Global.DynamicWallSetting.setIsAllowPostStatus(innerObj.getString(ServiceResource.ISALLOWPOSTSTATUS));
                    Global.DynamicWallSetting.setIsAllowPostVideo(innerObj.getString(ServiceResource.ISALLOWPOSTVIDEO));
                    Global.DynamicWallSetting.setIsAllowSharePost(innerObj.getString(ServiceResource.ISALLOWSHAREPOST));

                }

                if (jsonArray.length() > 0) {
                    if (jsonArrayAdminsetting.length() > 0) {
                        dynamicWallSettingDataDao.deleteDynamicWallSettingData();
                        dynamicWallSettingDataDao.insertDynamicSettingWall(Global.DynamicWallSetting);
                    }
                }
            }

            Global.DynamicWallSetting = dynamicWallSettingDataDao.getDynamicWallSettingData();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
