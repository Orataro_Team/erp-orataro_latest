package com.edusunsoft.erp.orataro.model;

public class PhotoModel {

	private String photos;
	private String AlbumID,AlbumTitle,AlbumDetails,Photo,Total,new_name;

	public String getNew_name() {
		return new_name;
	}

	public void setNew_name(String new_name) {
		this.new_name = new_name;
	}

	public String getAlbumID() {
		return AlbumID;
	}

	public void setAlbumID(String albumID) {
		AlbumID = albumID;
	}

	public String getAlbumTitle() {
		return AlbumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		AlbumTitle = albumTitle;
	}

	public String getAlbumDetails() {
		return AlbumDetails;
	}

	public void setAlbumDetails(String albumDetails) {
		AlbumDetails = albumDetails;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}

	public String getTotal() {
		return Total;
	}

	public void setTotal(String total) {
		Total = total;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "PhotoModel{" +
				"photos='" + photos + '\'' +
				", AlbumID='" + AlbumID + '\'' +
				", AlbumTitle='" + AlbumTitle + '\'' +
				", AlbumDetails='" + AlbumDetails + '\'' +
				", Photo='" + Photo + '\'' +
				", Total='" + Total + '\'' +
				", new_name='" + new_name + '\'' +
				'}';
	}
}
