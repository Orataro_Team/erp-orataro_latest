package com.edusunsoft.erp.orataro.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.Config;
import com.edusunsoft.erp.orataro.util.JustifiedTextView;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class AboutUsActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private ImageView imgLeftheader, imgRightheader;
    private TextView txtHeader;
    private Context mContext;
    private JustifiedTextView wv_view;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private YouTubePlayerView youTubeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus_activity);
        mContext = AboutUsActivity.this;
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        wv_view = (JustifiedTextView) findViewById(R.id.txt_about);

        String content = "<p>ORATARO is a smart communication platform for students, parents and teachers with a real time update on Class Activity, Homework, Circular, Academic Calendar, Progress update and schoolgroups discussion for brainstorming and other project work within a class or at a school level. The super smart features of ORATARO will reinforce the scale of teacher and parent interaction and will influence more involvement of both in advancements of the child's education. The App allows the School to create content categories/sub-categories as and when they wish facilitate communication between Students, Parents and Teachers. This facility is not available among the other school App available on the Market. ORATARO supports all types of digital content including all document types e.g. PDF, Word, Excel, Videos and Photos.</p><p><strong><span style=\"font-size: medium;color:#006699;\">&nbsp;</span></strong></p>";

        wv_view.setText(content);

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);

        try {
            txtHeader.setText(getResources().getString(R.string.AboutUs) + " (" + Utility.GetFirstName(mContext) + ")");
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgLeftheader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }

        });

        youTubeView.initialize(Config.DEVELOPER_KEY, this);

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();

        } else {
            String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
            Utility.toast(this, errorMessage);
            //            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(Config.YOUTUBE_VIDEO_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }
}
