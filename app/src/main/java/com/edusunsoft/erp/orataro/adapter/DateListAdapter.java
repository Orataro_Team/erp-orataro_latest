package com.edusunsoft.erp.orataro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

import java.util.ArrayList;
import java.util.Locale;

public class DateListAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<String> dateGrp, copyList;
	private String strbg;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };
	public DateListAdapter(Context context) {

	}

	public DateListAdapter(Context context, ArrayList<String> date) {
		this.mContext = context;
		this.dateGrp = date;
		copyList = new ArrayList<String>();
		copyList.addAll(dateGrp);
	}

	@Override
	public int getCount() {
		return dateGrp.size();
	}

	@Override
	public Object getItem(int position) {
		return 0;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.date_list_item, parent, false);
		final TextView datelist = (TextView) convertView.findViewById(R.id.txt_date_item);
		datelist.setText(dateGrp.get(position).toString());
		return convertView;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		dateGrp.clear();
		if (charText.length() == 0) {
			dateGrp.addAll(copyList);
		} else {
			for (String vo : copyList) {
				if (vo.toLowerCase(Locale.getDefault()).contains(charText)) {
					dateGrp.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}
}
