package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class TodoListModel implements Serializable {

    boolean visibleMonth;
    private String TodosID;
    private String EndDate;
    private String Title;
    private String Details;
    private String UserName;
    private String TypeTerm;
    private String CompletDate;
    private String Status;
    private String OnCreated;
    private String Year;
    private String CreateOn, milliSecond, month, day;

    
    public boolean isVisibleMonth() {
        return visibleMonth;
    }

    public void setVisibleMonth(boolean visibleMonth) {
        this.visibleMonth = visibleMonth;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMilliSecond() {
        return milliSecond;
    }

    public void setMilliSecond(String milliSecond) {
        this.milliSecond = milliSecond;
    }

    public String getTodosID() {
        return TodosID;
    }

    public void setTodosID(String todosID) {
        TodosID = todosID;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getTypeTerm() {
        return TypeTerm;
    }

    public void setTypeTerm(String typeTerm) {
        TypeTerm = typeTerm;
    }

    public String getCompletDate() {
        return CompletDate;
    }

    public void setCompletDate(String completDate) {
        CompletDate = completDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getOnCreated() {
        return OnCreated;
    }

    public void setOnCreated(String onCreated) {
        OnCreated = onCreated;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getCreateOn() {
        return CreateOn;
    }

    public void setCreateOn(String createOn) {
        CreateOn = createOn;
    }

}
