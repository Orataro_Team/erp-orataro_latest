package com.edusunsoft.erp.orataro.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class LoadedImage implements Parcelable {

	private Bitmap mBitmap;
	private Uri uri;
	private boolean isSelected;

	public LoadedImage() {

	}

	public static Creator<LoadedImage> CREATOR = new Creator<LoadedImage>(){
		public LoadedImage createFromParcel(Parcel source) {
			return new LoadedImage(source);
		}
		public LoadedImage[] newArray(int size) {
			return new LoadedImage[size];
		}
	};

	public LoadedImage(Bitmap bitmap, Uri uri, boolean isSelected) {
		mBitmap = bitmap;
		this.uri = uri;
	}

	private LoadedImage(Parcel in) {
		this.mBitmap = in.readParcelable(Bitmap.class.getClassLoader());
		this.uri = in.readParcelable(Uri.class.getClassLoader());
		this.isSelected = in.readByte() != 0;
	}

	public boolean isSelected() {
		return isSelected;
	}
	
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Uri getUri() {
		return uri;
	}

	public void setUri(Uri uri) {
		this.uri=uri;
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public int describeContents() { 
		return 0; 
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(this.mBitmap, flags);
		dest.writeParcelable(this.uri, flags);
		dest.writeByte(isSelected ? (byte) 1 : (byte) 0);
	}

}
