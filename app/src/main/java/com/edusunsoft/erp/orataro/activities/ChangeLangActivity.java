package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UUIDSharedPrefrance;
import com.edusunsoft.erp.orataro.util.Utility;

public class ChangeLangActivity extends Activity {

    private ImageView imgLeftheader, imgRightheader;
    private TextView txtHeader;
    private Context mContext;
    private Button btnGujarati, btnEnglish, btnFrench, btnHindi;
    private String isFrom = "";
    private boolean isFromDashboard;
    private View langHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.changelang);
        mContext = ChangeLangActivity.this;
        langHeader = findViewById(R.id.langHeader);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        btnEnglish = (Button) findViewById(R.id.btnEnglish);
        btnGujarati = (Button) findViewById(R.id.btnGujarati);
        btnFrench = (Button) findViewById(R.id.btnFrench);
        btnHindi = (Button) findViewById(R.id.btnHindi);
        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);
        txtHeader.setText(getResources().getString(R.string.changelang));

        isFrom = getIntent().getStringExtra("isFrom");

        if (isFrom != null && !isFrom.equalsIgnoreCase("")) {
            if (isFrom.equalsIgnoreCase(ServiceResource.FROMDASHBOARD)) {
                isFromDashboard = true;
                langHeader.setVisibility(View.VISIBLE);
            } else {
                isFromDashboard = false;
            }
        } else {
            isFromDashboard = false;
        }

        btnHindi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new UUIDSharedPrefrance(mContext).setLang("hi");
                Utility.setLanguage(mContext, "hi");
                if (isFromDashboard) {
                    Intent i = new Intent(ChangeLangActivity.this, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                } else {
                    new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                    Intent i = new Intent(ChangeLangActivity.this, RegisterActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        });

        btnEnglish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new UUIDSharedPrefrance(mContext).setLang("en_US");
                Utility.setLanguage(mContext, "en_US");
                if (isFromDashboard) {
                    Intent i = new Intent(ChangeLangActivity.this, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                } else {
                    new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                    new UUIDSharedPrefrance(mContext).setLang("en_US");
                    Intent i = new Intent(ChangeLangActivity.this, RegisterActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        });

        btnGujarati.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                new UUIDSharedPrefrance(mContext).setLang("GU");

                Utility.setLanguage(mContext, "GU");


                if (isFromDashboard) {

                    Intent i = new Intent(ChangeLangActivity.this, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);

                } else {

                    new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                    Intent i = new Intent(ChangeLangActivity.this, RegisterActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        });

        btnFrench.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new UUIDSharedPrefrance(mContext).setLang("FR");
                Utility.setLanguage(mContext, "FR");
                if (isFromDashboard) {
                    Intent i = new Intent(ChangeLangActivity.this, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                } else {
                    new UUIDSharedPrefrance(mContext).setIsFirstTime(true);
                    Intent i = new Intent(ChangeLangActivity.this, RegisterActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        });


        imgLeftheader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(ChangeLangActivity.this, DashBoardActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);

            }

        });

    }

}
