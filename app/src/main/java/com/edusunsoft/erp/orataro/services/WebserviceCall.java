package com.edusunsoft.erp.orataro.services;

import android.util.Log;

import com.edusunsoft.erp.orataro.util.Constants;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class WebserviceCall {

    String namespace = "http://tempuri.org/";
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    HttpTransportSE androidHttpTransport;
    private String result;
    long startTimeMillsceond, endTimeMilliSceond;

    /**
     * Set Envelope
     */
    protected void SetEnvelope(String url) {
        try {
            // Creating SOAP envelope
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            // You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new HttpTransportSE(url);
            androidHttpTransport.debug = true;

        } catch (Exception e) {

            System.out.println("Soap Exception---->>>" + e.toString());

        }

    }

    // MethodName variable is define for which webservice function will call
    public String getJSONFromSOAPWS(String MethodName, HashMap<Integer, HashMap<String, String>> map, String url) {

        try {

            startTimeMillsceond = System.currentTimeMillis();
            SOAP_ACTION = namespace + MethodName;

            // Adding values to request object
            request = new SoapObject(namespace, MethodName);

            Set<Integer> keys = map.keySet();
            
            for (Iterator<Integer> i = keys.iterator(); i.hasNext(); ) {

                Integer key = (Integer) i.next();
                HashMap<String, String> value = (HashMap<String, String>) map.get(key);
                Set<String> keysmap = value.keySet();
                for (Iterator<String> j = keysmap.iterator(); j.hasNext(); ) {
                    String keyMapValue = (String) j.next();
                    String valueMap = (String) value.get(keyMapValue);

                    if (Constants.DATATYPE_INT == key) {

                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(Integer.class);
                        request.addProperty(pi);

                    } else if (Constants.DATATYPE_STRING == key) {

                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(String.class);
                        request.addProperty(pi);

                    } else if (Constants.DATATYPE_DOUBLE == key) {

                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(Double.class);
                        request.addProperty(pi);

                    } else if (Constants.DATATYPE_BYTE == key) {

                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(Byte.class);
                        request.addProperty(pi);

                    }

                }
            }

            SetEnvelope(url);

            try {
                String[] str = url.split("/");
                HttpTransportSE androidHttpTransport = new HttpTransportSE(url, 30000);
                androidHttpTransport.call(SOAP_ACTION, envelope);

                // Got Webservice response
                result = envelope.getResponse().toString();
                Log.e(MethodName, result);
                endTimeMilliSceond = System.currentTimeMillis();
                Log.v("Time ", (endTimeMilliSceond - startTimeMillsceond) + "");
            } catch (Exception e) {
                return e.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
        return result;
    }

    public String getJSONFromSOAPWSWithBoolean(String MethodName,
                                               HashMap<Integer, HashMap<String, String>> map, String url,
                                               String keybool, boolean bool) {

        try {
            SOAP_ACTION = namespace + MethodName;

            // Adding values to request object
            request = new SoapObject(namespace, MethodName);

            Set<Integer> keys = map.keySet();
            for (Iterator<Integer> i = keys.iterator(); i.hasNext(); ) {
                Integer key = (Integer) i.next();
                HashMap<String, String> value = (HashMap<String, String>) map
                        .get(key);

                Set<String> keysmap = value.keySet();
                for (Iterator<String> j = keysmap.iterator(); j.hasNext(); ) {
                    String keyMapValue = (String) j.next();
                    String valueMap = (String) value.get(keyMapValue);
                    if (Constants.DATATYPE_INT == key) {
                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(Integer.class);
                        request.addProperty(pi);
                    } else if (Constants.DATATYPE_STRING == key) {
                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(String.class);
                        request.addProperty(pi);
                    } else if (Constants.DATATYPE_DOUBLE == key) {
                        System.out.println(keyMapValue + " = " + valueMap);
                        PropertyInfo pi = new PropertyInfo();
                        pi.setName(keyMapValue);
                        pi.setValue(valueMap);
                        pi.setType(Double.class);
                        request.addProperty(pi);
                    }
                }
            }

            PropertyInfo pi = new PropertyInfo();
            pi.setName(keybool);
            pi.setValue(bool);
            pi.setType(Boolean.class);
            request.addProperty(pi);

            Log.v("param bool", keybool + "=" + bool);
            SetEnvelope(url);

            try {

                String[] str = url.split("/");
                HttpTransportSE androidHttpTransport = new HttpTransportSE(url, 30000);

                // SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);
                Log.v("url", url);
                Log.v("methodName", MethodName);
                SOAP_ACTION = namespace + MethodName;

                // Got Webservice response
                String result = envelope.getResponse().toString();
                Log.v(MethodName, result);
                return result;
            } catch (Exception e) {
                return e.toString();
            }
        } catch (Exception e) {
            return e.toString();
        }
    }
}