package com.edusunsoft.erp.orataro.model;

public class MyPhoneChainModel {

	private String parent_name;
	private String parent_relation;
	private String parent_number,PhoneBookID,ContactTypeTerm,PriorityLevelTerm;

	public String getParent_name() {
		return parent_name;
	}

	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}

	public String getParent_relation() {
		return parent_relation;
	}

	public void setParent_relation(String parent_relation) {
		this.parent_relation = parent_relation;
	}

	public String getPhoneBookID() {
		return PhoneBookID;
	}

	public void setPhoneBookID(String phoneBookID) {
		PhoneBookID = phoneBookID;
	}

	public String getContactTypeTerm() {
		return ContactTypeTerm;
	}

	public void setContactTypeTerm(String contactTypeTerm) {
		ContactTypeTerm = contactTypeTerm;
	}

	public String getPriorityLevelTerm() {
		return PriorityLevelTerm;
	}

	public void setPriorityLevelTerm(String priorityLevelTerm) {
		PriorityLevelTerm = priorityLevelTerm;
	}

	public String getParent_number() {
		return parent_number;
	}

	public void setParent_number(String parent_number) {
		this.parent_number = parent_number;
	}

}
