package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

public class ViewParentsPageDetailsActivity extends Activity implements OnClickListener {

	private Context mContext;
	private ImageView img_back;
	private TextView txt_teacher_name, txt_pr_note;
	private Intent intent;
	private String pr_Teacher_name, pr_note;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parents_page_details);

		mContext = ViewParentsPageDetailsActivity.this;
		pr_Teacher_name = getIntent().getStringExtra("Pr_Teacher_name");
		pr_note = getIntent().getStringExtra("Pr_note");
		txt_teacher_name = (TextView) findViewById(R.id.txt_teacher_name);
		txt_pr_note = (TextView) findViewById(R.id.txt_pr_note);
		img_back = (ImageView) findViewById(R.id.img_back);
		txt_teacher_name.setText(pr_Teacher_name.replace("To ", "").replace(",", ""));
		txt_pr_note.setText(pr_note);
		img_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			finish();
			break;
		default:
			break;
		}
	}

}
