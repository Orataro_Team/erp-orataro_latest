package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.ListSelectionActivity;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.ClassworkAdapter;
import com.edusunsoft.erp.orataro.database.ClassWorkListModel;
import com.edusunsoft.erp.orataro.database.ClassworkListDataDao;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.loadmoreListView.PullAndLoadListView;
import com.edusunsoft.erp.orataro.loadmoreListView.PullToRefreshListView.OnRefreshListener;
import com.edusunsoft.erp.orataro.model.ClassWorkModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/*
 * Classwork List
 */
public class ClassWorkListFragment extends Fragment implements ResponseWebServices, RefreshListner {


    private static final String TAG = ClassWorkListFragment.class.getCanonicalName();
    PullAndLoadListView lvClassWork;
    private int[] teacher_img = {R.drawable.teacher_0, R.drawable.teacher_1, R.drawable.teacher_2};
    public static ClassworkAdapter adapter;
    private ImageView imgLeftHeader, imgRightHeader;
    private TextView txtHeader;
    private Context mContext;
    private LinearLayout ll_add_new;
    private TextView txt_nodatafound;
    private View view_header;

    // linearlayout for search
    LinearLayout searchlayout;
    private EditText edtTeacherName;

    // variable declaration for homeworklist from ofline database
    ClassworkListDataDao classworkListDataDao;
    private List<ClassWorkListModel> ClassworkList = new ArrayList<>();
    ClassWorkListModel classworkmodel = new ClassWorkListModel();
    public String ISDataExist = "";
    /*END*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View convertView = inflater.inflate(R.layout.classworklist, container, false);
        mContext = getActivity();
        lvClassWork = (PullAndLoadListView) convertView.findViewById(R.id.lv_classwork);
        ll_add_new = (LinearLayout) convertView.findViewById(R.id.ll_add_new);
        txt_nodatafound = (TextView) convertView.findViewById(R.id.txt_nodatafound);

        searchlayout = (LinearLayout) convertView.findViewById(R.id.searchlayout);
        edtTeacherName = (EditText) convertView.findViewById(R.id.edtsearchStudent);

        if (HomeWorkFragmentActivity.txt_header != null) {
            HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.Classwork) + " (" + Utility.GetFirstName(mContext) + ")");
            HomeWorkFragmentActivity.txt_header.setPadding(0, 0, 50, 0);
        }

        classworkListDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).classworkListDataDao();

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.CLASSWORK).getIsCreate()) {
                ll_add_new.setVisibility(View.VISIBLE);
            } else {
                ll_add_new.setVisibility(View.GONE);
            }
        } else {
            ll_add_new.setVisibility(View.GONE);
        }

        ll_add_new.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, ListSelectionActivity.class);
                i.putExtra("isFrom", ServiceResource.CLASSWORK_FLAG);
                startActivity(i);
                getActivity().overridePendingTransition(0, 0);

            }

        });

        Utility.ISLOADCLASSWORK = false;

        // Display HomeworkList from offline as well as online
        try {
            DisplayClassworkList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*END*/

        lvClassWork.setOnRefreshListener(new OnRefreshListener() {

            public void onRefresh() {

                // display classwork lisr for online as well as offline : Commented By Krishna : 02-07-2020
                try {
                    DisplayClassworkList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*END*/

            }

        });

        edtTeacherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);

            }
        });

        return convertView;

    }

    @Override
    public void onResume() {

        // display classwork lisr for online as well as offline : Commented By Krishna : 02-07-2020
        if (Utility.ISLOADCLASSWORK) {
            // Display HomeworkList from offline as well as online
            try {
                DisplayClassworkList();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        /*END*/
        } else {
        }

        super.onResume();

//        // display classwork lisr for online as well as offline : Commented By Krishna : 02-07-2020
//        try {
//            DisplayClassworkList();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        /*END*/

    }

    private void DisplayClassworkList() {
        // Commented By Krishna : 27-06-2020 - get homework from offline database.
        if (Utility.isNetworkAvailable(mContext)) {
            ClassworkList = classworkListDataDao.getClassworkList();
            if (ClassworkList.isEmpty() || ClassworkList.size() == 0 || ClassworkList == null) {
                ISDataExist = "true";
                classwork(true);
            } else {
                //set Adapter
                setClassworklistAdapter(ClassworkList);
                /*END*/
                classwork(false);
            }
        } else {
            ClassworkList = classworkListDataDao.getClassworkList();
            if (ClassworkList != null) {
                setClassworklistAdapter(ClassworkList);
            }
        }
    }

    private void setClassworklistAdapter(List<ClassWorkListModel> classworkList) {

        if (classworkList != null && classworkList.size() > 0) {

            searchlayout.setVisibility(View.VISIBLE);
            Collections.sort(classworkList, new SortedDate());
            Collections.reverse(classworkList);

            for (int i = 0; i < classworkList.size(); i++) {
                if (i != 0) {
                    String temp = classworkList.get(i - 1).getMonth();
                    if (temp.equalsIgnoreCase(classworkList.get(i).getMonth())) {
                        if (classworkList.get(i).getYear().equalsIgnoreCase(classworkList.get(i - 1).getYear())) {
                            classworkList.get(i).setVisibleMonth(false);
                        } else {
                            classworkList.get(i).setVisibleMonth(true);
                        }
                    } else {
                        classworkList.get(i).setVisibleMonth(true);
                    }
                } else {
                    classworkList.get(i).setVisibleMonth(true);
                }
            }
        }

        if (classworkList != null && classworkList.size() > 0) {
            adapter = new ClassworkAdapter(mContext, classworkList, this);
            lvClassWork.setAdapter(adapter);
            txt_nodatafound.setVisibility(View.GONE);
            lvClassWork.setVisibility(View.VISIBLE);
        } else {
            lvClassWork.setVisibility(View.GONE);
            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.ClassWorkNotAvailable));
        }

    }

    /**
     * getClasswork List webservice call
     */

    public void classwork(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));

        if (Utility.isTeacher(mContext)) {

            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, null));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, null));

        } else {

            arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));
            arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
            arrayList.add(new PropertyVo(ServiceResource.GRADEID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));

        }

        Log.d("getsdfxfg", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.CLASSWORK_METHODNAME, ServiceResource.CLASSWORK_URL);

    }

    public class SortedDate implements Comparator<ClassWorkListModel> {
        @Override
        public int compare(ClassWorkListModel o1, ClassWorkListModel o2) {

            return o1.getDateInMillisecond().compareTo(o2.getDateInMillisecond());

        }

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.CLASSWORK_METHODNAME.equalsIgnoreCase(methodName)) {
            //Log.d(TAG,result);
            parseClasswork(result, ServiceResource.UPDATE);
        }

    }

    @Override
    public void refresh(String methodName) {

        // display classwork lisr for online as well as offline : Commented By Krishna : 02-07-2020
        try {
            DisplayClassworkList();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void parseClasswork(String result, String update) {


        JSONArray jsonObj;
        classworkListDataDao.deleteClassworkListItem();

        try {

            Global.classWorkModels = new ArrayList<ClassWorkModel>();
            jsonObj = new JSONArray(result);

            for (int i = 0; i < jsonObj.length(); i++) {

                try {

                    ClassWorkModel model = new ClassWorkModel();
                    JSONObject innerObj = jsonObj.getJSONObject(i);

                    // parse homework list data
                    ParseClassworkList(innerObj);
                    /*END*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (classworkListDataDao.getClassworkList().size() > 0) {
                //set Adapter
                setClassworklistAdapter(classworkListDataDao.getClassworkList());
                /*END*/
            }

        } catch (JSONException e) {

            e.printStackTrace();

        }


    }

    private void ParseClassworkList(JSONObject innerObj) {

        String date = null;
        try {
            date = Utility.getDate(
                    Long.valueOf(innerObj.getString(ServiceResource.CLASSWORK_DATEOFCLASSWORK)
                            .replace("/Date(", "").replace(")/", "")),
                    "EEEE/dd/MMM/yyyy/");

            classworkmodel.setDateOfClassWork(date);

            try {

                String dateView = Utility.getDate(
                        Long.valueOf(innerObj.getString(ServiceResource.CLASSWORK_EXPECTINGDATEOFCOMPLETION)
                                .replace("/Date(", "").replace(")/", "")),
                        "dd MMM yyyy");
                classworkmodel.setExpectingDateOfCompletion(dateView);

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                String dateView2 = Utility.getDate(
                        Long.valueOf(innerObj.getString(ServiceResource.DATEOFCLASSWORK)
                                .replace("/Date(", "").replace(")/", "")),
                        "dd MMM yyyy");
                classworkmodel.setDateOfClassWork(dateView2);

            } catch (Exception e) {

                e.printStackTrace();

            }




            //Start Time and End Time set new data (key == strStartTime) for AM PM issue , changed by Hardik Kanak 19-12-2020
            String starttime = innerObj.getString(ServiceResource.CLASSWORK_STR_STARTTIME);
            String endtime = innerObj.getString(ServiceResource.CLASSWORK_STR_ENDTIME);

//            String starttime = Utility.getDate(
//                    Long.valueOf(innerObj.getString(ServiceResource.CLASSWORK_STARTTIME)
//                            .replace("/Date(", "").replace(")/", "")),
//                    "hh:mm a");
//
//            String endtime = Utility.getDate(
//                    Long.valueOf(innerObj.getString(ServiceResource.CLASSWORK_ENDTIME)
//                            .replace("/Date(", "").replace(")/", "")),
//                    "hh:mm a");


            String[] dateArray = date.split("/");
            if (dateArray != null && dateArray.length > 0) {

                Log.v("date", (dateArray[0] + "" + dateArray[1] + "" + dateArray[2]));
                classworkmodel.setDay(dateArray[1] + " " + dateArray[0].substring(0, 3));
                classworkmodel.setMonth(dateArray[2]);
                classworkmodel.setYear(dateArray[3]);

            }


            classworkmodel.setDateInMillisecond(innerObj.getString(ServiceResource.CLASSWORK_DATEOFCLASSWORK).replace("/Date(", "").replace(")/", ""));
            classworkmodel.setClassWorkID(innerObj.getString(ServiceResource.CLASSWORK_CLASSWORKID));
            classworkmodel.setClassWorkTitle(innerObj.getString(ServiceResource.CLASSWORK_TITLE));
            classworkmodel.setClassWorkDetails(innerObj.getString(ServiceResource.CLASSWORK_DETAIL));
            classworkmodel.setDivisionID(innerObj.getString(ServiceResource.CLASSWORK_DIVISIONID));
            classworkmodel.setEndTime(endtime);
            classworkmodel.setGradeID(innerObj.getString(ServiceResource.CLASSWORK_GRADEID));
            classworkmodel.setPostID(innerObj.getString(ServiceResource.CLASSWORK_POSTID));
            classworkmodel.setReferenceLink(innerObj.getString(ServiceResource.CLASSWORK_REFERENCELINK));
            classworkmodel.setStartTime(starttime);
            classworkmodel.setSubjectID(innerObj.getString(ServiceResource.CLASSWORK_SUBJECTID));
            classworkmodel.setSubjectName(innerObj.getString(ServiceResource.CLASSWORK_SUBJECTNAME));
            classworkmodel.setUserName(innerObj.getString(ServiceResource.CLASSWORK_USERNAME));
            classworkmodel.setTeacherID(innerObj.getString(ServiceResource.CLASSWORK_TEACHERID));
            classworkmodel.setTechearName(innerObj.getString(ServiceResource.CLASSWORK_TEACHERNAME));
            classworkmodel.setTeacherImg(innerObj.getString(ServiceResource.CLASSWORK_TEACHERPROFILEPIC));
            classworkmodel.setImgUrl(ServiceResource.BASE_IMG_URL + ServiceResource.DATAFILE + innerObj.getString(ServiceResource.PHOTOPARSE));
            classworkmodel.setGradeName(innerObj.getString(ServiceResource.GRADENAME));
            classworkmodel.setDivisionName(innerObj.getString(ServiceResource.DIVISIONNAME));
            classworkmodel.setStrDateOfClasswork(innerObj.getString(ServiceResource.STRDATEOFCLASSWORK));
            String isRead = innerObj.getString(ServiceResource.HOMEWORK_ISREAD);
            String isFinish = innerObj.getString(ServiceResource.HOMEWORK_ISCHECKED);
            Log.d("getisRead", isRead);
            Log.d("getisChecked", isFinish);

            if (isFinish.equalsIgnoreCase("true")) {
                classworkmodel.setCheck(true);
            } else {
                classworkmodel.setCheck(false);
            }
            if (isRead.equalsIgnoreCase("true")) {
                classworkmodel.setRead(true);
            } else {
                classworkmodel.setRead(false);
            }

            // insert record into database.
            classworkListDataDao.insertClassworkList(classworkmodel);
            /*END*/
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
