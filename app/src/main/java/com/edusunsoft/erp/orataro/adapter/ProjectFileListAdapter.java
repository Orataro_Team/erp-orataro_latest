package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ProjectFileModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class ProjectFileListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater layoutInfalater;
	private ArrayList<ProjectFileModel> projectFileList, copyList;
	private TextView txtUserName, txtFileName;
	private int isFile = 0;
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	public ProjectFileListAdapter(Context mContext, ArrayList<ProjectFileModel> projectFileList) {
		this.mContext = mContext;
		this.projectFileList = projectFileList;
		copyList = new ArrayList<ProjectFileModel>();
		copyList.addAll(copyList);
	}

	@Override
	public int getCount() {
		return projectFileList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.projectfilelistraw, parent, false);
		txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
		txtFileName = (TextView) convertView.findViewById(R.id.txtFileName);

		if (isFile == 0) {
			convertView.setBackgroundColor(colors[position % colors.length]);
		}

		if (Utility.isNull(projectFileList.get(position).getUsername())) {
			txtUserName.setText(projectFileList.get(position).getUsername());
		}
		if (Utility.isNull(projectFileList.get(position).getFilename())) {
			txtFileName.setText(projectFileList.get(position).getFilename());
		}

		return convertView;

	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		projectFileList.clear();
		if (charText.length() == 0) {
			projectFileList.addAll(copyList);
		} else {
			for (ProjectFileModel vo : copyList) {
				if (vo.getFilename().toLowerCase(Locale.getDefault()).contains(charText)) {
					projectFileList.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}
}
