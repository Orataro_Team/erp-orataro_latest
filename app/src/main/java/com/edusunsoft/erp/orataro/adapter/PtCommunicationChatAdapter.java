package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ChatModel;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;

public class PtCommunicationChatAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<ChatModel> list = new ArrayList<>();

	public PtCommunicationChatAdapter(Context context, ArrayList<ChatModel> list) {
		this.list = list;
		mContext = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.chatraw, null);
		LinearLayout ll_chat = (LinearLayout) convertView.findViewById(R.id.ll_chat);
		TextView txtchat = (TextView) convertView.findViewById(R.id.txtchat);
		TextView txttime = (TextView) convertView.findViewById(R.id.txttime);
		ImageView img_tick = (ImageView) convertView.findViewById(R.id.img_tick);
		if(list.get(position).isSelf()){
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
			params.gravity = Gravity.RIGHT;
			ll_chat.setLayoutParams(params);
			ll_chat.setBackgroundResource(R.drawable.in_message_bg);
		}else{
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
			params.gravity = Gravity.LEFT;
			ll_chat.setLayoutParams(params);
			ll_chat.setBackgroundResource(R.drawable.out_message_bg);
		}

		if(Utility.isTeacher(mContext)){
			if(list.get(position).getIsParentRead()){
				img_tick.setImageResource(R.drawable.tick_sky_blue);
			}else{
				img_tick.setImageResource(R.drawable.tick);
			}
		} else{
			if(list.get(position).getIsTeacherRead()){
				img_tick.setImageResource(R.drawable.tick_sky_blue);
			}else{
				img_tick.setImageResource(R.drawable.tick);
			}
		}

		txtchat.setText(list.get(position).getDetails());
		txttime.setText(list.get(position).getTime());
		return convertView;
	}

}
