package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.VideoListAdapter;
import com.edusunsoft.erp.orataro.model.PhotoModel;

import java.util.ArrayList;

public class VideoListActivity extends Activity implements OnClickListener {
	GridView grv_photos;
	ImageView img_back, iv_add_photos;

	PhotoModel photoModel;
	ArrayList<PhotoModel> photoModels;

	VideoListAdapter photo_ListAdapter;

	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		mContext = VideoListActivity.this;

		grv_photos = (GridView) findViewById(R.id.grv_video);

		img_back = (ImageView) findViewById(R.id.img_back);
		iv_add_photos = (ImageView) findViewById(R.id.iv_add_photos);

		AddFriendList();

		img_back.setOnClickListener(this);
		iv_add_photos.setOnClickListener(this);
	}

	private void AddFriendList() {

		photoModels = new ArrayList<PhotoModel>();

		photoModel = new PhotoModel();
		photoModel.setPhotos("cr_detail_chart_2.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("b7.png");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("sport.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("1.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("cul.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("g3.png");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("teacher_0.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("exam.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("2.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("teacher_1.png");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("3.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("4.jpg");
		photoModels.add(photoModel);

		photoModel = new PhotoModel();
		photoModel.setPhotos("teacher_2.jpg");
		photoModels.add(photoModel);

		photo_ListAdapter = new VideoListAdapter(mContext, photoModels);
		grv_photos.setAdapter(photo_ListAdapter);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			finish();
			break;
		case R.id.iv_add_friend:
			finish();
			break;
		default:
			break;
		}
	}

}
