package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.Interface.UpdateListner;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.MyPhoneChainModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.CustomDialog;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyPhoneChainAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context context;
    private TextView tv_name;
    private TextView tv_relation;
    private TextView tv_phone_no;
    private ImageView img_edit;
    private ImageView img_delete;
    private String spnTypeStr, spnPriortyStr, contactNameStr, contactDetailStr, contactNumberStr, editIdStr;
    private ArrayList<MyPhoneChainModel> myPhoneChainModels, copyList;
    private boolean isValid;
    private Dialog mEditDialog;
    private String message;
    private UpdateListner updateListner;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    public String Message;

    public MyPhoneChainAdapter(Context context, ArrayList<MyPhoneChainModel> myPhoneChainModels, UpdateListner updateListner) {
        this.context = context;
        this.myPhoneChainModels = myPhoneChainModels;
        copyList = new ArrayList<MyPhoneChainModel>();
        copyList.addAll(myPhoneChainModels);
        this.updateListner = updateListner;
    }

    @Override
    public int getCount() {
        return myPhoneChainModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.phone_chain_list, parent, false);
        tv_name = (TextView) convertView.findViewById(R.id.tv_name);
        tv_relation = (TextView) convertView.findViewById(R.id.tv_relation);
        tv_phone_no = (TextView) convertView.findViewById(R.id.tv_phone_no);
        img_edit = (ImageView) convertView.findViewById(R.id.img_edit);
        img_delete = (ImageView) convertView.findViewById(R.id.img_delete);
        convertView.setBackgroundColor(colors[position % colors.length]);
        tv_name.setText(myPhoneChainModels.get(position).getParent_name());
        tv_phone_no.setText(myPhoneChainModels.get(position).getParent_number());
        tv_relation.setText(myPhoneChainModels.get(position).getParent_relation());

        tv_phone_no.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                try {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + tv_phone_no.getText().toString()));
                    context.startActivity(callIntent);

                } catch (ActivityNotFoundException activityException) {

                    Log.e("Calling a Phone Number", "Call failed", activityException);

                }

            }

        });

        img_edit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mEditDialog = CustomDialog.ShowDialog(context, R.layout.dialog_add_phone_chain, true);

//                mEditDialog = new Dialog(context);
//                mEditDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                mEditDialog.getWindow().setFlags(
//                        WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                mEditDialog.getWindow().setBackgroundDrawableResource(
//                        android.R.color.transparent);
//                mEditDialog.setContentView(R.layout.dialog_add_phone_chain);
//                mEditDialog.setCancelable(true);
//                mEditDialog.show();

                TextView tv_header = (TextView) mEditDialog.findViewById(R.id.tv_header);
                final EditText edt_name = (EditText) mEditDialog.findViewById(R.id.edt_name);
                final EditText edt_relation = (EditText) mEditDialog.findViewById(R.id.edt_relation);
                final EditText edt_phone_no = (EditText) mEditDialog.findViewById(R.id.edt_phone_no);
                ImageView img_close = (ImageView) mEditDialog.findViewById(R.id.img_close);
                ImageView img_add = (ImageView) mEditDialog.findViewById(R.id.img_add);

                Spinner spn_type = (Spinner) mEditDialog.findViewById(R.id.spn_type);
                Spinner spn_priorty = (Spinner) mEditDialog.findViewById(R.id.spn_priority);

                List<String> type = new ArrayList<String>();
                type.add("Please Select Contact Type");
                type.add("Mobile");
                type.add("Home");
                type.add("Office");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, type);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spn_type.setAdapter(adapter);
                spn_type.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        if (position == 0) {
                            spnTypeStr = "";
                        } else {
                            spnTypeStr = parent.getItemAtPosition(position).toString();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                List<String> priorty = new ArrayList<String>();
                priorty.add("Please Select priority");
                priorty.add("Important");
                priorty.add("Normal");
                priorty.add("Not Important");
                ArrayAdapter<String> adapterPriorty = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, priorty);
                adapterPriorty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spn_priorty.setAdapter(adapterPriorty);
                spn_priorty.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        if (position == 0) {
                            spnPriortyStr = "";
                        } else {
                            spnPriortyStr = parent.getItemAtPosition(position).toString();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                tv_header.setText(context.getResources().getString(R.string.Editphone));
                edt_name.setText(myPhoneChainModels.get(position).getParent_name());
                edt_relation.setText(myPhoneChainModels.get(position).getParent_relation());
                edt_phone_no.setText(myPhoneChainModels.get(position).getParent_number());

                if (myPhoneChainModels.get(position).getContactTypeTerm().equalsIgnoreCase("Mobile")) {
                    spn_type.setSelection(1);
                    spnTypeStr = myPhoneChainModels.get(position).getContactTypeTerm();
                } else if (myPhoneChainModels.get(position).getContactTypeTerm().equalsIgnoreCase("Home")) {
                    spn_type.setSelection(2);
                    spnTypeStr = myPhoneChainModels.get(position).getContactTypeTerm();
                } else if (myPhoneChainModels.get(position).getContactTypeTerm().equalsIgnoreCase("Office")) {
                    spn_type.setSelection(3);
                    spnTypeStr = myPhoneChainModels.get(position).getContactTypeTerm();
                }

                if (myPhoneChainModels.get(position).getPriorityLevelTerm().equalsIgnoreCase("Important")) {
                    spn_priorty.setSelection(1);
                    spnPriortyStr = myPhoneChainModels.get(position).getPriorityLevelTerm();
                } else if (myPhoneChainModels.get(position).getPriorityLevelTerm().equalsIgnoreCase("Normal")) {
                    spn_priorty.setSelection(2);
                    spnPriortyStr = myPhoneChainModels.get(position).getPriorityLevelTerm();
                } else if (myPhoneChainModels.get(position).getPriorityLevelTerm().equalsIgnoreCase("Not Important")) {
                    spnPriortyStr = myPhoneChainModels.get(position).getPriorityLevelTerm();
                    spn_priorty.setSelection(3);
                }

                img_add.setImageResource(R.drawable.save);

                img_close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mEditDialog.dismiss();
                    }
                });

                img_add.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        editIdStr = myPhoneChainModels.get(position).getPhoneBookID();
                        contactNameStr = edt_name.getText().toString();
                        contactDetailStr = edt_relation.getText().toString();
                        contactNumberStr = edt_phone_no.getText().toString();
                        if (contactNameStr == null || contactNameStr.equalsIgnoreCase("")) {
                            toast(context.getResources().getString(R.string.PleaseEnterName));
                        } else if (contactDetailStr == null || contactDetailStr.equalsIgnoreCase("")) {
                            toast(context.getResources().getString(R.string.PleaseEnterRelation));
                        } else if (contactNumberStr == null || contactNumberStr.equalsIgnoreCase("")) {
                            toast(context.getResources().getString(R.string.PleaseEnterRelation));
                        } else if (contactNumberStr.length() > 9) {
                            toast(context.getResources().getString(R.string.PleaseEnterValidNum));
                        } else if (spnTypeStr == null || spnTypeStr.equalsIgnoreCase("")) {
                            toast(context.getResources().getString(R.string.PleaseSelectContactType));
                        } else if (spnPriortyStr == null || spnPriortyStr.equalsIgnoreCase("")) {
                            toast(context.getResources().getString(R.string.PleaseSelectpriority));
                        } else {
                            EditPhone();
                            mEditDialog.dismiss();
                        }
                        ;
                    }
                });

            }
        });

        img_delete.setOnClickListener(new OnClickListener() {

            private Dialog mDeleteDialog;

            @Override
            public void onClick(View v) {

                Constants.ForDialogStyle = "Logout";

                mDeleteDialog = CustomDialog.ShowDialog(context, R.layout.dialog_logout_password, true);

//                mDeleteDialog = new Dialog(context);
////                mDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                mDeleteDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
////                mDeleteDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
////                mDeleteDialog.setContentView(R.layout.dialog_logout_password);
////                mDeleteDialog.setCancelable(true);
////                mDeleteDialog.show();

                TextView tv_header = (TextView) mDeleteDialog.findViewById(R.id.tv_header);
                TextView tv_message = (TextView) mDeleteDialog.findViewById(R.id.tv_say_something);
                tv_header.setText(context.getResources().getString(R.string.Delete));
                tv_message.setText(context.getResources().getString(R.string.Areyousureyouwanttodelete));
                LinearLayout ll_submit = (LinearLayout) mDeleteDialog.findViewById(R.id.ll_submit);

                ll_submit.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (Utility.isNetworkAvailable(context)) {
                            MyPhoneChainDelete(myPhoneChainModels.get(position).getPhoneBookID());
                        } else {
                            Utility.showAlertDialog(context, context.getResources().getString(R.string.PleaseCheckyourinternetconnection),"Error");
                        }
                        mDeleteDialog.dismiss();
                    }
                });

                ImageView img_close = (ImageView) mDeleteDialog.findViewById(R.id.img_close);

                img_close.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDeleteDialog.dismiss();
                    }
                });

            }
        });

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myPhoneChainModels.clear();
        if (charText.length() == 0) {
            myPhoneChainModels.addAll(copyList);
        } else {
            for (MyPhoneChainModel vo : copyList) {
                if (vo.getParent_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myPhoneChainModels.add(vo);
                }
            }
        }
        this.notifyDataSetChanged();
    }

    public void toast(String text) {
        Utility.toast(context,text);
//        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public void MyPhoneChainDelete(String phoneId) {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_DELETE_PHONEBOOKID, phoneId));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.PHONEBOOK_DELETE_METHODNAME,
                ServiceResource.PHONEBOOK_URL);
    }

    public void EditPhone() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.EDITID, editIdStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTNAME, contactNameStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTDETAIL, contactDetailStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTNO, contactNumberStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_CONTACTTYPETERM, spnTypeStr));
        arrayList.add(new PropertyVo(ServiceResource.PHONEBOOK_ADD_PRIORITYLEVEL, spnPriortyStr));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(context).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(context).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(context).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(context).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.BEATCH_ID, null));
        new AsynsTaskClass(context, arrayList, true, this).execute(ServiceResource.PHONEBOOK_ADD_METHODNAME, ServiceResource.PHONEBOOK_URL);
    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.PHONEBOOK_DELETE_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.phonebookModels = new ArrayList<MyPhoneChainModel>();

                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    MyPhoneChainModel phonechainmodel = new MyPhoneChainModel();
                    phonechainmodel.setParent_name(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNAME));
                    phonechainmodel.setParent_number(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNO));
                    phonechainmodel.setParent_relation(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTDETAIL));
                    phonechainmodel.setPhoneBookID(innerObj.getString(ServiceResource.PHONEBOOK_PHONEBOOKID));
                    phonechainmodel.setPriorityLevelTerm(innerObj.getString(ServiceResource.PHONEBOOK_PRIORITYLEVELTERM));
                    phonechainmodel.setContactTypeTerm(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTTYPETERM));
                    Global.phonebookModels.add(phonechainmodel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            updateListner.Update();
        }

        if (ServiceResource.PHONEBOOK_METHODNAME.equalsIgnoreCase(methodName)) {
            JSONArray jsonObj;
            try {
                Global.phonebookModels = new ArrayList<MyPhoneChainModel>();

                jsonObj = new JSONArray(result);

                for (int i = 0; i < jsonObj.length(); i++) {
                    JSONObject innerObj = jsonObj.getJSONObject(i);
                    MyPhoneChainModel phonechainmodel = new MyPhoneChainModel();
                    phonechainmodel.setParent_name(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNAME));
                    phonechainmodel.setParent_number(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTNO));
                    phonechainmodel.setParent_relation(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTDETAIL));
                    phonechainmodel.setPhoneBookID(innerObj.getString(ServiceResource.PHONEBOOK_PHONEBOOKID));
                    phonechainmodel.setPriorityLevelTerm(innerObj.getString(ServiceResource.PHONEBOOK_PRIORITYLEVELTERM));
                    phonechainmodel.setContactTypeTerm(innerObj.getString(ServiceResource.PHONEBOOK_CONTACTTYPETERM));
                    Global.phonebookModels.add(phonechainmodel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (ServiceResource.PHONEBOOK_ADD_METHODNAME.equalsIgnoreCase(methodName)) {
            if (isValid) {
                toast(Message);
                mEditDialog.dismiss();
            } else {
                toast(Message);
            }
            updateListner.Update();
        }
    }

}
