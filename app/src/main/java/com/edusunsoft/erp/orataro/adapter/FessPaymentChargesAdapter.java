package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ButtonClick;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FeesPaymentCharges;
import com.edusunsoft.erp.orataro.model.FeesPaymentType;

import java.util.ArrayList;

/**
 * Created by admin on 24-05-2017.
 */

public class FessPaymentChargesAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<FeesPaymentType> listFeespaymenttype;
    private ArrayList<FeesPaymentCharges> listFeesscharges;
    private String DoubleAmountStr ="0";
    private ButtonClick buttonclick;

    public FessPaymentChargesAdapter(Context mContext, ArrayList<FeesPaymentType> listFeespaymenttype, String DoubleAmountStr, ButtonClick buttonclick){
        this.mContext = mContext;
        this.listFeespaymenttype =listFeespaymenttype;
        this.DoubleAmountStr = DoubleAmountStr;
        this.buttonclick = buttonclick;
    }

    @Override
    public int getCount() {
        return listFeespaymenttype.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInfalater.inflate(R.layout.feeslistraw, viewGroup, false);

        TextView txtfees = (TextView) view.findViewById(R.id.txtfees);
        TextView txtcharges = (TextView) view.findViewById(R.id.txtammount);
        final TextView txttotal = (TextView) view.findViewById(R.id.txtdue);
        TextView txtaction = (TextView) view.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) view.findViewById(R.id.imgremove);
        txtfees.setText(listFeespaymenttype.get(i).getAcctName());
       txtcharges.setText(listFeespaymenttype.get(i).getCharges());
        txttotal.setText(Double.valueOf(DoubleAmountStr)+ Double.valueOf(txtcharges.getText().toString())+"");
        txtaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String paymentMode = "NB";
                if(listFeespaymenttype.get(i).getAcctCode().equalsIgnoreCase("CRD")){
                    paymentMode = "CC";
                }else if(listFeespaymenttype.get(i).getAcctCode().equalsIgnoreCase("DBC")){
                    paymentMode = "DC";
                }else if(listFeespaymenttype.get(i).getAcctCode().equalsIgnoreCase("NB")){
                    paymentMode = "NB";
                }else{
                    paymentMode = "NB";
                }
                buttonclick.buttonClick(i,paymentMode, Double.valueOf(txttotal.getText().toString()));
            }
        });

        return view;
    }

}
