package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.AtteandanceModel;
import com.edusunsoft.erp.orataro.model.AttendanceModelABPS;
import com.edusunsoft.erp.orataro.services.ServiceResource;

import java.util.ArrayList;

public class AttendaceTeacherAdapter extends BaseAdapter {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private int[] colors = new int[]{Color.parseColor("#F2F2F2"), Color.parseColor("#FFFFFF")};
    private ArrayList<AtteandanceModel> atteandanceModel;
    private String date;

    public AttendaceTeacherAdapter(Context c, ArrayList<AtteandanceModel> atteandanceModel, String date) {
        this.mContext = c;
        this.atteandanceModel = atteandanceModel;
        this.date = date;
    }

    @Override
    public int getCount() {
        return atteandanceModel.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.attendancelistraw, parent, false);

        TextView txtrollnumber = (TextView) convertView.findViewById(R.id.txtrollnumber);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        RadioGroup rbd_group = (RadioGroup) convertView.findViewById(R.id.rbd_group);

        final RadioButton rbpresent = (RadioButton) convertView.findViewById(R.id.rbpresent);
        final RadioButton rbabsant = (RadioButton) convertView.findViewById(R.id.rbabsant);
        final RadioButton rbsikleave = (RadioButton) convertView.findViewById(R.id.rbsikleave);
        final RadioButton rbLeave = (RadioButton) convertView.findViewById(R.id.rbLeave);
//
        if (!atteandanceModel.get(position).getRollNo().equalsIgnoreCase("null")) {

            txtrollnumber.setText(atteandanceModel.get(position).getRollNo());

        } else {

            txtrollnumber.setText("");

        }

        txtName.setText(atteandanceModel.get(position).getFullName());
        AttendanceModelABPS abps = new AttendanceModelABPS(atteandanceModel.get(position).getMap().get(date));
        rbpresent.setChecked(abps.isPresent());

        rbabsant.setChecked(abps.isAbsant());
        rbsikleave.setChecked(abps.isSikleave());
        rbLeave.setChecked(abps.isLeave());

        if (rbpresent.isChecked()) {

            atteandanceModel.get(position).getMap().put(date, ServiceResource.PRESENT);

        }

        if (rbabsant.isChecked()) {

            atteandanceModel.get(position).getMap().put(date, ServiceResource.ABSENT);

        }

        if (rbsikleave.isChecked()) {

            atteandanceModel.get(position).getMap().put(date, ServiceResource.SICKLEAVE);

        }

        if (rbLeave.isChecked()) {

            atteandanceModel.get(position).getMap().put(date, ServiceResource.LEAVE);

        }


        rbd_group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (rbpresent.isChecked()) {
                    atteandanceModel.get(position).getMap().put(date, ServiceResource.PRESENT);
                } else if (rbabsant.isChecked()) {
                    atteandanceModel.get(position).getMap().put(date, ServiceResource.ABSENT);
                } else if (rbsikleave.isChecked()) {
                    atteandanceModel.get(position).getMap().put(date, ServiceResource.SICKLEAVE);
                } else if (rbLeave.isChecked()) {
                    atteandanceModel.get(position).getMap().put(date, ServiceResource.LEAVE);
                }
            }
        });

        convertView.setBackgroundColor(colors[position % colors.length]);
        return convertView;
    }

    public ArrayList<AtteandanceModel> getModifyAttendanceList() {

        return atteandanceModel;

    }

}
