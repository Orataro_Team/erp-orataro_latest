package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BlogListDataDao {

    @Insert
    void insertBlog(BlogListModel blogListModel);

    @Query("SELECT * from Blog")
    List<BlogListModel> getBlogList();

    @Query("DELETE  FROM Blog")
    int deleteBlogList();


}
