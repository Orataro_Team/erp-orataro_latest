package com.edusunsoft.erp.orataro.model;

import java.io.Serializable;

public class MyHappyGramModel implements Serializable {

    private String hg_date;
    private String RollNo;
    private String hg_note;
    private String hg_apprication;
    private boolean isVisibleMonth = false, isChecked = false;
    private String hg_month;
    private String year;
    private String SubjectID, SubjectName, MemberID, HappyGramID, Emotion, FullName, registationNo, dateInMillisecond;

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDateInMillisecond() {
        return dateInMillisecond;
    }

    public void setDateInMillisecond(String dateInMillisecond) {
        this.dateInMillisecond = dateInMillisecond;
    }

    public String getRegistationNo() {
        return registationNo;
    }

    public void setRegistationNo(String registationNo) {
        this.registationNo = registationNo;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getMemberID() {
        return MemberID;
    }

    public void setMemberID(String memberID) {
        MemberID = memberID;
    }

    public String getHappyGramID() {
        return HappyGramID;
    }

    public void setHappyGramID(String happyGramID) {
        HappyGramID = happyGramID;
    }

    public String getEmotion() {
        return Emotion;
    }

    public void setEmotion(String emotion) {
        Emotion = emotion;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getHg_date() {
        return hg_date;
    }

    public void setHg_date(String hg_date) {
        this.hg_date = hg_date;
    }

    public String getHg_note() {
        return hg_note;
    }

    public void setHg_note(String hg_note) {
        this.hg_note = hg_note;
    }

    public String getHg_apprication() {
        return hg_apprication;
    }

    public void setHg_apprication(String hg_apprication) {
        this.hg_apprication = hg_apprication;
    }

    public boolean isVisibleMonth() {
        return isVisibleMonth;
    }

    public void setVisibleMonth(boolean isVisibleMonth) {
        this.isVisibleMonth = isVisibleMonth;
    }

    public String getHg_month() {
        return hg_month;
    }

    public void setHg_month(String hg_month) {
        this.hg_month = hg_month;
    }

    
}
