package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.edusunsoft.erp.orataro.Interface.MyClickableSpan;
import com.edusunsoft.erp.orataro.Interface.Popup;
import com.edusunsoft.erp.orataro.Interface.RefreshListner;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddHappyGramActivity;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.Constants;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

public class MyHappyGramListAdapter extends BaseAdapter implements ResponseWebServices {

    private LayoutInflater layoutInfalater;
    private Context mContext;
    private TextView homeworkListSubject;
    private ImageView list_image;
    private TextView txt_month;
    private TextView txt_date, txt_hg_date, txt_hg_note, txt_hg_to_teacher;
    private LinearLayout ll_date, lyl_happygram;
    private LinearLayout ll_view, ll_date_img;
    private View firstLine;
    public ImageView btn_delete;
    private RefreshListner listner;
    public int posRemove;
    private int[] emojies = {R.drawable.emotion_01, R.drawable.emotion_02, R.drawable.emotion_03
            , R.drawable.emotion_04, R.drawable.emotion_05, R.drawable.emotion_06
            , R.drawable.emotion_07, R.drawable.emotion_08, R.drawable.emotion_09
            , R.drawable.emotion_10, R.drawable.emotion_11, R.drawable.emotion_12
            , R.drawable.emotion_13, R.drawable.emotion_14, R.drawable.emotion_15
            , R.drawable.emotion_16, R.drawable.emotion_17, R.drawable.emotion_18
            , R.drawable.emotion_19, R.drawable.emotion_20, R.drawable.emotion_21
            , R.drawable.emotion_22, R.drawable.emotion_23, R.drawable.emotion_24
            , R.drawable.emotion_25
    };

    private ArrayList<MyHappyGramModel> happyGramModels, copyList;
    private StdDivSubModel stdModel;
    private int[] colors = new int[]{Color.parseColor("#FFFFFF"),
            Color.parseColor("#F2F2F2")};
    private int[] colors_list = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private TextView txt_studentName;
    private ImageView img_emoiges;

    public MyHappyGramListAdapter(Context context, ArrayList<MyHappyGramModel> happyGramModels, StdDivSubModel stdmodel, RefreshListner refreshListner) {
        mContext = context;
        this.happyGramModels = happyGramModels;
        this.listner = refreshListner;
        this.stdModel = stdmodel;
        copyList = new ArrayList<MyHappyGramModel>();
        copyList.addAll(happyGramModels);
    }

    @Override
    public int getCount() {
        return happyGramModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInfalater.inflate(R.layout.happygram_listraw, parent, false);
        ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
        lyl_happygram = (LinearLayout) convertView.findViewById(R.id.lyl_happygram);
        list_image = (ImageView) convertView.findViewById(R.id.list_image);
        firstLine = convertView.findViewById(R.id.firstLine);
        txt_studentName = (TextView) convertView.findViewById(R.id.txt_studentName);
        txt_month = (TextView) convertView.findViewById(R.id.txt_month);
        txt_date = (TextView) convertView.findViewById(R.id.txt_date);
        txt_hg_date = (TextView) convertView.findViewById(R.id.txt_hg_date);
        txt_hg_note = (TextView) convertView.findViewById(R.id.txt_hg_note);
        txt_hg_to_teacher = (TextView) convertView.findViewById(R.id.txt_hg_apprication);
        img_emoiges = (ImageView) convertView.findViewById(R.id.img_emoiges);
        ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
        ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
        btn_delete = (ImageView) convertView.findViewById(R.id.btn_delete);

        if (Utility.isTeacher(mContext)) {
            if (Utility.ReadWriteSetting(ServiceResource.HappyGram).getIsDelete()) {
                btn_delete.setVisibility(View.VISIBLE);
            } else {
                btn_delete.setVisibility(View.GONE);
            }
        } else {
            btn_delete.setVisibility(View.GONE);
        }

        btn_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.deleteDialog(mContext, mContext.getResources().getString(R.string.Happygram), happyGramModels.get(position).getHg_apprication(), new Popup() {

                    @Override
                    public void deleteYes() {
                        posRemove = position;
                        deleteHappygram(happyGramModels.get(position).getHappyGramID(), position);
                    }

                    @Override
                    public void deleteNo() {
                    }

                });

            }

        });

        if (happyGramModels.get(position).isVisibleMonth() == true) {
            ll_date.setVisibility(View.VISIBLE);
        } else {
            ll_date.setVisibility(View.GONE);
        }

        if (happyGramModels.get(position).getEmotion() != null && !happyGramModels.get(position).getEmotion().equalsIgnoreCase("")) {
            img_emoiges.setImageResource(strngtointemojies(happyGramModels.get(position).getEmotion()));
        }

        txt_hg_date.setText(happyGramModels.get(position).getHg_date());
        txt_hg_to_teacher.setText(happyGramModels.get(position).getHg_apprication());
        txt_month.setText(" " + happyGramModels.get(position).getHg_month()
                + "\n" + happyGramModels.get(position).getYear());
        if (position == 0) {
            firstLine.setVisibility(View.INVISIBLE);
        }
        if (Utility.isTeacher(mContext)) {
            txt_studentName.setText(happyGramModels.get(position).getFullName());
            txt_studentName.setVisibility(View.VISIBLE);
        } else {
            txt_studentName.setText(happyGramModels.get(position).getSubjectName());
            txt_studentName.setVisibility(View.VISIBLE);
        }
        if (happyGramModels.get(position).getHg_note() != null) {

            if (happyGramModels.get(position).getHg_note().length() > Constants.CONTINUEREADINGSIZE) {
                String continueReadingStr = Constants.CONTINUEREADINGSTR;
                String tempText = happyGramModels.get(position).getHg_note()
                        .substring(0, Constants.CONTINUEREADINGSIZE)
                        + continueReadingStr;

                SpannableString text = new SpannableString(tempText);

                text.setSpan(
                        new ForegroundColorSpan(
                                Constants.CONTINUEREADINGTEXTCOLOR),
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                MyClickableSpan clickfor = new MyClickableSpan(
                        tempText.substring(Constants.CONTINUEREADINGSIZE, Constants.CONTINUEREADINGSIZEWITHCONTUNUEREADING)) {

                    @Override
                    public void onClick(View widget) {
                        Intent i = new Intent(mContext, AddHappyGramActivity.class);
                        i.putExtra("isEdit", true);
                        i.putExtra("model", happyGramModels.get(position));
                        mContext.startActivity(i);
                    }
                };

                text.setSpan(
                        clickfor,
                        Constants.CONTINUEREADINGSIZE,
                        Constants.CONTINUEREADINGSIZE
                                + continueReadingStr.length(), 0);

                txt_hg_note.setMovementMethod(LinkMovementMethod.getInstance());
                txt_hg_note.setText(text, BufferType.SPANNABLE);
            } else {
                txt_hg_note.setText(happyGramModels.get(position).getHg_note());
            }

        }

        lyl_happygram.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(mContext, AddHappyGramActivity.class);
                i.putExtra("isEdit", true);
                i.putExtra("stdmodel", stdModel);
                i.putExtra("model", happyGramModels.get(position));
                mContext.startActivity(i);
            }
        });

//        txt_hg_note.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(mContext, AddHappyGramActivity.class);
//                i.putExtra("isEdit", true);
//                i.putExtra("stdmodel", stdModel);
//                i.putExtra("model", happyGramModels.get(position));
//                mContext.startActivity(i);

//            }
//        });
//
//        convertView.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent i = new Intent(mContext, AddHappyGramActivity.class);
//                i.putExtra("isEdit", true);
//                i.putExtra("model", happyGramModels.get(position));
//                mContext.startActivity(i);
//
//            }
//
//        });

        return convertView;
    }

    private void deleteHappygram(String happyGramID, int position) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.HAPPYGRAM_ID, happyGramID));
        Log.d("RequestofHappygram", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, true, this, false).execute(ServiceResource.HAPPYGRAM_DELETEMETHODNAME, ServiceResource.HAPPYGRAM_URL);

    }

    public int strngtointemojies(String str) {

        if (str != null && !str.equalsIgnoreCase("") && !str.equalsIgnoreCase("null")) {
            int pos = 0;
            try {
                pos = Integer.valueOf(str.substring(str.length() - 2, str.length()));
            } catch (NumberFormatException e) {
                pos = Integer.valueOf(str.substring(str.length() - 1));
            }
            return emojies[pos - 1];
        } else {
            return emojies[0];
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        happyGramModels.clear();
        if (charText.length() == 0) {
            happyGramModels.addAll(copyList);
        } else {
            for (MyHappyGramModel vo : copyList) {
                if (vo.getHg_note().toLowerCase(Locale.getDefault()).contains(charText) || vo.getFullName().toLowerCase(Locale.getDefault()).contains(charText)
                        || vo.getHg_date().toLowerCase(Locale.getDefault()).contains(charText)) {
                    happyGramModels.add(vo);
                }

            }

        }

        this.notifyDataSetChanged();

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.HAPPYGRAM_DELETEMETHODNAME.equalsIgnoreCase(methodName)) {

            listner.refresh(methodName);
            Utility.showToast(mContext.getResources().getString(R.string.happygramDeletesmessage), mContext);

        }

    }

}
