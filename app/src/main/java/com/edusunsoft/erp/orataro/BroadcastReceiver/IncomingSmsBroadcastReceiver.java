package com.edusunsoft.erp.orataro.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.edusunsoft.erp.orataro.Interface.SMSListener;
import com.edusunsoft.erp.orataro.activities.RegisterActivity;

public class IncomingSmsBroadcastReceiver extends BroadcastReceiver {

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();
    private static SMSListener mListener;

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                String senderNum = "";
                String message = "";
                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    senderNum = phoneNumber;
                    message = currentMessage.getDisplayMessageBody();

//					Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);


                    // Show Alert


                } // end for loop

                if (message.contains("OTP of your ORATARO Account is")) {
                    if (RegisterActivity.edt_optnumber != null) {
                        String[] number = message.split(" ");
                        if (number != null && number.length >= 9) {

                            RegisterActivity.edt_optnumber.setText(number[6].replace(".", ""));

                        }
                    }
                } else if (message.contains("OTP for Registration of your ORATARO Account is")) {

                    if (RegisterActivity.edt_optnumber != null) {

                        String[] number = message.split(" ");

                        if (number != null && number.length >= 8) {

                            RegisterActivity.edt_optnumber.setText(number[8].replace(".", ""));

                        }

                    }

                }


            } // bundle is null

        } catch (Exception e) {
//			Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }

    public static void bindListener(SMSListener listener) {

        mListener = listener;
    }
}