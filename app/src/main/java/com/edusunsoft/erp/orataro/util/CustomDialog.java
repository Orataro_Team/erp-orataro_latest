package com.edusunsoft.erp.orataro.util;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

public class CustomDialog {

    public static Dialog dialog;

    public static Dialog ShowDialog(Context context, int view, boolean cancelable) {

        if (Constants.ForDialogStyle.equalsIgnoreCase("Logout")) {

            dialog = new Dialog(context);

        } else {

            dialog = new Dialog(context,
                    android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        }

        dialog
                .requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow()
                .setBackgroundDrawableResource(android.R.color.transparent);
        dialog
                .setContentView(view);
        dialog.setCancelable(cancelable);
        dialog.show();

        return dialog;

    }
}
