package com.edusunsoft.erp.orataro.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.Interface.OnRouteItemClickListener;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.RouteListAdapter;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.RouteListResModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.edusunsoft.erp.orataro.util.Utility.getCurrentDate;

public class RouteListActivity extends AppCompatActivity implements ResponseWebServices {

    private static final String TAG = "RouteListActivity";
    Context mContext;

    RecyclerView routeList;
    private TextView txtNoDataFound;

    ImageView imgLeftHeader, imgRightHeader;
    TextView txtHeader;
    //private int stYear, stDay, stMonth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);

        Initialization();

    }

    private void Initialization() {

        mContext = this;

        routeList = findViewById(R.id.routelist);
        txtNoDataFound = findViewById(R.id.txt_nodatafound);
        imgLeftHeader = findViewById(R.id.img_home);
        imgRightHeader = findViewById(R.id.img_menu);
        txtHeader = findViewById(R.id.header_text);

        try {
            txtHeader.setText(getResources().getString(R.string.routelist));
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgLeftHeader.setOnClickListener(v -> Utility.RedirectToDashboard(RouteListActivity.this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utility.isNetworkAvailable(mContext)) {
            getRouteList(true);
        }
    }

    private void getRouteList(boolean isViewPopup) {

        String CurrentDate = getCurrentDate("yyyy-MM-dd");

        ArrayList<PropertyVo> arrayList = new ArrayList<>();
        arrayList.add(new PropertyVo(ServiceResource.STUDENTBUSREGMASTERID,
                new UserSharedPrefrence(mContext).getLoginModel().getStudentBusRegID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.DATE,
                CurrentDate));

        Log.d("routeListRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.GetStudentWiseBusPickupAndDropInformation, ServiceResource.ROUTELIST_URL);

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Utility.RedirectToDashboard(RouteListActivity.this);

    }

    @Override
    public void response(String result, String methodName) {
        Log.d("getResulttransport", result);

        if (result.contains("[{\"message\":\"No Data Found\",\"success\":0}]")) {
            routeList.setAdapter(null);
            routeList.setVisibility(View.GONE);
            txtNoDataFound.setVisibility(View.VISIBLE);
        } else if (result.contains("[{\"message\":\"Server is not responding, Please Try after some time.\",\"success\":0}]")) {
            routeList.setAdapter(null);
            routeList.setVisibility(View.GONE);
            Utility.showToast("Server is not responding, Please Try after some time", mContext);
        } else {
            JSONObject resultJson = new JSONObject();
            try {
                resultJson.put("data", new JSONArray(result));
                RouteListResModel routListResModel = new Gson().fromJson(resultJson.toString(), RouteListResModel.class);
                RouteListAdapter routeListAdapter = new RouteListAdapter(routListResModel.getData(), onRouteItemClickListener);
                routeList.setAdapter(routeListAdapter);
                routeList.setVisibility(View.VISIBLE);
                txtNoDataFound.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
                routeList.setVisibility(View.GONE);
                txtNoDataFound.setVisibility(View.VISIBLE);
            }

        }

    }

    private OnRouteItemClickListener onRouteItemClickListener = data -> {
        Log.d(TAG, "onItemClick: " + data.getRouteName());
        startActivity(RouteMapActivity.getInstance(this, data));
    };
}
