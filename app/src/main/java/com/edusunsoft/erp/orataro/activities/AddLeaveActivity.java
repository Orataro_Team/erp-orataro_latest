package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.GroupMemberModel;
import com.edusunsoft.erp.orataro.model.LeaveListModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class AddLeaveActivity extends Activity implements OnClickListener, OnItemSelectedListener, ResponseWebServices {

    private Spinner spn_teacher;
    private Context mContext;
    private ImageView imgLeftheader, imgRightheader;
    private TextView txtHeader;
    private EditText edt_detail;
    private ArrayList<String> teacherList;
    private CheckBox chkPreApplication;
    private TextView txtstartDate, txtendDate;
    private boolean isEdit;
    private ArrayList<GroupMemberModel> groupMemberList;
    private int stYear, stDay, stMonth;
    private int enYear, enDay, enMonth;
    private LinearLayout ll_save;
    private LeaveListModel model;
    private int spnPosition;
    private TextView tv_approved;
    private LinearLayout ll_leave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_leave);

        mContext = this;
        model = (LeaveListModel) getIntent().getSerializableExtra("model");
        isEdit = getIntent().getBooleanExtra("isEdit", false);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        edt_detail = (EditText) findViewById(R.id.edt_detail);
        spn_teacher = (Spinner) findViewById(R.id.spn_teacher);
        chkPreApplication = (CheckBox) findViewById(R.id.chkPreApplication);
        txtstartDate = (TextView) findViewById(R.id.txtstartDate);
        txtendDate = (TextView) findViewById(R.id.txtendDate);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        tv_approved = (TextView) findViewById(R.id.tv_approved);
        ll_leave = (LinearLayout) findViewById(R.id.ll_leave);
        ll_leave.setVisibility(View.GONE);
        Calendar c = Calendar.getInstance();
        stYear = c.get(Calendar.YEAR);
        stMonth = c.get(Calendar.MONTH);
        stDay = c.get(Calendar.DAY_OF_MONTH);
        enYear = c.get(Calendar.YEAR);
        enMonth = c.get(Calendar.MONTH);
        enDay = c.get(Calendar.DAY_OF_MONTH);
        txtstartDate.setText(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));
        txtendDate.setText(Utility.getDate(System.currentTimeMillis(), "dd-MM-yyyy"));
        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);

        try {

            if (isEdit) {

                txtHeader.setText(getResources().getString(R.string.editleave) + " (" + Utility.GetFirstName(mContext) + ")");
            } else {
                txtHeader.setText(getResources().getString(R.string.addleave) + " (" + Utility.GetFirstName(mContext) + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtstartDate.setOnClickListener(this);
        txtendDate.setOnClickListener(this);
        imgLeftheader.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        spn_teacher.setOnItemSelectedListener(this);

        if (Utility.isNetworkAvailable(mContext)) {
            getWallAllTeacher();
        } else {
            Utility.showAlertDialog(mContext, getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
        }

    }

    private void fillData() {

        edt_detail.setText(model.getReasonForLeave());
        String sDate = model.getStartDate().replace("/Date(", "").replace(")/", "");
        txtstartDate.setText(Utility.getDate(Long.valueOf(sDate), "dd-MM-yyyy"));
        String eDate = model.getEndDate().replace("/Date(", "").replace(")/", "");
        txtendDate.setText(Utility.getDate(Long.valueOf(eDate), "dd-MM-yyyy"));
        chkPreApplication.setChecked(model.getIsPerApplication());
        teacherNameFromId(model.getApplicationToTeacherID());
        spn_teacher.setSelection(spnPosition);

        String[] sDateArray = Utility.getDate(Long.valueOf(sDate), "dd-MM-yyyy").split("-");
        stDay = Integer.valueOf(sDateArray[0]);
        stMonth = Integer.valueOf(sDateArray[1]);
        stYear = Integer.valueOf(sDateArray[2]);

        String[] eDateArray = Utility.getDate(Long.valueOf(eDate), "dd-MM-yyyy").split("-");
        enDay = Integer.valueOf(eDateArray[0]);
        enMonth = Integer.valueOf(eDateArray[1]);
        enYear = Integer.valueOf(eDateArray[2]);

        Calendar c = Calendar.getInstance();
        tv_approved.setText(model.getApplicationStatus_Term());

        Log.d("getLeaveStatus", model.getApplicationStatus_Term());

        if (model.getApplicationStatus_Term().equalsIgnoreCase("Approved")) {
            ll_save.setVisibility(View.GONE);
            editFalse();
        } else if (model.getApplicationStatus_Term().equalsIgnoreCase("Reject")) {
            ll_save.setVisibility(View.GONE);
            editFalse();
        } else if (Long.valueOf(model.getStartDate().replace("/Date(", "").replace(")/", "")) < c.getTimeInMillis() + 86400000) {
            ll_save.setVisibility(View.GONE);
            editFalse();
        }

        ll_leave.setVisibility(View.VISIBLE);

    }

    /***
     * get WAll All Teacher List
     *
     */
    public void getWallAllTeacher() {

        /*Commented By Krishna : 23-05-2019 Get Only ClasssTeacher List*/

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));

        Log.d("getTeacherRequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETCLASSTEACHERLISTNAMEANDMEMBERID_METHODNAME, ServiceResource.FRIENDS_URL);

        /*END*/


//        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
//        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
//        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
//        arrayList.add(new PropertyVo(ServiceResource.DIVISIONID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionId()));
//        arrayList.add(new PropertyVo(ServiceResource.STANDARDID, new UserSharedPrefrence(mContext).getLoginModel().getGradeId()));
//
//        Log.d("getTeacherRequest", arrayList.toString());
//
//        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME, ServiceResource.FRIENDS_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.WALLMEMBERS_METHODNAME.equalsIgnoreCase(methodName)
                || ServiceResource.GETCLASSTEACHERLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)
//                || ServiceResource.GETTEACHERLISTNAMEANDMEMBERID_METHODNAME.equalsIgnoreCase(methodName)
        ) {

            Utility.writeToFile(result, methodName, mContext);
            parsesMemberTeacher(result);

        } else if (ServiceResource.LEAVE_SAVEUPDATETODOS.equalsIgnoreCase(methodName)) {

            /*commented By Krishna : 05-02-2019 --- get Response Message from Array and Display Toast of that message*/

            JSONArray jsonObj;

            try {

                jsonObj = new JSONArray(result);
                JSONObject innerObj = jsonObj.getJSONObject(0);
                String message = innerObj.getString("message");
                Utility.toast(mContext, message);
                Intent i = new Intent();
                setResult(1, i);
                Utility.ISLOADLEAVELIST = true;
                finish();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*END*/

//            Intent i = new Intent();
//            setResult(1, i);
//            finish();

//            sendPushNotification(mContext);

        } else if (ServiceResource.SENDNOTIFICATION_METHODNAME.equalsIgnoreCase(methodName)) {

            Intent i = new Intent();
            setResult(1, i);
            finish();

        }

    }

    public void parsesMemberTeacher(String result) {
        try {
            JSONArray hJsonArray = new JSONArray(result);//jsonObj.getJSONArray(ServiceResource.TABLE);
            groupMemberList = new ArrayList<GroupMemberModel>();
            teacherList = new ArrayList<String>();
            GroupMemberModel model;
            for (int i = 0; i < hJsonArray.length(); i++) {
                JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                model = new GroupMemberModel();
                model.setMemberId(hJsonObject.getString(ServiceResource.MEMBERID));
                model.setMemberName(hJsonObject.getString(ServiceResource.FULLNAMEPARAM));
                teacherList.add(hJsonObject.getString(ServiceResource.FULLNAMEPARAM));
                groupMemberList.add(model);
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                    mContext, android.R.layout.simple_spinner_item,
                    teacherList);

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spn_teacher.setAdapter(dataAdapter);

            if (isEdit) {
                fillData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addleave() {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.USER_ID, new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, new UserSharedPrefrence(mContext).getLoginModel().getGradeID()));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, new UserSharedPrefrence(mContext).getLoginModel().getDivisionID()));
        arrayList.add(new PropertyVo(ServiceResource.STARTDATE, Utility.dateFormate(txtstartDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));
        arrayList.add(new PropertyVo(ServiceResource.ENDDATE, Utility.dateFormate(txtendDate.getText().toString(), "MM-dd-yyyy", "dd-MM-yyyy")));

        if (isEdit) {
            arrayList.add(new PropertyVo(ServiceResource.SCHOOLLEAVENOTEID, model.getSchoolLeaveNoteID()));
        } else {
            arrayList.add(new PropertyVo(ServiceResource.SCHOOLLEAVENOTEID, null));
        }


        arrayList.add(new PropertyVo(ServiceResource.POSTBYTYPE, new UserSharedPrefrence(mContext).getLoginModel().getPostByType()));
        arrayList.add(new PropertyVo(ServiceResource.LEAVE_TEACHERID, groupMemberList.get(spnPosition).getMemberId()));
        arrayList.add(new PropertyVo(ServiceResource.ISPREAPPLICATION, chkPreApplication.isChecked()));
        arrayList.add(new PropertyVo(ServiceResource.REASONFORLEAVE, edt_detail.getText().toString()));

        Log.d("addleaverequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.LEAVE_SAVEUPDATETODOS, ServiceResource.LEAVE_URL);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spnPosition = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.txtstartDate) {

            DatePickerDialog dpd = new DatePickerDialog(mContext,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                            txtstartDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }

                    }, stYear, stMonth, stDay);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            }

            dpd.show();

        } else if (v.getId() == R.id.txtendDate) {

            DatePickerDialog adpd = new DatePickerDialog(mContext,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // Display Selected date in textbox
                            txtendDate.setText(dayOfMonth + "-"
                                    + (monthOfYear + 1) + "-" + year);

                        }

                    }, enYear, enMonth, enDay);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                adpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            }

            adpd.show();

        } else if (v.getId() == R.id.ll_save) {

            if (Utility.isNetworkAvailable(mContext)) {

                if (edt_detail.getText().toString().equalsIgnoreCase("")) {

                    Utility.toast(mContext, "Reason Should not be Empty");

                } else if (Utility.dateToMilliSeconds(txtstartDate.getText().toString(), "dd-MM-yyyy") <= Utility.dateToMilliSeconds(txtendDate.getText().toString(), "dd-MM-yyyy")
                        && System.currentTimeMillis() - 86400000 < Utility.dateToMilliSeconds(txtstartDate.getText().toString(), "dd-MM-yyyy")) {

                    addleave();

                } else {

                    Utility.toast(mContext, getResources().getString(R.string.pleaseselectproperdata));

                }

            } else {

                Utility.showAlertDialog(mContext, mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");

            }

        } else if (v.getId() == R.id.img_home) {

            Utility.ISLOADLEAVELIST = false;
            finish();

        }


    }

    public String teacherNameFromId(String id) {

        String teacherName = "";
        if (groupMemberList != null && groupMemberList.size() > 0) {

            for (int i = 0; i < groupMemberList.size(); i++) {

                if (groupMemberList.get(i).getMemberId().equalsIgnoreCase(id)) {

                    teacherName = groupMemberList.get(i).getMemberName();
                    spnPosition = i;

                }

            }

        }

        return teacherName;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.ISLOADLEAVELIST = false;
        finish();
    }

    public void sendPushNotification(Context mContext) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();


        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SENDNOTIFICATION_METHODNAME,
                ServiceResource.NOTIFICATION_URL);

    }

    public void editFalse() {

        spn_teacher.setEnabled(false);
        edt_detail.setEnabled(false);
        txtstartDate.setEnabled(false);
        txtendDate.setEnabled(false);
        chkPreApplication.setEnabled(false);

    }

}
