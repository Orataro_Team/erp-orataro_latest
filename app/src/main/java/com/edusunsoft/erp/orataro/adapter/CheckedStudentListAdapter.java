package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.CheckedStudentModel;

import java.util.List;

/**
 * Created by admin on 16-05-2017.
 */

public class CheckedStudentListAdapter extends RecyclerView.Adapter<CheckedStudentListAdapter.MyViewHolder> {

    public List<CheckedStudentModel> studentList;
    public Context mcontext;
    public int selectedPosition;

    int getSelectedPosition;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView DateofExam, Subject, txt_roll_no, txt_contact_no, txt_isView, txt_checked, ExamType, txt_name;
        LinearLayout lyl_container;

        public MyViewHolder(View view) {

            super(view);

            getSelectedPosition = getLayoutPosition();

            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_roll_no = (TextView) view.findViewById(R.id.txt_rollno);
            txt_contact_no = (TextView) view.findViewById(R.id.txt_contact_no);
            txt_isView = (TextView) view.findViewById(R.id.txt_isView);
            txt_checked = (TextView) view.findViewById(R.id.txt_checked);
            DateofExam = (TextView) view.findViewById(R.id.txt_date);
            Subject = (TextView) view.findViewById(R.id.txt_subject);
            ExamType = (TextView) view.findViewById(R.id.txt_examtype);
            lyl_container = (LinearLayout) view.findViewById(R.id.lyl_container);

        }

    }


    public CheckedStudentListAdapter(Context mContext, List<CheckedStudentModel> studentList) {

        this.mcontext = mContext;
        this.studentList = studentList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checked_student_list_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        CheckedStudentModel studentModel = studentList.get(position);

        /*Fillup Detail with Validation */

        if (studentModel.getFullName().equalsIgnoreCase("") || studentModel.getFullName().equalsIgnoreCase("null") ||
                studentModel.getFullName().equalsIgnoreCase(null)) {
            holder.txt_name.setText("");
        } else {
            holder.txt_name.setText(studentModel.getFullName());
        }

        if (studentModel.getRollNo().equalsIgnoreCase("") || studentModel.getRollNo().equalsIgnoreCase("null") ||
                studentModel.getRollNo().equalsIgnoreCase(null)) {
            holder.txt_roll_no.setText("");
        } else {
            holder.txt_roll_no.setText(studentModel.getRollNo());
        }

        if (studentModel.getContactNo1().equalsIgnoreCase("") || studentModel.getContactNo1().equalsIgnoreCase("null") ||
                studentModel.getContactNo1().equalsIgnoreCase(null)) {
            holder.txt_contact_no.setText("");
        } else {
            holder.txt_contact_no.setText(studentModel.getContactNo1());
        }

        if (studentModel.isRead()) {
            holder.txt_isView.setText("Yes");
        } else {
            holder.txt_isView.setText("No");
        }

        if (studentModel.isApprove()) {
            holder.txt_checked.setText("Yes");
        } else {
            holder.txt_checked.setText("No");
        }

        if ((position % 2 == 0)) {

            holder.lyl_container.setBackgroundColor(mcontext.getResources().getColor(R.color.white));

        } else {

            holder.lyl_container.setBackgroundColor(mcontext.getResources().getColor(R.color.gray_color));

        }


        /*END*/

    }

    @Override
    public int getItemCount() {

        return studentList.size();

    }

}
