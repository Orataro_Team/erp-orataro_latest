package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;

public class MenuAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInfalater;
    private final int[] iconlist;
    private final String[] namelist;
    private boolean showText;
    private boolean UserGraycolor = false;
    private boolean mShowArrow[];
    private int[] colors = new int[]{Color.parseColor("#323B66"),
            Color.parseColor("#21294E")};
    private int[] colors2 = new int[]{Color.parseColor("#F2F2F2"),
            Color.parseColor("#FFFFFF")};
    private boolean showCount;
    private int count = 0;

    public MenuAdapter(Context ctx, int[] imagelist, String[] actionlist, boolean showText, boolean showCount) {
        this.context = ctx;
        this.iconlist = imagelist;
        this.namelist = actionlist;
        this.showText = showText;
        this.showCount = showCount;
        if (new UserSharedPrefrence(context).getISMULTIPLE()) {
            count = iconlist.length;
        } else {
            count = iconlist.length - 1;
        }

        mShowArrow = new boolean[imagelist.length];

        for (int i = 0; i < imagelist.length; i++) {

            mShowArrow[i] = false;

        }

    }

    public MenuAdapter(Context ctx, int[] imagelist, String[] actionlist, boolean showText, boolean[] showArraow, boolean showCount) {

        this.context = ctx;
        this.iconlist = imagelist;
        this.namelist = actionlist;
        this.showText = showText;
        this.showCount = showCount;
        mShowArrow = showArraow;
        count = iconlist.length;

    }

    public void setcolor(boolean gray_white) {
        UserGraycolor = gray_white;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return namelist.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolderItem();
            layoutInfalater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInfalater.inflate(R.layout.menushell, parent, false);

            viewHolder.menu_item = (ImageView) convertView.findViewById(R.id.menu_item);
            viewHolder.tv_menu = (TextView) convertView.findViewById(R.id.tv_menu);
            viewHolder.arrow = (ImageView) convertView.findViewById(R.id.img_arrow);
            viewHolder.txt_count = (TextView) convertView.findViewById(R.id.txt_count);
            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolderItem) convertView.getTag();

        }

        viewHolder.txt_count.setVisibility(View.GONE);
        viewHolder.tv_menu.setText(namelist[position].replace("_", " "));
        viewHolder.tv_menu.setVisibility(View.GONE);

        if (this.showText) {

            viewHolder.tv_menu.setVisibility(View.VISIBLE);

        }

        if (!showCount) {

            viewHolder.txt_count.setVisibility(View.GONE);

        }

        if (UserGraycolor) {

            int colorPos = position % colors2.length;
            convertView.setBackgroundColor(colors2[colorPos]);
            viewHolder.menu_item.setImageResource(iconlist[position]);
            viewHolder.menu_item.setTag(iconlist[position], namelist[position]);
            viewHolder.menu_item.setColorFilter(Color.parseColor("#27305B"));
            viewHolder.tv_menu.setTextColor(Color.BLACK);

            if (position == 14) {

                Log.d("getLeaveCount", new UserSharedPrefrence(context).getLEAVECOUNT());

                if (!new UserSharedPrefrence(context).getLEAVECOUNT().equalsIgnoreCase("0")) {

                    viewHolder.txt_count.setVisibility(View.VISIBLE);
                    viewHolder.txt_count.setText(new UserSharedPrefrence(context).getLEAVECOUNT());

                }

            }


        } else {

            int colorPos = position % colors.length;
            convertView.setBackgroundColor(colors[colorPos]);
            viewHolder.menu_item.setImageResource(iconlist[position]);
            viewHolder.menu_item.setTag(iconlist[position], namelist[position]);

        }

        if (mShowArrow[position]) {

            viewHolder.arrow.setVisibility(View.VISIBLE);

        } else {

            viewHolder.arrow.setVisibility(View.GONE);

        }

        if (position == 2) {

            /*commented By Krishna :  20-04-2019  Hide Notification Count of Wall */

//            if (!new UserSharedPrefrence(context).getLOGIN_NOTIFICATIONCOUNT().equalsIgnoreCase("0")) {
//                if (showCount) {
//                    viewHolder.txt_count.setVisibility(View.VISIBLE);
//                    viewHolder.txt_count.setText(new UserSharedPrefrence(context).getLOGIN_NOTIFICATIONCOUNT());
//                } else {
//                    viewHolder.txt_count.setVisibility(View.GONE);
//                }
//            }

            /*END*/
        }

        return convertView;
    }

    public static class ViewHolderItem {
        ImageView menu_item;
        TextView tv_menu, txt_count;
        ImageView arrow;
    }
}