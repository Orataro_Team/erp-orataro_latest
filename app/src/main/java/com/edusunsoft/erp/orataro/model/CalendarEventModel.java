package com.edusunsoft.erp.orataro.model;

public class CalendarEventModel {

	private String cl_date;
	private String cl_subject;
	private int cl_detail_img;
	private String cl_detail_txt;
	private String type;
	private String month;
	private boolean isVisibleMonth = false;
	private String cl_month;
	private String ActivityID;
	private String ReferenceLink;


	public String getReferenceLink() {
		return ReferenceLink;
	}

	public void setReferenceLink(String referenceLink) {
		ReferenceLink = referenceLink;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getActivityID() {
		return ActivityID;
	}

	public void setActivityID(String activityID) {
		ActivityID = activityID;
	}

	private String OnCreated,Year,UserName,EventID,ClientID,instituteID,ReminderDate,StartDate,EndDate,IsActive,UpdateBy;
	private String UpdateOn,CreateBy,CreateOn,SeqNo,DivisionID,GradeID,BeachID,subjectId;

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getOnCreated() {
		return OnCreated;
	}

	public void setOnCreated(String onCreated) {
		OnCreated = onCreated;
	}

	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		Year = year;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getEventID() {
		return EventID;
	}

	public void setEventID(String eventID) {
		EventID = eventID;
	}

	public String getClientID() {
		return ClientID;
	}

	public void setClientID(String clientID) {
		ClientID = clientID;
	}

	public String getInstituteID() {
		return instituteID;
	}

	public void setInstituteID(String instituteID) {
		this.instituteID = instituteID;
	}

	public String getReminderDate() {
		return ReminderDate;
	}

	public void setReminderDate(String reminderDate) {
		ReminderDate = reminderDate;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getIsActive() {
		return IsActive;
	}

	public void setIsActive(String isActive) {
		IsActive = isActive;
	}

	public String getUpdateBy() {
		return UpdateBy;
	}

	public void setUpdateBy(String updateBy) {
		UpdateBy = updateBy;
	}

	public String getUpdateOn() {
		return UpdateOn;
	}

	public void setUpdateOn(String updateOn) {
		UpdateOn = updateOn;
	}

	public String getCreateBy() {
		return CreateBy;
	}

	public void setCreateBy(String createBy) {
		CreateBy = createBy;
	}

	public String getCreateOn() {
		return CreateOn;
	}

	public void setCreateOn(String createOn) {
		CreateOn = createOn;
	}

	public String getSeqNo() {
		return SeqNo;
	}

	public void setSeqNo(String seqNo) {
		SeqNo = seqNo;
	}

	public String getDivisionID() {
		return DivisionID;
	}

	public void setDivisionID(String divisionID) {
		DivisionID = divisionID;
	}

	public String getGradeID() {
		return GradeID;
	}

	public void setGradeID(String gradeID) {
		GradeID = gradeID;
	}

	public String getBeachID() {
		return BeachID;
	}

	public void setBeachID(String beachID) {
		BeachID = beachID;
	}

	public int getCl_detail_img() {
		return cl_detail_img;
	}

	public void setCl_detail_img(int cl_detail_img) {
		this.cl_detail_img = cl_detail_img;
	}

	public String getCl_date() {
		return cl_date;
	}

	public void setCl_date(String cl_date) {
		this.cl_date = cl_date;
	}

	public String getCl_subject() {
		return cl_subject;
	}

	public void setCl_subject(String cl_subject) {
		this.cl_subject = cl_subject;
	}

	public String getCl_detail_txt() {
		return cl_detail_txt;
	}

	public void setCl_detail_txt(String cl_detail_txt) {
		this.cl_detail_txt = cl_detail_txt;
	}

	public boolean isVisibleMonth() {
		return isVisibleMonth;
	}

	public void setVisibleMonth(boolean isVisibleMonth) {
		this.isVisibleMonth = isVisibleMonth;
	}

	public String getCl_month() {
		return cl_month;
	}

	public void setCl_month(String cl_month) {
		this.cl_month = cl_month;
	}

	@Override
	public String toString() {
		return "CalendarEventModel{" +
				"cl_date='" + cl_date + '\'' +
				", cl_subject='" + cl_subject + '\'' +
				", cl_detail_img=" + cl_detail_img +
				", cl_detail_txt='" + cl_detail_txt + '\'' +
				", type='" + type + '\'' +
				", month='" + month + '\'' +
				", isVisibleMonth=" + isVisibleMonth +
				", cl_month='" + cl_month + '\'' +
				", OnCreated='" + OnCreated + '\'' +
				", Year='" + Year + '\'' +
				", UserName='" + UserName + '\'' +
				", EventID='" + EventID + '\'' +
				", ClientID='" + ClientID + '\'' +
				'}';
	}
}
