package com.edusunsoft.erp.orataro.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteStudentListModel {

    @SerializedName("data")
    private List<DataItem> data;

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public List<DataItem> getData() {
        return data;
    }

    public class DataItem {

        @SerializedName("Latitude")
        private double latitude;

        @SerializedName("PickupPointID")
        private String pickupPointID;

        @SerializedName("PointName")
        private String pointName;

        @SerializedName("Longitude")
        private double longitude;

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setPickupPointID(String pickupPointID) {
            this.pickupPointID = pickupPointID;
        }

        public String getPickupPointID() {
            return pickupPointID;
        }

        public void setPointName(String pointName) {
            this.pointName = pointName;
        }

        public String getPointName() {
            return pointName;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLongitude() {
            return longitude;
        }

        @Override
        public String toString() {
            return
                    "DataItem{" +
                            "latitude = '" + latitude + '\'' +
                            ",pickupPointID = '" + pickupPointID + '\'' +
                            ",pointName = '" + pointName + '\'' +
                            ",longitude = '" + longitude + '\'' +
                            "}";
        }
    }

    @Override
    public String toString() {
        return
                "RoutStudentListModel{" +
                        "data = '" + data + '\'' +
                        "}";
    }
}