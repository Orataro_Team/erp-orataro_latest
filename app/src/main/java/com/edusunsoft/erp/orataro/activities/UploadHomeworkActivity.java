package com.edusunsoft.erp.orataro.activities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.PhotoListAdapter;
import com.edusunsoft.erp.orataro.model.LoadedImage;
import com.edusunsoft.erp.orataro.model.PhotoModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.FileUtils;
import com.edusunsoft.erp.orataro.util.ImageAndroid11;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

import static com.edusunsoft.erp.orataro.util.Utility.showToast;

public class UploadHomeworkActivity extends AppCompatActivity implements View.OnClickListener, ResponseWebServices {

    TextView header_text;
    EditText et_album_name;
    LinearLayout ll_add_new;
    protected int REQUEST_CAMERA = 1;
    protected int SELECT_FILE = 2;
    private String fielPath, SelectedFilePath;
    private byte[] byteArray;
    private GridView gv_photo;

    private PhotoModel photoModel;
    private PhotoListAdapter photo_ListAdapter;
    private ArrayList<PhotoModel> photoModels_add;

    ImageView img_save;

    Context mContext;

    String ASSOCIATIONID = "";

    public static ArrayList<String> GetImagename = new ArrayList<>();
    Bitmap bitmap = null;
    String BASE64_STRING = "", Path = "";

    protected static final int REQUEST_PATH = 118;

    private Uri fileUri;
    private String filePath;
    private String curFileName;
    private String FileMineType = "TEXT";
    private int fileType = -1;
    private boolean ismp3 = false;
    final DecimalFormat format = new DecimalFormat("#.#");
    final long MB = 1024 * 1024;
    public String file_size;

    ImageView img_file, img_back;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_album);
        Initialization();
    }

    private void Initialization() {

        mContext = UploadHomeworkActivity.this;

        ASSOCIATIONID = getIntent().getStringExtra("AssociationID");

        header_text = (TextView) findViewById(R.id.header_text);
        header_text.setText(this.getResources().getString(R.string.upload_homework));
        et_album_name = (EditText) findViewById(R.id.et_album_name);
        et_album_name.setVisibility(View.GONE);
        et_album_name.setHint(this.getResources().getString(R.string.text_contains));

        gv_photo = (GridView) findViewById(R.id.gv_photo_album);
        img_file = (ImageView) findViewById(R.id.img_file);
        img_back = (ImageView) findViewById(R.id.img_back);

        ll_add_new = (LinearLayout) findViewById(R.id.ll_add_new);
        ll_add_new.setOnClickListener(this);

        photoModels_add = new ArrayList<PhotoModel>();

        img_save = (ImageView) findViewById(R.id.img_save);
        img_save.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ll_add_new:
                if (GetImagename.size() == 5) {
                    Utility.showToast("You can not select more than 5 images", mContext);
                } else {
                    selectImage();
                }


                break;
            case R.id.img_save:

                saveHomework();
                photoModels_add.clear();
                break;

            case R.id.img_back:
                if (GetImagename.size() > 0) {
                    GetImagename.clear();
                }
                finish();
                break;

        }

    }

    private void selectImage() {
        final CharSequence[] items = {this.getResources().getString(R.string.takephoto),
                this.getResources().getString(R.string.ChoosefromLibrary),
                this.getResources().getString(R.string.File),
                this.getResources().getString(R.string.Cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(this.getResources().getString(R.string.AddPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(UploadHomeworkActivity.this.getResources().getString(R.string.takephoto))) {

                    /*commented By Hardik : 29-04-2019 - Android 11 image capture*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        try {
                            startActivityForResult(takePictureIntent, ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11);
                        } catch (ActivityNotFoundException e) {
                            Log.e("PATH","PATH:: "+e.toString());
                        }
                    } else{
                        Intent intent = new Intent(UploadHomeworkActivity.this, PreviewImageActivity.class);
                        intent.putExtra("FromIntent", "true");
                        intent.putExtra("RequestCode", 100);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }

                } else if (items[item].equals(UploadHomeworkActivity.this.getResources().getString(R.string.ChoosefromLibrary))) {


                    /*commented By Hardik : 29-04-2019 - Android 11 image selection*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11);
                    } else{
                        Intent intent = new Intent(UploadHomeworkActivity.this, ImageSelectionActivity.class);
                        intent.putExtra("count", 5);
                        startActivityForResult(intent, SELECT_FILE);
                    }


                } else if (items[item].equals(mContext.getResources().getString(R.string.File))) {
                    // change file picker from filechooserActivity

                    /*commented By Hardik : 29-04-2019 - Android 11 select file*/
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.setType("*/*"); // for all file
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);

                        String[] mimeTypes = {
                                "application/pdf",
                                "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "text/plain"};
                        intent .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(intent, ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11);
                    } else{
                        //Old Code
//                        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                        chooseFile.setType("*/*");
//                        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
//                        startActivityForResult(chooseFile, REQUEST_PATH);

                        //By Hardik Kanak 27-06-2021
                        String[] mimeTypes = {
                                "application/pdf",
                                "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                "text/plain"};

                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("*/*");
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(intent, REQUEST_PATH);
                    }

                    /*END*/

                } else if (items[item].equals(UploadHomeworkActivity.this.getResources().getString(R.string.Cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //do your stuff
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_OK) {

            fileType = 0;

            if (requestCode == REQUEST_CAMERA) {

                try {

                    Utility.getUserModelData(this);

                    fielPath = data.getExtras().getString("imageData_uri");
                    Path = Utility.ImageCompress(fielPath, mContext);

                    File file = new File(fielPath);
                    File compressedImageFile = null;
                    try {
                        compressedImageFile = new Compressor(this).compressToFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());

//                    Utility.GetBASE64STRING(Path);

                    photoModel = new PhotoModel();
                    photoModel.setPhoto(Path);
                    photoModels_add.add(photoModel);
                    Log.d("photoadded", photoModels_add.toString());
                    photo_ListAdapter = new PhotoListAdapter(UploadHomeworkActivity.this, photoModels_add, 1);
                    gv_photo.setAdapter(photo_ListAdapter);

                    // upload Selected Image
                    uploadPhoto(fileType, Path);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*END*/

            } else if (requestCode == ImageAndroid11.REQUEST_IMAGE_CAPTURE_ANDROID_11) {

                try {
                    //handle Camera CAPTURE Image for ANDROID 11 device
                    if (data != null){
                        fileType = 0;
                        handleCaptureImage_Android11(data);

                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_IMAGE_ANDROID_11) {

                try {
                    //handle SELECTED image ANDROID 11 device
                    if (data != null){
                        fileType = 0;
                        handleImageAfterSelection_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == ImageAndroid11.REQUEST_PICK_FILE_ANDROID_11) {

                try {
                    if (data != null){
                        fileType = 0;
                        handleFileSelected_Android11(data);
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == SELECT_FILE) {

                try {
                    if (data != null) {

                        ArrayList<LoadedImage> imgList = data.getParcelableArrayListExtra("list");

                        fileType = 0;

                        for (int k = 0; k < imgList.size(); k++) {

                            fielPath = imgList.get(k).getUri().toString();

                            File file = new File(fielPath);
                            File compressedImageFile = null;
                            try {
                                compressedImageFile = new Compressor(this).compressToFile(file);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                            Path = Utility.ImageCompress(fielPath, mContext);
                            Utility.GetBASE64STRING(compressedImageFile.getAbsolutePath());
                            /*END*/

    //                        // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
    //                        Path = Utility.ImageCompress(fielPath, mContext);
    //                        Utility.GetBASE64STRING(Path);
    //                        /*END*/

    //                        photoModel = new PhotoModel();
    //                        photoModel.setPhoto(fielPath);
    //                        photoModels_add.add(photoModel);
    //
    //                        Log.d("photomodels", photoModels_add.toString());
    //                        photo_ListAdapter = new PhotoListAdapter(UploadHomeworkActivity.this, photoModels_add, 1);
    //                        gv_photo.setAdapter(photo_ListAdapter);

                            // upload Selected Image
                            uploadPhoto(fileType, Path);
                            /*END*/

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (requestCode == REQUEST_PATH) {
                try {
                    fileUri = data.getData();
                    filePath = fileUri.getPath();

                    System.out.println(fileUri.toString());

                    String selectdpath = FileUtils.getPath(UploadHomeworkActivity.this, fileUri);
                    Log.i("newImageFilePath", "" + selectdpath);

                    // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
                    Utility.convertToBase64String(mContext, fileUri);
                    /*END*/

                    File f = new File("" + fileUri);

                    curFileName = new File(selectdpath).getName();
                    Log.d("getcurclfilename", curFileName + " " + fileUri.toString());
                    Log.d("getfilename", filePath + " " + curFileName + " " + f.getName());

                    /*commented By : 13-03-2019 Get FileMimeType From Extension*/

                    File file = new File(curFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

                    /*END*/


//                fileType = 2;
//                if (curFileName.contains("pdf")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("txt")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("doc")) {
//                    FileMineType = mimeType;
//                } else if (curFileName.contains("mp3")) {
//                    FileMineType = mimeType;
//                    fileType = 1;
//                    ismp3 = true;
//                } else if (curFileName.contains("wav")) {
//                    FileMineType = mimeType;
//                    fileType = 1;
//                    ismp3 = true;
//                } else {
//                    FileMineType = mimeType;
//                }


                    //By Hardik Kanak 27-06-2021
                    fileType = 2;
                    String selectedFileExtension = FileUtils.getExtension(curFileName);
                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                    } else if (FileUtils.isMP3(selectedFileExtension)) { //Check mp3
                        FileMineType = mimeType;
                        fileType = 1;
                        ismp3 = true;
                    }else if (FileUtils.isWAV(selectedFileExtension)) { //Check waw
                        FileMineType = mimeType;
                        fileType = 1;
                        ismp3 = true;
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }

                    Log.d("filepathhhhhh", filePath);
//                // Commented By Krishna : 09-05-2020 for Image upload issue  and PNG image and to Upload Base64 file
//                Utility.GetBASE64STRINGFROMFILE(BitmapUtil.getRealPathFromURI(filePath, mContext));
//                /*END*/

                    /* commented by Krishna : 04-02-2019 Get Selected Video File Size*/

                    File file2 = new File(selectdpath);
                    final double length = file2.length();

                    if (length > MB) {

                        file_size = format.format(length / MB) + " MB";
                        Log.d("getFilesize", file_size);

                    }

                    if (Double.valueOf(format.format(length / MB)) > 20) { //file size change 5 to 20 by hardik_kanak 26-11-2020
                        Utility.toast(mContext, "Upload File with Maximum Size of 20 MB");
                    } else {
                        if (Utility.BASE64_STRING != null)
                            uploadPhoto(fileType, selectdpath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private void handleFileSelected_Android11(Intent data) {
        if (data.getData() != null) {

            try {
                Uri uri = data.getData();

                Log.e("PATH","data:: "+data.toString());
                Log.e("PATH","fileUri:: "+uri);

                String selectdpath = ImageAndroid11.getPathFromFileURL(this,uri);

                Log.e("PATH","PATH:: "+selectdpath);


                File file2 = new File(selectdpath);
                final double length = file2.length();

                if (length > MB) {

                    file_size = format.format(length / MB) + " MB";
                    Log.d("PATH", "SIZE:: "+file_size);

                }

                if (Double.valueOf(format.format(length / MB)) > 20) { //file size change 5 to 20 by hardik_kanak 26-11-2020
    //                Utility.toast(mContext, "Upload File with Maximum Size of 20 MB");
                    new androidx.appcompat.app.AlertDialog.Builder(this)
                            .setTitle(getString(R.string.alert))
                            .setMessage(getString(R.string.upload_file_alert))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {

                    Utility.convertToBase64String(mContext, uri);

                    File f = new File("" + uri);

                    String tempFileName = new File(selectdpath).getName();
                    Log.d("getcurclfilename", tempFileName + " " + uri.toString());
                    Log.d("getfilename", selectdpath + " " + tempFileName + " " + f.getName());

                    File file = new File(tempFileName);
                    Uri selectedUri = Uri.fromFile(file);
                    String fileExtension
                            = MimeTypeMap.getFileExtensionFromUrl(selectedUri.toString());
                    String mimeType
                            = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);

//                    fileType = 2;
//                    if (tempFileName.contains("pdf")) {
//                        FileMineType = mimeType;
//                    } else if (tempFileName.contains("txt")) {
//                        FileMineType = mimeType;
//                    } else if (tempFileName.contains("doc")) {
//                        FileMineType = mimeType;
//                    } else if (tempFileName.contains("mp3")) {
//                        FileMineType = mimeType;
//                        fileType = 1;
//                        ismp3 = true;
//                    } else if (tempFileName.contains("wav")) {
//                        FileMineType = mimeType;
//                        fileType = 1;
//                        ismp3 = true;
//                    } else {
//                        FileMineType = mimeType;
//                    }


                    fileType = 2;
                    String selectedFileExtension = FileUtils.getExtension(curFileName);
                    if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                        FileMineType = mimeType;
                    } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                        FileMineType = mimeType;
                    } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                        FileMineType = mimeType;
                    } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                        FileMineType = mimeType;
                    } else if (FileUtils.isMP3(selectedFileExtension)) { //Check mp3
                        FileMineType = mimeType;
                        fileType = 1;
                        ismp3 = true;
                    }else if (FileUtils.isWAV(selectedFileExtension)) { //Check waw
                        FileMineType = mimeType;
                        fileType = 1;
                        ismp3 = true;
                    } else {
                        showToast(getResources().getString(R.string.unknown_extension), this);
                    }

                    if (Utility.BASE64_STRING != null){
                        uploadPhoto(fileType, selectdpath);
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }

        }
    }

    private void handleCaptureImage_Android11(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");

        String path = ImageAndroid11.getPathFromBitmap(this,imageBitmap);

        //Must apply base 64 string function before upload photo
        if (path != null){
            Log.e("PATH","PATH:: "+path);
            Path = path;
            Utility.GetBASE64STRING(path);
            uploadPhoto(fileType, Path);
        } else  {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void handleImageAfterSelection_Android11(Intent data) {
        if (data.getData() != null){

            try {
                Uri selectedImage = data.getData();
                String path = ImageAndroid11.getPathFromURI(this,selectedImage);
                Log.d("PATH","PATH:: "+path);

                //Must apply base 64 string function before upload photo
                if (path != null){
                    Path = path;
                    Utility.GetBASE64STRING(path);
                    uploadPhoto(fileType, Path);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }


        } else  {
            if (data.getClipData() != null) {
                try {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();

                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        mArrayUri.add(uri);
                    }

                    for (int i = 0; i < mArrayUri.size(); i++) {
                        String path = ImageAndroid11.getPathFromURI(this,mArrayUri.get(i));
                        Log.d("PATH","PATH:: "+path);

                        //Must apply base 64 string function before upload photo
                        if (path != null){
                            Path = path;
                            Utility.GetBASE64STRING(path);
                            uploadPhoto(fileType, Path);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                            .show();
                }
            }
        }
    }

    public void uploadPhoto(int args, String filepathparam) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
//        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
//                new File(Utility.NewFileName).getName()));

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILENAME,
                new File(filepathparam).getName()));

        if (args == 0) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "IMAGE"));
        } else if (args == 2) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "FILE"));
        }

        arrayList.add(new PropertyVo(ServiceResource.PHOTO_FILE,
                Utility.BASE64_STRING));


        Log.d("getimagerequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.UPLOAD_HOMEWORK_METHODNAME3,
                ServiceResource.UPLOADHOMEWORK_URL);

    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.UPLOAD_HOMEWORK_METHODNAME3.equalsIgnoreCase(methodName)) {

            Log.d("uploadphotoresult", result);

            if (fileType == 0) {

//                if (photoModels_add != null && photoModels_add.size() > 0) {

                photoModels_add.clear();

                JSONArray arr = null;

                try {
                    arr = new JSONArray(result);
                    JSONObject jObj = arr.getJSONObject(0);
                    String message = jObj.getString("message");
                    GetImagename.add(message);

                    for (int i = 0; i < GetImagename.size(); i++) {

                        photoModel = new PhotoModel();
                        photoModel.setPhoto(ServiceResource.BASE_IMG_URL + GetImagename.get(i));
                        photoModels_add.add(photoModel);

                        Log.d("photomodels", photoModels_add.toString());
                        photo_ListAdapter = new PhotoListAdapter(UploadHomeworkActivity.this, photoModels_add, 1);
                        gv_photo.setAdapter(photo_ListAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                }
            } else {
                JSONArray arr = null;
                try {
                    arr = new JSONArray(result);
                    JSONObject jObj = arr.getJSONObject(0);
                    String message = jObj.getString("message");
                    GetImagename.add(message);

//                    photoModel = new PhotoModel();
//                    photoModel.setPhoto(ServiceResource.BASE_IMG_URL + GetImagename.toString());
//                    photoModels_add.add(photoModel);
//
//                    Log.d("photomodels", photoModels_add.toString());
//                    photo_ListAdapter = new PhotoListAdapter(UploadHomeworkActivity.this, photoModels_add, 1);
//                    gv_photo.setAdapter(photo_ListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (result != null){
                try {
                    JSONArray jsonArr = new JSONArray(result);

                    if (jsonArr.length() > 0){
                        String msg = "";
                        JSONObject json_data = new JSONObject();
                        for (int i = 0; i < jsonArr.length(); i++) {
                            json_data = jsonArr.getJSONObject(i);

                            msg = json_data.getString("message");

                        }


                        if (fileType != 0) {
                            gv_photo.setVisibility(View.GONE);
                            img_file.setVisibility(View.VISIBLE);
                            ll_add_new.setVisibility(View.GONE);

                            String selectedFileExtension = FileUtils.getExtension(msg);

                            Log.d("uploadphotoresult", selectedFileExtension);


                            if (FileUtils.isPDF(selectedFileExtension)) { // Check PDF
                                img_file.setImageResource(R.drawable.pdf);
                            } else if (FileUtils.isTXT(selectedFileExtension)) { //Check TXT
                                img_file.setImageResource(R.drawable.txt);
                            } else if (FileUtils.isDOC(selectedFileExtension)) { //Check Doc
                                img_file.setImageResource(R.drawable.doc);
                            } else if (FileUtils.isEXCEL(selectedFileExtension)) { //Check excel
                                img_file.setImageResource(R.drawable.excel_file);
                            } else {
                                showToast(getResources().getString(R.string.unknown_extension), this);
                            }


//                            if (FileMineType.equalsIgnoreCase("text/plain")) {
//                                img_file.setImageResource(R.drawable.txt);
//                            } else if (FileMineType.equalsIgnoreCase("application/msword")) {
//                                img_file.setImageResource(R.drawable.doc);
//                            } else if (FileMineType.equalsIgnoreCase("application/doc")) {
//                                img_file.setImageResource(R.drawable.doc);
//                            } else if (FileMineType.equalsIgnoreCase("application/pdf")) {
//                                img_file.setImageResource(R.drawable.pdf);
//                            } else if (FileMineType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
//                                img_file.setImageResource(R.drawable.excel_file);
//                            } else if (FileMineType.equalsIgnoreCase("application/vnd.openxmlform")) {
//                                img_file.setImageResource(R.drawable.excel_file);
//                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        } else if (ServiceResource.SUBMIT_HOMEWORK_METHODNAME.equals(methodName)) {
            Log.d("getResultttt", result);
            JSONArray arr = null;
            try {
                arr = new JSONArray(result);
                JSONObject jObj = arr.getJSONObject(0);
                String message = jObj.getString("message");
                if (message.equals("Enter Value In Text Contains OR Select Image")) {
                    Utility.toast(mContext,
                            message);
                } else if (message.equals("No Data Found\n" +
                        "\n" +
                        "\n")) {
                    Utility.toast(mContext,
                            message);
                } else {
                    if (GetImagename.size() > 0) {
                        GetImagename.clear();
                    }
                    Utility.toast(mContext,
                            message);
                    UploadHomeworkActivity.this.finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveHomework() {
        String imagenameList = GetImagename.toString();
        String Imagename = imagenameList.substring(1, imagenameList.length() - 1).replace(", ", ",");
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONID,
                ASSOCIATIONID));
        arrayList.add(new PropertyVo(ServiceResource.ASSOCIATIONTYPE,
                "Homework"));
        arrayList.add(new PropertyVo(ServiceResource.ALBUM_CLIENTID,
                new UserSharedPrefrence(mContext).getLoginModel().getClientID()));
        arrayList.add(new PropertyVo(ServiceResource.HOMEWORK_CREATED_BY,
                new UserSharedPrefrence(mContext).getLoginModel().getUserID()));
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID,
                new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));
        arrayList.add(new PropertyVo(ServiceResource.MEMBERID,
                new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));
        arrayList.add(new PropertyVo(ServiceResource.HOMEWORK_NOTE, "null"));
        arrayList.add(new PropertyVo(ServiceResource.HOMEWORK_TEXT_CONTAINS, ""));
        arrayList.add(new PropertyVo(ServiceResource.HOMEWORK_IMAGES_URL,
                Imagename));

        if (fileType == 0) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "IMAGE"));
        } else if (fileType == 2) {
            arrayList.add(new PropertyVo(ServiceResource.FILETYPE,
                    "FILE"));
        }

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.SUBMIT_HOMEWORK_METHODNAME,
                ServiceResource.UPLOADHOMEWORK_URL);

        Log.d("requestUploadhomework", arrayList.toString());

    }

}

