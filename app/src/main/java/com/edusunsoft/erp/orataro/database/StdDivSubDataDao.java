package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StdDivSubDataDao {

    @Insert
    void insertStdDivSub(StdDivSubModel stdDivSubModel);

    @Query("SELECT * from StdDivSub")
    List<StdDivSubModel> getStdDivSubList();

    @Query("DELETE  FROM StdDivSub where isLeave=:isFrom")
    int deleteStdDivSubItem(String isFrom);

    @Query("SELECT * from StdDivSub where isLeave=:isFrom")
    List<StdDivSubModel> getStdDivList(String isFrom);


//    @Query("delete from StdDivSub where AssignmentID=:assignementid")
//    void deleteHomeworkItemByAssignmentID(String assignementid);

}
