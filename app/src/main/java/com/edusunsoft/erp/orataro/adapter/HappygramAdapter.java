package com.edusunsoft.erp.orataro.adapter;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.MyHappyGramModel;
import com.edusunsoft.erp.orataro.util.CustomDialog;

import java.util.ArrayList;
import java.util.Locale;

public class HappygramAdapter extends RecyclerView.Adapter<HappygramAdapter.ViewHolder> {

    Context context;
    ArrayList<MyHappyGramModel> HappygramList = new ArrayList<>();
    ArrayList<MyHappyGramModel> copyList = new ArrayList<>();
    int selectedPosition;
    public Dialog mPoweroffDialog;

    private String[] emojiesClassName = {"emotion1", "emotion2", "emotion3", "emotion4", "emotion5",
            "emotion6", "emotion7", "emotion8", "emotion9", "emotion10",
            "emotion11", "emotion12", "emotion13", "emotion14", "emotion15",
            "emotion16", "emotion17", "emotion18", "emotion19", "emotion20",
            "emotion21", "emotion22", "emotion23", "emotion24", "emotion25"};

    private int[] emojies = {R.drawable.emotion_01, R.drawable.emotion_02, R.drawable.emotion_03
            , R.drawable.emotion_04, R.drawable.emotion_05, R.drawable.emotion_06
            , R.drawable.emotion_07, R.drawable.emotion_08, R.drawable.emotion_09
            , R.drawable.emotion_10, R.drawable.emotion_11, R.drawable.emotion_12
            , R.drawable.emotion_13, R.drawable.emotion_14, R.drawable.emotion_15
            , R.drawable.emotion_16, R.drawable.emotion_17, R.drawable.emotion_18
            , R.drawable.emotion_19, R.drawable.emotion_20, R.drawable.emotion_21
            , R.drawable.emotion_22, R.drawable.emotion_23, R.drawable.emotion_24
            , R.drawable.emotion_25
    };

    public HappygramAdapter(Context context, ArrayList<MyHappyGramModel> HappygramList) {

        {
            this.HappygramList = HappygramList;
            this.context = context;
            copyList.addAll(HappygramList);
        }

    }

    @NonNull
    @Override
    public HappygramAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the layout

        View happygramView = inflater.inflate(R.layout.studentlistraw,
                viewGroup, false);

        HappygramAdapter.ViewHolder viewHolder = new HappygramAdapter.ViewHolder(happygramView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull HappygramAdapter.ViewHolder viewHolder, int position) {

        MyHappyGramModel myListData = HappygramList.get(position);

        viewHolder.chkStudent.setVisibility(View.VISIBLE);
        if (myListData.getRollNo().equals("") || myListData.getRollNo().equals(null) || myListData.getRollNo().equals("null")) {
            viewHolder.txtGrno.setText("");
        } else {
            viewHolder.txtGrno.setText("R.No : " + myListData.getRollNo());
        }

        viewHolder.txtStudentName.setText(myListData.getFullName());
        viewHolder.edt_appretion.setText(myListData.getHg_apprication());
        viewHolder.edt_note.setText(myListData.getHg_note());

        if (HappygramList.get(position).getEmotion() != null && !HappygramList.get(position).getEmotion().equalsIgnoreCase("") && !HappygramList.get(position).getEmotion().equalsIgnoreCase("null")) {

            viewHolder.img_emojies.setImageResource(strngtointemojies(myListData.getEmotion()));
            viewHolder.img_emojies.setVisibility(View.VISIBLE);
            viewHolder.imgcross.setVisibility(View.VISIBLE);
        }

        viewHolder.ll_bottam.setVisibility(View.VISIBLE);
        viewHolder.edt_appretion.setVisibility(View.VISIBLE);

        if (HappygramList.get(position).isChecked()) {

            viewHolder.chkStudent.setChecked(true);

        } else {

            viewHolder.chkStudent.setChecked(false);

        }

    }

    @Override
    public int getItemCount() {

        return HappygramList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView, img_smile, img_emojies, imgcross;
        public TextView textView, txtGrno, txtStudentName;
        public RelativeLayout relativeLayout;
        public LinearLayout ll_bottam;
        public AutoCompleteTextView edt_appretion;
        public EditText edt_note;
        public CheckBox chkStudent;

        public ViewHolder(View itemView) {

            super(itemView);

            this.txtGrno = (TextView) itemView.findViewById(R.id.txtgrno);
            this.txtStudentName = (TextView) itemView.findViewById(R.id.txtstudentname);
            this.chkStudent = (CheckBox) itemView.findViewById(R.id.chkstudent);
            this.ll_bottam = (LinearLayout) itemView.findViewById(R.id.ll_bottom);
            this.img_smile = (ImageView) itemView.findViewById(R.id.img_smile);
            this.img_emojies = (ImageView) itemView.findViewById(R.id.img_emojies);
            this.edt_appretion = (AutoCompleteTextView) itemView.findViewById(R.id.edt_appretion);
            this.edt_note = (EditText) itemView.findViewById(R.id.edt_note);
            this.imgcross = (ImageView) itemView.findViewById(R.id.imgcross);

            selectedPosition = getLayoutPosition();

            this.chkStudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    selectedPosition = getLayoutPosition();

                    if (isChecked) {

                        HappygramList.get(selectedPosition).setChecked(true);

                    } else {

                        HappygramList.get(selectedPosition).setChecked(false);

                    }


                }


            });

            this.edt_appretion.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    selectedPosition = getLayoutPosition();

                    if (s.length() != 0) {

                        HappygramList.get(selectedPosition).setHg_apprication(s.toString());

                    }

                }

            });

            edt_note.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {


                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    if (s != null && s.length() != 0) {

                        HappygramList.get(selectedPosition).setHg_note(s.toString());

                    }

                }

            });

            img_smile.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    mPoweroffDialog = CustomDialog.ShowDialog(context, R.layout.dialog_emojies, true);

                    GridView grd_emojies = (GridView) mPoweroffDialog.findViewById(R.id.grd_emojies);
                    EmojiesAdapter emojieAdapter = new EmojiesAdapter(context);
                    grd_emojies.setAdapter(emojieAdapter);

                    grd_emojies.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                            img_emojies.setVisibility(View.VISIBLE);
                            imgcross.setVisibility(View.VISIBLE);
                            img_emojies.setImageResource(emojies[pos]);
                            HappygramList.get(selectedPosition).setEmotion(emojiesClassName[pos]);
                            mPoweroffDialog.dismiss();

                        }

                    });

                }

            });

            img_emojies.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    img_emojies.setVisibility(View.INVISIBLE);
                    imgcross.setVisibility(View.INVISIBLE);
                    HappygramList.get(selectedPosition).setEmotion("");
                }

            });


        }


    }

    public ArrayList<MyHappyGramModel> getSelectedStudentList() {

        ArrayList<MyHappyGramModel> seletedStudentList = new ArrayList<MyHappyGramModel>();

        if (HappygramList != null && HappygramList.size() > 0) {
            for (int i = 0; i < HappygramList.size(); i++) {
                if (HappygramList.get(i).isChecked()) {
                    seletedStudentList.add(HappygramList.get(i));
                    Log.d("getAddedCheckedList", seletedStudentList.toString());
                }
            }

            return seletedStudentList;

        } else {

            return null;

        }

    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        HappygramList.clear();
        if (charText.length() == 0) {
            HappygramList.addAll(copyList);
        } else {
            for (MyHappyGramModel vo : copyList) {
                if (vo.getFullName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    HappygramList.add(vo);
                }

            }

        }

        this.notifyDataSetChanged();

    }

    public int strngtointemojies(String str) {
        if (str != null && !str.equalsIgnoreCase("") && !str.equalsIgnoreCase("null")) {
            int pos = 0;
            try {
                pos = Integer.valueOf(str.substring(str.length() - 2, str.length()));
            } catch (NumberFormatException e) {
                pos = Integer.valueOf(str.substring(str.length() - 1, str.length()));
            }
            return emojies[pos - 1];
        } else {
            return emojies[0];
        }
    }

}
