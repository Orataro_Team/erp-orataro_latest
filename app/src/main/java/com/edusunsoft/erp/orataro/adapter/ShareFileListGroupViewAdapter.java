package com.edusunsoft.erp.orataro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.ShareFIleModel;

import java.util.ArrayList;
import java.util.Locale;

public class ShareFileListGroupViewAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater layoutInfalater;
	private LinearLayout ll_view;
	private ImageView iv_file_img;
	private TextView tv_filename;
	private TextView tv_fileDate;
	private TextView tv_fileSize;
	private TextView txt_month;
	private TextView txt_date;
	private LinearLayout ll_date, ll_date_img;
	private ArrayList<ShareFIleModel> shareFileList = new ArrayList<>();
	private ArrayList<ShareFIleModel> copyList = new ArrayList<>();
	
	private int[] colors = new int[] { Color.parseColor("#FFFFFF"),
			Color.parseColor("#F2F2F2") };

	private int[] colors_list = new int[] { Color.parseColor("#323B66"),
			Color.parseColor("#21294E") };

	public ShareFileListGroupViewAdapter(Context context, ArrayList<ShareFIleModel> list) {
		this.mContext = context;
		this.shareFileList = list;
		copyList.addAll(list);
	}

	@Override
	public int getCount() {
		return shareFileList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = layoutInfalater.inflate(R.layout.filelistraw, parent, false);
		iv_file_img = (ImageView) convertView.findViewById(R.id.iv_file_img);
		tv_filename = (TextView) convertView.findViewById(R.id.tv_filename);
		tv_fileDate = (TextView) convertView.findViewById(R.id.tv_fileDate);
		tv_fileSize = (TextView) convertView.findViewById(R.id.tv_fileSize);
		txt_month = (TextView) convertView.findViewById(R.id.txt_month);
		txt_date = (TextView) convertView.findViewById(R.id.txt_date);
		ll_view = (LinearLayout) convertView.findViewById(R.id.ll_view);
		ll_date = (LinearLayout) convertView.findViewById(R.id.ll_date);
		ll_date_img = (LinearLayout) convertView.findViewById(R.id.ll_date_img);
		convertView.setBackgroundColor(colors[position % colors.length]);
		ll_view.setBackgroundColor(colors_list[position % colors_list.length]);

		if (shareFileList.get(position).isChnageMonth()) {
			ll_date.setVisibility(View.VISIBLE);
			if (position == 1) {
				txt_month.setText("MAY");
			}
			if (position == 3) {
				txt_month.setText("APRIL");
			}
		} else {
			ll_date.setVisibility(View.GONE);
		}

		if (shareFileList.get(position).getFileName() != null) {
			tv_filename.setText(shareFileList.get(position).getFileName());
		}
		if (shareFileList.get(position).getFileSize() != null) {
			tv_fileSize.setText(shareFileList.get(position).getFileSize());
		}
		if (shareFileList.get(position).getFileDate() != null) {
			tv_fileDate.setText(shareFileList.get(position).getFileDate());
		}

		return convertView;

	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		shareFileList.clear();
		if (charText.length() == 0) {
			shareFileList.addAll(copyList);
		} else {

			for (ShareFIleModel vo : copyList) {
				if (vo.getFileName().toLowerCase(Locale.getDefault()).contains(charText)) {
					shareFileList.add(vo);
				}
			}
		}
		this.notifyDataSetChanged();
	}

}
