package com.edusunsoft.erp.orataro.model;

public class ExamTimingModel {

    String StdExaScheduleID,StartDate,EndDate,DateOfExam,ExamDayName,SubjectName,PaperTypeName,StartTime,EndTime,ExamMasterID,ExamName;

    public String getStdExaScheduleID() {
        return StdExaScheduleID;
    }

    public void setStdExaScheduleID(String stdExaScheduleID) {
        StdExaScheduleID = stdExaScheduleID;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getDateOfExam() {
        return DateOfExam;
    }

    public void setDateOfExam(String dateOfExam) {
        DateOfExam = dateOfExam;
    }

    public String getExamDayName() {
        return ExamDayName;
    }

    public void setExamDayName(String examDayName) {
        ExamDayName = examDayName;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getExamMasterID() {
        return ExamMasterID;
    }

    public void setExamMasterID(String examMasterID) {
        ExamMasterID = examMasterID;
    }

    public String getExamName() {
        return ExamName;
    }

    public void setExamName(String examName) {
        ExamName = examName;
    }

    @Override
    public String toString() {
        return "ExamTimingModel{" +
                "StdExaScheduleID='" + StdExaScheduleID + '\'' +
                ", StartDate='" + StartDate + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", DateOfExam='" + DateOfExam + '\'' +
                ", ExamDayName='" + ExamDayName + '\'' +
                ", SubjectName='" + SubjectName + '\'' +
                ", PaperTypeName='" + PaperTypeName + '\'' +
                ", StartTime='" + StartTime + '\'' +
                ", EndTime='" + EndTime + '\'' +
                ", ExamMasterID='" + ExamMasterID + '\'' +
                ", ExamName='" + ExamName + '\'' +
                '}';
    }
}
