package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.StudentListGroupAdapter;
import com.edusunsoft.erp.orataro.model.StudentModel;

import java.util.ArrayList;
import java.util.Locale;

public class ViewStudentListActivity extends Activity implements OnClickListener {

	private Context mContext;
	private ImageView img_back;
	private ListView lst_students;
	private StudentListGroupAdapter studentListGroupAdapter;
	private ArrayList<StudentModel> studentList;
	private EditText edtTeacherName;
	private StudentListGroupAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student_list);
		mContext = ViewStudentListActivity.this;
		img_back = (ImageView) findViewById(R.id.img_back);
		edtTeacherName = (EditText) findViewById(R.id.edtsearchStudent);
		lst_students = (ListView) findViewById(R.id.lst_students);
		if (studentList != null && studentList.size() > 0) {
			adapter = new StudentListGroupAdapter(mContext, studentList);
			lst_students.setAdapter(adapter);
		}

		img_back.setOnClickListener(this);
		edtTeacherName.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				String text = edtTeacherName.getText().toString().toLowerCase(Locale.getDefault());
				adapter.filter(text);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			finish();
			break;
		default:
			break;
		}
	}
}
