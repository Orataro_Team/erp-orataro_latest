package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.adapter.StudentInformationAdapter;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.model.StudentModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;

public class StudentInformationActivity extends Activity implements ResponseWebServices, OnClickListener {

    ImageView imgLeft;
    TextView txtHeader, txtnodata;
    Context mContext;
    StandardModel model;
    ListView lv_studentInformation;
    StudentInformationAdapter studentinformationadapter;
    ArrayList<StudentModel> list;
    ImageView img_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade__wall);

        mContext = StudentInformationActivity.this;

        model = (StandardModel) getIntent().getSerializableExtra("model");

        imgLeft = (ImageView) findViewById(R.id.img_back);
        txtHeader = (TextView) findViewById(R.id.header_text);
        txtnodata = (TextView) findViewById(R.id.txtnodata);
        lv_studentInformation = (ListView) findViewById(R.id.lst_grade_wall);
        img_back = (ImageView) findViewById(R.id.img_back);


        txtHeader.setText(getResources().getString(R.string.studentinformation) + " (" + Utility.GetFirstName(mContext) + ")");

        StudenInformationList();

        img_back.setOnClickListener(this);

    }

    public void StudenInformationList() {
        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Utility.getUserModelData(mContext);
        }
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.BATCHID, new UserSharedPrefrence(mContext).getLoginModel().getBatchID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.GradeID, model.getStandrdId()));
        arrayList.add(new PropertyVo(ServiceResource.DivisionID, model.getDivisionId()));

        new AsynsTaskClass(mContext, arrayList, true, this).execute(ServiceResource.MEMBERS_GETSTUDENTLISTFORTEACHERLISTINGONLY, ServiceResource.ATTENDANCE_URL);


    }

    @Override
    public void response(String result, String methodName) {

        if (ServiceResource.MEMBERS_GETSTUDENTLISTFORTEACHERLISTINGONLY.equalsIgnoreCase(methodName)) {

            JSONArray jsonObj;
            try {
                jsonObj = new JSONArray(result);

                list = new ArrayList<StudentModel>();
                if (result.contains("No data")) {

                } else {
                    for (int i = 0; i < jsonObj.length(); i++) {
                        StudentModel model = new StudentModel();
                        JSONObject innerObj = jsonObj.getJSONObject(i);

                        model.setMemberID(innerObj
                                .getString(ServiceResource.MemberID));
                        model.setFullname(innerObj
                                .getString(ServiceResource.SFULLNAME));
                        model.setRegistrationNo(innerObj
                                .getString(ServiceResource.REGISTRATIONNO));
                        model.setRollNo(innerObj
                                .getString(ServiceResource.RollNo));
                        model.setSContactNo(innerObj
                                .getString(ServiceResource.SCONTACTNO));
                        model.setMFullName(innerObj
                                .getString(ServiceResource.MFULLNAME));

                        model.setMContactNo(innerObj
                                .getString(ServiceResource.MCONTACTNO));
                        model.setFFullName(innerObj
                                .getString(ServiceResource.FFULLNAME));
                        model.setFContactNo(innerObj
                                .getString(ServiceResource.FCONTACTNO));
                        model.setPresent(innerObj
                                .getString(ServiceResource.Present));
                        model.setProfilePic(innerObj
                                .getString(ServiceResource.ProfilePicture));
                        model.setAbsent(innerObj
                                .getString(ServiceResource.ABSENT));
                        list.add(model);

                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (list != null && list.size() > 0) {

//				Collections.sort(list, comparator);

                studentinformationadapter = new StudentInformationAdapter(mContext, list);
                lv_studentInformation.setAdapter(studentinformationadapter);
                txtnodata.setVisibility(View.GONE);
                lv_studentInformation.setVisibility(View.VISIBLE);
            } else {
                lv_studentInformation.setVisibility(View.GONE);
                txtnodata.setVisibility(View.VISIBLE);
                txtnodata.setText(getResources().getString(R.string.nostudentavailable));
            }
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_back) {
            finish();
        }
    }    // TODO Auto-generated method stub

    public class SortName implements Comparator<StudentModel> {
        @Override
        public int compare(StudentModel o1, StudentModel o2) {
            return o1.getFullname().compareTo(o2.getFullname());
        }
    }

}
