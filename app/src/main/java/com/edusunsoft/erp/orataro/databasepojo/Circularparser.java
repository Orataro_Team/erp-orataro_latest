package com.edusunsoft.erp.orataro.databasepojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Circularparser implements Parcelable {

    @SerializedName("CircularID")
    @Expose
    private String circularID;
    @SerializedName("TeacherName")
    @Expose
    private String teacherName;
    @SerializedName("DateOfCircular")
    @Expose
    private String dateOfCircular;
    @SerializedName("GradeID")
    @Expose
    private String gradeID;
    @SerializedName("CircularTitle")
    @Expose
    private String circularTitle;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("CircularDetails")
    @Expose
    private String circularDetails;
    @SerializedName("CircularTypeTerm")
    @Expose
    private String circularTypeTerm;
    @SerializedName("TeacherID")
    @Expose
    private String teacherID;
    @SerializedName("ProfilePic")
    @Expose
    private String profilePic;
    @SerializedName("DivisionID")
    @Expose
    private String divisionID;
    @SerializedName("SubjectID")
    @Expose
    private String subjectID;
    @SerializedName("IsRead")
    @Expose
    private String isRead;
    @SerializedName("SeqNo")
    @Expose
    private String seqNo;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("IsApproved")
    @Expose
    private String isApproved;
    @SerializedName("Photo")
    @Expose
    private String photo;
    public final static Parcelable.Creator<Circularparser> CREATOR = new Creator<Circularparser>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Circularparser createFromParcel(Parcel in) {
            Circularparser instance = new Circularparser();
            instance.circularID = ((String) in.readValue((String.class.getClassLoader())));
            instance.teacherName = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfCircular = ((String) in.readValue((String.class.getClassLoader())));
            instance.gradeID = ((String) in.readValue((String.class.getClassLoader())));
            instance.circularTitle = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileType = ((String) in.readValue((String.class.getClassLoader())));
            instance.circularDetails = ((String) in.readValue((String.class.getClassLoader())));
            instance.circularTypeTerm = ((String) in.readValue((String.class.getClassLoader())));
            instance.teacherID = ((String) in.readValue((String.class.getClassLoader())));
            instance.profilePic = ((String) in.readValue((String.class.getClassLoader())));
            instance.divisionID = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectID = ((String) in.readValue((String.class.getClassLoader())));
            instance.isRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.seqNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectName = ((String) in.readValue((String.class.getClassLoader())));
            instance.isApproved = ((String) in.readValue((String.class.getClassLoader())));
            instance.photo = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Circularparser[] newArray(int size) {
            return (new Circularparser[size]);
        }

    };

    public String getCircularID() {
        return circularID;
    }

    public void setCircularID(String circularID) {
        this.circularID = circularID;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getDateOfCircular() {
        return dateOfCircular;
    }

    public void setDateOfCircular(String dateOfCircular) {
        this.dateOfCircular = dateOfCircular;
    }

    public String getGradeID() {
        return gradeID;
    }

    public void setGradeID(String gradeID) {
        this.gradeID = gradeID;
    }

    public String getCircularTitle() {
        return circularTitle;
    }

    public void setCircularTitle(String circularTitle) {
        this.circularTitle = circularTitle;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getCircularDetails() {
        return circularDetails;
    }

    public void setCircularDetails(String circularDetails) {
        this.circularDetails = circularDetails;
    }

    public String getCircularTypeTerm() {
        return circularTypeTerm;
    }

    public void setCircularTypeTerm(String circularTypeTerm) {
        this.circularTypeTerm = circularTypeTerm;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDivisionID() {
        return divisionID;
    }

    public void setDivisionID(String divisionID) {
        this.divisionID = divisionID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(circularID);
        dest.writeValue(teacherName);
        dest.writeValue(dateOfCircular);
        dest.writeValue(gradeID);
        dest.writeValue(circularTitle);
        dest.writeValue(fileType);
        dest.writeValue(circularDetails);
        dest.writeValue(circularTypeTerm);
        dest.writeValue(teacherID);
        dest.writeValue(profilePic);
        dest.writeValue(divisionID);
        dest.writeValue(subjectID);
        dest.writeValue(isRead);
        dest.writeValue(seqNo);
        dest.writeValue(subjectName);
        dest.writeValue(isApproved);
        dest.writeValue(photo);
    }

    public int describeContents() {
        return 0;
    }

}