package com.edusunsoft.erp.orataro.database;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface GroupListDataDao {

    @Insert
    void insertGroupList(GroupListModel groupListModel);

    @Query("SELECT * from GroupList")
    List<GroupListModel> getGroupList();

    @Query("DELETE  FROM GroupList")
    int deleteGroupList();

    @Query("delete from GroupList where groupId=:groupid")
    void deleteGroupByGroupID(String groupid);

}
