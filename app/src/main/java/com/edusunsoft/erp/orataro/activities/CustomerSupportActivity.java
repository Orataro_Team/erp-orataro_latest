package com.edusunsoft.erp.orataro.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerSupportActivity extends Activity implements OnItemSelectedListener {

    Context mContext = CustomerSupportActivity.this;
    Spinner spn_type;
    String strType = "";
    ImageView imgLeftheader, imgRightheader;
    TextView txtHeader, txtPhone;
    EditText edt_detail;
    LinearLayout ll_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customersupport);

        spn_type = (Spinner) findViewById(R.id.spn_type);
        imgLeftheader = (ImageView) findViewById(R.id.img_home);
        imgRightheader = (ImageView) findViewById(R.id.img_menu);
        txtHeader = (TextView) findViewById(R.id.header_text);
        ll_send = (LinearLayout) findViewById(R.id.ll_send);
        txtPhone = (TextView) findViewById(R.id.txtPhone);

        imgLeftheader.setImageResource(R.drawable.back);
        imgRightheader.setVisibility(View.INVISIBLE);
        txtHeader.setText(getResources().getString(R.string.Helpdesk));


        imgLeftheader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                finish();

            }

        });

//		if(txtPhone != null){
//			txtPhone.setMovementMethod(LinkMovementMethod.getInstance());
//		}
//
//		txtPhone.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent callIntent = new Intent(Intent.ACTION_CALL);
//				callIntent.setData(Uri.parse("tel:+"+txtPhone.getText().toString().trim()));
//				startActivity(callIntent );
//			}
//		});

        List<String> categories = new ArrayList<String>();
        categories.add("Please Select Type");
        categories.add("Classwork");
        categories.add("Homework");
        categories.add("Circular");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);


        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spn_type.setAdapter(adapter);
        spn_type.setOnItemSelectedListener(this);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        if (position == 0) {
            strType = "";
        } else {
            strType = parent.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }
}
