package com.edusunsoft.erp.orataro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.model.FeesStructureModel;

import java.util.ArrayList;

/**
 * Created by admin on 16-05-2017.
 */

public class FeesStructureAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<FeesStructureModel> listFeesStructure;

    public FeesStructureAdapter(Context mContext, ArrayList<FeesStructureModel> listFeesStructure){
        this.mContext = mContext;
        this.listFeesStructure = listFeesStructure;
    }

    @Override
    public int getCount() {
        return listFeesStructure.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInfalater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInfalater.inflate(R.layout.feesinnerview, viewGroup, false);

        TextView txtfees = (TextView) view.findViewById(R.id.txtfees);
        TextView txtammount = (TextView) view.findViewById(R.id.txtammount);
        TextView txtdue = (TextView) view.findViewById(R.id.txtdue);
        TextView txtaction = (TextView) view.findViewById(R.id.txtaction);
        ImageView imgremove = (ImageView) view.findViewById(R.id.imgremove);

        txtfees.setText(listFeesStructure.get(i).getFeeHeadName());
        txtammount.setText(listFeesStructure.get(i).getOriginalStructureHeadAmount());
        txtdue.setText(listFeesStructure.get(i).getTotalDue());
        txtaction.setText(listFeesStructure.get(i).getTotalDue());

        return view;
    }

}
