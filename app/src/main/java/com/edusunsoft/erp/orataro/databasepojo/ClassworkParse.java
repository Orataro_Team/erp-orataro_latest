package com.edusunsoft.erp.orataro.databasepojo;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassworkParse implements Parcelable {

    @SerializedName("ClassWorkID")
    @Expose
    private String classWorkID;
    @SerializedName("DateOfClassWork")
    @Expose
    private String dateOfClassWork;
    @SerializedName("PostID")
    @Expose
    private String postID;
    @SerializedName("GradeID")
    @Expose
    private String gradeID;
    @SerializedName("DivisionID")
    @Expose
    private String divisionID;
    @SerializedName("SubjectID")
    @Expose
    private String subjectID;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("TeacherID")
    @Expose
    private String teacherID;
    @SerializedName("TecherName")
    @Expose
    private String techerName;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("ClassWorkTitle")
    @Expose
    private String classWorkTitle;
    @SerializedName("ClassWorkDetails")
    @Expose
    private String classWorkDetails;
    @SerializedName("ReferenceLink")
    @Expose
    private String referenceLink;
    @SerializedName("ExpectingDateOfCompletion")
    @Expose
    private String expectingDateOfCompletion;
    @SerializedName("StartTime")
    @Expose
    private String startTime;
    @SerializedName("EndTime")
    @Expose
    private String endTime;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("IsRead")
    @Expose
    private String isRead;
    @SerializedName("IsChecked")
    @Expose
    private String isChecked;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("GradeName")
    @Expose
    private String gradeName;
    @SerializedName("DivisionName")
    @Expose
    private String divisionName;
    public final static Parcelable.Creator<ClassworkParse> CREATOR = new Creator<ClassworkParse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ClassworkParse createFromParcel(Parcel in) {
            ClassworkParse instance = new ClassworkParse();
            instance.classWorkID = ((String) in.readValue((String.class.getClassLoader())));
            instance.dateOfClassWork = ((String) in.readValue((String.class.getClassLoader())));
            instance.postID = ((String) in.readValue((String.class.getClassLoader())));
            instance.gradeID = ((String) in.readValue((String.class.getClassLoader())));
            instance.divisionID = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectID = ((String) in.readValue((String.class.getClassLoader())));
            instance.fileType = ((String) in.readValue((String.class.getClassLoader())));
            instance.teacherID = ((String) in.readValue((String.class.getClassLoader())));
            instance.techerName = ((String) in.readValue((String.class.getClassLoader())));
            instance.profilePicture = ((String) in.readValue((String.class.getClassLoader())));
            instance.classWorkTitle = ((String) in.readValue((String.class.getClassLoader())));
            instance.classWorkDetails = ((String) in.readValue((String.class.getClassLoader())));
            instance.referenceLink = ((String) in.readValue((String.class.getClassLoader())));
            instance.expectingDateOfCompletion = ((String) in.readValue((String.class.getClassLoader())));
            instance.startTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.endTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.userName = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectName = ((String) in.readValue((String.class.getClassLoader())));
            instance.isRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.isChecked = ((String) in.readValue((String.class.getClassLoader())));
            instance.photo = ((String) in.readValue((String.class.getClassLoader())));
            instance.gradeName = ((String) in.readValue((String.class.getClassLoader())));
            instance.divisionName = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ClassworkParse[] newArray(int size) {
            return (new ClassworkParse[size]);
        }

    };

    public String getClassWorkID() {
        return classWorkID;
    }

    public void setClassWorkID(String classWorkID) {
        this.classWorkID = classWorkID;
    }

    public String getDateOfClassWork() {
        return dateOfClassWork;
    }

    public void setDateOfClassWork(String dateOfClassWork) {
        this.dateOfClassWork = dateOfClassWork;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getGradeID() {
        return gradeID;
    }

    public void setGradeID(String gradeID) {
        this.gradeID = gradeID;
    }

    public String getDivisionID() {
        return divisionID;
    }

    public void setDivisionID(String divisionID) {
        this.divisionID = divisionID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTecherName() {
        return techerName;
    }

    public void setTecherName(String techerName) {
        this.techerName = techerName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getClassWorkTitle() {
        return classWorkTitle;
    }

    public void setClassWorkTitle(String classWorkTitle) {
        this.classWorkTitle = classWorkTitle;
    }

    public String getClassWorkDetails() {
        return classWorkDetails;
    }

    public void setClassWorkDetails(String classWorkDetails) {
        this.classWorkDetails = classWorkDetails;
    }

    public String getReferenceLink() {
        return referenceLink;
    }

    public void setReferenceLink(String referenceLink) {
        this.referenceLink = referenceLink;
    }

    public String getExpectingDateOfCompletion() {
        return expectingDateOfCompletion;
    }

    public void setExpectingDateOfCompletion(String expectingDateOfCompletion) {
        this.expectingDateOfCompletion = expectingDateOfCompletion;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(String isChecked) {
        this.isChecked = isChecked;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(classWorkID);
        dest.writeValue(dateOfClassWork);
        dest.writeValue(postID);
        dest.writeValue(gradeID);
        dest.writeValue(divisionID);
        dest.writeValue(subjectID);
        dest.writeValue(fileType);
        dest.writeValue(teacherID);
        dest.writeValue(techerName);
        dest.writeValue(profilePicture);
        dest.writeValue(classWorkTitle);
        dest.writeValue(classWorkDetails);
        dest.writeValue(referenceLink);
        dest.writeValue(expectingDateOfCompletion);
        dest.writeValue(startTime);
        dest.writeValue(endTime);
        dest.writeValue(userName);
        dest.writeValue(subjectName);
        dest.writeValue(isRead);
        dest.writeValue(isChecked);
        dest.writeValue(photo);
        dest.writeValue(gradeName);
        dest.writeValue(divisionName);
    }

    public int describeContents() {
        return 0;
    }

}