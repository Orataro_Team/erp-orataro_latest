package com.edusunsoft.erp.orataro.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.edusunsoft.erp.orataro.FragmentActivity.HomeWorkFragmentActivity;
import com.edusunsoft.erp.orataro.FragmentActivity.StudentTeacherListActivity;
import com.edusunsoft.erp.orataro.Interface.ICancleAsynkTask;
import com.edusunsoft.erp.orataro.Interface.ResponseWebServices;
import com.edusunsoft.erp.orataro.R;
import com.edusunsoft.erp.orataro.activities.AddDataByTeacherActivity;
import com.edusunsoft.erp.orataro.activities.AddNotesActivity;
import com.edusunsoft.erp.orataro.activities.ListLeaveActivity;
import com.edusunsoft.erp.orataro.activities.StudentInformationActivity;
import com.edusunsoft.erp.orataro.adapter.DivisionAdapter;
import com.edusunsoft.erp.orataro.adapter.StdDivSubAdapter;
import com.edusunsoft.erp.orataro.adapter.SubjectListAdapter;
import com.edusunsoft.erp.orataro.database.ERPOrataroDatabase;
import com.edusunsoft.erp.orataro.database.HolidaysModel;
import com.edusunsoft.erp.orataro.database.StdDivSubDataDao;
import com.edusunsoft.erp.orataro.database.StdDivSubModel;
import com.edusunsoft.erp.orataro.model.DivisionModel;
import com.edusunsoft.erp.orataro.model.PropertyVo;
import com.edusunsoft.erp.orataro.model.StandardModel;
import com.edusunsoft.erp.orataro.model.SubjectModel;
import com.edusunsoft.erp.orataro.services.AsynsTaskClass;
import com.edusunsoft.erp.orataro.services.ServiceResource;
import com.edusunsoft.erp.orataro.services.WebserviceCall;
import com.edusunsoft.erp.orataro.util.Global;
import com.edusunsoft.erp.orataro.util.LoaderProgress;
import com.edusunsoft.erp.orataro.util.UserSharedPrefrence;
import com.edusunsoft.erp.orataro.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListselectionFragment extends Fragment implements OnClickListener, OnItemClickListener, ResponseWebServices {

    ListView lv_standard;
    TextView txt_next, txtSubject;
    ImageView imgLeftHeader, imgRightHeader;
    TextView txt_header;
    boolean isStanderd = true, isDivision, isSubject, isClassWork, isHomeWork, isWIthoutSubject;
    Context mContext;
    int pos = 0;
    String gradeId;
    String from;
    boolean isActivity;

    TextView txt_nodatafound;
    private boolean isNote;
    private LinearLayout header_layout;
    private static final String POS = "pos";
    private static final String ISFROM = "isFrom";
    private static final String GRADEID = "gradeId";
    private static final String ISHOMEWORK = "isHomeWork";
    private static final String ISCLASSWORK = "isClassWork";
    private static final String ISSUBJECT = "isSubject";
    private static final String ISDIVISION = "isDivision";
    private static final String ISACTIVITY = "isActivity";
    private static final String ISWITHOUTSUBJECT = "isWithoutSubject";

    // variable declaration for homeworklist from offline database
    StdDivSubDataDao stdDivSubDataDao;
    private List<StdDivSubModel> StdDivSubList = new ArrayList<>();
    StdDivSubModel stdDivSubModel = new StdDivSubModel();
    /*END*/

    public static final ListselectionFragment newInstance(boolean isDivision, boolean isSubject, boolean isClasswork, boolean isHomework,
                                                          String gradeId, String isFrom, int pos, boolean isActivity, boolean isWithoutSubject) {
        ListselectionFragment f = new ListselectionFragment();
        Bundle bdl = new Bundle(2);
        bdl.putBoolean(ISHOMEWORK, isHomework);
        bdl.putBoolean(ISCLASSWORK, isClasswork);
        bdl.putBoolean(ISSUBJECT, isSubject);
        bdl.putBoolean(ISDIVISION, isDivision);

        bdl.putString(GRADEID, gradeId);
        bdl.putString(ISFROM, isFrom);
        bdl.putInt(POS, pos);
        bdl.putBoolean(ISACTIVITY, isActivity);
        bdl.putBoolean(ISWITHOUTSUBJECT, isWithoutSubject);
        f.setArguments(bdl);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_list_selection, null);
        mContext = getActivity();

        stdDivSubDataDao = ERPOrataroDatabase.getERPOrataroDatabase(getActivity()).stdDivSubDataDao();

        txt_header = (TextView) v.findViewById(R.id.header_text);
        header_layout = (LinearLayout) v.findViewById(R.id.header_layout);
        imgLeftHeader = (ImageView) v.findViewById(R.id.img_back);
        imgRightHeader = (ImageView) v.findViewById(R.id.img_menu);
        lv_standard = (ListView) v.findViewById(R.id.lv_standard);

        imgLeftHeader.setImageResource(R.drawable.back);
        imgRightHeader.setVisibility(View.INVISIBLE);
        imgLeftHeader.setOnClickListener(this);

        txtSubject = (TextView) v.findViewById(R.id.txtSubject);

        Global.StandardModels = new ArrayList<StandardModel>();

        txt_nodatafound = (TextView) v.findViewById(R.id.txt_nodatafound);

        if (getArguments() != null) {

            isDivision = getArguments().getBoolean("isDivision", false);
            isSubject = getArguments().getBoolean("isSubject", false);
            isClassWork = getArguments().getBoolean("isClassWork", false);
            isHomeWork = getArguments().getBoolean("isHomeWork", false);
            isWIthoutSubject = getArguments().getBoolean(ISWITHOUTSUBJECT);
            gradeId = getArguments().getString("gradeId");
            from = getArguments().getString("isFrom");
            pos = getArguments().getInt("pos", 0);
            isActivity = getArguments().getBoolean(ISACTIVITY, false);
            if (isDivision || isSubject) {
                isStanderd = false;
            }

        }

        try {

            if (isActivity) {

                header_layout.setVisibility(View.VISIBLE);

                try {
                    txt_header.setText(getResources().getString(R.string.ListSelection) + " (" + Utility.GetFirstName(mContext) + ")");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                if (HomeWorkFragmentActivity.txt_header != null) {
                    HomeWorkFragmentActivity.txt_header.setText(getResources().getString(R.string.ListSelection) + " (" + Utility.GetFirstName(mContext) + ")");
                }
            }

        } catch (Exception e) {
        }

        if (from.equalsIgnoreCase(ServiceResource.LEAVE_FLAG)) {
            txtSubject.setVisibility(View.INVISIBLE);
        }

        if (isStanderd) {

            if (ServiceResource.LEAVE_FLAG.equalsIgnoreCase(from)) {

                if (Utility.isNetworkAvailable(mContext)) {
                    StdDivSubList = stdDivSubDataDao.getStdDivList("YES");
                    if (StdDivSubList.isEmpty() || StdDivSubList.size() == 0 || StdDivSubList == null) {
                        StandardGradeListWithLeaveCount(true);
                    } else {
                        //set Adapter
                        setStdDivlistAdapter(StdDivSubList);
                        /*END*/
                        StandardGradeListWithLeaveCount(false);
                    }
                } else {
                    StdDivSubList = stdDivSubDataDao.getStdDivList("YES");
                    if (StdDivSubList != null) {
                        setStdDivlistAdapter(StdDivSubList);
                    }
                }

            } else {

                if (Utility.isNetworkAvailable(mContext)) {
                    StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
                    if (StdDivSubList.isEmpty() || StdDivSubList.size() == 0 || StdDivSubList == null) {
                        StandardGradeList(true);
                    } else {
                        //set Adapter
                        setStdDivlistAdapter(StdDivSubList);
                        /*END*/
                        StandardGradeList(false);
                    }
                } else {
                    StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
                    if (StdDivSubList != null) {
                        setStdDivlistAdapter(StdDivSubList);
                    }
                }

            }

        }

        if (isDivision) {
            //			txt_header.setText(getResources().getString(R.string.selectdivision)+" ("+new UserSharedPrefrence(mContext).getLoginModel().getFirstName()+")");
            if (Utility.isNetworkAvailable(mContext)) {
                new DivisionList().execute();
            } else {
                Utility.showAlertDialog(mContext,
                        mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

        }

        if (isSubject) {
            //			txt_header.setText(getResources().getString(R.string.SelectSubject)+" ("+new UserSharedPrefrence(mContext).getLoginModel().getFirstName()+")");
            if (Utility.isNetworkAvailable(mContext)) {
                new SubjectList().execute();

            } else {
                Utility.showAlertDialog(mContext,
                        mContext.getResources().getString(R.string.PleaseCheckyourinternetconnection), "Error");
            }

        }

        lv_standard.setOnItemClickListener(this);
        return v;

    }

    private void setStdDivlistAdapter(List<StdDivSubModel> stdDivSubList) {

        if (stdDivSubList.size() > 0) {

            StdDivSubAdapter adapter = new StdDivSubAdapter(mContext,
                    stdDivSubList, from);
            lv_standard.setAdapter(adapter);

        } else {

            txt_nodatafound.setVisibility(View.VISIBLE);
            txt_nodatafound.setText(getResources().getString(R.string.NoDataAvailable));

        }

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.img_back) {

            Utility.RedirectToDashboard(getActivity());


        }

    }

    public class DivisionList extends AsyncTask<Void, Void, Void> implements ICancleAsynkTask {

        //		ProgressDialog progressDialog;
        String result;
        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
            //			progressDialog = new ProgressDialog(mContext);
            //			progressDialog.setMessage(mContext.getResources().getString(R.string.pleasewait));
            //			progressDialog.setCancelable(false);
            //			progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();

            map1.put(ServiceResource.GRADEID, Global.StandardModels.get(pos)
                    .getStandrdId());
            map1.put(ServiceResource.DIVISIONID, null);
            map1.put(ServiceResource.TEACHERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map1.put(ServiceResource.TYPE,
                    ServiceResource.STANDERD_WSCALL_TYPE_DIVISION);
            map1.put(ServiceResource.SUBJECTID, null);
            map.put(2, map1);

            Log.d("getDivList", map.toString());

            result = webcall.getJSONFromSOAPWS(
                    ServiceResource.STANDERD_METHODNAME, map,
                    ServiceResource.STANDERD_URL);

            JSONArray hJsonArray;

            try {

                Global.holidaysModel = new HolidaysModel();

                if (result.contains("\"success\":0")) {

                } else {
                    hJsonArray = new JSONArray(result);
                    Global.divisionModels = new ArrayList<DivisionModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        DivisionModel model = new DivisionModel();
                        model.setDivisionId(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONID));
                        model.setDivisionName(hJsonObject
                                .getString(ServiceResource.STANDARD_DIVISIONNAME));
                        Global.divisionModels.add(model);
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                if (LoaderProgress != null) {
                    LoaderProgress.dismiss();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }
            if (Global.StandardModels != null
                    && Global.StandardModels.size() > 0) {
                DivisionAdapter adapter = new DivisionAdapter(mContext,
                        Global.divisionModels);
                lv_standard.setAdapter(adapter);
            } else {

            }

        }

        @Override
        public void onCancleTask() {
            // TODO Auto-generated method stub
            cancel(true);
        }

    }

    public class SubjectList extends AsyncTask<Void, Void, Void> implements ICancleAsynkTask {

        //		ProgressDialog progressDialog;
        String result;
        JSONObject mJsonObject;
        JSONArray jobListJsonArray;
        private com.edusunsoft.erp.orataro.util.LoaderProgress LoaderProgress;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            LoaderProgress = new LoaderProgress(mContext, this);
            LoaderProgress.setMessage(mContext.getResources().getString(R.string.pleasewait));
            LoaderProgress.setCancelable(true);
            LoaderProgress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            WebserviceCall webcall = new WebserviceCall();
            HashMap<Integer, HashMap<String, String>> map = new HashMap<Integer, HashMap<String, String>>();

            HashMap<String, String> map1 = new HashMap<String, String>();

            map1.put(ServiceResource.GRADEID, gradeId);
            map1.put(ServiceResource.DIVISIONID, Global.divisionModels.get(pos)
                    .getDivisionId());
            map1.put(ServiceResource.TEACHERID,
                    new UserSharedPrefrence(mContext).getLoginModel().getMemberID());
            map1.put(ServiceResource.CLIENT_ID,
                    new UserSharedPrefrence(mContext).getLoginModel().getClientID());
            map1.put(ServiceResource.INSTITUTEID,
                    new UserSharedPrefrence(mContext).getLoginModel().getInstituteID());
            map1.put(ServiceResource.TYPE,
                    ServiceResource.STANDERD_WSCALL_TYPE_SUBJECT);
            map1.put(ServiceResource.SUBJECTID, null);
            map.put(2, map1);

            result = webcall.getJSONFromSOAPWS(
                    ServiceResource.STANDERD_METHODNAME, map,
                    ServiceResource.STANDERD_URL);

            JSONArray hJsonArray;
            try {
                Global.holidaysModel = new HolidaysModel();
                if (result.contains("\"success\":0")) {

                } else {
                    hJsonArray = new JSONArray(result);
                    Global.subjectModels = new ArrayList<SubjectModel>();

                    for (int i = 0; i < hJsonArray.length(); i++) {
                        JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                        SubjectModel model = new SubjectModel();
                        model.setSubjectId(hJsonObject
                                .getString(ServiceResource.STANDARD_SUBJECTID));
                        model.setSubjectname(hJsonObject
                                .getString(ServiceResource.STANDARD_SUBJECTNAME));
                        Global.subjectModels.add(model);
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                if (LoaderProgress != null) {
                    LoaderProgress.dismiss();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (LoaderProgress != null) {
                LoaderProgress.dismiss();
            }
            if (Global.StandardModels != null
                    && Global.StandardModels.size() > 0) {
                SubjectListAdapter adapter = new SubjectListAdapter(mContext,
                        Global.subjectModels);
                lv_standard.setAdapter(adapter);
                txt_nodatafound.setVisibility(View.GONE);
            } else {
                txt_nodatafound.setVisibility(View.VISIBLE);
                txt_nodatafound.setText(getResources().getString(R.string.NoStandardAvailable));
            }
        }

        @Override
        public void onCancleTask() {
            // TODO Auto-generated method stub
            cancel(true);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        onItemClick(position, false);
    }

    public void StandardGradeList(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();
        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Utility.getUserModelData(mContext);
        }
        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));
        arrayList.add(new PropertyVo(ServiceResource.ROLE, new UserSharedPrefrence(mContext).getLoginModel().getMemberType()));

        Log.d("getstdrequest", arrayList.toString());

        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME, ServiceResource.STANDERDDIVISIONSUBJECT_URL);

    }


    public void StandardGradeListWithLeaveCount(boolean isViewPopup) {

        ArrayList<PropertyVo> arrayList = new ArrayList<PropertyVo>();

        if (new UserSharedPrefrence(mContext).getLoginModel() == null) {
            Utility.getUserModelData(mContext);
        }

        arrayList.add(new PropertyVo(ServiceResource.INSTITUTEID, new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()));//"0146bb5b-9fd9-4fd7-b3bc-1c5eea9f634c" /*new UserSharedPrefrence(mContext).getLoginModel().getInstituteID()*/));
        arrayList.add(new PropertyVo(ServiceResource.CLIENT_ID, new UserSharedPrefrence(mContext).getLoginModel().getClientID()));//"041d796d-de2f-4fcf-998f-7e4867b954b9"/*new UserSharedPrefrence(mContext).getLoginModel().getClientID()*/));

        arrayList.add(new PropertyVo(ServiceResource.MEMBERID, new UserSharedPrefrence(mContext).getLoginModel().getMemberID()));//"b47d9df1-9228-4066-8201-6bbb51172ab1"/*new UserSharedPrefrence(mContext).getLoginModel().getMemberID()*/));

        Log.d("isstdRequest", arrayList.toString());
        new AsynsTaskClass(mContext, arrayList, isViewPopup, this).execute(ServiceResource.LEAVECOUNTBYGRADEDIVISION_METHODNAME, ServiceResource.LEAVE_URL);

    }

    @Override
    public void response(String result, String methodName) {
        if (ServiceResource.STANDERDDIVISIONSUBJECT_METHODNAME.equalsIgnoreCase(methodName)) {
            parse(result);
        } else if (ServiceResource.LEAVECOUNTBYGRADEDIVISION_METHODNAME.equalsIgnoreCase(methodName)) {
            parseWithCount(result);
        }
    }


    public void onItemClick(int position, boolean isFinish) {

        if (from.equalsIgnoreCase(ServiceResource.CLASSWORK_FLAG_TEACHER)) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
            Intent i = new Intent(mContext, AddDataByTeacherActivity.class);
            i.putExtra("gradeId", StdDivSubList.get(position).getStandrdId());
            i.putExtra("divisionId", StdDivSubList.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", StdDivSubList.get(position)
                    .getSubjectId());
            i.putExtra("isFrom", from);
            startActivity(i);

            getActivity().finish();

        } else if ((from
                .equalsIgnoreCase(ServiceResource.HOMEWORK_FLAG_TEACHER))) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
            Log.d("getlisthere", StdDivSubList.toString());
            Intent i = new Intent(mContext,
                    AddDataByTeacherActivity.class);
            i.putExtra("gradeId", StdDivSubList.get(position).getStandrdId());
            i.putExtra("divisionId", StdDivSubList.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", StdDivSubList.get(position)
                    .getSubjectId());
            i.putExtra("subjectName", StdDivSubList.get(position)
                    .getSubjectName());
            i.putExtra("isFrom", from);
            startActivity(i);
            getActivity().finish();
        } else if ((from
                .equalsIgnoreCase(ServiceResource.HAPPYGRAM_FLAG))) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
            Intent i = new Intent(mContext,
                    HappygramFragment.class);
            i.putExtra("gradeId", StdDivSubList.get(position).getStandrdId());
            i.putExtra("divisionId", StdDivSubList.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", StdDivSubList.get(position)
                    .getSubjectId());
            i.putExtra("subjectName", StdDivSubList.get(position)
                    .getSubjectName());
            i.putExtra("model", StdDivSubList.get(position));
            i.putExtra("isFrom", from);
            startActivity(i);
        } else if ((from
                .equalsIgnoreCase(ServiceResource.NOTE_FLAG_TEACHER))) {
            Intent i = new Intent(mContext,
                    AddNotesActivity.class);
            i.putExtra("gradeId", Global.StandardModels.get(position).getStandrdId());
            i.putExtra("divisionId", Global.StandardModels.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", Global.StandardModels.get(position)
                    .getSubjectId());
            i.putExtra("subjectName", Global.StandardModels.get(position)
                    .getSubjectName());
            i.putExtra("isFrom", from);
            startActivity(i);
            getActivity().finish();
            //			finish();
        } else if ((from
                .equalsIgnoreCase(ServiceResource.HAPPYGRAM_FLAG))) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
            Intent i = new Intent(mContext,
                    HappygramFragment.class);
            i.putExtra("gradeId", StdDivSubList.get(position).getStandrdId());
            i.putExtra("divisionId", StdDivSubList.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", StdDivSubList.get(position)
                    .getSubjectId());
            i.putExtra("subjectName", StdDivSubList.get(position)
                    .getSubjectName());
            i.putExtra("model", StdDivSubList.get(position));
            i.putExtra("isFrom", from);
            startActivity(i);
            getActivity().finish();
        } else if (from
                .equalsIgnoreCase(ServiceResource.PTCOMMUNICATION_FLAG)) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("NO");
            Intent i = new Intent(mContext,
                    StudentTeacherListActivity.class);
            i.putExtra("model", StdDivSubList.get(position));
            startActivity(i);

        } else if (from
                .equalsIgnoreCase(ServiceResource.LEAVE_FLAG)) {

            StdDivSubList = stdDivSubDataDao.getStdDivList("YES");
            Intent i = new Intent(mContext,
                    ListLeaveActivity.class);
            i.putExtra("model", StdDivSubList.get(position));
            startActivity(i);
        } else if (from
                .equalsIgnoreCase(ServiceResource.STUDENT_INFORMATION)) {
            Intent i = new Intent(mContext,
                    StudentInformationActivity.class);
            i.putExtra("model", Global.StandardModels.get(position));
            startActivity(i);
            getActivity().finish();
        } else {
            Intent i = new Intent(mContext,
                    AddDataByTeacherActivity.class);
            i.putExtra("gradeId", Global.StandardModels.get(position).getStandrdId());
            i.putExtra("divisionId", Global.StandardModels.get(position)
                    .getDivisionId());
            i.putExtra("subjectId", Global.StandardModels.get(position)
                    .getSubjectId());
            i.putExtra("isFrom", from);
            startActivity(i);
            getActivity().finish();
        }

        if (isFinish) {
            getActivity().finish();
        }

    }


    public void parse(String result) {

        Log.d("getLeaveresult", result);
        stdDivSubDataDao.deleteStdDivSubItem("NO");
        JSONArray hJsonArray;
        try {

            if (result.contains("\"success\":0")) {
                stdDivSubDataDao.deleteStdDivSubItem("NO");
            } else {

                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StdDivSubModel model = new StdDivSubModel();
                    model.setStandardName(hJsonObject
                            .getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject
                            .getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject
                            .getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject
                            .getString(ServiceResource.DIVISION));
                    model.setIsLeave("NO");
                    if (isWIthoutSubject) {

                    } else {
                        model.setSubjectId(hJsonObject
                                .getString(ServiceResource.STANDARD_SUBJECTID));
                        model.setSubjectName(hJsonObject
                                .getString(ServiceResource.SUBJECT));

                    }
                    stdDivSubDataDao.insertStdDivSub(model);

                }

                if (stdDivSubDataDao.getStdDivList("NO").size() > 0) {
                    //set Adapter
                    setStdDivlistAdapter(stdDivSubDataDao.getStdDivList("NO"));
                    /*END*/
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public void parseWithCount(String result) {

        Log.d("getLeaveresult", result);
        stdDivSubDataDao.deleteStdDivSubItem("YES");
        JSONArray hJsonArray;
        try {

            if (result.contains("\"success\":0")) {
                stdDivSubDataDao.deleteStdDivSubItem("YES");
            } else {

                hJsonArray = new JSONArray(result);

                for (int i = 0; i < hJsonArray.length(); i++) {
                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
                    StdDivSubModel model = new StdDivSubModel();
                    model.setStandardName(hJsonObject
                            .getString(ServiceResource.GRADE));
                    model.setStandrdId(hJsonObject
                            .getString(ServiceResource.STANDARD_GRADEID));
                    model.setDivisionId(hJsonObject
                            .getString(ServiceResource.STANDARD_DIVISIONID));
                    model.setDivisionName(hJsonObject
                            .getString(ServiceResource.DIVISIONNAME));
                    model.setCountleave(hJsonObject
                            .getString(ServiceResource.LEAVECOUNT));
                    model.setIsLeave("YES");
                    if (isWIthoutSubject) {

                    } else {

                        model.setSubjectId(hJsonObject
                                .getString(ServiceResource.STANDARD_SUBJECTID));
                        model.setSubjectName(hJsonObject
                                .getString(ServiceResource.SUBJECT));

                    }

                    stdDivSubDataDao.insertStdDivSub(model);

                }

                if (stdDivSubDataDao.getStdDivList("YES").size() > 0) {
                    //set Adapter
                    setStdDivlistAdapter(stdDivSubDataDao.getStdDivList("YES"));
                    /*END*/
                }

            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


//        JSONArray hJsonArray;
//        try {
//
//            Global.StandardModels.clear();
//            if (result.contains("\"success\":0")) {
//
//
//            } else {
//
//                hJsonArray = new JSONArray(result);
//
//                for (int i = 0; i < hJsonArray.length(); i++) {
//                    JSONObject hJsonObject = hJsonArray.getJSONObject(i);
//                    StandardModel model = new StandardModel();
//                    model.setStandardName(hJsonObject
//                            .getString(ServiceResource.GRADE));
//                    model.setStandrdId(hJsonObject
//                            .getString(ServiceResource.STANDARD_GRADEID));
//                    model.setDivisionId(hJsonObject
//                            .getString(ServiceResource.STANDARD_DIVISIONID));
//                    model.setDivisionName(hJsonObject
//                            .getString(ServiceResource.DIVISIONNAME));
//                    model.setCountleave(hJsonObject
//                            .getString(ServiceResource.LEAVECOUNT));
//                    if (isWIthoutSubject) {
//
//                    } else {
//                        model.setSubjectId(hJsonObject
//                                .getString(ServiceResource.STANDARD_SUBJECTID));
//                        model.setSubjectName(hJsonObject
//                                .getString(ServiceResource.SUBJECT));
//                    }
//
//                    if (isWIthoutSubject) {
//                        if (Global.StandardModels != null && Global.StandardModels.size() > 0) {
//                            boolean isAlready = false;
//                            for (int j = 0; j < Global.StandardModels.size(); j++) {
//                                if (Global.StandardModels.get(j).getStandrdId().equalsIgnoreCase(model.getStandrdId())) {
//                                    if (Global.StandardModels.get(j).getDivisionId().equalsIgnoreCase(model.getDivisionId())) {
//                                        isAlready = true;
//                                        break;
//                                    } else {
//                                        //											Global.StandardModels.add(model);
//                                        //											break;
//                                    }
//
//                                } else {
//                                    //										Global.StandardModels.add(model);
//                                    //										break;
//                                }
//
//                            }
//                            if (!isAlready) {
//
//                                Global.StandardModels.add(model);
//
//                            }
//
//                        } else {
//                            Global.StandardModels.add(model);
//                        }
//                    } else {
//                        Global.StandardModels.add(model);
//                    }
//
//                    //						Global.StandardModels.add(model);
//
//                    Log.d("standardmodel", Global.StandardModels.toString());
//
//                }
//            }
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//
//        if (Global.StandardModels != null
//                && Global.StandardModels.size() > 0) {
//
//            StanderdAdapter adapter = new StanderdAdapter(mContext,
//                    Global.StandardModels, from);
//            lv_standard.setAdapter(adapter);
//            txt_nodatafound.setVisibility(View.GONE);
//
//        } else {
//
//            txt_nodatafound.setVisibility(View.VISIBLE);
//            txt_nodatafound.setText(getResources().getString(R.string.NoDataAvailable));
//
//        }

    }


    public boolean onBackPressed() {
        return true;
    }

}
