package com.edusunsoft.erp.orataro.imagepicker;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.fragment.app.Fragment;

/**
 * Created by sunwei on 7/1/16.
 */
public class PermissionsHelper {

	@SuppressLint("NewApi") public static boolean checkAndRequestPermission(Activity activity, int requestCode) {
		if (isHavePermission(activity)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressLint("NewApi") public static boolean checkAndRequestPermission(Fragment fragment, int requestCode) {
		if (isHavePermission(fragment.getActivity())) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressLint("NewApi") private static boolean isHavePermission(Activity activity) {
		return true;
	}
}
